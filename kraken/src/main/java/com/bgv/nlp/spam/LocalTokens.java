package com.bgv.nlp.spam;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.google.common.base.Splitter;

public class LocalTokens {
	String normalizeText(String content) {
//		private  String normalizeText(String content, Tokenizer tokenizer) {
		    String trainstr ="";
			 String newStr = content.replaceAll("\\p{Cntrl}", " ");
//			 newStr = newStr.replaceAll("\\d+", "");
//			 text = newStr.replaceAll("\\^w", "");
//			 newStr = text.replaceAll("[^�-��-�\\s\\.]", "");
//			 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); System.out.println("TEXT="+newStr); return newStr;}
			// try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){return newStr;}
			 
			 try{
				 
				 trainstr=getTokets(newStr);
			 
			 }catch(Exception e){return newStr;}
			 
			 
		return trainstr;
		}
		
		private String getTokets(String text) {
			StringBuffer sb=new StringBuffer();
			String tokens[]	= text.toString().split("\\p{P}?[ \\t\\n\\r]+");
			for (String a : tokens)
				sb.append(a).append(" ");
			   
			
			return sb.toString();
			
		}
		public String[] buildContext(String text) {
			List<String> tokens = newArrayList(Splitter.on(' ').split(text));
			return tokens.toArray(new String[tokens.size()]);
		}
	
}
