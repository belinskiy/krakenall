package com.bgv.nlp.spam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.bgv.springmvc.domain.SpamTextStatus;
import com.google.common.base.Splitter;

import opennlp.maxent.GISModel;
import opennlp.tools.tokenize.Tokenizer;

@Service
public class SpamImpl implements SpamDAO {

	@Autowired
	 private TokenizerDAO token;
	@Autowired
	 private GISModelDAO model;
	
	@Override
	public SpamTextStatus discover(String id, String text) {
		SpamTextStatus result = new SpamTextStatus();
		String evaltype = "";
		String evalvalues = "";
		String guid = id;
		String msg = "common error";
		String response = "false";

		
		
		try{
			
			Tokenizer tokenizer = token.getTokenizers();
			GISModel currentModel = model.getGISModel();
			String t ="";
			 try{	// t = normalizeText(text, tokenizer);
				   t = normalizeText(text);
			 
		        }catch(Exception e){e.printStackTrace();}
			 
			 if(!t.equals("")&&t!=null){
			       double[] out = currentModel.eval(buildContext(t));
			       evaltype = currentModel.getBestOutcome(out);
				  //int numOutcomes = currentModel.getNumOutcomes();
				   evalvalues = currentModel.getAllOutcomes(out);
				  //Object[] data = currentModel.getDataStructures();
				   response = "true";
				   msg = "";
			 }	else{
				 msg="empty sentese";
				 response = "false";
			 }  
				  
			
		}catch(Exception e){e.printStackTrace();} 
		
		
		result.setEvaltype(evaltype);

		result.setEvalvalues(evalvalues);

		result.setGuid(guid);

		result.setMsg(msg);

		result.setResponse(response);

		return result;
	}

	private String normalizeText(String content) {
//	private  String normalizeText(String content, Tokenizer tokenizer) {
	    String trainstr ="";
		 String newStr = content.replaceAll("\\p{Cntrl}", " ");
//		 newStr = newStr.replaceAll("\\d+", "");
//		 text = newStr.replaceAll("\\^w", "");
//		 newStr = text.replaceAll("[^�-��-�\\s\\.]", "");
//		 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); System.out.println("TEXT="+newStr); return newStr;}
		// try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){return newStr;}
		 
		 try{
			 
			 trainstr=getTokets(newStr);
		 
		 }catch(Exception e){return newStr;}
		 
		 
	return trainstr;
	}
	
	private String getTokets(String text) {
		StringBuffer sb=new StringBuffer();
		//String tokens[]	= sb.toString().replaceAll("[\\W&&[^\\s]]", "").split("\\W+");
		String tokens[]	= text.toString().split("\\p{P}?[ \\t\\n\\r]+");
		for (String a : tokens)
			sb.append(a).append(" ");
		   
		
		return sb.toString();
		
	}
	public String[] buildContext(String text) {
		List<String> tokens = newArrayList(Splitter.on(' ').split(text));
		return tokens.toArray(new String[tokens.size()]);
	}
	
//	private  String getTokets(String text, Tokenizer tokenizer) {
//		StringBuffer sb=new StringBuffer();
//		String tokens[] = tokenizer.tokenize(text);
//		for (String a : tokens)
//			sb.append(a).append(" ");
//		   
//		
//		return sb.toString();
//		
//	}
}
