package com.bgv.nlp.spam;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bgv.entity.Person;
import com.bgv.service.LearnMESpam;
import com.bgv.springmvc.domain.Config;

import opennlp.maxent.GISModel;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
public class MysqlSelect {
	 public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException
	  {
		  String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://172.16.252.26/nlp";
	      Class.forName(myDriver);
	      Connection conns = DriverManager.getConnection(myUrl, "admin", "kjwQ5%6JYgmu");
	      
		List<Person> p = new ArrayList<Person>();
	    Map<String,String> mp=new HashMap<String,String>();
	    
		int count = ind();
		System.out.print(""+count+"\n");
		int kk=1;
		String id; 
		int counter =1;
		ArrayList list=new ArrayList(); 
        Statement selectStatement = conns.createStatement();
		for(int i=1; i< count/100;i++)
		{
			
			int b= (i-1);
					int e=(i *100)-1;
			
					String querys = "SELECT * FROM person limit " + b +", " + e ; 
					selectStatement.execute(querys); 
					ResultSet rs=selectStatement.getResultSet(); 
		    while(rs.next()){     
		    	int ids = rs.getInt("pid");
		        String city = rs.getString("city");
		        String gender = rs.getString("gender");
		      
		        //Person ed =new Person(); 
		        //ed.setCity(city);
		        //ed.setGender(gender);
		        mp.put(city, gender);
				//p.add(ed);
				//System.out.format("%s, %s, %s\n", ids, city, gender);
				System.out.print("--"+kk+"\n");
				kk++;
		    }
		 }
		try {
			selectStatement.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/*
		try
	    {
	      // create our mysql database connection
	    
	      Connection conn = DriverManager.getConnection(myUrl, "admin", "kjwQ5%6JYgmu");
	      
	      // our SQL SELECT query. 
	      // if you only need a few columns, specify them by name instead of using "*"
	      //String query = "SELECT * FROM person LIMIT 100"; WHERE `id` > :OFFSET LIMIT :BATCH_SIZE;

	      String query = "SELECT * FROM person";
	      // create the java statement
	      Statement st = conn.createStatement();
	      
	      // execute the query, and get a java resultset
	      ResultSet rs = st.executeQuery(query);
	      
	      // iterate through the java resultset
	      while (rs.next())
	      {
	        int id = rs.getInt("pid");
	        String city = rs.getString("city");
	        String gender = rs.getString("gender");
	      
	        Person e =new Person(); 
	        e.setCity(city);
	        e.setGender(gender);
			p.add(e);
	        // print the results
	        // System.out.format("%s, %s, %s\n", id, city, gender);
	      }
	      st.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception! ");
	      System.err.println(e.getMessage());
	    }
		
		*/
	    Config cf=new Config();
		
		 String rooot=cf.getRoot();
	    InputStream is = new FileInputStream(rooot+"en-token.bin");
		ArrayList nameslist = new ArrayList();
		
		TokenizerModel tmodel = new TokenizerModel(is);

		TokenizerME tokenizerd = new TokenizerME(tmodel);
	    
		Tokenizer tokenizer = tokenizerd;
	    
		String modelname = new Config().getModel();
	    
	    
    	java.util.Date date= new java.util.Date();
	        Timestamp d = new Timestamp(date.getTime());
    	
    	Collection<LearningSample> el=new ArrayList<LearningSample>();;
    	int k=0;
    	for (Map.Entry<String, String> entry : mp.entrySet()){
    	//for(Person w:p){
    	//String content =w.getCity();
    	//String clss =w.getGender();	
        	String content =entry.getKey();
        	String clss =entry.getValue();	
    		String ltext = normalizeText(content, tokenizer); 
    		 System.out.println("EXPORT="+ltext+" \n");
    		 ltext = normalizeText(content); 
    		 System.out.println("IMPORT="+ltext);
    		LearningSample mind=new LearningSample(clss, ltext);
    		el.add(mind);
    	k++;
    	}
    	System.out.println("Save model...");
    	 if (k>0){
    		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		       modelname = "spammodel_"+dateFormat.format( new Date() ) ;
		     
		       System.out.println("modelname="+modelname);      
		       LearnMESpam   learnService = new    LearnMESpam();
		 learnService.learn(el, modelname);
	     try {
			Thread.currentThread().sleep(15000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		 String fname=modelname;
		 String modelFileName=rooot+fname;
		 GISModel newmodel = (GISModel)new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel();
		 
		 cf.setModel(modelname);
		 
    	 }
	  }
	 
		 private static String normalizeText(String content) {
			     //String trainstr ="";
				 String newStr = content.replaceAll("\\p{Cntrl}", " ");
				 try{
					 newStr=getTokets(newStr);
				 }catch(Exception e){ e.printStackTrace(); return newStr;}
				 
				 
			return newStr;
			}
			
			private static String getTokets(String text) {
				StringBuffer sb=new StringBuffer();
			
				String tokens[]	= text.toString().toString().split("\\p{P}?[ \\t\\n\\r]+");
				for (String a : tokens)
					sb.append(a).append(" ");
				   
				
				return sb.toString();
				
			}
			
			
	private static String normalizeText(String content, Tokenizer tokenizer) {
		     String trainstr ="";
			 String newStr = content.replaceAll("\\p{Cntrl}", " ");

			 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); return newStr;}
			 
				 
		return trainstr;
	}



	private static String getTokets(String text, Tokenizer tokenizer) {
		StringBuffer sb=new StringBuffer();
		String tokens[] = tokenizer.tokenize(text);
		for (String a : tokens)
			sb.append(a).append(" ");
		   
		
		return sb.toString();
		
	}
	
	public static int ind() throws SQLException, ClassNotFoundException{
		 String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://172.16.252.26/nlp";
	      Class.forName(myDriver);
	      Connection conn = DriverManager.getConnection(myUrl, "admin", "kjwQ5%6JYgmu");
		String query = "SELECT COUNT(*) FROM person";
		int count=0;
		
		Statement selectStatement = conn.createStatement();
		selectStatement.execute(query); 
		ResultSet rs=selectStatement.getResultSet(); 
		while(rs.next()){     
		count=rs.getInt(1);     
		}
		selectStatement.close();
		return count;
		
	}
}
