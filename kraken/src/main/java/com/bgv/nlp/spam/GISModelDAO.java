package com.bgv.nlp.spam;

import opennlp.maxent.GISModel;

public interface GISModelDAO {

	GISModel getGISModel();

	void setCurrentModel(GISModel currentModel);

}
