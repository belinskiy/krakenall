package com.bgv.nlp.spam;

public class Sentence {
	public String id;
	
	public String speech;
	
	public String evaluation;
	
	public String url;
	
	@Override
	public String toString() {
    return evaluation+" "+speech+"("+id+")"+url;
	
	  }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSpeech() {
		return speech;
	}

	public void setSpeech(String speech) {
		this.speech = speech;
	}

	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
