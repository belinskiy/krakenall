package com.bgv.nlp.obj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bgv.dao.IObjectsDAO;
import com.bgv.entity.Person;
import com.bgv.entity.Sheduler;
import com.bgv.nlp.spam.EvalPrecisionDAO;
import com.bgv.nlp.spam.GISModelDAO;
import com.bgv.nlp.spam.LearningSample;
import com.bgv.nlp.spam.TokenizerDAO;
import com.bgv.service.ILearnMESpam;
import com.bgv.service.IPersonService;
import com.bgv.service.IShedulerService;
import com.bgv.springmvc.domain.Config;

import opennlp.maxent.GISModel;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.InvalidFormatException;
//import org.apache.lucene.analysis.PorterStemmer;

@Service
public class NameFinderModelMEImpl implements NameFinderModelMEDAO{
	static NameFinderME	currentModel;
	static Map<String, String> swordmap=new ConcurrentHashMap<String, String>();
	static Map<String, String> liniarmap=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> weakSynonimMap=new ConcurrentHashMap<String, String>();
	static Map<String, String> weakContextMap=new ConcurrentHashMap<String, String>();
	static Map<String, String> matwords=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> citataMap=new ConcurrentHashMap<String, String>();
	static Map<String, int[]> citataIntMap=new ConcurrentHashMap<String, int[]>();
	static Map<String, String> objectTypeMap=new ConcurrentHashMap<String, String>();
	
	static LuceneMorphology luceneMorph;
	
	@Override
	public LuceneMorphology getLuceneMorph() {
		return luceneMorph;
	}

	public void setLuceneMorph(LuceneMorphology luceneMorph) {
		NameFinderModelMEImpl.luceneMorph = luceneMorph;
	}

	@Autowired
	 private TokenizerDAO token;
	
	@Autowired
	 private IObjectsDAO objcs;
	
	@Autowired
		private IPersonService personService;
	 @Autowired
		private ILearnMESpam learnService;
	 
	 @Autowired
		private IShedulerService shedulerService;
	 
	 @Autowired
	 private GISModelDAO spammodel;
	 
	 @Autowired
	 private EvalPrecisionDAO precision;
	 
	@Override
	public Map<String, String> getSwordmap() {
		return swordmap;
	}

	@Override
	public Map<String, String> getLiniarmap() {
		return liniarmap;
	}
	
	@Override
	public Map<String, String> getSynonimMap() {
		return weakSynonimMap;
	}
	
	@Override
	public Map<String, String> getContextMap() {
		return weakContextMap;
	}

	
	@Override
	public Map<String, String> getMatmap() {
		return matwords;
	}
	
	@Override
	public NameFinderME getNameFinderModel() {
		
		return currentModel;
	}
	@Override
	public Map<String, String> get�itataMap() {
		return citataMap;
	}
	
	@Override
	public Map<String, String> getObjectTypeMap() {
		return objectTypeMap;
	}
	
	
	@Override
	public Map<String, int[]> get�itataIntMap() {
		return citataIntMap;
	}
	
	
	@PostConstruct
	public void initialize() throws IOException{
		 Config cf=new Config();
		 String rooot=cf.getRoot();
		 String name=cf.getNamefindermodel();
		 String objtext=cf.getObjectstext();
		 String textFileName=rooot+objtext;
		 
		 String modelFileName=rooot+name;
		 
		    InputStream modelIn = new FileInputStream(modelFileName);
			TokenNameFinderModel model = new TokenNameFinderModel(modelIn);
			
			
			 currentModel =  new NameFinderME(model);
			 File f = new File(textFileName);
			 if(f.exists() && !f.isDirectory()) { 
				 textFileName=rooot+objtext;
			 }else{
				 textFileName=rooot+"objects_v2.txt";
			 }
			 loadObjects(textFileName);
			 
			 
			 LuceneMorphology luceneMorph = new RussianLuceneMorphology();
			 setLuceneMorph(luceneMorph);
			 
			// PorterStemmer stemmer = new PorterStemmer();
			 
			 readMat(rooot+"rumatdictionary.txt");
	}

	@Scheduled(fixedRate=20000000)
    public void doTask() throws IOException {
		System.out.println("Start learn tasks...");
		//Tokenizer tokenizer = token.getTokenizer();
		java.util.Date dates= new java.util.Date();
		Timestamp stamp = new Timestamp(dates.getTime());
		   
		List<Sheduler> s = shedulerService.getAllSheduler();
		Sheduler toupdate = new Sheduler();
		toupdate.setLastupdate(stamp);
		Config conf=new Config(); 
		String modelname = conf.getModel();
		for(Sheduler l:s){
		int	spammodelmax=0;
		double bestvalue=0;
		
		String name = l.getSname();	
		
		if(name.equals("spammodel")){
			
				spammodelmax=l.getLastpid();
				toupdate=l;
				String tmodelname = l.getModelname();
				if(!tmodelname.equals("")&&tmodelname!=null){
					modelname = tmodelname;
					 BigDecimal bd=l.getPrecisioncount();
					 double d = bd.doubleValue(); 
					 
					bestvalue=d;
				}
				
			}
		int maxdocprocessed = 0;	
		
			maxdocprocessed = personService.getMaxDocuments();
			
			
		if(maxdocprocessed>spammodelmax){
			
			toupdate.setLastpid(maxdocprocessed);
	    	java.util.Date date= new java.util.Date();
   	        Timestamp d = new Timestamp(date.getTime());
	    	List<Person> p = personService.getAllDocuments(d);
	    	Collection<LearningSample> el=new ArrayList<LearningSample>();
	    	int k=0;
	    	Map<String,String> mp=new HashMap<String,String>();
	    	int limit=0;
	    	int limmax=conf.getLimitEdu();
	    	for(Person w:p){
	    		String content =w.getCity();
	    		//String ltext = normalizeText(content, tokenizer); 
	    		String ltext = normalizeText(content); 
	    		String cls=w.getGender();
	    		mp.put(ltext, cls);
	    		
	    		limit++;
	    		if(limit>limmax) 
	    		break; 
	    		
	    	k++;
	    	}
	    	for (Map.Entry<String, String> entry : mp.entrySet()){
	        	    String content =entry.getKey();
	            	String clss =entry.getValue();	
	            	LearningSample mind=new LearningSample(clss, content);
		    		el.add(mind);
	    	
	    	}
	    
	    	
	    	System.out.println("Save model...");
	    	 if (k>0){
	    		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
			       modelname = "spammodel_"+dateFormat.format( new Date() ) ;
			       toupdate.setModelname(modelname);
			       System.out.println("modelname="+modelname);      
			       
			 learnService.learn(el, modelname);
		     try {
				Thread.currentThread().sleep(10000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		     Config cf=new Config();
			 String rooot=cf.getRoot();
			 String fname=modelname;
			 String modelFileName=rooot+fname;
			 String evalfile=cf.getEvalfile();
			 
			 String evalfilename=rooot+evalfile;
			
			 System.out.println("precision.bestvalue="+bestvalue);
			 double current = precision.evaluateModel(evalfilename, modelFileName); 
			 System.out.println("precision.evaluateModel="+current);
			 //��� ����� ������ ���������
			 //������ toupdate �� �������� ������ ���������
			 
			 if(bestvalue<current){
		     BigDecimal bcurrent = new BigDecimal(current, MathContext.DECIMAL64);
			 toupdate.setPrecisioncount(bcurrent);
			 
			 GISModel newmodel = (GISModel)new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel();
			 spammodel.setCurrentModel(newmodel); 
			 cf.setModel(modelname);}
			 
	    	 }else{
	    	    
	    		 //��������� � ���� ���
	    		 shedulerService.updateShedulerLog(toupdate);
	    		 //������ �� ��������� � ������
	    		 toupdate=null;
	    	 }
		}
		
		}
		
		if(toupdate!=null)
		{	
			  
			//!!!! �� ��������� ����� ������ ��� ������� � ��������
			shedulerService.updateSheduler(toupdate);
	    }
		int o = personService.getMaxDocuments();
    	
		System.out.println("Task Completed...");
    	System.out.println("="+o);

    	
    }
	
	private String normalizeText(String content) {
//		private  String normalizeText(String content, Tokenizer tokenizer) {
		    String trainstr ="";
			 String newStr = content.replaceAll("\\p{Cntrl}", " ");
//			 newStr = newStr.replaceAll("\\d+", "");
//			 text = newStr.replaceAll("\\^w", "");
//			 newStr = text.replaceAll("[^�-��-�\\s\\.]", "");
//			 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); System.out.println("TEXT="+newStr); return newStr;}
			// try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){return newStr;}
			 
			 try{
				 
				 trainstr=getTokets(newStr);
			 
			 }catch(Exception e){return newStr;}
			 
			 
		return trainstr;
		}
		
		private String getTokets(String text) {
			StringBuffer sb=new StringBuffer();
			String tokens[]	= text.toString().toString().split("\\p{P}?[ \\t\\n\\r]+");
			for (String a : tokens)
				sb.append(a).append(" ");
			   
			
			return sb.toString();
			
		}	   
//	   private String normalizeText(String content, Tokenizer tokenizer) {
//		     String trainstr ="";
//			 String newStr = content.replaceAll("\\p{Cntrl}", " ");
////			 newStr = newStr.replaceAll("\\d+", "");
////			 text = newStr.replaceAll("\\^w", "");
////			 text = text.replaceAll("[^�-��-�\\s\\.]", "");
//			 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); return newStr;}
//			 
//			 
//		return trainstr;
//	}
//
//
//
//	private String getTokets(String text, Tokenizer tokenizer) {
//		StringBuffer sb=new StringBuffer();
//		String tokens[] = tokenizer.tokenize(text);
//		for (String a : tokens)
//			sb.append(a).append(" ");
//		   
//		
//		return sb.toString();
//		
//	}
		
		
		@Scheduled(fixedRate=1080000)
	    public void updateObjectTask() throws IOException {
			
			try {
				boolean sts=objcs.update();
				 Config cf=new Config();
				 String rooot=cf.getRoot();
				
				 String objtext=cf.getObjectstext();
				 String textFileName=rooot+objtext;
					if(sts){
							loadObjects(textFileName);
					}else{
					
						textFileName=rooot+"objects_v2.txt";
						loadObjects(textFileName);
					}
				
				
			} catch (ClassNotFoundException | SQLException e) {
				
				e.printStackTrace();
			}
			
			try {
				 boolean sts=objcs.updateCitation();
				 Config cf=new Config();
				 String rooot=cf.getRoot();
				
				 String objtext=cf.getCitationtext();
				 String textFileName=rooot+objtext;
					if(sts){
							loadCitation(textFileName);
					}else{
					
						textFileName=rooot+"citations_v2.txt";
						loadCitation(textFileName);
					}
				
				
			} catch (ClassNotFoundException | SQLException e) {
				
				e.printStackTrace();
			}
			
			
		}
		 public void loadCitation(String textFileName) throws InvalidFormatException, IOException{
			 Map<String, int[]> lcitataMap=new ConcurrentHashMap<String, int[]>();
			 
			 try {
				    
				 
				   File file=new File(textFileName);
	          
	                  FileInputStream inStream=new FileInputStream(file);
	                  InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

	             
				 
				     JSONParser parser = new JSONParser();
	//Linux
				     Object obj = parser.parse(new BufferedReader(reader));
				    		
				    //Windows		
		            //Object obj = parser.parse(new FileReader(textFileName));
		 
		            JSONObject jsonObject = (JSONObject) obj;
		 
		           
		            JSONArray companyList = (JSONArray) jsonObject.get("Company List");
		 
		            Iterator<JSONObject> iterator = companyList.iterator();
		            while (iterator.hasNext()) {
		            	try{
		            	JSONObject json = iterator.next();
	          	    	String ids = (String) ""+json.get("id");
	          	    	String names = (String) json.get("name");
	          	    	JSONObject main = (JSONObject) parser.parse(names);
	          	    	
	          	    	String id = (String) ""+main.get("Id");
	          	    	String expresion = (String) ""+main.get("Expresion");
	          	    	int pDistance =0;
	          	    	int pDistanceAfter=0;
	          	    	int oDistance =0;
	          	    	int oDistanceAfter=0;
	          	    	
	          	    	 try{ JSONObject person = (JSONObject) main.get("Person");
	          	    	 if(person!=null){
	          	    	 try{ String pDistances = (String) ""+person.get("Distance"); pDistance=Integer.parseInt(pDistances.trim());} catch (Exception e) {}
	          	    	 try{ String pDistanceAfters = (String) ""+person.get("DistanceAfter"); pDistanceAfter=Integer.parseInt(pDistanceAfters.trim());} catch (Exception e) {}}} catch (Exception e) {}
	          	    	
	          	    	
	          	    	
	          	    	
	          	    	 try{ JSONObject organization = (JSONObject) main.get("Organization");
	          	    	 if(organization!=null){
	          	    	 try{ String  oDistances = (String) ""+organization.get("Distance");         oDistance=Integer.parseInt(oDistances.trim());} catch (Exception e) {}
	          	    	 try{ String oDistanceAfters = (String) ""+organization.get("DistanceAfter");oDistanceAfter=Integer.parseInt(oDistanceAfters.trim());} catch (Exception e) {}}} catch (Exception e) {}
	          	    	
	          	    	
	          	    	
	          	    	/*{"Id":1,"Expresion":"������","Person":{"Distance":1,"DistanceAfter":0},"Organization":null}
	          	    	���������� ���� �� ������� ������� �������-�������� ����;
	          	    	���������� ���� �� ������� ������� �������-�����������;
	          	    	���������� ���� ����� ���������� ������� �������-�������� ����;
	          	    	���������� ���� ����� ���������� ������� �������-�����������;*/
				        try{ 
				        	   expresion=clean(expresion.toLowerCase().trim());
				        } catch (Exception e) {e.printStackTrace();}
				        
	          	    	
	          	    	if(expresion!=null&&!"".equals(expresion)&&!"null".equals(expresion)){
	          	    		int[] index = new int[4];
	          	    		index[0]=pDistance;
	          	    		index[1]=pDistanceAfter;
	          	    		index[2]=oDistance;
	          	    		index[3]=oDistanceAfter;
	          	    		if(pDistance>0||pDistanceAfter>0||oDistance>0||oDistanceAfter>0){
	          	    		lcitataMap.put(expresion, index);
	          	    		System.out.println(expresion+": "+index[0]+" "+index[1]+" "+index[2]+" "+index[3]);
	          	    		}
	          	    		
	          	    	}
		            } catch (Exception e) {e.printStackTrace();} 
		            }
			 } catch (Exception e) {
		            e.printStackTrace();
		        }
			 
			 citataIntMap=lcitataMap;
		 }
		 
		 public void loadObjects(String textFileName) throws InvalidFormatException, IOException{

			 		Map<String, String> lcitataMap=new ConcurrentHashMap<String, String>();
/*����������� � ������� ����� (0/1);
����������� � ������������ (0/1);
���������� ���� �� ������� ������� �������-�������� ����;
���������� ���� �� ������� ������� �������-�����������;
���������� ���� ����� ���������� ������� �������-�������� ����;
���������� ���� ����� ���������� ������� �������-�����������;*/
			 		
			 		lcitataMap.put("�������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("�����������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("����������", "3|1|1|1|1");
			 		lcitataMap.put("���������", "3|1|1|1|1");
			 		lcitataMap.put("����������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("������������", "3|1|1|1|1");
			 		lcitataMap.put("�����������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("����������", "3|1|1|1|1");
			 		lcitataMap.put("������������", "3|1|1|1|1");

			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("���������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");



			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("�����������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("�����������", "3|1|1|1|1");
			 		lcitataMap.put("���������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("����������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");


			 		lcitataMap.put("�����", "3|1|1|1|1");
			 		lcitataMap.put("������������", "3|1|1|1|1");
			 		lcitataMap.put("��������", "3|1|1|1|1");
			 		lcitataMap.put("���������", "3|1|1|1|1");
			 		lcitataMap.put("�������", "3|1|1|1|1");
			 		lcitataMap.put("������������", "3|1|1|1|1");
			 		lcitataMap.put("������", "3|1|1|1|1");

			 		lcitataMap.put("�����������", "3|1|1|1|1");
			 		
			 		Map<String, String> lobjectTypeMap=new ConcurrentHashMap<String, String>();
					Map<String, String> wordmap=new ConcurrentHashMap<String, String>();
					Map<String, String> liniarwordmap=new ConcurrentHashMap<String, String>();
					
					Map<String, String> localweakSynonimMap=new ConcurrentHashMap<String, String>();
					
					Map<String, String> localweakContextMap=new ConcurrentHashMap<String, String>();
					
					 try {
						    
						 
						   File file=new File(textFileName);
	                       
	                               FileInputStream inStream=new FileInputStream(file);
	                               InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

	                          
						 
						     JSONParser parser = new JSONParser();
//Linux
						     Object obj = parser.parse(new BufferedReader(reader));
						    		
						    //Windows		
				            //Object obj = parser.parse(new FileReader(textFileName));
				 
				            JSONObject jsonObject = (JSONObject) obj;
				 
				           
				            JSONArray companyList = (JSONArray) jsonObject.get("Company List");
				 
				            Iterator<JSONObject> iterator = companyList.iterator();
				            while (iterator.hasNext()) {
				            	
			            	    JSONObject json = iterator.next();
			            	    String ids = (String) ""+json.get("id");
					            String names = (String) json.get("name");
					            String typeid = (String) ""+json.get("typeid");
					            String categoryid = (String) ""+json.get("categoryid");
					            String synonyms = (String) json.get("synonyms");
					            
					            String wsynonyms = (String) json.get("wsynonyms");
					            String weaksearchcontext = (String) json.get("weaksearchcontext");
					            
					            
					            if(ids!=null&&typeid!=null){
					            lobjectTypeMap.put(ids, typeid);
					            //System.out.println("----"+ids+"-----"+typeid);
					            }
					            
					            
					            //System.out.println(names+"\n");
					            
					            /*------------�������� ���� 
					            if(names!=null&&!names.equals("")){
					            wordmap.put(names, ids);
			            	    String lowerStr = names.toLowerCase();
			            	    liniarwordmap.put(clean(lowerStr), ids);	
				                }
					            ----------------------------*/
					            
					            
					            
					            /* �������� ������ ��������� � ����� ��������
					            if(wsynonyms!=null){
					            	if(wsynonyms.contains("|")){
					            	String[] myData = wsynonyms.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    liniarwordmap.put(lowerStrd, ids);
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(wsynonyms!=null&&!wsynonyms.equals("")){
								            wordmap.put(wsynonyms, ids);
						            	    String lowerStrx = wsynonyms.toLowerCase();
						            	    liniarwordmap.put(lowerStrx, ids);	
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            */
					            if(weaksearchcontext!=null){
					            	if(weaksearchcontext.contains("|")){
					            	String[] myData = weaksearchcontext.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    //wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    lowerStrd=clean(lowerStrd);
					            	    if (localweakContextMap.containsKey(lowerStrd))
					            	    {
					            	    String ifd = localweakContextMap.get(lowerStrd);
					            	    String allid=ifd+"|"+ids;
					            	    localweakContextMap.put(lowerStrd, allid);
					            	    }
					            	    else
					            	    localweakContextMap.put(lowerStrd, ids);
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(weaksearchcontext!=null&&!weaksearchcontext.equals("")){
								            //wordmap.put(weaksearchcontext, ids);
						            	    String lowerStrx = weaksearchcontext.toLowerCase();
						            	    lowerStrx=clean(lowerStrx);
						            	    if (localweakContextMap.containsKey(lowerStrx)){
						            	    	    String ifd = localweakContextMap.get(lowerStrx);
						            	    	    String allid=ifd+"|"+ids;
								            	    localweakContextMap.put(lowerStrx, allid);
						            	    }
						            	    else
						            	    localweakContextMap.put(lowerStrx, ids);
						            	    
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            if(wsynonyms!=null){
					            	if(wsynonyms.contains("|")){
					            	String[] myData = wsynonyms.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    //wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    lowerStrd=clean(lowerStrd);
					            	    if (localweakSynonimMap.containsKey(lowerStrd)){
					            	    	
					            	    	    String ifd = localweakSynonimMap.get(lowerStrd);
					            	    	    String allid=ifd+"|"+ids;
					            	    	    localweakSynonimMap.put(lowerStrd, allid);
					            	    }
					            	    else
					            	    localweakSynonimMap.put(lowerStrd, ids);
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(wsynonyms!=null&&!wsynonyms.equals("")){
								            //wordmap.put(wsynonyms, ids);
						            	    String lowerStrx = wsynonyms.toLowerCase();
						            	    lowerStrx=clean(lowerStrx);
						            	    if (localweakSynonimMap.containsKey(lowerStrx)){
						            	    	
					            	    	    String ifd = localweakSynonimMap.get(lowerStrx);
					            	    	    String allid=ifd+"|"+ids;
					            	    	    localweakSynonimMap.put(lowerStrx, allid);
						            	    }
					            	    else
						            	    localweakSynonimMap.put(lowerStrx, ids);	
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            
					            
					            if(synonyms!=null){
					            	
					            if(synonyms.contains("|")){
					            	String[] myData = synonyms.split("\\|");
					            	for (String syns: myData) {
					            		syns=clean(syns);
					            	    wordmap.put(syns, ids);
					            	    String lowerStre = syns.toLowerCase();
					            	    liniarwordmap.put(lowerStre, ids);
					            	}
					            	
					            	
					            }else{ 
					            	    if(synonyms!=null&&!synonyms.equals("")){
					            	    	synonyms=clean(synonyms);	
							            wordmap.put(synonyms, ids);
					            	    String lowerStrz = synonyms.toLowerCase();
					            	    liniarwordmap.put(lowerStrz, ids);	
						                }
					            }
					            
					            }

				            }
				 
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
					
					 
//					 for (Map.Entry<String, String> entry : localweakContextMap.entrySet())
//					 {
//					     //System.out.println(entry.getKey() + "/" + entry.getValue());
//					     
//					     String  value = entry.getValue();
//					     if("��������".equals(value))
//					    		 {System.out.println(entry.getKey() + "/" + entry.getValue());}
//					     if(value.contains("|")){
//					    	 if("��������".equals(entry.getValue()))
//					    	 System.out.println(entry.getKey() + "/" + entry.getValue());
//					    	 
//					     }
//					 }
					 
					 
					 objectTypeMap=lobjectTypeMap;
					 swordmap=wordmap;
					 liniarmap=liniarwordmap;
					 weakSynonimMap=localweakSynonimMap;
					 weakContextMap=localweakContextMap;
					 citataMap=lcitataMap;
			 
		 }
			public void readMat(String args) {

				BufferedReader br = null;

				try {

					String sCurrentLine;

					br = new BufferedReader(new FileReader(args));
		            int k=1000000;
					while ((sCurrentLine = br.readLine()) != null) {
						//System.out.println(sCurrentLine);
						matwords.put(sCurrentLine, ""+k);
					    k++;
					}
					

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}

			}
		
		 public String clean(String text){
			 
			 String dirty=text;
		    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
		    	try{dirty=dirty.replaceAll("�|�","�");}catch(Exception e){	e.printStackTrace();}
		    	try{dirty=dirty.replaceAll("[\\s]{2,}", " ");}catch(Exception e){	e.printStackTrace();}
				return dirty;
		 }
}
