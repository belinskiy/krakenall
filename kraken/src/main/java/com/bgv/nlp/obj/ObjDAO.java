package com.bgv.nlp.obj;

import java.util.List;

import org.apache.lucene.morphology.LuceneMorphology;

import com.bgv.springmvc.domain.FindObject;

public interface ObjDAO {

	List<FindObject> discover(String id, String text);
	List<FindObject> discoverLiniar(String id, String text);
	LuceneMorphology getLuceneMorph();
}
