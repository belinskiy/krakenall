package com.bgv.nlp.obj;

import java.util.Map;

import org.apache.lucene.morphology.LuceneMorphology;

import opennlp.tools.namefind.NameFinderME;

public interface NameFinderModelMEDAO {
	NameFinderME getNameFinderModel();

	Map<String, String> getSwordmap();

	Map<String, String> getLiniarmap();

	LuceneMorphology getLuceneMorph();

	Map<String, String> getSynonimMap();

	Map<String, String> getContextMap();

	Map<String, String> getMatmap();

	Map<String, String> get�itataMap();

	Map<String, int[]> get�itataIntMap();

	Map<String, String> getObjectTypeMap();
}
