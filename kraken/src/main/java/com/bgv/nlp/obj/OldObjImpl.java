package com.bgv.nlp.obj;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bgv.nlp.spam.TokenizerDAO;
import com.bgv.springmvc.domain.FindObject;
//import com.bgv.springmvc.domain.SpamTextStatus;
import com.bgv.springmvc.domain.�itation;

import opennlp.tools.namefind.NameFinderME;
//import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.Span;


@Service
public class OldObjImpl implements OldObjDAO {
	
	@Autowired
	 private NameFinderModelMEDAO model;
	
	@Override
	public LuceneMorphology getLuceneMorph() {
		return model.getLuceneMorph();
	}
	

	
	public String getObjId(String text){
		String Id="-1";
		Map<String, String> wordmap=new HashMap<String, String>();
		wordmap=model.getSwordmap();
		try{
		Id=wordmap.get(text);
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return Id;
		
	}
	

	@Override
	public List<FindObject> discoverLiniar(String id, String text) {
		try{
		
			String dirty=text;
			String dirtyNoProbel=text;
	    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
	    	try{dirty=dirty.replaceAll("�|�","�");}catch(Exception e){	e.printStackTrace();}
	    	try{dirtyNoProbel=dirty.replaceAll("[\\s]{2,}", " ");}catch(Exception e){	e.printStackTrace();}
	    	
		String tokens[] = dirty.toString().split("\\p{P}?[\\| \\t\\n\\r]+");
		List<String[]> alltokens = new  ArrayList<String[]>();
		List<String[]> alltokenstrim = new  ArrayList<String[]>();
		Map<String, Integer>  wordCount=new HashMap<String, Integer>();
		int size = tokens.length;
		
		
   	 List<IndexWrapper> probelTextCount = new ArrayList<>();
   	 Matcher matcher = Pattern.compile("[\\s]{2,}").matcher(dirty);
   	 while (matcher.find()) {
   		 int start = matcher.start();
   		 int end = matcher.end();
   		 IndexWrapper p=new IndexWrapper(start, end);
  		
   		 probelTextCount.add(p);
   		
   	 }
		
		
		
		
		for(int i=0;i<size;i++){
			String[] indexsingle = new String[2];
			String[] indexbigram = new String[2];
			String[] indextrigram = new String[2];
			String[] indexfourgram = new String[2];
			String[] indexfivegram = new String[2];
			String[] indexsixgram = new String[2];
			String[] indexsevengram = new String[2];
			String[] indexeightgram = new String[2];
			String[] indexninegram = new String[2];
			
			String single=null;
			String bigram=null;
			String trigram=null;
			String fourgram=null;
			String fivegram=null;
			String sixgram=null;
			String sevengram=null;
			String eightgram=null;
			String ninegram=null;
			
			single=tokens[i];
			indexsingle[0] = single;
			indexsingle[1] = i+"|1";
			
			if(wordCount.containsKey(single)){
			
			int t=wordCount.get(single);
			
			indexsingle[1] = i+"|1|"+(t+1);
			wordCount.put(single, t+1);
			
			}
			else{
				wordCount.put(single, 1);
				indexsingle[1] = i+"|1|1";
			}
			
			
			if(i+1<size){
			bigram=tokens[i]+" "+tokens[i+1];
			indexbigram[0] = bigram;
			indexbigram[1] = i+"|2";
			    if(wordCount.containsKey(bigram)){
				
				int t=wordCount.get(bigram);
				indexbigram[1] = i+"|2|"+(t+1);
				wordCount.put(bigram, t+1);
				
				}
				else{
					wordCount.put(bigram, 1);
					indexbigram[1] = i+"|2|1";
				}
			
			}
			if(i+2<size){
			trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
			indextrigram[0] = trigram;
			indextrigram[1] = i+"|3";
			
		    if(wordCount.containsKey(trigram)){
				
			int t=wordCount.get(trigram);
			indextrigram[1] = i+"|3|"+(t+1);
			wordCount.put(trigram, t+1);
			
			}
			else{
				wordCount.put(trigram, 1);
				indextrigram[1] = i+"|3|1";
			}
			
			}
			if(i+3<size){
			fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
			indexfourgram[0] = fourgram;
			indexfourgram[1] = i+"|4";
			
		    if(wordCount.containsKey(fourgram)){
				
			int t=wordCount.get(fourgram);
			indexfourgram[1] = i+"|4|"+(t+1);
			wordCount.put(fourgram, t+1);
			
			}
			else{
				wordCount.put(fourgram, 1);
				indexfourgram[1] = i+"|4|1";
			}
		    
			}
			if(i+4<size){
				fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
				indexfivegram[0] = fivegram;
				indexfivegram[1] = i+"|5";
				
			    if(wordCount.containsKey(fivegram)){
					
					int t=wordCount.get(fivegram);
					indexfivegram[1] = i+"|5|"+(t+1);
					wordCount.put(fivegram, t+1);
					
					}
					else{
						wordCount.put(fivegram, 1);
						indexfivegram[1] = i+"|5|1";
					}
				
			}
			if(i+5<size){
				sixgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5];
				indexsixgram[0] = sixgram;
				indexsixgram[1] = i+"|6";
				
			    if(wordCount.containsKey(sixgram)){
					
					int t=wordCount.get(sixgram);
					indexsixgram[1] = i+"|6|"+(t+1);
					wordCount.put(sixgram, t+1);
					
					}
					else{
						wordCount.put(sixgram, 1);
						indexsixgram[1] = i+"|6|1";
					}
				
			}
			
			if(i+6<size){
				sevengram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6];
				indexsevengram[0] = sevengram;
				indexsevengram[1] = i+"|7";
				
			    if(wordCount.containsKey(sevengram)){
					
					int t=wordCount.get(sevengram);
					indexsevengram[1] = i+"|7|"+(t+1);
					wordCount.put(sevengram, t+1);
					
					}
					else{
						wordCount.put(sevengram, 1);
						indexsevengram[1] = i+"|7|1";
					}
				
			}
			
			if(i+7<size){
				eightgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6]+" "+tokens[i+7];
				indexeightgram[0] = eightgram;
				indexeightgram[1] = i+"|8";
				
			    if(wordCount.containsKey(eightgram)){
					
					int t=wordCount.get(eightgram);
					indexeightgram[1] = i+"|8|"+(t+1);
					wordCount.put(eightgram, t+1);
					
					}
					else{
						wordCount.put(eightgram, 1);
						indexeightgram[1] = i+"|8|1";
					}
				
			}
			
			if(i+8<size){
				ninegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6]+" "+tokens[i+7]+" "+tokens[i+8];
				indexninegram[0] = ninegram;
				indexninegram[1] = i+"|9";
				
			    if(wordCount.containsKey(ninegram)){
					
					int t=wordCount.get(ninegram);
					indexninegram[1] = i+"|9|"+(t+1);
					wordCount.put(eightgram, t+1);
					
					}
					else{
						wordCount.put(ninegram, 1);
						indexninegram[1] = i+"|9|1";
					}
				
			}
			
			
			if(single!=null)
				alltokens.add(indexsingle);
				if(bigram!=null)
					alltokens.add(indexbigram);
					if(trigram!=null)
						alltokens.add(indextrigram);
						if(fourgram!=null)
							alltokens.add(indexfourgram);
							if(fivegram!=null)
								alltokens.add(indexfivegram);
		
								if(sixgram!=null)
									alltokens.add(indexsixgram);
								
								if(sevengram!=null)
									alltokens.add(indexsevengram);
								
								if(eightgram!=null)
									alltokens.add(indexeightgram);
								
								if(ninegram!=null)
									alltokens.add(indexninegram);
							
								
								
							
					          if(i>60000){
					        	  break;
					          }	
		}
		
		//�������� ����������
		for (String[] wordtokend: alltokens) { 
			String[] index = new String[2];
			index[0]=wordtokend[0].trim();
			index[1]=wordtokend[1];
			
			alltokenstrim.add(index);
		}
		alltokens=alltokenstrim;
		
			
		List<FindObject> list=new ArrayList<FindObject>();
		Map<String, �itation> find�itation=new HashMap<String, �itation>();
		List<FindObject> findlist=new ArrayList<FindObject>();
		
		Map<String, String> liniarmap=new HashMap<String, String>();
		liniarmap=model.getLiniarmap();
		
		Map<String, String> weaksynonimMap=new HashMap<String, String>();
		weaksynonimMap=model.getSynonimMap();
		
		Map<String, String> weakcontextMap=new HashMap<String, String>();
		weakcontextMap=model.getContextMap();
		
		Map<String, String> matmap=new HashMap<String, String>();
		matmap=model.getMatmap();
		
		Map<String, String> lcitataMap=new HashMap<String, String>();
		lcitataMap=model.get�itataMap();
		
		Map<String, int[]> lcitataIntMap=new HashMap<String, int[]>();
		lcitataIntMap=model.get�itataIntMap();
		
		Map<String, String> objectType=new HashMap<String, String>();
		objectType=model.getObjectTypeMap();
		
		try{
			int k=0;
			for (String[] wordtoken: alltokens) { 
			  
				try{	
					String lowerStr = wordtoken[0].toLowerCase();
					
					
					try{
					//������ lcitataMap �� ������� ��� ��������������
					int[] objectcitata=lcitataIntMap.get(lowerStr);
					//lcitataIntMap
					
					
					if(objectcitata!=null){
						�itation citation=new �itation();
					
					// ��� ����� N
						citation.setName(wordtoken[0]);
					// ��� ������ ������� ��������� 3672 �� - �� 1 ������
					// ����� ��������� �� N ������ � ������� T:N
						citation.setProperties(objectcitata);
						
					int b=getPisitionB(dirty, wordtoken);
					
					
					int d=wordtoken[0].length()+b;
					citation.setStrpos(""+b);
					citation.setStrend(""+d);
					
					if(b>-1){
						
						String[] myData = wordtoken[1].split("\\|");
						String tokenleght = myData[1];
						String wordPosition = myData[0];
						//���� tokenleght > 1 ������� ���������� � ������
						
						find�itation.put(wordPosition, citation);
						
					}
					}
					}catch(Exception e){}
					
					
					String objectmat=matmap.get(lowerStr);
					
					if(objectmat!=null){
					FindObject obj=new FindObject();
					obj.setId(objectmat);
					obj.setName(wordtoken[0]);
					int b=getPisitionB(dirty, wordtoken);
					int d=wordtoken[0].length()+b;
					obj.setStrpos(""+b);
					obj.setStrend(""+d);
					obj.setIsmat("true");
					if(b>-1)
					list.add(obj);
					}
					
					
					//������ �������� �������� �� ������� 
					String objectid=liniarmap.get(lowerStr);
					
					if(objectid!=null){
					FindObject obj=new FindObject();
					obj.setWordtoken(wordtoken[1]);
					obj.setId(objectid);
					obj.setIsmat("false");
					// ��� ����� N
					obj.setName(wordtoken[0]);
					// ��� ������ ������� ��������� 3672 �� - �� 1 ������
					// ����� ��������� �� N ������ � ������� T:N
					
					//int b=getPisitionB(dirty, wordtoken);
					 int[] beginend=new int[2]; 
					 beginend=getPisitionC(dirtyNoProbel, wordtoken, probelTextCount);
					int b=beginend[0];
					int d=beginend[1];
				
					obj.setStrpos(""+b);
					obj.setStrend(""+d);
					if(b>-1)
					list.add(obj);
					else if(b==-1){
						b=0;
						d=wordtoken[0].length();
						obj.setStrpos(""+b);
						obj.setStrend(""+d);
						
						list.add(obj);	
					}
					}
					
					//������ ������ ��������� �������� �� ������� 
					
					String weaksynonim=weaksynonimMap.get(lowerStr);
					//String weakcontext=weakcontextMap.get(lowerStr);
					
					
					if(weaksynonim!=null){
					FindObject weakobj=new FindObject();
					 //System.out.println("weaksynonim="+lowerStr+" weaksynonimID="+weaksynonim);
					
					//���������� weaksynonim - ������ ��������������� ID ������ ���������
					//getMyweekID(String weaksynonimText, String weaksynonim, Map<String, String> weakcontextMap, List<String> alltokens, String allText)
					String weakobjectid=getMyweekID(wordtoken, weaksynonim, lowerStr, weakcontextMap, alltokens, dirty);
					//System.out.println("lowerStr="+lowerStr+" objectid="+weakobjectid);
					if(weakobjectid!=null){		
							
					weakobj.setId(weakobjectid);
					weakobj.setName(wordtoken[0]);
					weakobj.setWordtoken(wordtoken[1]);
					weakobj.setIsmat("false");
					//System.out.println("Begin weaksynonim*_"+wordtoken[0]+"_*get my position_"+wordtoken[1]);
					//int bweak=getPisitionB(dirty, wordtoken);
					//int dweak=wordtoken[0].length()+bweak;

					int[] beginend=new int[2]; 
					beginend=getPisitionC(dirtyNoProbel, wordtoken, probelTextCount);
					int bweak=beginend[0];
					int dweak=beginend[1];
					
					weakobj.setStrpos(""+bweak);
					weakobj.setStrend(""+dweak);

					if(bweak>-1)
					list.add(weakobj);
					else if(bweak==-1){
						bweak=0;
						dweak=wordtoken[0].length();
						weakobj.setStrpos(""+bweak);
						weakobj.setStrend(""+dweak);
						list.add(weakobj);	
					}
					}
					
					
					}
					
				}catch(Exception e){
						e.printStackTrace();
						
					}
				  k++;
		          if(k>60000){
		        	  break;
		          }	
			}
			
			try{
			if(list.size()>0){
				//����� ��������� ����� ��������� � ��������
				
				if(lcitataMap.size()>0){
					//- � ����� ����� N ������ I ���������� ���� ���������

					
					
					for(FindObject l:list){
					try{	
						String ids = l.getId();
					
						String type = objectType.get(ids);
						if(type!=null)
						if(type.equals("2")||type.equals("3")){
						String lwordtoken=l.getWordtoken();
						String TN=lwordtoken;
						String[] objectidData = TN.split("\\|");
						String T=objectidData[0];
						String I=objectidData[1];
						int i=Integer.parseInt(I);
						//t - ���������
						int beginObject=Integer.parseInt(T);
						//i - �������� ������� 
						int endObject=beginObject+i;
						//������ �������� ������ �� ��������� �����������
						//������ �������� ����� �� ������ �����������
						boolean found=false;	
						int limout=10;
						int limin=0;
						for(int z=beginObject-1;z>0;z--){
							limout--;
							limin++;
							if(limout<=0)break;
							
							if(find�itation.containsKey(""+z)){
								�itation citation=new �itation();
								citation=find�itation.get(""+z);
								int[] index=citation.getProperties();
								
		          	    		int pDistance=index[0];
		          	    		int oDistance=index[2];
		          	    		
		          	    		//Id	Name
		          	    		//1	����������
		          	    		//2	�����������
		          	    		//3	������� ����
		          	    		//4	������
		          	    		
		          	    		if(pDistance>0&&type.equals("3")){
		          	    		if(pDistance>=limin){l.setCitation(citation); found=true; break;} 
		          	    		}
		          	    			
		          	    		if(oDistance>0&&type.equals("2")){	
		          	    		if(oDistance>=limin){l.setCitation(citation); found=true; break;}
		          	    		}

							}
						}
						
						if(!found){
						int limoutk=10;
						int limink=0;
						for(int a=endObject;a<size;a++){
							limoutk--;
							limink++;
							if(limoutk<=0)break;
							
							if(find�itation.containsKey(""+a)){
								�itation citation=new �itation();
								citation=find�itation.get(""+a);
								int[] index=citation.getProperties();
								
		          	    		
		          	    		int pDistanceAfter=index[1];
		          	    		
		          	    		int oDistanceAfter=index[3];
		          	    		if(pDistanceAfter>0&&type.equals("3")){
			          	    		if(pDistanceAfter>=limink){l.setCitation(citation); break;} 
			          	    		}
			          	    			
			          	    		if(oDistanceAfter>0&&type.equals("2")){	
			          	    		if(oDistanceAfter>=limink){l.setCitation(citation); break;}
			          	    		}
								
								
							}
						}
						}
						
						
					}
					}catch(Exception e){
						e.printStackTrace();
					}
						findlist.add(l);
						
					}
					
				}else{
					findlist=list;	
				}
				
				//create new List;
				
			}
			}catch(Exception e){
				e.printStackTrace();
			}
		
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return findlist;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
	}
	


	private String getMyweekID(String[] weaksynonimText, String weaksynonim,String weaksynonimlowerStr, Map<String, String> weakcontextMap, List<String[]> alltokens, String allText) {
		
		
		//����� ���� ���� ID � ����� ���� ��������� -- "|"
		Map<String, String> weaksynmap=new HashMap<String, String>();
		if(weaksynonim.contains("|")){
			String[] myData = weaksynonim.split("\\|");
        	for (String syns: myData) {
        		weaksynmap.put(syns, "1");
        	}
			
		}else{
			weaksynmap.put(weaksynonim, "1");
			
		}
		
		Map<String, String> contextMap=new HashMap<String, String>();
		
		contextMap=weakcontextMap;
		String objectid=null;
		int k=0;
		for (String[] wordtoken: alltokens) { 
			
				try{	
					 String lowerStr = wordtoken[0].toLowerCase().trim();
					//System.out.println("token="+lowerStr);
					
					//������ ��������� �������� �� ������� 
					 //����� �� ������ ��������� �� ������ ��������� - ����� ��� � ��������� �� ���� 
					 // if(!weaksynonimlowerStr.equals(lowerStr))
						 //System.out.println("weaksynonimlowerStr="+weaksynonimlowerStr+" lowerStr="+lowerStr); 
					 
					 if(!weaksynonimlowerStr.equals(lowerStr))
					 objectid=contextMap.get(lowerStr);
					 
					 if(objectid==null){
						 
						 //return null;
						 
					 }else{
					//	 System.out.println(" Context_objectid="+objectid);
					 //����� ���� ���� ID � ����� ���� ��������� -- "|"
					 if(objectid.contains("|")){
							 String[] objectidData = objectid.split("\\|");
					        	for (String key: objectidData) {
					        		if (weaksynmap.containsKey(key))
					        		{
					        		boolean dd=calcWordsDistanceB(weaksynonimText, wordtoken[0], allText);
					        		if(dd)
					        		return key;
					        			}
					        		
					        	}
					 }
					 else{
						 if (weaksynmap.containsKey(objectid))
			        			{
							 
							     boolean dd=calcWordsDistanceB(weaksynonimText, wordtoken[0], allText);
							     if(dd)
							     return objectid;
							     }
					
					 //���������� ���� �� ���������
					
					 }
					 }
					 
				}catch(Exception e){
					e.printStackTrace();
					return null;
				}
				
				 k++;
		          if(k>60000){
		        	  break;
		          }	
		}
		
		
		
		return null;
	}




	
	
    public List<IndexWrapper> findIndexesForKeyword(String searchString, String keyword) {
        String regex = "\\b"+keyword+"\\b";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(searchString);
 
        List<IndexWrapper> wrappers = new ArrayList<IndexWrapper>();
 
        while(matcher.find() == true){
            int end = matcher.end();
            int start = matcher.start();
            IndexWrapper wrapper = new IndexWrapper(start, end);
            wrappers.add(wrapper);
        }
        return wrappers;
    }
    
	int[] getPisitionC(String Str, String[] wordtoken, List<IndexWrapper>  probelTextCount){
		
		 int[] beginend=new int[2]; 
		 beginend[0]=-1000;
		 beginend[1]=-1000;
		//text:T|N - � ����� ����� N ������ I ���������� ���� ���������
		//����� ����� N + ��� ����� ���������� �������� 
		//���� ��� 0 �� ������ �����
		//int myposition=-1000;  
		try{
		String text=wordtoken[0];
		  String TN=wordtoken[1];
		 
		  
		  
		String[] objectidData = TN.split("\\|");
		 
		 String T=objectidData[0];
		 String I=objectidData[2];

		 if(I.equals("1")){
			    List<IndexWrapper> list = new ArrayList<>();
			    list =  findIndexesForKeyword(Str,text);
			    try{
			    if(list.size()>0){
			    	int lbeforeposstart = list.get(0).getStart();
			    	int lbeforeposend = list.get(0).getEnd();
			    	 	
			         beginend=getTruePos(lbeforeposstart, lbeforeposend,  probelTextCount);
			   //myposition=Str.indexOf( text );
				return beginend;}
			 	}catch(Exception e){e.printStackTrace();}
		}
		else{
			//int t=Integer.parseInt(T);
			int i=Integer.parseInt(I);
			if(i>0) i=i-1;
			List<IndexWrapper> list = new ArrayList<>();
		    list =  findIndexesForKeyword(Str,text);
		    try{
			    if(list.size()>0){
			    
			    	//myposition=findIndexesForKeyword(Str,text).get(i).getStart();
			    	int lbeforeposstart = list.get(i).getStart();
			    	int lbeforeposend = list.get(i).getEnd();
			    		
			         beginend=getTruePos(lbeforeposstart, lbeforeposend,  probelTextCount);
			
			         return beginend;	
			}
			    }catch(Exception e){e.printStackTrace();}
		}
		
	}catch(Exception e){e.printStackTrace(); return beginend;}
		return beginend;
	}
	
	public  int[] getTruePos(int beforeposstart, int beforeposend, List<IndexWrapper>  probelTextCount){
		 int[] beginend=new int[2]; 
		 beginend[0]=0;
		 beginend[1]=0;
		 try{ 
							//int intermleght=0;
					   	   // int skolikostart=0;
					   			 for (IndexWrapper entry : probelTextCount)
					   			 {
					   				 int start=entry.getStart();
					   				 int end=entry.getEnd();
					   				 if(beforeposstart>start){			   			     
					   			    	 int deleted=end-start-1;
						   			    	 if(deleted>0){
						   			    	 beforeposstart=beforeposstart+deleted;   			    	 
						   			    	 }
					   			     }
					   			     if(start<beforeposend){
					   			    	 int deleted=end-start-1;
						   			    	 if(deleted>0){
						   			    		 beforeposend=beforeposend+deleted; 
						   			    	 }
					   			     }
					   			    
//					   			     if(beforeposstart>start){
//					   			    	 int deleted=end-start-1;
//					   			    	 //System.out.println("deleted="+deleted);
//					   			    	 skolikostart=skolikostart+deleted;
//					   			     }
//					   			     //������� ������ �������
//					   			     if(start>beforeposstart&&start<beforeposend){
//					   			    	 intermleght=end-start-1;
//					   			    	 //System.out.println("intermleght="+intermleght); 
//					   			     }
					   			    
					   			 } 
					   	 
					   	
					   	 
					   			 int begin=beforeposstart;
					   			 int bend=beforeposend;
					   		   	 //int totalleght=(beforeposend-beforeposstart);
					   		   	 //int z=begin+totalleght;
					   	 beginend[0]=begin;
					   	 beginend[1]=bend;
						 //beginend[1]=z;
		}catch(Exception e){e.printStackTrace();}
	   //	System.out.println("������������ �������="+begin+" ����� ����� �������="+totalleght+" ���������="+z);
		return beginend;
	   	 
		}
    
	int getPisitionB(String Str, String[] wordtoken){
		
		
		//text:T|N - � ����� ����� N ������ I ���������� ���� ���������
		//����� ����� N + ��� ����� ���������� �������� 
		//���� ��� 0 �� ������ �����
		int myposition=-1000;  
		try{
		String text=wordtoken[0];
		  String TN=wordtoken[1];
		 
		  
		  
		String[] objectidData = TN.split("\\|");
		 
		 String T=objectidData[0];
		 String I=objectidData[2];

		 if(I.equals("1")){
			    List<IndexWrapper> list = new ArrayList<>();
			    list =  findIndexesForKeyword(Str,text);
			    try{
			    if(list.size()>0){
			    myposition=list.get(0).getStart();
			   
			   //myposition=Str.indexOf( text );
				return myposition;}
			 	}catch(Exception e){e.printStackTrace();}
		}
		else{
			//int t=Integer.parseInt(T);
			int i=Integer.parseInt(I);
			if(i>0) i=i-1;
			List<IndexWrapper> list = new ArrayList<>();
		    list =  findIndexesForKeyword(Str,text);
		    try{
			    if(list.size()>0){
			    
			    	myposition=findIndexesForKeyword(Str,text).get(i).getStart();
			
			
			
//			int last=0;
//			int lastgo=0;
//			//T ��� ����� ����� � ������ == 22 � i ��� ��������� 2-� ��������
//
//			//int k=0; //������� ��������� � ������
//			
//			//k=wordCountin.get(text);
//			
//			for(int k=1;k<=i;k++){	
//				
//				//�������� ������� ���� ��������� �����
//				last=Str.indexOf( text, lastgo );
//				lastgo=last+text.length();
//			
//				
//				
//				if(k>20)break;
//			}
			
			return myposition;	}
			    }catch(Exception e){}
		}
		
	}catch(Exception e){e.printStackTrace(); return myposition;}
		return myposition;
	}
	
	
	private boolean calcWordsDistanceB(String[] weaksynonimText, String synonimContext,String text) {
		boolean dist = false;
		try{
		
		int firstW=getPisitionB(text, weaksynonimText);
		List<Integer> list = new ArrayList<>();
		list=getWordPisitionB(text, synonimContext);
		int k=0;
		for(Integer secondW:list){
		if(firstW>-1 && secondW >-1)
		{
					
			//100 ��������
			//firstW 10+����� ����� - secondW 100= -90 < 30
			//secondW 100+����� ����� - firstW 10=  50  < 30
			
			if(secondW>firstW)
			{
				int total=secondW-(firstW+weaksynonimText[0].length());
				if(total<100)
					dist = true;
				
			}
			else{
				
				int total=firstW-(secondW+synonimContext.length());
				if(total<100)
					dist = true;
			}

		}
		k++;
		if(k>30){
      	  break;
        }			
		}
		}catch(Exception e){e.printStackTrace(); }
		return dist;
	}
	
	
	private List<Integer> getWordPisitionB(String text, String synonimContext) {
		List<Integer> list = new ArrayList<>();
		try{
		
        String regex = "\\b"+synonimContext+"\\b";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        int k=0;
        while(matcher.find() == true){
            
            int start = matcher.start();
           
            int myposition=start+synonimContext.length();
	  		list.add(myposition);
	  		
	  		 k++;
	          if(k>10){
	        	  break;
	          }
        }

		
		}catch(Exception e){e.printStackTrace(); }
		  
		  
		return list;
	}
}
