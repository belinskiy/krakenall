package com.bgv.dao;

import java.sql.SQLException;

public interface IObjectsDAO {

	boolean update() throws SQLException, ClassNotFoundException;

	boolean updateCitation() throws SQLException, ClassNotFoundException;

}
