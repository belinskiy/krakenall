package com.bgv.dao;

import java.util.List;

import com.bgv.entity.Addmat;

public interface IAddmatDAO {
	    
	
	    List<Addmat> getAllAddmat();
	  
	    boolean addAddmat(Addmat addmat);

}
