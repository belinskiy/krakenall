package com.bgv.dao;

import java.sql.DriverManager;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.bgv.springmvc.domain.Config;
import com.bgv.springmvc.domain.Objects;
@Service
public class ObjectsDAO implements IObjectsDAO{
	//static final String conns="jdbc:sqlserver://172.16.252.6:1433;user=sa;password=saSA123$;database=SocialMediaManagement;encrypt=true;trustServerCertificate=true;loginTimeout=30";
	//static final String conns="jdbc:sqlserver://10.89.72.6:1433;user=sa;password=saSA123$;database=SocialMediaManagement;encrypt=true;trustServerCertificate=true;loginTimeout=30";
	static final String conns="jdbc:sqlserver://10.208.105.27:1433;user=sa;password=saSA123$;database=SocialMediaManagement;encrypt=true;trustServerCertificate=true;loginTimeout=30";
	
	
	@Override
	public boolean update() throws SQLException, ClassNotFoundException {
		boolean sts=false;
		try{Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");	
		//Connection conn = DriverManager.getConnection("jdbc:sqlserver://10.89.72.6:1433;user=sa;password=saSA123$;database=SocialMediaManagement;encrypt=true;trustServerCertificate=true;loginTimeout=30");
	    Connection conn = DriverManager.getConnection(conns);
		System.out.println("MS Connect to 172.16.252.6:1433");
		Statement sta = conn.createStatement();
		sta.setFetchSize(10);
		String Sql = "Select d.Id, d.name, d.strongSynonyms,  d.TypeId, d.categoryId, d.weakSynonyms, d.weakSearchContext from [Objects] d with (NOLOCK) where d.IsDeleted = 0";
		//String Sql = "Select d.Id, d.name, d.strongSynonyms,  d.categoryId, d.weakSynonyms, d.weakSearchContext from [Objects] d with (NOLOCK) where d.IsDeleted = 0 AND d.CreatedAt BETWEEN '2008-08-14' AND '2017-05-01'";

		ResultSet rs = sta.executeQuery(Sql);
		 List<Objects> list = new ArrayList<Objects>();
		  while (rs.next()) {
	    	   int id = rs.getInt("Id");
	    	   int categoryId = rs.getInt("CategoryId");
	    	   int typeId = rs.getInt("TypeId");
	    	   
	           String name = rs.getString("Name");
	           String strongSynonyms = rs.getString("StrongSynonyms");
	           String weakSynonyms = rs.getString("WeakSynonyms");
	           String weakSearchContext = rs.getString("WeakSearchContext");
	           //System.out.println("MS Load objects..."+name);
	           Objects dept = new Objects();
	           dept.setCategoryId(categoryId);
	           dept.setTypeId(typeId);
	           dept.setId(id);
	           dept.setName(name);
	           dept.setStrongSynonyms(strongSynonyms);
	           dept.setWeakSynonyms(weakSynonyms);
	           dept.setWeakSearchContext(weakSearchContext);
	          
	           //if(!map.containsKey(""+id))
	        	   list.add(dept);
	           
	           
	          sts=true;
	      }
		
		
		
	    JSONObject obj = new JSONObject();
			obj.put("Name", "NLP");
			obj.put("Author", "BGVv1");
			
			JSONArray company = new JSONArray();
			
			
	    List<Objects> depts = list;
	    System.out.println("MS List objects...");
	    for (Objects dept : depts) {
	        
	        JSONObject objs = new JSONObject();
	        objs.put("id", dept.getId());
	        objs.put("name", dept.getName());
	        objs.put("synonyms", dept.getStrongSynonyms());
	        objs.put("wsynonyms", dept.getWeakSynonyms());
	        objs.put("categoryid", dept.getCategoryId());
	        objs.put("typeid", dept.getTypeId());
	        objs.put("weaksearchcontext", dept.getWeakSearchContext());
	        
			company.add(objs);
			
			
			
			obj.put("Company List", company);
	    }
	    

	    Config cf=new Config();
	    String root = cf.getRoot();
	    String fname = cf.getObjectstext();
		
			String d=root+fname;
			try (FileWriter file = new FileWriter(d)) {
				file.write(obj.toJSONString());
				System.out.println("MS Successfully Copied JSON Object to File..."+d);
				//System.out.println("\nJSON Object: " + obj);
			} catch (IOException e) {
				
				e.printStackTrace();
				return false;
			}
	} catch (Exception e) {
		
		e.printStackTrace();
		
		return false;
	}
			return sts;
			
			
	    
	}
	
	
	
	@Override
	public boolean updateCitation() throws SQLException, ClassNotFoundException {
		boolean sts=false;
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");	
			Connection conn = DriverManager.getConnection(conns);
			System.out.println("test");
			Statement sta = conn.createStatement();
			sta.setFetchSize(10);
			String Sql = "Select d.Id, d.Text, d.Hash from [CitationAttributes] d with (NOLOCK)";
			ResultSet rs = sta.executeQuery(Sql);
			Map<String,String> objects = new ConcurrentHashMap<String,String>();
			  while (rs.next()) {
		    	   int id = rs.getInt("Id");
		    	   String value = rs.getString("Text");
		           String hash = rs.getString("Hash");
		           objects.put(hash, value);
		      }
			
			
			
		    JSONObject obj = new JSONObject();
				obj.put("Name", "NLP");
				obj.put("Author", "BGVv1");
				
				JSONArray company = new JSONArray();
				
				
				for (Map.Entry<String, String> entry : objects.entrySet())
				{
					String value=entry.getValue();
					 String hash =entry.getKey();
		        System.out.println("hash " + hash);
		        System.out.println("text " + value);
		        
		        if(hash!=null&&!hash.equals("")&&value!=null&&!value.equals("")){
		        JSONObject objs = new JSONObject();
		        
		        objs.put("id", hash);
		        objs.put("name", value);

				company.add(objs);
				obj.put("Company List", company);
		        }
				
				
		    }
	    

	    Config cf=new Config();
	    String root = cf.getRoot();
	    String fname = cf.getCitationtext();
		
			String d=root+fname;
			try (FileWriter file = new FileWriter(d)) {
				file.write(obj.toJSONString());
				System.out.println("MS Successfully Copied JSON Object to File..."+d);
				//System.out.println("\nJSON Object: " + obj);
			} catch (IOException e) {
				
				e.printStackTrace();
				return false;
			}
	} catch (Exception e) {
		
		e.printStackTrace();
		
		return false;
	}
			return sts;
			
			
	    
	}
	
}
