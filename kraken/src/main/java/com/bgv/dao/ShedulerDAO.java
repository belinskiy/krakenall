package com.bgv.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Sheduler;
import com.bgv.entity.Shedulerslog;


@Transactional
@Repository
public class ShedulerDAO implements IShedulerDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Sheduler> getAllSheduler() {
		
	
			String hql = "FROM Sheduler as p ORDER BY p.pid";
			return (List<Sheduler>) hibernateTemplate.find(hql);
		
	}

	@Override
	public boolean addSheduler(Sheduler person) {
		hibernateTemplate.save(person);
		return false;
	}
	
	@Override
	public boolean addShedulerLog(Shedulerslog log) {
		hibernateTemplate.save(log);
		return false;
	}

	@Override
	public void updateSheduler(Sheduler person) {
		Sheduler p = getShedulerById(person.getPid());
		p.setLastpid(person.getLastpid());
		p.setLastupdate(person.getLastupdate());
		p.setSname(person.getSname());
		p.setModelname(person.getModelname());
		p.setPrecisioncount(person.getPrecisioncount());
		hibernateTemplate.update(p);
		
	}
	
	@Override
	public Sheduler getShedulerById(int pid) {
		return hibernateTemplate.get(Sheduler.class, pid);
	}
	   
}
