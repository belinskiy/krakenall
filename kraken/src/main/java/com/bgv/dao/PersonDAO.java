package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;
import javax.transaction.Transactional;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Person;

@Transactional
@Repository
public class PersonDAO implements IPersonDAO {
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	@Override
	public Person getPersonById(int pid) {
		return hibernateTemplate.get(Person.class, pid);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Person> getAllPersons() {
		String hql = "FROM Person as p ORDER BY p.pid";
		return (List<Person>) hibernateTemplate.find(hql);
	}

	
	@Override
	public List<Person> getAllDocuments(Timestamp dateupdate) {
		
		String query = "FROM Person p where p.dateupdate < :dateupdate ";
			    return (List<Person>)  this.hibernateTemplate.findByNamedParam(query, "dateupdate", dateupdate);
			    
			    
//		String hql = "FROM Person as p ORDER BY p.pid";
//		return (List<Person>) hibernateTemplate.find(hql);
	}	
	
	
	@Override
	public boolean addPerson(Person person) {
		hibernateTemplate.save(person);
		return false;
	}
	@Override
	public void updatePerson(Person person) {
		Person p = getPersonById(person.getPid());
		p.setUsername(person.getUsername());
		p.setPassword(person.getPassword());
		p.setAge(person.getAge());
		p.setGender(person.getGender());
		p.setCity(person.getCity());
		hibernateTemplate.update(p);
	}
	@Override
	public void deletePerson(int pid) {
		hibernateTemplate.delete(getPersonById(pid));
	}
	@SuppressWarnings("unchecked")
	@Override
	public boolean personExists(String username) {
		String hql = "FROM Person as p WHERE p.username = ?";
		List<Person> persons = (List<Person>) hibernateTemplate.find(hql, username);
		return persons.size() > 0 ? true : false;
	}
	
	@Override
	public int getMaxDocuments() {

	    int maxId = 0;

	   
	    DetachedCriteria criteria = DetachedCriteria
	            .forClass(Person.class);
	    criteria.setProjection(Projections.max("pid"));
try{
	    List result = hibernateTemplate.findByCriteria(criteria);
	    maxId = (Integer) result.get(0);
}catch(Exception e){}
	    return maxId;
	}
} 