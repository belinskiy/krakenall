package com.bgv.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Addmat;
import com.bgv.entity.Sheduler;
import com.bgv.entity.Shedulerslog;


@Transactional
@Repository
public class AddmatDAO implements IAddmatDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Addmat> getAllAddmat() {
		
	
			String hql = "FROM Addmat as p ORDER BY p.pid";
			return (List<Addmat>) hibernateTemplate.find(hql);
		
	}

	@Override
	public boolean addAddmat(Addmat addmat) {
		hibernateTemplate.save(addmat);
		return false;
	}
	
	
}
