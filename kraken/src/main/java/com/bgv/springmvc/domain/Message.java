package com.bgv.springmvc.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "morfology")
public class Message {

	String word;
	String wordform;
	String normalform;
	public Message(){
		
	}
	
	public Message(String word, String wordform, String normalform) {
		this.word = word;
		this.wordform = wordform;
		this.normalform=normalform;
	}

	@XmlElement
	public String getWord() {
		return word;
	}
	
	@XmlElement
	public String getWordform() {
		return wordform;
	}
	
	@XmlElement
	public String getNormalform() {
		return normalform;
	}

}
