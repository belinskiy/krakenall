package com.bgv.springmvc.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name = "spamtext")
public class SpamText {
	    private String guid;
	    private String category;
	    private String utftext;
	   // @XmlElement
	    public String getGuid() {
			return guid;
		}
		public void setGuid(String guid) {
			this.guid = guid;
		}
		//@XmlElement
		public String getUtftext() {
			return utftext;
		}
		public void setUtftext(String utftext) {
			this.utftext = utftext;
		}
		@Override
		public String toString() {
			return "SpamText [guid=" + guid + ", utftext=" + utftext + "]";
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		
	    
	    
}
