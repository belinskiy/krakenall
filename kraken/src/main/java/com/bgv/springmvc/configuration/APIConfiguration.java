package com.bgv.springmvc.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@Configuration
@EnableWebMvc
@EnableScheduling
@ComponentScan(basePackages = "com.bgv")
public class APIConfiguration {
	
	
	  public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    	registry.addResourceHandler("/app-resources/**").addResourceLocations("/resources/");
	    }    	

}