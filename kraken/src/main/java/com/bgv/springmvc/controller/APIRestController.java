package com.bgv.springmvc.controller;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.morphology.LuceneMorphology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bgv.entity.Addmat;
import com.bgv.entity.Person;
import com.bgv.nlp.obj.ObjDAO;
import com.bgv.nlp.obj.OldObjDAO;
import com.bgv.nlp.spam.SpamDAO;
import com.bgv.service.IAddmatService;
import com.bgv.service.IPersonService;
import com.bgv.springmvc.domain.Config;
import com.bgv.springmvc.domain.FindObject;
import com.bgv.springmvc.domain.FindObjects;
import com.bgv.springmvc.domain.Message;
import com.bgv.springmvc.domain.SpamText;
import com.bgv.springmvc.domain.SpamTextStatus;

@RestController
public class APIRestController {
	 @Autowired
	 private SpamDAO spam;
	 
	 @Autowired
	 private ObjDAO objs;
	 @Autowired
	 private OldObjDAO oldobjs;
	 @Autowired
	 private IAddmatService addmatService;
	 
	    @Autowired
		private IPersonService personService;
	 
	    private String version="1.10.11.1";
	    
	    
	@RequestMapping("/")
	public String welcome() {
		return "Welcome to Kraken "+version;
	}

//	@RequestMapping("/v1/nlp/{player}")
//	public Message message(@PathVariable String player) {
//
//		Message msg = new Message(player, " " + player);
//		return msg;
//	}
	
	
	@RequestMapping("/v1/nlp/morfology/{word}")
	public Message messageWord(@PathVariable String word) {
		String wordform="none"; 
		String normalform="none"; 
		LuceneMorphology luceneMorph = objs.getLuceneMorph();
		
		try{
		
		
		List<String> wordBaseForms = luceneMorph.getMorphInfo(word);
		StringBuilder sb = new StringBuilder();
		for (String s : wordBaseForms)
		{
		    sb.append(s);
		    sb.append(",");
		}
		
		
		String ist = sb.toString();
		if(!ist.equals("")&&ist!=null){
			wordform=ist;
		}
		}catch(Exception e){}
		
		
		
		try{
			
			
			List<String> wordBaseForms = luceneMorph.getNormalForms(word);
			StringBuilder sb = new StringBuilder();
			for (String s : wordBaseForms)
			{
			    sb.append(s);
			    sb.append(" ");
			}
			
			
			String ist = sb.toString();
			if(!ist.equals("")&&ist!=null){
				normalform=ist;
			}
			}catch(Exception e){}
		
		
		Message msg = new Message(word, wordform, normalform);
		
		// RussianAnalyzer russian = new RussianAnalyzer();
		// russian.
		return msg;
	}
	
	@RequestMapping("/v1/nlp/spam/model")
	public String getModel() {
		Config d=new Config();
		return d.getModel();
		
	}
	
	@RequestMapping(value = "/v1/nlp/spam/discover/", method = RequestMethod.POST)
	
	public @ResponseBody SpamTextStatus discover(@RequestBody SpamText text) {
		
		
		  if (text == null) {
			  SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty request Body");
			  return msg;
		    }
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String utext = text.getUtftext();
		if(id!=null&&utext!=null){
		SpamTextStatus msg = spam.discover(id, utext);	
		return msg;}
		else{
			SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty text");
			  return msg;
		}
		}catch(Exception e){return null;}  
	}

	
	@RequestMapping(value = "/v1/nlp/object/discover/", method = RequestMethod.POST)
	
	public @ResponseBody FindObjects  discoverObjects(@RequestBody SpamText text) {
		
		
		 if (text == null) {
			  FindObjects result= new FindObjects();
				result.setFindObject(null);
				result.setGuid("");
				result.setMsg("");
				result.setResponse("false");
				result.setText("empty request Body");
				return result;
		    }
		  
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String utext = text.getUtftext();
		if(id!=null&&utext!=null){
		
			List<FindObject> msg = objs.discover(id, utext);	
			FindObjects result= new FindObjects();
			result.setFindObject(msg);
			result.setGuid(id);
			result.setMsg("");
			result.setResponse("true");
			result.setText(utext);
			return result;}
		else{
			FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid(id);
			result.setMsg("empty text");
			result.setResponse("false");
			result.setText("");
			return result;
		}
		}catch(Exception e){e.printStackTrace(); FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid("");
		result.setMsg("");
		result.setResponse("false");
		result.setText("empty request Body");
		return result;}  
	}
	@RequestMapping(value = "/v1/nlp/object/discover/liniar", method = RequestMethod.POST)
	
	public @ResponseBody FindObjects  discoverLinianObjects(@RequestBody SpamText text) {
		
		
		  if (text == null) {
			  FindObjects result= new FindObjects();
				result.setFindObject(null);
				result.setGuid("");
				result.setMsg("");
				result.setResponse("false");
				result.setText("empty request Body");
				return result;
		    }
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String utext = text.getUtftext();
		if(id!=null&&utext!=null){
		
			//List<FindObject> msg = objs.discoverLiniar(id, utext);	
			List<FindObject> msg = oldobjs.discoverLiniar(id, utext);
			FindObjects result= new FindObjects();
			result.setFindObject(msg);
			result.setGuid(id);
			result.setMsg("");
			result.setResponse("true");
			result.setText(utext);
		
		return result;}
		else{
			FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid(id);
			result.setMsg("empty text");
			result.setResponse("false");
			result.setText("");
			return result;
		}
		}catch(Exception e){e.printStackTrace(); 
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid("");
		result.setMsg("");
		result.setResponse("false");
		result.setText("empty request Body");
		return result;}  
	}
	
	
	
@RequestMapping(value = "/v1/nlp/spam/add/", method = RequestMethod.POST)
	
	public @ResponseBody SpamTextStatus add(@RequestBody SpamText text) {
		
		
		  if (text == null) {
			  SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty request Body");
			  return msg;
		    }
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String category = text.getCategory();
		String utext = text.getUtftext();
		// System.out.println("utext="+utext);
		if(id!=null&&utext!=null&&category!=null){
			Person person = new Person();
			person.setAge(12);
			person.setCity(utext);
			person.setGender(category);
			person.setPassword("d");
			 Calendar calendar = Calendar.getInstance();
			    java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
			    person.setDateupdate(ourJavaTimestampObject);
			person.setUsername(id);
			personService.addPerson(person);
			
		
		SpamTextStatus msg = new SpamTextStatus("", "", "", "true", ""); 
		return msg;}
		else{
			SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty text");
			  return msg;
		}
		}catch(Exception e){e.printStackTrace(); return null;}  
	}	

		@RequestMapping("/v1/nlp/object/addmat/{word}")
		public String matWord(@PathVariable String word) {
		try{	if(word!=null&&!word.equals("")){
				Calendar calendar = Calendar.getInstance();
			    java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
			
			Addmat	addmat = new Addmat();
			addmat.setDateupdate(ourJavaTimestampObject);
			addmat.setMatword(word);
			addmatService.addAddmat(addmat);}
			return "ok";
			
		}catch(Exception e){return "ok";}  
		}
	
	
}
