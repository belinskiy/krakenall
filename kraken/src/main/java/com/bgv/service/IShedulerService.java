package com.bgv.service;

import java.util.List;

import com.bgv.entity.Sheduler;

public interface IShedulerService {
	List<Sheduler> getAllSheduler();

	void updateSheduler(Sheduler person);

	void updateShedulerLog(Sheduler toupdate);

}
