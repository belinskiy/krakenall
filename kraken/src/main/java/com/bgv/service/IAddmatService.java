package com.bgv.service;

import java.util.List;

import com.bgv.entity.Addmat;

public interface IAddmatService {
    List<Addmat> getAllAddmat();
	  
    boolean addAddmat(Addmat addmat);

}
