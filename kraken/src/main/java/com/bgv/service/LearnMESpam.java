package com.bgv.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Service;

import com.bgv.nlp.spam.LearningSample;
import com.bgv.nlp.spam.PlainTextEventStream;
import com.bgv.springmvc.domain.Config;

import opennlp.maxent.GIS;
import opennlp.maxent.GISModel;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;

@Service
public class LearnMESpam implements ILearnMESpam{
	   static int iterations =1000;
	   static int cutoff =1;
	   static boolean smoothing=true;
	   static boolean addplaintext=true;
	   
	   
	@Override
	public void learn( Collection<LearningSample> el, String modelname) {

		Config conf=new Config();
	    Collection<LearningSample> eList=new ArrayList<LearningSample>();
		   

		   
	    eList=el;
	    
	    
		PlainTextEventStream stream = new PlainTextEventStream(eList);
		/*
		iterations � ����� ���������� �������� �������� ��������;
		cutoff � ����������� ���������� ������������ ������������������ ��������, ��� ������� ������� ����������� � ��������. �������� � ������������ ������ cutoff, ������������;
		smoothing� ������� ���� ���������� �����������. 
		*/
					GISModel model;
					try {
						model = GIS.trainModel(stream, iterations, cutoff, smoothing, true);
					 File outputFile = new File(conf.getRoot()+modelname);
					    GISModelWriter writer = new SuffixSensitiveGISModelWriter(model, outputFile);
					    writer.persist();
					    
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					    
					   
		
	}

}
