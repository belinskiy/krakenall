package com.bgv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IAddmatDAO;
import com.bgv.dao.IShedulerDAO;
import com.bgv.entity.Addmat;
import com.bgv.entity.Sheduler;
import com.bgv.entity.Shedulerslog;

@Service
public class AddmatService implements IAddmatService{
	@Autowired
	private IAddmatDAO addmatDAO;
	@Override
	public List<Addmat> getAllAddmat() {
		
		return addmatDAO.getAllAddmat();
	}

	@Override
	public boolean addAddmat(Addmat addmat) {
		
		return addmatDAO.addAddmat(addmat);
	}
	
}
