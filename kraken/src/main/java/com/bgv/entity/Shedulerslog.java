package com.bgv.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the shedulerslog database table.
 * 
 */
@Entity
@Table(name= "shedulerslog")
@NamedQuery(name="shedulerslog.findAll", query="SELECT s FROM Shedulerslog s")
public class Shedulerslog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int pid;

	private int lastpid;

	private Timestamp lastupdate;

	private String modelname;

private BigDecimal precisioncount;
	
	public BigDecimal getPrecisioncount() {
		return this.precisioncount;
	}

	public void setPrecisioncount(BigDecimal precisioncount) {
		this.precisioncount = precisioncount;
	}

	private String sname;

	public Shedulerslog() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getLastpid() {
		return this.lastpid;
	}

	public void setLastpid(int lastpid) {
		this.lastpid = lastpid;
	}

	public Timestamp getLastupdate() {
		return this.lastupdate;
	}

	public void setLastupdate(Timestamp lastupdate) {
		this.lastupdate = lastupdate;
	}

	public String getModelname() {
		return this.modelname;
	}

	public void setModelname(String modelname) {
		this.modelname = modelname;
	}

	

	public String getSname() {
		return this.sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

}