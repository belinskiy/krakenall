package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the addmat database table.
 * 
 */
@Entity
@Table(name= "addmat")
@NamedQuery(name="Addmat.findAll", query="SELECT a FROM Addmat a")
public class Addmat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pid;

	private Timestamp dateupdate;

	private String matword;

	public Addmat() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public Timestamp getDateupdate() {
		return this.dateupdate;
	}

	public void setDateupdate(Timestamp dateupdate) {
		this.dateupdate = dateupdate;
	}

	public String getMatword() {
		return this.matword;
	}

	public void setMatword(String matword) {
		this.matword = matword;
	}

}