package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilityobjects database table.
 * 
 */
@Entity
@Table(name="probabilityobjects")
@NamedQuery(name="Probabilityobject.findAll", query="SELECT p FROM Probabilityobject p")
public class Probabilityobject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pid")
	private int pid;
	private String exguid;
	private String guid;

	private int objecta;

	private int objectb;

	private int objectc;

	private int objectd;

	private int objecte;

	private int objectf;

	private int objectg;

	private int objecti;

	private int objectj;

	private int objectk;

	public Probabilityobject() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getObjecta() {
		return this.objecta;
	}

	public void setObjecta(int objecta) {
		this.objecta = objecta;
	}

	public int getObjectb() {
		return this.objectb;
	}

	public void setObjectb(int objectb) {
		this.objectb = objectb;
	}

	public int getObjectc() {
		return this.objectc;
	}

	public void setObjectc(int objectc) {
		this.objectc = objectc;
	}

	public int getObjectd() {
		return this.objectd;
	}

	public void setObjectd(int objectd) {
		this.objectd = objectd;
	}

	public int getObjecte() {
		return this.objecte;
	}

	public void setObjecte(int objecte) {
		this.objecte = objecte;
	}

	public int getObjectf() {
		return this.objectf;
	}

	public void setObjectf(int objectf) {
		this.objectf = objectf;
	}

	public int getObjectg() {
		return this.objectg;
	}

	public void setObjectg(int objectg) {
		this.objectg = objectg;
	}

	public int getObjecti() {
		return this.objecti;
	}

	public void setObjecti(int objecti) {
		this.objecti = objecti;
	}

	public int getObjectj() {
		return this.objectj;
	}

	public void setObjectj(int objectj) {
		this.objectj = objectj;
	}

	public int getObjectk() {
		return this.objectk;
	}

	public void setObjectk(int objectk) {
		this.objectk = objectk;
	}
	public String getExguid() {
		return this.exguid;
	}

	public void setExguid(String exguid) {
		this.exguid = exguid;
	}
}