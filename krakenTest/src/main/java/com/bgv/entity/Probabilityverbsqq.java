package com.bgv.entity;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilityverbsqq database table.
 * 
 */
@Entity
@Table(name="probabilityverbsqq")
@NamedQuery(name="Probabilityverbsqq.findAll", query="SELECT p FROM Probabilityverbsqq p")
public class Probabilityverbsqq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String categoryid;

	private String nounname;

	private int totalvalue;

	private String typeid;

	private String verbname;

	public Probabilityverbsqq() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryid() {
		return this.categoryid;
	}

	public void setCategoryid(String categoryid) {
		this.categoryid = categoryid;
	}

	public String getNounname() {
		return this.nounname;
	}

	public void setNounname(String nounname) {
		this.nounname = nounname;
	}

	public int getTotalvalue() {
		return this.totalvalue;
	}

	public void setTotalvalue(int totalvalue) {
		this.totalvalue = totalvalue;
	}

	public String getTypeid() {
		return this.typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getVerbname() {
		return this.verbname;
	}

	public void setVerbname(String verbname) {
		this.verbname = verbname;
	}

}