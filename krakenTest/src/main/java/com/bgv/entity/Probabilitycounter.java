package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilitycounter database table.
 * 
 */
@Entity
@Table(name= "probabilitycounter")
@NamedQuery(name="Probabilitycounter.findAll", query="SELECT p FROM Probabilitycounter p")
public class Probabilitycounter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pid;

	private String exguid;

	private String thash;

	public Probabilitycounter() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getExguid() {
		return this.exguid;
	}

	public void setExguid(String exguid) {
		this.exguid = exguid;
	}

	public String getThash() {
		return this.thash;
	}

	public void setThash(String thash) {
		this.thash = thash;
	}

}