package com.bgv.entity;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilitybwa database table.
 * 
 */
@Entity
@Table(name="probabilitybwa")
@NamedQuery(name="Probabilitybwa.findAll", query="SELECT p FROM Probabilitybwa p")
public class Probabilitybwa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String beforeword;

	private int totalvalue;

	private String propword;
	
	private String afterword;

	public Probabilitybwa() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public int getTotalvalue() {
		return this.totalvalue;
	}

	public void setTotalvalue(int totalvalue) {
		this.totalvalue = totalvalue;
	}

	public String getBeforeword() {
		return beforeword;
	}

	public void setBeforeword(String beforeword) {
		this.beforeword = beforeword;
	}

	public String getPropword() {
		return propword;
	}

	public void setPropword(String propword) {
		this.propword = propword;
	}

	public String getAfterword() {
		return afterword;
	}

	public void setAfterword(String afterword) {
		this.afterword = afterword;
	}

	
}