package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the positive database table.
 * 
 */
@Entity
@Table(name="positive")
@NamedQuery(name="Positive.findAll", query="SELECT p FROM Positive p")
public class Positive implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String nword;

	private int total;

	private String word;

	public Positive() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNword() {
		return this.nword;
	}

	public void setNword(String nword) {
		this.nword = nword;
	}

	public int getTotal() {
		return this.total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getWord() {
		return this.word;
	}

	public void setWord(String word) {
		this.word = word;
	}

}