package com.bgv.entity;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilityverbs database table.
 * 
 */
@Entity
@Table(name="probabilityverbs")
@NamedQuery(name="Probabilityverb.findAll", query="SELECT p FROM Probabilityverb p")
public class Probabilityverb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String nounname;

	private int totalvalue;

	private String verbname;

	public Probabilityverb() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNounname() {
		return this.nounname;
	}

	public void setNounname(String nounname) {
		this.nounname = nounname;
	}

	public int getTotalvalue() {
		return this.totalvalue;
	}

	public void setTotalvalue(int totalvalue) {
		this.totalvalue = totalvalue;
	}

	public String getVerbname() {
		return this.verbname;
	}

	public void setVerbname(String verbname) {
		this.verbname = verbname;
	}

}