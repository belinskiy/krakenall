package com.bgv.entity;

public class MaltEntity {

	String to ;
    String word ;
    String name;
    String add1;
    String add2;
    
    String type;
    
    String N1;
    String N2; 
    String normword;
    
    String number;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdd1() {
		return add1;
	}

	public void setAdd1(String add1) {
		this.add1 = add1;
	}

	public String getAdd2() {
		return add2;
	}

	public void setAdd2(String add2) {
		this.add2 = add2;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getN1() {
		return N1;
	}

	public void setN1(String n1) {
		N1 = n1;
	}

	public String getN2() {
		return N2;
	}

	public void setN2(String n2) {
		N2 = n2;
	}

	public String getNormword() {
		return normword;
	}

	public void setNormword(String normword) {
		this.normword = normword;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
    
}
