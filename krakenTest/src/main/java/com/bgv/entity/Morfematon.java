package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the morfematon database table.
 * 
 */
@Entity
@Table(name="morfematon")
@NamedQuery(name="Morfematon.findAll", query="SELECT m FROM Morfematon m")
public class Morfematon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String aword;

	private int awordton;

	private String bword;

	private int bwordton;

	private String vword;

	private int vwordton;

	public Morfematon() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAword() {
		return this.aword;
	}

	public void setAword(String aword) {
		this.aword = aword;
	}

	public int getAwordton() {
		return this.awordton;
	}

	public void setAwordton(int awordton) {
		this.awordton = awordton;
	}

	public String getBword() {
		return this.bword;
	}

	public void setBword(String bword) {
		this.bword = bword;
	}

	public int getBwordton() {
		return this.bwordton;
	}

	public void setBwordton(int bwordton) {
		this.bwordton = bwordton;
	}

	public String getVword() {
		return this.vword;
	}

	public void setVword(String vword) {
		this.vword = vword;
	}

	public int getVwordton() {
		return this.vwordton;
	}

	public void setVwordton(int vwordton) {
		this.vwordton = vwordton;
	}

}