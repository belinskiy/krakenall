package com.bgv.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the probabilitydirty database table.
 * 
 */
@Entity
@Table(name= "probabilitydirty")
@NamedQuery(name="Probabilitydirty.findAll", query="SELECT p FROM Probabilitydirty p")
public class Probabilitydirty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pid;

	private String exguid;

	private String thash;

	@Lob
	private String tokenjson;
	
	private Timestamp dateupdate;

	public Probabilitydirty() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getExguid() {
		return this.exguid;
	}

	public void setExguid(String exguid) {
		this.exguid = exguid;
	}

	public String getThash() {
		return this.thash;
	}

	public void setThash(String thashb) {
		this.thash = thashb;
	}

	public String getTokenjson() {
		return this.tokenjson;
	}

	public void setTokenjson(String tokenjson) {
		this.tokenjson = tokenjson;
	}
	public Timestamp getDateupdate() {
		return this.dateupdate;
	}

	public void setDateupdate(Timestamp dateupdate) {
		this.dateupdate = dateupdate;
	}
}