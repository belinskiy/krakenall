package com.bgv.entity;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilityverbs database table.
 * 
 */
@Entity
@Table(name="probabilityadjective")
@NamedQuery(name="Probabilityadjective.findAll", query="SELECT p FROM Probabilityadjective p")
public class Probabilityadjective implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String nounname;

	private int totalvalue;

	private String adjective;

	public Probabilityadjective() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNounname() {
		return this.nounname;
	}

	public void setNounname(String nounname) {
		this.nounname = nounname;
	}

	public int getTotalvalue() {
		return this.totalvalue;
	}

	public void setTotalvalue(int totalvalue) {
		this.totalvalue = totalvalue;
	}

	public String getAdjective() {
		return adjective;
	}

	public void setAdjective(String adjective) {
		this.adjective = adjective;
	}



}