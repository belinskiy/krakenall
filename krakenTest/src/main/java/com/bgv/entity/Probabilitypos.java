package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilitypos database table.
 * 
 */
@Entity
@Table(name="probabilitypos")
@NamedQuery(name="Probabilitypos.findAll", query="SELECT p FROM Probabilitypos p")
public class Probabilitypos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pid")
	private int pid;
	private String exguid;
	private String guid;

	private int tokenpos;

	private int tokentype;

	public Probabilitypos() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getTokenpos() {
		return this.tokenpos;
	}

	public void setTokenpos(int tokenpos) {
		this.tokenpos = tokenpos;
	}

	public int getTokentype() {
		return this.tokentype;
	}

	public void setTokentype(int tokentype) {
		this.tokentype = tokentype;
	}
	public String getExguid() {
		return this.exguid;
	}

	public void setExguid(String exguid) {
		this.exguid = exguid;
	}
}