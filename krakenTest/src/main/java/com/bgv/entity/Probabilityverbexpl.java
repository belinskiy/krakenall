package com.bgv.entity;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the probabilityverbexpl database table.
 * 
 */
@Entity
@Table(name="probabilityverbexpl")
@NamedQuery(name="Probabilityverbexpl.findAll", query="SELECT p FROM Probabilityverbexpl p")
public class Probabilityverbexpl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String nounname;

	private int totalvalue;

	private String verbname;
	
	private String nounnamet;
	
	private String exptype;
	
	private String exptext;

	public Probabilityverbexpl() {
	}

	public int getId() {
		return this.id;
	}

	public String getExptype() {
		return exptype;
	}

	public void setExptype(String exptype) {
		this.exptype = exptype;
	}

	public String getExptext() {
		return exptext;
	}

	public void setExptext(String exptext) {
		this.exptext = exptext;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNounname() {
		return this.nounname;
	}

	public void setNounname(String nounname) {
		this.nounname = nounname;
	}
	public String getNounnamet() {
		return this.nounnamet;
	}

	public void setNounnamet(String nounnamet) {
		this.nounnamet = nounnamet;
	}
	public int getTotalvalue() {
		return this.totalvalue;
	}

	public void setTotalvalue(int totalvalue) {
		this.totalvalue = totalvalue;
	}

	public String getVerbname() {
		return this.verbname;
	}

	public void setVerbname(String verbname) {
		this.verbname = verbname;
	}

}