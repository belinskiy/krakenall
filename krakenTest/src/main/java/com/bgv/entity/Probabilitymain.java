package com.bgv.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the probabilitymain database table.
 * 
 */
@Entity
@Table(name= "probabilitymain")
@NamedQuery(name="Probabilitymain.findAll", query="SELECT p FROM Probabilitymain p")
public class Probabilitymain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pid")
	private int pid;

	private String guid;

	private String tokentext;

	private int tokentotal;

	private int tokentype;
	private Timestamp dateupdate;
	public Probabilitymain() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getTokentext() {
		return this.tokentext;
	}

	public void setTokentext(String tokentext) {
		this.tokentext = tokentext;
	}

	public int getTokentotal() {
		return this.tokentotal;
	}

	public void setTokentotal(int tokentotal) {
		this.tokentotal = tokentotal;
	}

	public int getTokentype() {
		return this.tokentype;
	}

	public void setTokentype(int tokentype) {
		this.tokentype = tokentype;
	}
	public Timestamp getDateupdate() {
		return this.dateupdate;
	}

	public void setDateupdate(Timestamp dateupdate) {
		this.dateupdate = dateupdate;
	}
}