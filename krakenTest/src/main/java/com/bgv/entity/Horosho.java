package com.bgv.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the morfema database table.
 * 
 */
@Entity
@Table(name="horosho")
@NamedQuery(name="Horosho.findAll", query="SELECT m FROM Horosho m")
public class Horosho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String word;

	private String nword;

	private int total;

	public Horosho() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getNword() {
		return nword;
	}

	public void setNword(String nword) {
		this.nword = nword;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getWord() {
		return this.word;
	}

	public void setWord(String word) {
		this.word = word;
	}

}