package com.bgv.springmvc.domain;

import java.util.ArrayList;
import java.util.List;

public class TokenProbability {
	
	List<String[]> alltokens = new  ArrayList<String[]>();
	
	List<FindObject> findobject = new ArrayList<FindObject>();

	public List<String[]> getAlltokens() {
		return alltokens;
	}

	public void setAlltokens(List<String[]> alltokens) {
		this.alltokens = alltokens;
	}

	public List<FindObject> getFindobject() {
		return findobject;
	}

	public void setFindobject(List<FindObject> findobject) {
		this.findobject = findobject;
	}
	
	
}
