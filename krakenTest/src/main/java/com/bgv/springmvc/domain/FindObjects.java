package com.bgv.springmvc.domain;

import java.util.List;

public class FindObjects {
	private String guid;
	private String text;
    private List <FindObject> findObject;
    private String response;
    private String msg;
    
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<FindObject> getFindObject() {
		return findObject;
	}
	public void setFindObject(List<FindObject> findObject) {
		this.findObject = findObject;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}


}
