package com.bgv.springmvc.domain;

import org.springframework.scheduling.annotation.EnableScheduling;

public class Config {
//static String root="C:/Users/Electron/Downloads/";
	static String root="/opt/wildfly-10.1.0.Final/standalone/data/";
	static String model="spammodel_15000_Token";
	static String namefindermodel="ru-ner-person-Token-25000-Rx.bin";
	static String objectstext="objects.txt";
	static String evalfile="evalspam.xml";
	static int limitEval=25000;
	static int limitEdu=250000;
	
public static int getLimitEval() {
		return limitEval;
	}

	public static void setLimitEval(int limitEval) {
		Config.limitEval = limitEval;
	}

	public static int getLimitEdu() {
		return limitEdu;
	}

	public static void setLimitEdu(int limitEdu) {
		Config.limitEdu = limitEdu;
	}

public static String getRoot() {
	return root;
}

public static void setRoot(String root) {
	Config.root = root;
}

public static String getModel() {
	return model;
}

public static void setModel(String model) {
	Config.model = model;
}

public static String getNamefindermodel() {
	return namefindermodel;
}

public static void setNamefindermodel(String namefindermodel) {
	Config.namefindermodel = namefindermodel;
}

public static String getObjectstext() {
	return objectstext;
}

public static void setObjectstext(String objectstext) {
	Config.objectstext = objectstext;
}

public String getEvalfile() {
	return evalfile;
}





}
