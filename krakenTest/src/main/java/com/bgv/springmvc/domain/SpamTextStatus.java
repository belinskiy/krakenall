package com.bgv.springmvc.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name = "spamtextstatus")
public class SpamTextStatus {
	    private String guid;
	    private String evaltype;
	    private String evalvalues;
	    private String response;
	    private String msg;
	    
	    
		public SpamTextStatus(String guid, String evaltype, String evalvalues, String response,String msg) {
			this.guid = guid;
			this.evaltype = evaltype;
			this.evalvalues = evalvalues;
			this.response=response;
			this.msg=msg;
		}
		
		
	    public String getResponse() {
			return response;
		}


		public void setResponse(String response) {
			this.response = response;
		}


		public SpamTextStatus(){
			
		}
	    
	    
	    public String getMsg() {
			return msg;
		}


		public void setMsg(String msg) {
			this.msg = msg;
		}

		//@XmlElement
	    public String getGuid() {
			return guid;
		}
		public void setGuid(String guid) {
			this.guid = guid;
		}
		//@XmlElement
		public String getEvaltype() {
			return evaltype;
		}
		public void setEvaltype(String evaltype) {
			this.evaltype = evaltype;
		}
		//@XmlElement
		public String getEvalvalues() {
			return evalvalues;
		}
		public void setEvalvalues(String evalvalues) {
			this.evalvalues = evalvalues;
		}
		
	    
	    
}
