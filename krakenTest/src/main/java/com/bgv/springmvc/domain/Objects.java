package com.bgv.springmvc.domain;



import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the [mindscan.nlp.objects] database table.
 * 
 */
//@Entity
//@Table(name="[Objects]")
//@NamedQuery(name="Objects.findAll", query="SELECT m FROM Objects m")
public class Objects implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Id")
	private long id;

	@Column(name="Name")
	private String name;

	@Column(name="StrongSynonyms")
	private String strongSynonyms;

	@Column(name="WeakSynonyms")
	private String weakSynonyms;
	
	@Column(name="WeakSearchContext")
	private String weakSearchContext; 
	
	
	public String getWeakSearchContext() {
		return weakSearchContext;
	}

	public void setWeakSearchContext(String weakSearchContext) {
		this.weakSearchContext = weakSearchContext;
	}

	public String getWeakSynonyms() {
		return weakSynonyms;
	}

	public void setWeakSynonyms(String weakSynonyms) {
		this.weakSynonyms = weakSynonyms;
	}

	@Column(name="CategoryId")
	private int categoryId;
	
	
	@Column(name="TypeId")
	private int typeId;
	


	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public Objects() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrongSynonyms() {
		return strongSynonyms;
	}

	public void setStrongSynonyms(String strongSynonyms) {
		this.strongSynonyms = strongSynonyms;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}




	

}