package com.bgv.springmvc.configuration;

public class TaskMessage
{
    private String url;

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}