package com.bgv.springmvc.configuration;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bgv.entity.Probabilitycounter;
import com.bgv.service.IProbabilityService;
import com.bgv.service.IProbabilitycounterService;
import com.bgv.service.IProbabilitydirtyService;
import com.bgv.springmvc.domain.FindObject;
import com.bgv.springmvc.domain.TokenProbability;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ScrapingResultHandler
{
 //   @Autowired
//    private BookmarkRepository bookmarkRepository;
	    @Autowired
		private IProbabilityService probService;

		
		@Autowired
		private IProbabilitycounterService counterService;
		
		
    public void handleMessage(ScrapingResultMessage scrapingResultMessage) throws ParseException
    {
        System.out.println("ResultHandler summary: " + scrapingResultMessage.getSummary());
        final String exguid = scrapingResultMessage.getUrl();
        final String thashs = scrapingResultMessage.getSummary();
        final String stringToParse = scrapingResultMessage.getCodeSnippets();
        
	
		Probabilitycounter counter=new Probabilitycounter();
		counter.setExguid(exguid);
		counter.setThash(thashs);					
		counterService.addProbabilitycounter(counter);
		
		
		
		
		    JSONParser parser = new JSONParser();
	        JSONArray slideContent = (JSONArray) parser.parse(stringToParse);
	        Iterator i = slideContent.iterator();					 
	        
	        
	        
	     List<TokenProbability> problist = new ArrayList<TokenProbability>();
		 TokenProbability c=new TokenProbability();
         List<String[]> alltokens=new ArrayList<String[]>();
         List<FindObject> obj=new ArrayList<FindObject>();
int k=0;
	     while (i.hasNext()) {
	            
	        	
	        	
	     JSONObject json = (JSONObject) i.next();
	            
	        
	        
	       
		 
		 

         
         String thash="";
         if(k==0)
         thash = (String) ""+json.get("thash");
         
         
         else if(k==1){
         JSONArray findObject = (JSONArray) json.get("findObject");
		 Iterator<JSONObject> iteratorfindObject = findObject.iterator();
         while (iteratorfindObject.hasNext()) {
         	
     	    JSONObject find = iteratorfindObject.next();
     	        String id = (String) find.get("id");
	            String names = (String) find.get("name");
	            FindObject n=new FindObject();
	            n.setId(id);
	            n.setName(names);
	            obj.add(n);
	            
         }
	        }
         else  if(k==2){
		 JSONArray tokens = (JSONArray) json.get("tokens");
		 Iterator<JSONObject> iterator = tokens.iterator();
         while (iterator.hasNext()) {
         	
     	    JSONObject tok = iterator.next();
     	        String ids = (String) tok.get("name");
	            String names = (String) tok.get("value");
	            String[] index = new String[2];
	            index[0]=ids;
	            index[1]=names;	
			    		try{       
			            String d= names.trim();
			            if(d!=null&&!d.equals(""))
			            	alltokens.add(index);
			    		}catch(Exception e){e.printStackTrace();}
         }
         
         }
         k++;
		
	        }
	        c.setAlltokens(alltokens);
		    c.setFindobject(obj);
	        problist.add(c);
		
		    probService.addTokenProbability(exguid, problist);
	       
	
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//        final List<Bookmark> bookmarks = bookmarkRepository.findByUrl(url);
//        if (bookmarks.size() == 0)
//        {
//            System.out.println("No bookmark of url: " + url + " found.");
//        }
//        else
//        {
//            for (Bookmark bookmark : bookmarks)
//            {
//                bookmark.setSummary(scrapingResultMessage.getSummary());
//                bookmarkRepository.save(bookmarks);
//                System.out.println("updated bookmark: " + url);
//            }
//        }
    }
}