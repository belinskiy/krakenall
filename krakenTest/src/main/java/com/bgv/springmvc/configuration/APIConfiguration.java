package com.bgv.springmvc.configuration;
import javax.sql.DataSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
@Configuration
@EnableWebMvc
@EnableAsync
@EnableScheduling
@ComponentScan(basePackages = "com.bgv")
public class APIConfiguration {
	
//	 	@Autowired
//	 	@Qualifier("dsUsers")
//		DataSource dataSource;
//
//		@Bean
//		public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
//			return new NamedParameterJdbcTemplate(dataSource);
//		}
//
//	 	@Bean
//		public JdbcTemplate getJdbcTemplate() {
//			return new JdbcTemplate(dataSource);
//		}
	 	
	 	
	  public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    	registry.addResourceHandler("/app-resources/**").addResourceLocations("/resources/");
	    }    	

	  
	 
}