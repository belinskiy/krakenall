package com.bgv.springmvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bgv.dao.probability.IilGsonReadDAO;
import com.bgv.practice.AnalysisDAO;
import com.bgv.practice.Predicates;
import com.bgv.practice.ProcessorDAO;
import com.bgv.practice.ProcessorResult;
import com.bgv.service.IParserSevice;
import com.bgv.service.ITESTService;
import com.bgv.springmvc.domain.FindObjects;
import com.bgv.springmvc.domain.Message;
import com.bgv.springmvc.domain.SpamText;

@RestController
public class APIService {
	 static boolean ineededu=false;
	 private String version="17.5.14.1";
	 
	        @Autowired
			private AnalysisDAO analysis;
	
	        @Autowired
	   	    private  ProcessorDAO processor;
	        
	        @Autowired
	    	IParserSevice maltrest;
	        
	        @Autowired
			private IilGsonReadDAO iilGsonRead;   
	        
	        @Autowired
			private ITESTService testService;
	        
	  
	        
	        @RequestMapping(value = "/v1/nlp/object/discover/new/liniar", method = RequestMethod.POST)
	    	public @ResponseBody FindObjects getNewData(@RequestBody SpamText text) { 	
	    	boolean getrezult=false;
	    	JSONObject objson = new JSONObject();
	    	JSONArray listjson = new JSONArray();

	    	  if (text == null) {
	    		  FindObjects result= new FindObjects();
	    			result.setFindObject(null);
	    			result.setGuid("");
	    			result.setMsg("");
	    			result.setResponse("false");
	    			result.setText("empty request Body");
	    			return null;
	    	    }
	    	  
	    	try{
	    	String id = text.getGuid();
	    	if(id.equals("33"))getrezult=true;

	    	String utext = text.getUtftext();
	    	if(id!=null&&utext!=null){

	    		
	    		
	    		Map<Integer,String> strings=analysis.getSentecesFromString(utext);

	    		if(strings!=null){
	    			 
	    			//1 разобрать каждую на массив
	    			//2 поискать сущ
	    			//3 поискать объекты
	    			//4 поискать фразио
	    			 int stnum=0;
	    			 for (Map.Entry<Integer, String> estring : strings.entrySet())
	    			 {try{
	    				 stnum++;
	    				 JSONObject srtjson = new JSONObject();
	    				 JSONArray wordlist = new JSONArray();
	    	//номер строки в документе				
	    				 String singltext=estring.getValue();		 
	    	//начальный текст				
	    				
	    				 

	    	ProcessorResult datain = processor.getTextRezult(singltext,ineededu, getrezult);
	    	         	 //1.Предикаты
	    			    
	    				   
	    				    Map<Integer, String> predicates = new  HashMap<Integer, String>();
	    				    JSONObject resmaltjson=new JSONObject(); 
	    				    if(getrezult){
	    					try{	
	    						Predicates resmalt = maltrest.getCollPOSdataAndPredicates(singltext, datain.getWordindex());
	    						predicates  = resmalt.getPredicates();
	    					    resmaltjson = resmalt.getOutjson();
	    					   
	    					} catch (Exception e) {
	    						//e.printStackTrace();
	    					}
	    					}    
	    				    
	    				    
	    				 //1. Подготовка ответа рест с core
	    				 srtjson =   processor.createNewResult(datain, ineededu, getrezult, predicates);
	    				 srtjson.put("predicate", "false"); if(getrezult) srtjson.put("predicate", resmaltjson);
	    				 srtjson.put("origtext", singltext);
	    				 srtjson.put("meaning", "0.88");
	    				 srtjson.put("number", ""+stnum);
	    				 String category="";
	    			     try{for (Map.Entry<String, Integer> dd:datain.getCategoryMap().entrySet()){category+=""+dd.getKey()+"-"+dd.getValue()+"#";}} catch (Exception e) {}
	    			     srtjson.put("category", category);
	    			     

	    			     
	    				  //3.Вопросы  
	    				  if(analysis.hasVoprosSymbol(singltext)){srtjson.put("question", "true");}else srtjson.put("question", "false");
	    				 
	    				 listjson.add(srtjson);
	    				 
	    				 
	    			     }catch(Exception e){e.printStackTrace();}
	    			 }}
	    		
	            objson.put("all", listjson);
	    		
	    		FindObjects result= new FindObjects();
	    		result.setFindObject(null);
	    		result.setGuid(id);
	    		result.setMsg("");
	    		result.setResponse("true");
	    		if(getrezult)
	    			result.setText(objson.toString());
	    		else
	    		result.setText("");
	    		
	    		return result;
	    	//return objson;
	    	}
	    	else{
	    		FindObjects result= new FindObjects();
	    		result.setFindObject(null);
	    		result.setGuid(id);
	    		result.setMsg("empty text");
	    		result.setResponse("false");
	    		result.setText("");
	    		return null;
	    	}
	    	}catch(Exception e){e.printStackTrace(); 
	    	FindObjects result= new FindObjects();
	    	result.setFindObject(null);
	    	result.setGuid("");
	    	result.setMsg("");
	    	result.setResponse("false");
	    	result.setText("empty request Body");
	    	return null;}  



	    	}
	    	       
	        
	        
	@RequestMapping(value = "/v1/nlp/object/discover/liniar", method = RequestMethod.POST)
	public @ResponseBody FindObjects getModData(@RequestBody SpamText text) { 	
	boolean getrezult=false;
	JSONObject objson = new JSONObject();
	JSONArray listjson = new JSONArray();

	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return null;
	    }
	  
	try{
	String id = text.getGuid();
	if(id.equals("33"))getrezult=true;

	String utext = text.getUtftext();
	if(id!=null&&utext!=null){

		
		
		Map<Integer,String> strings=analysis.getSentecesFromString(utext);

		if(strings!=null){
			 
			//1 разобрать каждую на массив
			//2 поискать сущ
			//3 поискать объекты
			//4 поискать фразио
			 int stnum=0;
			 for (Map.Entry<Integer, String> estring : strings.entrySet())
			 {try{
				 stnum++;
				 JSONObject srtjson = new JSONObject();
				 JSONArray wordlist = new JSONArray();
	//номер строки в документе				
				 String singltext=estring.getValue();		 
	//начальный текст				
				
				 

		ProcessorResult datain = processor.getTextRezult(singltext, ineededu, getrezult);
				 srtjson =   processor.createRestRezult(datain, ineededu, getrezult);
				 srtjson.put("origtext", singltext);
				 srtjson.put("number", ""+stnum);
				 String category="";
				 
			     //try{for (Map.Entry<String, Integer> dd:datain.getCategoryMap().entrySet()){category+=""+dd.getKey()+"-"+dd.getValue()+"#";}} catch (Exception e) {}
			     
			     srtjson.put("category", category);
			     
			     
				 if(analysis.hasVoprosSymbol(singltext)){srtjson.put("isvopros", "true");}else srtjson.put("isvopros", "false");
				 JSONObject resmalt=new JSONObject();
				 srtjson.put("predicate", "false");
				    if(getrezult){
					try{	
						resmalt=maltrest.getCollPOSdata(singltext);
					    srtjson.put("predicate", resmalt);
					} catch (Exception e) {
						//e.printStackTrace();
					}
					}
				 listjson.add(srtjson);
				 
				 
			     }catch(Exception e){e.printStackTrace();}
			 }}
		
        objson.put("all", listjson);
		
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("");
		result.setResponse("true");
		if(getrezult)
			result.setText(objson.toString());
		else
		result.setText("");
		
		return result;
	//return objson;
	}
	else{
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("empty text");
		result.setResponse("false");
		result.setText("");
		return null;
	}
	}catch(Exception e){e.printStackTrace(); 
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid("");
	result.setMsg("");
	result.setResponse("false");
	result.setText("empty request Body");
	return null;}  



	}
	
	@RequestMapping("/iil/init")
	public String init() {
		 try {analysis.restLoadEmotion();} catch (Exception e) {e.printStackTrace();}
		 try {analysis.restLoadVerb();} catch (Exception e) {e.printStackTrace();}
		 try {iilGsonRead.readJsonStream();} catch (Exception e) {e.printStackTrace();}
		return "Init Kraken ";
	}
	
	@RequestMapping("/edu/on")
	public String eduon() {
		ineededu=true;
		
		return "Kraken ineededu "+ineededu;
	}
	@RequestMapping("/edu/off")
	public String eduoff() {
		ineededu=false;
		
		return "Kraken ineededu "+ineededu;
	}
	
	
	@RequestMapping(value = "/v1/nlp/rule/{cmd}", produces = "text/plain;charset=UTF-8")
	public @ResponseBody String searchRootRule(@PathVariable String cmd) {
		return testService.searchRootRule(cmd);
	}
	@RequestMapping(value = "/v1/nlp/rule/test/{cmd}", produces = "text/plain;charset=UTF-8")
	public @ResponseBody String searchRootRuleAVBProp(@PathVariable String cmd) {
		return testService.searchRootRuleAVBProp(cmd);
	}
	
	
	@RequestMapping("/verbs/compress")
	public String compressVerb() {

		 try {analysis.compressVerb();} catch (Exception e) {e.printStackTrace();}

		return "verbs compress Kraken ";
	}
	@RequestMapping("/verbs/compress/category")
	public String compressVerbcategory() {

		 //try {analysis.compressVerbCategory();} catch (Exception e) {e.printStackTrace();}
		 try {analysis.compressVerbCategoryVersionB();} catch (Exception e) {e.printStackTrace();}
		
		return "verbs compress category Kraken ";
	}
	
	
	
	@RequestMapping("/verbs/compress/triplet")
	public String compressVerbAVB() {

		 try {analysis.compressVerbAVB();} catch (Exception e) {e.printStackTrace();}

		return "Kraken triplet verbs compress done!";
	}
	
	@RequestMapping("/verbs/compress/categorytriplet")
	public String compressVerbAVBAndCategory() {

		 try {analysis.compressVerbAVBAndCategory();} catch (Exception e) {e.printStackTrace();}

		return "Kraken categorytriplet verbs compress done!";
	}
	
	
	@RequestMapping(value = "/v1/nlp/phrase/{cmd}", produces = "text/plain;charset=UTF-8")
	public String getPhraseSortedToken(@PathVariable String cmd) {
		int exec=0;
	     String a="";
	     String b="b";
		try{
			if(cmd.contains("|")){
			 String[] objectidData = cmd.split("\\|");
			 exec=Integer.parseInt(objectidData[0]);
			 a=objectidData[1];
		     return testService.emotionTest(a, exec);
		     }
		 } catch (Exception e) {e.printStackTrace();}
		
		return b;

	}
	
	
	
	@RequestMapping(value = "/v1/nlp/vec/discover/liniar", method = RequestMethod.POST)
	public @ResponseBody JSONObject getVecRezult(@RequestBody SpamText text) { 	
	
	JSONObject objson = new JSONObject();
	

	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return null;
	    }
	  
	
	String id = text.getGuid();
	
	String utext = text.getUtftext();
	if(id!=null&&utext!=null){	
		objson = processor.getVecRezult(utext);
	}
	
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid(id);
	result.setMsg("");
	result.setResponse("true");
	
	
		result.setText(objson.toString());
	
	
	return objson;
	
	}
	
	
	@RequestMapping(value = "/v1/nlp/word/discover/liniar", method = RequestMethod.POST)
	public @ResponseBody JSONObject getVerData(@RequestBody SpamText text) { 	
	boolean getrezult=false;
	boolean minimumres=false;
	JSONObject objson = new JSONObject();
	JSONArray listjson = new JSONArray();

	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return null;
	    }
	  
	
	String id = text.getGuid();
	if(id.equals("33"))getrezult=true;
	if(id.equals("44"))getrezult=true;  minimumres=true;
	String utext = text.getUtftext();
	if(id!=null&&utext!=null){	
		objson = processor.getVariationRezult(utext, ineededu, getrezult, minimumres);
	}
	
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid(id);
	result.setMsg("");
	result.setResponse("true");
	
	if(getrezult)
		result.setText(objson.toString());
	else
	result.setText("");
	
	return objson;
	
	}
	
	
	
	@RequestMapping(value = "/verbs/compress/verb/single/{cmd}", produces = "text/plain;charset=UTF-8")
	public String compressVerbAVBSingle(@PathVariable String cmd) {
		String data="нету";
		 try {
			 if(cmd.contains("|")){
				 
				 String[] objectidData = cmd.split("\\|");
				 String nouna=objectidData[0];
				 String verb=objectidData[1];
				 String nounb=objectidData[2];
			    data= analysis.compressVerbAVBSingle(  nouna, verb,  nounb);
			     
			 }
		 } catch (Exception e) {e.printStackTrace();}

		return "Kraken = "+data;
	}
	
	

    @RequestMapping(value = "/v1/nlp/corrector", method = RequestMethod.POST)
	public @ResponseBody FindObjects correctorRule(@RequestBody SpamText text) { 	
		String utext = text.getUtftext();
		Object objson = analysis.correctorData(utext);
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid("33");
		result.setMsg("");
		result.setResponse("true");
        result.setText(objson.toString());
        return result;
	}
	
    
	@RequestMapping("/v1/nlp/morfology/{word}")
	public Message messageWord(@PathVariable String word) {
		Message res;
		res=analysis.getMorfologyWord(word);
		return res;
	}
	
	@RequestMapping("/reload/verbs")
	public String reloadverb() {

		 try {analysis.reLoadVerb();} catch (Exception e) {e.printStackTrace();}

		return "verbs loaded";
	}
	@RequestMapping("/reload/objs")
	public String reloadObjs() {

		 try {analysis.restUpdateObject();} catch (Exception e) {e.printStackTrace();}

		return "objs loaded";
	}
	
	@RequestMapping("/reload/noname")
	public String reloadnoname() {

		 try {analysis.restReLoadQnoname();} catch (Exception e) {e.printStackTrace();}

		return "noname loaded";
	}
	
	@RequestMapping("/reload/emotion")
	public String emotion() {

		 try {analysis.restLoadEmotion();} catch (Exception e) {e.printStackTrace();}

		return "emotion loaded";
	}
	
}
