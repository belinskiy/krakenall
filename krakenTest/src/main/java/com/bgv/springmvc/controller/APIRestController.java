package com.bgv.springmvc.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bgv.dao.probability.IilGsonReadDAO;
import com.bgv.entity.Morfematon;
import com.bgv.entity.Person;
import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.obj.IndexWrapper;
import com.bgv.nlp.obj.NameFinderModelMEDAO;
import com.bgv.nlp.obj.ObjDAO;
import com.bgv.nlp.obj.OldObjDAO;
import com.bgv.nlp.spam.SpamDAO;
import com.bgv.practice.AnalysisDAO;
import com.bgv.practice.CleanTextDAO;
import com.bgv.practice.ProcessorDAO;
import com.bgv.practice.ProcessorResult;
import com.bgv.practice.QResult;
import com.bgv.service.IMorfematonService;
import com.bgv.service.IParserSevice;
import com.bgv.service.IPersonService;
import com.bgv.service.ITonalityService;
import com.bgv.springmvc.domain.Config;
import com.bgv.springmvc.domain.FindObject;
import com.bgv.springmvc.domain.FindObjects;
import com.bgv.springmvc.domain.Message;
import com.bgv.springmvc.domain.SpamText;
import com.bgv.springmvc.domain.SpamTextStatus;

//@RestController
public class APIRestController {
	/*
	@Autowired
	IParserSevice maltrest; 
	@Autowired
	 private SpamDAO spam;
	 
	 @Autowired
	 private ObjDAO objs;
	 
	 @Autowired
	 private OldObjDAO oldobjs;
	 
	 @Autowired
	 private  ProcessorDAO processor;
	 
	 @Autowired
	 private NameFinderModelMEDAO nameFinderModelMEDAO;
	 
	    @Autowired
		private IPersonService personService;
	    
	    @Autowired
		private IMorfematonService morfematonService;
	    
	    @Autowired
	 	private ITonalityService tonalityService;
	 
	    @Autowired
		private  CleanTextDAO cleanText;
	    
	    @Autowired
		private FastModelDAO fastMemoryModel;
	    
	    @Autowired
		private AnalysisDAO analysis;
	    
	    @Autowired
		 private IilGsonReadDAO iilGsonRead;   
	    
	    private String version="17.1.24.1";
	    static boolean logactive=false;
	    static boolean ineededu=false;
	    //static int postcount=0;
	    
	@RequestMapping("/")
	public String welcome() {
		//return "Welcome to Kraken "+version+" postcount="+postcount;
		return "Welcome to Kraken "+version;
	}
	
	@RequestMapping("/iil/on")
	public String iil() {
	
		System.out.println("------------------------Start iilGsonRead tasks...");
		 
		 try {iilGsonRead.readJsonStream();} catch (Exception e) {e.printStackTrace();}
		 
		 System.out.println("------------------------END iilGsonRead tasks...");
		return "iilGsonRead loaded to Kraken ";
	}
	
	@RequestMapping("/verbs/on")
	public String verbs() {

		 try {analysis.restLoadVerb();} catch (Exception e) {e.printStackTrace();}

		return "verbs loaded to Kraken ";
	}
	
	@RequestMapping("/verbs/atributes/{cmd}")
	
	public Map<String, Integer> getAtributesVerb(@PathVariable String cmd) {
		 Map<String, Integer> returnmap = new HashMap<String, Integer>(); 
		 returnmap.put("n", -1);
		 if(cmd!=null)
		 try {returnmap = analysis.getAtributesVerb(cmd); returnmap =sortByValueSI(returnmap);} catch (Exception e) {e.printStackTrace(); returnmap.put(""+e.toString(), 1); return returnmap;}
         return returnmap;
	}
	
	
	@RequestMapping("/verbs/reload")
	public String reloadverb() {

		 try {analysis.reLoadVerb();} catch (Exception e) {e.printStackTrace();}

		return "verbs loaded to Kraken ";
	}
	@RequestMapping("/objs/reload")
	public String reloadObjs() {

		 try {analysis.restUpdateObject();} catch (Exception e) {e.printStackTrace();}

		return "objs loaded to Kraken ";
	}
	
	@RequestMapping("/verbs/compress")
	public String compressVerb() {

		 try {analysis.compressVerb();} catch (Exception e) {e.printStackTrace();}

		return "verbs compress Kraken ";
	}
	@RequestMapping("/verbs/compress/category")
	public String compressVerbcategory() {

		 try {analysis.compressVerbCategory();} catch (Exception e) {e.printStackTrace();}

		return "verbs compress category Kraken ";
	}
	
	
	
	@RequestMapping("/verbs/compress/triplet")
	public String compressVerbAVB() {

		 try {analysis.compressVerbAVB();} catch (Exception e) {e.printStackTrace();}

		return "Kraken triplet verbs compress done!";
	}
	@RequestMapping("/verbs/compress/categorytriplet")
	public String compressVerbAVBAndCategory() {

		 try {analysis.compressVerbAVBAndCategory();} catch (Exception e) {e.printStackTrace();}

		return "Kraken categorytriplet verbs compress done!";
	}
	
	
	
	@RequestMapping("/emotion/on")
	public String emotion() {

		 try {analysis.restLoadEmotion();} catch (Exception e) {e.printStackTrace();}

		return "emotion loaded to Kraken ";
	}
	
	@RequestMapping("/iil/init")
	public String init() {
		 try {analysis.restLoadEmotion();} catch (Exception e) {e.printStackTrace();}
		 try {analysis.restLoadVerb();} catch (Exception e) {e.printStackTrace();}
		 try {iilGsonRead.readJsonStream();} catch (Exception e) {e.printStackTrace();}
		return "Init Kraken ";
	}
	
	@RequestMapping("/edu/on")
	public String eduon() {
		ineededu=true;
		
		return "Kraken ineededu "+ineededu;
	}
	@RequestMapping("/edu/off")
	public String eduoff() {
		ineededu=false;
		
		return "Kraken ineededu "+ineededu;
	}
	
	@RequestMapping("/logactive/on")
	public String logactiveon() {
		logactive=true;
		
		return "Welcome to Kraken "+logactive;
	}
	@RequestMapping("/logactive/off")
	public String logactiveoff() {
		logactive=false;
		
		return "Welcome to Kraken "+logactive;
	}

	//=======================debug===========================================
	
	QResult getQobjects(String nword, String type){
	 	    QResult result=new QResult();
	 	    StringBuffer sbBuffer=new StringBuffer();
	 	    sbBuffer.append("");
		    boolean foundresult=false; 
		    try{ 
		  
		    Map<String, List> iilobjectsList = fastMemoryModel.getIilobjects();
		    Map<String, List> iilinstanceList = fastMemoryModel.getOfinstance();
			Map<String, List> iilsubclassofList = fastMemoryModel.getIilsubclassof();
			Map<String, List> iilmaincategory = fastMemoryModel.getMaincategory();
			Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
			Map<String, List> iiltext = fastMemoryModel.getIilobjectsToText();
			Map<String, List> iilproperties = fastMemoryModel.getIilotherprops();
			
			
			 List<String> qindexList = iilobjectsList.get(nword);
			    
			    sbBuffer.append("type="+type+"[nword=").append(nword).append("]");
			 
			 
			    Map<String, List> returtQ = new HashMap<String, List>();

				returtQ = searchMainObjectArray(qindexList, iilobjectsList, iilinstanceList,
						iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);
				 
				int qsize=0;
				try{qsize=returtQ.size();}catch(Exception e){}
				
				sbBuffer.append("[size=").append(qsize).append("]");
				 
				if(qsize>0){ 
				sbBuffer.append("[map=").append(returtQ.size()).append("]"); 
				for (Map.Entry<String, List> entry : returtQ.entrySet())
				{
				    
					String mainQ=entry.getKey();
					sbBuffer.append("[mainQ=").append(mainQ).append("]");
					List<String> content = entry.getValue();
					if(content!=null){
						for(String data:content){	
							sbBuffer.append("").append(data).append("/");
							foundresult=true;
						}
					}
					sbBuffer.append("=map]"); 
				}}
				
		        } catch (Exception e) {
					e.printStackTrace();
				}
		   result.setOut(sbBuffer.toString());
		   result.setFoundresult(foundresult);
		return result;
	}
	
	@RequestMapping(value = "/v1/nlp/rule/{cmd}", produces = "text/plain;charset=UTF-8")
	//@RequestMapping("/v1/nlp/rule/{cmd}")
	public @ResponseBody String searchRootRule(@PathVariable String cmd) {
	    String result="1";
	   
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
		Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
	    try{ 
	    	StringBuffer sbBuffer=new StringBuffer();
	    	if(cmd!=null){
	    		if(!cmd.equals("")){
	    			
	    			
	    			 String[] objectidData = cmd.split("\\|");
				
	    			 String nword=objectidData[0];
	    			 String vword=objectidData[1];
	    			 
	    			 //nword - найти Q контекст
	    			 //searchRootObject 
	    			 //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
	    			 //записать весь лог
                        boolean foundresult=false;
                        QResult z = getQobjects(nword, "1");
                        String r = z.getOut();
                        sbBuffer.append(r);
                        //foundresult=z.isFoundresult();
						try{
                        if(!foundresult){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
							   
							    String syn = iwordSynonims.get(nword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
	
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();
										 z = getQobjects(newnword, "2");
				                         r = z.getOut();
				                         //foundresult=z.isFoundresult();
				                         sbBuffer.append(r);
				                        
									}
								}
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
					try{	if(!foundresult){
							
							ArrayList tempdoList = null;
							tempdoList = iSymantex.get(nword);
							if (tempdoList != null) {
								int j = 0;
								for (Object value : tempdoList) {
									j++;
									String[] index = new String[2];
									index = (String[]) value;

									String iSymantexword = index[0];
									String itype = index[1];
									iSymantexword = iSymantexword.trim();
									// надо - выйти из цикла
									 //if(foundresult)break;
									
									 z = getQobjects(iSymantexword, "3");
			                         r = z.getOut();
			                         //foundresult=z.isFoundresult();
			                         sbBuffer.append(r);

									}
								}
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
					//vword - найти контекст глаголов 
					boolean foundverb=false;
					 sbBuffer.append(" [verb=").append(vword).append("]");
					try{
					 String varbcat = verbglobalcategory.get(vword);
					 if(varbcat!=null){
					 if(!varbcat.equals("")){	 
						 //foundverb=true;
						 sbBuffer.append(" [varbcat=").append(varbcat).append("]");
					 }
					 }
					 
					 
		    		} catch (Exception e) {
		    			e.printStackTrace();
		    		}
	    			 
	    			 
					try{
                        if(!foundverb){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
							   
							    String syn = iwordSynonims.get(vword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
	
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();
											try{
												 String varbcat = verbglobalcategory.get(newnword);
												 if(varbcat!=null){
												 if(!varbcat.equals("")){	 
													 //foundverb=true;
													 sbBuffer.append("["+newnword+"] [synonims=").append(varbcat).append("]");
												 }
												 }
												 
												 
									    		} catch (Exception e) {
									    			e.printStackTrace();
									    		}
								    			 
				                        
									}
								}
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
					
					try{	if(!foundverb){
						
						ArrayList tempdoList = null;
						tempdoList = iSymantex.get(vword);
						if (tempdoList != null) {
							int j = 0;
							for (Object value : tempdoList) {
								j++;
								String[] index = new String[2];
								index = (String[]) value;

								String iSymantexword = index[0];
								String itype = index[1];
								iSymantexword = iSymantexword.trim();
								// надо - выйти из цикла
								
								try{
									 String varbcat = verbglobalcategory.get(iSymantexword);
									 if(varbcat!=null){
									 if(!varbcat.equals("")){	 
										 //foundverb=true;
										 sbBuffer.append("["+iSymantexword+"] [synonims=").append(varbcat).append("]");
									 }
									 }
									 
									 
						    		} catch (Exception e) {
						    			e.printStackTrace();
						    		}
		                       

								}
							}
					}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
	    			 
	    			 
	    			
	    		}
	    	}
	    	result=sbBuffer.toString();
	    }catch(Exception e){}
		System.out.println("result="+result);
	    return result;
	}
	
	public Map<String, List> searchRootObject(List<String> qindexList, Map<String, List> iilobjectsList,
			Map<String, List> iilinstanceList, Map<String, List> iilsubclassofList, Map<String, List> iilproperties,
			Map<String, List> iilmaincategory, Map<String, List> iiltext, Map<String, List> iilhaspart) {

		Map<String, List> returndata = new HashMap<String, List>();

		Map<String, String> qgeo = fastMemoryModel.getCheckIfGeo();
		Map<String, String> qhuman = fastMemoryModel.getCheckIfHuman();
		Map<String, String> qorg = fastMemoryModel.getCheckIfOrg();
		String itypeid = "";
		String icategoryid = "";
		String savetypeid = "none";
		String savecategoryid = "";
		String itogo = "";
		String[] res = new String[2];
		res[0] = "";
		res[1] = "";

		boolean sgeo = false;
		boolean shuman = false;
		boolean sorg = false;
		if (sgeo == false && shuman == false && sorg == false) {
			// ----------------------------Q----------------------------
			// https://dumps.wikimedia.org/wikidatawiki/entities/
			boolean canexit = false;
			// список Q полученных по слову
			if (qindexList != null)
				for (String qindex : qindexList) {
					if (canexit)
						break;
					List<String> resultThree = new ArrayList<String>();

					if (qindex != null) {
						Map<String, String> viewed = new HashMap<String, String>();

						// StringBuffer sb=new StringBuffer();

						boolean doit = true;
						int count = 0;
						do {
							if (canexit)
								break;
							count++;
							if (count > 100)
								break;
							String preview = qindex;
							// думали об этом Q уже?
							if (!viewed.containsKey(qindex)) {

								List<String> vi = new ArrayList<String>();
								vi.add(qindex);

								int icount = 0;
								do {
									if (canexit)
										break;

									// System.out.println("--------vi.get-----="+vi.toString());
									// System.out.println(icount+"--------vi.get-----="+vi.get(0));
									String qdata = vi.get(0);
									icount++;
									resultThree.add(qdata);
									// String words=getTextofQ(first, iiltext);
									// String wordsprop=getPropertyOfType(qdata,
									// iilproperties);
									// List<String> otherPropList=
									// iilproperties.get(qdata);
									// if(otherPropList!=null)
									// for(String c:otherPropList){
									//
									// }

									if (icount > 1000)
										break;
									if (!viewed.containsKey(qdata)) {
										List<String> listofinstance = null;
										List<String> listsubclassof = null;
										List<String> listotherprops = null;
										List<String> listhaspart = null;
										String label = "";
										savetypeid = qdata;

										listofinstance = iilinstanceList.get(qdata);
										listsubclassof = iilsubclassofList.get(qdata);
										listotherprops = iilproperties.get(qdata);
										listhaspart = iilhaspart.get(qdata);
										// System.out.println(icount+"--------listofinstance-----="+listofinstance);
										// System.out.println(icount+"--------listsubclassof-----="+listsubclassof);

									
										// if(iorclass==1){
										// listofinstance =
										// getListInstance(qdata,
										// iilinstanceList);label="ofinstance";
										// listsubclassof =
										// getListInstance(qdata,
										// iilsubclassofList);label="subclassof";
										// }

										if (listhaspart != null) {

											for (String ins : listhaspart) {
												vi.add(ins);

												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}

											}
										}

										if (listotherprops != null) {

											for (String ins : listotherprops) {
												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}

											}
										}
										if (listofinstance != null) {
											for (String ins : listofinstance) {
												// System.out.println("--------ofinstance-----="+ins);
												vi.add(ins);
												// String
												// lwordsprop=getPropertyOfType(ins,
												// iiltype);
												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}
											}
										}
										if (listsubclassof != null) {
											for (String ins : listsubclassof) {
												// System.out.println("--------subclassof-----="+ins);
												vi.add(ins);
												// String
												// lwordsprop=getPropertyOfType(ins,
												// iiltype);
												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}
											}
										}

										// if(listhaspart!=null){
										//
										// for(String ins:listhaspart){
										// vi.add(ins);
										//
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										//
										// }
										// }
										//
										// if(listotherprops!=null){
										//
										// for(String ins:listotherprops){
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										//
										// }
										// }
										// if(listofinstance!=null){
										// for(String ins:listofinstance){
										// //System.out.println("--------ofinstance-----="+ins);
										// vi.add(ins);
										// //String
										// lwordsprop=getPropertyOfType(ins,
										// iiltype);
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										// }
										// }
										// if(listsubclassof!=null){
										// for(String ins:listsubclassof){
										// //System.out.println("--------subclassof-----="+ins);
										// vi.add(ins);
										// //String
										// lwordsprop=getPropertyOfType(ins,
										// iiltype);
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										// }
										// }
										// String
										// valuesCategory=getCategoryText(qdata,
										// iilmaincategory, iiltext);
										// returndata.put(qdata,
										// ""+icount+"#"+valuesCategory);
										vi.remove(qdata);
									}

								} while (vi.size() > 0);

								if (preview == qindex)
									doit = false;

								if (qindex == null)
									doit = false;
								// returndata.put(qindex, ""+icount);
								// String valuesCategory=getCategoryText(qindex,
								// iilmaincategory, iiltext);
								// returndata.put(qindex,
								// ""+icount+"u"+valuesCategory);
								viewed.put(qindex, qindex);
							}

						} while (doit);

						// itogo = sb.toString();
					}
					returndata.put(qindex, resultThree);
				}
		}

		List<String> resultsavetypeid = new ArrayList<String>();
		resultsavetypeid.add(savetypeid);
		returndata.put("1", resultsavetypeid);
		// returndata.put("2", itogo);
		// res[0]=savetypeid;
		// res[1]=itogo;
		return returndata;
	}
	
	//=======================debug===========================================
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/v1/nlp/phrase/{cmd}")
	public Map<String, Integer> getPhraseSortedToken(@PathVariable String cmd) {
	    try{ 
	    
	    	String path="/mindscan/home/trustsys/data/";
	    	
	    	    Map<String, String> probabilitycounter = new HashMap<String, String>();probabilitycounter = nameFinderModelMEDAO.getPhraseCounterMap();
			    
	    	    Map<String, Integer> probabilitymain=new HashMap<String, Integer>(); probabilitymain=nameFinderModelMEDAO.getPhraseTokenMapValue();
			    
	    	    Map<String, String> verb = new HashMap<String, String>(); verb = nameFinderModelMEDAO.getNounVerbMap();
	    	    
	    	    Map<String, Integer> returnmap = new HashMap<String, Integer>();
			    
	    	    
	    	    Map<String, Integer> verbprobabilitymain=new HashMap<String, Integer>(); verbprobabilitymain=fastMemoryModel.getObjectAndVerbCount();
			    
			    //Map<String, Integer> probabilitymain=new HashMap<String, Integer>();
			    //probabilitymain.putAll(main);
			    
			   //cmd
			    //1-N показать объем об.доков 1|1|1
			    //2-2 показать токены до Н штук с сортировкой 2|110|1
			    //3-1-2 показать токены до Н штук с сортировкой и типом токена 3|110|2
			    //4-посчитать вероятн
			    //5-сериализовать документ-сбросить на диск
			    //6-десериализовать документ-сбросить на диск
			    //7-2-20 показать токены лимитом 2 и значение которых до Н штук 7|2|20
			    //8-каунт бробаблити для фраз
			    //9-создать словарь слов
			    
			     int exec=0;
			     String a="";
			     String b="";
				 if(cmd.contains("|")){
					 String[] objectidData = cmd.split("\\|");
					 exec=Integer.parseInt(objectidData[0]);
					 a=objectidData[1];
					 b=objectidData[2];
				 }
				if(exec==1){
							returnmap.put(probabilitymain.size()+":tokens and docs:",probabilitycounter.size());
							return returnmap;
				}
				
				if(exec==2){
				int i =Integer.parseInt(a);
				
				int ii=i;
				if(ii>30000) ii=10000;
	    	
	    	
			    	Object[] ab = probabilitymain.entrySet().toArray();
			    	Arrays.sort(ab, new Comparator() {
			    	    public int compare(Object o1, Object o2) {
			    	        return ((Map.Entry<String, Integer>) o2).getValue()
			    	                   .compareTo(((Map.Entry<String, Integer>) o1).getValue());
			    	    }
			    	});
			    	
			    	int k=0;
			    	for (Object e : ab) {
			    	    //System.out.println(((Map.Entry<String, Integer>) e).getKey() + " : " + ((Map.Entry<String, Integer>) e).getValue());
			    	    
			    	     returnmap.put(((Map.Entry<String, Integer>) e).getKey(), ((Map.Entry<String, Integer>) e).getValue());
					     k++;
					     if(k>ii) break; 
			    	}
		
			    	return returnmap;
				}
				
				if(exec==3){
				int i =Integer.parseInt(a);
				int j =Integer.parseInt(b);
				int ii=i;
				if(ii>30000) ii=10000;
	    	
	    	
			    	Object[] ab = probabilitymain.entrySet().toArray();
			    	Arrays.sort(ab, new Comparator() {
			    	    public int compare(Object o1, Object o2) {
			    	        return ((Map.Entry<String, Integer>) o2).getValue()
			    	                   .compareTo(((Map.Entry<String, Integer>) o1).getValue());
			    	    }
			    	});
			    	
			    	int k=0;
			    	for (Object e : ab) {
			    		String tk=((Map.Entry<String, Integer>) e).getKey();
			    		int tokentype = countWords(tk);
			    		 
			    		 if(tokentype==j){
			    	     returnmap.put(tk, ((Map.Entry<String, Integer>) e).getValue());
					     
			    	     k++;
					     if(k>ii) break; }
			    		 if(k>1000000) break;
			    	}
		
			    	return returnmap;
				}
				
				if(exec==5){
					
					String name=a;
					saveMe(probabilitycounter, path+name+"c");
					saveMe(probabilitymain, path+name);
					

				}
				if(exec==6){
					
					String name=a;
				
					nameFinderModelMEDAO.setProbabilitycounterMap(loadMeCounter(path+name+"c"));
					
					
					
					Map<String, Integer> map = loadMeToken(path+name);
					Map<String, Integer> apObject = new ConcurrentHashMap<String, Integer>(map);
					//apObject.putAll(map);
					
					nameFinderModelMEDAO.setTokenMapValue(apObject);
				}
				if(exec==7){
					int k=0;
					int i =Integer.parseInt(a);
					int j =Integer.parseInt(b);
					int ii=i;
					if(ii>30000) ii=10000;
					
					
					 for (Map.Entry<String, Integer> entry : probabilitymain.entrySet())
					 {
						 
						 	 if(entry.getValue()<j){
						     returnmap.put(entry.getKey(), entry.getValue());
						     k++;
						     if(k>ii) break; }
						 	 
						 	 if(k>1000000) break;
					     
					 }
					 return returnmap;
				}
				if(exec==10){
					
					int val=0;
					try{val=probabilitymain.get(a);returnmap.put(a, val);}catch(Exception e){returnmap.put("0", val);}
					
				
					return returnmap;	
				}
				if(exec==11){
					try{
					int j =Integer.parseInt(b);
					double mainsize=(double) probabilitymain.size();
					double docsize =(double) probabilitycounter.size();
					double maintotal=1;
					double doctotal=1;
					if(j==1){
					
					int z=0;
					try{z=probabilitymain.get(a);}catch(Exception e){z=0;}
					
					if(z>0){//Если фраза найдена
						System.out.println("z="+z);
						System.out.println("mainsize="+mainsize);
						System.out.println("docsize="+docsize);
					//count(w,c)+1/probabilitymain.size()    - частота выражения среди выражений
					//count(w,c)+1/probabilitycounter.size() - частота выражения среди документов
						
						maintotal=(z+1)/mainsize;
						doctotal=(z+1)/docsize;
						returnmap.put(doctotal+":"+ maintotal,1);	
						return returnmap;
						
						
					}else{
					//Если фраза не найдена
					//считаем ее пословарно и выполняем произведение
					String dirty=a;
			    	try{dirty=a.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
			    	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
				    String tokens[] = dirty.toString().split("\\p{P}?[ \\t\\n\\r]+");
					
					int size = tokens.length;
					
					for(int i=0;i<size;i++){
						String single=tokens[i].trim();
						if(!single.equals("")){ 
							int val=0;
							try{val=probabilitymain.get(single);}catch(Exception e){val=0;}
							
							maintotal=maintotal*((val+1)/mainsize);
							doctotal=doctotal*((val+1)/docsize);
						}
					}
					returnmap.put(doctotal+":"+ maintotal,1);
					}
					}
					else if(j==2){
						
						String dirty=a;
				    	try{dirty=a.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
				    	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
					    String tokens[] = dirty.toString().split("\\p{P}?[ \\t\\n\\r]+");
					    List<String> alltokens = new  ArrayList<String>();
						int size = tokens.length;
						
						for(int i=0;i<size;i++){
							String indexsingle = "";
							String indexbigram = "";
							String indextrigram = "";
							String indexfourgram = "";
							String indexfivegram = "";
							
							String single=null;
							String bigram=null;
							String trigram=null;
							String fourgram=null;
							String fivegram=null;
							
							single=tokens[i];
							indexsingle = single;
							
							if(i+1<size){
							bigram=tokens[i]+" "+tokens[i+1];
													
							}
							if(i+2<size){
							trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
														
							}
							if(i+3<size){
							fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
													    
							}
							if(i+4<size){
								fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
								
							}
														
							if(single!=null)
								alltokens.add(indexsingle);
								if(bigram!=null)
									alltokens.add(indexbigram);
									if(trigram!=null)
										alltokens.add(indextrigram);
										if(fourgram!=null)
											alltokens.add(indexfourgram);
											if(fivegram!=null)
												alltokens.add(indexfivegram);
						
											
									          if(i>3500){
									        	  break;
									          }	
						}
						
						for (String wordtokend: alltokens) { 
							
							String single=wordtokend.trim();
							
							if(!single.equals("")){ 
								int val=0;
								try{val=probabilitymain.get(single);}catch(Exception e){val=0;}
								
								maintotal=maintotal*((val+1)/mainsize);
								doctotal=doctotal*((val+1)/docsize);
							}
						}
						returnmap.put(doctotal+":"+ maintotal,1);
						
					}
				}catch(Exception e){	e.printStackTrace();}
					return returnmap;
				}
	    		
				
				if(exec==12){		 
					Map<String, String> singlemap = new HashMap<String, String>();
					int k=0;
					int i =Integer.parseInt(a);
					int j =Integer.parseInt(b);
					int ii=i;
					if(ii>30000) ii=10000;
					
					
					 for (Map.Entry<String, Integer> entry : probabilitymain.entrySet())
					 {
						 String key=entry.getKey().trim();
						 if(key.contains(" ")){
								String[] myData = key.split(" ");
								
								if(singlemap.containsKey(myData[0])){
									
									String val=singlemap.get(myData[0]);
									val=val+"|"+myData[1];
									singlemap.put(myData[0], val);
								}
								else{
									singlemap.put(myData[0], myData[1]);
								}
						 }
						 		
						     k++;
						     
						 	 
						 	 if(k>10000000) break;
					     
					 }
					 
					 nameFinderModelMEDAO.setNounVerbMap(singlemap);
					 returnmap.put("List of verb - created", 1);
					 return returnmap;
					
				} 
				
				
				if(exec==14){
					int k=0;
					int i =Integer.parseInt(a);
					int j =Integer.parseInt(b);
					int ii=i;
					if(ii>30000) ii=10000;
					
					
					 for (Map.Entry<String, String> entry : verb.entrySet())
					 {
						 
						 	 
						     returnmap.put(entry.getKey()+"|"+ entry.getValue(),14);
						     k++;
						     if(k>ii) break; 
						 	 
						 	 if(k>1000000) break;
					     
					 }
					 return returnmap;
				}
				
				if(exec==15){
					
					String name=a;
					//saveMe(probabilitycounter, path+name+"c");
					saveMe(verb, path+name);
					

				}
				if(exec==16){
					
					String name=a;
				
					nameFinderModelMEDAO.setProbabilitycounterMap(loadMeCounter(path+name+"c"));
					
					
					
					Map<String, Integer> map = loadMeToken(path+name);
					Map<String, Integer> apObject = new ConcurrentHashMap<String, Integer>(map);
					//apObject.putAll(map);
					
					nameFinderModelMEDAO.setTokenMapValue(apObject);
				}
				 
				//===============================ObjectAndVerb===========================
				if(exec==20){
				
				
				   	int ab = verbprobabilitymain.size();
				   	returnmap.put("current verb phrase size=", ab);
			    	return returnmap;
				}
				
				if(exec==21){
					
					Map<String, Map<String, Integer>>	verbList=fastMemoryModel.getObjectAndVerbList();
				   	int ab = verbList.size();
				   	returnmap.put("verb size=", ab);
			    	return returnmap;
				}
				
				
				if(exec==22){
				int i =Integer.parseInt(a);
				
				int ii=i;
				if(ii>30000) ii=10000;
				
				   
			    	Object[] ab = verbprobabilitymain.entrySet().toArray();
			    	Arrays.sort(ab, new Comparator() {
			    	    public int compare(Object o1, Object o2) {
			    	        return ((Map.Entry<String, Integer>) o2).getValue()
			    	                   .compareTo(((Map.Entry<String, Integer>) o1).getValue());
			    	    }
			    	});
			    	
			    	int k=0;
			    	for (Object e : ab) {
			    	    //System.out.println(((Map.Entry<String, Integer>) e).getKey() + " : " + ((Map.Entry<String, Integer>) e).getValue());
			    	    
			    	     returnmap.put(((Map.Entry<String, Integer>) e).getKey(), ((Map.Entry<String, Integer>) e).getValue());
					     k++;
					     if(k>ii) break; 
			    	}
		
			    	return returnmap;
				}
				
				if(exec==23){
					 Map<String, Map<String, Integer>>	verbList=fastMemoryModel.getObjectAndVerbList();
					 int i =Integer.parseInt(a);
					 int d =Integer.parseInt(b);
					 int j=0;
						
						 for (Map.Entry<String, Map<String, Integer>> entry : verbList.entrySet())
						 {
							 String key=entry.getKey();
							 Map<String, Integer> value=entry.getValue();
							 
							 StringBuffer sb=new StringBuffer();
							 int total=0;
							 for (Map.Entry<String, Integer> val : value.entrySet())
							 {
								 
								String k= val.getKey();
								int v= val.getValue();
								sb.append(k).append("-").append(v).append("^");
								total=total+v;
							 }
							 if(d<total){
						     returnmap.put(key+"|"+sb, total);
						     j++;
						     if(j>i){break;}
						     }
						 }
						
						
					
						return returnmap;
				}
				if(exec==24){
					 Map<String, Map<String, Integer>>	verbList=fastMemoryModel.getObjectAndVerbList();
					
						
							 
							 Map<String, Integer> value=verbList.get(a);
							 if(value!=null){
							 StringBuffer sb=new StringBuffer();
							 int total=0;
							 for (Map.Entry<String, Integer> val : value.entrySet())
							 {
								 
								String k= val.getKey();
								int v= val.getValue();
								sb.append(k).append("-").append(v).append("^");
								total=total+v;
							 }
							 
						     returnmap.put(a+"|"+sb, total);
						     }
						
						
						
					
						return returnmap;
				}
				//===============================ObjectRelation===========================
				if(exec==30){
					
					Map<String, Integer> relationmain=new HashMap<String, Integer>();
				    relationmain=fastMemoryModel.getObjectRelationCount();
				   	int ab = relationmain.size();
				   	returnmap.put("relationmain size=", ab);
			    	return returnmap;
				}
				
				if(exec==31){
					
					Map<String, Map<String, Integer>>	verbList=fastMemoryModel.getObjectRelationList();
				   	int ab = verbList.size();
				   	returnmap.put("objectRelation size=", ab);
			    	return returnmap;
				}
				if(exec==32){
				int i =Integer.parseInt(a);
				
				int ii=i;
				if(ii>30000) ii=10000;
				    Map<String, Integer> relationmain=new HashMap<String, Integer>();
				    relationmain=fastMemoryModel.getObjectRelationCount();
			    	Object[] ab = relationmain.entrySet().toArray();
			    	Arrays.sort(ab, new Comparator() {
			    	    public int compare(Object o1, Object o2) {
			    	        return ((Map.Entry<String, Integer>) o2).getValue()
			    	                   .compareTo(((Map.Entry<String, Integer>) o1).getValue());
			    	    }
			    	});
			    	
			    	int k=0;
			    	for (Object e : ab) {
			    	    //System.out.println(((Map.Entry<String, Integer>) e).getKey() + " : " + ((Map.Entry<String, Integer>) e).getValue());
			    	    
			    	     returnmap.put(((Map.Entry<String, Integer>) e).getKey(), ((Map.Entry<String, Integer>) e).getValue());
					     k++;
					     if(k>ii) break; 
			    	}
		
			    	return returnmap;
				}
				
				if(exec==33){
					 Map<String, Map<String, Integer>>	verbList=fastMemoryModel.getObjectRelationList();
					 int i =Integer.parseInt(a);
					 int d =Integer.parseInt(b);
					 int j=0;
						
						 for (Map.Entry<String, Map<String, Integer>> entry : verbList.entrySet())
						 {
							 String key=entry.getKey();
							 Map<String, Integer> value=entry.getValue();
							 
							 StringBuffer sb=new StringBuffer();
							 int total=0;
							 for (Map.Entry<String, Integer> val : value.entrySet())
							 {
								 
								String k= val.getKey();
								int v= val.getValue();
								sb.append(k).append("-").append(v).append("^");
								total=total+v;
							 }
							 if(d<=total){
						     returnmap.put(key+"|"+sb, total);
						     j++;
						     if(j>i){break;}
						     }
						 }
						
						
					
						return returnmap;
				}
				if(exec==34){
					 Map<String, Map<String, Integer>>	verbList=fastMemoryModel.getObjectRelationList();
					
						
							 
							 Map<String, Integer> value=verbList.get(a);
							 if(value!=null){
							 StringBuffer sb=new StringBuffer();
							 int total=0;
							 for (Map.Entry<String, Integer> val : value.entrySet())
							 {
								 
								String k= val.getKey();
								int v= val.getValue();
								sb.append(k).append("-").append(v).append("^");
								total=total+v;
							 }
							 
						     returnmap.put(a+"|"+sb.toString(), total);
						     }
						
						
						
					
						return returnmap;
				}
				
				
				if(exec==50){
				 int iorclass = 0;
						 
						  try{iorclass = Integer.parseInt(b);} catch(Exception e){e.printStackTrace();}
						  
				 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
				 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
				 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
				 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
				 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
				 
				 String itogo="s";returnmap.put("a", 1);
				 
				 
				 try{
					 
					 //for()
					 List<String> qindexList = iiltextList.get(a); returnmap.put("1", 1);
					 if(qindexList!=null)
					 for(String qindex:qindexList){
					 
					 if(qindex!=null){ returnmap.put(qindex, 1);
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            System.out.println("--------search-----="+vi.get(0));
						            String first=vi.get(0);
						            icount++;
						            String words=getTextofQ(first, iiltext);
						            String wordsprop=getPropertyOfType(first, iiltype);
						            
						            
						            if(words!=null)
						                sb.append("search=").append(first).append("-").append(wordsprop).append("-").append(words);
						            else
						            	sb.append("search=").append("-").append(wordsprop).append(first);
						            
						            
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(first)){
						            	List<String> list =null;
						            	String label="";
						            	if(iorclass==1){
						            		list = getListInstance(first, iilinstanceList);
						            		label="ofinstance";
						            		}
						            	if(iorclass==2){
						            		list = getListInstance(first, iilsubclassofList);
						            		label="subclassof";
						            		}
						            if(list!=null){
							            for(String ins:list){
							            	System.out.println("--------ofinstance-----="+ins);
							            	  vi.add(ins);
							            	  String lwords=getTextofQ(ins, iiltext);
							            	  String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(lwords!=null)
							            		  sb.append(label+"=").append(ins).append("-").append(lwordsprop).append("-").append(words);
							            	  else 
							            		  sb.append(label+"=").append(ins).append("-").append(lwordsprop);  
							            }
						            }
						             vi.remove(first);
						            }
						            
						            
						            } while (vi.size()>0);
						            System.out.println("--------EXIT-----=");      
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					   itogo=sb.toString();
					 }
					 returnmap.put(itogo, 1);}
				} catch(Exception e){e.printStackTrace();}
			    	return returnmap;
				}
				
				
				if(exec==51){
				Map<String, String> iwordSynonims=fastMemoryModel.getWordSynonims();	
				String lowerStr=a.toLowerCase();
				StringBuffer sb=new StringBuffer();
//				 for (Map.Entry<String, String> entry : iwordSynonims.entrySet())
//				 {
//					 String key=entry.getKey();
//					 String value=entry.getValue();
//					 
//					 sb.append("key=").append(key).append(" value="+value);
//					 
//				 }
				 
				try{
				String syn=iwordSynonims.get(lowerStr);
				String[] newwords;
		  		if(syn.contains("/")){
		  			
		  			newwords = syn.split("\\/");
	          	
	          	}else{
	          		
	          		String[] synindex = new String[1];
	          		synindex[0]=syn;
	          		newwords = synindex;
	          	}
		  		int siz = newwords.length;
				 
				for(int ik=0;ik<siz;ik++){
					sb.append(newwords[ik]);
					sb.append("#");
				}
				}catch(Exception e){e.printStackTrace();} 
				 String itogo=sb.toString();
				 returnmap.put(itogo, 1);
				 return returnmap;
				}
				if(exec==52){
					Map<String, ArrayList> iSymantex=nameFinderModelMEDAO.getSymantexhashMap();
					 StringBuffer sb=new StringBuffer();
					String lowerStr=a.toLowerCase();	
					ArrayList tempdoList = null;
			        tempdoList = iSymantex.get(lowerStr); int c=0;
			    if(tempdoList !=null){
			    	for (Object value: tempdoList) {c++;
			    		
			    		  String[] index = new String[2];  
			        	  index =(String[]) value;
			        	  
//===================================ГИПЕРОНИМЫ===СИНОНИМЫ==================================================									        	  
			        	  	sb.append(lowerStr).append("-").append(index[0]+" Type : "+index[1]);
			        		  
			            
			    	}
			    }
	    
			     String itogo=sb.toString();
				 returnmap.put(itogo, 1);
				 return returnmap;
					
				}
				if(exec==53){
					 Map<String, String> iwordPositive=fastMemoryModel.getWordPositive();
					 Map<String, String> iwordNegative=fastMemoryModel.getWordNegative();
					 Map<String, String> morfema=fastMemoryModel.getMorfema();
					 Map<String, String> morfemawords=fastMemoryModel.getMorfemaWords();
					 LuceneMorphology luceneMorph = objs.getLuceneMorph();
					 
					 
					 List<String> wordBaseForms = luceneMorph.getMorphInfo(a);
					 String wordform="";
					 String nword ="";
						int sizer=0;
						try{sizer=wordBaseForms.size();}catch(Exception e){}
						
						if(sizer>0){
						
						StringBuilder sb = new StringBuilder();
						for (String s : wordBaseForms)
						{
						    sb.append(s);
						    sb.append(" #");
						}
						
						
						String ist = sb.toString();
						if(!ist.equals("")&&ist!=null){
							wordform=ist;
						}
						
					
					
						if(wordform.contains("|")){
							   String[] myData = wordform.split("\\|");
							   nword =myData[0].trim();
							   String morf =myData[1];}
						}
						
					   String m= morfema.get(nword); 
					   StringBuffer array = new StringBuffer();
					   if(m.contains("|")){
						   String[] mdata = m.split("\\|");   
						    int sizeb = mdata.length;
							for(int i=0;i<sizeb;i++){
								String single=mdata[i];
								 
								
								try{ String same=morfemawords.get(single);
								     array.append("words:"+single+" ["+same+"] because:");
								     
								     String pos=iwordPositive.get(single);
								     if(pos!=null){
								    	 array.append(nword+"["+single+"]").append("-POS ");
								    	 //wordjson.put("pos", true);
								    	 //synindex[0]=single;
								     }
								     }catch(Exception e){e.printStackTrace();}
								     
								try{ 
									     String neg=iwordNegative.get(single);
									     if(neg!=null){
									    	 array.append(nword+"["+single+"]").append("-NEG ");
									    	 //synindex[1]=single;
									     }
								}catch(Exception e){e.printStackTrace();} 
							}
							//wordjson.put("emo", array.toString());
							//synindex[2]= array.toString();
					   }else{
						   
						   try{ 
						     String same=morfemawords.get(m);
						     array.append("words:"+m+" ["+same+"] because:");
						     
							     String pos=iwordPositive.get(m);
							     if(pos!=null){
							    	 array.append(nword+"["+m+"]").append("-POS ");
							    	 //wordjson.put("emopos", m);
							    	 //wordjson.put("pos", true);
							    	 //synindex[0]=m;
							     }
							     }catch(Exception e){e.printStackTrace();}
							     
							try{ 
								     String neg=iwordNegative.get(m);
								     if(neg!=null){
								    	 array.append(nword+"["+m+"]").append("-NEG ");
								    	 //wordjson.put("emoneg", m);
								    	 //wordjson.put("neg", true);
								    	// synindex[1]=m;
								     }
							}catch(Exception e){e.printStackTrace();} 
					   }
					   
					     String itogo=array.toString();
						 returnmap.put(itogo, 1);
						 return returnmap;
				}
				if(exec==55){
					 Map<String, String> iwordPositive=fastMemoryModel.getWordPositive();
					 Map<String, String> iwordNegative=fastMemoryModel.getWordNegative();
					 StringBuilder sb = new StringBuilder();
					 for (Map.Entry<String, String> entry : iwordPositive.entrySet())
					 {
						 String key=entry.getKey();
						 String value=entry.getValue();
						 if(iwordNegative.containsKey(key)){
							 sb.append("same["+key+"]"+value+"-"+iwordNegative.get(key)+"#");	 
						 }
					 
					 }
					 String itogo=sb.toString();
					 returnmap.put(itogo, 1);
					 return returnmap;	
				}
				if(exec==57){
					 StringBuffer sbt=new StringBuffer();
//					String lowerStr=a.toLowerCase();
//					 Map<String, List> iilobjectsList=fastMemoryModel.getIilobjects();
					 Map<String, List> iilmaincategory=fastMemoryModel.getMaincategory();
					 int i=0;
					 for (Map.Entry<String, List> entry : iilmaincategory.entrySet())
					 {
					     System.out.println(entry.getKey() + "/" + entry.getValue().toString());
					     sbt.append(entry.getKey() + "/" + entry.getValue().toString());
					     for(Object qindex:entry.getValue()){
					    	 String d=(String) qindex;
					    	 System.out.println("d="+d);
					     }
					     i++;
					     if(i>10)break;
					 }
					 
					 
//				     List<String> qindexList = iilobjectsList.get(lowerStr);
//				     int f=0;
//				  
//					 if(qindexList!=null)
//						 for(String qindex:qindexList){
//							 f++;
//						    
//				  	         try{List<String> list= iilmaincategory.get(qindex);
//				    		 for(String c:list){
//				    			 
//				    			 sbt.append("[").append(c).append("]");
//				    				
//				    		 }}catch(Exception e){}
//						 }
//					 
//					 
					 
					 returnmap.put(sbt.toString(), fastMemoryModel.getMaincategory().size());	
						return returnmap;
				}
				
				//===============================EXPLAIN===========================
				if(exec==60){
					
					Map<String, String> relationmain=new HashMap<String, String>();
				    relationmain=fastMemoryModel.getVonrosiPrimeri();
				   	int ab = relationmain.size();
				   	returnmap.put("Voprosi size=", ab);
			    	return returnmap;
				}
				if(exec==61){
					 Map<String, String>	verbList=fastMemoryModel.getVonrosiPrimeri();
					 int i =Integer.parseInt(a);
					 int d =Integer.parseInt(b);
					 int j=0;
						
						 for (Map.Entry<String, String> entry : verbList.entrySet())
						 {
							 String key=entry.getKey();
							 String value=entry.getValue();
							 int k=1;
							 try{ k=Integer.parseInt(value);} catch(Exception e){}
							 
							 if (d==0){ 
								 returnmap.put(key, k); j++;}
							 else if(d==k){ 
								 returnmap.put(key, k); j++;}
						    
						     if(j>i){break;}
						     
						 }
						
						
					
						return returnmap;
				}
				if(exec==62){fastMemoryModel.getVonrosiPrimeriCleare();}
				if(exec==70){
					
					Map<String, String> relationmain=new HashMap<String, String>();
				    relationmain=fastMemoryModel.getExplainPrimeri();
				   	int ab = relationmain.size();
				   	returnmap.put("Explain size=", ab);
			    	return returnmap;
				}
				if(exec==71){
					 Map<String, String>	verbList=fastMemoryModel.getExplainPrimeri();
					 int i =Integer.parseInt(a);
					 int d =Integer.parseInt(b);
					 int j=0;
						
						 for (Map.Entry<String, String> entry : verbList.entrySet())
						 {
							 String key=entry.getKey();
							 String value=entry.getValue();
						
						     returnmap.put(key, 1);
						     j++;
						     if(j>i){break;}
						     
						 }
						
						
					
						return returnmap;
				}
				if(exec==72){fastMemoryModel.getExplainPrimeriCleare();}
				
				//===============================EXPLAIN===========================
				
				
				
//	    		ListIterator<String> iter = new ArrayList<>(map.keySet()).listIterator(map.size());
//	    		while (iter.hasPrevious()) {
//	    		    String key = iter.previous();
//	    		    returnmap.put(key,map.get(key));
//	    		    k++;
//	    		    if(k>ii) break;
//	    		}
	    		
	    		
//				 for (Map.Entry<String, Integer> entry : map.entrySet())
//				 {
//					     returnmap.put(entry.getKey(), entry.getValue());
//					     k++;
//					     if(k>ii) break; 
//				     
//				 }
		 
		 	
		 
	    }
	    	catch(Exception e){e.printStackTrace();}
	    
	    
	    return null;
	}
	 

	
		public String getCategoryText(String idx, Map<String, List> iilmaincategory,  Map<String, List> iilqtext){
		
			String ofinstance =null;
		  	  try{ 
			    		 List<String> list= iilmaincategory.get(idx);
			    		 StringBuffer sb=new StringBuffer();
			    		 String begin="C[";
			    		 for(String c:list){
			    				String words=getTextofQ(c, iilqtext);  
			    			    sb.append(c).append("|"+words+"|").append("-"); 
			    		 }
			    		 String end="]";
			    		 ofinstance = begin+sb.toString()+end; 
				  }catch(Exception e){}
		  	if(ofinstance ==null)ofinstance="";
		  	return ofinstance;
				
			} 
	 
	public String getPropertyOfType(String idx, Map<String, List> iiltext){
  	  String ofinstance =null;
  	  try{ 
	    		 List<String> list= iiltext.get(idx);
	    		 StringBuffer sb=new StringBuffer();
	    		 String begin="P[("+idx+")";
	    		 for(String c:list){
	    			 sb.append(c).append("-"); 
	    		 }
	    		 String end="]";
	    		 ofinstance = begin+sb.toString()+end; 
		  }catch(Exception e){}
  	if(ofinstance ==null)ofinstance="";
  	return ofinstance;
		
	}
	
    public String getTextofQ(String idx, Map<String, List> iiltext){
    	  String ofinstance =null;
    	  try{ 
	    		 List<String> list= iiltext.get(idx);
	    		 StringBuffer sb=new StringBuffer();
	    		 for(String c:list){
	    			 sb.append(c).append("-"); 
	    		 }
	    		 ofinstance = sb.toString(); 
		  }catch(Exception e){}
    	 
    	return ofinstance;
    }
    
    public List<String> getListInstance(String idx, Map<String, List> iilinstance){
    	List<String> ofinstance =null;
    	 try{ ofinstance =iilinstance.get(idx);
		 }catch(Exception e){}
    	 
    	return ofinstance;
    }
    
	public int countWords(String s){
		String trim = s.trim();
    if (trim.isEmpty())
        return 0;
    return trim.split("\\s+").length;
    }
	
	
	public void saveMe(Object hmap, String path){
		 try
         {
                FileOutputStream fos =
                   new FileOutputStream(path);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(hmap);
                oos.close();
                fos.close();
                System.out.printf("Serialized HashMap data is saved in hashmap.ser");
         }catch(IOException ioe)
          {
                ioe.printStackTrace();
          }
	}
	
	
	public Map<String, Integer> loadMeToken(String path) throws ClassNotFoundException{
		
		Map<String, Integer> map = null;
	      try
	      {
	         FileInputStream fis = new FileInputStream(path);
	         ObjectInputStream ois = new ObjectInputStream(fis);
	         map = (ConcurrentHashMap) ois.readObject();
	         ois.close();
	         fis.close();
	      }catch(IOException ioe)
	      {
	         ioe.printStackTrace();
	         return null;
	      }
	      
	      return map;
	}
	public Map<String, String> loadMeCounter(String path) throws ClassNotFoundException{
		
		Map<String, String> map = null;
	      try
	      {
	         FileInputStream fis = new FileInputStream(path);
	         ObjectInputStream ois = new ObjectInputStream(fis);
	         map = (HashMap) ois.readObject();
	         ois.close();
	         fis.close();
	      }catch(IOException ioe)
	      {
	         ioe.printStackTrace();
	         return null;
	      }
	      
	      return map;
	}
	
	@RequestMapping("/v1/nlp/morfology/{word}")
	public Message messageWord(@PathVariable String word) {
		String wordform="none"; 
		String normalform="none"; 
		LuceneMorphology luceneMorph = objs.getLuceneMorph();
		
		try{
		
		
		List<String> wordBaseForms = luceneMorph.getMorphInfo(word);
		StringBuilder sb = new StringBuilder();
		for (String s : wordBaseForms)
		{
		    sb.append(s);
		    sb.append(",");
		}
		
		
		String ist = sb.toString();
		if(!ist.equals("")&&ist!=null){
			wordform=ist;
		}
		}catch(Exception e){}
		
		
		
		try{
			
			
			List<String> wordBaseForms = luceneMorph.getNormalForms(word);
			StringBuilder sb = new StringBuilder();
			for (String s : wordBaseForms)
			{
			    sb.append(s);
			    sb.append(" ");
			}
			
			
			String ist = sb.toString();
			if(!ist.equals("")&&ist!=null){
				normalform=ist;
			}
			}catch(Exception e){}
		
		
		Message msg = new Message(word, wordform, normalform);
		
		// RussianAnalyzer russian = new RussianAnalyzer();
		// russian.
		return msg;
	}
	
	@RequestMapping("/v1/nlp/spam/model")
	public String getModel() {
		Config d=new Config();
		return d.getModel();
		
	}
	
	@RequestMapping(value = "/v1/nlp/spam/discover/", method = RequestMethod.POST)
	
	public @ResponseBody SpamTextStatus discover(@RequestBody SpamText text) {
		
		
//		  if (text == null) {
//			  SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty request Body");
//			  return msg;
//		    }
//		  
//		
//		  
//		  
//		try{
//		String id = text.getGuid();
//		String utext = text.getUtftext();
//		if(id!=null&&utext!=null){
//		SpamTextStatus msg = spam.discover(id, utext);	
//		return msg;}
//		else{
//			SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty text");
//			  return msg;
//		}
//		}catch(Exception e){return null;}   
		SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty request Body");
		  return msg;
	}

	
	@RequestMapping(value = "/v1/nlp/object/discover/", method = RequestMethod.POST)
	
	public @ResponseBody FindObjects  discoverObjects(@RequestBody SpamText text) {
		
		
		 if (text == null) {
			  FindObjects result= new FindObjects();
				result.setFindObject(null);
				result.setGuid("");
				result.setMsg("");
				result.setResponse("false");
				result.setText("empty request Body");
				return result;
		    }
		  
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String utext = text.getUtftext();
		if(id!=null&&utext!=null){
		
			List<FindObject> msg = objs.discover(id, utext);	
			FindObjects result= new FindObjects();
			result.setFindObject(msg);
			result.setGuid(id);
			result.setMsg("");
			result.setResponse("true");
			result.setText(utext);
			return result;}
		else{
			FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid(id);
			result.setMsg("empty text");
			result.setResponse("false");
			result.setText("");
			return result;
		}
		}catch(Exception e){e.printStackTrace(); FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid("");
		result.setMsg("");
		result.setResponse("false");
		result.setText("empty request Body");
		return result;}  
	}
//	@RequestMapping(value = "/v1/nlp/object/discover/liniar", method = RequestMethod.POST)
	@RequestMapping(value = "/v1/nlp/object/discover/liniarold", method = RequestMethod.POST)
	public @ResponseBody FindObjects  discoverLinianObjects(@RequestBody SpamText text) {
		
		//postcount++;
		  if (text == null) {
			  FindObjects result= new FindObjects();
				result.setFindObject(null);
				result.setGuid("");
				result.setMsg("");
				result.setResponse("false");
				result.setText("empty request Body");
				return result;
		    }
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String utext = text.getUtftext();
		if(id!=null&&utext!=null){
			int[] level=new int[5];
			 //level[0]=1;
			// level[1]=0;
			// level[2]=1;
			// level[3]=1;
			// level[4]=1;
			 level[0]=1;
			 level[1]=0;
			 level[2]=1;
			 level[3]=0;
			 level[4]=0;
			 Map<Integer, List<String[]>> map = cleanText.getText(level, utext);
			 
			
			 analysis.getRelationsObjectAndVerb(map);
//			 //analysis.thisIsVopros(utext);
//			 
//			 if(logactive){		 
//			 List<String[]> value = null;
//			 for (Map.Entry<Integer, List<String[]>> entrys : map.entrySet())
//			 {
//			    
//				 Integer k = entrys.getKey();
//				 value = entrys.getValue();
//				 System.out.println("k="+k);
//				 for(String[] index:value){
//					 System.out.println("index[0]="+index[0]+" index[1]="+index[1]);
//				 }
//			     
//			 }
//			}
			
			
//			List<FindObject> msg =objs.discoverVerbs(id, utext);	
//			List<FindObject> msg = objs.discoverLiniar(id, utext);	
//------------------------------CREATE EMPTY RESULT-----------------------
//			FindObjects result= new FindObjects();
//			result.setFindObject(msg);
//			result.setGuid(id);
//			result.setMsg("");
//			result.setResponse("true");
//			result.setText(utext);
			
			
			FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("");
			
			
		
		return result;}
		else{
			FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid(id);
			result.setMsg("empty text");
			result.setResponse("false");
			result.setText("");
			return result;
		}
		}catch(Exception e){e.printStackTrace(); 
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid("");
		result.setMsg("");
		result.setResponse("false");
		result.setText("empty request Body");
		return result;}  
	}
	
	
	
@RequestMapping(value = "/v1/nlp/spam/add/", method = RequestMethod.POST)
	
	public @ResponseBody SpamTextStatus add(@RequestBody SpamText text) {
		
		
		  if (text == null) {
			  SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty request Body");
			  return msg;
		    }
		  
		
		  
		  
		try{
		String id = text.getGuid();
		String category = text.getCategory();
		String utext = text.getUtftext();
		// System.out.println("utext="+utext);
		if(id!=null&&utext!=null&&category!=null){
			Person person = new Person();
			person.setAge(12);
			person.setCity(utext);
			person.setGender(category);
			person.setPassword("d");
			 Calendar calendar = Calendar.getInstance();
			    java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
			    person.setDateupdate(ourJavaTimestampObject);
			person.setUsername(id);
			personService.addPerson(person);
			
		
		SpamTextStatus msg = new SpamTextStatus("", "", "", "true", ""); 
		return msg;}
		else{
			SpamTextStatus msg = new SpamTextStatus("", "", "", "false", "empty text");
			  return msg;
		}
		}catch(Exception e){e.printStackTrace(); return null;}  
	}	
	
@RequestMapping(value = "/v1/nlp/object/add/triplet/tonality", method = RequestMethod.POST)

public @ResponseBody FindObjects  updateTonality(@RequestBody Morfematon morfematon) {
	

	
	String aword=null;
	String vword=null;
	String bword=null;
	int awordton=-10;
	int vwordton=-10;
	int bwordton=-10;
	
	try{aword=morfematon.getAword();}catch (Exception e) {}
	try{int aton=morfematon.getAwordton();if(aton==1||aton==-1||aton==2)awordton=aton;}catch (Exception e) {}
	
	try{bword=morfematon.getBword();}catch (Exception e) {}
	try{int bton=morfematon.getBwordton();if(bton==1||bton==-1||bton==2)bwordton=bton;}catch (Exception e) {}
	
	try{vword=morfematon.getVword();}catch (Exception e) {}
	try{int vton=morfematon.getVwordton();if(vton==1||vton==-1||vton==2)vwordton=vton;}catch (Exception e) {}
	
	
	
	morfematonService.morfematonSet(aword, vword, bword, awordton, vwordton, bwordton);

	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid("");
	result.setMsg("");
	result.setResponse("true");
	result.setText("");
	return result;
}

@RequestMapping(value = "/v1/nlp/object/add/word/tonality", method = RequestMethod.POST)

public @ResponseBody FindObjects  updateWordTonality(@RequestBody Morfematon morfematon) {

		String aword=null;
		int awordton=-10;
		String nword=null;
		String vword=null;

		try{aword=morfematon.getAword();}catch (Exception e) {}
		try{int aton=morfematon.getAwordton();if(aton==1||aton==-1||aton==2)awordton=aton;}catch (Exception e) {}
		try{vword=morfematon.getVword();}catch (Exception e) {}
	
		if(awordton==1){vword="positive";}
		if(awordton==-1){vword="negative";}	
		
		nword=aword;
				try{	
					   LuceneMorphology luceneMorph = objs.getLuceneMorph();
					   String wordform="";
					   String singleword=nword.trim().toLowerCase();
					
					   String wrdi=singleword.replaceAll("[^А-я]", "").trim(); 
					 
					   List<String> wordBaseForms = null;
				       if(!"".equals(wrdi)){
							wordBaseForms = luceneMorph.getMorphInfo(wrdi);
						}	
						int sizer=0;
						try{sizer=wordBaseForms.size();}catch(Exception e){}
						
						if(sizer>0){
						
						StringBuilder sb = new StringBuilder();
						for (String s : wordBaseForms)
						{
						    sb.append(s);
						    sb.append(" #");
						}
						
						
						String ist = sb.toString();
						if(!ist.equals("")&&ist!=null){
							wordform=ist;
						}
						
					
					
						if(wordform.contains("|")){
							   String[] myData = wordform.split("\\|");
							   String inword =myData[0].trim();
							   nword=inword;
							   }
						}
				
				
					tonalityService.addTonality(aword, nword, vword);
				}catch (Exception e) {}
				
			FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("true");
			result.setText("");
			return result;
}


@RequestMapping(value = "/v1/nlp/object/discover/linear", method = RequestMethod.POST)

public @ResponseBody FindObjects  discoverLinianOldObjects(@RequestBody SpamText text) {
	
	
	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return result;
	    }
	  
	
	  
	  
	try{
	String id = text.getGuid();
	String utext = text.getUtftext();
	if(id!=null&&utext!=null){
	
		List<FindObject> msg = oldobjs.discoverLiniar(id, utext);	
		
		FindObjects result= new FindObjects();
		//result.setFindObject(msg);
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("");
		result.setResponse("true");
		result.setText(utext);
	
	return result;}
	else{
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("empty text");
		result.setResponse("false");
		result.setText("");
		return result;
	}
	}catch(Exception e){e.printStackTrace(); 
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid("");
	result.setMsg("");
	result.setResponse("false");
	result.setText("empty request Body");
	return result;}  
}


@RequestMapping(value = "/v1/nlp/object/discover/sentece", method = RequestMethod.POST)

public @ResponseBody FindObjects  getSenteceStats(@RequestBody SpamText text) {
	
	//postcount++;
	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return result;
	    }
	  
	
	  
	  
	try{
	String id = text.getGuid();
	String utext = text.getUtftext();
	if(id!=null&&utext!=null){
		int[] level=new int[5];
		 level[0]=1;
		 level[1]=0;
		 level[2]=1;
		 level[3]=1;
		 level[4]=1;
	
		 Map<Integer, List<String[]>> map = cleanText.getText(level, utext);
		 List<String[]> value = map.get(3);
		 List<FindObject> msg = new ArrayList<FindObject>();
		 for(String[] index:value){
			 
			 FindObject on= new FindObject();
			 on.setId("1");
			 on.setName(index[0]);
			 on.setStrpos(index[1]);
			 
			msg.add(on);
		 }
		FindObjects result= new FindObjects();
		
		result.setFindObject(msg);
		result.setGuid(id);
		result.setMsg("");
		result.setResponse("true");
		result.setText("");
	
		
	
	return result;}
	else{
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("empty text");
		result.setResponse("false");
		result.setText("");
		return result;
	}
	}catch(Exception e){e.printStackTrace(); 
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid("");
	result.setMsg("");
	result.setResponse("false");
	result.setText("empty request Body");
	return result;}  
}
public boolean hasVoprosSymbol(String str) {
	boolean t=false;
    if (str != null && str.length() > 0) {
      str = str.substring(str.length()-1);
      
      if(str.equals("?"))t=true;
    }
    return t;
}

@RequestMapping(value = "/v1/nlp/object/discover/sentecedata", method = RequestMethod.POST)

public @ResponseBody FindObjects  getSenteceData(@RequestBody SpamText text) {
	
	//postcount++;
	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return result;
	    }
	  
	
	  
	  
	try{
	String id = text.getGuid();
	String utext = text.getUtftext();
	if(id!=null&&utext!=null){
		
		Map<Integer,String> strings=analysis.getSentecesFromString(utext);
		//hasVoprosSymbol - тип предложения
		 LuceneMorphology luceneMorph = objs.getLuceneMorph();
		 Map<String, List> iilobjectsList=fastMemoryModel.getIilobjects();
		 
		 Map<String, String> iwordSynonims=fastMemoryModel.getWordSynonims();
		 Map<String, String> iwordFraseologism=fastMemoryModel.getWordFraseologism();
		 Map<String, String> igetWordExclude=fastMemoryModel.getWordExclude();
		 Map<String, ArrayList> iSymantex=nameFinderModelMEDAO.getSymantexhashMap();
		 
		 Map<String, Integer>   inounverbdb=fastMemoryModel.getNounverbdb();
		 Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getLemmanounverbdb();
			
		 Map<String, String> iwordPositive=fastMemoryModel.getWordPositive();
		 Map<String, String> iwordNegative=fastMemoryModel.getWordNegative();
		
		 //Map<String, String> liniarmap=new HashMap<String, String>();
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		
		 Map<String, String> morfema=fastMemoryModel.getMorfema();
			
			
//		 for (Map.Entry<String, String> entry : iwordPositive.entrySet())
//		 {
//		     System.out.println(entry.getKey() + "/" + entry.getValue());
//		 }
//		 for (Map.Entry<String, String> entry : iwordNegative.entrySet())
//		 {
//		     System.out.println(entry.getKey() + "/" + entry.getValue());
//		 }
		StringBuffer resultarray = new StringBuffer();

		
		if(strings!=null){
			
			//1 разобрать каждую на массив
			//2 поискать сущ
			//3 поискать объекты
			//4 поискать фразио
			int stnum=0;
			 for (Map.Entry<Integer, String> estring : strings.entrySet())
			 {try{
				 stnum++;
				
				 String singltext=estring.getValue();
				 if(hasVoprosSymbol(singltext)){resultarray.append(stnum+":strVopros ");}else 
				 resultarray.append(stnum+":strNumber ");
				 
				 ArrayList<String> podlejashie = new ArrayList<String>();
				 ArrayList<String> skazuemie = new ArrayList<String>();
				 
				 
//				 int[] level=new int[5];
//				 level[0]=1;
//				 level[1]=0;
//				 level[2]=0;
//				 level[3]=0;
//				 level[4]=0;
//			
//				 Map<Integer, List<String[]>> map = cleanText.getText(level, utext);
//				 List<String[]> value = map.get(1);
//				
//				 for(String[] index:value){
//					 
//					String word = index[0];
//					
//				 }
				 
				 String dirty=singltext;
					
			    	try{dirty=singltext.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
			    	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
			    	
			    	
				String tokens[] = dirty.toString().split("\\p{P}?[\\| \\t\\n\\r]+");
				List<String[]> alltokens = new  ArrayList<String[]>();
				List<String[]> alltokenstrim = new  ArrayList<String[]>();
				Map<String, Integer>  wordCount=new HashMap<String, Integer>();
				int size = tokens.length;
				 
				for(int i=0;i<size;i++){
					String[] indexsingle = new String[2];
					String[] indexbigram = new String[2];
					String[] indextrigram = new String[2];
					String[] indexfourgram = new String[2];
					String[] indexfivegram = new String[2];
					String[] indexsixgram = new String[2];
					String[] indexsevengram = new String[2];
					String[] indexeightgram = new String[2];
					String[] indexninegram = new String[2];
					
					String single=null;
					String bigram=null;
					String trigram=null;
					String fourgram=null;
					String fivegram=null;
					String sixgram=null;
					String sevengram=null;
					String eightgram=null;
					String ninegram=null;
					
					single=tokens[i];
					indexsingle[0] = single;
					indexsingle[1] = i+"|1";
					
					if(wordCount.containsKey(single)){
					
					int t=wordCount.get(single);
					
					indexsingle[1] = i+"|1|"+(t+1);
					wordCount.put(single, t+1);
					
					}
					else{
						wordCount.put(single, 1);
						indexsingle[1] = i+"|1|1";
					}
					
					
					if(i+1<size){
					bigram=tokens[i]+" "+tokens[i+1];
					indexbigram[0] = bigram;
					indexbigram[1] = i+"|2";
					    if(wordCount.containsKey(bigram)){
						
						int t=wordCount.get(bigram);
						indexbigram[1] = i+"|2|"+(t+1);
						wordCount.put(bigram, t+1);
						
						}
						else{
							wordCount.put(bigram, 1);
							indexbigram[1] = i+"|2|1";
						}
					
					}
					if(i+2<size){
					trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
					indextrigram[0] = trigram;
					indextrigram[1] = i+"|3";
					
				    if(wordCount.containsKey(trigram)){
						
					int t=wordCount.get(trigram);
					indextrigram[1] = i+"|3|"+(t+1);
					wordCount.put(trigram, t+1);
					
					}
					else{
						wordCount.put(trigram, 1);
						indextrigram[1] = i+"|3|1";
					}
					
					}
					if(i+3<size){
					fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
					indexfourgram[0] = fourgram;
					indexfourgram[1] = i+"|4";
					
				    if(wordCount.containsKey(fourgram)){
						
					int t=wordCount.get(fourgram);
					indexfourgram[1] = i+"|4|"+(t+1);
					wordCount.put(fourgram, t+1);
					
					}
					else{
						wordCount.put(fourgram, 1);
						indexfourgram[1] = i+"|4|1";
					}
				    
					}
					if(i+4<size){
						fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
						indexfivegram[0] = fivegram;
						indexfivegram[1] = i+"|5";
						
					    if(wordCount.containsKey(fivegram)){
							
							int t=wordCount.get(fivegram);
							indexfivegram[1] = i+"|5|"+(t+1);
							wordCount.put(fivegram, t+1);
							
							}
							else{
								wordCount.put(fivegram, 1);
								indexfivegram[1] = i+"|5|1";
							}
						
					}
					if(i+5<size){
						sixgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5];
						indexsixgram[0] = sixgram;
						indexsixgram[1] = i+"|6";
						
					    if(wordCount.containsKey(sixgram)){
							
							int t=wordCount.get(sixgram);
							indexsixgram[1] = i+"|6|"+(t+1);
							wordCount.put(sixgram, t+1);
							
							}
							else{
								wordCount.put(sixgram, 1);
								indexsixgram[1] = i+"|6|1";
							}
						
					}
					
					if(i+6<size){
						sevengram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6];
						indexsevengram[0] = sevengram;
						indexsevengram[1] = i+"|7";
						
					    if(wordCount.containsKey(sevengram)){
							
							int t=wordCount.get(sevengram);
							indexsevengram[1] = i+"|7|"+(t+1);
							wordCount.put(sevengram, t+1);
							
							}
							else{
								wordCount.put(sevengram, 1);
								indexsevengram[1] = i+"|7|1";
							}
						
					}
					
					if(i+7<size){
						eightgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6]+" "+tokens[i+7];
						indexeightgram[0] = eightgram;
						indexeightgram[1] = i+"|8";
						
					    if(wordCount.containsKey(eightgram)){
							
							int t=wordCount.get(eightgram);
							indexeightgram[1] = i+"|8|"+(t+1);
							wordCount.put(eightgram, t+1);
							
							}
							else{
								wordCount.put(eightgram, 1);
								indexeightgram[1] = i+"|8|1";
							}
						
					}
					
					if(i+8<size){
						ninegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6]+" "+tokens[i+7]+" "+tokens[i+8];
						indexninegram[0] = ninegram;
						indexninegram[1] = i+"|9";
						
					    if(wordCount.containsKey(ninegram)){
							
							int t=wordCount.get(ninegram);
							indexninegram[1] = i+"|9|"+(t+1);
							wordCount.put(eightgram, t+1);
							
							}
							else{
								wordCount.put(ninegram, 1);
								indexninegram[1] = i+"|9|1";
							}
						
					}
					
					
					if(single!=null)
						alltokens.add(indexsingle);
						if(bigram!=null)
							alltokens.add(indexbigram);
							if(trigram!=null)
								alltokens.add(indextrigram);
								if(fourgram!=null)
									alltokens.add(indexfourgram);
									if(fivegram!=null)
										alltokens.add(indexfivegram);
				
										if(sixgram!=null)
											alltokens.add(indexsixgram);
										
										if(sevengram!=null)
											alltokens.add(indexsevengram);
										
										if(eightgram!=null)
											alltokens.add(indexeightgram);
										
										if(ninegram!=null)
											alltokens.add(indexninegram);
									
										
										
									
							          if(i>15000){
							        	  break;
							          }	
				}
				
				//Возможны торможения
				for (String[] wordtokend: alltokens) { 
					String[] index = new String[2];
					index[0]=wordtokend[0].trim();
					index[1]=wordtokend[1];
					
					alltokenstrim.add(index);
				}
				alltokens=alltokenstrim;
				
				try{
					
					for (String[] wordtoken: alltokens) { 
					  
						try{	
							
							String lowerStr = wordtoken[0].toLowerCase();
							String lowerStrleght = "";
							//wordtoken[1] метрика слова или фразы "1|9|1" позиция начала / количество слов \ повторяемость
							    if(wordtoken[1].contains("|")){
								   String[] myData = wordtoken[1].split("\\|");
								   lowerStrleght =myData[1];
								}
//----2 поискать сущ
						
							    
							  //анализ основных объектов по токенам 
								String objectid=liniarmap.get(lowerStr);
								
								if(objectid!=null){
									resultarray.append(wordtoken[0]+"["+objectid+"] ");  
								}
								
								
							    
						 try{		
					if(lowerStrleght.equals("1")){
							    
						if(!igetWordExclude.containsKey(lowerStr)){	     try{ 
						     String pos=iwordPositive.get(lowerStr);
						     if(pos!=null){
						    	 resultarray.append(lowerStr).append("-POS ");
						     }
						     }catch(Exception e){e.printStackTrace();}
						     
						    try{ 
							     String neg=iwordNegative.get(lowerStr);
							     if(neg!=null){
							    	 resultarray.append(lowerStr).append("-NEG ");
							     }
							     }catch(Exception e){e.printStackTrace();} 
							}
						    
						
						
							    String wordform="";
								
									
//!!!!!!!!!!!!!!!!!!!!!!!!!!!не нашел слово === ищи синоним	- правоохранители 							
									List<String> wordBaseForms = luceneMorph.getMorphInfo(lowerStr);
									
									int sizer=0;
									try{sizer=wordBaseForms.size();}catch(Exception e){}
									
									if(sizer>0){
									
									StringBuilder sb = new StringBuilder();
									for (String s : wordBaseForms)
									{
									    sb.append(s);
									    sb.append(" #");
									}
									
									
									String ist = sb.toString();
									if(!ist.equals("")&&ist!=null){
										wordform=ist;
									}
									
								
								
									if(wordform.contains("|")){
										   String[] myData = wordform.split("\\|");
										   String nword =myData[0].trim();
										   String morf =myData[1];
										   boolean[] rule = new boolean[2];
										   rule[0]=false;
										   rule[1]=false;
										   rule = analysis.podlejasheeRule(morf);
										   
										   if(rule[0]==true||rule[1]==true){
											   
											   resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("rule1^");  
											   
											   podlejashie.add(lowerStr);
										   }
										   else{
											   boolean[] srule = new boolean[2];
											   srule[0]=false;
											   srule[1]=false;
											   srule = analysis.skazuemoeRule(nword, morf, "", ""); 
											   if(srule[0]==true&&srule[1]==true){
												   resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb2word# ");
												   
											   }else if(srule[0]==true){
												   resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb1# ");
												   skazuemie.add(lowerStr);
											   }
										   }
										  
//--------------------------MORFEMA----------------------------
										   if(!igetWordExclude.containsKey(lowerStr)){  if(morfema.containsKey(nword)){
											   String m= morfema.get(nword);
											   if(m.contains("|")){
												   String[] mdata = m.split("\\|");   
												    int sizeb = mdata.length;
													for(int i=0;i<sizeb;i++){
														String single=mdata[i];
														
														try{ 
														     String pos=iwordPositive.get(single);
														     if(pos!=null){
														    	 resultarray.append(lowerStr+"["+single+"]").append("-POS ");
														     }
														     }catch(Exception e){e.printStackTrace();}
														     
														try{ 
															     String neg=iwordNegative.get(single);
															     if(neg!=null){
															    	 resultarray.append(lowerStr+"["+single+"]").append("-NEG ");
															     }
														}catch(Exception e){e.printStackTrace();} 
													}
												   
											   }else{
												   
												   try{ 
													     String pos=iwordPositive.get(m);
													     if(pos!=null){
													    	 resultarray.append(lowerStr+"["+m+"]").append("-POS ");
													     }
													     }catch(Exception e){e.printStackTrace();}
													     
													try{ 
														     String neg=iwordNegative.get(m);
														     if(neg!=null){
														    	 resultarray.append(lowerStr+"["+m+"]").append("-NEG ");
														     }
													}catch(Exception e){e.printStackTrace();} 
											   }
											   
											   
								        	}}
										       
											    
										   //=============================================================
										   
									}
									
									  
									
									
									}else{
										String syn=iwordSynonims.get(lowerStr);
										//get sin
										String[] newwords;
										  		if(syn.contains("/")){
										  			newwords = syn.split("\\/");
									          	
									          	}else{
									          		// System.out.println ();
									          		String[] synindex = new String[1];
									          		synindex[0]=syn;
									          		newwords = synindex;
									          	}
										  		int siz = newwords.length;
												 
														for(int ik=0;ik<siz;ik++){
															
														String mysynonim=clean(newwords[ik]).toLowerCase().trim();
														List<String> swordBaseForms = luceneMorph.getMorphInfo(mysynonim);
														int sizers=0;
														try{sizers=swordBaseForms.size();}catch(Exception e){}
														
																if(sizers>0){
																
																	StringBuilder sb = new StringBuilder();
																	for (String s : swordBaseForms)
																	{
																	    sb.append(s);
																	    sb.append(" #");
																	}
																	
																	
																	String ist = sb.toString();
																	if(!ist.equals("")&&ist!=null){
																		wordform=ist;
																	}
																	
																
																
																
																	if(wordform.contains("|")){
																		   String[] myData = wordform.split("\\|");
																		   String morf =myData[1];
																		   String nword =myData[0].trim();
																		   boolean[] rule = new boolean[2];
																		   rule[0]=false;
																		   rule[1]=false;
																		   rule = analysis.podlejasheeRule(morf);
																		   
																		   if(rule[0]==true||rule[1]==true){
																			   
																			   resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("rule1# ");  
																		   }
																		   else{
																			   boolean[] srule = new boolean[2];
																			   srule[0]=false;
																			   srule[1]=false;
																			   srule = analysis.skazuemoeRule(nword, morf, "", ""); 
																			   if(srule[0]==true&&srule[1]==true){
																				   resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb2word# ");
																			   }else if(srule[0]==true){
																				   resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb1# ");
																			   }
																			   
																		   }
																	}
																break;
																}	
															
														}	  
										  
									}
							    }}catch(Exception e){
									e.printStackTrace();}
					
					try{
							     if(!igetWordExclude.containsKey(lowerStr)){
//3 поискать объекты - NB приводятся к нижнему регистру
							    try{	 
							     List<String> qindexList = iilobjectsList.get(lowerStr); 
								 if(qindexList!=null)
								 for(String qindex:qindexList){
									 resultarray.append(wordtoken[0]).append("-").append(wordtoken[1]).append("-").append("rule2").append("-"+qindex+"#");
								 }
							     }catch(Exception e){
										e.printStackTrace();
										
							     }
								 
//4 поискать фразио
									try{String objectfrasio=iwordFraseologism.get(lowerStr);
									
									if(objectfrasio!=null){
										resultarray.append(wordtoken[0]).append("-").append(wordtoken[1]).append("-").append("rule3Frazio");
										 
										
									}}catch(Exception e){
										e.printStackTrace();
										
								     }	
										 
									try{    ArrayList tempdoList = null;
									        tempdoList = iSymantex.get(lowerStr);
									    if(tempdoList !=null){
									    	for (Object value: tempdoList) {
									    		
									    		  String[] index = new String[2];  
									        	  index =(String[]) value;
									        	  
//===================================ГИПЕРОНИМЫ===СИНОНИМЫ==================================================									        	  
//resultarray.append(lowerStr).append("-").append(index[0]+" Type : "+index[1]).append("-").append("rule4");
									            	  
									            
									    	}
									    }}catch(Exception e){
											e.printStackTrace();
											
									     }
							     }
							     
							     
							   
							     
						}catch(Exception e){
							e.printStackTrace();}
						}catch(Exception e){
								e.printStackTrace();}
						
					} 
					
//расчет появления подлежащего и сказуемого по глагольному ряду в этом предолжении
					
					//создаем массив возможных пар
					ArrayList<String> objs = new ArrayList<String>();
					for(String pod:podlejashie){
							
						for(String skaz:skazuemie){
							objs.add(pod+" "+skaz);
						}
					}
					
					
						for(String wordkey:objs){
								int totilemma = -2;
								int	toti=-1;
								try{ totilemma = ilemmanounverbdb.get(wordkey);}catch(Exception e){}
								try{ toti=inounverbdb.get(wordkey);}catch(Exception e){}
								
								
								resultarray.append(wordkey).append("=").append("["+toti+"#"+totilemma+"]").append("rule5");
						}
						
				  }catch(Exception e){
					e.printStackTrace();}

			 }catch(Exception e){	e.printStackTrace();}
			 }
			
			
		}
		
		
		
		

		FindObjects result= new FindObjects();
		
//		result.setFindObject(msg);
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("");
		result.setResponse("true");
		result.setText(resultarray.toString());
//		result.setText("");
	
		
	
	return result;}
	else{
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("empty text");
		result.setResponse("false");
		result.setText("");
		return result;
	}
	}catch(Exception e){e.printStackTrace(); 
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid("");
	result.setMsg("");
	result.setResponse("false");
	result.setText("empty request Body");
	return result;}  
}

public  String clean(String text){
       
	 String dirty=text; 
   	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
   	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
   	try{dirty=dirty.replaceAll("[\\s]{2,}", " ");}catch(Exception e){	e.printStackTrace();}
   	
		  
		return dirty;
}

public boolean exclude(String ex){
	
	boolean yes=false;
	
	
	return yes;
}




























//=======================Tonality=========================================================================


//@RequestMapping(value = "/v1/nlp/object/discover/tonality", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
//public @ResponseBody JSONObject getTonalityData(@RequestBody SpamText text) {

@RequestMapping(value = "/v1/nlp/object/discover/old2liniar", method = RequestMethod.POST)

//@RequestMapping(value = "/v1/nlp/object/discover/liniar", method = RequestMethod.POST)	
public @ResponseBody FindObjects getData(@RequestBody SpamText text) { 	
	boolean getrezult=false;
	  if (text == null) {
		  FindObjects result= new FindObjects();
			result.setFindObject(null);
			result.setGuid("");
			result.setMsg("");
			result.setResponse("false");
			result.setText("empty request Body");
			return null;
	    }
	  
	
	  
	  
	try{
	String id = text.getGuid();
	if(id.equals("33"))getrezult=true;
	
	String utext = text.getUtftext();
	if(id!=null&&utext!=null){
		//boolean alllog = true;
		//boolean arryalog = false;
		//if(id.equals("1")) alllog = true;
		//if(id.equals("2")) arryalog = true;
		
		Map<String, Integer>  wordPhraseCount=new HashMap<String, Integer>();
		Map<String, Integer>  wordPhraseCountPredlod=new HashMap<String, Integer>();
		Map<String, Integer>  wordHoroshoCount=new HashMap<String, Integer>();
		Map<String, Integer>  wordPlohoCount=new HashMap<String, Integer>();
		
		
		Map<Integer,String> strings=analysis.getSentecesFromString(utext);
		//hasVoprosSymbol - тип предложения
		 LuceneMorphology luceneMorph = objs.getLuceneMorph();
		 Map<String, List> iilobjectsList=fastMemoryModel.getIilobjects();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 Map<String, List> iilproperties= fastMemoryModel.getIilotherprops();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iilmaincategory=fastMemoryModel.getMaincategory();
		 Map<String, List> iilhaspart= fastMemoryModel.getIilhaspart();
		 Map<String, Integer> triplet=fastMemoryModel.getNounverbdbTriplet();
			
		 
		 Map<String, String> iwordSynonims=fastMemoryModel.getWordSynonims();
		 Map<String, String> iwordFraseologism=fastMemoryModel.getWordFraseologism();
		 Map<String, String> igetWordExclude=fastMemoryModel.getWordExclude();
		 Map<String, ArrayList> iSymantex=nameFinderModelMEDAO.getSymantexhashMap();
		 
		 Map<String, Integer>   inounverbdb=fastMemoryModel.getNounverbdb();
		 Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getLemmanounverbdb();
			
		 Map<String, String> iwordPositive=fastMemoryModel.getWordPositive();
		 Map<String, String> iwordNegative=fastMemoryModel.getWordNegative();
		 Map<String, String> iwordPositiveHuman=fastMemoryModel.getWordPositiveHuman();
		 Map<String, String> iwordNegativeHuman=fastMemoryModel.getWordNegativeHuman();
		
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 Map<String, String> morfema=fastMemoryModel.getMorfema();
		 
		 Map<String, String> familii=nameFinderModelMEDAO.getFamilii();
		 Map<String, String> morfemawords=fastMemoryModel.getMorfemaWords();
		 Map<String, String> negpredlog=fastMemoryModel.getWordNegPreglog();
		 
		 Map<String, String> predlogMesta=fastMemoryModel.getWordPredlogMesta();
		 Map<String, String> narechiyaVremeni=fastMemoryModel.getNarechiyaVremeni(); 
		 Map<String, String> dniMeseats=fastMemoryModel.getDniMeseats();
		 
		 Map<String, String> politehwords=fastMemoryModel.getPolitehwords();
		 
		 Map<String, String> economicswords=fastMemoryModel.getEconomicswords();
		 
		 Map<String, String> podlejasheeExclude=new HashMap<String, String>();
		 podlejasheeExclude.put("из", "из");
		 
		 Map<String, String> razProtsent=new HashMap<String, String>();
		 razProtsent.put("раз", "раз");
		 razProtsent.put("процентный", "процентный");
		 razProtsent.put("процентных", "процентных");
		 razProtsent.put("процент", "процент");
		 
		 
		 Map<String, String> metricsTypeOnel=fastMemoryModel.getMetricsTypeOne();
		 Map<String, String> metricsTypeTwol=fastMemoryModel.getMetricsTypeTwo();
		 Map<String, String> metricsTypeThreel=fastMemoryModel.getMetricsTypeThree();
		 Map<String, String> wordKvantor=fastMemoryModel.getWordKvantor();
			
		 
		 JSONObject objson = new JSONObject();
		 JSONArray listjson = new JSONArray();
		 Map<String, Integer> allcategoryMap = new HashMap<String, Integer>();
		 
		 Map<String, Integer> allemmotionMap = new HashMap<String, Integer>();
		 allemmotionMap.put("positive", 0);
		 allemmotionMap.put("negative", 0);
		 
		 
		 Map<String, Integer> coreemmotionMap = new HashMap<String, Integer>();
		 coreemmotionMap.put("positive", 0);
		 coreemmotionMap.put("negative", 0);
		 
		
		 if(ineededu){
				int[] level=new int[5];
				
				 level[0]=1;
				 level[1]=0;
				 level[2]=1;
				 level[3]=0;
				 level[4]=0;
				 Map<Integer, List<String[]>> map = cleanText.getText(level, utext);
				 
				
				 analysis.getRelationsObjectAndVerb(map);
		 }
		 
		if(strings!=null){
			 
			//1 разобрать каждую на массив
			//2 поискать сущ
			//3 поискать объекты
			//4 поискать фразио
			int stnum=0;
			 for (Map.Entry<Integer, String> estring : strings.entrySet())
			 {try{
				 stnum++;
				 JSONObject srtjson = new JSONObject();
				 JSONArray wordlist = new JSONArray();
//номер строки в документе				
				 srtjson.put("number", ""+stnum);
			
				 
				 String singltext=estring.getValue();
				 if(hasVoprosSymbol(singltext)){srtjson.put("isvopros", "true");}else srtjson.put("isvopros", "false");
				 
//начальный текст				
				 srtjson.put("origtext", singltext);
				 
				 
				 ArrayList<Integer> podlejashie = new ArrayList<Integer>();
				 ArrayList<Integer> skazuemie = new ArrayList<Integer>();
				 				 
				 String dirty=singltext;
					
			    	try{dirty=singltext.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
			    	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
			    	
			    	
				String tokens[] = dirty.toString().split("\\p{P}?[\\| \\t\\n\\r]+");
				List<String[]> alltokens = new  ArrayList<String[]>();
				List<String[]> alltokenstrim = new  ArrayList<String[]>();
				Map<String, Integer>  wordCount=new HashMap<String, Integer>();
				int size = tokens.length;
//длина предложения
				srtjson.put("words", ""+size); 
				
				
				String[] iilarrayL1 = new String[size];
				String[] nwordarrayL2 = new String[size];
				String[] morfoarrayL3 = new String[size];
				//String[] emotionarrayL4 = new String[size];
				
				String[] emotionarrayL4OUT = new String[size];
				
				String[] fonemaarrayL5 = new String[size];
				String[] objectarrayL6 = new String[size];
				String[] familiiarrayL7 = new String[size];
				String[] politeharrayL8= new String[size];
				String[] chislaarrayL9= new String[size];
				String[] timeplaceL10= new String[size];
				String[] kvantorL11= new String[size];
				String[] metricsL12= new String[size];
				String[] economicsarrayL15= new String[size];
				
				Map<String, Map<String, String>> objectMap = new HashMap<String, Map<String, String>>();
				
				
				
				ArrayList<String> cores = new ArrayList<String>();
				ArrayList<String> maincore = new ArrayList<String>();
				ArrayList<String> maincorelemma = new ArrayList<String>();
				
				for(int i=0;i<size;i++){
					String[] indexsingle = new String[2];
					String[] indexbigram = new String[2];
					String[] indextrigram = new String[2];
					String[] indexfourgram = new String[2];
					String[] indexfivegram = new String[2];
					String[] indexsixgram = new String[2];
					String[] indexsevengram = new String[2];
					String[] indexeightgram = new String[2];
					String[] indexninegram = new String[2];
					
					String single=null;
					String bigram=null;
					String trigram=null;
					String fourgram=null;
					String fivegram=null;
					String sixgram=null;
					String sevengram=null;
					String eightgram=null;
					String ninegram=null;
					
					single=tokens[i];
					indexsingle[0] = single;
					indexsingle[1] = i+"|1";
					
					if(wordCount.containsKey(single)){
					
					int t=wordCount.get(single);
					
					indexsingle[1] = i+"|1|"+(t+1);
					wordCount.put(single, t+1);
					
					}
					else{
						wordCount.put(single, 1);
						indexsingle[1] = i+"|1|1";
					}
					
					
					if(i+1<size){
					bigram=tokens[i]+" "+tokens[i+1];
					indexbigram[0] = bigram;
					indexbigram[1] = i+"|2";
					    if(wordCount.containsKey(bigram)){
						
						int t=wordCount.get(bigram);
						indexbigram[1] = i+"|2|"+(t+1);
						wordCount.put(bigram, t+1);
						
						}
						else{
							wordCount.put(bigram, 1);
							indexbigram[1] = i+"|2|1";
						}
					
					}
					if(i+2<size){
					trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
					indextrigram[0] = trigram;
					indextrigram[1] = i+"|3";
					
				    if(wordCount.containsKey(trigram)){
						
					int t=wordCount.get(trigram);
					indextrigram[1] = i+"|3|"+(t+1);
					wordCount.put(trigram, t+1);
					
					}
					else{
						wordCount.put(trigram, 1);
						indextrigram[1] = i+"|3|1";
					}
					
					}
					if(i+3<size){
					fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
					indexfourgram[0] = fourgram;
					indexfourgram[1] = i+"|4";
					
				    if(wordCount.containsKey(fourgram)){
						
					int t=wordCount.get(fourgram);
					indexfourgram[1] = i+"|4|"+(t+1);
					wordCount.put(fourgram, t+1);
					
					}
					else{
						wordCount.put(fourgram, 1);
						indexfourgram[1] = i+"|4|1";
					}
				    
					}
					if(i+4<size){
						fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
						indexfivegram[0] = fivegram;
						indexfivegram[1] = i+"|5";
						
					    if(wordCount.containsKey(fivegram)){
							
							int t=wordCount.get(fivegram);
							indexfivegram[1] = i+"|5|"+(t+1);
							wordCount.put(fivegram, t+1);
							
							}
							else{
								wordCount.put(fivegram, 1);
								indexfivegram[1] = i+"|5|1";
							}
						
					}
					if(i+5<size){
						sixgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5];
						indexsixgram[0] = sixgram;
						indexsixgram[1] = i+"|6";
						
					    if(wordCount.containsKey(sixgram)){
							
							int t=wordCount.get(sixgram);
							indexsixgram[1] = i+"|6|"+(t+1);
							wordCount.put(sixgram, t+1);
							
							}
							else{
								wordCount.put(sixgram, 1);
								indexsixgram[1] = i+"|6|1";
							}
						
					}
					
					if(i+6<size){
						sevengram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6];
						indexsevengram[0] = sevengram;
						indexsevengram[1] = i+"|7";
						
					    if(wordCount.containsKey(sevengram)){
							
							int t=wordCount.get(sevengram);
							indexsevengram[1] = i+"|7|"+(t+1);
							wordCount.put(sevengram, t+1);
							
							}
							else{
								wordCount.put(sevengram, 1);
								indexsevengram[1] = i+"|7|1";
							}
						
					}
					
					if(i+7<size){
						eightgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6]+" "+tokens[i+7];
						indexeightgram[0] = eightgram;
						indexeightgram[1] = i+"|8";
						
					    if(wordCount.containsKey(eightgram)){
							
							int t=wordCount.get(eightgram);
							indexeightgram[1] = i+"|8|"+(t+1);
							wordCount.put(eightgram, t+1);
							
							}
							else{
								wordCount.put(eightgram, 1);
								indexeightgram[1] = i+"|8|1";
							}
						
					}
					
					if(i+8<size){
						ninegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5]+" "+tokens[i+6]+" "+tokens[i+7]+" "+tokens[i+8];
						indexninegram[0] = ninegram;
						indexninegram[1] = i+"|9";
						
					    if(wordCount.containsKey(ninegram)){
							
							int t=wordCount.get(ninegram);
							indexninegram[1] = i+"|9|"+(t+1);
							wordCount.put(eightgram, t+1);
							
							}
							else{
								wordCount.put(ninegram, 1);
								indexninegram[1] = i+"|9|1";
							}
						
					}
					
					
					if(single!=null)
						alltokens.add(indexsingle);
						if(bigram!=null)
							alltokens.add(indexbigram);
							if(trigram!=null)
								alltokens.add(indextrigram);
								if(fourgram!=null)
									alltokens.add(indexfourgram);
									if(fivegram!=null)
										alltokens.add(indexfivegram);
				
										if(sixgram!=null)
											alltokens.add(indexsixgram);
										
										if(sevengram!=null)
											alltokens.add(indexsevengram);
										
										if(eightgram!=null)
											alltokens.add(indexeightgram);
										
										if(ninegram!=null)
											alltokens.add(indexninegram);
									
										
										
									
							          if(i>15000){
							        	  break;
							          }	
				}
				
				//Возможны торможения
				for (String[] wordtokend: alltokens) { 
					String[] index = new String[2];
					index[0]=wordtokend[0].trim();
					index[1]=wordtokend[1];
					
					alltokenstrim.add(index);
				}
				alltokens=alltokenstrim;
				
				
				Map<String, String>  findobject=new HashMap<String, String>();
				Map<String, Integer> categoryMap = new HashMap<String, Integer>();
				
				
				try{
					
					for (String[] wordtoken: alltokens) { 
					  
						try{	
							
							String lowerStr = wordtoken[0].toLowerCase();
							String tokenleght = "";
							String wordPosition = "";
							int iwordPosition = 0;
							int itokenleght = 0;
							//wordtoken[1] метрика слова или фразы "1|9|1" позиция начала / количество слов \ повторяемость
							    if(wordtoken[1].contains("|")){
								   String[] myData = wordtoken[1].split("\\|");
								   tokenleght =myData[1];
								   wordPosition=myData[0];
								   iwordPosition = Integer.parseInt(wordPosition);
								   itokenleght = Integer.parseInt(tokenleght);
								}
//----2 поискать сущ
						
							    String poly=politehwords.get(lowerStr);
								if(poly!=null){
									
									try{
										politeharrayL8[iwordPosition]=poly;
									if(itokenleght>1){
										int co=itokenleght-1; 
										for(int o=0; o<co;o++)
											politeharrayL8[iwordPosition+co]=poly;
										
									}}catch(Exception e){}
								}
							    
							    String economics=economicswords.get(lowerStr);
								if(economics!=null){
									
									try{
										economicsarrayL15[iwordPosition]=economics;
									if(itokenleght>1){
										int co=itokenleght-1; 
										for(int o=0; o<co;o++)
											economicsarrayL15[iwordPosition+co]=economics;
										
									}}catch(Exception e){}
								}
							  //анализ основных объектов по токенам 
								String objectid=liniarmap.get(lowerStr);
								
								if(objectid!=null){
									
									findobject.put("object"+wordPosition, objectid+"|"+wordtoken[1]);
									try{
									objectarrayL6[iwordPosition]=objectid;
									if(itokenleght>1){
										int co=itokenleght-1; //3
										for(int o=0; o<co;o++)
										objectarrayL6[iwordPosition+co]=objectid;
										
									}}catch(Exception e){}
									
									
									
									wordlist.add(findobject);	
									//resultarray.append(wordtoken[0]+"["+objectid+"] ");  
								}
								
						 try{		
							 if(tokenleght.equals("1")){
						 JSONObject wordjson = new JSONObject();
//позиция слова и слово						
//wordjson.put("рosition", ""+wordPosition+"-"+lowerStr);
						 
						 

						    
						
						 boolean familiinFind=true;
						 try{
							 String familiiid=familii.get(lowerStr);
						 		
						 if(familiiid!=null){if(Character.isUpperCase(wordtoken[0].codePointAt(0))){familiiarrayL7[iwordPosition] = lowerStr; familiinFind=false;}}
						 }catch(Exception e){}	
						
							    String wordform="";
								
									
//!!!!!!!!!!!!!!!!!!!!!!!!!!!не нашел слово === ищи синоним	- правоохранители 
							        String wrd=lowerStr.replaceAll("[^А-я]", "").trim(); 
							        List<String> wordBaseForms = null;
							        if(!"".equals(wrd)){
										wordBaseForms = luceneMorph.getMorphInfo(wrd);
									}
									
									int sizer=0;
									try{sizer=wordBaseForms.size();}catch(Exception e){}
									boolean ifindemo=true;
									if(sizer>0){
									
									StringBuilder sb = new StringBuilder();
									for (String s : wordBaseForms)
									{
									    sb.append(s);
									    sb.append(" #");
									}
									
									
									String ist = sb.toString();
									if(!ist.equals("")&&ist!=null){
										wordform=ist;
									}
									
								
								
									if(wordform.contains("|")){
										   String[] myData = wordform.split("\\|");
										   String nword =myData[0].trim();
										   String morf =myData[1];
//Начальная форма слова и морфология
											   wordjson.put("nword", nword);
										       wordjson.put("morf", morf);

										   
											nwordarrayL2[iwordPosition] = nword;
											morfoarrayL3[iwordPosition] = morf;
											
										  
										   
										   
										   
										   boolean[] rule = new boolean[2];
										   rule[0]=false;
										   rule[1]=false;
										   
										   if(!podlejasheeExclude.containsKey(lowerStr))
										   rule = analysis.podlejasheeRule(morf);
										   
										   if(rule[0]==true||rule[1]==true){
//Подлежащее											   
 wordjson.put("rule1", true);
											   podlejashie.add(iwordPosition);
										   }
										   else{
											   boolean[] srule = new boolean[2];
											   srule[0]=false;
											   srule[1]=false;
											   srule = analysis.skazuemoeRule(nword, morf, "", ""); 
											   if(srule[0]==true&&srule[1]==true){
//сказуемое
wordjson.put("rule2", true);
skazuemie.add(iwordPosition);
												   //resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb2word# ");
												   
											   }else if(srule[0]==true){
wordjson.put("rule3", true);
												   //resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb1# ");
skazuemie.add(iwordPosition);
											   }
										   }
										   
										   if(familiinFind){ try{
												 String familiiid=familii.get(nword);
											 		
											 if(familiiid!=null){if(Character.isUpperCase(wordtoken[0].codePointAt(0))){familiiarrayL7[iwordPosition] = nword; familiinFind=false;}}
											 }catch(Exception e){}	}
										  
//--------------------------MORFEMA----------------------------
										   
										   //Пожарная команда для морфем в виде???? синонимов, семантик, Q объектов 
										   if(!igetWordExclude.containsKey(lowerStr)){  
											 
											 boolean foundtonality=false;  
												  if(morfema.containsKey(nword)){	   
													   String[] morfjson = new String[3]; 
													   morfjson =  getMorfema( nword,  morfema, iwordPositive, iwordNegative);
													   
													  String neg      =  morfjson[0];
													  String pos      =  morfjson[1];
													  String allposneg=  morfjson[2];
													  if(neg!=null||pos!=null){
//morfjson тональность слова														 
wordjson.put("morfjson", "neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true;  
													  
													  //emotionarrayL4[iwordPosition]=neg+"|"+pos;
													  //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/a"+neg+"|"+pos;
													  String prdf="";
													
													  if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
														  if(negpredlog.containsKey(prdf)){
															    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
															    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
															  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
															  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
														  }else{
															    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
															    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
															  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
															  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
														  }
														  foundtonality=true;
													  }
													  else{
														  	if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
														  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
														  foundtonality=true;
													  }
													  
													  }	  
												  }
											//поиск тональности в синонимах	  
											    if(!foundtonality){
													    //get sin
//это не оригональное слово lowerStr
														try{
														
														String syn=iwordSynonims.get(nword);
														if(syn!=null){
														String[] newwords;
														  		if(syn.contains("/")){
														  			
														  			newwords = syn.split("\\/");
													          	
													          	}
														  		if(syn.contains(",")){
														  			
														  			newwords = syn.split(",");
													          	
													          	}
														  		else{
													          		
													          		String[] synindex = new String[1];
													          		synindex[0]=syn;
													          		newwords = synindex;
													          	}
														  		int siz = newwords.length;
																 
																for(int ik=0;ik<siz;ik++){
																	//Если найдено значение - выйти из цикла
																	  String[] morfjson = new String[3]; 
wordjson.put("morfjsonsyn"+ik, "word="+newwords[ik].trim()); 
																	  morfjson =  getMorfema( newwords[ik].trim(),  morfema, iwordPositive, iwordNegative);
																	  String neg      =  morfjson[0];
																	  String pos      =  morfjson[1];
																	  String allposneg=  morfjson[2];
																	  if(neg!=null||pos!=null){
wordjson.put("morfjsonsyn"+ik, "neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true;
																		  //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/s"+"["+newwords[ik]+"]"+neg+"|"+pos;
																		  
																		  String prdf="";
																			
																		  if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																			  if(negpredlog.containsKey(prdf)){
																				    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
																				    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																				  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																				  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																			  }else{
																				    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																				    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																				  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
																				  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																			  }
																			  foundtonality=true;
																		  }
																		  else{
																			  	if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
																			  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
																			  foundtonality=true;
																		  }
																	  }		  
																}
														}
														}catch(Exception e){ e.printStackTrace();}
												  }
										   
												//поиск тональности в семантик 
										     if(!foundtonality){	
//это не оригональное слово	lowerStr										   
											  
											   
											   try{     ArrayList tempdoList = null;
											   			tempdoList = iSymantex.get(nword);
											   			if(tempdoList !=null){
											   				int j=0;
														    	for (Object value: tempdoList) {
														    	j++;	
														    		  String[] index = new String[2];  
														        	  index =(String[]) value;
														        	  
														        	  
														        	  String iSymantexword=index[0];
														        	  String itype=index[1];
//wordjson.put("isymantex"+j, "word="+iSymantexword); 
														        	  String[] morfjson = new String[3]; 
														        	   iSymantexword= iSymantexword.trim();
																	  morfjson =  getMorfema( iSymantexword,  morfema, iwordPositive, iwordNegative);
																	  String neg      =  morfjson[0];
																	  String pos      =  morfjson[1];
																	  String allposneg=  morfjson[2];
																	  if(neg!=null||pos!=null){
wordjson.put("isymantex"+j, "word="+iSymantexword+" neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true;

																		 //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/i"+"["+iSymantexword+"^"+itype+"]"+neg+"|"+pos;
																  
																		  String prdf="";
																		  
																		  if(iwordPosition!=0){try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																			  if(negpredlog.containsKey(prdf)||itype.equals("antonyms")){
																				    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1);
																				    String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																				    }
																				  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																				  	 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																				  	}
																			  }else{
																				    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																				     String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																				    }
																				  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);
																				  	 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																				  	}
																			  }
																			  foundtonality=true;
																			  break;
																		  }
																		  else{
																			  if(itype.equals("antonyms")){
																			    	if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1);
																			    	 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																			    	}
																					if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																					 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																					}
																			    }else{
																			    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																			         String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																			    }
																				if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);
																				     String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																				}
																				} 
																			  foundtonality=true;
																			  break;
																		  }
																	  
																	  }		 
					
														            	  
														            
														    	}
														    }}catch(Exception e){e.printStackTrace();}
																   }
												  
												  
												//поиск тональности Q объектов 	
										     if(!foundtonality){	
											   
										   try{  
											   List<String> qindexList = iilobjectsList.get(nword);
											   int j=0;
											   if(qindexList!=null)
													 for(String qindex:qindexList){
													 
														 if(qindex!=null){ 
														 j++;
														 
														    String words=getTextofQ(qindex, iiltext);
												           // String wordsprop=getPropertyOfType(qindex, iiltype);
												            
												            String[] morfjson = new String[3]; 
wordjson.put("qdata"+j, "qdata="+words); 
															  morfjson =  getMorfema( words,  morfema, iwordPositive, iwordNegative);
															  String neg      =  morfjson[0];
															  String pos      =  morfjson[1];
															  String allposneg=  morfjson[2];
															  if(neg!=null||pos!=null){
wordjson.put("qdata"+j, "neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true; 
																  //emotionarrayL4[iwordPosition]=neg+"|0|"+words+"#"+pos+"|0|"+words;
																  //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/q"+"["+words+"]"+neg+"|"+pos;


																  String prdf="";
																	
																  if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																	  if(negpredlog.containsKey(prdf)){
																		    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
																		    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																		  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																		  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																	  }else{
																		    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																		    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																		  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
																		  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																	  }
																	  foundtonality=true;
																  }
																  else{
																	  	if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
																	  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
																	  foundtonality=true;
																  }
																  
																	}		 
														 }
													 
													 }
										   }catch(Exception e){e.printStackTrace();}
										   }
										}
										   
										
										   
									}
									
									  
									
									
									}else{
										//Пожарная команда для морфологии ??? синонимов, семантик, Q объектов 
										try{
										String syn=iwordSynonims.get(lowerStr);
										//get sin
										String[] newwords;
										if(syn!=null){
										  		if(syn.contains("/")){
										  			newwords = syn.split("\\/");
									          	
									          	}else{
									          		
									          		String[] synindex = new String[1];
									          		synindex[0]=syn;
									          		newwords = synindex;
									          	}
										  		int siz = newwords.length;
												 
														for(int ik=0;ik<siz;ik++){
															
														String mysynonim=clean(newwords[ik]).toLowerCase().trim();
														String wrdi=mysynonim.replaceAll("[^А-я]", "").trim(); 
														List<String> swordBaseForms = null;
												        if(!"".equals(wrdi)){
															swordBaseForms = luceneMorph.getMorphInfo(wrdi);
														}
														
														int sizers=0;
														try{sizers=swordBaseForms.size();}catch(Exception e){}
														
																if(sizers>0){
																
																	StringBuilder sb = new StringBuilder();
																	for (String s : swordBaseForms)
																	{
																	    sb.append(s);
																	    sb.append(" #");
																	}
																	
																	
																	String ist = sb.toString();
																	if(!ist.equals("")&&ist!=null){
																		wordform=ist;
																	}
																	
																
																
																
																	if(wordform.contains("|")){
																		   String[] myData = wordform.split("\\|");
																		   String morf =myData[1];
																		   String nword =myData[0].trim();
																		   boolean[] rule = new boolean[2];
																		   rule[0]=false;
																		   rule[1]=false;
																		   if(!podlejasheeExclude.containsKey(lowerStr))
																		   rule = analysis.podlejasheeRule(morf);
																		   
																		   if(rule[0]==true||rule[1]==true){
																			   
wordjson.put("rule4", true);
																			   //resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("rule1# ");  
																			   podlejashie.add(iwordPosition);
//не введено слово синоним в выдачу																				   
																		   }
																		   else{
																			   boolean[] srule = new boolean[2];
																			   srule[0]=false;
																			   srule[1]=false;
																			   srule = analysis.skazuemoeRule(nword, morf, "", ""); 
																			   if(srule[0]==true&&srule[1]==true){
wordjson.put("rule5", true);
																				   skazuemie.add(iwordPosition);
																				   // resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb2word# ");
																			   }else if(srule[0]==true){
wordjson.put("rule6", true);
																				   // resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb1# ");
//не введено слово синоним в выдачу																				   
																				   skazuemie.add(iwordPosition);
																			   }
																			   
																		   }
																		   
																		   
																		   
																		   
																		   
																		   
																		 //--------------------------MORFEMA----------------------------
																		   
																		   //Пожарная команда для морфем в виде???? синонимов, семантик, Q объектов 
																		   if(!igetWordExclude.containsKey(lowerStr)){  
																			 
																			 boolean foundtonality=false;  
																				  if(morfema.containsKey(nword)){	   
																					   String[] morfjson = new String[3]; 
																					   morfjson =  getMorfema( nword,  morfema, iwordPositive, iwordNegative);
																					   
																					  String neg      =  morfjson[0];
																					  String pos      =  morfjson[1];
																					  String allposneg=  morfjson[2];
																					  if(neg!=null||pos!=null){
wordjson.put("morfjson", "neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true;  
																						 //emotionarrayL4[iwordPosition]=neg+"|"+pos;
																						 //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/aa"+neg+"|"+pos;
																						  String prdf="";
																							
																						  if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																							  if(negpredlog.containsKey(prdf)){
																								    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
																								    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																								  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																								  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																							  }else{
																								    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																								    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																								  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
																								  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																							  }
																							  foundtonality=true;
																						  }
																						  else{
																							  	if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
																							  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
																							  foundtonality=true;
																						  }
																					  }	  
																				  }
																			//поиск тональности в синонимах	  
																			    if(!foundtonality){
																					    //get sin
								//это не оригональное слово lowerStr
																						try{String synl=iwordSynonims.get(nword);
																						if(synl!=null){
																						String[] newwordsl;
																						  		if(syn.contains("/")){
																						  			
																						  			newwordsl = synl.split("\\/");
																					          	
																					          	}
																						  		if(synl.contains(",")){
																						  			
																						  			newwordsl = synl.split(",");
																					          	
																					          	}
																						  		else{
																					          		
																					          		String[] synindex = new String[1];
																					          		synindex[0]=synl;
																					          		newwordsl = synindex;
																					          	}
																						  		int sizl = newwordsl.length;
																								 
																								for(int ikl=0;ikl<sizl;ikl++){
																									//Если найдено значение - выйти из цикла
																									  String[] morfjson = new String[3]; 
wordjson.put("morfjsonsyn"+ikl, "word="+newwordsl[ikl].trim()); 
																									  morfjson =  getMorfema( newwordsl[ikl].trim(),  morfema, iwordPositive, iwordNegative);
																									  String neg      =  morfjson[0];
																									  String pos      =  morfjson[1];
																									  String allposneg=  morfjson[2];
																									  if(neg!=null||pos!=null){
wordjson.put("morfjsonsyn"+ikl, "neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true;
																										 //emotionarrayL4[iwordPosition]=neg+"|"+pos;
																										 //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/sd"+"["+newwords[ik]+"]"+neg+"|"+pos;
																											String prdf="";
																											
																											if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																												  if(negpredlog.containsKey(prdf)){
																													    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
																													    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																													  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																													  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																												  }else{
																													    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																													    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																													  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
																													  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																												  }
																												  foundtonality=true;
																											}
																											else{
																												  	if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
																												  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
																												  foundtonality=true;
																											}	 
																									  }		  
																								}}}catch(Exception e){ e.printStackTrace();}
																				  }
																		   
																				//поиск тональности в семантик 
																		     if(!foundtonality){	
								//это не оригональное слово	lowerStr										   
																			  
																			   
																			   try{     ArrayList tempdoList = null;
																			   			tempdoList = iSymantex.get(nword);
																			   			if(tempdoList !=null){
																			   				int j=0;
																						    	for (Object value: tempdoList) {
																						    	j++;	
																						    		  String[] index = new String[2];  
																						        	  index =(String[]) value;
																						        	  String itype=index[1];
																						        	  
																						        	  String iSymantexword=index[0];
//wordjson.put("isymantex"+j, "word="+iSymantexword); 
																						        	  String[] morfjson = new String[3]; 
																						        	   iSymantexword= iSymantexword.trim();
																									  morfjson =  getMorfema( iSymantexword,  morfema, iwordPositive, iwordNegative);
																									  String neg      =  morfjson[0];
																									  String pos      =  morfjson[1];
																									  String allposneg=  morfjson[2];
																									  if(neg!=null||pos!=null){
wordjson.put("isymantex"+j, "word="+iSymantexword+" neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true;
																										  // emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/ii"+"["+iSymantexword+"^"+itype+"]"+neg+"|"+pos;
																										 
																											  String prdf="";
																											  
																											  if(iwordPosition!=0){try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																												  if(negpredlog.containsKey(prdf)||itype.equals("antonyms")){
																													    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1);
																													    String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																													    }
																													  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																													  	 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																													  	}
																												  }else{
																													    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																													     String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																													    }
																													  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);
																													  	 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																													  	}
																												  }
																												  foundtonality=true;
																												  break;
																											  }
																											  else{
																												  if(itype.equals("antonyms")){
																												    	if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1);
																												    	 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																												    	}
																														if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																														 String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																														}
																												    }else{
																												    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																												         String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";
																												    }
																													if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);
																													     String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";
																													}
																													} 
																												  foundtonality=true;
																												  break;
																											  }
																									  }
																									  
																									  
																									  //===================================ГИПЕРОНИМЫ===СИНОНИМЫ==================================================									        	  
													//resultarray.append(lowerStr).append("-").append(index[0]+" Type : "+index[1]).append("-").append("rule4");
																						            	  
																						            
																						    	}
																						    }}catch(Exception e){e.printStackTrace();}
																								   }
																				  
																				  
																				//поиск тональности Q объектов 	
																		     if(!foundtonality){	
																			   
																		   try{  
																			   List<String> qindexList = iilobjectsList.get(nword);
																			   int j=0;
																			   if(qindexList!=null)
																					 for(String qindex:qindexList){
																					 
																						 if(qindex!=null){ 
																						 j++;
																						 
																						    String words=getTextofQ(qindex, iiltext);
																				            String wordsprop=getPropertyOfType(qindex, iiltype);
																				            
																				            String[] morfjson = new String[3]; 
wordjson.put("qdata"+j, "qdata="+words); 
																							  morfjson =  getMorfema( words,  morfema, iwordPositive, iwordNegative);
																							  String neg      =  morfjson[0];
																							  String pos      =  morfjson[1];
																							  String allposneg=  morfjson[2];
																							  if(neg!=null||pos!=null){
wordjson.put("qdata"+j, "neg="+neg+" pos="+pos+" all="+allposneg);//foundtonality=true; 
																								  //emotionarrayL4[iwordPosition]=neg+"|0|"+words+"#"+pos+"|0|"+words;
																								  // emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/qq"+"["+words+"]"+neg+"|"+pos;
																									String prdf="";
																									
																									if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
																										  if(negpredlog.containsKey(prdf)){
																											    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
																											    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																											  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																											  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																										  }else{
																											    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																											    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																											  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
																											  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																										  }
																										  foundtonality=true;
																									}
																									else{
																										  	if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
																										  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
																										  foundtonality=true;
																									}
																									  }		 
																						 }
																					 
																					 }
																		   }catch(Exception e){e.printStackTrace();}
																		   }
																		} 
																		   
																		   
																		   
																		   
																	}
																break;
																}	
														}
														}	  }catch(Exception e){e.printStackTrace();}
										  
									}
//У этого множества созданного руками нет приоритета									
												if(!igetWordExclude.containsKey(lowerStr)){	     
													String pos=null;
													String neg=null;
													try{pos=iwordPositive.get(lowerStr);}catch(Exception e){e.printStackTrace();}
												     
												    try{neg=iwordNegative.get(lowerStr);}catch(Exception e){e.printStackTrace();} 
												      
													
													  if(neg!=null||pos!=null){
wordjson.put("posneglin", "neg="+neg+" pos="+pos);
												      //emotionarrayL4[iwordPosition]=neg+"|0|"+lowerStr+"#"+pos+"|0|"+lowerStr;
												      //emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/f"+neg+"|"+pos;
														String prdf="";
														
														if(iwordPosition!=0){  try{  prdf=nwordarrayL2[iwordPosition-1];}catch(Exception e){}
															  if(negpredlog.containsKey(prdf)){
																    if(neg!=null){int negt=allemmotionMap.get("positive"); allemmotionMap.put("positive", negt+1); 
																    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
																  	if(pos!=null){int post= allemmotionMap.get("negative"); allemmotionMap.put("negative", post+1);
																  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
															  }else{
																    if(neg!=null){int negt=allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);
																    			  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#true|false";else emotionarrayL4OUT[iwordPosition]="#true|false";}
																  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);  
																  				  String past=emotionarrayL4OUT[iwordPosition]; if(past!=null)emotionarrayL4OUT[iwordPosition]=past+"#false|true";else emotionarrayL4OUT[iwordPosition]="#false|true";}
															  }
															  
														}
														else{
															  	if(neg!=null){int negt= allemmotionMap.get("negative"); allemmotionMap.put("negative", negt+1);}
															  	if(pos!=null){int post= allemmotionMap.get("positive"); allemmotionMap.put("positive", post+1);}
															 
														}
													  }
												}
									
//wordlist.add(wordjson);	    
					}
					
						 }catch(Exception e){e.printStackTrace();}
					
						 
						 
						 //проверить если идущее слово или цифры относится к вопросу  _сколько_ ЧИСЛ десять, ЧИСЛ сравн
						 //проверить если идущее слово относится к вопросу ___КОГДА___? А-формат дат, наречия и месяцы, ЧИСЛ десятое, в десять, на десятое
						 
						 //проверить если идущее слово относится к вопросу  _ГДЕ_место(предлоги в на)
						 
						 try{
							 if(tokenleght.equals("1")){
							try{	
							 // _в на_	 
							 if(predlogMesta.containsKey(lowerStr)){
								 chislaarrayL9[iwordPosition]="1";
								      
							 }
							 //числа
							 else if(lowerStr.matches(".*\\d.*")){
								 chislaarrayL9[iwordPosition]="2";
								      
							 }
							 
							 // _сколько_
							 else if(isPresent("(ЧИСЛ сравн)",morfoarrayL3[iwordPosition])){
								 chislaarrayL9[iwordPosition]="3";
								      
							 }
							 
							 else if(isPresent("(ЧИСЛ)",morfoarrayL3[iwordPosition])){
								 chislaarrayL9[iwordPosition]="4";
								      
							 }
							 //_КОГДА_ катя завтра вечером
							 else if(narechiyaVremeni.containsKey(nwordarrayL2[iwordPosition])){
								 chislaarrayL9[iwordPosition]="5";
								     
							 }
							 
							 else if(dniMeseats.containsKey(nwordarrayL2[iwordPosition])){
								 chislaarrayL9[iwordPosition]="6";
								     
							 }
							 else if(narechiyaVremeni.containsKey(tokens[iwordPosition].toLowerCase())){
								 chislaarrayL9[iwordPosition]="5";
								   
							 }
							 }catch(Exception e){}
  
							
							 }
							 
						       try{
						    	   if(iwordPosition>1&&iwordPosition<=size)
						    	   if(tokens[iwordPosition-1]!=null&&tokens[iwordPosition-2]!=null){
									 String morfposa= tokens[iwordPosition-2].toLowerCase().trim();
									 String morfposb= tokens[iwordPosition-1].toLowerCase().trim();
									 String morfposc= tokens[iwordPosition].toLowerCase().trim();
									 if(morfposb.equals("это")&&morfposc.equals("плохо")){
										 //System.out.println("плохо--------------"+morfposc);
										 //if(wordPlohoCount.containsKey(morfposb)){int r=wordPlohoCount.get(morfposb); wordPlohoCount.put(morfposb, r+1);}else{wordPlohoCount.put(morfposb, 1);}
										  if(wordPlohoCount.containsKey(morfposa)){int r=wordPlohoCount.get(morfposa); wordPlohoCount.put(morfposa, r+1);}else{wordPlohoCount.put(morfposa, 1);}
									 }
									 else if(morfposb.equals("это")&&morfposc.equals("хорошо")){
										 //System.out.println("хорошо--------------"+morfposc);
										 //if(wordHoroshoCount.containsKey(morfposb)){int r=wordHoroshoCount.get(morfposb); wordHoroshoCount.put(morfposb, r+1);}else{wordHoroshoCount.put(morfposb, 1);}
										  if(wordHoroshoCount.containsKey(morfposa)){int r=wordHoroshoCount.get(morfposa); wordHoroshoCount.put(morfposa, r+1);}else{wordHoroshoCount.put(morfposa, 1);}
									 }}
									 
									 }catch(Exception e){}
							
//							 try{	
//							 if(metricsTypeOnel.containsKey(nwordarrayL2[iwordPosition])){
//								 metricsL12[iwordPosition]=metricsTypeOnel.get(nwordarrayL2[iwordPosition]);
//								     
//							 }
//							 }catch(Exception e){}
//							 if(tokenleght.equals("2")){
//								 
//								 try{	
//									 if(metricsTypeTwol.containsKey(nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition])){
//										String typeTwo= metricsTypeTwo.get(nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition]);
//										 metricsL12[iwordPosition]=typeTwo;
//										 metricsL12[iwordPosition+1]=typeTwo;
//										     
//									 }
//									 }catch(Exception e){}
//								 
//							 }
//							 if(tokenleght.equals("3")){
//								 
//								 try{	
//									 if(metricsTypeThreel.containsKey(nwordarrayL2[iwordPosition-2]+" "+nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition])){
//										String typeThree= metricsTypeThreel.get(nwordarrayL2[iwordPosition-2]+" "+nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition]);
//										 metricsL12[iwordPosition]=typeThree;
//										 metricsL12[iwordPosition+1]=typeThree;
//										 metricsL12[iwordPosition+2]=typeThree;
//										     
//									 }
//									 }catch(Exception e){}
//								 
//								 
//							 }
							 
							 
							 
							 //--  --квантор--  --тип
							 //Предицирующие связки     - 1  «есть» и «не есть» S есть Р (где S - логич. подлежащее, или субъект, Р - логич. сказуемое, или предикат)
							 //Пропозициональные связки - 2  «и», «или», «неверно, что», «если…, то», «если и только если» — отрицание («неверно, что...»), «&» («∧») — конъюнкция («и»), «v» — нестрогая дизъюнкция («или»), «v» — строгая дизъюнкция («либо... либо...»), «⇒» — импликация («если..., то...»), «⇔» — эквиваленция («если и только если»).
							 //Квантор общности         - 3  «для всех…», «для каждого…», «для любого…» или «все…», «каждый…», «любой…» «ни один» 
							 //Квантор существования    - 4  «существует», «некоторые», «большинство», «какой-нибудь»
							 //Квантор количественный   - 5  больше меньше равно высоко и низко далеко и близко, снизил, уменьшилась-увеличилась, снижение, поднялась, сократились, продлят,превышать 

							 
//Квантор качественый      - 6  <word>умнее</word> <wordform>умный|Y П сравн,од,но,</wordform>  <word>выше</word> <wordform> выша|G С жр,ед,дт,выше|j Н,высокий|Y П сравн,од,но,</wordform>
							  
							 if(nwordarrayL2[iwordPosition]!=null){
							 	String eKvantor=wordKvantor.get(nwordarrayL2[iwordPosition]);
								if(eKvantor!=null){
									try{
										kvantorL11[iwordPosition]=eKvantor;
									}catch(Exception e){}
								}
							   }
							 
							 if(isPresent("(сравн)",morfoarrayL3[iwordPosition])){kvantorL11[iwordPosition]="6";}
							 
						 }catch(Exception e){e.printStackTrace();}
						 
						 
						 try{
						   String a="";
						   String b="";
						   String c="";
						   
						   int ia=iwordPosition-2;
						   int ib=iwordPosition-1;
						   int ic=iwordPosition;
						   
						    
							
						     	
						          
						          if(ic>=0&&ic<=size){
						    	      if(chislaarrayL9[iwordPosition]!=null){
						        	  c= chislaarrayL9[iwordPosition]; 
						              if(c.equals("5")){timeplaceL10[iwordPosition]="time";}
						              if(c.equals("6")){timeplaceL10[iwordPosition]="time";}
						    	      }
						    	      
						    	             try{
						    	             if(nwordarrayL2[iwordPosition]!=null)	 
											 if(metricsTypeOnel.containsKey(nwordarrayL2[iwordPosition])){
												 metricsL12[iwordPosition]=metricsTypeOnel.get(nwordarrayL2[iwordPosition]);
												 if(getrezult)
												 System.out.println("datavalue1="+iwordPosition+" "+nwordarrayL2[iwordPosition]+" "+metricsTypeOnel.get(nwordarrayL2[iwordPosition]));
												     
											 }
											 }catch(Exception e){}
						          
						          }
						          
						          if(ib>=0&&ib<=size){
						        	 try{	
						        		     if(nwordarrayL2[iwordPosition-1]!=null&&nwordarrayL2[iwordPosition]!=null)
											 if(metricsTypeTwol.containsKey(nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition])){
												String typeTwo= metricsTypeTwol.get(nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition]);
												 metricsL12[iwordPosition]=typeTwo;
												 metricsL12[iwordPosition-1]=typeTwo;
												 if(getrezult)
													 System.out.println("datavalue2="+iwordPosition+" "+nwordarrayL2[iwordPosition]+" "+metricsTypeOnel.get(nwordarrayL2[iwordPosition])+" typeTwo="+typeTwo);  
											 }
											
											 
											 
									  }catch(Exception e){}
						        	  
						        	  if(chislaarrayL9[iwordPosition-1]!=null){
						        	  b= chislaarrayL9[iwordPosition-1];
						        	  
						        	  if((b.equals("2")||b.equals("4"))&&metricsL12[iwordPosition]!=null){
						        		  
						        		  metricsL12[iwordPosition-1]=metricsL12[iwordPosition];
						        		  
						        	  }
						        	  
						        	  
							          if(b.equals("2")&&c.equals("6")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";}
							          else if(b.equals("4")&&c.equals("6")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";}
							          else if(b.equals("2")&&c.equals("5")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";}
							          else if(b.equals("4")&&c.equals("5")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";}
						        	  }
						          }
						          
						          
						          
						          try{ 
						          if(ia>=0&&ia<=size){

						        	  try{	if(nwordarrayL2[iwordPosition-2]!=null&&nwordarrayL2[iwordPosition-1]!=null&&nwordarrayL2[iwordPosition]!=null)
											 if(metricsTypeThreel.containsKey(nwordarrayL2[iwordPosition-2]+" "+nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition])){
												String typeThree= metricsTypeThreel.get(nwordarrayL2[iwordPosition-2]+" "+nwordarrayL2[iwordPosition-1]+" "+nwordarrayL2[iwordPosition]);
												 metricsL12[iwordPosition]=typeThree;
												 metricsL12[iwordPosition-1]=typeThree;
												 metricsL12[iwordPosition-2]=typeThree;
												 if(getrezult)
													 System.out.println("datavalue3="+iwordPosition+" "+nwordarrayL2[iwordPosition]+" "+metricsTypeOnel.get(nwordarrayL2[iwordPosition])+" typeThree="+typeThree);  
												     
											 }
						        	  
						        	 
						        	  
											 }catch(Exception e){}
						        	  
						        	  
							       if(chislaarrayL9[iwordPosition-2]!=null&&chislaarrayL9[iwordPosition-1]!=null&&chislaarrayL9[iwordPosition]!=null){
						           a= chislaarrayL9[iwordPosition-2];
						           try{
						           if((a.equals("2")||a.equals("4"))&&metricsL12[iwordPosition-1]!=null){metricsL12[iwordPosition-2]=metricsL12[iwordPosition-1];
						           if(getrezult)
										 System.out.println("datavalue4="+iwordPosition+" "+metricsL12[iwordPosition-1]+" "+metricsTypeOnel.get(nwordarrayL2[iwordPosition-1]));
						           }
						            }catch(Exception e){}
						           if(a.equals("1")&&b.equals("2")&&c.equals("5")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";timeplaceL10[iwordPosition-2]="time";}
						           else if(a.equals("1")&&b.equals("4")&&c.equals("5")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";timeplaceL10[iwordPosition-2]="time";}
						           else if(a.equals("5")&&b.equals("1")&&c.equals("2")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";timeplaceL10[iwordPosition-2]="time";}
						           else if(a.equals("5")&&b.equals("1")&&c.equals("4")){timeplaceL10[iwordPosition]="time";timeplaceL10[iwordPosition-1]="time";timeplaceL10[iwordPosition-2]="time";}
						           //в десять раз
						           else if(a.equals("1")&&(b.equals("2")||b.equals("4"))&&razProtsent.containsKey(nwordarrayL2[iwordPosition])){timeplaceL10[iwordPosition]="kvant";timeplaceL10[iwordPosition-1]="kvant";timeplaceL10[iwordPosition-2]="kvant";}
							      
							       }
							       

						          
						          }
						           }catch(Exception e){}
						    		  try{
									  if(ib>=0&&ib<=size&&b.equals("1")){  
									   String objsd ="";
									   objsd = objectarrayL6[ic];
									  
									   if(objsd!= null){ 
										   
										   String d=idcategorymap.get(objsd);
										   String[] newwordsl;
										   if(d!=null)
									  		if(d.contains("/")){
									  		  
									  			newwordsl = d.split("\\/");
									  			String val=newwordsl[1];
									  			
									  			if(val.equals("9")){
									  				timeplaceL10[iwordPosition]="place";
									  			}
								          	}
									   
									   }
									  }
								   }catch(Exception e){}
						
						     
						     
						     
						     
						 }catch(Exception e){e.printStackTrace();}  
						 
						 
						//-------------------------------обработка фраз----------------------------------------без приведения к начальным формам morfo
						//-------------------------------обработка фраз----------------------------------------без приведения к начальным формам morfo
						//-------------------------------обработка фраз----------------------------------------без приведения к начальным формам morfo
						//-------------------------------обработка фраз----------------------------------------без приведения к начальным формам morfo
						//-------------------------------обработка фраз----------------------------------------без приведения к начальным формам morfo
						//-------------------------------обработка фраз----------------------------------------без приведения к начальным формам morfo
						 
						 
						 JSONObject wordphrasejson = new JSONObject();
//wordphrasejson.put("рosition", ""+wordPosition+"-"+lowerStr);
						 
//3 поискать объекты - NB приводятся к нижнему регистру
						 
//нужно взять фонемы и приветси прил. глаг. и проч. к нормальной форме
						 //деревянный = дерево
						 //неправомерный = право
						
						 if(!igetWordExclude.containsKey(lowerStr)){
							 try{	 
								     // 
								     //начало поиска объектов
								     //одиночные слова
								     if(tokenleght.equals("1")){
								    	 
								    	 //nwordarrayL2 - нормальная форма/приветси прил. глаг. и проч. к существительному
								    	 //http://172.16.252.25:8080/krakenTest/v1/nlp/phrase/53%7C%D0%BA%D1%80%D0%B0%D1%81%D0%B8%D0%B2%D1%8B%D0%B9%7C1
								    	 //поискать синонимы если нет
								    	 //
								    	 ArrayList<String> listnword = new ArrayList<String>();

								    	 
								    	 String nword=nwordarrayL2[iwordPosition];
								    	 if(nword!=null){
								    		 String morf= morfoarrayL3[iwordPosition];
								    		 boolean issush = analysis.sushRule(morf);
								    		 if(issush){
								    			 listnword.add(nword);	 
								    		 }else{
								    			 boolean prolognar = analysis.prolognarRule(morf);
								    			 if(prolognar){
								    				 
													   String m= morfema.get(nword); 
													   if(m!=null){
														   //System.out.println("-----------m-----------"+m); 
													   if(m.contains("|")){
														   String[] mdata = m.split("\\|");   
														    int sizeb = mdata.length;
															for(int i=0;i<sizeb;i++){
																String single=mdata[i];
																 
																
																try{ String same=morfemawords.get(single);
																
															     if(same!=null){
															    	// System.out.println("-----------msame-----------"+same); 
															    	 if(
															    		 same.contains("|")){
																	     String[] msamedata = same.split("\\|");   
																	     int sizec = msamedata.length;
																	     String wordform="";
																		 for(int j=0;j<sizec;j++){
																			 String singleword=msamedata[j].trim().toLowerCase();
																			// System.out.println("-----------singleword-----------"+singleword); 
																			 String wrdi=singleword.replaceAll("[^А-я]", "").trim(); 
																			 
																			    List<String> wordBaseForms = null;
																		        if(!"".equals(wrdi)){
																					wordBaseForms = luceneMorph.getMorphInfo(wrdi);
																				}	
																				int sizer=0;
																				try{sizer=wordBaseForms.size();}catch(Exception e){}
																				
																				if(sizer>0){
																				
																				StringBuilder sb = new StringBuilder();
																				for (String s : wordBaseForms)
																				{
																				    sb.append(s);
																				    sb.append(" #");
																				}
																				
																				
																				String ist = sb.toString();
																				if(!ist.equals("")&&ist!=null){
																					wordform=ist;
																				}
																				
																			
																			
																				if(wordform.contains("|")){
																					   String[] myData = wordform.split("\\|");
																					   String inword =myData[0].trim();
																					   String imorf =myData[1];
																			           boolean iissush = analysis.sushRule(imorf);
																			           //System.out.println(iissush+"-----------inword-----------"+inword);
																			          // System.out.println(iissush+"-----------imorf-----------"+imorf); 
																				if(iissush){ listnword.add(inword);	 }
																				}
																				}
																		 }
																		
															     }
															     }

																     }catch(Exception e){e.printStackTrace();}

															}
															
													   }else{
														   
														   try{ 
														     String same=morfemawords.get(m);
														    
														     if(same!=null){if(
														    		 same.contains("|")){
																     String[] msamedata = same.split("\\|");   
																     int sizec = msamedata.length;
																     String wordform="";
																	 for(int i=0;i<sizec;i++){
																		 String singleword=msamedata[i].trim().toLowerCase();
																		 //System.out.println("-----------singleword-----------"+singleword); 
																		 String wrdi=singleword.replaceAll("[^А-я]", "").trim(); 
																		 //List<String> wordBaseForms = luceneMorph.getMorphInfo(wrdi);
																		 List<String> wordBaseForms = null;
																	        if(!"".equals(wrdi)){
																				wordBaseForms = luceneMorph.getMorphInfo(wrdi);
																			}	
																	        
																			int sizer=0;
																			try{sizer=wordBaseForms.size();}catch(Exception e){}
																			
																			if(sizer>0){
																			
																			StringBuilder sb = new StringBuilder();
																			for (String s : wordBaseForms)
																			{
																			    sb.append(s);
																			    sb.append(" #");
																			}
																			
																			
																			String ist = sb.toString();
																			if(!ist.equals("")&&ist!=null){
																				wordform=ist;
																			}
																			
																		
																		
																			if(wordform.contains("|")){
																				   String[] myData = wordform.split("\\|");
																				   String inword =myData[0].trim();
																				   String imorf =myData[1];
																		           boolean iissush = analysis.sushRule(imorf);
																		           
																		           //System.out.println(iissush+"-----------inword-----------"+inword);
																		           //System.out.println(iissush+"-----------imorf-----------"+imorf);
																			if(iissush){ listnword.add(inword);	 }
																			}
																			}
																	 }
																	
														     }
														     }
															}catch(Exception e){e.printStackTrace();}
													
													   }}
								    			 }
								    		 }
								    	 }else{
								    		 //искать синонимы слова т.к. для него нет морфологии
								    		 
								    	 }
								    	 String itemtype ="";
								    	 for(String texst:listnword){
								    		// System.out.println("-----------texstList-----------"+texst);
								    		 try{
								    			 
								    			 								    			 
									    	 List<String> qindexList = iilobjectsList.get(texst);
											     int f=0;
											     //StringBuffer sbt=new StringBuffer();
												 if(qindexList!=null)
													 for(String qindex:qindexList){
														 f++;
													    
														
														   //String values=getCategoryText(qindex, iilmaincategory, iiltext);
													       //String values=getPropertyOfType(qindex,    iilproperties);
														   //sbt = sbt.append(qindex+"category ["+values+"]");	
														 
													  	         try{List<String> list= iilmaincategory.get(qindex);
													    		 for(String c:list){
													    				String textcateg=getTextofQ(c, iiltext);  
													    				if(categoryMap.containsKey(textcateg)){int r=categoryMap.get(textcateg); categoryMap.put(textcateg, r+1);}else{categoryMap.put(textcateg, 1);}
													    		 }}catch(Exception e){}
													     
													 }
													  
												      //iilarrayL1[iwordPosition]=iilarrayL1[iwordPosition]+"/"+iwordPosition+sbt.toString();
												
													 String[] res=new String[2];
													 Map<String, List> returtQ=new HashMap<String, List>();  
													   
													 returtQ = searchMainObjectArray(qindexList, iilobjectsList,  iilinstanceList,  iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);
													 
//													 String cet=res[1];
//													 if(returtQ!=null){
//														 itemtype = returtQ.get("1");
//														 cet = returtQ.get("2");
//													if(!itemtype.equals("none")){
//														 objectMap.put(wordPosition, returtQ);
//														
//													 //iilarrayL1[iwordPosition]=iilarrayL1[iwordPosition]+"/ global type="+iwordPosition+"["+itemtype+"] cet=("+cet+")";
//													 //wordphrasejson.put("qindex"+f, ""+wordtoken[0]+"-"+wordtoken[1]+"-"+"["+itemtype+"] cet=("+cet+")"); 
//													}
//													 }

												
													 
								    	 }catch(Exception e){e.printStackTrace();}
								    	 }
								    	 
								     }
								     else{
								    
								    	 
								    	 
								    	 
								    	 
								    	 
								     List<String> qindexList = iilobjectsList.get(lowerStr);
								     String itemtype ="";	 
								     int f=0;
								     StringBuffer sbt=new StringBuffer();
									 if(qindexList!=null)
										 for(String qindex:qindexList){
											 f++;
                                             //ДОБАВИТЬ поиск в иерархии объектов по фразе	    
											 
											 //String values=getCategoryText(qindex,    iilmaincategory, iiltext);
										     //String values=getPropertyOfType(qindex,    iilproperties);
										     //sbt = sbt.append(qindex+"qcategory ["+values+"]");	
										    
								  	         try{List<String> list= iilmaincategory.get(qindex);
								    		 for(String c:list){
								    				String textcateg=getTextofQ(c, iiltext);  
								    				if(categoryMap.containsKey(textcateg)){int r=categoryMap.get(textcateg); categoryMap.put(textcateg, r+1);}else{categoryMap.put(textcateg, 1);}
								    		 }}catch(Exception e){}
										 }
									 
									 
									 //iilarrayL1[iwordPosition]=iilarrayL1[iwordPosition]+"/"+iwordPosition+sbt.toString();
									 
									 String[] res=new String[2];
									 Map<String, List> returtQ=new HashMap<String, List>();  
									   
									 returtQ = searchMainObjectArray(qindexList, iilobjectsList,  iilinstanceList,  iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);
									 
//									 String cet=res[1];
//									 if(returtQ!=null){
//										 itemtype = returtQ.get("1");
//										 cet = returtQ.get("2");
//										 if(!itemtype.equals("none")){
//									     
//											 //objectMap.put(iwordPosition, returtQ);
//									     
//										 }
//									 }
									 
								     }
									 
							     }catch(Exception e){
										e.printStackTrace();
										
							     }
						 }
						 				
						 wordlist.add(wordphrasejson);
					
						}catch(Exception e){e.printStackTrace();}
						
	} 
					
					
//					srtjson.put("wordlist", ""+wordlist); 
					
					
//расчет появления подлежащего и сказуемого по глагольному ряду в этом предолжении
					
					//создаем массив возможных пар
					
					JSONArray core = new JSONArray();
					
					
					ArrayList<String> objs = new ArrayList<String>();
					Map<String, Integer[]> resmap = new HashMap<String, Integer[]>();
					

					
					for(Integer pod:podlejashie){//pod=0 pod=7 

						for(Integer skaz:skazuemie){//skaz=2 skaz=6
							//0-2 0-6 
							//7-6 7-6
							    Integer[] inf=new Integer[3];
								inf[0]=pod;
								inf[1]=skaz;
								
								
								
								  
								    String podlej=tokens[pod].toLowerCase().trim();
									String skazuem=tokens[skaz].toLowerCase().trim();
									//может мочь быть
									
//будет, был не добавлен
									//отрицание
									int ihasNo=0;
								
								
									
									try{if(negpredlog.containsKey(nwordarrayL2[skaz-1])) ihasNo=1;}catch(Exception e){}
									
									
									
									
									if(nwordarrayL2[skaz].equals("может")||nwordarrayL2[skaz].equals("мочь")||nwordarrayL2[skaz].equals("быть")){
										try{if(negpredlog.containsKey(nwordarrayL2[skaz-1])) ihasNo=1;}catch(Exception e){}
										
										if(skaz>=0&&skaz<=size){
											boolean ex=false;
											//--->
											int lim=3;
											for(int s=skaz+1;s<skaz+lim; s++){
												 if(s<0)break;
												 if(s>=size)break;
												 
												
												 
												 if(isPresent("Г",morfoarrayL3[s])||isPresent("ИНФИНИТИВ",morfoarrayL3[s])){
													 if(nwordarrayL2[s].equals("может")||nwordarrayL2[s].equals("мочь")||nwordarrayL2[s].equals("быть")){
														 try{if(negpredlog.containsKey(nwordarrayL2[s-1])) ihasNo=1;}catch(Exception e){}
													 }else{
														 
														 try{if(negpredlog.containsKey(nwordarrayL2[s-1])) ihasNo=1;}catch(Exception e){}
														 skaz=s;
														 ex=true;
														 break;
													 }
												 }
											}
											if(getrezult)System.out.println("skazuem="+skazuem+" skaz="+skaz+" ihasNo="+ihasNo);
											//<---
											if(!ex)
											for(int s=skaz-1;s>skaz-lim;s--){
												 if(s<0)break;
												 if(s>=size)break;
												 if(isPresent("Г",morfoarrayL3[s])||isPresent("ИНФИНИТИВ",morfoarrayL3[s])){
													 if(nwordarrayL2[s].equals("может")||nwordarrayL2[s].equals("мочь")||nwordarrayL2[s].equals("быть")){
														 try{if(negpredlog.containsKey(nwordarrayL2[s-1])) ihasNo=1;}catch(Exception e){}
													 }else{
														 
														 try{if(negpredlog.containsKey(nwordarrayL2[s-1])) ihasNo=1;}catch(Exception e){}
														 skaz=s;
														 ex=true;
														 break;
													 }
												 }
											
											}

										}
									}
									//skazuem=tokens[skaz].toLowerCase().trim();
									
									
									String key=""+pod+""+skaz;
									inf[0]=pod;
									inf[1]=skaz;
									inf[2]=ihasNo;
									//if(getrezult)System.out.println("pod="+pod+" skaz="+skaz+" ihasNo="+ihasNo);
									resmap.put(key, inf);
						}
						
					}
					 
//найти ближайшее Б для анализа отношений и свойств 
//maincorelemma maincore
					 
					
					Map<Integer[], Integer> unsortMapI = new HashMap<Integer[], Integer>();
					Map<Integer[], Integer> unsortMaplemmaI = new HashMap<Integer[], Integer>();
					for (Map.Entry<String, Integer[]> entry : resmap.entrySet())
					{
						Integer[] inf=new Integer[3];
					
						inf=entry.getValue();
						
						int pod=inf[0];
						int skaz=inf[1];
						int ihasNo=inf[2];
						String wordkey=null;
						String lemmakey=null;
						int totilemma = -2;
						int	toti=-1;
						
						String podlej=tokens[pod].toLowerCase().trim();
						String skazuem=tokens[skaz].toLowerCase().trim();
						

						
						try{wordkey=podlej+" "+skazuem;}catch(Exception e){}
						
						try{lemmakey=nwordarrayL2[pod]+" "+nwordarrayL2[skaz];}catch(Exception e){}
						
						try{ if(wordkey!=null)toti=inounverbdb.get(wordkey);}catch(Exception e){}
						try{ if(lemmakey!=null)totilemma = ilemmanounverbdb.get(lemmakey);}catch(Exception e){}
								unsortMapI.put(inf,toti);
								unsortMaplemmaI.put(inf,totilemma);


					}
					int j=1;
					
					
					JSONArray maincorearray = new JSONArray();
					 for (Map.Entry<Integer[], Integer> entry : sortByValueI(unsortMapI).entrySet()) {
						  JSONObject inobj = new JSONObject();
						  JSONObject itempod = new JSONObject();
						  JSONObject itemscaz = new JSONObject();
						  //JSONObject itembefore = new JSONObject();
						  //JSONObject itemmiddle = new JSONObject();
						  //JSONObject itemafter = new JSONObject();
						  
						  int tonpos=0;
					      int tonneg=0;
					      
					      int tonposscaz=0;
					      int tonnegscaz=0;
					      
						    int negbefore=0;
				            int posbefore=0;
				            
				            int negmiddle=0;
				            int posmiddle=0;
				            
				            int negafter=0;
				            int posafter=0;
				            
				            
						 //{"type":"1","word":"Москву","strpos":"15","strend":"21", pos:"true", neg:"true"}
						    
						   
						 
						 Integer[] inf=new Integer[3];
							
							inf=entry.getKey();
							
							int pod=inf[0];
							int skaz=inf[1];
							int ihasNo=inf[2];
							  String spod=tokens[pod].trim();
							  int pbegin=0;
							  int pend=0;
							    List<IndexWrapper> list = new ArrayList<>();
							    list =  findIndexesForKeyword(singltext,spod);
							    try{
							    if(list.size()>0){
							    
							    	 pbegin=list.get(0).getStart();
							    	 pend=spod.length()+pbegin;
							   
								}
							 	}catch(Exception e){}
							    
							    
							    
							    
							 
								String sskaz=tokens[skaz].trim();
								int sbegin=0;
								int send=0;
								List<IndexWrapper> lists = new ArrayList<>();
							    lists =  findIndexesForKeyword(singltext,sskaz);
							    try{
							    if(list.size()>0){
							    
							    	 sbegin=lists.get(0).getStart();
							    	 send=spod.length()+sbegin;
							   
								}
							 	}catch(Exception e){}
							    
							  
							    
							    
							
							      int[] ipod = getNegPos( emotionarrayL4OUT[inf[0]]);
							      itempod.put("type", 1);
								  itempod.put("podid", inf[0]);
								  itempod.put("word", spod);
								  itempod.put("start", pbegin);
								  itempod.put("end", pend);
								  //itempod.put("tonneg",ipod[0]);
								  //itempod.put("tonpos",ipod[1]);
								  try{ String objsd ="";
								   objsd = objectarrayL6[pod];
								   if(objsd!= null){ itempod.put("object",""+objsd+"|"+idcategorymap.get(objsd));}}catch(Exception e){}
								  
								  String prdf="";

								  try{prdf=nwordarrayL2[pod-1];}catch(Exception e){}
								 
								  
								     try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[pod])){ if(negpredlog.containsKey(prdf)){itempod.put("tonpos",""+1); tonpos++;}else {itempod.put("tonneg",""+1);tonneg++;}}else{if(ipod[0]>0){itempod.put("tonneg",""+ipod[0]);tonneg++;}} }catch(Exception e){}
									 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[pod])){ if(negpredlog.containsKey(prdf)){itempod.put("tonneg",""+1); tonneg++;}else {itempod.put("tonpos",""+1);tonpos++;}}else{if(ipod[1]>0){itempod.put("tonpos",""+ipod[1]);tonpos++;}} }catch(Exception e){} 
									 
								  
								  
//								  int htonneg=0; 
//								  int htonpos=0; 
//								  try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[pod])) if(negpredlog.containsKey(prdf)){htonpos=1;}else{htonneg=1;} }catch(Exception e){}
//								  try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[pod])) if(negpredlog.containsKey(prdf)){htonneg=1;}else{htonpos=1;} }catch(Exception e){} 
//							   
//								    itempod.put("htonneg",""+htonneg);
//								    itempod.put("htonpos",""+htonpos);
									  
								   try{ 
								   String timeplace ="";
								   
								          
								     timeplace = timeplaceL10[pod];	
								   
								   
								   
								   
								   if(timeplace!=null)
								   itempod.put("timeplace",""+timeplace);
								   
								   
								   
								   }catch(Exception e){} 
								   
								   try{
									   String v=""+pod;
								   Map<String, String> returtQ = objectMap.get(v);
								   if(returtQ!=null)
								   {
									     StringBuffer sb=new StringBuffer();
										
										 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
										 {
											 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
										    
										 }
								 
										 itempod.put("qdata", sb.toString());
								   }
								   
					               }catch(Exception e){} 
								   
								   
								   
								  prdf="";

								  try{prdf=nwordarrayL2[skaz-1];}catch(Exception e){}
								  int[] iskaz = getNegPos( emotionarrayL4OUT[inf[1]]);  
									  
								  itemscaz.put("type", 2);
								  itemscaz.put("skazid", inf[1]);
								  itemscaz.put("word", sskaz);
								  itemscaz.put("start", sbegin);
								  itemscaz.put("end", send);
								  //itemscaz.put("tonneg",iskaz[0]);
								  //itemscaz.put("tonpos",iskaz[1]);
								  itemscaz.put("ihasno",ihasNo);
								   
								  try{ String objsd ="";
								   objsd = objectarrayL6[skaz];
								   if(objsd!= null){ itemscaz.put("object",""+objsd+"|"+idcategorymap.get(objsd));}}catch(Exception e){}
								  
								  
								  
								  
								     try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[skaz])){ if(ihasNo==1){itemscaz.put("tonpos",""+1);tonposscaz++;}else {itemscaz.put("tonneg",""+1);tonnegscaz++;}}else{if(iskaz[0]>0){itemscaz.put("tonneg",""+iskaz[0]);tonnegscaz++;}} }catch(Exception e){}
									 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[skaz])){ if(ihasNo==1){itemscaz.put("tonneg",""+1);tonnegscaz++;}else {itemscaz.put("tonpos",""+1);tonposscaz++;}}else{if(iskaz[1]>0){itemscaz.put("tonpos",""+iskaz[1]);tonposscaz++;}} }catch(Exception e){} 
									 
								 
								  
//								  int htonnegs=0;  
//								  int htonposs=0; 
//								  try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[skaz]))if(negpredlog.containsKey(prdf)){htonposs=1;}else{htonnegs=1;}}catch(Exception e){}
//								  try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[skaz]))if(negpredlog.containsKey(prdf)){htonnegs=1;}else{htonposs=1;}}catch(Exception e){}  
//								  
//								   itemscaz.put("htonneg",""+htonnegs);
//								   itemscaz.put("htonpos",""+htonposs);
//								   
								   try{ 
									  
									   String timeplace ="";   
									   timeplace = timeplaceL10[skaz];	
									   if(timeplace!=null)
										   itemscaz.put("timeplace",""+timeplace);}catch(Exception e){} 
								   
								   try{
									   String v=""+skaz;
									   Map<String, String> returtQ = objectMap.get(v);
									   if(returtQ!=null)
									   {
										     StringBuffer sb=new StringBuffer();
											
											 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
											 {
												 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
											    
											 }
									 
											 itemscaz.put("qdata", sb.toString());
									   }
									   
						               }catch(Exception e){} 
							
							String before="";
							String middle="";
							String after="";
							JSONArray corebefore = new JSONArray();
							JSONArray coremiddle = new JSONArray();
							JSONArray coreafter = new JSONArray();
							
							try{int lim=10;
							
							//1 < 9 сказуемое дальше
							if(pod<skaz){
								
								
								 if(ineededu){
									 int g=skaz+1;
									 if(g<size){
									 if(morfoarrayL3[skaz+1]!=null)
									 if(isPresent("С (жр|мр|ср),(ед|мн)", morfoarrayL3[skaz+1])){
										 
										    String firstphraseverb="";
											String secondphraseverb="";
											String threedphraseverb="";
											
											firstphraseverb=nwordarrayL2[pod];
											secondphraseverb=nwordarrayL2[skaz];
											threedphraseverb=nwordarrayL2[skaz+1];
											if(!firstphraseverb.equals(threedphraseverb)){
											if(wordPhraseCount.containsKey(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb)){
												int cont=wordPhraseCount.get(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb);
												wordPhraseCount.put(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb, cont+1); 
											}else
											wordPhraseCount.put(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb, 1);
											}
									 } 
									 
									 
									 if(tokens[skaz+1]!=null)
									 if(predlogMesta.containsKey(tokens[skaz+1])){
										 
										 if(morfoarrayL3[skaz+2]!=null){
											 
											 if(isPresent("С (жр|мр|ср),(ед|мн)", morfoarrayL3[skaz+2])){
												 
												    String firstphraseverb="";
													String secondphraseverb="";
													String threedphraseverb="";
													
													firstphraseverb=nwordarrayL2[pod];
													secondphraseverb=nwordarrayL2[skaz];
													threedphraseverb=nwordarrayL2[skaz+2];
													if(!firstphraseverb.equals(threedphraseverb)){
													if(wordPhraseCountPredlod.containsKey(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb)){
														int cont=wordPhraseCountPredlod.get(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb);
														wordPhraseCountPredlod.put(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb, cont+1); 
													}else
													wordPhraseCountPredlod.put(firstphraseverb+" "+secondphraseverb+" "+threedphraseverb, 1);
													}
											 }
											 
										 }
									 }
									 
									 }
					               }
								
								
										//           1-1    1-1>1-10(-9) 0
								inobj.put("vec", "direct");
										for(int p = pod-1;	p>pod-lim; p--){
										 if(p<0)break;
										 if(p>=size)break;
										 String l = tokens[p];
										 //<----
										 
										 //проверить если идущее слово или цифры относится к вопросу  _сколько_ ЧИСЛ десять, ЧИСЛ сравн
										 //проверить если идущее слово относится к вопросу ___КОГДА___? А-формат дат, наречия и месяцы, ЧИСЛ десятое, в десять, на десятое
										 
										 //проверить если идущее слово относится к вопросу  _ГДЕ_место(предлоги в на)
										  JSONObject beforeforarray = new JSONObject();
										  
										 try{ 
											 
											   String typevalue="";
											   typevalue=metricsL12[p];
											   if(typevalue!=null){
												   beforeforarray.put("typevalue",""+typevalue);
											   }
													   
											   String timeplace ="";   
											   timeplace = timeplaceL10[p];	
											   if(timeplace!=null){
												   
												   beforeforarray.put("timeplace",""+timeplace);
												  
											   }
											   String politeh ="";
											   politeh =politeharrayL8[p];
											   if(politeh!=null){ beforeforarray.put("politeh",""+politeh);}
											   
											   String economy ="";
											   economy =economicsarrayL15[p];
											   if(economy!=null){ beforeforarray.put("economy",""+economy);}
											   
											   String kvant ="";
											   kvant =kvantorL11[p];
											   if(kvant!=null){ beforeforarray.put("kvant",""+kvant);}
											   
											   String objsd ="";
											   objsd = objectarrayL6[p];
											  // if(objsd!= null){ beforeforarray.put("object",""+objsd);}
											   if(objsd!= null){ beforeforarray.put("object",""+objsd+"|"+idcategorymap.get(objsd));}
											   
											 
											   
											   
											   
										 }catch(Exception e){} 
										 
										 
										 int wordtype=0;
										 
										 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[p])){
											 before=before+" Б["+l+"]";
											 
											 
											 int[] is = getNegPos( emotionarrayL4OUT[p]); 
											
											 beforeforarray.put("nmb", p);
											 beforeforarray.put("type", 1);
											 beforeforarray.put("word", l);
											 
											 
											 
											 prdf="";
											 try{prdf=nwordarrayL2[p-1];}catch(Exception e){}
											  
											 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonpos",""+1);posbefore++;}else {beforeforarray.put("tonneg",""+1);negbefore++;}}else{if(is[0]>0){beforeforarray.put("tonneg",""+is[0]);negbefore++;}} }catch(Exception e){}
											 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonneg",""+1);negbefore++;}else {beforeforarray.put("tonpos",""+1);posbefore++;}}else{if(is[1]>0){beforeforarray.put("tonpos",""+is[1]);posbefore++;}} }catch(Exception e){} 
											
											 
												try{
													String wordpos=tokens[p].trim();
													int beginpos=0;
													int endpos=0;
													List<IndexWrapper> listspos = new ArrayList<>();
													listspos =  findIndexesForKeyword(singltext,wordpos);
													
													if(listspos.size()>0){
													
														beginpos=listspos.get(0).getStart();
														endpos=wordpos.length()+beginpos;
												   
													}
													
													
													beforeforarray.put("start", beginpos);
													beforeforarray.put("end", endpos);
												  
												  }catch(Exception e){}
												
												
												     try{
												    	 String v=""+p;
													   Map<String, String> returtQ = objectMap.get(v);
													   if(returtQ!=null)
													   {
														     StringBuffer sb=new StringBuffer();
															
															 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
															 {
																 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
															    
															 }
													 
															 beforeforarray.put("qdata", sb.toString());
													   }
													   
										               }catch(Exception e){} 
												
										 }
										 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[p])){
											 before=before+" "+l;	
											
											 int[] is = getNegPos( emotionarrayL4OUT[p]); 
											 beforeforarray.put("nmb", p);
											 beforeforarray.put("type", 0);
											 beforeforarray.put("word", l);
											 beforeforarray.put("tonneg",""+is[0]);
											 beforeforarray.put("tonpos",""+is[1]);
											 
											 prdf="";
											 try{prdf=nwordarrayL2[p-1];}catch(Exception e){}
											 
											 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonpos",""+1);posbefore++;}else {beforeforarray.put("tonneg",""+1);negbefore++;}}else{if(is[0]>0){beforeforarray.put("tonneg",""+is[0]);negbefore++;}} }catch(Exception e){}
											 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonneg",""+1);negbefore++;}else {beforeforarray.put("tonpos",""+1);posbefore++;}}else{if(is[1]>0){beforeforarray.put("tonpos",""+is[1]);posbefore++;}} }catch(Exception e){} 


											 try{
													String wordpos=tokens[p].trim();
													int beginpos=0;
													int endpos=0;
													List<IndexWrapper> listspos = new ArrayList<>();
													listspos =  findIndexesForKeyword(singltext,wordpos);
													
													if(listspos.size()>0){
													
														beginpos=listspos.get(0).getStart();
														endpos=wordpos.length()+beginpos;
												   
													}
													
													
													beforeforarray.put("start", beginpos);
													beforeforarray.put("end", endpos);
												  
												  }catch(Exception e){}
											 
											 
										     try{
										    	 
										       String v=""+p;	 
											   Map<String, String> returtQ = objectMap.get(v);
											   if(returtQ!=null)
											   {
												     StringBuffer sb=new StringBuffer();
													
													 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
													 {
														 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
													    
													 }
											 
													 beforeforarray.put("qdata", sb.toString());
											   }
											   
								               }catch(Exception e){} 
										 }
										 corebefore.add(beforeforarray);
										}
										
										//посередине, между подлижащим и сказуемым нет данных
										//--->
										for(int s=pod+1;s<skaz; s++){
											 if(s<0)break;
											 if(s>=size)break;
											 String l = tokens[s];
											  JSONObject middlearray = new JSONObject();
											 try{ 
												   String typevalue="";
												   typevalue=metricsL12[s];
												   if(typevalue!=null){
													   middlearray.put("typevalue",""+typevalue);
												   }
												   
												   String timeplace ="";   
												   timeplace = timeplaceL10[s];	
												   if(timeplace!=null){
													 
													   middlearray.put("timeplace",""+timeplace);
													   
												   }
												       String politeh ="";
													   politeh =politeharrayL8[s];
													   if(politeh!=null){ middlearray.put("politeh",""+politeh);}
													   
													   String economy ="";
													   economy =economicsarrayL15[s];
													   if(economy!=null){ middlearray.put("economy",""+economy);}
													   
													   String kvant ="";
													   kvant =kvantorL11[s];
													   if(kvant!=null){ middlearray.put("kvant",""+kvant);}
													   
													   String objsd ="";
													   objsd = objectarrayL6[s];
													   if(objsd!= null){ middlearray.put("object",""+objsd+"|"+idcategorymap.get(objsd));}
													   
													   
											 }catch(Exception e){} 
											 
											 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
												 middle=middle+" Б["+l+"]"; 
												 int[] is = getNegPos( emotionarrayL4OUT[s]); 
												 
												 
												 
												
												 middlearray.put("nmb", s);
												 middlearray.put("type", 1);
												 middlearray.put("word", l);
												 middlearray.put("tonneg",""+is[0]);
												 middlearray.put("tonpos",""+is[1]);
												 
												 prdf="";
												 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
												 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonpos",""+1);posmiddle++;}else {middlearray.put("tonneg",""+1);negmiddle++;}}else{if(is[0]>0){middlearray.put("tonneg",""+is[0]);negmiddle++;}} }catch(Exception e){}
												 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonneg",""+1);negmiddle++;}else {middlearray.put("tonpos",""+1);posmiddle++;}}else{if(is[1]>0){middlearray.put("tonpos",""+is[1]);posmiddle++;}} }catch(Exception e){} 
												
												 
												 try{
														String wordpos=tokens[s].trim();
														int beginpos=0;
														int endpos=0;
														List<IndexWrapper> listspos = new ArrayList<>();
														listspos =  findIndexesForKeyword(singltext,wordpos);
														
														if(listspos.size()>0){
														
															beginpos=listspos.get(0).getStart();
															endpos=wordpos.length()+beginpos;
													   
														}
														
														
														middlearray.put("start", beginpos);
														middlearray.put("end", endpos);
													  
													  }catch(Exception e){}
												 
											     try{
											    	 String v=""+s;
												   Map<String, String> returtQ = objectMap.get(v);
												   if(returtQ!=null)
												   {
													     StringBuffer sb=new StringBuffer();
														
														 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
														 {
															 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
														    
														 }
												 
														 middlearray.put("qdata", sb.toString());
												   }
												   
									               }catch(Exception e){} 
											 }
											 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
												 middle=middle+" "+l;	
												 
												 
												 
												 int[] is = getNegPos( emotionarrayL4OUT[s]); 
												 middlearray.put("nmb", s);
												 middlearray.put("type", 0);
												 middlearray.put("word", l);
												 middlearray.put("tonneg",""+is[0]);
												 middlearray.put("tonpos",""+is[1]);
												 prdf="";
												 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
												 
												 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonpos",""+1);posmiddle++;}else {middlearray.put("tonneg",""+1);negmiddle++;}}else{if(is[0]>0){middlearray.put("tonneg",""+is[0]);negmiddle++;}} }catch(Exception e){}
												 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonneg",""+1);negmiddle++;}else {middlearray.put("tonpos",""+1);posmiddle++;}}else{if(is[1]>0){middlearray.put("tonpos",""+is[1]);posmiddle++;}} }catch(Exception e){} 


												       try{
														String wordpos=tokens[s].trim();
														int beginpos=0;
														int endpos=0;
														List<IndexWrapper> listspos = new ArrayList<>();
														listspos =  findIndexesForKeyword(singltext,wordpos);
														
														if(listspos.size()>0){
														
															beginpos=listspos.get(0).getStart();
															endpos=wordpos.length()+beginpos;
													   
														}
														
														
														middlearray.put("start", beginpos);
														middlearray.put("end", endpos);
													  
													  }catch(Exception e){}
												       
												       try{
												    	   String v=""+s;
														   Map<String, String> returtQ = objectMap.get(v);
														   if(returtQ!=null)
														   {
															     StringBuffer sb=new StringBuffer();
																
																 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
																 {
																	 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
																    
																 }
														 
																 middlearray.put("qdata", sb.toString());
														   }
														   
											               }catch(Exception e){} 
											 }
											 
											 coremiddle.add(middlearray);
										}
										
										
										
										
										int calcvalue=0;
										
										//9 = 9+1  9+1<9+10
										
										for(int s=skaz+1;s<skaz+lim; s++){
											 if(s<0)break;
											 if(s>=size)break;
											 String l = tokens[s];
											 JSONObject afterarray = new JSONObject();
											 if(nwordarrayL2[s]!=null)
											 if(triplet.containsKey(nwordarrayL2[inf[0]]+" "+nwordarrayL2[inf[1]]+" "+nwordarrayL2[s])){
												 int curval=triplet.get(nwordarrayL2[inf[0]]+" "+nwordarrayL2[inf[1]]+" "+nwordarrayL2[s]);
												 if(calcvalue<curval){calcvalue=curval;
												 afterarray.put("triplet", nwordarrayL2[s]);}
											 }
											 
											 try{ 
												 String typevalue="";
												   typevalue=metricsL12[s];
												   if(typevalue!=null){
													   afterarray.put("typevalue",""+typevalue);
												   }
												   
												   String timeplace ="";   
												   timeplace = timeplaceL10[s];	
												   if(timeplace!=null){
													 
													   afterarray.put("timeplace",""+timeplace);
													  
													   
												   } 
												   String politeh ="";
													   politeh =politeharrayL8[s];
													   if(politeh!=null){ afterarray.put("politeh",""+politeh);}
													   String economy ="";
													   economy =economicsarrayL15[s];
													   if(economy!=null){ afterarray.put("economy",""+economy);}
													   
													   String kvant ="";
													   kvant =kvantorL11[s];
													   if(kvant!=null){ afterarray.put("kvant",""+kvant);}
													   
													   String objsd ="";
													   objsd = objectarrayL6[s];
													   if(objsd!= null){ afterarray.put("object",""+objsd+"|"+idcategorymap.get(objsd));}
													   //if(objsd!= null){ afterarray.put("object",""+objsd);}
											 }catch(Exception e){} 
											 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
												 after=after+" Б["+l+"]"; 
												 
												 int[] is = getNegPos( emotionarrayL4OUT[s]);
											
												
												 afterarray.put("nmb", s);
												 afterarray.put("type", 1);
												 afterarray.put("word", l);
												 afterarray.put("tonneg",""+is[0]);
												 afterarray.put("tonpos",""+is[1]);
												 
												 prdf="";
												 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
												 
												 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonpos",""+1);posafter++;}else {afterarray.put("tonneg",""+1);negafter++;}}else{if(is[0]>0){afterarray.put("tonneg",""+is[0]);negafter++;}} }catch(Exception e){}
												 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonneg",""+1);negafter++;}else {afterarray.put("tonpos",""+1);posafter++;}}else{if(is[1]>0){afterarray.put("tonpos",""+is[1]);posafter++;}} }catch(Exception e){} 
												 
												 
												 try{
														String wordpos=tokens[s].trim();
														int beginpos=0;
														int endpos=0;
														List<IndexWrapper> listspos = new ArrayList<>();
														listspos =  findIndexesForKeyword(singltext,wordpos);
														
														if(listspos.size()>0){
														
															beginpos=listspos.get(0).getStart();
															endpos=wordpos.length()+beginpos;
													   
														}
														
														
														afterarray.put("start", beginpos);
														afterarray.put("end", endpos);
													  
													  }catch(Exception e){}
												 
												 try{
													 String v=""+s;
													   Map<String, String> returtQ = objectMap.get(v);
													   if(returtQ!=null)
													   {
														     StringBuffer sb=new StringBuffer();
															
															 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
															 {
																 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
															    
															 }
													 
															 afterarray.put("qdata", sb.toString());
													   }
													   
										               }catch(Exception e){} 
											 }
											 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
												 after=after+" "+l;	
												
												 int[] is = getNegPos( emotionarrayL4OUT[s]);
												 afterarray.put("nmb", s);
												 afterarray.put("type", 0);
												 afterarray.put("word", l);
												 afterarray.put("tonneg",""+is[0]);
												 afterarray.put("tonpos",""+is[1]);
												 prdf="";
												 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
												 
												 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonpos",""+1);posafter++;}else {afterarray.put("tonneg",""+1);negafter++;}}else{if(is[0]>0){afterarray.put("tonneg",""+is[0]);negafter++;}} }catch(Exception e){}
												 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonneg",""+1);negafter++;}else {afterarray.put("tonpos",""+1);posafter++;}}else{if(is[1]>0){afterarray.put("tonpos",""+is[1]);posafter++;}} }catch(Exception e){} 
												 
												 try{
														String wordpos=tokens[s].trim();
														int beginpos=0;
														int endpos=0;
														List<IndexWrapper> listspos = new ArrayList<>();
														listspos =  findIndexesForKeyword(singltext,wordpos);
														
														if(listspos.size()>0){
														
															beginpos=listspos.get(0).getStart();
															endpos=wordpos.length()+beginpos;
													   
														}
														
														
														afterarray.put("start", beginpos);
														afterarray.put("end", endpos);
													  
													  }catch(Exception e){}
												 
												 try{
													 String v=""+s;
													   Map<String, String> returtQ = objectMap.get(v);
													   if(returtQ!=null)
													   {
														     StringBuffer sb=new StringBuffer();
															
															 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
															 {
																 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
															    
															 }
													 
															 afterarray.put("qdata", sb.toString());
													   }
													   
										               }catch(Exception e){} 
												 
											 }
											 coreafter.add(afterarray);
										}
										
							 String wordkey="";
								//try{wordkey=tokens[pod].toLowerCase().trim()+" "+tokens[skaz].toLowerCase().trim();}catch(Exception e){}
								//maincore.add(" *"+pod+"/"+skaz+before+"("+wordkey+")"+after+"=["+entry.getValue()+"]");
							 	//maincore.add("-- pod="+pod+"/skaz="+skaz+" текст="+before+" ("+spod+")"+middle+"("+sskaz+") "+after+"=["+entry.getValue()+"]");			
							 	   
							 	   //itembefore.put("before", before);
								   //itemmiddle.put("middle", middle);
								   //itemafter.put("after", after);	
							}
							
							//7 6
							  int calcvalue=0;
							  
							  if(pod>skaz){
								  inobj.put("vec", "indirect");
								//<-----	
								for(int s=skaz-1;s>skaz-lim;s--){
									 if(s<0)break;
									 if(s>=size)break;
									 String l = tokens[s];
									 JSONObject afterarray = new JSONObject();
									 try{ 
										 if(nwordarrayL2[s]!=null)
											 if(triplet.containsKey(nwordarrayL2[inf[0]]+" "+nwordarrayL2[inf[1]]+" "+nwordarrayL2[s])){
												 int curval=triplet.get(nwordarrayL2[inf[0]]+" "+nwordarrayL2[inf[1]]+" "+nwordarrayL2[s]);
												 if(calcvalue<curval){calcvalue=curval;
												 afterarray.put("triplet", nwordarrayL2[s]);}
											 }
										 String typevalue="";
										   typevalue=metricsL12[s];
										   if(typevalue!=null){
											   afterarray.put("typevalue",""+typevalue);
										   }
										   
										   String timeplace ="";   
										   timeplace = timeplaceL10[s];	
										   if(timeplace!=null){
											   
											   afterarray.put("timeplace",""+timeplace);
											   
											  
										   }
										   String politeh ="";
											   politeh =politeharrayL8[s];
											   if(politeh!=null){ afterarray.put("politeh",""+politeh);}
											   
											   String economy ="";
											   economy =economicsarrayL15[s];
											   if(economy!=null){ afterarray.put("economy",""+economy);}
											   
											   String kvant ="";
											   kvant =kvantorL11[s];
											   if(kvant!=null){ afterarray.put("kvant",""+kvant);}
											   
											   String objsd ="";
											   objsd = objectarrayL6[s];
											   if(objsd!= null){ afterarray.put("object",""+objsd+"|"+idcategorymap.get(objsd));}
											   //if(objsd!= null){ afterarray.put("object",""+objsd);}
									 }catch(Exception e){} 
									 
									 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
										 after=after+" Б["+l+"]";  
										 
										 int[] is = getNegPos( emotionarrayL4OUT[s]);
										 //itemafter.put("word", l);
										 //itemafter.put("tonneg",""+is[0]);
										 //itemafter.put("tonpos",""+is[1]);
										
										 
										
										 afterarray.put("nmb", s);
										 afterarray.put("type", 1);
										 afterarray.put("word", l);
										 afterarray.put("tonneg",""+is[0]);
										 afterarray.put("tonpos",""+is[1]);
										 prdf="";
										 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
										 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonpos",""+1);posafter++;}else {afterarray.put("tonneg",""+1);negafter++;}}else{if(is[0]>0){afterarray.put("tonneg",""+is[0]);negafter++;}} }catch(Exception e){}
										 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonneg",""+1);negafter++;}else {afterarray.put("tonpos",""+1);posafter++;}}else{if(is[1]>0){afterarray.put("tonpos",""+is[1]);posafter++;}} }catch(Exception e){} 
										 
										 try{
												String wordpos=tokens[s].trim();
												int beginpos=0;
												int endpos=0;
												List<IndexWrapper> listspos = new ArrayList<>();
												listspos =  findIndexesForKeyword(singltext,wordpos);
												
												if(listspos.size()>0){
												
													beginpos=listspos.get(0).getStart();
													endpos=wordpos.length()+beginpos;
											   
												}
												
												
												afterarray.put("start", beginpos);
												afterarray.put("end", endpos);
											  
											  }catch(Exception e){}
										 
										 try{
											 String v=""+s;
											   Map<String, String> returtQ = objectMap.get(v);
											   if(returtQ!=null)
											   {
												     StringBuffer sb=new StringBuffer();
													
													 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
													 {
														 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
													    
													 }
											 
													 afterarray.put("qdata", sb.toString());
											   }
											   
								               }catch(Exception e){} 
									 }
									 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
										 before=before+" "+l;	
										
										 int[] is = getNegPos( emotionarrayL4OUT[s]);
										 afterarray.put("nmb", s);
										 afterarray.put("type", 0);
										 afterarray.put("word", l);
										 afterarray.put("tonneg",""+is[0]);
										 afterarray.put("tonpos",""+is[1]);
										 prdf="";
										 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
										 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonpos",""+1);posafter++;}else {afterarray.put("tonneg",""+1);negafter++;}}else{if(is[0]>0){afterarray.put("tonneg",""+is[0]);negafter++;}} }catch(Exception e){}
										 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){afterarray.put("tonneg",""+1);negafter++;}else {afterarray.put("tonpos",""+1);posafter++;}}else{if(is[1]>0){afterarray.put("tonpos",""+is[1]);posafter++;}} }catch(Exception e){} 
										 
										 try{
												String wordpos=tokens[s].trim();
												int beginpos=0;
												int endpos=0;
												List<IndexWrapper> listspos = new ArrayList<>();
												listspos =  findIndexesForKeyword(singltext,wordpos);
												
												if(listspos.size()>0){
												
													beginpos=listspos.get(0).getStart();
													endpos=wordpos.length()+beginpos;
											   
												}
												
												
												afterarray.put("start", beginpos);
												afterarray.put("end", endpos);
											  
											  }catch(Exception e){}
										 
										 try{
											 String v=""+s;
											   Map<String, String> returtQ = objectMap.get(v);
											   if(returtQ!=null)
											   {
												     StringBuffer sb=new StringBuffer();
													
													 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
													 {
														 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
													    
													 }
											 
													 afterarray.put("qdata", sb.toString());
											   }
											   
								               }catch(Exception e){} 
									 }
									 coreafter.add(afterarray);
								}
								//8
								
								//посередине, между подлижащим и сказуемым нет данных
								for(int s=skaz+1;s<pod; s++){
									 if(s<0)break;
									 if(s>=size)break;
									 String l = tokens[s];
									 JSONObject middlearray = new JSONObject();
									 try{ 
										 String typevalue="";
										   typevalue=metricsL12[s];
										   if(typevalue!=null){
											   middlearray.put("typevalue",""+typevalue);
										   }
										   String timeplace ="";   
										   timeplace = timeplaceL10[s];	
										   if(timeplace!=null){
											  
											   middlearray.put("timeplace",""+timeplace);
											   
											  
											  
										   }
										   String politeh ="";
											   politeh =politeharrayL8[s];
											   if(politeh!=null){ middlearray.put("politeh",""+politeh);}
											   
											   String economy ="";
											   economy =economicsarrayL15[s];
											   if(economy!=null){ middlearray.put("economy",""+economy);}
											   
											   String kvant ="";
											   kvant =kvantorL11[s];
											   if(kvant!=null){ middlearray.put("kvant",""+kvant);}
											   
											   
											   String objsd ="";
											   objsd = objectarrayL6[s];
											   if(objsd!= null){ middlearray.put("object",""+objsd+"|"+idcategorymap.get(objsd));}
											   //if(objsd!= null){ middlearray.put("object",""+objsd);}
									 }catch(Exception e){} 
									 
									 
									 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
										 middle=middle+" Б["+l+"]"; 
										 
										 int[] is = getNegPos( emotionarrayL4OUT[s]); 
										 //itemmiddle.put("word", l);
										 //itemmiddle.put("tonneg",""+is[0]);
										 //itemmiddle.put("tonpos",""+is[1]);
										 
										 
										
										 middlearray.put("nmb", s);
										 middlearray.put("type", 1);
										 middlearray.put("word", l);
										 middlearray.put("tonneg",""+is[0]);
										 middlearray.put("tonpos",""+is[1]);
										 prdf="";
										 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
										 
										 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonpos",""+1);posmiddle++;}else {middlearray.put("tonneg",""+1);negmiddle++;}}else{if(is[0]>0){middlearray.put("tonneg",""+is[0]);negmiddle++;}} }catch(Exception e){}
										 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonneg",""+1);negmiddle++;}else {middlearray.put("tonpos",""+1);posmiddle++;}}else{if(is[1]>0){middlearray.put("tonpos",""+is[1]);posmiddle++;}} }catch(Exception e){} 
										 try{
												String wordpos=tokens[s].trim();
												int beginpos=0;
												int endpos=0;
												List<IndexWrapper> listspos = new ArrayList<>();
												listspos =  findIndexesForKeyword(singltext,wordpos);
												
												if(listspos.size()>0){
												
													beginpos=listspos.get(0).getStart();
													endpos=wordpos.length()+beginpos;
											   
												}
												
												
												middlearray.put("start", beginpos);
												middlearray.put("end", endpos);
											  
											  }catch(Exception e){}
										 
										 try{
											 String v=""+s;
											   Map<String, String> returtQ = objectMap.get(v);
											   if(returtQ!=null)
											   {
												     StringBuffer sb=new StringBuffer();
													
													 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
													 {
														 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
													    
													 }
											 
													 middlearray.put("qdata", sb.toString());
											   }
											   
								               }catch(Exception e){} 
									 }
									 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
										 middle=middle+" "+l;	
										
										
										 int[] is = getNegPos( emotionarrayL4OUT[s]); 
										 middlearray.put("nmb", s);
										 middlearray.put("type", 0);
										 middlearray.put("word", l);
										 middlearray.put("tonneg",""+is[0]);
										 middlearray.put("tonpos",""+is[1]);
										 prdf="";
										 try{prdf=nwordarrayL2[s-1];}catch(Exception e){}
										 
										 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonpos",""+1);posmiddle++;}else {middlearray.put("tonneg",""+1);negmiddle++;}}else{if(is[0]>0){middlearray.put("tonneg",""+is[0]);negmiddle++;}} }catch(Exception e){}
										 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[s])){ if(negpredlog.containsKey(prdf)){middlearray.put("tonneg",""+1);negmiddle++;}else {middlearray.put("tonpos",""+1);posmiddle++;}}else{if(is[1]>0){middlearray.put("tonpos",""+is[1]);posmiddle++;}} }catch(Exception e){} 


										 try{
												String wordpos=tokens[s].trim();
												int beginpos=0;
												int endpos=0;
												List<IndexWrapper> listspos = new ArrayList<>();
												listspos =  findIndexesForKeyword(singltext,wordpos);
												
												if(listspos.size()>0){
												
													beginpos=listspos.get(0).getStart();
													endpos=wordpos.length()+beginpos;
											   
												}
												
												
												middlearray.put("start", beginpos);
												middlearray.put("end", endpos);
											  
											  }catch(Exception e){}
										 
										 try{
											 String v=""+s;
											   Map<String, String> returtQ = objectMap.get(v);
											   if(returtQ!=null)
											   {
												     StringBuffer sb=new StringBuffer();
													
													 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
													 {
														 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
													    
													 }
											 
													 middlearray.put("qdata", sb.toString());
											   }
											   
								               }catch(Exception e){} 
										 
									 }
									 coremiddle.add(middlearray);
								}
								
								for(int p=pod+1;p<pod+lim;p++){
								 if(p<0)break;
								 if(p>=size)break;
								 String l = tokens[p];
								  JSONObject beforeforarray = new JSONObject();
								  
								  try{ 
									  
									  String typevalue="";
									   typevalue=metricsL12[p];
									   if(typevalue!=null){
										   beforeforarray.put("typevalue",""+typevalue);
									   }
									   String timeplace ="";   
									   timeplace = timeplaceL10[p];	
									   if(timeplace!=null){
										   //JSONObject beforeforarray = new JSONObject();
										   beforeforarray.put("timeplace",""+timeplace);
										  
										   
										   
									   }
									   String politeh ="";
										   politeh =politeharrayL8[p];
										   if(politeh!=null){ beforeforarray.put("politeh",""+politeh);}
										   
										   String economy ="";
										   economy =economicsarrayL15[p];
										   if(economy!=null){ beforeforarray.put("economy",""+economy);}
										   
										   String kvant ="";
										   kvant =kvantorL11[p];
										   if(kvant!=null){ beforeforarray.put("kvant",""+kvant);}
										   
										   
										   String objsd ="";
										   objsd = objectarrayL6[p];
										   if(objsd!= null){ beforeforarray.put("object",""+objsd+"|"+idcategorymap.get(objsd));}
										   //if(objsd!= null){ beforeforarray.put("object",""+objsd);}
								  }catch(Exception e){} 
								 
								 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[p])){
									 before=before+" "+l;	
									 //break;
									 
									 int[] is = getNegPos( emotionarrayL4OUT[p]); 
									 //itembefore.put("type", 1);
									 //itembefore.put("word", l);
									 //itembefore.put("tonneg",""+is[0]);
									 //itembefore.put("tonpos",""+is[1]);
									 
									
									 beforeforarray.put("nmb", p);
									 beforeforarray.put("type", 1);
									 beforeforarray.put("word", l);
									 beforeforarray.put("tonneg",""+is[0]);
									 beforeforarray.put("tonpos",""+is[1]);
									 prdf="";
									 try{prdf=nwordarrayL2[p-1];}catch(Exception e){}
									  
									 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonpos",""+1);posbefore++;}else {beforeforarray.put("tonneg",""+1);negbefore++;}}else{if(is[0]>0){beforeforarray.put("tonneg",""+is[0]);negbefore++;}} }catch(Exception e){}
									 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonneg",""+1);negbefore++;}else {beforeforarray.put("tonpos",""+1);posbefore++;}}else{if(is[1]>0){beforeforarray.put("tonpos",""+is[1]);posbefore++;}} }catch(Exception e){} 


									 
									 try{
											String wordpos=tokens[p].trim();
											int beginpos=0;
											int endpos=0;
											List<IndexWrapper> listspos = new ArrayList<>();
											listspos =  findIndexesForKeyword(singltext,wordpos);
											
											if(listspos.size()>0){
											
												beginpos=listspos.get(0).getStart();
												endpos=wordpos.length()+beginpos;
										   
											}
											
											
											beforeforarray.put("start", beginpos);
											beforeforarray.put("end", endpos);
										  
										  }catch(Exception e){}
									 try{
										 String v=""+p;
										   Map<String, String> returtQ = objectMap.get(v);
										   if(returtQ!=null)
										   {
											     StringBuffer sb=new StringBuffer();
												
												 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
												 {
													 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
												    
												 }
										 
												 beforeforarray.put("qdata", sb.toString());
										   }
										   
							               }catch(Exception e){} 
								 }
								 
								 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[p])){
									 after=after+" "+l;	
									 
									// JSONObject beforeforarray = new JSONObject();
									 int[] is = getNegPos( emotionarrayL4OUT[p]); 
									 beforeforarray.put("nmb", p);
									 beforeforarray.put("type", 0);
									 beforeforarray.put("word", l);
									 beforeforarray.put("tonneg",""+is[0]);
									 beforeforarray.put("tonpos",""+is[1]);
									 prdf="";
									 try{prdf=nwordarrayL2[p-1];}catch(Exception e){}
									  
									 try{ if(iwordNegativeHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonpos",""+1);posbefore++;}else {beforeforarray.put("tonneg",""+1);negbefore++;}}else{if(is[0]>0){beforeforarray.put("tonneg",""+is[0]);negbefore++;}} }catch(Exception e){}
									 try{ if(iwordPositiveHuman.containsKey(nwordarrayL2[p])){ if(negpredlog.containsKey(prdf)){beforeforarray.put("tonneg",""+1);negbefore++;}else {beforeforarray.put("tonpos",""+1);posbefore++;}}else{if(is[1]>0){beforeforarray.put("tonpos",""+is[1]);posbefore++;}} }catch(Exception e){} 


									 
									 try{
											String wordpos=tokens[p].trim();
											int beginpos=0;
											int endpos=0;
											List<IndexWrapper> listspos = new ArrayList<>();
											listspos =  findIndexesForKeyword(singltext,wordpos);
											
											if(listspos.size()>0){
											
												beginpos=listspos.get(0).getStart();
												endpos=wordpos.length()+beginpos;
										   
											}
											
											
											beforeforarray.put("start", beginpos);
											beforeforarray.put("end", endpos);
										  
										  }catch(Exception e){}
									 
									 
									 try{ String v=""+p;
										   Map<String, String> returtQ = objectMap.get(v);
										   if(returtQ!=null)
										   {
											     StringBuffer sb=new StringBuffer();
												
												 for (Map.Entry<String, String> entryQ : returtQ.entrySet())
												 {
													 sb.append(""+entryQ.getKey()+"|"+entryQ.getValue()+"n ");
												    
												 }
										 
												 beforeforarray.put("qdata", sb.toString());
										   }
										   
							               }catch(Exception e){} 
									 
									
								 }
								 corebefore.add(beforeforarray);
								}
								
								 String wordkey="";
									//try{wordkey=tokens[pod].toLowerCase().trim()+" "+tokens[skaz].toLowerCase().trim();}catch(Exception e){}
									//maincore.add(" *"+pod+"/"+skaz+before+"("+wordkey+")"+after+"=["+entry.getValue()+"]");
									
//								  String spod=tokens[pod].toLowerCase().trim();
//								  String sskaz=tokens[skaz].toLowerCase().trim();
								  //maincore.add("-- pod="+pod+"/skaz="+skaz+" текст="+before+" ("+sskaz+")"+middle+"("+spod+") "+after+"=["+entry.getValue()+"]");
							
							  
								  
							  
							  }
							  
							  inobj.put("corebefore", corebefore);
							  inobj.put("coremiddle", coremiddle);
							  inobj.put("coreafter", coreafter);
							  
							  //inobj.put("before", itembefore);
							  //inobj.put("middle", itemmiddle);
							  //inobj.put("after", itemafter);
							  inobj.put("itempod", itempod);
							  inobj.put("itemscaz", itemscaz);
							  maincorearray.add(inobj);
								
						}catch(Exception e){e.printStackTrace();}
						 
							
							
							  tonpos=tonpos+posbefore;
						      tonneg=tonneg+negbefore;
							
							  tonposscaz=tonposscaz+posafter;
						      tonnegscaz=tonnegscaz+negafter;
						      
						      	// negmiddle=0;
					            // posmiddle=0;
							 

					            int negt = coreemmotionMap.get("negative"); coreemmotionMap.put("negative", negt+tonneg+tonnegscaz);
							  	int post = coreemmotionMap.get("positive"); coreemmotionMap.put("positive", post+tonpos+tonposscaz);
							
						 j++;
							if(j>1)break;
				     }
					 
//					 j=1;
//					 JSONArray maincorearraylemma = new JSONArray();
//					 for (Map.Entry<Integer[], Integer> entry : sortByValueI(unsortMaplemmaI).entrySet()) {
//						  JSONObject inobj = new JSONObject();
//						  JSONObject itempod = new JSONObject();
//						  JSONObject itemscaz = new JSONObject();
//						  JSONObject itembefore = new JSONObject();
//						  JSONObject itemmiddle = new JSONObject();
//						  JSONObject itemafter = new JSONObject();
//						 
//						    
//						   
//						 
//						 Integer[] inf=new Integer[2];
//							
//							inf=entry.getKey();
//							
//							int pod=inf[0];
//							int skaz=inf[1];
//							  String spod=tokens[pod].trim();
//							  int pbegin=0;
//							  int pend=0;
//							    List<IndexWrapper> list = new ArrayList<>();
//							    list =  findIndexesForKeyword(singltext,spod);
//							    try{
//							    if(list.size()>0){
//							    
//							    	 pbegin=list.get(0).getStart();
//							    	 pend=spod.length()+pbegin;
//							   
//								}
//							 	}catch(Exception e){e.printStackTrace();}
//							    
//							    
//							    
//							    
//							 
//								String sskaz=tokens[skaz].trim();
//								int sbegin=0;
//								int send=0;
//								List<IndexWrapper> lists = new ArrayList<>();
//							    lists =  findIndexesForKeyword(singltext,sskaz);
//							    try{
//							    if(list.size()>0){
//							    
//							    	 sbegin=lists.get(0).getStart();
//							    	 send=spod.length()+sbegin;
//							   
//								}
//							 	}catch(Exception e){e.printStackTrace();}
//							    
//							  
//							   int[] ipod = getNegPos( emotionarrayL4OUT[inf[0]]);
//							    
//							    
//							    
//							  itempod.put("type", 1);
//							  itempod.put("podid", inf[0]);
//							  itempod.put("word", spod);
//							  itempod.put("strpos", pbegin);
//							  itempod.put("strend", pend);
//							  itempod.put("tonneg",ipod[0]);
//							  itempod.put("tonpos",ipod[1]);
//							 
//							  int[] iskaz = getNegPos( emotionarrayL4OUT[inf[1]]);  
//								  
//							  itemscaz.put("type", 2);
//							  itemscaz.put("skazid", inf[1]);
//							  itemscaz.put("word", sskaz);
//							  itemscaz.put("strpos", sbegin);
//							  itemscaz.put("strend", send);
//							  itemscaz.put("tonneg",iskaz[0]);
//							  itemscaz.put("tonpos",iskaz[1]);
//							String before="";
//							String middle="";
//							String after="";
//							try{int lim=10;
//							
//							//1 < 9 сказуемое дальше
//							if(pod<skaz){
//										//           1-1    1-1>1-10(-9) 0
//										for(int p = pod-1;	p>pod-lim; p--){
//										 if(p<0)break;
//										 if(p>=size)break;
//										 String l = tokens[p];
//										
//										 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[p])){
//											 before=before+" Б["+l+"]";
//											 //break;
//										 }
//										 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[p])){
//											 before=before+" "+l;	
//											
//										 }
//										 
//										}
//										
//										//посередине, между подлижащим и сказуемым нет данных
//										for(int s=pod+1;s<skaz; s++){
//											 if(s<0)break;
//											 if(s>=size)break;
//											 String l = tokens[s];
//											 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
//												 middle=middle+" Б["+l+"]"; 
//												 //break;	
//											 }
//											 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
//												 middle=middle+" "+l;	
//												
//											 }
//										}
//										
//										
//										
//										
//										      //9 = 9+1  9+1<9+10
//										for(int s=skaz+1;s<skaz+lim; s++){
//											 if(s<0)break;
//											 if(s>=size)break;
//											 String l = tokens[s];
//											 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
//												 after=after+" Б["+l+"]"; 
//												 //break;	
//											 }
//											 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
//												 after=after+" "+l;	
//												
//											 }
//										}
//										
//										 String wordkey="";
//											//try{wordkey=tokens[pod].toLowerCase().trim()+" "+tokens[skaz].toLowerCase().trim();}catch(Exception e){}
//											//maincore.add(" *"+pod+"/"+skaz+before+"("+wordkey+")"+after+"=["+entry.getValue()+"]");
//											
//										  //String spod=tokens[pod].toLowerCase().trim();
//										  //String sskaz=tokens[skaz].toLowerCase().trim();
//										  maincore.add("++ pod="+pod+"/skaz="+skaz+" текст="+before+" ("+spod+")"+middle+"("+sskaz+") "+after+"=["+entry.getValue()+"]");		
//										   
//										   itembefore.put("before", before);
//										   itemmiddle.put("middle", middle);
//										   itemafter.put("after", after);	
//							}
//							
//							//7 6
//							  if(pod>skaz){
//								
//								for(int s=skaz-1;s>skaz-lim;s--){
//									 if(s<0)break;
//									 if(s>=size)break;
//									 String l = tokens[s];
//									 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
//										 after=after+" Б["+l+"]";  
//										 //break;	
//									 }
//									 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
//										 before=before+" "+l;	
//										
//									 }
//								}
//								//8
//								
//								//посередине, между подлижащим и сказуемым нет данных
//								for(int s=skaz+1;s<pod; s++){
//									 if(s<0)break;
//									 if(s>=size)break;
//									 String l = tokens[s];
//									 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[s])){
//										 middle=middle+" Б["+l+"]"; 
//										 //break;	
//									 }
//									 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[s])){
//										 middle=middle+" "+l;	
//										
//									 }
//								}
//								
//								for(int p=pod+1;p<pod+lim;p++){
//								 if(p<0)break;
//								 if(p>=size)break;
//								 String l = tokens[p];
//								 if(isPresent("((С (жр|мр|ср))|(МС [0-9]л))",morfoarrayL3[p])){
//									 before=before+" "+l;	
//									 //break;	
//								 }
//								 
//								 else if(!isPresent("(СОЮЗ|ПРЕДЛ)",morfoarrayL3[p])){
//									 after=after+" "+l;	
//									
//								 }
//								}
//								
//								 String wordkey="";
//									//try{wordkey=tokens[pod].toLowerCase().trim()+" "+tokens[skaz].toLowerCase().trim();}catch(Exception e){}
//									//maincore.add(" *"+pod+"/"+skaz+before+"("+wordkey+")"+after+"=["+entry.getValue()+"]");
//									
//								  //String spod=tokens[pod].toLowerCase().trim();
//								  //String sskaz=tokens[skaz].toLowerCase().trim();
//								  maincore.add("++ pod="+pod+"/skaz="+skaz+" текст="+before+" ("+sskaz+")"+middle+"("+spod+") "+after+"=["+entry.getValue()+"]");
//							}
//							 
//							  	
//							  inobj.put("before", itembefore);
//							  inobj.put("middle", itemmiddle);
//							  inobj.put("after", itemafter);
//							  inobj.put("itempod", itempod);
//							  inobj.put("itemscaz", itemscaz);
//							  maincorearraylemma.add(inobj);
//							  
//						}catch(Exception e){e.printStackTrace();} 
//						 
//						 
//						 j++;
//							if(j>2)break;
//				     }
						
					 srtjson.put("core", ""+maincorearray);
					 //srtjson.put("corelemma", ""+maincorearraylemma); 					 
//srtjson.put("core", ""+maincore); 
listjson.add(srtjson);
				  
				  }catch(Exception e){
					  
					  e.printStackTrace();
					}
//сильнее <wordform>сильный|Y П сравн,од,но,</wordform>
				
//				if(arryalog){
//					String output = "";
//					 
//					
//					
//					 int i=1;
//					 
//					 for(String str: maincore){
//					        output=output+" "+i+str; i++;}
//					 results.append("main:"+output+" ");  
//					 
//					 output = "";i=1;
//					 for(String str: cores){
//					        output=output+" "+i+str; i++;}
//					 results.append("cores:"+output+" ");  
//					    
//					    
//					 output = "";i=1;
//					 for(String str: nwordarrayL2){
//					        output=output+" "+i+str; i++;}
//					 
//					results.append("nword:"+output+" ");
//					 
//					 output = "";i=1;
//					 for(String str: morfoarrayL3){
//					        output=output+" "+i+str; i++;}
//					results.append("morfo:"+output+" ");
//					
//					 output = "";i=1;
//					 for(String str: emotionarrayL4){
//					        output=output+" "+i+str; i++;}
//					 results.append("motion:"+output+" ");
//					
//					 
//					 output = "";i=1;
//					 for(String str: objectarrayL6){
//					
//					        output=output+" "+i+"["+str+"|"+idcategorymap.get(str)+"]"; i++;}
//					results.append("objects:"+output+" ");
//					
//					 output = "";i=1;
//					 for(String str: iilarrayL1){
//					        output=output+" "+i+"["+str+"]"; i++;}
//					 results.append("qdata:"+output+" ");
//					
//					 output = "";i=1;
//					 for(String str: familiiarrayL7){
//					        output=output+" "+i+"["+str+"]"; i++;}
//					 results.append("familii:"+output+" ");
//					 
//					 output = "";i=1;
//					 for(String str: politeharrayL8){
//					        output=output+" "+i+"["+str+"]"; i++;}
//					 results.append("politeh:"+output+" ");
//					 
//					 
//					 
//					 
//					 output = "";i=1;
//					 for (Map.Entry<String, Integer> entry : categoryMap.entrySet())
//					 {    
//						 
//						 
//						 
//						 if(allcategoryMap.containsKey(entry.getKey()))
//						 {int r=allcategoryMap.get(entry.getKey()); allcategoryMap.put(entry.getKey(), r+entry.getValue());}
//						 else{allcategoryMap.put(entry.getKey(), 1);}
//					
//						 output=output+" "+i+"["+entry.getKey() + "/" + entry.getValue()+"]"; i++; 
//					 }	 results.append("category:"+output+" ");
//					
//					 
//				}
				
				
				 for (Map.Entry<String, Integer> entry : categoryMap.entrySet())
				 {   if(allcategoryMap.containsKey(entry.getKey()))
					 {int r=allcategoryMap.get(entry.getKey()); allcategoryMap.put(entry.getKey(), r+entry.getValue());}
					 else{allcategoryMap.put(entry.getKey(), 1);}
				 }
				
				 
			 }catch(Exception e){	e.printStackTrace();}
		   //предложение
			 }
				
				
		//текст	
		}
		
		
        try{
        	if(ineededu){
			saveVerbTriplet(wordPhraseCount);
			saveVerbTripletPredlog(wordPhraseCountPredlod);
			saveHoroshoPloho(wordHoroshoCount, wordPlohoCount);
        	}
			
		}catch(Exception e){e.printStackTrace();
		}
		
		 String output = ""; int i=1;
		 for (Map.Entry<String, Integer> entry : allcategoryMap.entrySet())
		 {   
			 output=output+" "+i+"["+entry.getKey() + "/" + entry.getValue()+"]"; i++; 
		 }
		 
		 objson.put("allcategory", output);
		 
		 //results.append("allcategory:"+output+" ");
		 
		  output = "";  i=1;
		 for (Map.Entry<String, Integer> entry : allemmotionMap.entrySet())
		 {   
			 output=output+" ["+entry.getKey() + "/" + entry.getValue()+"]"; i++; 
			 
			 //objson.put(entry.getKey(), entry.getValue());
			 //objson.put(entry.getKey(), entry.getValue());
			 
		 }	
		 for (Map.Entry<String, Integer> entry : coreemmotionMap.entrySet())
		 {   
			 //output=output+" ["+entry.getKey() + "/" + entry.getValue()+"]"; i++; 
			 
			 objson.put(entry.getKey(), entry.getValue());
			 objson.put(entry.getKey(), entry.getValue());
			 
		 }
		 
		
			objson.put("all", listjson);
		//if(arryalog) 
	    //    objson.put("all", results.toString());
	
		
		

		FindObjects result= new FindObjects();
		
//		result.setFindObject(msg);
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("");
		result.setResponse("true");
		//if(alllog)
		//result.setText(objson.toString());
       // if(arryalog) 
		//result.setText(results.toString());
		if(getrezult)
			result.setText(objson.toString());
		else
		result.setText("");
        
	
	return result;
	//return objson;
	}
	else{
		FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid(id);
		result.setMsg("empty text");
		result.setResponse("false");
		result.setText("");
		return null;
	}
	}catch(Exception e){e.printStackTrace(); 
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid("");
	result.setMsg("");
	result.setResponse("false");
	result.setText("empty request Body");
	return null;}  
	
	
	
}
//=======================Tonality========================================================================= 





@RequestMapping(value = "/v1/nlp/object/discover/liniar", method = RequestMethod.POST)
public @ResponseBody FindObjects getModData(@RequestBody SpamText text) { 	
boolean getrezult=false;
JSONObject objson = new JSONObject();
JSONArray listjson = new JSONArray();

  if (text == null) {
	  FindObjects result= new FindObjects();
		result.setFindObject(null);
		result.setGuid("");
		result.setMsg("");
		result.setResponse("false");
		result.setText("empty request Body");
		return null;
    }
  
try{
String id = text.getGuid();
if(id.equals("33"))getrezult=true;

String utext = text.getUtftext();
if(id!=null&&utext!=null){
	
	
	Map<String, Integer>  wordPhraseCount=new HashMap<String, Integer>();
	Map<String, Integer>  wordPhraseCountPredlod=new HashMap<String, Integer>();
	Map<String, Integer>  wordHoroshoCount=new HashMap<String, Integer>();
	Map<String, Integer>  wordPlohoCount=new HashMap<String, Integer>();
	
	
	Map<Integer,String> strings=analysis.getSentecesFromString(utext);

	if(strings!=null){
		 
		//1 разобрать каждую на массив
		//2 поискать сущ
		//3 поискать объекты
		//4 поискать фразио
		int stnum=0;
		 for (Map.Entry<Integer, String> estring : strings.entrySet())
		 {try{
			 stnum++;
			 JSONObject srtjson = new JSONObject();
			 JSONArray wordlist = new JSONArray();
//номер строки в документе				
			 
			 String singltext=estring.getValue();
			 
			 
//начальный текст				
			
			 
			// Map<Integer, String[]> datain = processor.getTextRezult(singltext, getrezult);
			 ProcessorResult datain = processor.getTextRezult(singltext, ineededu, getrezult);
			 srtjson =   processor.createRestRezult(datain, ineededu);
			 srtjson.put("origtext", singltext);
			 srtjson.put("number", ""+stnum);
			 if(hasVoprosSymbol(singltext)){srtjson.put("isvopros", "true");}else srtjson.put("isvopros", "false");
			 JSONObject resmalt=new JSONObject();
			 srtjson.put("predicate", "false");
				if(getrezult){
				try{	
					resmalt=maltrest.getCollPOSdata(singltext);
				    srtjson.put("predicate", resmalt);
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			 listjson.add(srtjson);
			 
			 
		     }catch(Exception e){e.printStackTrace();}
		 }}
	
	objson.put("all", listjson);
	
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid(id);
	result.setMsg("");
	result.setResponse("true");
	if(getrezult)
		result.setText(objson.toString());
	else
	result.setText("");
	
	return result;
//return objson;
}
else{
	FindObjects result= new FindObjects();
	result.setFindObject(null);
	result.setGuid(id);
	result.setMsg("empty text");
	result.setResponse("false");
	result.setText("");
	return null;
}
}catch(Exception e){e.printStackTrace(); 
FindObjects result= new FindObjects();
result.setFindObject(null);
result.setGuid("");
result.setMsg("");
result.setResponse("false");
result.setText("empty request Body");
return null;}  



}








void saveVerbTriplet(Map<String, Integer> wordCount){
	
	
	try {	
		
		  
		    Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCountTriplet();

		    	  
		    	  for (Map.Entry<String, Integer> entry : wordCount.entrySet())
		    	  {
			    	   try{
				    		  String tokens = entry.getKey().trim();
				    		  Integer indx = entry.getValue();
				    		  if(probabilitymain.containsKey(tokens)){
				    			  int cont=probabilitymain.get(tokens);
				    			  //probabilitymain.put(tokens, cont+indx); 
				    			  probabilitymain.put(tokens, cont+1); 
				    		  }
				    		  else
				    			  probabilitymain.put(tokens, 1); 
			    		  
			    	      } catch (Exception ex){}
		  	       }
		    	  

			
			
		
	} catch (Exception ex){ex.printStackTrace();}
}
void saveVerbTripletPredlog(Map<String, Integer> wordCount){
	
	
	try {	
		
		  
		    Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCountTripletPredlog();

		    	  
		    	  for (Map.Entry<String, Integer> entry : wordCount.entrySet())
		    	  {
			    	   try{
				    		  String tokens = entry.getKey().trim();
				    		  Integer indx = entry.getValue();
				    		  if(probabilitymain.containsKey(tokens)){
				    			  int cont=probabilitymain.get(tokens);
				    			  //probabilitymain.put(tokens, cont+indx); 
				    			  probabilitymain.put(tokens, cont+1); 
				    		  }
				    		  else
				    			  probabilitymain.put(tokens, 1); 
			    		  
			    	      } catch (Exception ex){}
		  	       }
		    	  

			
			
		
	} catch (Exception ex){ex.printStackTrace();}
}

void saveHoroshoPloho(Map<String, Integer> wordHoroshoCount, Map<String, Integer> wordPlohoCount){
	
	
	try {	
		   
				  Map<String, Integer> probabilitymain=fastMemoryModel.getObjectHorosho();
		    	  for (Map.Entry<String, Integer> entry : wordHoroshoCount.entrySet())
		    	  {
			    	   try{
				    		  String tokens = entry.getKey().trim();
				    		  Integer indx = entry.getValue();
				    		  if(probabilitymain.containsKey(tokens)){
				    			  int cont=probabilitymain.get(tokens);
				    			  probabilitymain.put(tokens, cont+indx); 
				    			  //probabilitymain.put(tokens, cont+1); 
				    		  }
				    		  else
				    			  probabilitymain.put(tokens, 1); 
			    		  
			    	      } catch (Exception ex){}
		  	       }
		    	  

                  
		    	  Map<String, Integer> probabilitymainP=fastMemoryModel.getObjectPloho();
		            for (Map.Entry<String, Integer> entry : wordPlohoCount.entrySet())
			    	  {
				    	   try{
					    		  String tokens = entry.getKey().trim();
					    		  Integer indx = entry.getValue();
					    		  if(probabilitymainP.containsKey(tokens)){
					    			  int cont=probabilitymainP.get(tokens);
					    			  probabilitymainP.put(tokens, cont+indx); 
					    			  //probabilitymain.put(tokens, cont+1); 
					    		  }
					    		  else
					    			  probabilitymainP.put(tokens, 1); 
				    		  
				    	      } catch (Exception ex){}
			  	       }
		    	  
			
		
	} catch (Exception ex){ex.printStackTrace();}
}

public Map<String, List> searchMainObjectArray(List<String> qindexList, Map<String, List> iilobjectsList,
		Map<String, List> iilinstanceList, Map<String, List> iilsubclassofList, Map<String, List> iilproperties, Map<String, List> iilmaincategory, Map<String, List> iiltext, Map<String, List> iilhaspart) {
	
	


	Map<String, List> returndata=new HashMap<String, List>();
	 
	 
	Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
	 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
	 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
	   String itypeid ="";
	   String icategoryid ="";
	   String savetypeid ="none";
	   String savecategoryid ="";
	   String itogo = "";
	   String[] res=new String[2];
	   res[0]="";
	   res[1]="";
	   
	boolean sgeo=false;
	boolean shuman=false;
	boolean sorg=false;
	if(sgeo==false&&shuman==false&&sorg==false){
//----------------------------Q---------------------------- 
//https://dumps.wikimedia.org/wikidatawiki/entities/	
	boolean canexit=false;
	//список Q полученных по слову 
	if(qindexList!=null)
	 for(String qindex:qindexList){
		 if(canexit)break;
		 List<String> resultThree=new ArrayList<String> ();
		
		 
				 if(qindex!=null){ 
				 Map<String, String> viewed=new HashMap<String, String> ();
				 
				 //StringBuffer sb=new StringBuffer();
				 
				 boolean doit=true;
				  int count=0; 
				   do {
					   if(canexit)break;
			       		count++;
			            if(count > 100) break;    
			            String preview=qindex;
			            //думали об этом Q уже?
			            if(!viewed.containsKey(qindex)){
			            
			            	
			            	
			            List<String> vi=new ArrayList<String> ();
			            vi.add(qindex);
			
			            		int icount=0; 
					            do {  
					            	 if(canexit)break;
					            	 
			//System.out.println("--------vi.get-----="+vi.toString());	 
			//System.out.println(icount+"--------vi.get-----="+vi.get(0));
					            String qdata=vi.get(0);
					            icount++;
					            resultThree.add(qdata);
					            //String words=getTextofQ(first, iiltext);
					            //String wordsprop=getPropertyOfType(qdata, iilproperties);
			//		            List<String> otherPropList= iilproperties.get(qdata);
			//		            if(otherPropList!=null)
			//		            for(String c:otherPropList){
			//		    			
			//		    		}
			            
					            
					            
					            if(icount > 1000) break;  
					            if(!viewed.containsKey(qdata)){
					            	List<String> listofinstance =null;
					            	List<String> listsubclassof =null;
					            	List<String> listotherprops =null;
					            	List<String> listhaspart =null;
					            	String label="";
					            	savetypeid =qdata;
				            		
					            	listofinstance = iilinstanceList.get(qdata);
				            		listsubclassof = iilsubclassofList.get(qdata);
				            		listotherprops = iilproperties.get(qdata);
				            		listhaspart =iilhaspart.get(qdata);
			//System.out.println(icount+"--------listofinstance-----="+listofinstance);
			//System.out.println(icount+"--------listsubclassof-----="+listsubclassof);
				            		
				            		
			            		
//String values=getCategoryText(qdata,    iilmaincategory, iiltext);
//sb.append(" "+qdata+"category["+values+"]");
			            		
				            		
					            	//if(iorclass==1){
					            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
					            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
					            		//}
				            		
				            		
					            	if(listhaspart!=null){
					            		
					            		  for(String ins:listhaspart){
					            			  vi.add(ins);
					            			  
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
					            			  
					            		  }
					            		}
				            		
				            	if(listotherprops!=null){
				            		
				            		  for(String ins:listotherprops){
				            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
					            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
					            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
				            			  
				            		  }
				            		}
					            if(listofinstance!=null){
						            for(String ins:listofinstance){
						            	//System.out.println("--------ofinstance-----="+ins);
						            	  vi.add(ins);
						            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
						            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
					            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
					            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
						            }
					            }
					            if(listsubclassof!=null){
						            for(String ins:listsubclassof){
						            	//System.out.println("--------subclassof-----="+ins);
						            	  vi.add(ins);
						            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
						            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
					            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
					            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
						            }
					            }
					            
				            		
//					            	if(listhaspart!=null){
//					            		
//					            		  for(String ins:listhaspart){
//					            			  vi.add(ins);
//					            			  
//					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//					            			  
//					            		  }
//					            		}
//				            		
//				            	if(listotherprops!=null){
//				            		
//				            		  for(String ins:listotherprops){
//				            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//					            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//					            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//				            			  
//				            		  }
//				            		}
//					            if(listofinstance!=null){
//						            for(String ins:listofinstance){
//						            	//System.out.println("--------ofinstance-----="+ins);
//						            	  vi.add(ins);
//						            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
//						            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//					            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//					            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//						            }
//					            }
//					            if(listsubclassof!=null){
//						            for(String ins:listsubclassof){
//						            	//System.out.println("--------subclassof-----="+ins);
//						            	  vi.add(ins);
//						            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
//						            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//					            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//					            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//						            }
//					            }
					             //String valuesCategory=getCategoryText(qdata,    iilmaincategory, iiltext);
					             //returndata.put(qdata, ""+icount+"#"+valuesCategory);
					             vi.remove(qdata);
					            }
					            
					            
					            } while (vi.size()>0);
					            
			            
			
			            if(preview==qindex)doit=false;
			            
			            if(qindex==null)doit=false;
			            //returndata.put(qindex, ""+icount);
			            //String valuesCategory=getCategoryText(qindex,    iilmaincategory, iiltext);
			            //returndata.put(qindex, ""+icount+"u"+valuesCategory);
			            viewed.put(qindex, qindex);
			            }
			            
			        } while (doit);
				   
				   //itogo = sb.toString();
				 }
				 returndata.put(qindex, resultThree);
	 }
	 }
	
	 List<String> resultsavetypeid=new ArrayList<String> ();
	 resultsavetypeid.add(savetypeid);
	 returndata.put("1", resultsavetypeid);
	//returndata.put("2", itogo);
	   //res[0]=savetypeid;
	   //res[1]=itogo;
	return returndata;
}

public int[] getNegPos(String word){
	 int[] synindex = new int[2];
		synindex[0]=0;
		synindex[1]=0;
		if(word!=null){
			if(word.contains("#")){
				   String[] myData = word.split("#");
				   int n=0;
				   int p=0;
				   
				   for(String s:myData){
					   if(s.contains("|")){
						   String[] negpos = s.split("\\|");
						   
						   String neg =negpos[0].trim();
						   String pos =negpos[1].trim();
						   
						   if(neg.equals("true")){
							 n=n+1;  
						   }
						   if(pos.equals("true")){
							 p=p+1; 					   
						   }
					   }
					}
				    synindex[0]=n;
					synindex[1]=p;
			}	
		}
	return synindex;
}

public String[] getMorfema(String nword, Map<String, String> morfema, Map<String, String> iwordPositive, Map<String, String> iwordNegative){
	    String[] synindex = new String[3];
		synindex[0]=null;
		synindex[1]=null;
		synindex[2]=null;
	try{
	   String m= morfema.get(nword); 
	   StringBuffer array = new StringBuffer();
	   if(m!=null){
	   if(m.contains("|")){
		   String[] mdata = m.split("\\|");   
		    int sizeb = mdata.length;
			for(int i=0;i<sizeb;i++){
				String single=mdata[i];
				try{ 
					     String neg=iwordNegative.get(single);
					     if(neg!=null){
					    	 array.append(nword+"["+single+"]").append("-NEG ");
					    	 synindex[0]=single;
					     }
				}catch(Exception e){e.printStackTrace();}  
				
				try{ 
				     String pos=iwordPositive.get(single);
				     if(pos!=null){
				    	 array.append(nword+"["+single+"]").append("-POS ");
				    	 //wordjson.put("pos", true);
				    	 synindex[1]=single;
				     }
				     }catch(Exception e){e.printStackTrace();}
				     
			
			}
			//wordjson.put("emo", array.toString());
			synindex[2]= array.toString();
	   }else{
		   
		  try{ 
				     String neg=iwordNegative.get(m);
				     if(neg!=null){
				    	 //resultarray.append(lowerStr+"["+m+"]").append("-NEG ");
				    	 //wordjson.put("emoneg", m);
				    	 //wordjson.put("neg", true);
				    	 synindex[0]=m;
				     }
			}catch(Exception e){e.printStackTrace();}  
		  
		  try{ 
			     String pos=iwordPositive.get(m);
			     if(pos!=null){
			    	 //resultarray.append(lowerStr+"["+m+"]").append("-POS ");
			    	 //wordjson.put("emopos", m);
			    	 //wordjson.put("pos", true);
			    	 synindex[1]=m;
			     }
			     }catch(Exception e){e.printStackTrace();}
			     
			
	   }
	}
	   
}catch(Exception e){e.printStackTrace();} 
	
	
	return synindex;}





//public void searchMainObject(String nword, Map<String, List> iiltextList,  Map<String, List> iilinstanceList,  Map<String, List> iilsubclassofList, Map<String, List> iilproperties){
//
//
//	 
//	 
//	Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
//	 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
//	 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
//	   String itypeid ="";
//	   String icategoryid ="";
//	   String savetypeid ="";
//	   String savecategoryid ="";
//	   
//	boolean sgeo=false;
//	boolean shuman=false;
//	boolean sorg=false;
//	if(sgeo==false&&shuman==false&&sorg==false){
////----------------------------Q---------------------------- 
//	List<String> qindexList = iiltextList.get(nword);
//	boolean canexit=false;
//	
//	 if(qindexList!=null)
//	 for(String qindex:qindexList){
//		 if(canexit)break;
//	 if(qindex!=null){ 
//	 Map<String, String> viewed=new HashMap<String, String> ();
//	 
//	 StringBuffer sb=new StringBuffer();
//	 
//	 boolean doit=true;
//	  int count=0; 
//	   do {
//		   if(canexit)break;
//       		count++;
//            if(count > 100) break;    
//            String preview=qindex;
//            if(!viewed.containsKey(qindex)){
//            
//            	
//            	
//            List<String> vi=new ArrayList<String> ();
//            vi.add(qindex);
//
//            		int icount=0; 
//		            do {  
//		            	 if(canexit)break;
//		            //System.out.println("--------search-----="+vi.get(0));
//		            String qdata=vi.get(0);
//		            icount++;
//		            //String words=getTextofQ(first, iiltext);
//		           // String wordsprop=getPropertyOfType(qdata, iiltype);
//
//            
//		            
//		            
//		            if(icount > 1000) break;  
//		            if(!viewed.containsKey(qdata)){
//		            	List<String> listofinstance =null;
//		            	List<String> listsubclassof =null;
//		            	List<String> listotherprops =null;
//		            	String label="";
//		            	
//	            		listofinstance =iilinstanceList.get(qdata);
//	            		listsubclassof =iilsubclassofList.get(qdata);
//	            		listotherprops =iilproperties.get(qdata);
//	            		
//	            		
//		            	//if(iorclass==1){
//		            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
//		            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
//		            		//}
//	            			
//	            	if(listotherprops!=null){
//	            		
//	            		  for(String ins:listotherprops){
//	            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//		            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//		            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//	            			  
//	            		  }
//	            		}
//		            if(listofinstance!=null){
//			            for(String ins:listofinstance){
//			            	//System.out.println("--------ofinstance-----="+ins);
//			            	  vi.add(ins);
//			            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
//			            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//		            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//		            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//			            }
//		            }
//		            if(listsubclassof!=null){
//			            for(String ins:listsubclassof){
//			            	//System.out.println("--------subclassof-----="+ins);
//			            	  vi.add(ins);
//			            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
//			            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//		            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//		            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//			            }
//		            }
//		             vi.remove(qdata);
//		            }
//		            
//		            
//		            } while (vi.size()>0);
//		            
//            
//
//            if(preview==qindex)doit=false;
//            
//            if(qindex==null)doit=false;
//            
//            viewed.put(qindex, qindex);
//            }
//            
//        } while (doit);
//	   
//	   //itogo=sb.toString();
//	 }}
//	 }
//	
//}


boolean isPresent(String serchword, String text){
	if(text!=null){
	 String regex = "\\b"+serchword+"\\b";
       Pattern pattern = Pattern.compile(regex);
       Matcher matcher = pattern.matcher(text);
   
       while(matcher.find() == true){
       	
       	return true;
       }
		return false;} else return false;
}


private Map<String, Integer> sortByValue(Map<String, Integer> unsortMap) {

    // 1. Convert Map to List of Map
    List<Map.Entry<String, Integer>> list =
            new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

    // 2. Sort list with Collections.sort(), provide a custom Comparator
    //    Try switch the o1 o2 position for a different order
    Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
        public int compare(Map.Entry<String, Integer> o1,
                           Map.Entry<String, Integer> o2) {
            return (o2.getValue()).compareTo(o1.getValue());
        }
    });

    // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
    Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
    for (Map.Entry<String, Integer> entry : list) {
        sortedMap.put(entry.getKey(), entry.getValue());
    }




    return sortedMap;
}

private Map<Integer[], Integer> sortByValueI(Map<Integer[], Integer> unsortMap) {

    // 1. Convert Map to List of Map
    List<Map.Entry<Integer[], Integer>> list =
            new LinkedList<Map.Entry<Integer[], Integer>>(unsortMap.entrySet());

    // 2. Sort list with Collections.sort(), provide a custom Comparator
    //    Try switch the o1 o2 position for a different order
    Collections.sort(list, new Comparator<Map.Entry<Integer[], Integer>>() {
        public int compare(Map.Entry<Integer[], Integer> o1,
                           Map.Entry<Integer[], Integer> o2) {
            return (o2.getValue()).compareTo(o1.getValue());
        }
    });

    // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
    Map<Integer[], Integer> sortedMap = new LinkedHashMap<Integer[], Integer>();
    for (Map.Entry<Integer[], Integer> entry : list) {
        sortedMap.put(entry.getKey(), entry.getValue());
    }




    return sortedMap;
}

private Map<String, Integer> sortByValueSI(Map<String, Integer> unsortMap) {

    // 1. Convert Map to List of Map
    List<Map.Entry<String, Integer>> list =
            new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

    // 2. Sort list with Collections.sort(), provide a custom Comparator
    //    Try switch the o1 o2 position for a different order
    Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
        public int compare(Map.Entry<String, Integer> o1,
                           Map.Entry<String, Integer> o2) {
            return (o2.getValue()).compareTo(o1.getValue());
        }
    });

    // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
    Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
    for (Map.Entry<String, Integer> entry : list) {
        sortedMap.put(entry.getKey(), entry.getValue());
    }




    return sortedMap;
}

public List<IndexWrapper> findIndexesForKeyword(String searchString, String keyword) {
    String regex = "\\b"+keyword+"\\b";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(searchString);

    List<IndexWrapper> wrappers = new ArrayList<IndexWrapper>();
    int start=0;
    int end = 0;
    boolean inotfind=true;
    while(matcher.find() == true){
         end = matcher.end();
         start = matcher.start();
        IndexWrapper wrapper = new IndexWrapper(start, end);
        wrappers.add(wrapper);
        inotfind=false;
    }
    if(inotfind){
    	IndexWrapper wrapper = new IndexWrapper(start, end);
        wrappers.add(wrapper);}
    return wrappers;
}
*/
}
