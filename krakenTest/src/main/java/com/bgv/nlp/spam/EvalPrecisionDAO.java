package com.bgv.nlp.spam;

import opennlp.maxent.GISModel;

public interface EvalPrecisionDAO {

	double evaluateModel(String evalfilename, String modelFileName);

}
