package com.bgv.nlp.spam;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.entity.Sheduler;
import com.bgv.service.IShedulerService;
import com.bgv.springmvc.domain.Config;
import com.google.common.base.Splitter;

import opennlp.maxent.GISModel;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.tools.tokenize.Tokenizer;


@Service
public class EvalPrecisionImpl implements EvalPrecisionDAO{

	@Override
	public double evaluateModel(String evalfilename, String modelFileName) {
		double calc=0;
		Map<String, String> getList;
		try {
			getList = getEvalData(evalfilename);
			calc=evalText(modelFileName, getList);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return calc;
	}
	
	
	public  double evalText(String modelFileName, Map<String, String> hashmap) throws IOException {
		GISModel	currentModel = (GISModel)new SuffixSensitiveGISModelReader(
	            new File(modelFileName)).getModel();
		  int i=0;  
		  int t=0;
		  int f=0;
		  LocalTokens token = new LocalTokens();
		
		  for (Map.Entry entry : hashmap.entrySet()) {
		    
		  	  double[] out = currentModel.eval(token.buildContext((String) entry.getKey()));
			  
		  	  String bestOutcomeb = currentModel.getBestOutcome(out);
			  //int numOutcomes = currentModel.getNumOutcomes();
			  String allnumOutcomes = currentModel.getAllOutcomes(out);
			  //Object[] data = currentModel.getDataStructures();
				// System.out.println("Text="+(String) entry.getKey());
		   		//  System.out.println("EVAL="+bestOutcomeb+" ALLnumOutcomes="+allnumOutcomes);
		   		//  System.out.println("\n");
			  if(bestOutcomeb.equals("SPAM")){
	    		  t++;
	    	  }
	    	  else{
	    		f++;  
	    	
	    	  }
		    	i++;
			}

		  
		     System.out.println(""+i+ " ="+t+" ="+f+"");
			 float total = (float)((t*100)/i);
			 System.out.println("="+total+"%");
			 float totalf = (float)((f*100)/i);
			 System.out.println("="+totalf+"%");
			 
			 return Double.parseDouble(new Float(total).toString());
			 //return total;
	}


	private Map<String, String> getEvalData(String fileName) throws ParserConfigurationException, SAXException, IOException {
		 			LocalTokens token = new LocalTokens();
					List<Sentence> eduList = new ArrayList<>();
	 			    Map<String, String> hashmap = new HashMap<String, String>();
			        DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance();
				    DocumentBuilder builder = factory.newDocumentBuilder();
				    Document document = builder.parse (new File(fileName));
				  
				    Config conf=new Config(); int limmax=conf.getLimitEval();
				  
				    NodeList nodeList = document.getDocumentElement().getChildNodes();
	int limit=0;
				    for (int k = 0; k < nodeList.getLength(); k++) {

				      
				      Node node = nodeList.item(k);
				      if (node instanceof Element) {
	if(limit>limmax)break;
	limit++;
				    	try{Sentence emp = new Sentence();
				        emp.id = node.getAttributes().getNamedItem("id").getNodeValue();

				        NodeList childNodes = node.getChildNodes();
				        for (int j = 0; j < childNodes.getLength(); j++) {
				          Node cNode = childNodes.item(j);

				      
				          if (cNode instanceof Element) {
				            String content = cNode.getLastChild().getNodeValue();
				            switch (cNode.getNodeName()) {
				              case "speech":
				            	  
				            	content=token.normalizeText(content);  
				            	//System.out.println(content+"\n");
				                emp.speech = content;
				                break;
				              case "evaluation":
				                emp.evaluation = content;
				                break;
				              case "url":
				                emp.url = content;
				                break;
				            }
				          }
				        
				        }
				       if(emp.getEvaluation().equals("SPAM")){ 
				    	 
				    	   hashmap.put(emp.speech, "SPAM");
				       }
				       if(emp.getEvaluation().equals("HAM")){ 
				    	  
				    	   hashmap.put(emp.speech, "HAM");
				       }
				      }catch(Exception e){e.printStackTrace();}
				      }

				    }
				    
				  
		  
		return hashmap;
	}
	
	


}
