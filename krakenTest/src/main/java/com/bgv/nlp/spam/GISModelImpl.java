package com.bgv.nlp.spam;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.entity.Sheduler;
import com.bgv.service.IShedulerService;
import com.bgv.springmvc.domain.Config;

import opennlp.maxent.GISModel;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;


@Service
public class GISModelImpl implements GISModelDAO{
	 @Autowired
		private IShedulerService shedulerService;
	
	static GISModel	currentModel;
	
	@Override
	public GISModel getGISModel() {
		
		return currentModel;
	}
	
	@Override
	public void setCurrentModel(GISModel currentModel) {
		GISModelImpl.currentModel = currentModel;
	}


	@PostConstruct
	public void initialize() throws IOException{
		 
		List<Sheduler> s = shedulerService.getAllSheduler();
		Sheduler toupdate = null;
		 String modelname=null;
		for(Sheduler l:s){
		int	spammodelmax=0;
		String name = l.getSname();	
		
		if(name.equals("spammodel")){
			
				spammodelmax=l.getLastpid();
				toupdate=l;
				modelname=l.getModelname();
				
			}
		}
		 Config cf=new Config();
		 String rooot=cf.getRoot();
		 
		 if(modelname==null){
		  
			 modelname=cf.getModel();
		 
		 }
		 String modelFileName=rooot+modelname;
		 currentModel = (GISModel)new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel();
		
	}
}
