package com.bgv.nlp.spam;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.bgv.springmvc.domain.Config;

import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;

@Service
public class TokenizerImpl implements TokenizerDAO{
	
	static Tokenizer tokenizer;
	
	@PostConstruct
	public void initialize() throws InvalidFormatException, IOException {
		 Config cf=new Config();
		 String rooot=cf.getRoot();
		  
		    InputStream is = new FileInputStream(rooot+"en-token.bin");
			ArrayList nameslist = new ArrayList();
			
			TokenizerModel tmodel = new TokenizerModel(is);

			tokenizer = new TokenizerME(tmodel);
		
	}

	@Override
	public Tokenizer getTokenizers() {
		
		return tokenizer;
	}

}
