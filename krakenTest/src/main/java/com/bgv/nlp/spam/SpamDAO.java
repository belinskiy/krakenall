package com.bgv.nlp.spam;

import com.bgv.springmvc.domain.SpamTextStatus;

public interface SpamDAO {
    public SpamTextStatus discover(String id, String text); 
}
