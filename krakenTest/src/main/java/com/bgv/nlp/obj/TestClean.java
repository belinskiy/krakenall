package com.bgv.nlp.obj;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.bgv.practice.LangRulesImpl;

public class TestClean {
	 public static void main( String[] args ) throws IOException
	    {
		 
		 
		 loadMedJSON("C:\\Users\\Admin\\Downloads\\out.json", "C:\\Users\\Admin\\Downloads\\outz.json");
		  Runtime.getRuntime().exit(0);
		 System.out.println( isPresent("(С (жр|мр|ср))|(П (жр|мр|ср))","Y П мр,ед,тв,од,но"));
		
		 
		 String[] ind = new String[2];
		 	
		    ind =getData("рука");
		    String word1=ind[0]; 
			String morf1=ind[1]; 
			
			ind =getData("состоит");
			String word2=ind[0]; 
			String morf2=ind[1]; 
			 	
			ind =getData("из");
			String word3=ind[0];  
			String morf3=ind[1]; 
			
			ind =getData("тела");
			String word4=ind[0]; 
			String morf4=ind[1]; 
			//System.out.println("1="+word1+" "+ morf1+" 2="+  word2+" "+  morf2+" 3="+  word3+" "+ morf3+" 4="+  word4+" "+ morf4);	
			/*
		 LangRulesImpl test=new LangRulesImpl();
		 boolean[] re = test.semanticRuleSet(word1, morf1, word2, morf2, word3, morf3, word4, morf4);
		 //System.out.println(re[0]+" "+re[1]+" "+re[2]+" "+re[3]+" ");
		 

		 System.out.println( isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)","который|f МС-П мн,им,од,но"));
		 System.out.println( isPresent("С (жр|мр|ср),(ед|мн),(вн|рд)","который|f МС-П мн,им,од,но"));
		 */
		 LuceneMorphology luceneMorph = new RussianLuceneMorphology();
			 word1="ты";
			
			 morf1 = getnormalFormSyn(word1,luceneMorph);
			LangRulesImpl rl= new LangRulesImpl();
			boolean[] l = rl.predicateRuleSet(word1, morf1, "", "", "", "", "", "");
			
			System.out.println("morf1="+morf1+" l0="+l[0]+" l1="+l[1]);
		 
			Map<Integer,String> stringsMap=rl.getSenteces("Ну почему именно музыканты? Почему Прекрасный ансамбль. Спасибо, конечно, боженьке за бонус в виде съемочной группы НТВошек, но остальные-то почему?");
			for (Map.Entry<Integer,String> entrys : stringsMap.entrySet())
			 {
				 Integer key = entrys.getKey();
				 String value = entrys.getValue();
				 System.out.println(value);
				 System.out.println("l1="+rl.hasVoprosSymbol(value));
				 System.out.println("l2="+rl.voprosIsMissing(value));

			 }
		 
		 /*
		 getSenteces(" Sentence \"four\". Sentence \"five\"! Сколько тебе лет?");
		 
		 CleanTextImpl t=new CleanTextImpl();
		 int[] level=new int[5];
		 level[0]=1;
		 level[1]=1;
		 level[2]=1;
		 level[3]=1;
		 level[4]=1;
		 String text="Во всих ш/колах Масквы блять одни бля.ти?";
		 //text=getText();
		 text="С каких пор? Сколько тебе лет?";
		
		
		 Map<Integer, List<String[]>> map = t.getText(level, text);
		 List<String[]> value = null;
		 for (Map.Entry<Integer, List<String[]>> entrys : map.entrySet())
		 {
		     //System.out.println(entry.getKey() + "/" + entry.getValue());
			 Integer k = entrys.getKey();
			 value = entrys.getValue();
			 System.out.println("k="+k);
			 for(String[] index:value){
				// System.out.println("index[0]="+index[0]);
				// System.out.println("index[1]="+index[1]);
			 }
		     
		 }
		
		*/
		 
	    }
	 
		public static void loadMedJSON(String jsonData, String jsonOUTData)  {}
		
		static String getText(){
			String[] arr = new String[10]; // 10 is the length of array.
			arr[0] = "Кузнецов поговорил с судьей о картошке во время матча Кубка мира Видео: wch2016.com управы района Северное Тушино. В сети появилась запись с микрофона, закрепленного на форме нападающего сборной России Евгения Кузнецова во время матча Кубка мира по хоккею со сборной Северной Америки. Запись размещена на официальном сайте турнира. В одном из эпизодов встречи к Кузнецову подъехал судья и задал вопрос, суть которого из-за невысокого качества записи расслышать не удается. В ответ хоккеист рассказал арбитру, что любит стейки слабой прожарки с картошкой и грибами. Во второй игре группового турнира подопечные Олега Знарка обыграли североамериканцев со счетом 4:3. Кузнецов стал автором третьей шайбы россиян. В первой игре со шведами (1:2) микрофон был закреплен на амуниции капитана сборной России Александра Овечкина.";
			arr[1] = "Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны";
			arr[2] = "Крыша расположенного в «новой» Москве здания спортивно-культурного центра по реабилитации воинов-участников локальных конфликтов будет отремонтирована до конца 2016 г., сообщили в пресс-службе Москомэкспертизы. «Москомэкспертиза сообщает о согласовании проектной документации для объекта капитального строительства: «Капитальный ремонт кровли здания спортивно-культурного центра по реабилитации воинов-участников локальных конфликтов» по адресу: пос. Знамя Октября, д. 31 стр. 3, поселение Рязановское, Новомосковский административный округ Москвы. Расчетная продолжительность ремонта кровли составляет 2,5 месяца. По итогам государственной экспертизы сметная стоимость проекта снижена на 558,6 тыс. руб.», - говорится в сообщении. Как сообщил председатель Москомэкспертизы Валерий Леонов, слова которого приводятся в сообщении, спортивно-культурный центр по реабилитации воинов-участников локальных конфликтов был построен в 2004 г. Он представляет собой четырехэтажное здание с мансардой и подземным этажом. В настоящее время кровля центра смонтирована из сэндвич-панелей. «В ходе работ кровлю отремонтируют, покрытие заменят на металлочерепицу. Вместо деревянных окон в мансарде установят пластиковые. В рамках капитального ремонта будет установлен снегодержатель, устройства для страховочного троса, ограждение кровли, переходных мостиков и кровельных лестниц. На четвертом этаже предусмотрена установка горизонтальной и вертикальной звукоизоляции помещений», - пояснили в пресс-службе. По словам В.Леонова, работы будут осуществляться без применения ударных механизмов, вручную, при помощи средств малой механизации";
			arr[3] = "Российские бомбардировщики уничтожают успехи собственной дипломатии Путин, казалось, был близок к цели: вместо Асада главным вопросом в Сирии стала борьба с международным терроризмом. Хозяева Кремля могли откинуться в кресле и наблюдать, как США терпят фиаско в разделении террористов и повстанцев. Но нет. Когда в прошлую субботу два бомбардировщика американских ВВС вместе с двумя австралийскими самолетами нанесли удар по позициям сирийских правительственных войск вблизи города Дейр-эз-Зор, реакция из Москвы не заставила себя долго ждать. В привычном раздраженно-циничном тоне официальная представительница российского МИДа заявила, что Вашингтон защищает террористов \"Исламского государства\" (организация запрещена в РФ. - Прим. ред.).\"Подобное заявление было не только предельно абсурдно, но и противоречило собственным интересам Москвы\", - пишет Юлиан Ханс на страницах швейцарской газеты Tagesanzeiger. После вступления в силу режима прекращения огня США начали бомбить позиции ИГИЛ на востоке Сирии, где террористическая организация воевала с войсками Асада. Американцы делали как раз то, чего месяцами требовала Москва, - начать борьбу с террористами и \"отстать\" от Асада. Более того, россияне дали свое одобрение на эти полеты: координация - также требование Москвы. Ранее в этом году позиции сил Асада случайно оказались под \"дружественным огнем\" российских бомбардировщиков, говорится в статье. \"Когда же был нанесен удар по гуманитарному конвою ООН, Москвасначала молчала, а затем М";
			arr[4] = "гу мвд рф по москве";
			arr[5] = "Кузнецов поговорил с судьей управы района Северное Тушино. В сети появилась запись с микрофона, закрепленного на форме нападающего сборной России Евгения Кузнецова во время матча Кубка мира по хоккею со сборной Северной Америки. Запись размещена на официальном сайте турнира. В одном из эпизодов встречи к Кузнецову подъехал судья и задал вопрос, суть которого из-за невысокого качества записи расслышать не удается. В ответ хоккеист рассказал арбитру, что любит стейки слабой прожарки с картошкой и грибами. Во второй игре группового турнира подопечные Олега Знарка обыграли североамериканцев со счетом 4:3. Кузнецов стал автором третьей шайбы россиян. В первой игре со шведами (1:2) микрофон был закреплен на амуниции капитана сборной России Александра Овечкина.  Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Президент РФ - Путин В.В., Генеральная прокуратура, Уполномоченный при Президенте Астахов П.А., Министерство Энергетики РФ, Следственный комитет РФ: Найдите виновных в смерти моей дочери Захаровой Ариадны Крыша расположенного в «новой» Москве здания спортивно-культурного центра по реабилитации воинов-участников локальных конфликтов будет отремонтирована до конца 2016 г., сообщили в пресс-службе Москомэкспертизы. «Москомэкспертиза сообщает о согласовании проектной документации для объекта капитального строительства: «Капитальный ремонт кровли здания спортивно-культурного центра по реабилитации воинов-участников локальных конфликтов» по адресу: пос. Знамя Октября, д. 31 стр. 3, поселение Рязановское, Новомосковский административный округ Москвы. Расчетная продолжительность ремонта кровли составляет 2,5 месяца. По итогам государственной экспертизы сметная стоимость проекта снижена на 558,6 тыс. руб.», - говорится в сообщении. Как сообщил председатель Москомэкспертизы Валерий Леонов, слова которого приводятся в сообщении, спортивно-культурный центр по реабилитации воинов-участников локальных конфликтов был построен в 2004 г. Он представляет собой четырехэтажное здание с мансардой и подземным этажом. В настоящее время кровля центра смонтирована из сэндвич-панелей. «В ходе работ кровлю отремонтируют, покрытие заменят на металлочерепицу. Вместо деревянных окон в мансарде установят пластиковые. В рамках капитального ремонта будет установлен снегодержатель, устройства для страховочного троса, ограждение кровли, переходных мостиков и кровельных лестниц. На четвертом этаже предусмотрена установка горизонтальной и вертикальной звукоизоляции помещений», - пояснили в пресс-службе. По словам В.Леонова, работы будут осуществляться без применения ударных механизмов, вручную, при помощи средств малой механизации Российские бомбардировщики уничтожают успехи собственной дипломатии Путин, казалось, был близок к цели: вместо Асада главным вопросом в Сирии стала борьба с международным терроризмом. Хозяева Кремля могли откинуться в кресле и наблюдать, как США терпят фиаско в разделении террористов и повстанцев. Но нет. Когда в прошлую субботу два бомбардировщика американских ВВС вместе с двумя австралийскими самолетами нанесли удар по позициям сирийских правительственных войск вблизи города Дейр-эз-Зор, реакция из Москвы не заставила себя долго ждать. В привычном раздраженно-циничном тоне официальная представительница российского МИДа заявила, что Вашингтон защищает террористов \"Исламского государства\" (организация запрещена в РФ. - Прим. ред.).\"Подобное заявление было не только предельно абсурдно, но и противоречило собственным интересам Москвы\", - пишет Юлиан Ханс на страницах швейцарской газеты Tagesanzeiger. После вступления в силу режима прекращения огня США начали бомбить позиции ИГИЛ на востоке Сирии, где террористическая организация воевала с войсками Асада. Американцы делали как раз то, чего месяцами требовала Москва, - начать борьбу с террористами и \"отстать\" от Асада. Более того, россияне дали свое одобрение на эти полеты: координация - также требование Москвы. Ранее в этом году позиции сил Асада случайно оказались под \"дружественным огнем\" российских бомбардировщиков, говорится в статье. \"Когда же был нанесен удар по гуманитарному конвою ООН, Москвасначала молчала, а затем М гу мвд рф по москве Крыша расположенного в «новой» Москве здания спортивно-культурного центра по реабилитации воинов-участников локальных конфликтов будет отремонтирована до конца 2016 г., сообщили в пресс-службе Москомэкспертизы. «Москомэкспертиза сообщает о согласовании проектной документации для объекта капитального строительства: «Капитальный ремонт кровли здания спортивно-культурного центра по реабилитации воинов-участников локальных конфликтов» по адресу: пос. Знамя Октября, д. 31 стр. 3, поселение Рязановское, Новомосковский административный округ Москвы. Расчетная продолжительность ремонта кровли составляет 2,5 месяца. По итогам государственной экспертизы сметная стоимость проекта снижена на 558,6 тыс. руб.», - говорится в сообщении. Как сообщил председатель Москомэкспертизы Валерий Леонов, слова которого приводятся в сообщении, спортивно-культурный центр по реабилитации воинов-участников локальных конфликтов был построен в 2004 г. Он представляет собой четырехэтажное здание с мансардой и подземным этажом. В настоящее время кровля центра смонтирована из сэндвич-панелей. «В ходе работ кровлю отремонтируют, покрытие заменят на металлочерепицу. Вместо деревянных окон в мансарде установят пластиковые. В рамках капитального ремонта будет установлен снегодержатель, устройства для страховочного троса, ограждение кровли, переходных мостиков и кровельных лестниц. На четвертом этаже предусмотрена установка горизонтальной и вертикальной звукоизоляции помещений», - пояснили в пресс-службе. По словам В.Леонова, работы будут осуществляться без применения ударных механизмов, вручную, при помощи средств малой механизации";
			arr[6] = "Американские военные подтверждают использование белого фосфора в операциях против группировки \"Исламское государство\" (ИГ, запрещена в России) в Ираке, но не раскрывают конкретные детали и цели таких операций, сообщает газета Washington Post.";
			arr[7] = "Директор ФБР Джеймс Коми утверждает, что кандидат в президенты США от Демократической партии Хиллари Клинтон в бытность госсекретарем проявляла \"крайнюю небрежность\" в отношении секретной информации, передает телеканал ABC.";
			arr[8] = "Вывод об использовании снарядов с белым фосфором был сделан на основании анализа размещенных на одном из сайтов Пентагона фотографий, на которых видны начиненные этим веществом снаряды М825А1 калибром 155 мм. Авторы статьи отмечают, что один такой снаряд может обеспечить 10-минутную дымовую завесу. \"Силы коалиции используют эти снаряды с осторожностью и всегда в соответствии с Законом о вооруженном конфликте. М825А1 используются в районах, где нет гражданского населения, и никогда против сил врага\", — заявил в ответ на запрос издания представитель сил возглавляемой США коалиции полковник Джозеф Скрокка. По его словам, белый фосфор используется для создания \"завесы и подачи сигналов\". Между тем, как заверил другой представитель коалиции, полковник Джон Дорриан, в случае использования таких снарядов США делают это так, чтобы \"учесть возможные побочные эффекты на гражданских лиц и гражданские структуры\". \"Военные США предпринимают все необходимые меры предосторожности, чтобы минимизировать риск случайных ранений гражданского населения и ущерба для мирных объектов\", — прокомментировал Дорриан. Он пояснил, что заинтересовавшие журналистов фотографии были сделаны во время операции коалиции по поддержке курдских формирований на севере Ирака, но не смог сказать, сбрасывались ли снаряды на отряды ИГ или в районы, где могли находиться мирные жители. Несмотря на заверения минобороны США, правозащитники озабочены сообщениями о применении белого фосфора в ходе готовящейся операции по освобождению Мосула. \"ВС США и Ирака должны воздержаться от использования белого фосфора в городских поселениях, подобных Мосулу, поскольку, каким бы ни было тактическое преимущество, его превзойдут ужасные ожоги, полученные мирными жертвами\", — предупреждает Марк Хизнай из Human Rights Watch. Фосфорные боеприпасы самовоспламеняются при контакте с кислородом. У людей это вещество вызывает глубокие ожоги, на поверхности земли начинаются пожары. Ранее США уже признавали, что применяли боеприпасы с белым фосфором в Ираке в 2004 году, а также в Афганистане, и подвергались за это критике. Представители Пентагона также затруднились ответить на вопрос о том, сколько раз применялось вещество и сбрасывались ли снаряды с ним на позиции террористов.";
			arr[9] = "Владимир Путин похвалил ЦИК за честные выборы. Что с этим не так? https://meduza.io/feature/2016/0";
		
			Random randomGenerator = new Random();
			 int t = randomGenerator.nextInt(9);
			
			return arr[t];
			
		}
		
		public static void getSenteces(String subjectString) {
	        //subjectString = 
	        //"This is a sentence. " +
	        //"So is \"this\"! And is \"this?\" " +
	        //"This is 'stackoverflow.com!' " +
	        //"Hello World";
	        String[] sentences = null;
	        Pattern re = Pattern.compile(
	            "# Match a sentence ending in punctuation or EOS.\n" +
	            "[^.!?\\s]    # First char is non-punct, non-ws\n" +
	            "[^.!?]*      # Greedily consume up to punctuation.\n" +
	            "(?:          # Group for unrolling the loop.\n" +
	            "  [.!?]      # (special) inner punctuation ok if\n" +
	            "  (?!['\"]?\\s|$)  # not followed by ws or EOS.\n" +
	            "  [^.!?]*    # Greedily consume up to punctuation.\n" +
	            ")*           # Zero or more (special normal*)\n" +
	            "[.!?]?       # Optional ending punctuation.\n" +
	            "['\"]?       # Optional closing quote.\n" +
	            "(?=\\s|$)", 
	            Pattern.MULTILINE | Pattern.COMMENTS);
	        Matcher reMatcher = re.matcher(subjectString);
	        while (reMatcher.find()) {
	           // System.out.println(reMatcher.group());
	            
	            if(etoVopros(reMatcher.group()))System.out.println("Это вопрос");
	        } 
	    } 
		
		
		public static boolean etoVopros(String str) {
			boolean t=false;
		    if (str != null && str.length() > 0) {
		      str = str.substring(str.length()-1);
		      //System.out.println("index[1]="+str);
		      if(str.equals("?"))t=true;
		    }
		    return t;
		}
		
		
	static	boolean  isPresent(String serchword, String text){
			
			 String regex = "\\b"+serchword+"\\b";
		        Pattern pattern = Pattern.compile(regex);
		        Matcher matcher = pattern.matcher(text);
		    
		        while(matcher.find() == true){
		        	
		        	return true;
		        }
				return false;
		}
	
	static String[] getData(String text) throws IOException{
		 String[] index = new String[2];
		 String wordform="-1";	
		 LuceneMorphology luceneMorph = new RussianLuceneMorphology();
		 List<String> wordBaseForms =luceneMorph.getMorphInfo(text.toLowerCase());
		 StringBuilder sb = new StringBuilder();
			for (String s : wordBaseForms)
			{
			    sb.append(s);
			    sb.append("#");
			}
			
			String ist = sb.toString();
			if(!ist.equals("")&&ist!=null){
				wordform=ist;
			
				if(wordform.contains("|")){
					String[] morfdata = wordform.split("\\|");
					index[0]=text;
					index[1]=morfdata[1];	
				}
			
   			
   			
			}
			return index;  
	}
	
	
	private static String getnormalFormSyn(String word, LuceneMorphology luceneMorph){
		
		
		String wordform=word;
		String normalform=word;	
		
		try{
		
		
		List<String> wordBaseForms = luceneMorph.getMorphInfo(word);
		StringBuilder sb = new StringBuilder();
		for (String s : wordBaseForms)
		{
		    sb.append(s);
		    sb.append(",");
		}
		
		
		String ist = sb.toString();
		if(!ist.equals("")&&ist!=null){
			wordform=ist;
		}
		}catch(Exception e){}
		
		/*
		
		try{
			
			
			List<String> wordBaseForms = luceneMorph.getNormalForms(word);
			StringBuilder sb = new StringBuilder();
			for (String s : wordBaseForms)
			{
			    sb.append(s);
			    sb.append(" ");
			}
			
			
			String ist = sb.toString();
			if(!ist.equals("")&&ist!=null){
				normalform=ist;
			}
			}catch(Exception e){}
	*/	
	//<wordform>семья|G С жр,ед,им,</wordform> существительным или личным местоимением в именительном падеже (я, ты, он, она, оно, мы, вы, они)
	//<wordform>он|e МС 3л,мр,ед,им,</wordform>
	//МС 1л,ед,им

	return wordform;
}
}
