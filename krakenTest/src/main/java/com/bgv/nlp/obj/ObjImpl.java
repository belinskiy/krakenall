package com.bgv.nlp.obj;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.entity.Probabilitydirty;
import com.bgv.nlp.spam.TokenizerDAO;
import com.bgv.service.IProbabilitydirtyService;
import com.bgv.springmvc.domain.FindObject;
import com.bgv.springmvc.domain.SpamTextStatus;
import com.derby.model.MprobabilitydirtyDao;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.Span;


@Service
public class ObjImpl implements ObjDAO {
	@Autowired
	 private TokenizerDAO token;
	@Autowired
	 private NameFinderModelMEDAO model;
	
	@Autowired
	 private IProbabilitydirtyService dirtyService; 
	//@Autowired
	//private	MprobabilitydirtyDao probabilitydirtyDao;
	
	@Override
	public LuceneMorphology getLuceneMorph() {
		return model.getLuceneMorph();
	}
	
	@Override
	public List<FindObject> discover(String id, String text) {
		List<FindObject> list=new ArrayList<FindObject>();
		    //Tokenizer tokenizer = token.getTokenizer();
			//String tokens[] = tokenizer.tokenize(text);
			//String tokens[] = text.toString().replaceAll("[\\W&&[^\\s]]", "").split("\\W+");
		String tokens[] = text.toString().split("\\p{P}?[ \\t\\n\\r]+");
		NameFinderME nameFinder = model.getNameFinderModel();
		
		
		
		Span nameSpans[] = nameFinder.find(tokens);
		for (int i=0;i<nameSpans.length;i++)
		{
			
			for (int j=nameSpans[i].getStart();j<nameSpans[i].getEnd();j++)
			{
			FindObject obj=new FindObject();
			String wordobj=tokens[j];
			
			String objid=getObjId(wordobj);
			
			obj.setId(objid);
			obj.setName(wordobj);
			obj.setStrpos(""+nameSpans[i].getStart());
			obj.setStrend(""+nameSpans[i].getEnd());
			
			
			
			list.add(obj);
			
			//list.add(tokens[j]);
			}
		}
		
		
		return list;
	}
	
	public String getObjId(String text){
		String Id="-1";
		Map<String, String> wordmap=new HashMap<String, String>();
		wordmap=model.getSwordmap();
		try{
		Id=wordmap.get(text);
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return Id;
		
	}
	//docid= textorig=
	//list[ objid= strpos= endpos= 
	//]

	@Override
	public List<FindObject> discoverLiniar(String id, String text) {
		try{
			Calendar calendar = Calendar.getInstance();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTime().getTime());	
			
			String dirty=text;
	    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
		String tokens[] = dirty.toString().split("\\p{P}?[ \\t\\n\\r]+");
		List<String[]> alltokens = new  ArrayList<String[]>();
		List<String[]> alltokenstrim = new  ArrayList<String[]>();
		Map<String, Integer>  wordCount=new HashMap<String, Integer>();
		int size = tokens.length;
		
		for(int i=0;i<size;i++){
			String[] indexsingle = new String[2];
			String[] indexbigram = new String[2];
			String[] indextrigram = new String[2];
			String[] indexfourgram = new String[2];
			String[] indexfivegram = new String[2];
			
			String single=null;
			String bigram=null;
			String trigram=null;
			String fourgram=null;
			String fivegram=null;
			
			single=tokens[i];
			indexsingle[0] = single;
			indexsingle[1] = i+"|1";
			
			if(wordCount.containsKey(single)){
			
			int t=wordCount.get(single);
			
			indexsingle[1] = i+"|1|"+(t+1);
			wordCount.put(single, t+1);
			
			}
			else{
				wordCount.put(single, 1);
				indexsingle[1] = i+"|1|1";
			}
			
			
			if(i+1<size){
			bigram=tokens[i]+" "+tokens[i+1];
			indexbigram[0] = bigram;
			indexbigram[1] = i+"|2";
			    if(wordCount.containsKey(bigram)){
				
				int t=wordCount.get(bigram);
				indexbigram[1] = i+"|2|"+(t+1);
				wordCount.put(bigram, t+1);
				
				}
				else{
					wordCount.put(bigram, 1);
					indexbigram[1] = i+"|2|1";
				}
			
			}
			if(i+2<size){
			trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
			indextrigram[0] = trigram;
			indextrigram[1] = i+"|3";
			
		    if(wordCount.containsKey(trigram)){
				
			int t=wordCount.get(trigram);
			indextrigram[1] = i+"|3|"+(t+1);
			wordCount.put(trigram, t+1);
			
			}
			else{
				wordCount.put(trigram, 1);
				indextrigram[1] = i+"|3|1";
			}
			
			}
			if(i+3<size){
			fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
			indexfourgram[0] = fourgram;
			indexfourgram[1] = i+"|4";
			
		    if(wordCount.containsKey(fourgram)){
				
			int t=wordCount.get(fourgram);
			indexfourgram[1] = i+"|4|"+(t+1);
			wordCount.put(fourgram, t+1);
			
			}
			else{
				wordCount.put(fourgram, 1);
				indexfourgram[1] = i+"|4|1";
			}
		    
			}
			if(i+4<size){
				fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
				indexfivegram[0] = fivegram;
				indexfivegram[1] = i+"|5";
				
			    if(wordCount.containsKey(fivegram)){
					
					int t=wordCount.get(fivegram);
					indexfivegram[1] = i+"|5|"+(t+1);
					wordCount.put(fivegram, t+1);
					
					}
					else{
						wordCount.put(fivegram, 1);
						indexfivegram[1] = i+"|5|1";
					}
				
			}
			//System.out.println("="+fivegram); 
			
			
			if(single!=null)
				alltokens.add(indexsingle);
				if(bigram!=null)
					alltokens.add(indexbigram);
					if(trigram!=null)
						alltokens.add(indextrigram);
						if(fourgram!=null)
							alltokens.add(indexfourgram);
							if(fivegram!=null)
								alltokens.add(indexfivegram);
		
							
					          if(i>3500){
					        	  break;
					          }	
		}
		
		
		for (String[] wordtokend: alltokens) { 
			String[] index = new String[2];
			index[0]=wordtokend[0].trim();
			index[1]=wordtokend[1];
			
			alltokenstrim.add(index);
		}
		alltokens=alltokenstrim;
		
		//System.out.println("STEP2...");
		
		List<FindObject> list=new ArrayList<FindObject>();
		
		Map<String, String> liniarmap=new HashMap<String, String>();
		liniarmap=model.getLiniarmap();
		
		Map<String, String> weaksynonimMap=new HashMap<String, String>();
		weaksynonimMap=model.getSynonimMap();
		
		Map<String, String> weakcontextMap=new HashMap<String, String>();
		weakcontextMap=model.getContextMap();
		
		Map<String, String> matmap=new HashMap<String, String>();
		matmap=model.getMatmap();
		
//		System.out.println(liniarmap.size()+"\n");
//		for (Map.Entry<String, String> entry : liniarmap.entrySet())
//		{
//		    System.out.println(entry.getKey() + "/" + entry.getValue());
//		}
		
//		for (String wordtokend: alltokens) { 
//		
//				//String lowerStrd = wordtokend.toLowerCase().trim();
//				System.out.println("alltoken="+wordtokend+"*");
//				
//			}
		
		
		try{
			int k=0;
			for (String[] wordtoken: alltokens) { 
			//for (String wordtoken: tokens) {  
				try{	
					String lowerStr = wordtoken[0].toLowerCase();
					
					
					
					
					String objectmat=matmap.get(lowerStr);
					
					if(objectmat!=null){
					FindObject obj=new FindObject();
					obj.setId(objectmat);
					
					obj.setName(wordtoken[0]);
					
					int b=getPisition(dirty, wordtoken);
					int d=wordtoken[0].length()+b;
					obj.setStrpos(""+b);
					obj.setStrend(""+d);
					obj.setIsmat("true");
					if(b>-1)
					list.add(obj);
					}
					
					
					
					
					String objectid=liniarmap.get(lowerStr);
					
					if(objectid!=null){
					FindObject obj=new FindObject();
					obj.setId(objectid);
					obj.setIsmat("false");
					
					obj.setName(wordtoken[0]);
					
					int b=getPisition(dirty, wordtoken);
					int d=wordtoken[0].length()+b;
					obj.setStrpos(""+b);
					obj.setStrend(""+d);
					if(b>-1)
					list.add(obj);
					else if(b==-1){
						b=0;
						d=wordtoken[0].length();
						obj.setStrpos(""+b);
						obj.setStrend(""+d);
						
						list.add(obj);	
					}
					}
					
					
					
					String weaksynonim=weaksynonimMap.get(lowerStr);
					//String weakcontext=weakcontextMap.get(lowerStr);
					
					
					if(weaksynonim!=null){
					FindObject weakobj=new FindObject();
					 //System.out.println("weaksynonim="+lowerStr+" weaksynonimID="+weaksynonim);
					
					
					//getMyweekID(String weaksynonimText, String weaksynonim, Map<String, String> weakcontextMap, List<String> alltokens, String allText)
					String weakobjectid=getMyweekID(wordtoken, weaksynonim, lowerStr, weakcontextMap, alltokens, dirty);
					//System.out.println("lowerStr="+lowerStr+" objectid="+weakobjectid);
					if(weakobjectid!=null){		
							
					weakobj.setId(weakobjectid);
					weakobj.setName(wordtoken[0]);
					weakobj.setIsmat("false");
					//System.out.println("Begin weaksynonim*_"+wordtoken[0]+"_*get my position_"+wordtoken[1]);
					int bweak=getPisition(dirty, wordtoken);
					
					
					
					int dweak=wordtoken[0].length()+bweak;
					weakobj.setStrpos(""+bweak);
					weakobj.setStrend(""+dweak);
					if(bweak>-1)
					list.add(weakobj);
					else if(bweak==-1){
						bweak=0;
						dweak=wordtoken[0].length();
						weakobj.setStrpos(""+bweak);
						weakobj.setStrend(""+dweak);
						list.add(weakobj);	
					}
					}
					
					
					}
					
				}catch(Exception e){
						e.printStackTrace();
						
					}
				  k++;
		          if(k>3500){
		        	  break;
		          }	
			}
			
			
		
		}catch(Exception e){
			e.printStackTrace();
			
		}
		//---------------------WARNING------------------------
		//do not save
		saveDirty(id, text, alltokens, list, wordCount, timestamp);
		
		
		return list;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
	}
	


	private String getMyweekID(String[] weaksynonimText, String weaksynonim,String weaksynonimlowerStr, Map<String, String> weakcontextMap, List<String[]> alltokens, String allText) {
		
//		for (String wordtokend: alltokens) { 
//			
//			String lowerStrd = wordtokend.toLowerCase().trim();
//			System.out.println("alltoken="+lowerStrd);
//			
//		}
		
		
		Map<String, String> weaksynmap=new HashMap<String, String>();
		if(weaksynonim.contains("|")){
			String[] myData = weaksynonim.split("\\|");
        	for (String syns: myData) {
        		weaksynmap.put(syns, "1");
        	}
			
		}else{
			weaksynmap.put(weaksynonim, "1");
			
		}
		
		Map<String, String> contextMap=new HashMap<String, String>();
		
		contextMap=weakcontextMap;
		String objectid=null;
		int k=0;
		for (String[] wordtoken: alltokens) { 
			
				try{	
					 String lowerStr = wordtoken[0].toLowerCase().trim();
					//System.out.println("token="+lowerStr);
					
					 
					 // if(!weaksynonimlowerStr.equals(lowerStr))
						 //System.out.println("weaksynonimlowerStr="+weaksynonimlowerStr+" lowerStr="+lowerStr); 
					 
					 if(!weaksynonimlowerStr.equals(lowerStr))
					 objectid=contextMap.get(lowerStr);
					 
					 if(objectid==null){
						 
						 //return null;
						 
					 }else{
					//	 System.out.println(" Context_objectid="+objectid);
					
					 if(objectid.contains("|")){
							 String[] objectidData = objectid.split("\\|");
					        	for (String key: objectidData) {
					        		if (weaksynmap.containsKey(key))
					        		{
					        		boolean dd=calcWordsDistance(weaksynonimText, wordtoken[0], allText);
					        		if(dd)
					        		return key;
					        			}
					        		
					        	}
					 }
					 else{
						 if (weaksynmap.containsKey(objectid))
			        			{
							 
							     boolean dd=calcWordsDistance(weaksynonimText, wordtoken[0], allText);
							     if(dd)
							     return objectid;
							     }
					 //System.out.println("lowerStr="+lowerStr+" objectid="+objectid);
					 //String n = weaksynmap.get(objectid);
		        		//if(n!=null)
		        			//return n;
					
					
					 }
					 }
					 
				}catch(Exception e){
					e.printStackTrace();
					return null;
				}
				
				 k++;
		          if(k>3500){
		        	  break;
		          }	
		}
		
		
		
		return null;
	}

	private boolean calcWordsDistance(String[] weaksynonimText, String synonimContext,String text) {
		boolean dist = false;
		try{
		
		int firstW=getPisition(text, weaksynonimText);
		List<Integer> list = new ArrayList<>();
		list=getWordPisition(text, synonimContext);
		int k=0;
		for(Integer secondW:list){
		if(firstW>-1 && secondW >-1)
		{
					

			
			if(secondW>firstW)
			{
				int total=secondW-(firstW+weaksynonimText[0].length());
				if(total<100)
					dist = true;
				
			}
			else{
				
				int total=firstW-(secondW+synonimContext.length());
				if(total<100)
					dist = true;
			}

		}
		k++;
		if(k>30){
      	  break;
        }			
		}
		}catch(Exception e){e.printStackTrace(); }
		return dist;
	}

	private List<Integer> getWordPisition(String text, String synonimContext) {
		List<Integer> list = new ArrayList<>();
		
		
		int myposition=0;
		int i = 0;
		int k=0;
		
		try{
		
	    while (i >= 0){
	    	
	    	  
	    	  myposition=text.indexOf( synonimContext,  myposition);
	    	  
	    	  if(myposition>=0){
	    		  myposition=myposition+synonimContext.length();
	    	  		list.add(myposition); }
	    	  
	    	  i=myposition; //-1
	    
	    	 
	  		  
	          k++;
	          if(k>10){
	        	  break;
	          }
	    }
		
		}catch(Exception e){e.printStackTrace(); }
		  
		  
		return list;
	}

	int getPisition(String Str, String[] wordtoken){
		
		

		int myposition=-1000;  
		try{
		String text=wordtoken[0];
		  String TN=wordtoken[1];
		 
		  
		  
		String[] objectidData = TN.split("\\|");
		 
		 String T=objectidData[0];
		 String I=objectidData[2];
		 
		 
//		if(T.equals("0")){
//	    myposition=Str.indexOf( text );
//		return myposition;
//		}
//		else 
		 if(I.equals("1")){
			 myposition=Str.indexOf( text );
				return myposition;
		}
		else{
			//int t=Integer.parseInt(T);
			int i=Integer.parseInt(I);
			int last=0;
			int lastgo=0;

			
			for(int k=1;k<=i;k++){	
				
				
				last=Str.indexOf( text, lastgo );
				lastgo=last+text.length();
			
				
				
				if(k>20)break;
			}
			
			return myposition=last;	
			
		}
		
	}catch(Exception e){e.printStackTrace(); return myposition;}
		
	}
	
	//----------------------------ProcessMorpho---------------------------
	void saveDirty(String exguid, String text, List<String[]> alltokens, List<FindObject> objlist,Map<String, Integer> wordCount,java.sql.Timestamp timestamp){
		
		
		try {	
			if(objlist.size()>0){
			    String thash = hashString(text);
			    Map<String, String> probabilitycounterMap = model.getPhraseCounterMap();
			    Map<String, Integer> probabilitymain=model.getPhraseTokenMapValue();
			    
			    //System.out.println("probabilitycounterMap size: " +probabilitycounterMap.size() );
			 
			    
			    
			    if(!probabilitycounterMap.containsKey(thash))
			    { 
			    	 
			    	   probabilitycounterMap.put(thash, ""+timestamp);
			    	
			    	  // System.out.println("probabilitymain size: " +probabilitymain.size() );
			    	  
			    	  for (Map.Entry<String, Integer> entry : wordCount.entrySet())
			    	  {
				    	   try{
					    		  String tokens = entry.getKey().trim();
					    		  Integer indx = entry.getValue();
					    		  if(probabilitymain.containsKey(tokens)){
					    			  int cont=probabilitymain.get(tokens);
					    			  probabilitymain.put(tokens, cont+indx); 
					    		  }
					    		  else
					    			  probabilitymain.put(tokens, 1); 
				    		  
				    	      } catch (Exception ex){}
			  	       }
			    	  
			    /*
			    //UUID idOne = UUID.randomUUID();
			    //String thash=String.valueOf(idOne);
			    
				if(thash!=null){
				JSONArray jsonArray = new JSONArray();
				
				JSONObject obj = new JSONObject();
				obj.put("thash", thash);
				jsonArray.add(obj);
				
				List<FindObject> list=new ArrayList<FindObject>();
				list=objlist;
				
				JSONObject objs = getJsonFromMyFormObject(list);
				jsonArray.add(objs);
				
				JSONObject objtokens = getJsonTokens(alltokens);
				jsonArray.add(objtokens);
				
				String tokenjson = jsonArray.toString();
				
				Probabilitydirty dirty =new Probabilitydirty();
				dirty.setExguid(exguid);
				dirty.setThash(thash);
				dirty.setTokenjson(tokenjson);
				    Calendar calendar = Calendar.getInstance();
				    java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
				    dirty.setDateupdate(ourJavaTimestampObject);
				    
				//do not save
				   
				dirtyService.addProbabilitydirty(dirty); //System.out.println("Save lowerStr="+thash);
				//probabilitydirtyDao.save(exguid, thash, tokenjson);
				
				
				}*/
				}
				
			}
		} catch (Exception ex){ex.printStackTrace();}
	}
	
	String hashString(String message) throws Exception {
	
	try {
	MessageDigest digest = MessageDigest.getInstance("MD5");
	byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
	return convertByteArrayToHexString(hashedBytes);
	} catch (Exception ex)
	{}
	return null;
	}
	
	String convertByteArrayToHexString(byte[] arrayBytes) {
	StringBuffer stringBuffer = new StringBuffer();
	for (int i = 0; i < arrayBytes.length; i++) {
	stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
	}
	return stringBuffer.toString();
	}
	
	public JSONObject getJsonFromMyFormObject(List<FindObject> form)
	  {
	    JSONObject responseDetailsJson = new JSONObject();
	    JSONArray jsonArray = new JSONArray();

	    for (int i = 0; i < form.size(); i++)
	    {
	      JSONObject formDetailsJson = new JSONObject();
	      formDetailsJson.put("id", form.get(i).getId());
	      formDetailsJson.put("name", form.get(i).getName());

	      jsonArray.add(formDetailsJson);
	    }
	    responseDetailsJson.put("findObject", jsonArray);
	    return responseDetailsJson;
	  }
	
	
	public JSONObject getJsonTokens(List<String[]> alltokens)
	  {
	    JSONObject responseDetailsJson = new JSONObject();
	    JSONArray jsonArray = new JSONArray();

	    for (int i = 0; i < alltokens.size(); i++)
	    {
	      JSONObject formDetailsJson = new JSONObject();
	      
	      String[] z = alltokens.get(i);
	      String tokens = z[0];
	      String indx = z[1];
	      formDetailsJson.put("name", tokens);
	      formDetailsJson.put("value", indx);

	      jsonArray.add(formDetailsJson);
	    }
	    responseDetailsJson.put("tokens", jsonArray);
	    return responseDetailsJson;
	  }
	
	
	
	
	
	
	
	

	
	@Override
	public List<FindObject> discoverVerbs(String id, String text) {
		try{
			Calendar calendar = Calendar.getInstance();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTime().getTime());	
			
			String dirty=text;
	    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
		String tokens[] = dirty.toString().split("\\p{P}?[ \\t\\n\\r]+");
		List<String[]> alltokens = new  ArrayList<String[]>();
//		List<String[]> alltokenstrim = new  ArrayList<String[]>();
		Map<String, Integer>  wordCount=new HashMap<String, Integer>();
		Map<String, Integer>  wordPhraseCount=new HashMap<String, Integer>();
		int size = tokens.length;
		

		
		for(int i=0;i<size;i++){
			String[] indexsingle = new String[2];
			String[] indexbigram = new String[2];
//			String[] indextrigram = new String[2];
//			String[] indexfourgram = new String[2];
//			String[] indexfivegram = new String[2];
			
			String single=null;
			String bigram=null;
//			String trigram=null;
//			String fourgram=null;
//			String fivegram=null;
			
			single=tokens[i];
			indexsingle[0] = single;
			indexsingle[1] = i+"|1";
			
//			if(wordCount.containsKey(single)){
//			
//			int t=wordCount.get(single);
//			
//			indexsingle[1] = i+"|1|"+(t+1);
//			wordCount.put(single, t+1);
//			
//			}
//			else{
//				wordCount.put(single, 1);
//				indexsingle[1] = i+"|1|1";
//			}
			
			
			if(i+1<size){
			bigram=tokens[i].toLowerCase()+" "+tokens[i+1].toLowerCase();
			indexbigram[0] = bigram;
			indexbigram[1] = i+"|2";
			    if(wordCount.containsKey(bigram)){
				
				int t=wordCount.get(bigram);
				indexbigram[1] = i+"|2|"+(t+1);
				wordCount.put(bigram, t+1);
				
				}
				else{
					wordCount.put(bigram, 1);
					indexbigram[1] = i+"|2|1";
				}
			
			}
//			if(i+2<size){
//			trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
//			indextrigram[0] = trigram;
//			indextrigram[1] = i+"|3";
//			
//		    if(wordCount.containsKey(trigram)){
//				
//			int t=wordCount.get(trigram);
//			indextrigram[1] = i+"|3|"+(t+1);
//			wordCount.put(trigram, t+1);
//			
//			}
//			else{
//				wordCount.put(trigram, 1);
//				indextrigram[1] = i+"|3|1";
//			}
//			
//			}
//			if(i+3<size){
//			fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
//			indexfourgram[0] = fourgram;
//			indexfourgram[1] = i+"|4";
//			
//		    if(wordCount.containsKey(fourgram)){
//				
//			int t=wordCount.get(fourgram);
//			indexfourgram[1] = i+"|4|"+(t+1);
//			wordCount.put(fourgram, t+1);
//			
//			}
//			else{
//				wordCount.put(fourgram, 1);
//				indexfourgram[1] = i+"|4|1";
//			}
//		    
//			}
//			if(i+4<size){
//				fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
//				indexfivegram[0] = fivegram;
//				indexfivegram[1] = i+"|5";
//				
//			    if(wordCount.containsKey(fivegram)){
//					
//					int t=wordCount.get(fivegram);
//					indexfivegram[1] = i+"|5|"+(t+1);
//					wordCount.put(fivegram, t+1);
//					
//					}
//					else{
//						wordCount.put(fivegram, 1);
//						indexfivegram[1] = i+"|5|1";
//					}
//				
//			}
			//System.out.println("="+fivegram); 
			
			
			//if(single!=null)
			//	alltokens.add(indexsingle);
				if(bigram!=null)
					alltokens.add(indexbigram);
				//if(trigram!=null)
				//		alltokens.add(indextrigram);
				//	if(fourgram!=null)
				//			alltokens.add(indexfourgram);
				//			if(fivegram!=null)
				//				alltokens.add(indexfivegram);
		
							
					          if(i>3500){
					        	  break;
					          }	
		}
		
		
//		for (String[] wordtokend: alltokens) { 
//			String[] index = new String[2];
//			index[0]=wordtokend[0].trim();
//			index[1]=wordtokend[1];
//			
//			alltokenstrim.add(index);
//		}
//		alltokens=alltokenstrim;

		//---------------------WARNING------------------------
		
	
		//
		 for (Map.Entry<String, Integer> entry : wordCount.entrySet()){
			String bigram=entry.getKey();
			Integer indx = entry.getValue();
			bigram=bigram.trim();
			
			String wfirstphrase=null;
			String wsecondphrase=null;
			
			
			try{
			if(bigram.contains(" ")){
				String[] myData = bigram.split(" ");
				
				String firstw=myData[0];
				boolean isfirstC=false;
				
				String secondw=myData[1];
				boolean issecond=false;
				
				List<String> wordBaseForms = getLuceneMorph().getMorphInfo(firstw);
				
				for (String s : wordBaseForms)
				{
					
					if(s.contains("|")){
						String[] morfdata = s.split("\\|");
						String word=morfdata[0];
						String morf=morfdata[1];
						if(isPresent("�",morf)){
							
							wfirstphrase=word;
							isfirstC=true;
							
						}
				        
						break;
					}
				}
				
				if(isfirstC){
					List<String> wordGBaseForms = getLuceneMorph().getMorphInfo(secondw);
					for (String s : wordGBaseForms)
					{
						
						if(s.contains("|")){
							String[] morfdata = s.split("\\|");
							String word=morfdata[0];
							String morf=morfdata[1];
							if(isPresent("�",morf)){
								
								wsecondphrase=word;
								issecond=true;
								
							}
					        
							break;
						}
						
					}
				}
				
				if(isfirstC&&issecond){
					
					wordPhraseCount.put(wfirstphrase+" "+wsecondphrase, indx);
				}
				
			}
		 }catch(Exception e){
				//e.printStackTrace();
			 }


			}
		//do not save
		saveVerb(id, text, wordPhraseCount, timestamp);
		
		
		return null;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
	}
	boolean isPresent(String serchword, String text){
		
		 String regex = "\\b"+serchword+"\\b";
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(text);
	        int k=0;
	        while(matcher.find() == true){
	        	
	        	return true;
	        }
			return false;
	} 
	//----------------------------ProcessMorpho---------------------------
	void saveVerb(String exguid, String text, Map<String, Integer> wordCount,java.sql.Timestamp timestamp){
	
		
		try {	
			
			    String thash = hashString(text);
			    Map<String, String> probabilitycounterMap = model.getPhraseCounterMap();
			    Map<String, Integer> probabilitymain=model.getPhraseTokenMapValue();
			    
			    //System.out.println("probabilitycounterMap size: " +probabilitycounterMap.size() );
			 
			    
			    
			    if(!probabilitycounterMap.containsKey(thash))
			    { 
			    	 
			    	   probabilitycounterMap.put(thash, ""+timestamp);
			    	
			    	  // System.out.println("probabilitymain size: " +probabilitymain.size() );
			    	  
			    	  for (Map.Entry<String, Integer> entry : wordCount.entrySet())
			    	  {
				    	   try{
					    		  String tokens = entry.getKey().trim();
					    		  Integer indx = entry.getValue();
					    		  if(probabilitymain.containsKey(tokens)){
					    			  int cont=probabilitymain.get(tokens);
					    			  probabilitymain.put(tokens, cont+indx); 
					    		  }
					    		  else
					    			  probabilitymain.put(tokens, 1); 
				    		  
				    	      } catch (Exception ex){}
			  	       }
			    	  

				}
				
			
		} catch (Exception ex){ex.printStackTrace();}
	}
}
