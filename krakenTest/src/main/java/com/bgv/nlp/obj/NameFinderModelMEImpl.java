package com.bgv.nlp.obj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.languagetool.JLanguageTool;
import org.languagetool.language.Russian;
import org.maltparser.MaltParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bgv.dao.IObjectsDAO;
import com.bgv.dao.probability.IilGsonReadDAO;
import com.bgv.entity.Person;
import com.bgv.entity.Sheduler;
import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.spam.EvalPrecisionDAO;
import com.bgv.nlp.spam.GISModelDAO;
import com.bgv.nlp.spam.LearningSample;
import com.bgv.nlp.spam.TokenizerDAO;
import com.bgv.service.ILearnMESpam;
import com.bgv.service.IPersonService;
import com.bgv.service.IQnonameService;
import com.bgv.service.IShedulerService;
import com.bgv.springmvc.domain.Config;

import opennlp.maxent.GISModel;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.InvalidFormatException;
//import org.apache.lucene.analysis.PorterStemmer;

@Service
public class NameFinderModelMEImpl implements NameFinderModelMEDAO{
	static NameFinderME	currentModel;
	static Map<String, String> swordmap=new HashMap<String, String>();
	static Map<String, String>  familii=new HashMap<String, String>();
	
	static Map<String, String> liniarmap=new HashMap<String, String>();
	static Map<String, String> idcategorymapobj=new HashMap<String, String>();
	
	static Map<String, String> weakSynonimMap=new HashMap<String, String>();
	static Map<String, String> weakContextMap=new HashMap<String, String>();
	static Map<String, String> matwords=new HashMap<String, String>();
	static Map<String, String> probabilitycounterMap =new HashMap<String, String>();
	static Map<String, String> symantexmapword = new HashMap<String, String>();
	static Map<String, String> symantexmapcategory = new HashMap<String, String>();
	
	static Map<String, List> adjectiveNounMap = new ConcurrentHashMap<String, List>();
	
	static Map<String, ArrayList> symantexhashMap = new ConcurrentHashMap<String, ArrayList>();
	static Map<String, String> voprNarechia = new  HashMap<String, String>();
	
	 
	 static Map<String, String> idcategorymapobjmed=new HashMap<String, String>();
	 static Map<String, String> swordmapmed=new HashMap<String, String>();
	 static Map<String, String> liniarmapmed=new HashMap<String, String>();
		static Map<String, String> weakSynonimMapmed=new HashMap<String, String>();
		static Map<String, String> weakContextMapmed=new HashMap<String, String>();
	
	 
	 
		static   Map<String,String> verbstatements = new ConcurrentHashMap<String,String>();
		static   Map<String,String> verbcategory = new ConcurrentHashMap<String,String>();

		static   Map<String,String> verbmaincategory = new ConcurrentHashMap<String,String>();
		static   Map<String,String> verbsubcategory = new ConcurrentHashMap<String,String>();
		
		
		/*    
		 * "l1": "требование переместиться или остаться требование перемещения или удаления (гнать, посылать, удалять)", 
    	   "l2": "(26; 241) распоряжение, пору чение, назначение", 
    	   "words": []
		 */
		//уровень L1 категории глагола
		@Override
		public Map<String, String> getVerbGlobalCategoryMap() {
			return verbcategory;
		}
		
		//уровень L2 категории глагола
		@Override
		public Map<String, String> getVerbSubCategoryMap() {
			return verbsubcategory;
		}
	
	@Override
	public Map<String, String> getvoprNarechiaMap() {
		return voprNarechia;
	}
	
	 @Autowired
	 private IilGsonReadDAO iilGsonRead;
	 
	 @Autowired
	 private  IQnonameService nonameService;
	 
	 @Autowired
	 private FastModelDAO fastmodel;
	
	@Override
	public Map<String, ArrayList> getSymantexhashMap() {
		return symantexhashMap;
	}
	@Override
	public Map<String, List> getadjectiveNounMap() {
		return adjectiveNounMap;
	}
	
	
	//static Map<String, Integer> tokenMapValue =new HashMap<String, Integer>();
	
	static Map<String, Integer> tokenMapValue =new ConcurrentHashMap<String, Integer>();
	
	static Map<String, String> nounVerb=new ConcurrentHashMap<String, String>();
	
	@Override
	public  void setProbabilitycounterMap(Map<String, String> probabilitycounterMap) {
		NameFinderModelMEImpl.probabilitycounterMap = probabilitycounterMap;
	}
	@Override
	public  void setTokenMapValue(Map<String, Integer> tokenMapValue) {
		NameFinderModelMEImpl.tokenMapValue = tokenMapValue;
	}

	static LuceneMorphology luceneMorph;
	static JLanguageTool languageTool;
	
	static MaltParserService maltParser;
	static WordVectors vecl;
	
	@Override
	public WordVectors getWordVectors() {
		return vecl;
	}
	public void setWordVectors(WordVectors lvecl) {
		NameFinderModelMEImpl.vecl=lvecl;
	}
	
	@Override
	public JLanguageTool getLanguageTool() {
		return languageTool;
	}
	public static void setLanguageTool(JLanguageTool languageTool) {
		NameFinderModelMEImpl.languageTool = languageTool;
	}
	@Override
	public LuceneMorphology getLuceneMorph() {
		return luceneMorph;
	}

	public void setLuceneMorph(LuceneMorphology luceneMorph) {
		NameFinderModelMEImpl.luceneMorph = luceneMorph;
	}
	
	@Override
	public MaltParserService getMaltParser(){
		
		return	maltParser;
	}
	
	@Override
	public void setMaltParser(MaltParserService maltParser){
		NameFinderModelMEImpl.maltParser = maltParser;
	}
	
	
	@Override
	 public  Map<String, String> getFamilii() {
		return familii;
	}
	@Override
	public  void setFamilii(Map<String, String> familii) {
		NameFinderModelMEImpl.familii = familii;
	}

	@Autowired
	 private TokenizerDAO token;
	
	 @Autowired
	 private IObjectsDAO objcs;
	
	 @Autowired
	 private IPersonService personService;
	 
	 @Autowired
	 private ILearnMESpam learnService;
	 
	 @Autowired
	 private IShedulerService shedulerService;
	 
	 @Autowired
	 private GISModelDAO spammodel;
	 
	 @Autowired
	 private EvalPrecisionDAO precision;
	 
	 @Override
	 public Map<String, String> getSwordmap() {
		return swordmap;
	 }
	
	@Override
	public Map<String, String> getPhraseCounterMap() {
		return probabilitycounterMap;
	}
	
	@Override
	public Map<String, Integer> getPhraseTokenMapValue() {
		return tokenMapValue;
	}
	
	@Override
	public Map<String, Integer> getPhraseSortedTokenMapValue() {
		
		return	sortHashMapByValues(tokenMapValue);
		
	}
	@Override
	public  Map<String, String> getIdcategorymapobj() {
		return idcategorymapobj;
	}
	@Override
	public void setIdcategorymapobj(Map<String, String> idcategorymapobj) {
		NameFinderModelMEImpl.idcategorymapobj = idcategorymapobj;
	}
	@Override
	public Map<String, String> getLiniarmap() {
		return liniarmap;
	}
	
	@Override
	public Map<String, String> getSynonimMap() {
		return weakSynonimMap;
	}
	
	@Override
	public Map<String, String> getContextMap() {
		return weakContextMap;
	}

	@Override
	public Map<String, String> getLiniarmapmed() {
		return liniarmapmed;
	}
	
	@Override
	public Map<String, String> getSynonimMapmed() {
		return weakSynonimMapmed;
	}
	
	@Override
	public Map<String, String> getContextMapmed() {
		return weakContextMapmed;
	}

	
	@Override
	public Map<String, String> getMatmap() {
		return matwords;
	}
	 @Override
	 public Map<String, String> getNounVerbMap() {
		return nounVerb;
	 }
	 
	 @Override
	 public void setNounVerbMap(Map<String, String> map) {
		this.nounVerb=map;
	 }
	 
	@Override
	public NameFinderME getNameFinderModel() {
		
		return currentModel;
	}
	
	@PostConstruct
	public void initialize() throws IOException{
		 Config cf=new Config();
		 String rooot=cf.getRoot();
		 String name=cf.getNamefindermodel();
		 String objtext=cf.getObjectstext();
		 String textFileName=rooot+objtext;
		 
		 String modelFileName=rooot+name;
		 
		    InputStream modelIn = new FileInputStream(modelFileName);
			TokenNameFinderModel model = new TokenNameFinderModel(modelIn);
			
			
			 currentModel =  new NameFinderME(model);
			 File f = new File(textFileName);
			 if(f.exists() && !f.isDirectory()) { 
				 textFileName=rooot+objtext;
			 }else{
				 textFileName=rooot+"objects_v2.txt";
			 }
			 loadObjects(textFileName);
			 
			 loadMEDObjects(rooot+"medobjects");
			 
			 
			 LuceneMorphology luceneMorph = new RussianLuceneMorphology();
			 setLuceneMorph(luceneMorph);
			 
			 JLanguageTool langTool = new JLanguageTool(new Russian());
			 setLanguageTool(langTool);
		  
			try{	 
			 MaltParserService service =  new MaltParserService();
			 service.initializeParserModel("-c rus-test -m parse -w . -lfi parser.log");
			 setMaltParser(service);
			} catch (Exception e) {e.printStackTrace();}
			 
			
	        try{ 
	        	
	        	WordVectors vecl= WordVectorSerializer.readWord2VecModel(new File("/mindscan/wildfly/02032017/all.norm-sz100-w10-cb0-it1-min100.w2v")); //E:/Data/all.norm-sz100-w10-cb0-it1-min100.w2v E:/Data/all.norm-sz500-w10-cb0-it3-min5.w2v
	        	setWordVectors(vecl);
	        } catch (Exception e) {e.printStackTrace();}
			
			 
			// PorterStemmer stemmer = new PorterStemmer();
			 
			 readMat(rooot+"rumatdictionary.txt");
			 
			 
			 try {
				//loadSymantex(rooot+"symantic.txt");
				loadSymantexB();
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			 
			 
			 
			 Map<String, String> checkIfGeo=new HashMap<String, String>();
			 checkIfGeo.put("Q515","Q515");
			 checkIfGeo.put("Q2264924","Q2264924");
			 checkIfGeo.put("Q208511","Q208511");
			 checkIfGeo.put("Q6256","Q6256");
			 checkIfGeo.put("Q1520223","Q1520223");
			 checkIfGeo.put("Q7270","Q7270");
			 checkIfGeo.put("Q13226383","Q13226383");
			 checkIfGeo.put("Q2221906","Q2221906");
			 checkIfGeo.put("Q17334923","Q17334923");
			 checkIfGeo.put("Q15916867","Q15916867");
			 checkIfGeo.put("Q56061","Q56061");
			 checkIfGeo.put("Q15642541","Q15642541");
			 checkIfGeo.put("Q1496967","Q1496967");
			 checkIfGeo.put("Q82794","Q82794");
			 
			 
			//subclass of =Q515 Q2264924 Q208511 Q6256 Q1520223 Q7270 Q13226383 Q2221906 Q17334923
			fastmodel.setCheckIfGeo(checkIfGeo);
			
			 Map<String, String> checkIfHuman=new HashMap<String, String>();
			 checkIfHuman.put("Q5","Q5");
			 checkIfHuman.put("Q202444","Q202444");
			 
			fastmodel.setCheckIfHuman(checkIfHuman);
			
			Map<String, String> checkIfOrg=new HashMap<String, String>();
			checkIfOrg.put("Q43229","Q43229");
			checkIfOrg.put("Q5613113","Q5613113");
			checkIfOrg.put("Q740752","Q740752");
			checkIfOrg.put("Q4830453","Q4830453");
			
			//instance of P31=Q43229
			//subclass of P279=Q43229
			//otherprops topic's main category=Q5613113/Q43229
			fastmodel.setCheckIfOrg(checkIfOrg);
			 
			 
			 voprNarechia.put("кто", "кто");
			 voprNarechia.put("что", "что");
			 voprNarechia.put("который", "который");
			 voprNarechia.put("чей", "чей");
			 voprNarechia.put("каков", "каков");
			 voprNarechia.put("сколько", "сколько");
			 voprNarechia.put("где", "где");
			 voprNarechia.put("куда", "куда");
			 voprNarechia.put("когда", "когда");
			 voprNarechia.put("откуда", "откуда");
			 voprNarechia.put("почему", "почему");
			 voprNarechia.put("зачем", "зачем");
			 
			 Map<String, String> negpred=new HashMap<String, String>();
			 negpred.put("без", "без");
			 //negpred.put("анти", "анти");
			 negpred.put("бес", "бес");
			 negpred.put("не", "не");
				negpred.put("ни", "ни");
				//negpred.put("дез", "дез");
				fastmodel.setWordNegPreglog(negpred);
			 
			 try {		 
				         loadSinonims(rooot+"sinonimsnew.txt");
			 			 loadSinonimsv(rooot+"sinonimsv.txt");
			 			 loadExclude(rooot+"exclude.txt");
			 			//loadMorfema(rooot+"morfema.txt");
			 			 loadMorfema(rooot+"morfemaweb.txt");
			 			 loadFamilii(rooot+"familii.txt");
			 			 readPoliteh(rooot+"politeh.txt");
			 			 readEconomics(rooot+"economics.txt");
			 			 loadPredlogMesta();
			 			
			 			readKvantor(rooot+"kvantor.txt");
			 			
			 			loadVerbsJSON(rooot+"sem423.txt");
			 			loadAdjective(rooot+"AdjectiveNoun.txt");
			 			
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
			 
			 restLoadQnoname();
	}
	
	
    public void restLoadQnoname() throws IOException {
    	nonameService.getLarge();
	}
	
	public  void loadVerbsJSON(String jsonData) throws IOException  {
		 int catg=1;
		// Open the file
					FileInputStream fstream = new FileInputStream(jsonData);
					BufferedReader br = new BufferedReader(new InputStreamReader(fstream,"UTF8"));

					String strLine;

					//Read File Line By Line
					boolean wordbegin=false;
					String value="";
					String category="";
					
					while ((strLine = br.readLine()) != null)   {
						 ///System.out.println (clean(strLine));	
						 String word = clean(strLine).trim();
						
						 if(!word.equals("")){
						 if(word.contains("l1")){
							 
							 //end words 
							 
							String s=word.replaceAll("-", " "); 
					        	s=s.replaceAll("[^А-я\\s-]", "").toLowerCase().trim();
					        	
					        	value=s;
					        	category=s;
					        	verbmaincategory.put(value, ""+catg);
					        	catg++;
					     
							 wordbegin=false; 
						 }
						 if(word.contains("l2")){
							
							 String s=word.replaceAll("-", " "); 
					        	s=s.replaceAll("[^А-я\\s-]", "").toLowerCase().trim();
					        	value=value+"#"+s;
					        	verbsubcategory.put(value, ""+catg);
					      	
							 wordbegin=false;						 
						 }
						 if(word.contains("words")){
						
							 wordbegin=true;
						 }
						 if(wordbegin){
							 if(!word.equals("words")){
								
								 
								 /*
								 if(verbcategory.containsKey(word)){
		            		    		String dd = verbcategory.get(word);
		            		    		
		            		    		verbcategory.put(word, dd+"/"+category);
		            		    		
		            		    	}else
		        		    		*/
		            		    		verbcategory.put(word, category);
								 
								 
								 
							  
								if(verbstatements.containsKey(word)){
	            		    		String dd = verbstatements.get(word);
	            		    		
	            		    		verbstatements.put(word, dd+"/"+value);
	            		    		
	            		    	}else
	        		    		
	            		    		verbstatements.put(word, value);
							 
								
								
								
							 
							 }
						 }
						 }
						
					}
					
					/*
					int i=1; 
					for (Map.Entry<String, String> entry : verbcategory.entrySet())
					{
					    System.out.println(i+"="+entry.getKey() + " value=" + entry.getValue());
					    i++;
					}*/


	}
	
	public void loadFamilii(String args) {

		BufferedReader br = null;

		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(args));
            int k=1;
			while ((sCurrentLine = br.readLine()) != null) {
				try{
					
					String word =clean(sCurrentLine.trim()).toLowerCase();
					if(word!=null)
				    familii.put(word, ""+k);
			        k++;
			      } catch (Exception e) {}
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
//	 @Override
//	public void  loadPositive(String filea) throws InvalidFormatException, IOException, ParseException{
//		Map<String, String> wordmap=new HashMap<String, String>();
//		// Open the file
//		FileInputStream fstream = new FileInputStream(filea);
//		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
//
//		String strLine;
//
//		//Read File Line By Line
//		while ((strLine = br.readLine()) != null)   {
//		try{ String word=strLine.toLowerCase().trim();
//		word=word.replaceAll("[^�-�\\s-]", "");
//		
//		     System.out.println("--"+word+"--");
//		List<String> wordFormsandMorf = luceneMorph.getMorphInfo(word);
//	
//		for (String s : wordFormsandMorf)
//		{
//			if(s.contains("|")){
//				   String[] myData = s.split("\\|");
//				   String nword =myData[0].trim();
//				   String morf =myData[1];
//				   word = nword;
//				   try{ wordmap.put(clean(word), filea); }catch(Exception e){	e.printStackTrace();}
//			}
//		   
//		}
//		
//		
//		
//        wordmap.put(clean(word), filea); }catch(Exception e){ System.out.println("**"+strLine+"**");	e.printStackTrace();}
//		}
//		br.close();
//		
//		fastmodel.setWordPositive(wordmap);
      
//	}
	@Override
	public void loadAdjective(String filename){
		
		Map<String, List> wordmap=new HashMap<String, List>();
		BufferedReader br = null;

		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(filename));
            //int k=1;
			while ((sCurrentLine = br.readLine()) != null) {
				try{
				if(sCurrentLine.contains("#")){
					  String[] worddata =  sCurrentLine.split("#");
					  
					  String key= worddata[0].toLowerCase().trim();
					  String value= worddata[1];
						String aa[] = value.toString().split(" ");
						List<String> words=new ArrayList<String>();
						for(String z:aa){
							z = z.toLowerCase().trim();
							//z=z.replaceAll("[^А-я\\s-]", "");
							if(z.length()>1)
							words.add(z);
						}
						
						wordmap.put(key, words);
					  
				}
				} catch (Exception e) {}
				
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		adjectiveNounMap=wordmap;
		
	}
	
	@Override
	public void  loadEmotion(String negative) {
		
		Map<String, String> morfema=fastmodel.getMorfema();
		Map<String, String> wordmap=new HashMap<String, String>();
		Map<String, String>  wordmaporig=new HashMap<String, String>();
		 Config cf=new Config();
		 String rooot=cf.getRoot();
	     File folderneg = new File(rooot+negative);
	     for (final File fileEntry : folderneg.listFiles()) {
	    	    try {
	    	    String	filea=rooot+negative+"/"+fileEntry.getName();
			
	    	    System.out.println("Open the file: "+filea+""); 
		// Open the file
		FileInputStream fstream = new FileInputStream(filea);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
			String word=strLine.toLowerCase().trim();
			      word=word.replaceAll("[^А-я\\s-]", ""); 
			      
			      wordmaporig.put(clean(word), negative);
		try{ 
		          List<String> wordFormsandMorf = luceneMorph.getMorphInfo(word);
	
					for (String s : wordFormsandMorf)
					{
						if(s.contains("|")){
							   String[] myData = s.split("\\|");
							   String nword =myData[0].trim();
							   String morf =myData[1];
							  
							   if(morfema.containsKey(nword)){
								   String m= morfema.get(nword);
								   if(m.contains("|")){
									   String[] mdata = m.split("\\|");   
									    int size = mdata.length;
										for(int i=0;i<size;i++){
											String single=mdata[i];
											 //wordmap.put(clean(single), morf);
											 wordmap.put(clean(single), negative);
										}
									   
								   }else
								   wordmap.put(clean(m), negative);
								   
								   
					        	}
							   
							  // 
							   try{ wordmaporig.put(clean(nword), negative);}catch(Exception e){	e.printStackTrace();}
							   try{ wordmap.put(clean(nword), negative); }catch(Exception e){	e.printStackTrace();}
						}
					   
					}
		
		}catch(Exception e){}
		
		wordmap.put(clean(word), negative); 
		wordmaporig.put(clean(word), negative);
		}
		br.close();
	    		} catch (Exception e) {}
	     }
	    if(negative.equals("positive"))
	    	fastmodel.setWordPositive(wordmap);
	    else
	    	fastmodel.setWordNegative(wordmap);
	    
	    if(negative.equals("positive"))
	    	fastmodel.setWordPositiveHuman(wordmaporig);
	    else
	    	fastmodel.setWordNegativeHuman(wordmaporig);
		
	}
	
	
//	@Scheduled(fixedRate=20000000)
    public void doTask() throws IOException {
		System.out.println("Start learn tasks...");
		//Tokenizer tokenizer = token.getTokenizer();
		java.util.Date dates= new java.util.Date();
		Timestamp stamp = new Timestamp(dates.getTime());
		   
		List<Sheduler> s = shedulerService.getAllSheduler();
		Sheduler toupdate = new Sheduler();
		toupdate.setLastupdate(stamp);
		Config conf=new Config(); 
		String modelname = conf.getModel();
		for(Sheduler l:s){
		int	spammodelmax=0;
		double bestvalue=0;
		
		String name = l.getSname();	
		
		if(name.equals("spammodel")){
			
				spammodelmax=l.getLastpid();
				toupdate=l;
				String tmodelname = l.getModelname();
				if(!tmodelname.equals("")&&tmodelname!=null){
					modelname = tmodelname;
					 BigDecimal bd=l.getPrecisioncount();
					 double d = bd.doubleValue(); 
					 
					bestvalue=d;
				}
				
			}
		int maxdocprocessed = 0;	
		
			maxdocprocessed = personService.getMaxDocuments();
			
			
		if(maxdocprocessed>spammodelmax){
			
			toupdate.setLastpid(maxdocprocessed);
	    	java.util.Date date= new java.util.Date();
   	        Timestamp d = new Timestamp(date.getTime());
	    	List<Person> p = personService.getAllDocuments(d);
	    	Collection<LearningSample> el=new ArrayList<LearningSample>();
	    	int k=0;
	    	Map<String,String> mp=new HashMap<String,String>();
	    	int limit=0;
	    	int limmax=conf.getLimitEdu();
	    	for(Person w:p){
	    		String content =w.getCity();
	    		//String ltext = normalizeText(content, tokenizer); 
	    		String ltext = normalizeText(content); 
	    		String cls=w.getGender();
	    		mp.put(ltext, cls);
	    		
	    		limit++;
	    		if(limit>limmax) 
	    		break; 
	    		
	    	k++;
	    	}
	    	for (Map.Entry<String, String> entry : mp.entrySet()){
	        	    String content =entry.getKey();
	            	String clss =entry.getValue();	
	            	LearningSample mind=new LearningSample(clss, content);
		    		el.add(mind);
	    	
	    	}
	    
	    	
	    	System.out.println("Save model...");
	    	 if (k>0){
	    		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
			       modelname = "spammodel_"+dateFormat.format( new Date() ) ;
			       toupdate.setModelname(modelname);
			       System.out.println("modelname="+modelname);      
			       
			 learnService.learn(el, modelname);
		     try {
				Thread.currentThread().sleep(10000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		     Config cf=new Config();
			 String rooot=cf.getRoot();
			 String fname=modelname;
			 String modelFileName=rooot+fname;
			 String evalfile=cf.getEvalfile();
			 
			 String evalfilename=rooot+evalfile;
			
			 System.out.println("precision.bestvalue="+bestvalue);
			 double current = precision.evaluateModel(evalfilename, modelFileName); 
			 System.out.println("precision.evaluateModel="+current);
			 //как найти лучший результат
			 //сейчас toupdate не содержит лучший результат
			 
			 if(bestvalue<current){
		     BigDecimal bcurrent = new BigDecimal(current, MathContext.DECIMAL64);
			 toupdate.setPrecisioncount(bcurrent);
			 
			 GISModel newmodel = (GISModel)new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel();
			 spammodel.setCurrentModel(newmodel); 
			 cf.setModel(modelname);}
			 
	    	 }else{
	    	    
	    		 //обновляем в базе лог
	    		 shedulerService.updateShedulerLog(toupdate);
	    		 //ничего не обновляем в моделе
	    		 toupdate=null;
	    	 }
		}
		
		}
		
		if(toupdate!=null)
		{	
			  
			//!!!! не записываю новую модель без условия её качества
			shedulerService.updateSheduler(toupdate);
	    }
		int o = personService.getMaxDocuments();
    	
		System.out.println("Task Completed...");
    	System.out.println("="+o);

    	
    }
	
	private String normalizeText(String content) {
//		private  String normalizeText(String content, Tokenizer tokenizer) {
		    String trainstr ="";
			 String newStr = content.replaceAll("\\p{Cntrl}", " ");
//			 newStr = newStr.replaceAll("\\d+", "");
//			 text = newStr.replaceAll("\\^w", "");
//			 newStr = text.replaceAll("[^А-Яа-я\\s\\.]", "");
//			 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); System.out.println("TEXT="+newStr); return newStr;}
			// try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){return newStr;}
			 
			 try{
				 
				 trainstr=getTokets(newStr);
			 
			 }catch(Exception e){return newStr;}
			 
			 
		return trainstr;
		}
		
		private String getTokets(String text) {
			StringBuffer sb=new StringBuffer();
			String tokens[]	= text.toString().toString().split("\\p{P}?[ \\t\\n\\r]+");
			for (String a : tokens)
				sb.append(a).append(" ");
			   
			
			return sb.toString();
			
		}	   
//	   private String normalizeText(String content, Tokenizer tokenizer) {
//		     String trainstr ="";
//			 String newStr = content.replaceAll("\\p{Cntrl}", " ");
//		 newStr = newStr.replaceAll("\\d+", "");
//		 text = newStr.replaceAll("\\^w", "");
//		 newStr = text.replaceAll("[^А-Яа-я\\s\\.]", "");
//			 try{trainstr=getTokets(newStr, tokenizer);}catch(Exception e){e.printStackTrace(); return newStr;}
//			 
//			 
//		return trainstr;
//	}
//
//
//
//	private String getTokets(String text, Tokenizer tokenizer) {
//		StringBuffer sb=new StringBuffer();
//		String tokens[] = tokenizer.tokenize(text);
//		for (String a : tokens)
//			sb.append(a).append(" ");
//		   
//		
//		return sb.toString();
//		
//	}
		
		
		//@Scheduled(fixedRate=60000)
	    public void updateObjectTask() throws IOException {
			
			try {
				boolean sts=objcs.update();
				 Config cf=new Config();
				 String rooot=cf.getRoot();
				
				 String objtext=cf.getObjectstext();
				 String textFileName=rooot+objtext;
					if(sts){
							loadObjects(textFileName);
					}else{
					
						textFileName=rooot+"objects_v2.txt";
						loadObjects(textFileName);
					}
				
				
			} catch (ClassNotFoundException | SQLException e) {
				
				e.printStackTrace();
			}
			
		}
		 public void loadMEDObjects(String textFileName) throws InvalidFormatException, IOException{

			 
			 Map<String, String> idcategorymap=new HashMap<String, String>();
				 
					Map<String, String> wordmap=new HashMap<String, String>();
					Map<String, String> liniarwordmap=new HashMap<String, String>();
					
					Map<String, String> localweakSynonimMap=new HashMap<String, String>();
					
					Map<String, String> localweakContextMap=new HashMap<String, String>();
					
					 try {
						    
						 
						   File file=new File(textFileName);
	                       
	                               FileInputStream inStream=new FileInputStream(file);
	                               InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

	                          
						 
						     JSONParser parser = new JSONParser();
//Linux
						     Object obj = parser.parse(new BufferedReader(reader));
						    		
						    //Windows		
				            //Object obj = parser.parse(new FileReader(textFileName));
				 
				            JSONObject jsonObject = (JSONObject) obj;
				 
				           
				            JSONArray companyList = (JSONArray) jsonObject.get("Company List");
				 
				            Iterator<JSONObject> iterator = companyList.iterator();
				            int k=1;
				            while (iterator.hasNext()) {
				            	//k++;if(k==50)break;
			            	    JSONObject json = iterator.next();
			            	    String ids = (String) ""+json.get("id");
					            String names = (String) json.get("name");
					            String categoryid = (String) ""+json.get("categoryid");
					            String typeid = (String) ""+json.get("typeid");
					            
					            String synonyms = (String) json.get("synonyms");
					            
					            String wsynonyms = (String) json.get("wsynonyms");
					            String weaksearchcontext = (String) json.get("weaksearchcontext");
					            
					            //System.out.println(names+"\n");
					            
					            
					            if(names!=null&&!names.equals("")){
					            if(names.length()>3){wordmap.put(names, ids);
			            	    String lowerStr = names.toLowerCase();
			            	    liniarwordmap.put(clean(lowerStr), ids);
			            	    
			            	    if(idcategorymap.containsKey(ids)){
			            	    	String val=idcategorymap.get(ids);
			            	    	
			            	    	String nval=val+"|"+typeid+"/"+categoryid;
			            	    	
			            	    	idcategorymap.put(ids, nval);
			            	    }
			            	    else
			            	    idcategorymap.put(ids, typeid+"/"+categoryid);
					            }
				                }

					            if(weaksearchcontext!=null){
					            	if(weaksearchcontext.contains("|")){
					            	String[] myData = weaksearchcontext.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    //wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    lowerStrd=clean(lowerStrd);
					            	    if(lowerStrd.length()>3){
					            	    if (localweakContextMap.containsKey(lowerStrd))
					            	    {
					            	    String ifd = localweakContextMap.get(lowerStrd);
					            	    String allid=ifd+"|"+ids;
					            	    localweakContextMap.put(lowerStrd, allid);
					            	    }
					            	    else
					            	    localweakContextMap.put(lowerStrd, ids);
					            	}
					            	    
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(weaksearchcontext!=null&&!weaksearchcontext.equals("")){
								            //wordmap.put(weaksearchcontext, ids);
						            	    String lowerStrx = weaksearchcontext.toLowerCase();
						            	    lowerStrx=clean(lowerStrx);
						            	    if(lowerStrx.length()>3){
						            	    if (localweakContextMap.containsKey(lowerStrx)){
						            	    	    String ifd = localweakContextMap.get(lowerStrx);
						            	    	    String allid=ifd+"|"+ids;
								            	    localweakContextMap.put(lowerStrx, allid);
						            	    }
						            	    else
						            	    localweakContextMap.put(lowerStrx, ids);
					            		    }
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            if(wsynonyms!=null){
					            	if(wsynonyms.contains("|")){
					            	String[] myData = wsynonyms.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    //wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    
					            	    lowerStrd=clean(lowerStrd);
					            	    
					            	    if(lowerStrd.length()>3){
					            	    if (localweakSynonimMap.containsKey(lowerStrd)){
					            	    	
					            	    	    String ifd = localweakSynonimMap.get(lowerStrd);
					            	    	    String allid=ifd+"|"+ids;
					            	    	    localweakSynonimMap.put(lowerStrd, allid);
					            	    }
					            	    else
					            	    localweakSynonimMap.put(lowerStrd, ids);}
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(wsynonyms!=null&&!wsynonyms.equals("")){
								            //wordmap.put(wsynonyms, ids);
						            	    String lowerStrx = wsynonyms.toLowerCase();
						            	    lowerStrx=clean(lowerStrx);
						            	    if(lowerStrx.length()>3){
						            	    if (localweakSynonimMap.containsKey(lowerStrx)){
						            	    	
					            	    	    String ifd = localweakSynonimMap.get(lowerStrx);
					            	    	    String allid=ifd+"|"+ids;
					            	    	    localweakSynonimMap.put(lowerStrx, allid);
						            	    }
					            	        else
						            	    localweakSynonimMap.put(lowerStrx, ids);	
					            		    }
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            
					            
					            if(synonyms!=null){
					            	
					            if(synonyms.contains("|")){
					            	String[] myData = synonyms.split("\\|");
					            	for (String syns: myData) {
					            		
					            		if(syns.length()>3){
					            		syns=clean(syns);
					            	    wordmap.put(syns, ids);
					            	    String lowerStre = syns.toLowerCase();
					            	    liniarwordmap.put(lowerStre, ids);}
					            	}
					            	
					            	
					            }else{ 
					            	    if(synonyms!=null&&!synonyms.equals("")){
					            	    synonyms=clean(synonyms);	
							            if(synonyms.length()>3)
					            	    {wordmap.put(synonyms, ids);
					            	    String lowerStrz = synonyms.toLowerCase();
					            	    liniarwordmap.put(lowerStrz, ids);}	
						                }
					            }
					            
					            }

				            }
				 
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
					
					 
					 
					 
					 idcategorymapobjmed=idcategorymap;
					 swordmapmed=wordmap;
					 liniarmapmed=liniarwordmap;
					 weakSynonimMapmed=localweakSynonimMap;
					 weakContextMapmed=localweakContextMap;
			 
		 }
		
		 public void loadObjects(String textFileName) throws InvalidFormatException, IOException{

			 
			 Map<String, String> idcategorymap=new HashMap<String, String>();
				 
					Map<String, String> wordmap=new HashMap<String, String>();
					Map<String, String> liniarwordmap=new HashMap<String, String>();
					
					Map<String, String> localweakSynonimMap=new HashMap<String, String>();
					
					Map<String, String> localweakContextMap=new HashMap<String, String>();
					
					 try {
						    
						 
						   File file=new File(textFileName);
	                       
	                               FileInputStream inStream=new FileInputStream(file);
	                               InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

	                          
						 
						     JSONParser parser = new JSONParser();
//Linux
						     Object obj = parser.parse(new BufferedReader(reader));
						    		
						    //Windows		
				            //Object obj = parser.parse(new FileReader(textFileName));
				 
				            JSONObject jsonObject = (JSONObject) obj;
				 
				           
				            JSONArray companyList = (JSONArray) jsonObject.get("Company List");
				 
				            Iterator<JSONObject> iterator = companyList.iterator();
				            int k=1;
				            while (iterator.hasNext()) {
				            	//k++;if(k==50)break;
			            	    JSONObject json = iterator.next();
			            	    String ids = (String) ""+json.get("id");
					            String names = (String) json.get("name");
					            String categoryid = (String) ""+json.get("categoryid");
					            String typeid = (String) ""+json.get("typeid");
					            
					            String synonyms = (String) json.get("synonyms");
					            
					            String wsynonyms = (String) json.get("wsynonyms");
					            String weaksearchcontext = (String) json.get("weaksearchcontext");
					            
					            //System.out.println(names+"\n");
					            
					            
					            if(names!=null&&!names.equals("")){
					            wordmap.put(names, ids);
			            	    String lowerStr = names.toLowerCase();
			            	    liniarwordmap.put(clean(lowerStr), ids);
			            	    
			            	    if(idcategorymap.containsKey(ids)){
			            	    	String val=idcategorymap.get(ids);
			            	    	
			            	    	String nval=val+"|"+typeid+"/"+categoryid;
			            	    	idcategorymap.put(ids, nval);
			            	    }
			            	    else
			            	    idcategorymap.put(ids, typeid+"/"+categoryid);
			            	    
				                }
					            /* Загрузка слабых синонимов в общий контектс
					            if(wsynonyms!=null){
					            	if(wsynonyms.contains("|")){
					            	String[] myData = wsynonyms.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    liniarwordmap.put(lowerStrd, ids);
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(wsynonyms!=null&&!wsynonyms.equals("")){
								            wordmap.put(wsynonyms, ids);
						            	    String lowerStrx = wsynonyms.toLowerCase();
						            	    liniarwordmap.put(lowerStrx, ids);	
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            */
					            if(weaksearchcontext!=null){
					            	if(weaksearchcontext.contains("|")){
					            	String[] myData = weaksearchcontext.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    //wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    lowerStrd=clean(lowerStrd);
					            	    if (localweakContextMap.containsKey(lowerStrd))
					            	    {
					            	    String ifd = localweakContextMap.get(lowerStrd);
					            	    String allid=ifd+"|"+ids;
					            	    localweakContextMap.put(lowerStrd, allid);
					            	    }
					            	    else
					            	    localweakContextMap.put(lowerStrd, ids);
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(weaksearchcontext!=null&&!weaksearchcontext.equals("")){
								            //wordmap.put(weaksearchcontext, ids);
						            	    String lowerStrx = weaksearchcontext.toLowerCase();
						            	    lowerStrx=clean(lowerStrx);
						            	    if (localweakContextMap.containsKey(lowerStrx)){
						            	    	    String ifd = localweakContextMap.get(lowerStrx);
						            	    	    String allid=ifd+"|"+ids;
								            	    localweakContextMap.put(lowerStrx, allid);
						            	    }
						            	    else
						            	    localweakContextMap.put(lowerStrx, ids);
						            	    
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            if(wsynonyms!=null){
					            	if(wsynonyms.contains("|")){
					            	String[] myData = wsynonyms.split("\\|");
					            	for (String syns: myData) {
					            	   
					            	    //wordmap.put(syns, ids);
					            	    String lowerStrd = syns.toLowerCase();
					            	    lowerStrd=clean(lowerStrd);
					            	    if (localweakSynonimMap.containsKey(lowerStrd)){
					            	    	
					            	    	    String ifd = localweakSynonimMap.get(lowerStrd);
					            	    	    String allid=ifd+"|"+ids;
					            	    	    localweakSynonimMap.put(lowerStrd, allid);
					            	    }
					            	    else
					            	    localweakSynonimMap.put(lowerStrd, ids);
					            	}
					            	
					            	
					            	}else{
					            		
					            		    if(wsynonyms!=null&&!wsynonyms.equals("")){
								            //wordmap.put(wsynonyms, ids);
						            	    String lowerStrx = wsynonyms.toLowerCase();
						            	    lowerStrx=clean(lowerStrx);
						            	    if (localweakSynonimMap.containsKey(lowerStrx)){
						            	    	
					            	    	    String ifd = localweakSynonimMap.get(lowerStrx);
					            	    	    String allid=ifd+"|"+ids;
					            	    	    localweakSynonimMap.put(lowerStrx, allid);
						            	    }
					            	    else
						            	    localweakSynonimMap.put(lowerStrx, ids);	
							                }
					            		
					            	}
					            	
					            	
					            	
					            }
					            
					            
					            if(synonyms!=null){
					            	
					            if(synonyms.contains("|")){
					            	String[] myData = synonyms.split("\\|");
					            	for (String syns: myData) {
					            		syns=clean(syns);
					            	    wordmap.put(syns, ids);
					            	    String lowerStre = syns.toLowerCase();
					            	    liniarwordmap.put(lowerStre, ids);
					            	}
					            	
					            	
					            }else{ 
					            	    if(synonyms!=null&&!synonyms.equals("")){
					            	    	synonyms=clean(synonyms);	
							            wordmap.put(synonyms, ids);
					            	    String lowerStrz = synonyms.toLowerCase();
					            	    liniarwordmap.put(lowerStrz, ids);	
						                }
					            }
					            
					            }

				            }
				 
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
					
					 
//					 for (Map.Entry<String, String> entry : localweakContextMap.entrySet())
//					 {
//					     //System.out.println(entry.getKey() + "/" + entry.getValue());
//					     
//					     String  value = entry.getValue();
//					     if("кузнецов".equals(value))
//					    		 {System.out.println(entry.getKey() + "/" + entry.getValue());}
//					     if(value.contains("|")){
//					    	 if("кузнецов".equals(entry.getValue()))
//					    	 System.out.println(entry.getKey() + "/" + entry.getValue());
//					    	 
//					     }
//					 }
					 
					 
					 idcategorymapobj=idcategorymap;
					 swordmap=wordmap;
					 liniarmap=liniarwordmap;
					 weakSynonimMap=localweakSynonimMap;
					 weakContextMap=localweakContextMap;
			 
		 }
			public void readPoliteh(String args) {
				
				
				Map<String, String> politehwords=new HashMap<String, String>();
				BufferedReader br = null;

				try {

					String sCurrentLine;

					br = new BufferedReader(new FileReader(args));
		            int k=1;
					while ((sCurrentLine = br.readLine()) != null) {
						
						try{String kv=clean(sCurrentLine.toLowerCase().trim()); if(!kv.equals("")) politehwords.put(kv, kv);} catch (Exception e) {}
					    k++;
					}
					

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				fastmodel.setPolitehwords(politehwords);
			}
			
			public void readEconomics(String args) {
				
				
				Map<String, String> politehwords=new HashMap<String, String>();
				BufferedReader br = null;

				try {

					String sCurrentLine;

					br = new BufferedReader(new FileReader(args));
		            int k=1;
					while ((sCurrentLine = br.readLine()) != null) {
						
						try{String kv=clean(sCurrentLine.toLowerCase().trim()); if(!kv.equals("")) politehwords.put(kv, kv);} catch (Exception e) {}
					    k++;
					}
					

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				fastmodel.setEconomicswords(politehwords);
			}
			
			public void readMat(String args) {

				BufferedReader br = null;

				try {

					String sCurrentLine;

					br = new BufferedReader(new FileReader(args));
		            int k=1;
					while ((sCurrentLine = br.readLine()) != null) {
						//System.out.println(sCurrentLine);
						matwords.put(sCurrentLine, ""+k);
					    k++;
					}
					

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}

			}
			public void readKvantor(String args) {

				BufferedReader br = null;
				Map<String, String> matwordsc=new HashMap<String, String>();
				try {

					String sCurrentLine;

					br = new BufferedReader(new FileReader(args));
		            int k=1;
					while ((sCurrentLine = br.readLine()) != null) {
						
						if(sCurrentLine!=null){
							if(sCurrentLine.contains("#")){
						String[] newwords = sCurrentLine.split("#");
				  		
				  		
				  		String word=getNormalFormForWord(newwords[0]);
				  		String type=getNormalFormForWord(newwords[1]);
				  		
				  		
				  		System.out.println(word + "/" + type);
				  		
						matwordsc.put(clean(word), type.trim());
						try{matwordsc.put(getNormalFormForWord(clean(word)), type.trim());} catch (Exception e) {}
							}
					    k++;
					}
					}

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				fastmodel.setWordKvantor(matwordsc);
			}
			
//		 public String clean(String text){
//			 
//			 String dirty=text;
//		    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
//				return dirty;
//		 }
			 public  String clean(String text){
				 
				 String dirty=text; 
			    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
			    	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
			    	try{dirty=dirty.replaceAll("[\\s]{2,}", " ");}catch(Exception e){	e.printStackTrace();}
			    	
					  
					return dirty;
			 }
		 public LinkedHashMap<String, Integer> sortHashMapByValues(
			        Map<String, Integer> passedMap) {
			    List<String> mapKeys = new ArrayList<>(passedMap.keySet());
			    List<Integer> mapValues = new ArrayList<>(passedMap.values());
			    Collections.sort(mapValues);
			    Collections.sort(mapKeys);

			    LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();

			    Iterator<Integer> valueIt = mapValues.iterator();
			    while (valueIt.hasNext()) {
			    	Integer val = valueIt.next();
			        Iterator<String> keyIt = mapKeys.iterator();

			        while (keyIt.hasNext()) {
			        	String key = keyIt.next();
			            Integer comp1 = passedMap.get(key);
			            Integer comp2 = val;

			            if (comp1.equals(comp2)) {
			                keyIt.remove();
			                sortedMap.put(key, val);
			                break;
			            }
			        }
			    }
			    return sortedMap;
			}
		public void loadSymantexB(){
			
			 Connection conn = null;
			    Statement stmt = null;
			    try{
			      
			       Class.forName("com.mysql.jdbc.Driver");

			       final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
				    final  String DB_URL = "jdbc:mysql://172.17.53.12/ruwiki";

				   
				    final  String USER = "admin";
				    final  String PASS = "kjwQ5%6JYgmu";
				    
			       System.out.println("Connecting to a selected database...");
			       conn = DriverManager.getConnection(DB_URL, USER, PASS);
			       System.out.println("Connected database successfully...");
			       
			      
			       System.out.println("Creating statement...");
			       stmt = conn.createStatement();

			       String sql = "SELECT lang_pos.id AS id, page_title AS entry1, wiki_text.text AS entry2, relation_type.name AS relation_name FROM lang_pos JOIN page ON lang_pos.page_id = page.id JOIN meaning ON meaning.lang_pos_id = lang_pos.id JOIN relation ON relation.meaning_id = meaning.id JOIN wiki_text ON wiki_text.id = relation.wiki_text_id JOIN relation_type ON relation_type.id = relation.relation_type_id WHERE lang_id = 804";
			       ResultSet rs = stmt.executeQuery(sql);
			     
			       while(rs.next()){
			    	 
			    	  String id  = rs.getString("entry1");
			          String ttext = rs.getString("entry2");
			          String tname = rs.getString("relation_name");
			          
			          String[] index = new String[2];
						index[0]=ttext;
						index[1]=tname;
			          
			         
			          addValues(id, index);
			          
			       
			          
			        
			       }
			       rs.close();
			    }catch(SQLException se){
			       //Handle errors for JDBC
			       se.printStackTrace();
			    }catch(Exception e){
			       //Handle errors for Class.forName
			       e.printStackTrace();
			    }finally{
			       //finally block used to close resources
			       try{
			          if(stmt!=null)
			             conn.close();
			       }catch(SQLException se){
			       }// do nothing
			       try{
			          if(conn!=null)
			             conn.close();
			       }catch(SQLException se){
			          se.printStackTrace();
			       }//end finally try
			    }//end try
		}
		 public void loadSymantex(String textFileName) throws InvalidFormatException, IOException, ParseException{
			   File file=new File(textFileName);
	         
	         FileInputStream inStream=new FileInputStream(file);
	         InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

	    

	         JSONParser parser = new JSONParser();
				//Linux
			 Object obj = parser.parse(new BufferedReader(reader));
				  		
				  //Windows		
			    //Object obj = parser.parse(new FileReader(textFileName));
			
			    JSONObject jsonObject = (JSONObject) obj;
			
			   
			    JSONArray companyList = (JSONArray) jsonObject.get("Company List");
			
			    Iterator<JSONObject> iterator = companyList.iterator();
			    int g=0;
				    while (iterator.hasNext()) {
				    	g++;
					    JSONObject json = iterator.next();
					    String ids = (String) ""+json.get("id");
				        String entry1 = (String) json.get("entry1");
				        String entry2 = (String) ""+json.get("entry2");
				        String relation_name = (String) ""+json.get("relation_name");
				          String[] index = new String[2];
							index[0]=entry2;
							index[1]=relation_name;
							if(g==100||g==200||g==300)System.out.println (entry1+"--------------"+relation_name+"--------------"+entry2);
							symantexmapword.put(entry1, entry2); 
							symantexmapcategory.put(entry2, entry2);
				          addValues(entry1, index);
				    }
		 }
		 public void addValues(String key, String[] index) {
			   ArrayList tempList = null;
			   if (symantexhashMap.containsKey(key)) {
			      tempList = symantexhashMap.get(key);
			      if(tempList == null)
			         tempList = new ArrayList();
			      tempList.add(index);  
			   } else {
			      tempList = new ArrayList();
			      tempList.add(index);               
			   }
			   symantexhashMap.put(key,tempList);
			}
			public void  loadExclude(String filea) throws InvalidFormatException, IOException, ParseException{
				Map<String, String> wordmap=new HashMap<String, String>();
				// Open the file
				FileInputStream fstream = new FileInputStream(filea);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

				String strLine;

				//Read File Line By Line
				while ((strLine = br.readLine()) != null)   {

		        try{ wordmap.put(clean(strLine.toLowerCase().trim()), strLine);   System.out.println(strLine+" exclude \n");}catch(Exception e){	e.printStackTrace();}
				}
				br.close();
				
				fastmodel.setWordExclude(wordmap);
		      
			}
			
			
			public void  loadPredlogMesta() throws InvalidFormatException, IOException, ParseException{
				Map<String, String> wordmap=new HashMap<String, String>();
				wordmap.put("в", "в");
				wordmap.put("на", "на");
				wordmap.put("у", "у");
				wordmap.put("за", "за");
				wordmap.put("перед", "перед");
				wordmap.put("позади", "позади");
				wordmap.put("под", "под");
				wordmap.put("напротив", "напротив");
				wordmap.put("между", "между");
				wordmap.put("недалеко", "недалеко");
				wordmap.put("далеко", "далеко");
				wordmap.put("близко", "близко");
				wordmap.put("около", "около");
				wordmap.put("выше", "выше");
				wordmap.put("ниже", "ниже");
				wordmap.put("право", "право");
				wordmap.put("лево", "лево");
				wordmap.put("направо", "направо");
				wordmap.put("налево", "налево");
				fastmodel.setWordPredlogMesta(wordmap);


				
wordmap=new HashMap<String, String>();
wordmap.put("вчера", "вчера");
wordmap.put("сегодня", "сегодня");
wordmap.put("завтра", "завтра");
wordmap.put("абие","абие");
wordmap.put("анадысь", "анадысь");
wordmap.put("ввек", "ввек");
wordmap.put("век", "век");
wordmap.put("вечером", "вечером");
wordmap.put("вечер", "вечер");
wordmap.put("вечор", "вечор");
wordmap.put("вовремя", "вовремя");
wordmap.put("впредь", "впредь");
wordmap.put("встарь", "встарь");
wordmap.put("вчера", "вчера");
wordmap.put("вчерась", "вчерась");
wordmap.put("давеча", "давеча");
wordmap.put("давно", "давно");
wordmap.put("днём", "днём");
wordmap.put("днесь", "днесь");
wordmap.put("днями", "днями");
wordmap.put("доднесь", "доднесь");
wordmap.put("доныне", "доныне");
wordmap.put("допрежь", "допрежь");
wordmap.put("досветла", "досветла");
wordmap.put("дотемна", "дотемна");
wordmap.put("древле", "древле");
wordmap.put("ежедневно", "ежедневно");
wordmap.put("ежеквартально", "ежеквартально");
wordmap.put("ежемесячно", "ежемесячно");
wordmap.put("еженощно", "еженощно");
wordmap.put("задолго", "задолго");
wordmap.put("заранее", "заранее");
wordmap.put("засветло", "засветло");
wordmap.put("засим", "засим");
wordmap.put("затем", "затем");
wordmap.put("затемно", "затемно");
wordmap.put("зимой", "зимой");
wordmap.put("издревле", "издревле");
wordmap.put("иногда", "иногда");
wordmap.put("испокон", "испокон");
wordmap.put("каждодневно", "каждодневно");
wordmap.put("летом", "летом");
wordmap.put("надысь", "надысь");
wordmap.put("накануне", "накануне");
wordmap.put("намедни", "намедни");
wordmap.put("наутро", "наутро");
wordmap.put("невовремя", "невовремя");
wordmap.put("недавно", "недавно");
wordmap.put("незадолго", "незадолго");
wordmap.put("нонеча", "нонеча");
wordmap.put("нонче", "нонче");
wordmap.put("ночером", "ночером");
wordmap.put("ночью", "ночью");
wordmap.put("ныне", "ныне");
wordmap.put("нынче", "нынче");
wordmap.put("однажды", "однажды");
wordmap.put("отныне", "отныне");
wordmap.put("поднесь", "поднесь");
wordmap.put("подчас", "подчас");
wordmap.put("позднее", "позднее");
wordmap.put("поздно", "поздно");
wordmap.put("позже", "позже");
wordmap.put("пока", "пока");
wordmap.put("покамест", "покамест");
wordmap.put("покуда", "покуда");
wordmap.put("поныне", "поныне");
wordmap.put("пополудни", "пополудни");
wordmap.put("пополуночи", "пополуночи");
wordmap.put("порой", "порой");
wordmap.put("посейчас", "посейчас");
wordmap.put("после", "после");
wordmap.put("послезавтра", "послезавтра");
wordmap.put("прежде", "прежде");
wordmap.put("присно", "прежде");
wordmap.put("ранее","ранее");
wordmap.put("рано", "рано");
wordmap.put("раньше", "раньше");
wordmap.put("сегодня", "сегодня");
wordmap.put("сейгод", "сейгод");
wordmap.put("сейчас", "сейчас");
wordmap.put("скоро","скоро");
wordmap.put("смолоду","смолоду");
wordmap.put("спокон","спокон");
wordmap.put("стемна","стемна");
wordmap.put("сыздетства","сыздетства");
wordmap.put("сызмала","сызмала");
wordmap.put("сызмалу","сызмалу");
wordmap.put("сызмальства","сызмальства");
wordmap.put("теперича","теперича");
wordmap.put("теперь","теперь");
wordmap.put("утром","утром");
wordmap.put("утра","утра");
wordmap.put("загодя","загодя");
wordmap.put("некогда","некогда");
wordmap.put("сначала","сначала");
wordmap.put("сперва","сперва");
wordmap.put("спервоначалу","спервоначалу");
wordmap.put("спервоначала","спервоначала");
wordmap.put("поначалу","поначалу");
wordmap.put("встарину","встарину");
wordmap.put("дотоль","дотоль");
wordmap.put("дотоле","дотоле");
wordmap.put("благовременно", "благовременно");
wordmap.put("пораньше","пораньше");
wordmap.put("давненько","давненько");
wordmap.put("давнехонько","давнехонько");
wordmap.put("давнешенько","давнешенько");
wordmap.put("издавна","издавна");
wordmap.put("исстари","исстари");
wordmap.put("денно","денно");
wordmap.put("поденно","поденно");
wordmap.put("изредка","изредка");
wordmap.put("временами","временами");
wordmap.put("порою","порою");
wordmap.put("заполночь", "заполночь");
wordmap.put("тотчас","тотчас");
wordmap.put("ночное","ночное");
wordmap.put("ночной","ночной");  
wordmap.put("ночные","ночные"); 
wordmap.put("дневные","дневные"); 
wordmap.put("дневной","дневной"); 
wordmap.put("дневный","дневный"); 
 

/*
сию секунду
сию минуту
сей же час
в настоящий момент
в настоящее время
в данный момент
в данное время
только что
как то раз
на днях
время от времени
иной раз
другой раз
кое когда
кой когда
от времени до времени
давным давно
вчерашний день
в первую очередь
раным рано
когда угодно
когда либо
день деньской
*/
				fastmodel.setNarechiyaVremeni(wordmap);
				
				
				wordmap=new HashMap<String, String>();
				wordmap.put("понедельник", "понедельник");
				wordmap.put("вторник", "вторник");
				wordmap.put("среда", "среда");
				wordmap.put("четверг", "четверг");
				wordmap.put("пятница", "пятница");
				wordmap.put("суббота", "суббота");
				wordmap.put("воскресенье", "воскресенье");
				wordmap.put("январь", "январь");
				wordmap.put("февраль", "февраль");	
				wordmap.put("март", "март");	
				wordmap.put("апрель", "апрель");
				wordmap.put("май", "май");	
				wordmap.put("июнь", "июнь");
				wordmap.put("июль", "июль");
				wordmap.put("август", "август");
				wordmap.put("сентябрь", "сентябрь");
				wordmap.put("октябрь", "октябрь");		
				wordmap.put("ноябрь", "ноябрь");	
				wordmap.put("декабрь", "декабрь");
				fastmodel.setDniMeseats(wordmap);
				
				
				wordmap=new HashMap<String, String>();
				
wordmap.put("миллиметр", "dlina");
				
				
				wordmap.put("сантиметр", "dlina");
				
				
				wordmap.put("дециметр", "dlina");
				
				
				wordmap.put("метр", "dlina");
				//
				
				wordmap.put("километр", "dlina");
				
				
				wordmap.put("мегаметр", "dlina");
				wordmap.put("мегаметр", "dlina");
				
				wordmap.put("дюйм", "dlina");
				wordmap.put("дюим", "dlina");
				wordmap.put("фут", "dlina");
				wordmap.put("ярд", "dlina");
				wordmap.put("миля", "dlina");
				wordmap.put("гектар", "ploshadi");
				
				
				wordmap.put("ар", "ploshadi");
				wordmap.put("миллилитр", "obyem");
				wordmap.put("мл", "obyem");
				
				wordmap.put("литр", "obyem");
				//wordmap.put("л", "obyem");
				
				wordmap.put("децилитр", "obyem");
				
				
				wordmap.put("мегалитр", "obyem");
				wordmap.put("гектолитр", "obyem");
				
				
				wordmap.put("децилитр", "obyem");
				
				
				wordmap.put("гектограмм", "massa");
				
				wordmap.put("миллиграмм", "massa");
				
				
				wordmap.put("грамм", "massa");
				//wordmap.put("г", "massa");
				wordmap.put("килограмм", "massa");
				
				
				wordmap.put("тонна", "massa");
				wordmap.put("тонн", "massa");
				wordmap.put("килотонна", "massa");
				wordmap.put("сантиграмм", "massa");
				
				
				
				wordmap.put("грузоподъемность", "massa");
				
				wordmap.put("центнер", "massa");
				//
				
				wordmap.put("гектокилограмм", "massa");
				wordmap.put("децитонна", "massa");
				wordmap.put("ватт", "energy");
				
				
				
				wordmap.put("киловатт", "energy");
				
				
				
				wordmap.put("мегаватт", "energy");
				wordmap.put("вольт", "energy");
				
				wordmap.put("киловольт", "energy");
				
				
				wordmap.put("киловар", "energy");
				wordmap.put("квар", "energy");
				
				
				wordmap.put("ампер", "energy");

				wordmap.put("кулон", "energy");
				
				wordmap.put("джоуль", "energy");
				
				
				wordmap.put("килоджоуль", "energy");
				
				
				wordmap.put("ом", "energy");
				wordmap.put("градус", "temperatura");
				wordmap.put("кандела", "tehno");
				
				
				wordmap.put("люмен", "tehno");
				
				
				
				wordmap.put("кельвин", "tehno");
				wordmap.put("ньютон", "tehno");
				wordmap.put("герц", "tehno");
				
				
				wordmap.put("килогерц", "tehno");
				
				
				wordmap.put("мегагерц", "tehno");
				
				
				wordmap.put("паскаль", "tehno");
				
				
				wordmap.put("сименс", "tehno");
				wordmap.put("килопаскаль", "tehno");
				
				
				wordmap.put("мегапаскаль", "tehno");
				
				
				wordmap.put("гигабеккерель", "tehno");
				
				
				
				wordmap.put("милликюри", "tehno");
				
				
				wordmap.put("кюри", "tehno");
				
				
				
				wordmap.put("миллибар", "tehno");
				wordmap.put("гектобар", "tehno");
				wordmap.put("килобар", "tehno");
				wordmap.put("фарад", "tehno");
				wordmap.put("беккерель", "tehno");
				wordmap.put("вебер", "tehno");
				wordmap.put("килокалория", "tehno");
				
				
				wordmap.put("гигакалория", "tehno");
				
				
				wordmap.put("узел", "speed");
				wordmap.put("секунда", "time");
				wordmap.put("минута", "time");
				wordmap.put("мин", "time");
				
				wordmap.put("час", "time");
				
				wordmap.put("сутки", "time");
				
				
				wordmap.put("неделя", "time");
				
				
				
				wordmap.put("декада", "time");
				
				wordmap.put("месяц", "time");
				
				
				wordmap.put("квартал", "time");
				
				
				wordmap.put("полугодие", "time");
				wordmap.put("полгода", "time");
				
				wordmap.put("год", "time");
				wordmap.put("лет", "time");
				wordmap.put("север", "location");
						wordmap.put("юг", "location");
								wordmap.put("запад", "location"); 
										wordmap.put("восток", "location");
										//улица|проезд|проспект|тупик|шоссе|город|переулок|бульвар
										wordmap.put("улица", "location");
										wordmap.put("проезд", "location");
										wordmap.put("проспект", "location");
										wordmap.put("тупик", "location");
										wordmap.put("шоссе", "location");
										wordmap.put("город", "location");
										wordmap.put("переулок", "location");
										wordmap.put("бульвар", "location");
										
										
				wordmap.put("десятилетие", "time");
				
				
				wordmap.put("микросекунда", "time");
				wordmap.put("миллисекунда", "time");
				wordmap.put("дюжина", "economics");
				wordmap.put("изделие", "economics");
				
				wordmap.put("посылка", "economics");
				wordmap.put("рулон", "economics");
				wordmap.put("штук", "economics");
				
				wordmap.put("упаковка", "economics");
				
				wordmap.put("карат", "economics");
				
				
				wordmap.put("топливо", "economics");
				wordmap.put("рубль", "economics");
				
				
				wordmap.put("тысяча", "economics");
				
				wordmap.put("миллион", "economics"); 
				
				wordmap.put("миллиард", "economics");  
				
				
				
				wordmap.put("пассажиропоток", "economics");
				wordmap.put("доллар", "economics");
				wordmap.put("евро", "economics");
				wordmap.put("автотонна", "economics");
				wordmap.put("птицемест", "economics");
				wordmap.put("процент", "economics");
				wordmap.put("домохозяйство", "economics");
				Map<String, String>	wordmapOne=new HashMap<String, String>();
				for (Map.Entry<String, String> entry : wordmap.entrySet())
				{

				    String word=getNormalFormForWord(entry.getKey());
				    
				    wordmapOne.put(word, entry.getValue());
				}

				
				
				fastmodel.setMetricsTypeOne(wordmapOne);
				
				wordmap=new HashMap<String, String>();
				
				wordmap.put("десять пар", "economics");
				wordmap.put("ампер час", "energy");
				wordmap.put("пассажирских мест", "economics");
				wordmap.put("квадратный миллиметр", "ploshadi");
				wordmap.put("квадратныи миллиметр", "ploshadi");
				wordmap.put("квадратный сантиметр", "ploshadi");
				wordmap.put("квадратныи сантиметр", "ploshadi");
				wordmap.put("квадратный дециметр", "ploshadi");
				wordmap.put("квадратныи дециметр", "ploshadi");
				wordmap.put("квадратный метр", "ploshadi");
				wordmap.put("квадратныи метр", "ploshadi");
				wordmap.put("квадратный километр", "ploshadi");
				wordmap.put("квадратныи километр", "ploshadi");
				wordmap.put("квадратный дюйм", "ploshadi");
				wordmap.put("квадратныи дюйм", "ploshadi");
				wordmap.put("квадратный дюим", "ploshadi");
				wordmap.put("квадратныи дюим", "ploshadi");
				wordmap.put("квадратный фут", "ploshadi");
				wordmap.put("квадратныи фут", "ploshadi");
				wordmap.put("квадратный ярд", "ploshadi");
				wordmap.put("квадратныи ярд", "ploshadi");
				wordmap.put("условная банка", "economics");
				wordmap.put("тысяча гектаров", "ploshadi");
				wordmap.put("морская миля", "dlina");
				wordmap.put("кубический миллиметр", "obyem");
								wordmap.put("кубическии миллиметр", "obyem");
								wordmap.put("кубический сантиметр", "obyem");
								wordmap.put("кубическии сантиметр", "obyem");
								wordmap.put("кубический дециметр", "obyem");
								wordmap.put("кубическии дециметр", "obyem");
								wordmap.put("кубический метр", "obyem");
								wordmap.put("кубическии метр", "obyem");
								wordmap.put("кубический дюйм", "obyem");
								wordmap.put("кубическии дюйм", "obyem");
								
								wordmap.put("кубический ярд", "obyem");
								wordmap.put("кубическии ярд", "obyem");
								
								wordmap.put("кубический фут", "obyem");
								wordmap.put("кубическии фут", "obyem");
								wordmap.put("метрический карат", "massa");
								wordmap.put("метрическии карат", "massa");
								wordmap.put("метрическая тонна", "massa");
								wordmap.put("вольт ампер", "energy");
								wordmap.put("киловольт ампер", "energy");
								wordmap.put("мегавольт ампер", "energy");
								wordmap.put("ватт час", "energy");
								
								wordmap.put("киловатт час", "energy");
								wordmap.put("мегаватт час", "energy");
								wordmap.put("градус цельсия", "temperatura");
								wordmap.put("градус фаренгейта", "temperatura");
								wordmap.put("физическая атмосфера", "tehno");
								
								wordmap.put("физическая атмосфера", "tehno");
								wordmap.put("техническая атмосфера", "tehno");
								wordmap.put("лошадиная сила", "tehno");
								wordmap.put("человеко час", "economics");
								wordmap.put("человеко день", "economics");
								
								Map<String, String>	wordmapTwo=new HashMap<String, String>();
								for (Map.Entry<String, String> entry : wordmap.entrySet())
								{
									String word=entry.getKey();
									try{String syn=entry.getKey();
									if(syn!=null){
										if(syn.contains(" ")){
								  			
								  		String[] newwords = syn.split(" ");
								  		
								  		
								  		String worda=getNormalFormForWord(newwords[0]);
								  		String wordb=getNormalFormForWord(newwords[1]);
								  		word=worda+" "+wordb;
										}
									}
									}catch(Exception e){}
								   
								    
								    wordmapTwo.put(word, entry.getValue());
								}
								
								
								fastmodel.setMetricsTypeTwo(wordmapTwo);
								
								wordmap=new HashMap<String, String>();
								
								wordmap.put("переработка в сутки", "economics");
								wordmap.put("голов в год", "economics");
								wordmap.put("тонна в час", "economics");
								wordmap.put("тонна в сутки", "economics");
								wordmap.put("тонна в смену", "economics");
								wordmap.put("тонна в сезон", "economics");
								wordmap.put("килограмм на гигакалорию", "economics");
								wordmap.put("килограмм в секунду", "economics");
								wordmap.put("метр в секунду", "speed");
								wordmap.put("оборот в секунду", "speed");
								wordmap.put("оборот в минуту", "speed");
								wordmap.put("километр в час", "speed");
								wordmap.put("метр в минуту", "speed");
								wordmap.put("метр в час", "speed");
								wordmap.put("калория в час", "tehno");
								wordmap.put("килокалория в час", "tehno");
								wordmap.put("гигакалория в час", "tehno");
								wordmap.put("миллион ампер часов", "tehno");
								wordmap.put("тысяча квадратных дециметров", "ploshadi");
								wordmap.put("миллион квадратных дециметров", "ploshadi");
								wordmap.put("миллион квадратных метров", "ploshadi");
								
								
								wordmap.put("условный квадратный метр", "ploshadi");
								wordmap.put("метр общей площади", "ploshadi");
								wordmap.put("метр жилой площади", "ploshadi");
								
								
								Map<String, String>	wordmapThree=new HashMap<String, String>();
								for (Map.Entry<String, String> entry : wordmap.entrySet())
								{
									String word=entry.getKey();
									try{String syn=entry.getKey();
									if(syn!=null){
										if(syn.contains(" ")){
								  			
								  		String[] newwords = syn.split(" ");
								  		
								  		
								  		String worda=getNormalFormForWord(newwords[0]);
								  		String wordb=getNormalFormForWord(newwords[1]);
								  		String wordc=getNormalFormForWord(newwords[2]);
								  		word=worda+" "+wordb+" "+wordc;
										}
									}
									}catch(Exception e){}
								    
								    wordmapThree.put(word, entry.getValue());
								}
								
								fastmodel.setMetricsTypeThree(wordmapThree);
								
								
								
								
								
								wordmap=new HashMap<String, String>();
								wordmap.put("невозможно", "1");
								//невозможно, запрещается, запрещено, возбраняется, воспрещается, воспрещено, нельзя, грешно, противопоказуется, исключено, неприлично, исключается	
								wordmap.put("запрещается", "1");
								wordmap.put("возбраняется", "1");
								wordmap.put("воспрещается", "1");
								wordmap.put("воспрещено", "1");
								wordmap.put("нельзя", "1");
								wordmap.put("грешно", "1");
								wordmap.put("противопоказуется", "1");
								wordmap.put("исключено", "1");
								wordmap.put("неприлично", "1");
								wordmap.put("исключается", "1");
								//wordmap.put("хорошо", "2");
								//wordmap.put("неплохо", "2");
								//wordmap.put("плохо", "3");
								//wordmap.put("нехорошо", "3");
								
								wordmap.put("положено", "4");
								//wordmap.put("следует", "4");
								wordmap.put("велено", "4");
								wordmap.put("дозволяется", "4");
								wordmap.put("позволяется", "4");
								wordmap.put("разрешается", "4");
								wordmap.put("поскольку", "5");
									
								
								fastmodel.setWordExplainOne(wordmap);
								wordmap=new HashMap<String, String>();
								//(потому что) | (потому, что) | (потому как) | (оттого что) | поскольку | (так как)	
								wordmap.put("потому что", "5");
								wordmap.put("потому как", "5");
								wordmap.put("оттого что", "5");
								wordmap.put("так как", "5");	
							    
								////не положено, не следует, не велено, не дозволяется, не позволяется, не разрешается
								wordmap.put("не невозможно", "4");
								wordmap.put("не запрещается", "4");
								wordmap.put("не возбраняется", "4");
								wordmap.put("не воспрещается", "4");
								wordmap.put("не воспрещено", "4");
								wordmap.put("не нельзя", "4");
								wordmap.put("не грешно", "4");
								wordmap.put("не противопоказуется", "4");
								wordmap.put("не исключено", "4");
								wordmap.put("не неприлично", "4");
								wordmap.put("не исключается", "4");
								
								wordmap.put("не положено", "1");
								wordmap.put("не следует", "1");
								wordmap.put("не велено", "1");
								wordmap.put("не дозволяется", "1");
								wordmap.put("не позволяется", "1");
								wordmap.put("не разрешается", "1");
								
								wordmap.put("это хорошо", "2");
								wordmap.put("это неплохо", "2");
								wordmap.put("это плохо", "3");
								wordmap.put("это нехорошо", "3");
								
								
								
								fastmodel.setWordExplainTwo(wordmap);
							    wordmap=new HashMap<String, String>();
							    //(Вследствие того что) | (Благодаря тому что) |(В силу того что)	 
								wordmap.put("вследствие того что", "5");
								wordmap.put("благодаря тому что", "5");
								wordmap.put("силу того что", "5");
							    
							    fastmodel.setWordExplainThree(wordmap);
			}
			
			
			public void loadSinonims(String filea) throws InvalidFormatException, IOException, ParseException{
				Map<String, String> wordmap=new HashMap<String, String>();
				// Open the file
				FileInputStream fstream = new FileInputStream(filea);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

				String strLine;

				//Read File Line By Line
				while ((strLine = br.readLine()) != null)   {
				  // Print the content on the console
					
				 try{
				  
				  String[] d=strLine.split("vals: ");
				  String word=d[0];
				  String syn= d[1];
				  word=word.replaceAll("key: ", "");
				  
				  String b="";
				  b=syn;
//				        if(syn.contains("/")){
//			          	String[] myData = syn.split("\\/");
//			          	
//			          	for (String ww: myData) {
//			          		 System.out.println (clean(ww));
//			          	}
//			          	}else{
//			          		 System.out.println (clean(syn));
//			          	}
				   String a="";
				  if(word.contains("/")){
		          	String[] myData = word.split("\\/");
		          	for (String ww: myData) {
		          		 //System.out.println (clean(ww)); 
		          		 wordmap.put(clean(ww.toLowerCase()).trim(), b);
		          	}
		          	}else{
		          		 wordmap.put(clean(word.toLowerCase()).trim(), b);
		          		 //System.out.println (clean(word));
		          	}  
				  
				  
				}catch(Exception e){	e.printStackTrace();} 
				}

				//Close the input stream
				br.close();
				
				fastmodel.setWordSynonims(wordmap);
		      
			}
			
			public void loadSinonimsv(String fileb) throws InvalidFormatException, IOException, ParseException{
				  
				Map<String, String> wordmap=new HashMap<String, String>();
				  File file=new File(fileb);
		          
		          FileInputStream inStream=new FileInputStream(file);
		          InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

		     
			 
			     JSONParser parser = new JSONParser();
		//Linux
			     Object obj = parser.parse(new BufferedReader(reader));
			    		
			    //Windows		
			     //Object obj = parser.parse(new FileReader(fileb));
				 
		       		        
		         JSONArray companyList = (JSONArray) obj;

		         Iterator<JSONObject> iterator = companyList.iterator();
		         while (iterator.hasNext()) {
		         	try{
		     	    JSONObject json = iterator.next();
		     	        String desc = (String) ""+json.get("desc");
			            String names = (String) json.get("name");
			           // System.out.println(names+" \n" + desc + "\n");
			            wordmap.put(clean(desc.toLowerCase().trim()), names);}catch(Exception e){	e.printStackTrace();}
		      }
		      
		         fastmodel.setWordFraseologism(wordmap);
			}
			
			public void loadMorfema(String filea) throws InvalidFormatException, IOException, ParseException{
				Map<String, String> morfword = new HashMap<String, String>();
				Map<String, String> inlog = new HashMap<String, String>();
				inlog.put("со", "со");
				inlog.put("рас", "рас");
				inlog.put("объ", "объ");
				inlog.put("мал", "мал");
				inlog.put("обо", "обо");
				inlog.put("ис", "ис");
				inlog.put("без", "без");
				inlog.put("анти", "анти");
				inlog.put("бес", "бес");
				inlog.put("не", "не");
				inlog.put("ни", "ни");
				inlog.put("дез", "дез");
				inlog.put("де", "де");
				inlog.put("изо", "изо");
				inlog.put("подо", "подо");
				
				
				
				Map<String, String> predlog = new HashMap<String, String>();
				predlog.put("ак", "ак");
				predlog.put("в", "кто");
				predlog.put("вс", "вс");
				predlog.put("вз", "кто");
				predlog.put("воз", "кто");
				predlog.put("вы", "кто");
				predlog.put("до", "кто");
				predlog.put("за", "кто");
				predlog.put("из", "кто");
				predlog.put("к", "кто");
				predlog.put("на", "кто");
				predlog.put("над", "кто");
				predlog.put("о", "кто");
				predlog.put("об", "кто");
				predlog.put("от", "кто");
				predlog.put("па", "кто");
				predlog.put("пере", "кто");
				predlog.put("по", "кто");
				predlog.put("под", "кто");
				predlog.put("после", "кто");
				predlog.put("пра", "кто");
				predlog.put("пре", "кто");
				predlog.put("пред", "кто");
				predlog.put("при", "кто");
				predlog.put("про", "кто");
				predlog.put("раз", "кто");
				predlog.put("с", "кто");
				predlog.put("сверх", "кто");
				predlog.put("су", "кто");
				predlog.put("тре", "кто");
				predlog.put("у", "кто");
				predlog.put("а", "кто");
				predlog.put("ана", "кто");
				predlog.put("гипер", "кто");
				predlog.put("гипо", "кто");
				predlog.put("мета", "кто");
				predlog.put("нео", "кто");
				predlog.put("орто", "кто");
				predlog.put("пан", "кто");
				predlog.put("пара", "кто");
				predlog.put("прото", "кто");
				predlog.put("ин", "кто");
				predlog.put("квази", "кто");
				predlog.put("пост", "кто");
				predlog.put("суб", "кто");
				predlog.put("супер", "кто");
				predlog.put("транс", "кто");
				predlog.put("ультра", "кто");
				predlog.put("экс", "кто");
				predlog.put("много", "много");
				//анти
				//без
				//бес
				//не
				//ни
				//антисемитский = анти
				//безграмотный = без
				//небезгрешный = не
				
				Map<String, String> wordmap=new HashMap<String, String>();
				
				// Open the file
				FileInputStream fstream = new FileInputStream(filea);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream,"UTF8"));

				String strLine;

				//Read File Line By Line
				while ((strLine = br.readLine()) != null)   {

		        try{ //System.out.println(strLine);
		        
		        
		        if(strLine.contains("|")){
					   String[] myData = strLine.split("\\|");
					   String word =myData[0].toLowerCase().trim();
					   String nword =myData[1].toLowerCase().trim();
					   //System.out.println("--"+word+"--");
				        if(nword.contains("/")){
				        
				        String tokens[] =  nword.split("\\/");
			        	int size = tokens.length;
			        	word=word.replaceAll("-", " "); 
			        	word=word.replaceAll("[^А-я\\s-]", ""); 
			        	
			        	for(int i=0;i<size;i++){
			        	
			        		if(!predlog.containsKey(tokens[0])){
			        			
			        			
			        			
			        			if(i==0){
			        				String s=tokens[0].replaceAll("[^А-я\\s-]", ""); 
		        				
			        				wordmap.put(clean(word), clean(s));
			        				
			        				if(morfword.containsKey(clean(s))){
			        				String val=morfword.get(clean(s));
			        				val=val+"|"+clean(word);
			        				morfword.put(clean(s), val);
			        				}else morfword.put(clean(s), clean(word));

			        				}
			        		
			        		}
			        		if(predlog.containsKey(tokens[0])){
			        			
			        			if(i==0){
			        			try{if(tokens[1]!=null){	
			        			
			        				String s=tokens[1].replaceAll("[^А-я]", ""); 
		        				
			        				wordmap.put(clean(word), clean(s));
			        				
			        				if(morfword.containsKey(clean(s))){
			        				String val=morfword.get(clean(s));
			        				val=val+"|"+clean(word);
			        				morfword.put(clean(s), val);
			        				}else morfword.put(clean(s), clean(word));

			        			}}catch(Exception e){	e.printStackTrace();}
			        		}
			        		}
			        		
			        		if(inlog.containsKey(tokens[0])){ 
			        			try{	if(tokens[1]!=null){
			        				String s=tokens[0].replaceAll("[^А-я\\s-]", ""); 
			        				String sa=tokens[1].replaceAll("[^А-я\\s-]", ""); 
			        				//wordmap.put(clean(word), clean(s+sa)+"|"+ clean(sa)); 
			        				wordmap.put(clean(word), clean(s+sa)); 
			        				if(morfword.containsKey(clean(s+sa))){
				        				String val=morfword.get(clean(s+sa));
				        				val=val+"|"+clean(word);
				        				morfword.put(clean(s+sa), val);
				        				}else morfword.put(clean(s+sa), clean(word));
			        				
			        				if(morfword.containsKey(clean(sa))){
				        				String val=morfword.get(clean(sa));
				        				val=val+"|"+clean(word);
				        				morfword.put(clean(sa), val);
				        				}else morfword.put(clean(sa), clean(word));
			        				
			        			} }catch(Exception e){	e.printStackTrace();}
			        		}
			        	}
			        	
			        	
			        	
			        	
			          	
			          	}else{
			          		 String s=nword.replaceAll("[^А-я\\s-]", ""); 
			          		 wordmap.put(clean(word), clean(s));
			          		if(morfword.containsKey(clean(s))){
		        				String val=morfword.get(clean(s));
		        				val=val+"|"+clean(word);
		        				morfword.put(clean(s), val);
		        				}else morfword.put(clean(s), clean(word));
			          	
			          	}
					   
					   
				}
		        
		        
		        }catch(Exception e){	e.printStackTrace();}
				}
				br.close();
				
				fastmodel.setMorfema(wordmap);
				
				fastmodel.setMorfemaWords(morfword);
		     
			}
			
			private String getNormalFormForWord(String word){
				
				
				String wordform=word;
				
				
				try{
				
				
				List<String> wordBaseForms = luceneMorph.getMorphInfo(word);
				StringBuilder sb = new StringBuilder();
				for (String s : wordBaseForms)
				{
				    sb.append(s);
				    sb.append(",");
				}
				
				
				String ist = sb.toString();
				if(!ist.equals("")&&ist!=null){
					wordform=ist;
				}
				

				if(wordform.contains("|")){
					   String[] myData = wordform.split("\\|");
					   String nword =myData[0].trim();
					   return nword;}
				}catch(Exception e){}
			return wordform;
		}

			@Override
			public Map<String, String> getVerbCategoryMap() {
				// TODO Auto-generated method stub
				return null;
			}
}
