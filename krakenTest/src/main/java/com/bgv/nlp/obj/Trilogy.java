package com.bgv.nlp.obj;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;


public class Trilogy {
	public static void main(String[] args) throws Exception {
		
		JsonParser jsonParser = new JsonFactory().createParser(new File("C:/Users/Electron/Downloads/semantic_data/latest-all.json/latest-all.json"));
		
		parseJSON(jsonParser);
		
	}
	
	
	private static void parseJSON(JsonParser jsonParser) throws JsonParseException, IOException {
		// boolean insidePropertiesObj=false;
		//loop through the JsonTokens
		while(jsonParser.nextToken() != JsonToken.END_OBJECT){
			String name = jsonParser.getCurrentName();
			if("id".equals(name)){
				jsonParser.nextToken();
				System.out.println(jsonParser.getText());
			}else if("type".equals(name)){
				jsonParser.nextToken();
				System.out.println(jsonParser.getText());
			
			}else if("lastrevid".equals(name)){
				jsonParser.nextToken();
				System.out.println(jsonParser.getText());
			}else if("modified".equals(name)){
				jsonParser.nextToken();
				System.out.println(jsonParser.getText());
				
		}else if("labels".equals(name)){
		jsonParser.nextToken();
		//nested object, recursive call
		while(jsonParser.nextToken() != JsonToken.END_OBJECT){
			String key = jsonParser.getCurrentName();
			jsonParser.nextToken();
			String value = jsonParser.getText();
boolean count=false; 			
			if(key.equals("ru")){count=true;
			//System.out.println("ikey="+key+" ivalue="+value);
			}
			
			int i=0;
			while(jsonParser.nextToken() != JsonToken.END_OBJECT){
				String keya = jsonParser.getCurrentName();
				
				jsonParser.nextToken();
				String valuea = jsonParser.getText();

				
				if(count){System.out.println("akey="+keya+" avalue="+valuea);
				i++;if(i>1)count=false;}
			}
		
		}
			
		}else if("descriptions".equals(name)){
		jsonParser.nextToken();
		
		while(jsonParser.nextToken() != JsonToken.END_OBJECT){
			String key = jsonParser.getCurrentName();
			jsonParser.nextToken();
			String value = jsonParser.getText();
boolean count=false; 			
			if(key.equals("ru")){count=true;
			//System.out.println("ikey="+key+" ivalue="+value);
			}
			
			int i=0;
			while(jsonParser.nextToken() != JsonToken.END_OBJECT){
				String keya = jsonParser.getCurrentName();
				
				jsonParser.nextToken();
				String valuea = jsonParser.getText();

				
				if(count){System.out.println("ckey="+keya+" cvalue="+valuea);
				i++;if(i>1)count=false;}
			}
		
		}
		}else if("aliases".equals(name)){
		jsonParser.nextToken();
		while(jsonParser.nextToken() != JsonToken.END_OBJECT){
			String key = jsonParser.getCurrentName();
			jsonParser.nextToken();
			String value = jsonParser.getText();
			//System.out.println("key="+key+" value="+value);
			boolean count=false; 			
			if(key.equals("ru")){count=true;
			System.out.println("ikey="+key+" ivalue="+value);
			}
			
			while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
				while(jsonParser.nextToken() != JsonToken.END_OBJECT){
					String keyx = jsonParser.getCurrentName();
					jsonParser.nextToken();
					String valuex = jsonParser.getText();
					if(count)System.out.println("keyx="+keyx+" valuex="+valuex);
				}
                // display msg1, msg2, msg3
				//System.out.println(jsonParser.getText());

			}
			
		}System.out.println("--------end aliases------------");
		}else if("claims".equals(name)){
		jsonParser.nextToken();
		//nested object, recursive call
		while(jsonParser.nextToken() != JsonToken.END_OBJECT){
			String key = jsonParser.getCurrentName();
			jsonParser.nextToken();
			String value = jsonParser.getText();
					System.out.println("zkey="+key+" zvalue="+value);
			
			
			
			
			if("mainsnak".equals(key))
			{while(jsonParser.nextToken() != JsonToken.END_OBJECT){
				String keymainsnak = jsonParser.getCurrentName();
				jsonParser.nextToken();
				String valuemainsnak = jsonParser.getText();
				System.out.println("ykey="+keymainsnak+" yvalue="+valuemainsnak);
				if("datavalue".equals(keymainsnak))
				{while(jsonParser.nextToken() != JsonToken.END_OBJECT){
					String keydata = jsonParser.getCurrentName();
					jsonParser.nextToken();
					String valuedata = jsonParser.getText();
					System.out.println("keydata="+keydata+" valuedata="+valuedata);
					if("value".equals(keydata))
					{while(jsonParser.nextToken() != JsonToken.END_OBJECT){
						String keydatavalue = jsonParser.getCurrentName();
						jsonParser.nextToken();
						String valuedatavalue = jsonParser.getText();
						System.out.println("data="+keydatavalue+" data="+valuedatavalue);
					}
					}
				}
				}
			}
			}
			if("snaks-order".equals(key)){
				while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
					
					System.out.println("data="+jsonParser.getText());
				}
			}
			
			JsonToken current;
			
			current = jsonParser.nextToken();
		    if (current != JsonToken.START_OBJECT) {
		      System.out.println("Error: root should be object: quiting.");
		      return;
		    }

		    while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
		      String fieldName = jsonParser.getCurrentName();
		      // move from field name to field value
		      current = jsonParser.nextToken();
		      if (fieldName.equals("references")) {
		        if (current == JsonToken.START_ARRAY) {
		          // For each of the records in the array
		          while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
		            // read the record into a tree model,
		            // this moves the parsing position to the end of it
		          //  JsonNode node = jsonParser.readValueAsTree();
		            // And now we have random access to everything in the object
		          //  System.out.println("field1: " + node.get("field1").getValueAsText());
		          //  System.out.println("field2: " + node.get("field2").getValueAsText());
		          }
		        } else {
		          System.out.println("Error: records should be an array: skipping.");
		          jsonParser.skipChildren();
		        }
		      } else {
		        System.out.println("Unprocessed property: " + fieldName);
		        jsonParser.skipChildren();
		      }
		    }                
			
			
			/*
			if("references".equals(key)){
				while(jsonParser.nextToken() != JsonToken.END_ARRAY){
					String keyreferences = jsonParser.getCurrentName();
					jsonParser.nextToken();
					String valuereferences = jsonParser.getText();	
				
					while(jsonParser.nextToken() != JsonToken.END_OBJECT){
						String vkey = jsonParser.getCurrentName();
						jsonParser.nextToken();
						String vvalue = jsonParser.getText();
								System.out.println("vkey="+vkey+" zvalue="+vvalue);
								
								if("snaks".equals(vkey))
								{while(jsonParser.nextToken() != JsonToken.END_OBJECT){
									String keymainsnak = jsonParser.getCurrentName();
									jsonParser.nextToken();
									String valuemainsnak = jsonParser.getText();
									System.out.println("snakkey="+keymainsnak+" snakvalue="+valuemainsnak);
									while(jsonParser.nextToken() != JsonToken.END_ARRAY){
										String keyre = jsonParser.getCurrentName();
										jsonParser.nextToken();
										String valuere= jsonParser.getText();	
									
									
									if("datavalue".equals(keymainsnak))
									{while(jsonParser.nextToken() != JsonToken.END_OBJECT){
										String keydata = jsonParser.getCurrentName();
										jsonParser.nextToken();
										String valuedata = jsonParser.getText();
										System.out.println("keydatavalue="+keydata+" datavalue="+valuedata);
										if("value".equals(keydata))
										{while(jsonParser.nextToken() != JsonToken.END_OBJECT){
											String keydatavalue = jsonParser.getCurrentName();
											jsonParser.nextToken();
											String valuedatavalue = jsonParser.getText();
											System.out.println("key="+keydatavalue+" value="+valuedatavalue);
										}
										}
									}
									}
								}}
								}
								if("snaks-order".equals(vkey)){
									while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
										
										System.out.println("snaks-order="+jsonParser.getText());
									}
								}
								
					}
				
			}
				System.out.println("--------end references------------"); }
			
			*/
			//}
			/*
			while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
				while(jsonParser.nextToken() != JsonToken.END_OBJECT){
					String keyx = jsonParser.getCurrentName();
					jsonParser.nextToken();
					String valuex = jsonParser.getText();
					
					
					if("mainsnak".equals(keyx)){
						while(jsonParser.nextToken() != JsonToken.END_OBJECT){
							String keyreferences = jsonParser.getCurrentName();
							jsonParser.nextToken();
							String valuereferences = jsonParser.getText();	}
						}
					if("references".equals(keyx)){
						while(jsonParser.nextToken() != JsonToken.END_OBJECT){
							String keyreferences = jsonParser.getCurrentName();
							jsonParser.nextToken();
							String valuereferences = jsonParser.getText();	
						
						System.out.println("keyw="+keyreferences+" valuew="+valuereferences);
						if("snaks".equals(keyreferences)){
							while(jsonParser.nextToken() != JsonToken.END_OBJECT){
								String keyc = jsonParser.getCurrentName();
								jsonParser.nextToken();
								String valuec = jsonParser.getText();
								System.out.println("keysnaks="+keyc+" valuesnaks="+valuec);
								while (jsonParser.nextToken() != JsonToken.END_ARRAY) {jsonParser.nextToken();}
							}
						}
						if("snaks-order".equals(keyreferences)){
							while (jsonParser.nextToken() != JsonToken.END_ARRAY) {System.out.println("Ok="+jsonParser.getText());}
						}
						
					
					}
						}
				}
             

	  } */
			
		}
		System.out.println("--------end claims------------");
		}else if("sitelinks".equals(name)){
			System.out.println("--------sitelinks------------");
		jsonParser.nextToken();
		//nested object, recursive call
		while(jsonParser.nextToken() != JsonToken.END_OBJECT){
			String key = jsonParser.getCurrentName();
			jsonParser.nextToken();
			String value = jsonParser.getText();
			System.out.println(jsonParser.getText());
		}
			System.out.println("--------sitelinks end------------");	
			
			}
			/*else if("properties".equals(name)){
				jsonParser.nextToken();
				while(jsonParser.nextToken() != JsonToken.END_OBJECT){
					String key = jsonParser.getCurrentName();
					jsonParser.nextToken();
					String value = jsonParser.getText();
					System.out.println("key="+key+" value="+value);
				}
			} */
		}
	}

}
