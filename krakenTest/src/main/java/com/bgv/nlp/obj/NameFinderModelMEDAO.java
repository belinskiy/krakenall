package com.bgv.nlp.obj;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.lucene.morphology.LuceneMorphology;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.json.simple.parser.ParseException;
import org.languagetool.JLanguageTool;
import org.maltparser.MaltParserService;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.util.InvalidFormatException;

public interface NameFinderModelMEDAO {
	NameFinderME getNameFinderModel();

	Map<String, String> getSwordmap();

	Map<String, String> getLiniarmap();

	LuceneMorphology getLuceneMorph();

	Map<String, String> getSynonimMap();

	Map<String, String> getContextMap();

	Map<String, String> getMatmap();

	
	
	
	Map<String, String> getPhraseCounterMap();

	Map<String, Integer> getPhraseTokenMapValue();

	Map<String, Integer> getPhraseSortedTokenMapValue();

	void setProbabilitycounterMap(Map<String, String> probabilitycounterMap);

	void setTokenMapValue(Map<String, Integer> tokenMapValue);

	void setNounVerbMap(Map<String, String> map);

	Map<String, String> getNounVerbMap();

	JLanguageTool getLanguageTool();

	Map<String, ArrayList> getSymantexhashMap();

	Map<String, String> getvoprNarechiaMap();

	//void loadNegative(String filea) throws InvalidFormatException, IOException, ParseException;

	//void loadPositive(String filea) throws InvalidFormatException, IOException, ParseException;

	void loadEmotion(String negative) throws InvalidFormatException, IOException, ParseException;

	Map<String, String> getIdcategorymapobj();

	void setIdcategorymapobj(Map<String, String> idcategorymapobj);

	Map<String, String> getFamilii();

	void setFamilii(Map<String, String> familii);

	Map<String, String> getLiniarmapmed();

	Map<String, String> getSynonimMapmed();

	Map<String, String> getContextMapmed();

	MaltParserService getMaltParser();

	void setMaltParser(MaltParserService maltParser);

	Map<String, String> getVerbCategoryMap();

	Map<String, String> getVerbGlobalCategoryMap();

	Map<String, String> getVerbSubCategoryMap();

	WordVectors getWordVectors();

	Map<String, List> getadjectiveNounMap();

	void loadAdjective(String filename);
}
