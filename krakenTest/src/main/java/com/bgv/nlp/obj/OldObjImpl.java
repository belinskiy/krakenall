package com.bgv.nlp.obj;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bgv.nlp.spam.TokenizerDAO;
import com.bgv.springmvc.domain.FindObject;
//import com.bgv.springmvc.domain.SpamTextStatus;


import opennlp.tools.namefind.NameFinderME;
//import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.util.Span;


@Service
public class OldObjImpl implements OldObjDAO {
	@Autowired
	 private TokenizerDAO token;
	@Autowired
	 private NameFinderModelMEDAO model;
	
	@Override
	public LuceneMorphology getLuceneMorph() {
		return model.getLuceneMorph();
	}
	
	@Override
	public List<FindObject> discover(String id, String text) {
		List<FindObject> list=new ArrayList<FindObject>();
		    //Tokenizer tokenizer = token.getTokenizer();
			//String tokens[] = tokenizer.tokenize(text);
			//String tokens[] = text.toString().replaceAll("[\\W&&[^\\s]]", "").split("\\W+");
		String tokens[] = text.toString().split("\\p{P}?[ \\t\\n\\r]+");
		NameFinderME nameFinder = model.getNameFinderModel();
		
		
		
		Span nameSpans[] = nameFinder.find(tokens);
		for (int i=0;i<nameSpans.length;i++)
		{
			
			for (int j=nameSpans[i].getStart();j<nameSpans[i].getEnd();j++)
			{
			FindObject obj=new FindObject();
			String wordobj=tokens[j];
			
			String objid=getObjId(wordobj);
			
			obj.setId(objid);
			obj.setName(wordobj);
			obj.setStrpos(""+nameSpans[i].getStart());
			obj.setStrend(""+nameSpans[i].getEnd());
			
			
			
			list.add(obj);
			
			//list.add(tokens[j]);
			}
		}
		
		
		return list;
	}
	
	public String getObjId(String text){
		String Id="-1";
		Map<String, String> wordmap=new HashMap<String, String>();
		wordmap=model.getSwordmap();
		try{
		Id=wordmap.get(text);
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return Id;
		
	}
	//docid= textorig=
	//list[ objid= strpos= endpos= 
	//]

	@Override
	public List<FindObject> discoverLiniar(String id, String text) {
		try{
		
			String dirty=text;
			String dirtyNoProbel=text;
	    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
	    	try{dirty=dirty.replaceAll("ё|Ё","е");}catch(Exception e){	e.printStackTrace();}
	    	try{dirtyNoProbel=dirty.replaceAll("[\\s]{2,}", " ");}catch(Exception e){	e.printStackTrace();}
	    	
		String tokens[] = dirty.toString().split("\\p{P}?[ \\t\\n\\r]+");
		List<String[]> alltokens = new  ArrayList<String[]>();
		List<String[]> alltokenstrim = new  ArrayList<String[]>();
		Map<String, Integer>  wordCount=new HashMap<String, Integer>();
		int size = tokens.length;
		
		
   	 List<IndexWrapper> probelTextCount = new ArrayList<>();
   	 Matcher matcher = Pattern.compile("[\\s]{2,}").matcher(dirty);
   	 while (matcher.find()) {
   		 int start = matcher.start();
   		 int end = matcher.end();
   		 IndexWrapper p=new IndexWrapper(start, end);
  		//p.setStart(start);
  		//p.setEnd(end);
  		
   		probelTextCount.add(p);
   		//System.out.println("start="+start+" end="+end);
   	 }
		
		
		
		
		for(int i=0;i<size;i++){
			String[] indexsingle = new String[2];
			String[] indexbigram = new String[2];
			String[] indextrigram = new String[2];
			String[] indexfourgram = new String[2];
			String[] indexfivegram = new String[2];
			String[] indexsixgram = new String[2];
			
			String single=null;
			String bigram=null;
			String trigram=null;
			String fourgram=null;
			String fivegram=null;
			String sixgram=null;
			
			single=tokens[i];
			indexsingle[0] = single;
			indexsingle[1] = i+"|1";
			
			if(wordCount.containsKey(single)){
			
			int t=wordCount.get(single);
			
			indexsingle[1] = i+"|1|"+(t+1);
			wordCount.put(single, t+1);
			
			}
			else{
				wordCount.put(single, 1);
				indexsingle[1] = i+"|1|1";
			}
			
			
			if(i+1<size){
			bigram=tokens[i]+" "+tokens[i+1];
			indexbigram[0] = bigram;
			indexbigram[1] = i+"|2";
			    if(wordCount.containsKey(bigram)){
				
				int t=wordCount.get(bigram);
				indexbigram[1] = i+"|2|"+(t+1);
				wordCount.put(bigram, t+1);
				
				}
				else{
					wordCount.put(bigram, 1);
					indexbigram[1] = i+"|2|1";
				}
			
			}
			if(i+2<size){
			trigram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2];
			indextrigram[0] = trigram;
			indextrigram[1] = i+"|3";
			
		    if(wordCount.containsKey(trigram)){
				
			int t=wordCount.get(trigram);
			indextrigram[1] = i+"|3|"+(t+1);
			wordCount.put(trigram, t+1);
			
			}
			else{
				wordCount.put(trigram, 1);
				indextrigram[1] = i+"|3|1";
			}
			
			}
			if(i+3<size){
			fourgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3];
			indexfourgram[0] = fourgram;
			indexfourgram[1] = i+"|4";
			
		    if(wordCount.containsKey(fourgram)){
				
			int t=wordCount.get(fourgram);
			indexfourgram[1] = i+"|4|"+(t+1);
			wordCount.put(fourgram, t+1);
			
			}
			else{
				wordCount.put(fourgram, 1);
				indexfourgram[1] = i+"|4|1";
			}
		    
			}
			if(i+4<size){
				fivegram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4];
				indexfivegram[0] = fivegram;
				indexfivegram[1] = i+"|5";
				
			    if(wordCount.containsKey(fivegram)){
					
					int t=wordCount.get(fivegram);
					indexfivegram[1] = i+"|5|"+(t+1);
					wordCount.put(fivegram, t+1);
					
					}
					else{
						wordCount.put(fivegram, 1);
						indexfivegram[1] = i+"|5|1";
					}
				
			}
			if(i+5<size){
				sixgram=tokens[i]+" "+tokens[i+1]+" "+tokens[i+2]+" "+tokens[i+3]+" "+tokens[i+4]+" "+tokens[i+5];
				indexsixgram[0] = sixgram;
				indexsixgram[1] = i+"|6";
				
			    if(wordCount.containsKey(sixgram)){
					
					int t=wordCount.get(sixgram);
					indexsixgram[1] = i+"|6|"+(t+1);
					wordCount.put(sixgram, t+1);
					
					}
					else{
						wordCount.put(sixgram, 1);
						indexsixgram[1] = i+"|6|1";
					}
				
			}
			//System.out.println("="+fivegram); 
			
			
			if(single!=null)
				alltokens.add(indexsingle);
				if(bigram!=null)
					alltokens.add(indexbigram);
					if(trigram!=null)
						alltokens.add(indextrigram);
						if(fourgram!=null)
							alltokens.add(indexfourgram);
							if(fivegram!=null)
								alltokens.add(indexfivegram);
		
								if(sixgram!=null)
									alltokens.add(indexsixgram);
							
							
					          if(i>15000){
					        	  break;
					          }	
		}
		
		
		for (String[] wordtokend: alltokens) { 
			String[] index = new String[2];
			index[0]=wordtokend[0].trim();
			index[1]=wordtokend[1];
			
			alltokenstrim.add(index);
		}
		alltokens=alltokenstrim;
		
		
		
		List<FindObject> list=new ArrayList<FindObject>();
		
		Map<String, String> liniarmap=new HashMap<String, String>();
		liniarmap=model.getLiniarmap();
		
		Map<String, String> weaksynonimMap=new HashMap<String, String>();
		weaksynonimMap=model.getSynonimMap();
		
		Map<String, String> weakcontextMap=new HashMap<String, String>();
		weakcontextMap=model.getContextMap();
		
		Map<String, String> matmap=new HashMap<String, String>();
		matmap=model.getMatmap();
		
//		System.out.println(liniarmap.size()+"\n");
//		for (Map.Entry<String, String> entry : liniarmap.entrySet())
//		{
//		    System.out.println(entry.getKey() + "/" + entry.getValue());
//		}
		
//		for (String wordtokend: alltokens) { 
//		
//				//String lowerStrd = wordtokend.toLowerCase().trim();
//				System.out.println("alltoken="+wordtokend+"*");
//				
//			}
		
		
		try{
			int k=0;
			for (String[] wordtoken: alltokens) { 
			//for (String wordtoken: tokens) {  
				try{	
					String lowerStr = wordtoken[0].toLowerCase();
					
					
					
					
					String objectmat=matmap.get(lowerStr);
					
					if(objectmat!=null){
					FindObject obj=new FindObject();
					obj.setId(objectmat);
					
					obj.setName(wordtoken[0]);

					
					int b=getPisitionB(dirty, wordtoken);
					
					
					int d=wordtoken[0].length()+b;
					obj.setStrpos(""+b);
					obj.setStrend(""+d);
					obj.setIsmat("true");
					if(b>-1)
					list.add(obj);
					}
					
					
					
					
					String objectid=liniarmap.get(lowerStr);
					
					if(objectid!=null){
					FindObject obj=new FindObject();
					obj.setId(objectid);
					obj.setIsmat("false");
					
					obj.setName(wordtoken[0]);
					
					 int[] beginend=new int[2]; 
					 beginend=getPisitionC(dirtyNoProbel, wordtoken, probelTextCount);
					int b=beginend[0];
					int d=beginend[1];
				
					obj.setStrpos(""+b);
					obj.setStrend(""+d);
					if(b>-1)
					list.add(obj);
					else if(b==-1){
						b=0;
						d=wordtoken[0].length();
						obj.setStrpos(""+b);
						obj.setStrend(""+d);
						
						list.add(obj);	
					}
					}
					
					
					
					String weaksynonim=weaksynonimMap.get(lowerStr);
					//String weakcontext=weakcontextMap.get(lowerStr);
					
					
					if(weaksynonim!=null){
					FindObject weakobj=new FindObject();
					String weakobjectid=getMyweekID(wordtoken, weaksynonim, lowerStr, weakcontextMap, alltokens, dirty);
					
					if(weakobjectid!=null){		
							
					weakobj.setId(weakobjectid);
					weakobj.setName(wordtoken[0]);
					weakobj.setIsmat("false");


					int[] beginend=new int[2]; 
					beginend=getPisitionC(dirtyNoProbel, wordtoken, probelTextCount);
					int bweak=beginend[0];
					int dweak=beginend[1];
					
					weakobj.setStrpos(""+bweak);
					weakobj.setStrend(""+dweak);

					if(bweak>-1)
					list.add(weakobj);
					else if(bweak==-1){
						bweak=0;
						dweak=wordtoken[0].length();
						weakobj.setStrpos(""+bweak);
						weakobj.setStrend(""+dweak);
						list.add(weakobj);	
					}
					}
					
					
					}
					
				}catch(Exception e){
						e.printStackTrace();
						
					}
				  k++;
		          if(k>15000){
		        	  break;
		          }	
			}
			
			
		
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return list;
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}
	}
	


	private String getMyweekID(String[] weaksynonimText, String weaksynonim,String weaksynonimlowerStr, Map<String, String> weakcontextMap, List<String[]> alltokens, String allText) {
		

		
	
		Map<String, String> weaksynmap=new HashMap<String, String>();
		if(weaksynonim.contains("|")){
			String[] myData = weaksynonim.split("\\|");
        	for (String syns: myData) {
        		weaksynmap.put(syns, "1");
        	}
			
		}else{
			weaksynmap.put(weaksynonim, "1");
			
		}
		
		Map<String, String> contextMap=new HashMap<String, String>();
		
		contextMap=weakcontextMap;
		String objectid=null;
		int k=0;
		for (String[] wordtoken: alltokens) { 
			
				try{	
					 String lowerStr = wordtoken[0].toLowerCase().trim();
							 
					 if(!weaksynonimlowerStr.equals(lowerStr))
					 objectid=contextMap.get(lowerStr);
					 
					 if(objectid==null){
						 
						 //return null;
						 
					 }else{
					 if(objectid.contains("|")){
							 String[] objectidData = objectid.split("\\|");
					        	for (String key: objectidData) {
					        		if (weaksynmap.containsKey(key))
					        		{
					        		boolean dd=calcWordsDistanceB(weaksynonimText, wordtoken[0], allText);
					        		if(dd)
					        		return key;
					        			}
					        		
					        	}
					 }
					 else{
						 if (weaksynmap.containsKey(objectid))
			        			{
							 
							     boolean dd=calcWordsDistanceB(weaksynonimText, wordtoken[0], allText);
							     if(dd)
							     return objectid;
							     }
					
					
					 }
					 }
					 
				}catch(Exception e){
					e.printStackTrace();
					return null;
				}
				
				 k++;
		          if(k>15000){
		        	  break;
		          }	
		}
		
		
		
		return null;
	}

	private boolean calcWordsDistance(String[] weaksynonimText, String synonimContext,String text) {
		boolean dist = false;
		try{
		
		int firstW=getPisition(text, weaksynonimText);
		List<Integer> list = new ArrayList<>();
		list=getWordPisition(text, synonimContext);
		int k=0;
		for(Integer secondW:list){
		if(firstW>-1 && secondW >-1)
		{
					
			
			
			if(secondW>firstW)
			{
				int total=secondW-(firstW+weaksynonimText[0].length());
				if(total<100)
					dist = true;
				
			}
			else{
				
				int total=firstW-(secondW+synonimContext.length());
				if(total<100)
					dist = true;
			}

		}
		k++;
		if(k>30){
      	  break;
        }			
		}
		}catch(Exception e){e.printStackTrace(); }
		return dist;
	}

	private List<Integer> getWordPisition(String text, String synonimContext) {
		List<Integer> list = new ArrayList<>();
		
		
		int myposition=0;
		int i = 0;
		int k=0;
		
		try{
		
	    while (i >= 0){
	    	
	    	  
	    	  myposition=text.indexOf( synonimContext,  myposition);
	    	  
	    	  if(myposition>=0){
	    		  myposition=myposition+synonimContext.length();
	    	  		list.add(myposition); }
	    	  
	    	  i=myposition; //-1
	    
	    	 
	  		  
	          k++;
	          if(k>10){
	        	  break;
	          }
	    }
		
		}catch(Exception e){e.printStackTrace(); }
		  
		  
		return list;
	}

	int getPisition(String Str, String[] wordtoken){
		
		
		
		int myposition=-1000;  
		try{
		String text=wordtoken[0];
		  String TN=wordtoken[1];
		 
		  
		  
		String[] objectidData = TN.split("\\|");
		 
		 String T=objectidData[0];
		 String I=objectidData[2];
		 
		 
//		if(T.equals("0")){
//	    myposition=Str.indexOf( text );
//		return myposition;
//		}
//		else 
		 if(I.equals("1")){
			 myposition=Str.indexOf( text );
				return myposition;
		}
		else{
			//int t=Integer.parseInt(T);
			int i=Integer.parseInt(I);
			int last=0;
			int lastgo=0;

			
			for(int k=1;k<=i;k++){	
				
				
				last=Str.indexOf( text, lastgo );
				lastgo=last+text.length();
			
				
				
				if(k>20)break;
			}
			
			return myposition=last;	
			
		}
		
	}catch(Exception e){e.printStackTrace(); return myposition;}
		
	}
	
    public List<IndexWrapper> findIndexesForKeyword(String searchString, String keyword) {
        String regex = "\\b"+keyword+"\\b";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(searchString);
 
        List<IndexWrapper> wrappers = new ArrayList<IndexWrapper>();
 
        while(matcher.find() == true){
            int end = matcher.end();
            int start = matcher.start();
            IndexWrapper wrapper = new IndexWrapper(start, end);
            wrappers.add(wrapper);
        }
        return wrappers;
    }
    
	int[] getPisitionC(String Str, String[] wordtoken, List<IndexWrapper>  probelTextCount){
		
		 int[] beginend=new int[2]; 
		 beginend[0]=-1000;
		 beginend[1]=-1000;
 
		try{
		String text=wordtoken[0];
		  String TN=wordtoken[1];
		 
		  
		  
		String[] objectidData = TN.split("\\|");
		 
		 String T=objectidData[0];
		 String I=objectidData[2];

		 if(I.equals("1")){
			    List<IndexWrapper> list = new ArrayList<>();
			    list =  findIndexesForKeyword(Str,text);
			    try{
			    if(list.size()>0){
			    	int lbeforeposstart = list.get(0).getStart();
			    	int lbeforeposend = list.get(0).getEnd();
			    	 	
			         beginend=getTruePos(lbeforeposstart, lbeforeposend,  probelTextCount);
			   //myposition=Str.indexOf( text );
				return beginend;}
			 	}catch(Exception e){e.printStackTrace();}
		}
		else{
			//int t=Integer.parseInt(T);
			int i=Integer.parseInt(I);
			if(i>0) i=i-1;
			List<IndexWrapper> list = new ArrayList<>();
		    list =  findIndexesForKeyword(Str,text);
		    try{
			    if(list.size()>0){
			    
			    	//myposition=findIndexesForKeyword(Str,text).get(i).getStart();
			    	int lbeforeposstart = list.get(i).getStart();
			    	int lbeforeposend = list.get(i).getEnd();
			    		
			         beginend=getTruePos(lbeforeposstart, lbeforeposend,  probelTextCount);
			
			         return beginend;	
			}
			    }catch(Exception e){e.printStackTrace();}
		}
		
	}catch(Exception e){e.printStackTrace(); return beginend;}
		return beginend;
	}
	
	public  int[] getTruePos(int beforeposstart, int beforeposend, List<IndexWrapper>  probelTextCount){
		 int[] beginend=new int[2]; 
		 beginend[0]=0;
		 beginend[1]=0;
		 try{ 

							int intermleght=0;
					   	    int skolikostart=0;
					   			 for (IndexWrapper entry : probelTextCount)
					   			 {
					   				 int start=entry.getStart();
					   				 int end=entry.getEnd();
					   				 
					   			     if(beforeposstart>start){
					   			    	 int deleted=end-start-1;
					   			    	
					   			    	 skolikostart=skolikostart+deleted;
					   			     }
					   			    
					   			     if(start>beforeposstart&&start<beforeposend){
					   			    	 intermleght=end-start-1;
					   			    	 //System.out.println("intermleght="+intermleght); 
					   			     }
					   			    
					   			 } 
					   	 
					   	
					   	 
					   	 int begin=(beforeposstart+skolikostart);
					   	 int totalleght=((beforeposend-beforeposstart)+intermleght);
					   	 int z=begin+totalleght;
					   	 beginend[0]=begin;
						 beginend[1]=z;
		}catch(Exception e){e.printStackTrace();}
	   
		return beginend;
	   	 
		}
    
	int getPisitionB(String Str, String[] wordtoken){
		
		

		int myposition=-1000;  
		try{
		String text=wordtoken[0];
		  String TN=wordtoken[1];
		 
		  
		  
		String[] objectidData = TN.split("\\|");
		 
		 String T=objectidData[0];
		 String I=objectidData[2];

		 if(I.equals("1")){
			    List<IndexWrapper> list = new ArrayList<>();
			    list =  findIndexesForKeyword(Str,text);
			    try{
			    if(list.size()>0){
			    myposition=list.get(0).getStart();
			   
			   //myposition=Str.indexOf( text );
				return myposition;}
			 	}catch(Exception e){e.printStackTrace();}
		}
		else{
			//int t=Integer.parseInt(T);
			int i=Integer.parseInt(I);
			if(i>0) i=i-1;
			List<IndexWrapper> list = new ArrayList<>();
		    list =  findIndexesForKeyword(Str,text);
		    try{
			    if(list.size()>0){
			    
			    	myposition=findIndexesForKeyword(Str,text).get(i).getStart();
			
			
			

			
			return myposition;	}
			    }catch(Exception e){e.printStackTrace();}
		}
		
	}catch(Exception e){e.printStackTrace(); return myposition;}
		return myposition;
	}
	
	
	private boolean calcWordsDistanceB(String[] weaksynonimText, String synonimContext,String text) {
		boolean dist = false;
		try{
		
		int firstW=getPisitionB(text, weaksynonimText);
		List<Integer> list = new ArrayList<>();
		list=getWordPisitionB(text, synonimContext);
		int k=0;
		for(Integer secondW:list){
		if(firstW>-1 && secondW >-1)
		{
					

			
			if(secondW>firstW)
			{
				int total=secondW-(firstW+weaksynonimText[0].length());
				if(total<100)
					dist = true;
				
			}
			else{
				
				int total=firstW-(secondW+synonimContext.length());
				if(total<100)
					dist = true;
			}

		}
		k++;
		if(k>30){
      	  break;
        }			
		}
		}catch(Exception e){e.printStackTrace(); }
		return dist;
	}
	
	
	private List<Integer> getWordPisitionB(String text, String synonimContext) {
		List<Integer> list = new ArrayList<>();
		try{
		
        String regex = "\\b"+synonimContext+"\\b";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        int k=0;
        while(matcher.find() == true){
            
            int start = matcher.start();
           
            int myposition=start+synonimContext.length();
	  		list.add(myposition);
	  		
	  		 k++;
	          if(k>10){
	        	  break;
	          }
        }

		
		}catch(Exception e){e.printStackTrace(); }
		  
		  
		return list;
	}

}
