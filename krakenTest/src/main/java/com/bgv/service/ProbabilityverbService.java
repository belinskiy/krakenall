package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IProbabilityverbDAO;
import com.bgv.entity.Horosho;
import com.bgv.entity.Ploho;
import com.bgv.entity.Probabilitybwa;
import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbexpl;
import com.bgv.entity.Probabilityverbtrip;
import com.bgv.entity.Probabilityverbtrippp;
import com.bgv.entity.Probabilityverbtripqcq;
import com.bgv.entity.Probabilityverbtripqq;



@Service
public class ProbabilityverbService implements IProbabilityverbService{
	
	@Autowired
	private IProbabilityverbDAO probability;

	

	@Override
	public boolean addProbabilitycounter(Probabilityverb counter) {
		probability.addProbabilitycounter(counter);
		return false;
	}
	@Override
	public boolean addProbabilitycounterTriplet(Probabilityverbtrip counter) {
		probability.addProbabilitycounterTriplet(counter);
		return false;
	}
	@Override
	public boolean addProbabilitycounterTripletqq(Probabilityverbtripqq counter) {
		probability.addProbabilitycounterTripletqq(counter);
		return false;
	}
	@Override
	 public List<Probabilityverb> getAllByPage(final int pageSize, final int pageNumber, final Timestamp dateupdate) {
		return probability.getAllByPage(pageSize,pageNumber, dateupdate);
	}


	@Override
	public Long countText() {
		return probability.countText();
	}
	@Override
	 public void getLarge() {probability.getLarge();}
	
	@Override
	 public void getLargeUpdate() {probability.getLargeUpdate();}
	@Override
	public void getLargeTriplet() {
		probability.getLargeTriplet();
		
	}
	@Override
	public void addProbabilitycounterTripletPredlog(Probabilityverbtrippp counter) {
		probability.addProbabilitycounterTripletPredlog(counter);
		
		
	}
	@Override
	public void addProbabilityPloho(Ploho counter) {
		probability.addProbabilityPloho(counter);
		
	}
	@Override
	public void addProbabilityHorosho(Horosho counter) {
		probability.addProbabilityHorosho(counter);
		
	}
	@Override
	public void addProbabilitycounterExpl(Probabilityverbexpl counter) {
		probability.addProbabilitycounterExpl(counter);	
	}
	@Override
	public void addProbabilitycounterTripletqcq(Probabilityverbtripqcq countern) {
		probability.addProbabilitycounterTripletqcq(countern);
		
	}
	@Override
	public void addProbabilitycounterProperty(Probabilitybwa counter) {
		probability.addProbabilitycounterProperty(counter);
		
	}
}
