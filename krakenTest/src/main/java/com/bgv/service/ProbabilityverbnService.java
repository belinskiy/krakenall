package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IProbabilityverbnDAO;
import com.bgv.entity.Probabilityadjective;
import com.bgv.entity.Probabilityverbn;



@Service
public class ProbabilityverbnService implements IProbabilityverbnService{
	
	@Autowired
	private IProbabilityverbnDAO probability;

	

	@Override
	public boolean addProbabilitycounter(Probabilityverbn counter) {
		probability.addProbabilitycounter(counter);
		return false;
	}

	
	@Override
	 public List<Probabilityverbn> getAllByPage(final int pageSize, final int pageNumber, final Timestamp dateupdate) {
		return probability.getAllByPage(pageSize,pageNumber, dateupdate);
	}


	@Override
	public Long countText() {
		return probability.countText();
	}
	 @Override
	 public void getLarge() {probability.getLarge();}


	@Override
	public List<Probabilityverbn> getNounnameFromVerb(String verbname) {
		return probability.getNounnameFromVerb(verbname);
	}


	@Override
	public void addProbabilitycounteradj(Probabilityadjective countern) {
		probability.addProbabilitycounteradj(countern);
	}
	 
	 
}
