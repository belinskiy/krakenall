package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Probabilityadjective;
import com.bgv.entity.Probabilityverbn;


public interface IProbabilityverbnService {
	boolean addProbabilitycounter(Probabilityverbn counter);

	List<Probabilityverbn> getAllByPage(int pageSize, int pageNumber, Timestamp dateupdate);

	Long countText();

	void getLarge();
	
	List<Probabilityverbn> getNounnameFromVerb(String verbname);

	void addProbabilitycounteradj(Probabilityadjective countern);

}
