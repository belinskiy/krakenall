package com.bgv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bgv.dao.ITonality;


@Service
public class TonalityService implements ITonalityService {
	@Autowired
	private ITonality tonalityDAO;
	
	@Override
	public void getLargeNegative() {
		tonalityDAO.getLargeNegative();
		
	}
	@Override
	public boolean addTonality(String word, String nword, String type) {
		tonalityDAO.addTonality( word,  nword,  type);
		return false;
	}

} 