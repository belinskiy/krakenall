package com.bgv.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.maltparser.MaltParserService;
import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.core.symbol.SymbolTable;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import org.maltparser.core.syntaxgraph.edge.Edge;
import org.maltparser.core.syntaxgraph.node.DependencyNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bgv.entity.MaltEntity;
import com.bgv.nlp.obj.IndexWrapper;
import com.bgv.nlp.obj.NameFinderModelMEDAO;
import com.bgv.practice.Predicates;

@Service
public class ParserSevice implements IParserSevice {
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private NameFinderModelMEDAO nameFinderModelMEDAO;

	@Override
	public JSONObject getCollPOSdata(String exguid) {
		String result = "none";
		JSONObject outjson = new JSONObject();
		try {
			JSONObject injson = new JSONObject();
			try {
				exguid = new String(exguid.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e1) {

				e1.printStackTrace();
			}

			injson.put("text", exguid);
			String jsonText = injson.toString();

			// System.out.println("jsonText="+jsonText+"\n");

			final String ursi = "http://172.17.53.12:32009";
			// final String ursi ="http://172.17.53.12:32008?mode=morpho";
			// final String ursi ="http://192.168.2.105:32007/";
			// System.out.println("start============0============"+new Date());
			String resultursi = restTemplate.postForObject(ursi, jsonText, String.class);

			// System.out.println("resultursi="+resultursi+"\n");
			JSONParser parser = new JSONParser();
			JSONObject json;

			json = (JSONObject) parser.parse(resultursi);

			JSONArray text = (JSONArray) json.get("tree");
			Iterator<JSONObject> iterator = text.iterator();
			List<MaltEntity> entityList = new ArrayList<MaltEntity>();
			while (iterator.hasNext()) {
				parser = new JSONParser();
				JSONObject jobj = (JSONObject) iterator.next();
				MaltEntity entity = new MaltEntity();

				String to = "";
				String word = "";
				String name = "";
				String add1 = "";
				String add2 = "";
				String type = "";
				String n1 = "";
				String n2 = "";
				String normword = "";
				String number = "";
				long nmb = 0;
				try {
					nmb = (Long) jobj.get("Number");
					number = "" + nmb;
				} catch (Exception e) {
				}

				try {
					word = (String) jobj.get("word");
				} catch (Exception e) {
				}

				try {
					type = (String) jobj.get("type");
				} catch (Exception e) {
				}

				try {
					n1 = (String) jobj.get("N1");
				} catch (Exception e) {
				}
				try {
					n2 = (String) jobj.get("N2");
				} catch (Exception e) {
				}
				try {
					normword = (String) jobj.get("lemma");
				} catch (Exception e) {
				}

				entity.setAdd1(add1);
				entity.setAdd2(add2);
				entity.setN1(n1);
				entity.setN2(n2);
				entity.setName(name);
				entity.setNormword(normword);
				entity.setNumber(number);
				entity.setTo(to);
				entity.setType(type);
				entity.setWord(word);

				entityList.add(entity);

			}

			int size = entityList.size();

			if (size > 0) {

				// in the CoNLL data format.
				String[] tokens = new String[size];
				for (MaltEntity entity : entityList) {
					String number = entity.getNumber();
					int ipos = Integer.parseInt(number);

					String word = entity.getWord();
					String normword = entity.getNormword();
					String n1 = entity.getN1();
					String n2 = entity.getN2();
					String type = entity.getType();

					String to = entity.getTo();
					String name = entity.getName();
					String add1 = entity.getAdd1();
					String add2 = entity.getAdd2();

					// Number, word, norm_word, N1, N2, type, to, name, add1,
					// add2

					// System.out.println(number+"\t"+word+"\t"+normword+"\t"+n1+"\t"+n2+"\t"+type);
					tokens[ipos - 1] = number + "\t" + word + "\t" + normword + "\t" + n1 + "\t" + n2 + "\t" + type;

				}
				/*
				 * tokens[0] = "1\tGrundavdraget\t_\tN\tNN\tDD|SS"; tokens[1] =
				 * "2\tupphör\t_\tV\tVV\tPS|SM"; tokens[2] =
				 * "3\talltså\t_\tAB\tAB\tKS"; tokens[3] =
				 * "4\tvid\t_\tPR\tPR\t_"; tokens[4] = "5\ten\t_\tN\tEN\t_";
				 * tokens[5] = "6\ttaxerad\t_\tP\tTP\tPA"; tokens[6] =
				 * "7\tinkomst\t_\tN\tNN\t_"; tokens[7] = "8\tpå\t_\tPR\tPR\t_";
				 * tokens[8] = "9\t52500\t_\tR\tRO\t_"; tokens[9] =
				 * "10\tkr\t_\tN\tNN\t_"; tokens[10] = "11\t.\t_\tP\tIP\t_";
				 */
				try {
					// System.out.println("start============1============"+new
					// Date());
					// Parses sentence above
					MaltParserService service = nameFinderModelMEDAO.getMaltParser();

					// MaltParserService service = new MaltParserService();
					// Inititalize the parser model 'model0' and sets the
					// working directory to '.' and sets the logging file to
					// 'parser.log'
					// service.initializeParserModel("-c rus-test -m parse -w .
					// -lfi parser.log");
					// System.out.println("start============2============"+new
					// Date());

					DependencyStructure graph;

					// Outputs the dependency graph created by MaltParser.
					graph = service.parse(tokens);
					// System.out.println(graph);
					// result=graph.toString();

					// outjson.put("graph", result);

					// Terminates the parser model
					// service.terminateParserModel();

					try {
						JSONArray jArray = new JSONArray();
						for (int i = 1; i <= graph.getHighestDependencyNodeIndex(); i++) {
							JSONObject jsonlemma = new JSONObject();

							DependencyNode node = graph.getDependencyNode(i);
							if (node != null) {
								int k = 1;
								for (SymbolTable table : node.getLabelTypes()) {
									String a = node.getLabelSymbol(table);
									// System.out.println( a+ "*-a---\t");
									jsonlemma.put("a" + k, a);

									if (k == 2) {
										try {
											List<IndexWrapper> listspos = new ArrayList<>();
											listspos = findIndexesForKeyword(exguid, a);
											int beginpos = 0;
											int endpos = 0;
											if (listspos.size() > 0) {

												beginpos = listspos.get(0).getStart();
												endpos = a.length() + beginpos;

											}

											jsonlemma.put("start", beginpos);
											jsonlemma.put("end", endpos);
										} catch (Exception e) {
										}
									}
									k++;
								}
								if (node.hasHead()) {
									Edge e = node.getHeadEdge();
									int b = e.getSource().getIndex();
									// System.out.println(b + "*-b---\t");

									jsonlemma.put("edge", "" + b);

									if (e.isLabeled()) {
										for (SymbolTable table : e.getLabelTypes()) {
											String c = e.getLabelSymbol(table);
											// System.out.println(c +
											// "*-c---\t");
											if (!c.equals("#false#"))
												jsonlemma.put("wordtype", "" + c);

										}
									} else {
										for (SymbolTable table : graph.getDefaultRootEdgeLabels().keySet()) {
											String d = graph.getDefaultRootEdgeLabelSymbol(table);
											// System.out.println(d +
											// "*-d---\t");
											jsonlemma.put("rootedge", "" + d);

										}
									}
								}

							}

							jArray.add(jsonlemma);
						}
						outjson.put("graphdata", jArray);
					} catch (Exception e) {
					}

				} catch (MaltChainedException e) {

					e.printStackTrace();
				}

				// System.out.println("start============3============"+new
				// Date());
			}

		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return outjson;

	}

	@Override
	public Predicates getCollPOSdataAndPredicates(String exguid, Map<String, Integer[]> wordlist) {
		String result = "none";
		JSONObject outjson = new JSONObject();
		Map<Integer, String> predicates = new HashMap<Integer, String>();
		try {
			JSONObject injson = new JSONObject();
			try {
				exguid = new String(exguid.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e1) {

				e1.printStackTrace();
			}

			injson.put("text", exguid);
			String jsonText = injson.toString();

			// System.out.println("jsonText="+jsonText+"\n");

			final String ursi = "http://172.17.53.12:32009";
			// final String ursi ="http://172.17.53.12:32008?mode=morpho";
			// final String ursi ="http://192.168.2.105:32007/";
			// System.out.println("start============0============"+new Date());
			String resultursi = restTemplate.postForObject(ursi, jsonText, String.class);

			// System.out.println("resultursi="+resultursi+"\n");
			JSONParser parser = new JSONParser();
			JSONObject json;

			json = (JSONObject) parser.parse(resultursi);

			JSONArray text = (JSONArray) json.get("tree");
			Iterator<JSONObject> iterator = text.iterator();
			List<MaltEntity> entityList = new ArrayList<MaltEntity>();
			while (iterator.hasNext()) {
				parser = new JSONParser();
				JSONObject jobj = (JSONObject) iterator.next();
				MaltEntity entity = new MaltEntity();

				String to = "";
				String word = "";
				String name = "";
				String add1 = "";
				String add2 = "";
				String type = "";
				String n1 = "";
				String n2 = "";
				String normword = "";
				String number = "";
				long nmb = 0;
				try {
					nmb = (Long) jobj.get("Number");
					number = "" + nmb;
				} catch (Exception e) {
				}

				try {
					word = (String) jobj.get("word");
				} catch (Exception e) {
				}

				try {
					type = (String) jobj.get("type");
				} catch (Exception e) {
				}

				try {
					n1 = (String) jobj.get("N1");
				} catch (Exception e) {
				}
				try {
					n2 = (String) jobj.get("N2");
				} catch (Exception e) {
				}
				try {
					normword = (String) jobj.get("lemma");
				} catch (Exception e) {
				}

				entity.setAdd1(add1);
				entity.setAdd2(add2);
				entity.setN1(n1);
				entity.setN2(n2);
				entity.setName(name);
				entity.setNormword(normword);
				entity.setNumber(number);
				entity.setTo(to);
				entity.setType(type);
				entity.setWord(word);

				entityList.add(entity);

			}

			int size = entityList.size();

			if (size > 0) {

				// in the CoNLL data format.
				String[] tokens = new String[size];
				for (MaltEntity entity : entityList) {
					String number = entity.getNumber();
					int ipos = Integer.parseInt(number);

					String word = entity.getWord();
					String normword = entity.getNormword();
					String n1 = entity.getN1();
					String n2 = entity.getN2();
					String type = entity.getType();

					String to = entity.getTo();
					String name = entity.getName();
					String add1 = entity.getAdd1();
					String add2 = entity.getAdd2();

					// Number, word, norm_word, N1, N2, type, to, name, add1,
					// add2

					// System.out.println(number+"\t"+word+"\t"+normword+"\t"+n1+"\t"+n2+"\t"+type);
					tokens[ipos - 1] = number + "\t" + word + "\t" + normword + "\t" + n1 + "\t" + n2 + "\t" + type;

				}
				/*
				 * tokens[0] = "1\tGrundavdraget\t_\tN\tNN\tDD|SS"; tokens[1] =
				 * "2\tupphör\t_\tV\tVV\tPS|SM"; tokens[2] =
				 * "3\talltså\t_\tAB\tAB\tKS"; tokens[3] =
				 * "4\tvid\t_\tPR\tPR\t_"; tokens[4] = "5\ten\t_\tN\tEN\t_";
				 * tokens[5] = "6\ttaxerad\t_\tP\tTP\tPA"; tokens[6] =
				 * "7\tinkomst\t_\tN\tNN\t_"; tokens[7] = "8\tpå\t_\tPR\tPR\t_";
				 * tokens[8] = "9\t52500\t_\tR\tRO\t_"; tokens[9] =
				 * "10\tkr\t_\tN\tNN\t_"; tokens[10] = "11\t.\t_\tP\tIP\t_";
				 */
				try {

					MaltParserService service = nameFinderModelMEDAO.getMaltParser();

					DependencyStructure graph;

					graph = service.parse(tokens);

					try {
						JSONArray jArray = new JSONArray();
						

						for (int i = 1; i <= graph.getHighestDependencyNodeIndex(); i++) {
							JSONObject jsonlemma = new JSONObject();

							boolean thisispredicate = false;
							String a1 = "";
							String a2 = "";
							String nword = "";
							String a4 = "";
							DependencyNode node = graph.getDependencyNode(i);
							if (node != null) {
								int k = 1;
								if (node.hasHead()) {
									Edge e = node.getHeadEdge();
									int b = e.getSource().getIndex();
									// System.out.println(b + "*-b---\t");

									jsonlemma.put("edge", "" + b);

									if (e.isLabeled()) {
										for (SymbolTable table : e.getLabelTypes()) {
											String c = e.getLabelSymbol(table);
											// System.out.println(c +
											// "*-c---\t");
											if (!c.equals("#false#"))
												jsonlemma.put("wordtype", "" + c);

											// предик
											if (c.equals("предик")) {
												thisispredicate = true;
											}
										}
									} else {
										for (SymbolTable table : graph.getDefaultRootEdgeLabels().keySet()) {
											String d = graph.getDefaultRootEdgeLabelSymbol(table);
											// System.out.println(d +
											// "*-d---\t");
											jsonlemma.put("rootedge", "" + d);
											thisispredicate = true;
										}
									}
								}
								for (SymbolTable table : node.getLabelTypes()) {
									String a = node.getLabelSymbol(table);
									// \"a1\":\"18\",\"a2\":\"человеку\",\"a3\":\"человек\"
									jsonlemma.put("a" + k, a);
									if (k == 4) {
										a4 = a;
										if (thisispredicate)
											System.out.println("P========================"+a4+"=");
									}
									if (k == 3) {
										nword = a;
										if (thisispredicate)
											System.out.println("P========================"+nword);
									}
									if (k == 2) {
										a2 = a.toLowerCase();
										/*
										 * try{ List<IndexWrapper> listspos =
										 * new ArrayList<>(); listspos =
										 * findIndexesForKeyword(exguid, a); int
										 * beginpos = 0; int endpos = 0; if
										 * (listspos.size() > 0) {
										 * 
										 * beginpos =
										 * listspos.get(0).getStart(); endpos =
										 * a.length() + beginpos;
										 * 
										 * }
										 * 
										 * jsonlemma.put("start", beginpos);
										 * jsonlemma.put("end", endpos);
										 * 
										 * } catch (Exception e) {}
										 */
									}
									if (k == 1) {
										a1 = a;
									}
									k++;
								}
								try {
									if (thisispredicate)
										if (a4.equals("N")||a4.equals("P")) {
											System.out.println("Nnword=================="+nword+"=a2="+a2+"==");
											Integer[] wordindex = wordlist.get(a2);
											int sizes = wordindex.length;
											String key = nword;
											System.out.println("N========================"+wordindex[0]+"=="+key);
											if (sizes == 1) {

												predicates.put(wordindex[0],key);

											} else if (sizes > 1) {
												// calc distance
												int maltpos = Integer.parseInt(a1);
												int min = 10000;
												int bestval = -1;
												for (int ik = 0; ik < sizes; ik++) {

													int inverbDistance = Math.abs(wordindex[ik] - maltpos);
													if (min > inverbDistance) {
														min = inverbDistance;
														bestval = wordindex[ik];
													}
												}
												predicates.put(wordindex[bestval],key);

											}

										}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							jArray.add(jsonlemma);
						}
						outjson.put("graphdata", jArray);
					} catch (Exception e) {
						
					}

				} catch (MaltChainedException e) {

					e.printStackTrace();
				}

				// System.out.println("start============3============"+new
				// Date());
			}

		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		Predicates pred=new Predicates();
		pred.setPredicates(predicates);
		pred.setOutjson(outjson);
		return pred;

	}

	public List<IndexWrapper> findIndexesForKeyword(String searchString, String keyword) {
		String regex = "\\b" + keyword + "\\b";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(searchString);

		List<IndexWrapper> wrappers = new ArrayList<IndexWrapper>();
		int start = 0;
		int end = 0;
		boolean inotfind = true;
		while (matcher.find() == true) {
			end = matcher.end();
			start = matcher.start();
			IndexWrapper wrapper = new IndexWrapper(start, end);
			wrappers.add(wrapper);
			inotfind = false;
		}
		if (inotfind) {
			IndexWrapper wrapper = new IndexWrapper(start, end);
			wrappers.add(wrapper);
		}
		return wrappers;
	}
}
