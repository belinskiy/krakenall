package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Person;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilitymain;
import com.bgv.springmvc.domain.TokenProbability;

public interface IProbabilityService {
	 //boolean addTokenProbability(List<TokenProbability> problist);

	 boolean addDerbyTokenProbability(List<TokenProbability> problist);

	 void addTokenProbability(String exguid, List<TokenProbability> problist);

	  List<Probabilitymain> getAllDocuments(Timestamp ourJavaTimestampObject);

	void deleteMainText(Timestamp ourJavaTimestampObject);

	Long countText(Timestamp ourJavaTimestampObject);

	List<Probabilitymain> getAllByPage(int ip, int pageSize, Timestamp ourJavaTimestampObject);
}
