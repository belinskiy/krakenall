package com.bgv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IShedulerDAO;

import com.bgv.entity.Sheduler;
import com.bgv.entity.Shedulerslog;

@Service
public class ShedulerService implements IShedulerService{
	@Autowired
	private IShedulerDAO shedulerDAO;

	
	@Override
	public List<Sheduler> getAllSheduler(){
		return shedulerDAO.getAllSheduler();
	}
	
	@Override
	public void updateSheduler(Sheduler person) {
		
		shedulerDAO.updateSheduler(person);
		
		Shedulerslog log= new Shedulerslog();
		log.setLastpid(person.getLastpid());
		log.setLastupdate(person.getLastupdate());
		log.setModelname(person.getModelname());
		log.setPrecisioncount(person.getPrecisioncount());
		log.setSname(person.getSname());
		
		shedulerDAO.addShedulerLog(log);
	}

	@Override
	public void updateShedulerLog(Sheduler person) {
		Shedulerslog log= new Shedulerslog();
		log.setLastpid(person.getLastpid());
		log.setLastupdate(person.getLastupdate());
		log.setModelname(person.getModelname());
		log.setPrecisioncount(person.getPrecisioncount());
		log.setSname(person.getSname());
		
		shedulerDAO.addShedulerLog(log);
		
	}

}
