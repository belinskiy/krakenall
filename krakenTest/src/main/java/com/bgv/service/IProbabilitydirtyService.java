package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Person;
import com.bgv.entity.Probabilitydirty;
import com.bgv.springmvc.domain.TokenProbability;

public interface IProbabilitydirtyService {
	boolean addProbabilitydirty(Probabilitydirty dirty);

	List<Probabilitydirty> getAlldirtytext();

	void deleteText();

	void deleteText(Timestamp ourJavaTimestampObject);

	List<Probabilitydirty> getAllDocuments(Timestamp ourJavaTimestampObject);

	Long countText(Timestamp ourJavaTimestampObject);

	List<Probabilitydirty> getAllByPage(int ip, int pageSize, Timestamp ourJavaTimestampObject);
}
