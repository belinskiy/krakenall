package com.bgv.service;
import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Person;

public interface IPersonService {
     List<Person> getAllPersons();
     Person getPersonById(int pid);
     boolean addPerson(Person person);
     void updatePerson(Person person);
     void deletePerson(int pid);
	List<Person> getAllDocuments(Timestamp d);
	int getMaxDocuments();
} 