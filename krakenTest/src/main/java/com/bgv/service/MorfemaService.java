package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IMorfematonDAO;
import com.bgv.dao.IPersonDAO;
import com.bgv.entity.Person;

@Service
public class MorfemaService implements IMorfematonService {
	@Autowired
	private IMorfematonDAO morfematonDAO;
	
	@Override
	public void getLarge() {
		morfematonDAO.getLarge();
		
	}
	@Override
	public boolean morfematonSet(String aword, String vword, String bword, int awordton, int vwordton, int bwordton) {
		morfematonDAO.morfematonExists(aword, vword, bword, awordton, vwordton, bwordton);
		return false;
	}

} 