package com.bgv.service;


import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IProbabilitycounterDAO;
import com.bgv.dao.IProbabilitydirtyDAO;
import com.bgv.entity.Probabilitycounter;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilityverb;



@Service
public class ProbabilitycounterService implements IProbabilitycounterService{
	
	@Autowired
	private IProbabilitycounterDAO probability;

	

	@Override
	public boolean addProbabilitycounter(Probabilitycounter counter) {
		probability.addProbabilitycounter(counter);
		return false;
	}

	@Override
	public List<Probabilitycounter> probabilityExists(String tokentext) {
		return probability.probabilityExists(tokentext);
	}
	
	

}
