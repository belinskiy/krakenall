package com.bgv.service;

import java.util.Collection;

import com.bgv.nlp.spam.LearningSample;

public interface ILearnMESpam {


void learn(Collection<LearningSample> el, String modelname);
}
