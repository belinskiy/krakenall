package com.bgv.service;

public interface ITonalityService {
	void getLargeNegative();
	boolean addTonality(String word, String nword, String type);
}
