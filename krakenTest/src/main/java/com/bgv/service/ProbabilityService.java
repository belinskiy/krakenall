package com.bgv.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IPersonDAO;
import com.bgv.dao.IProbabilitymainDAO;
import com.bgv.dao.IProbabilityobjectDAO;
import com.bgv.dao.IProbabilityposDAO;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilityobject;
import com.bgv.entity.Probabilitypos;
import com.bgv.springmvc.domain.FindObject;
import com.bgv.springmvc.domain.TokenProbability;
import com.derby.model.Mprobabilitymain;
import com.derby.model.MprobabilitymainDao;
import com.derby.model.MprobabilityobjectDao;


@Service
public class ProbabilityService implements IProbabilityService{
	
	@Autowired
	private IProbabilitymainDAO probabilitymain;
	
	@Autowired
	private IProbabilityobjectDAO probabilityobject;
	
	@Autowired
	private IProbabilityposDAO probabilitypos;
	

//	@Autowired
//	private MprobabilitymainDao probabilitymainDao;
//	
//	@Autowired
//	private MprobabilityobjectDao probabilityobjectDao;
//	
//	@Autowired
//	private MprobabilityposDao probabilityposDao;
	
	@Override
	public void addTokenProbability(String exguid, List<TokenProbability> problist) {
		
		
		List<TokenProbability> probs =new ArrayList<TokenProbability>();
		probs=problist;
		for(TokenProbability p:probs){
			List<String[]> alltokens = p.getAlltokens();
			List<FindObject> olist = p.getFindobject();
			
			
			for (String[] wordtokend: alltokens) {
				Probabilitymain pmain=new Probabilitymain();
				Probabilityobject pobject=new Probabilityobject();
			    Probabilitypos ppos=new Probabilitypos();
				//text:T|N|I
				
				
				String tokentext = wordtokend[0]; // text
				String tokentextvalue=wordtokend[1]; //T|N|I
				String[]  tokenData = tokentextvalue.split("\\|");
				
				 
				 String T=tokenData[0];
				 String N=tokenData[1];
				 String I=tokenData[2];
				 
				 pmain.setTokentext(tokentext);
				 pmain.setTokentotal(1);
				 pmain.setTokentype(Integer.parseInt(N));
			
			
				 String guid=probabilitymain.addProbabilitymain(pmain);
				 
				 int fi=0;
				 int[] index = new int[10];
				 for(FindObject f:olist){
					 
					 index[fi]=Integer.parseInt(f.getId());
					 fi++;
					
					 if(fi>9)break;
				 }
				 
				 pobject.setGuid(guid);
				 pobject.setObjecta(index[0]);
				 pobject.setObjectb(index[1]);
				 pobject.setObjectc(index[2]);
				 pobject.setObjectd(index[3]);
				 pobject.setObjecte(index[4]);
				 pobject.setObjectf(index[5]);
				 pobject.setObjectg(index[6]);
				 pobject.setObjecti(index[7]);
				 pobject.setObjectj(index[8]);
				 pobject.setObjectk(index[9]);
				 pobject.setExguid(exguid);
				// probabilityobject.addProbabilityobject(pobject);
				 
				 ppos.setGuid(guid);
				 ppos.setTokenpos(Integer.parseInt(T));
				 ppos.setTokentype(Integer.parseInt(N));
				 ppos.setExguid(exguid);
				// probabilitypos.addProbabilitypos(ppos);
			}

		}
		
		
		
	}
	@Override
	public List<Probabilitymain> getAllDocuments(Timestamp ourJavaTimestampObject) {
		return probabilitymain.getAllDocuments(ourJavaTimestampObject);
	}
	
	@Override
	public boolean addDerbyTokenProbability(List<TokenProbability> problist) {
		
/*		
		List<TokenProbability> probs =new ArrayList<TokenProbability>();
		probs=problist;
		for(TokenProbability p:probs){
			List<String[]> alltokens = p.getAlltokens();
			List<FindObject> olist = p.getFindobject();
			
			
			for (String[] wordtokend: alltokens) {
				Probabilitymain pmain=new Probabilitymain();
				Probabilityobject pobject=new Probabilityobject();
			    Probabilitypos ppos=new Probabilitypos();
				//text:T|N|I 
				
				
				String tokentext = wordtokend[0]; // text
				String tokentextvalue=wordtokend[1]; //T|N|I
				String[]  tokenData = tokentextvalue.split("\\|");
				
				 
				 String T=tokenData[0];
				 String N=tokenData[1];
				 String I=tokenData[2];
				
				 List<Mprobabilitymain> exist = probabilitymainDao.findByName(tokentext);
				 
				String guid;
				if(exist.isEmpty())
				 {	 
					 
					 guid= probabilitymainDao.save(tokentext, 1, Integer.parseInt(N));
				 
				 }
				 else{
					//update 
					 probabilitymainDao.update(tokentext);
					 guid=exist.get(0).getGuid();
					 //guid= exist.getGuid();
				 }
				 
				 int fi=0;
				 int[] index = new int[10];
				 for(FindObject f:olist){
					 
					 index[fi]=Integer.parseInt(f.getId());
					 fi++;
					
					 if(fi>9)break;
				 }
				 
				 probabilityobjectDao.save(guid, index[0], index[1],index[2],index[3],index[4], index[5], index[6], index[7], index[8], index[9]);
				 
//				 ppos.setGuid(guid);
//				 ppos.setTokenpos(Integer.parseInt(T));
//				 ppos.setTokentype(Integer.parseInt(N));
				 
				 probabilityposDao.save(guid,Integer.parseInt(T),  Integer.parseInt(N));
			}

		}
		
		*/
		return false;
	}
	@Override
	public void deleteMainText(Timestamp dateupdate) {
		probabilitymain.deleteText(dateupdate);
		
	}
	@Override
	public Long countText(Timestamp ourJavaTimestampObject) {
				return probabilitymain.countText(ourJavaTimestampObject);
	}
	@Override
	public List<Probabilitymain> getAllByPage(int ip, int pageSize, Timestamp ourJavaTimestampObject) {
		
		return probabilitymain.getAllByPage(pageSize, ip, ourJavaTimestampObject);
	}




}
