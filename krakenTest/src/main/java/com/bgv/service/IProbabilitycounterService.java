package com.bgv.service;

import java.util.List;

import com.bgv.entity.Probabilitycounter;


public interface IProbabilitycounterService {
	boolean addProbabilitycounter(Probabilitycounter counter);

	List<Probabilitycounter> probabilityExists(String tokentext);
}
