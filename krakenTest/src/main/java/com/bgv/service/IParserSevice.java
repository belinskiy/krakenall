package com.bgv.service;

import java.util.Map;

import org.json.simple.JSONObject;

import com.bgv.practice.Predicates;

public interface IParserSevice {

	JSONObject getCollPOSdata(String exguid);

	//JSONObject getCollPOSdataAndPredicates(String exguid);

	Predicates getCollPOSdataAndPredicates(String exguid, Map<String, Integer[]> wordlist);

}
