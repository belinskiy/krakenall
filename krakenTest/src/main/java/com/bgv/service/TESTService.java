package com.bgv.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.obj.NameFinderModelMEDAO;
import com.bgv.nlp.obj.OldObjDAO;
import com.bgv.practice.QResult;

@Service
public class TESTService implements ITESTService{
	
	@Autowired
	private FastModelDAO fastMemoryModel;
	@Autowired
	 private NameFinderModelMEDAO nameFinderModelMEDAO;
	 
	 //израильтянин
	
	
//нет поиска по морфеме	
	    
	
	    @Override
		public String searchRootRule(String cmd) {
	    String result="1";
	   
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
		Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
		Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
		Map<String, String> morfema = fastMemoryModel.getMorfema();
		Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
	    try{ 
	    	
	    	StringBuffer sbBuffer=new StringBuffer();
	    	if(cmd!=null){
	    		if(!cmd.equals("")){
	    			
	    			
	    			 String[] objectidData = cmd.split("\\|");
				
	    			 String nword=objectidData[0];
	    			 String vword=objectidData[1];
//System.out.println("nword="+nword+"\n");	    			 
	    			 //nword - найти Q контекст
	    			 //searchRootObject 
	    			 //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
	    			 //записать весь лог


                        boolean foundresult=false;
                        QResult z =new QResult();
                        String r ="";
                        
                        if(nonamesingle.containsKey(nword)){
//0. управляемые определения                         	
                        	List<String> qindexList = new  ArrayList<String>();
							//qindexList.add("Q1346360"); загрузка из ДБ
							qindexList.add(nonamesingle.get(nword));
                        	z = getQobjects(nword, "1", qindexList, true);
                            r = z.getOut();
                            sbBuffer.append(r);
                        	
                        	
                        }else{
 //1. поиск в синонимах                       
                        try{  
                    	z = getQobjects(nword, "1", null, false);
                        r = z.getOut();
                        sbBuffer.append(r);
                        //foundresult=z.isFoundresult();
                        //System.out.println("START1="+nword+"\n");							
                        if(!foundresult){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
                        		   
							    String syn = iwordSynonims.get(nword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
									
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();

										 z = getQobjects(newnword, "2", null, false);
				                         r = z.getOut();
				                         //foundresult=z.isFoundresult();
				                         sbBuffer.append(r);
				                        
									}
	
								}
						}
			    		} catch (Exception e) {
			    			e.printStackTrace();
			    		}
//2. поиск в гиперонимах
                        try{	
						
						
						if(!foundresult){
	
							ArrayList tempdoList = null;
							tempdoList = iSymantex.get(nword);
	
							if (tempdoList != null) {
								int j = 0;
								for (Object value : tempdoList) {
									j++;
									String[] index = new String[2];
									index = (String[]) value;
	
									String iSymantexword = index[0];
									String itype = index[1];
									iSymantexword = iSymantexword.trim();

									// надо - выйти из цикла
									 //if(foundresult)break;

									 z = getQobjects(iSymantexword, "3", null, false);
			                         r = z.getOut();
			                         //foundresult=z.isFoundresult();
			                         sbBuffer.append(r);
			                        
									}
								}
						
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
//3. поиск в морфемах
                        
                        try{     
                        
							String m = morfema.get(nword);
							if (m != null) {

								if (m.contains("|")) {
									String[] mdata = m.split("\\|");
									int sizeb = mdata.length;
									for (int i = 0; i < sizeb; i++) {
										String single = mdata[i];

										try {
											String same = morfemawords.get(single);

											if (same != null) {

												if (same.contains("|")) {
													String[] msamedata = same.split("\\|");
													int sizec = msamedata.length;
													String wordform = "";
													for (int j = 0; j < sizec; j++) {
														String singleword = msamedata[j].trim()
																.toLowerCase();

														String wrdi = singleword
																.replaceAll("[^А-я]", "").trim();

														List<String> wordBaseForms = null;
														if (!"".equals(wrdi)) {
															wordBaseForms = luceneMorph.getMorphInfo(wrdi);
														}
														int sizer = 0;
														try {
															sizer = wordBaseForms.size();
														} catch (Exception e) {
														}

														if (sizer > 0) {

															StringBuilder sb = new StringBuilder();
															for (String s : wordBaseForms) {
																sb.append(s);
																sb.append(" #");
															}

															String ist = sb.toString();
															if (!ist.equals("") && ist != null) {
																wordform = ist;
															}

															if (wordform.contains("|")) {
																String[] myData = wordform.split("\\|");
																String inword = myData[0].trim();
																String imorf = myData[1];
																
																if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																	 //listnword.add(inword);
																	 z = getQobjects(inword, "4", null, false);
											                         r = z.getOut();
											                         //foundresult=z.isFoundresult();
											                         sbBuffer.append(r);
																}
															}
														}
													}

												}
											}

										} catch (Exception e) {
											e.printStackTrace();
										}

									}

								} else {

									try {
										String same = morfemawords.get(m);

										if (same != null) {
											if (same.contains("|")) {
												String[] msamedata = same.split("\\|");
												int sizec = msamedata.length;
												String wordform = "";
//если результат найден выйди из цикла морфем												
												for (int i = 0; i < sizec; i++) {
													String singleword = msamedata[i].trim()
															.toLowerCase();

													String wrdi = singleword.replaceAll("[^А-я]", "")
															.trim();

													List<String> wordBaseForms = null;
													if (!"".equals(wrdi)) {
														wordBaseForms = luceneMorph.getMorphInfo(wrdi);
													}

													int sizer = 0;
													try {
														sizer = wordBaseForms.size();
													} catch (Exception e) {
													}
//мень, не обрабатываются все результаты морфологии только первый
													if (sizer > 0) {

														StringBuilder sb = new StringBuilder();
														for (String s : wordBaseForms) {
															sb.append(s);
															sb.append(" #");
														}

														String ist = sb.toString();
														if (!ist.equals("") && ist != null) {
															wordform = ist;
														}

														if (wordform.contains("|")) {
															String[] myData = wordform.split("\\|");
															String inword = myData[0].trim();
															String imorf = myData[1];
															if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																//listnword.add(inword);
																z = getQobjects(inword, "4", null, false);
										                        r = z.getOut();
										                        //foundresult=z.isFoundresult();
										                        sbBuffer.append(r);
															}
														}
													}
												}

											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

								}
							}
                        	
                        } catch (Exception e) {
        	    			e.printStackTrace();
        	    		}  
                        
	    		}   
					//vword - найти контекст глаголов 
					boolean foundverb=false;
					 sbBuffer.append(" [verb=").append(vword).append("]");
					try{
					 String varbcat = verbglobalcategory.get(vword);
					 if(varbcat!=null){
					 if(!varbcat.equals("")){	 
						 //foundverb=true;
						 sbBuffer.append(" [varbcat=").append(varbcat).append("]");
					 }
					 }
					 
					 
		    		} catch (Exception e) {
		    			e.printStackTrace();
		    		}
	    			 
	    			 
					try{
                        if(!foundverb){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
							   
							    String syn = iwordSynonims.get(vword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
	
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();
											try{
												 String varbcat = verbglobalcategory.get(newnword);
												 if(varbcat!=null){
												 if(!varbcat.equals("")){	 
													 //foundverb=true;
													 sbBuffer.append("["+newnword+"] [synonims=").append(varbcat).append("]");
												 }
												 }
												 
												 
									    		} catch (Exception e) {
									    			e.printStackTrace();
									    		}
								    			 
				                        
									}
								}
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
					
					try{	if(!foundverb){
						
						ArrayList tempdoList = null;
						tempdoList = iSymantex.get(vword);
						if (tempdoList != null) {
							int j = 0;
							for (Object value : tempdoList) {
								j++;
								String[] index = new String[2];
								index = (String[]) value;

								String iSymantexword = index[0];
								String itype = index[1];
								iSymantexword = iSymantexword.trim();
								// надо - выйти из цикла
								
								try{
									 String varbcat = verbglobalcategory.get(iSymantexword);
									 if(varbcat!=null){
									 if(!varbcat.equals("")){	 
										 //foundverb=true;
										 sbBuffer.append("["+iSymantexword+"] [synonims=").append(varbcat).append("]");
									 }
									 }
									 
									 
						    		} catch (Exception e) {
						    			e.printStackTrace();
						    		}
		                       

								}
							}
					}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
	    			 
	    			 
	    			
	    		}
	    	}
	    	result=sbBuffer.toString();
	    }catch(Exception e){e.printStackTrace();}
		
	    return result;
	}
		QResult getQobjects(String nword, String type, List<String> qindexListExt, boolean ext){
	 	    QResult result=new QResult();
	 	    StringBuffer sbBuffer=new StringBuffer();
	 	    sbBuffer.append("");
		    boolean foundresult=false; 
		    try{ 
		  
		    Map<String, List> iilobjectsList = fastMemoryModel.getIilobjects();
		    Map<String, List> iilinstanceList = fastMemoryModel.getOfinstance();
			Map<String, List> iilsubclassofList = fastMemoryModel.getIilsubclassof();
			Map<String, List> iilmaincategory = fastMemoryModel.getMaincategory();
			Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
			Map<String, List> iiltext = fastMemoryModel.getIilobjectsToText();
			Map<String, List> iilproperties = fastMemoryModel.getIilotherprops();
			 List<String> qindexList;
			 if(ext)
			 qindexList = qindexListExt;
			 else
			 qindexList = iilobjectsList.get(nword);
			 
			    sbBuffer.append("*type="+type+"[nword=").append(nword).append("]");
			 
			 
			    Map<String, List> returtQ = new HashMap<String, List>();

				returtQ = searchMainObjectArray(qindexList, iilobjectsList, iilinstanceList,
						iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);
				 
				int qsize=0;
				try{qsize=returtQ.size();}catch(Exception e){}
				
				sbBuffer.append("[size=").append(qsize).append("]");
				 
				if(qsize>0){ 
				sbBuffer.append("[map=").append(returtQ.size()).append("]"); 
				for (Map.Entry<String, List> entry : returtQ.entrySet())
				{
				    
					String mainQ=entry.getKey();
					sbBuffer.append("[mainQ=").append(mainQ).append("]");
					List<String> content = entry.getValue();
					if(content!=null){
						for(String data:content){	
							sbBuffer.append("").append(data).append("/");
							foundresult=true;
						}
					}
					sbBuffer.append("=map]"); 
				}}
				
		        } catch (Exception e) {
					e.printStackTrace();
				}
		   result.setOut(sbBuffer.toString());
		   result.setFoundresult(foundresult);
		return result;
	}
		
		QResult getQobjectForCompress(String nword, String type, List<String> qindexListExt, boolean ext){
	 	    QResult result=new QResult();
	 	    StringBuffer sbBuffer=new StringBuffer();
	 	    sbBuffer.append("");
		    boolean foundresult=false;
		      result.setOut("");
			  result.setFoundresult(foundresult);
		    try{ 
		  
		    Map<String, List> iilobjectsList = fastMemoryModel.getIilobjects();
		    Map<String, List> iilinstanceList = fastMemoryModel.getOfinstance();
			Map<String, List> iilsubclassofList = fastMemoryModel.getIilsubclassof();
			Map<String, List> iilmaincategory = fastMemoryModel.getMaincategory();
			Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
			Map<String, List> iiltext = fastMemoryModel.getIilobjectsToText();
			Map<String, List> iilproperties = fastMemoryModel.getIilotherprops();
			 List<String> qindexList;
			 if(ext)
			 qindexList = qindexListExt;
			 else
			 qindexList = iilobjectsList.get(nword);
			 
			 
			    Map<String, List> returtQ = new HashMap<String, List>();

				returtQ = searchMainObjectForCompress(qindexList, iilobjectsList, iilinstanceList,
						iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);

				List<String> resultsavetypeid=new ArrayList<String> ();
				
				
				
				resultsavetypeid=returtQ.get("1");
				String savetype="";
				for(String savetypeid:resultsavetypeid){
					
					if(!savetypeid.equals("none"))
					foundresult=true;
					savetype=savetypeid;
				}
				  result.setReturt(returtQ);
				  //result.setOut(savetype);
				    result.setFoundresult(foundresult);
				  
		        } catch (Exception e) {
					e.printStackTrace();
				}
		 
		return result;
	}
		
		public Map<String, List> searchMainObjectArray(List<String> qindexList, Map<String, List> iilobjectsList,
				Map<String, List> iilinstanceList, Map<String, List> iilsubclassofList, Map<String, List> iilproperties, Map<String, List> iilmaincategory, Map<String, List> iiltext, Map<String, List> iilhaspart) {
			
			


			Map<String, List> returndata=new HashMap<String, List>();
			 
			 
			Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
			 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
			 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
			   String itypeid ="";
			   String icategoryid ="";
			   String savetypeid ="none";
			   String savecategoryid ="";
			   String itogo = "";
			   String[] res=new String[2];
			   res[0]="";
			   res[1]="";
			   
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			if(sgeo==false&&shuman==false&&sorg==false){
		//----------------------------Q---------------------------- 
		//https://dumps.wikimedia.org/wikidatawiki/entities/	
			boolean canexit=false;
			//список Q полученных по слову 
			if(qindexList!=null)
			 for(String qindex:qindexList){
				 if(canexit)break;
				 List<String> resultThree=new ArrayList<String> ();
				
				 
						 if(qindex!=null){ 
						 Map<String, String> viewed=new HashMap<String, String> ();
						 
						 //StringBuffer sb=new StringBuffer();
						 
						 boolean doit=true;
						  int count=0; 
						   do {
							   if(canexit)break;
					       		count++;
					            if(count > 100) break;    
					            String preview=qindex;
					            //думали об этом Q уже?
					            if(!viewed.containsKey(qindex)){
					            
					            	
					            	
					            List<String> vi=new ArrayList<String> ();
					            vi.add(qindex);
					
					            		int icount=0; 
							            do {  
							            	 if(canexit)break;
							            	 
					//System.out.println("--------vi.get-----="+vi.toString());	 
					//System.out.println(icount+"--------vi.get-----="+vi.get(0));
							            String qdata=vi.get(0);
							            icount++;
							            //resultThree.add(qdata);

							            
							            
							            if(icount > 1000) break;  
							            if(!viewed.containsKey(qdata)){resultThree.add(qdata);
							            	List<String> listofinstance =null;
							            	List<String> listsubclassof =null;
							            	List<String> listotherprops =null;
							            	List<String> listhaspart =null;
							            	String label="";
							            	savetypeid =qdata;
						            		
							            	listofinstance = iilinstanceList.get(qdata);
						            		listsubclassof = iilsubclassofList.get(qdata);
						            		listotherprops = iilproperties.get(qdata);
						            		listhaspart =iilhaspart.get(qdata);
					//System.out.println(icount+"--------listofinstance-----="+listofinstance);
					//System.out.println(icount+"--------listsubclassof-----="+listsubclassof);
						            		
						            		
					            		
		//String values=getCategoryText(qdata,    iilmaincategory, iiltext);
		//sb.append(" "+qdata+"category["+values+"]");
					            		
						            		
							            	//if(iorclass==1){
							            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
							            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
							            		//}
						            		
						            		
							            	if(listhaspart!=null){
							            		
							            		  for(String ins:listhaspart){
							            			  vi.add(ins);
							            			  
							            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
								            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
								            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
							            			  
							            		  }
							            		}
						            		
						            	if(listotherprops!=null){
						            		
						            		  for(String ins:listotherprops){
						            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
						            			  
						            		  }
						            		}
							            if(listofinstance!=null){
								            for(String ins:listofinstance){
								            	//System.out.println("--------ofinstance-----="+ins);
								            	  vi.add(ins);
								            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
								            }
							            }
							            if(listsubclassof!=null){
								            for(String ins:listsubclassof){
								            	//System.out.println("--------subclassof-----="+ins);
								            	  vi.add(ins);
								            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
								            }
							            }
							            
						            		
//							            	if(listhaspart!=null){
//							            		
//							            		  for(String ins:listhaspart){
//							            			  vi.add(ins);
//							            			  
//							            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//								            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//								            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//							            			  
//							            		  }
//							            		}
//						            		
//						            	if(listotherprops!=null){
//						            		
//						            		  for(String ins:listotherprops){
//						            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//						            			  
//						            		  }
//						            		}
//							            if(listofinstance!=null){
//								            for(String ins:listofinstance){
//								            	//System.out.println("--------ofinstance-----="+ins);
//								            	  vi.add(ins);
//								            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
//								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//								            }
//							            }
//							            if(listsubclassof!=null){
//								            for(String ins:listsubclassof){
//								            	//System.out.println("--------subclassof-----="+ins);
//								            	  vi.add(ins);
//								            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
//								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//								            }
//							            }
							             //String valuesCategory=getCategoryText(qdata,    iilmaincategory, iiltext);
							             //returndata.put(qdata, ""+icount+"#"+valuesCategory);
							             vi.remove(qdata);
							            }
							            
							            
							            } while (vi.size()>0);
							            
					            
					
					            if(preview==qindex)doit=false;
					            
					            if(qindex==null)doit=false;
					            //returndata.put(qindex, ""+icount);
					            //String valuesCategory=getCategoryText(qindex,    iilmaincategory, iiltext);
					            //returndata.put(qindex, ""+icount+"u"+valuesCategory);
					            viewed.put(qindex, qindex);
					            }
					            
					        } while (doit);
						   
						   //itogo = sb.toString();
						 }
						 returndata.put(qindex, resultThree);
			 }
			 }
			
			 List<String> resultsavetypeid=new ArrayList<String> ();
			 resultsavetypeid.add(savetypeid);
			 returndata.put("1", resultsavetypeid);
			//returndata.put("2", itogo);
			   //res[0]=savetypeid;
			   //res[1]=itogo;
			return returndata;
		}
		
		
		
	    @Override
		public String searchRootRuleSingle(String cmd) {
	    String result="1";
	   
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();

		Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
		Map<String, String> morfema = fastMemoryModel.getMorfema();
		Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
	    try{ 
	    	
	    	//StringBuffer sbBuffer=new StringBuffer();
	    	String sbBuffer="";
	    	if(cmd!=null){
	    		if(!cmd.equals("")){
	    			
	    			
	    	
	    			 String nword=cmd;



                        boolean foundresult=false;
                        QResult z =new QResult();
                        String r ="";
                        
                        if(nonamesingle.containsKey(nword)){
//0. управляемые определения                         	
                        	List<String> qindexList = new  ArrayList<String>();
							//qindexList.add("Q1346360"); загрузка из ДБ
							qindexList.add(nonamesingle.get(nword));
                        	z = getQobjectSingle(nword, "1", qindexList, true);
                            r = z.getOut();
                            sbBuffer=r;
                        	
                        	
                        }else{
 //1. поиск в синонимах                       
                        try{  
                    	z = getQobjectSingle(nword, "1", null, false);
                        r = z.getOut();
                        sbBuffer=r;
                        foundresult=z.isFoundresult();
                        //System.out.println("START1="+nword+"\n");							
                        if(!foundresult){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
                        		   
							    String syn = iwordSynonims.get(nword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
									
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();

										 z = getQobjectSingle(newnword, "2", null, false);
				                         r = z.getOut();
				                         foundresult=z.isFoundresult();
				                         sbBuffer=r;
				                        
									}
	
								}
						}
			    		} catch (Exception e) {
			    			e.printStackTrace();
			    		}
//2. поиск в гиперонимах
                        try{	
						
						
						if(!foundresult){
	
							ArrayList tempdoList = null;
							tempdoList = iSymantex.get(nword);
	
							if (tempdoList != null) {
								int j = 0;
								for (Object value : tempdoList) {
									j++;
									String[] index = new String[2];
									index = (String[]) value;
	
									String iSymantexword = index[0];
									String itype = index[1];
									iSymantexword = iSymantexword.trim();

									// надо - выйти из цикла
									 //if(foundresult)break;

									 z = getQobjectSingle(iSymantexword, "3", null, false);
			                         r = z.getOut();
			                         foundresult=z.isFoundresult();
			                         sbBuffer=r;
			                        
									}
								}
						
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
//3. поиск в морфемах
                        
                        try{     
                        
							String m = morfema.get(nword);
							if (m != null) {

								if (m.contains("|")) {
									String[] mdata = m.split("\\|");
									int sizeb = mdata.length;
									for (int i = 0; i < sizeb; i++) {
										String single = mdata[i];

										try {
											String same = morfemawords.get(single);

											if (same != null) {

												if (same.contains("|")) {
													String[] msamedata = same.split("\\|");
													int sizec = msamedata.length;
													String wordform = "";
													for (int j = 0; j < sizec; j++) {
														String singleword = msamedata[j].trim()
																.toLowerCase();

														String wrdi = singleword
																.replaceAll("[^А-я]", "").trim();

														List<String> wordBaseForms = null;
														if (!"".equals(wrdi)) {
															wordBaseForms = luceneMorph.getMorphInfo(wrdi);
														}
														int sizer = 0;
														try {
															sizer = wordBaseForms.size();
														} catch (Exception e) {
														}

														if (sizer > 0) {

															StringBuilder sb = new StringBuilder();
															for (String s : wordBaseForms) {
																sb.append(s);
																sb.append(" #");
															}

															String ist = sb.toString();
															if (!ist.equals("") && ist != null) {
																wordform = ist;
															}

															if (wordform.contains("|")) {
																String[] myData = wordform.split("\\|");
																String inword = myData[0].trim();
																String imorf = myData[1];
																
																if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																	 //listnword.add(inword);
																	 z = getQobjectSingle(inword, "4", null, false);
											                         r = z.getOut();
											                         foundresult=z.isFoundresult();
											                         sbBuffer=r;
																}
															}
														}
													}

												}
											}

										} catch (Exception e) {
											e.printStackTrace();
										}

									}

								} else {

									try {
										String same = morfemawords.get(m);

										if (same != null) {
											if (same.contains("|")) {
												String[] msamedata = same.split("\\|");
												int sizec = msamedata.length;
												String wordform = "";
//если результат найден выйди из цикла морфем												
												for (int i = 0; i < sizec; i++) {
													String singleword = msamedata[i].trim()
															.toLowerCase();

													String wrdi = singleword.replaceAll("[^А-я]", "")
															.trim();

													List<String> wordBaseForms = null;
													if (!"".equals(wrdi)) {
														wordBaseForms = luceneMorph.getMorphInfo(wrdi);
													}

													int sizer = 0;
													try {
														sizer = wordBaseForms.size();
													} catch (Exception e) {
													}
//мень, не обрабатываются все результаты морфологии только первый
													if (sizer > 0) {

														StringBuilder sb = new StringBuilder();
														for (String s : wordBaseForms) {
															sb.append(s);
															sb.append(" #");
														}

														String ist = sb.toString();
														if (!ist.equals("") && ist != null) {
															wordform = ist;
														}

														if (wordform.contains("|")) {
															String[] myData = wordform.split("\\|");
															String inword = myData[0].trim();
															String imorf = myData[1];
															if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																//listnword.add(inword);
																z = getQobjectSingle(inword, "4", null, false);
										                        r = z.getOut();
										                        foundresult=z.isFoundresult();
										                        sbBuffer=r;
															}
														}
													}
												}

											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

								}
							}
                        	
                        } catch (Exception e) {
        	    			e.printStackTrace();
        	    		}  
                        
	    		}   
					
	    			
	    		}
	    	}
	    	result=sbBuffer;
	    }catch(Exception e){e.printStackTrace();}
		
	    return result;
	}
	    
	    
	    @Override
		public QResult searchRootRuleForCompress(String cmd) {
	    String result="1";
	   
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();

		Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
		Map<String, String> morfema = fastMemoryModel.getMorfema();
		Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
	    QResult z =new QResult();
	    
	    try{ 
	    	
	    	
	    	//String sbBuffer="";
	    	if(cmd!=null){
	    		if(!cmd.equals("")){
	    			
	    			
	    	
	    			 String nword=cmd;



                        boolean foundresult=false;
                        
                        String r ="";
                        
                        if(nonamesingle.containsKey(nword)){
//0. управляемые определения                         	
                        	List<String> qindexList = new  ArrayList<String>();
							//qindexList.add("Q1346360"); загрузка из ДБ
							qindexList.add(nonamesingle.get(nword));
                        	z = getQobjectForCompress(nword, "1", qindexList, true);
                            r = z.getOut();

                        }else{
 //1. поиск в синонимах                       
                        try{  
                    	z = getQobjectForCompress(nword, "1", null, false);
                        r = z.getOut();
                        //sbBuffer=r;
                        foundresult=z.isFoundresult();
                        //System.out.println("START1="+nword+"\n");							
                        if(!foundresult){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
                        		   
							    String syn = iwordSynonims.get(nword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
									
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();

										 z = getQobjectForCompress(newnword, "2", null, false);
				                         r = z.getOut();
				                         foundresult=z.isFoundresult();
				                        
				                        
									}
	
								}
						}
			    		} catch (Exception e) {
			    			e.printStackTrace();
			    		}
//2. поиск в гиперонимах
                        try{	
						
						
						if(!foundresult){
	
							ArrayList tempdoList = null;
							tempdoList = iSymantex.get(nword);
	
							if (tempdoList != null) {
								int j = 0;
								for (Object value : tempdoList) {
									j++;
									String[] index = new String[2];
									index = (String[]) value;
	
									String iSymantexword = index[0];
									String itype = index[1];
									iSymantexword = iSymantexword.trim();

									// надо - выйти из цикла
									 //if(foundresult)break;

									 z = getQobjectForCompress(iSymantexword, "3", null, false);
			                         r = z.getOut();
			                         foundresult=z.isFoundresult();
			                         
			                        
									}
								}
						
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
//3. поиск в морфемах
                        
                        try{     
                        
							String m = morfema.get(nword);
							if (m != null) {

								if (m.contains("|")) {
									String[] mdata = m.split("\\|");
									int sizeb = mdata.length;
									for (int i = 0; i < sizeb; i++) {
										String single = mdata[i];

										try {
											String same = morfemawords.get(single);

											if (same != null) {

												if (same.contains("|")) {
													String[] msamedata = same.split("\\|");
													int sizec = msamedata.length;
													String wordform = "";
													for (int j = 0; j < sizec; j++) {
														String singleword = msamedata[j].trim()
																.toLowerCase();

														String wrdi = singleword
																.replaceAll("[^А-я]", "").trim();

														List<String> wordBaseForms = null;
														if (!"".equals(wrdi)) {
															wordBaseForms = luceneMorph.getMorphInfo(wrdi);
														}
														int sizer = 0;
														try {
															sizer = wordBaseForms.size();
														} catch (Exception e) {
														}

														if (sizer > 0) {

															StringBuilder sb = new StringBuilder();
															for (String s : wordBaseForms) {
																sb.append(s);
																sb.append(" #");
															}

															String ist = sb.toString();
															if (!ist.equals("") && ist != null) {
																wordform = ist;
															}

															if (wordform.contains("|")) {
																String[] myData = wordform.split("\\|");
																String inword = myData[0].trim();
																String imorf = myData[1];
																
																if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																	 //listnword.add(inword);
																	 z = getQobjectForCompress(inword, "4", null, false);
											                         r = z.getOut();
											                         foundresult=z.isFoundresult();
											                        
																}
															}
														}
													}

												}
											}

										} catch (Exception e) {
											e.printStackTrace();
										}

									}

								} else {

									try {
										String same = morfemawords.get(m);

										if (same != null) {
											if (same.contains("|")) {
												String[] msamedata = same.split("\\|");
												int sizec = msamedata.length;
												String wordform = "";
//если результат найден выйди из цикла морфем												
												for (int i = 0; i < sizec; i++) {
													String singleword = msamedata[i].trim()
															.toLowerCase();

													String wrdi = singleword.replaceAll("[^А-я]", "")
															.trim();

													List<String> wordBaseForms = null;
													if (!"".equals(wrdi)) {
														wordBaseForms = luceneMorph.getMorphInfo(wrdi);
													}

													int sizer = 0;
													try {
														sizer = wordBaseForms.size();
													} catch (Exception e) {
													}
//мень, не обрабатываются все результаты морфологии только первый
													if (sizer > 0) {

														StringBuilder sb = new StringBuilder();
														for (String s : wordBaseForms) {
															sb.append(s);
															sb.append(" #");
														}

														String ist = sb.toString();
														if (!ist.equals("") && ist != null) {
															wordform = ist;
														}

														if (wordform.contains("|")) {
															String[] myData = wordform.split("\\|");
															String inword = myData[0].trim();
															String imorf = myData[1];
															if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																//listnword.add(inword);
																z = getQobjectForCompress(inword, "4", null, false);
										                        r = z.getOut();
										                        foundresult=z.isFoundresult();
										                       
															}
														}
													}
												}

											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

								}
							}
                        	
                        } catch (Exception e) {
        	    			e.printStackTrace();
        	    		}  
                        
	    		}   
					
	    			
	    		}
	    	}
	    	
	    }catch(Exception e){e.printStackTrace();}
		
	    return z;
	}
		
		QResult getQobjectSingle(String nword, String type, List<String> qindexListExt, boolean ext){
	 	    QResult result=new QResult();
	 	    StringBuffer sbBuffer=new StringBuffer();
	 	    sbBuffer.append("");
		    boolean foundresult=false;
		      result.setOut("");
			  result.setFoundresult(foundresult);
		    try{ 
		  
		    Map<String, List> iilobjectsList = fastMemoryModel.getIilobjects();
		    Map<String, List> iilinstanceList = fastMemoryModel.getOfinstance();
			Map<String, List> iilsubclassofList = fastMemoryModel.getIilsubclassof();
			Map<String, List> iilmaincategory = fastMemoryModel.getMaincategory();
			Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
			Map<String, List> iiltext = fastMemoryModel.getIilobjectsToText();
			Map<String, List> iilproperties = fastMemoryModel.getIilotherprops();
			 List<String> qindexList;
			 if(ext)
			 qindexList = qindexListExt;
			 else
			 qindexList = iilobjectsList.get(nword);
			 
			 
			    Map<String, List> returtQ = new HashMap<String, List>();

				returtQ = searchMainObjectSingle(qindexList, iilobjectsList, iilinstanceList,
						iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);

				List<String> resultsavetypeid=new ArrayList<String> ();
				resultsavetypeid=returtQ.get("1");
				String savetype="";
				for(String savetypeid:resultsavetypeid){
					foundresult=true;
					savetype=savetypeid;
				}
				  result.setOut(savetype);
				  result.setFoundresult(foundresult);
				  
		        } catch (Exception e) {
					e.printStackTrace();
				}
		 
		return result;
	}
		
		
		public Map<String, List> searchMainObjectSingle(List<String> qindexList, Map<String, List> iilobjectsList,
				Map<String, List> iilinstanceList, Map<String, List> iilsubclassofList, Map<String, List> iilproperties, Map<String, List> iilmaincategory, Map<String, List> iiltext, Map<String, List> iilhaspart) {
			
			


			Map<String, List> returndata=new HashMap<String, List>();
			 
			 
			Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
			 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
			 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
			   String itypeid ="";
			   String icategoryid ="";
			   String savetypeid ="none";
			   String savecategoryid ="";
			   String itogo = "";
			   String[] res=new String[2];
			   res[0]="";
			   res[1]="";
			int morethatone=0;   
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			if(sgeo==false&&shuman==false&&sorg==false){
		//----------------------------Q---------------------------- 
		//https://dumps.wikimedia.org/wikidatawiki/entities/	
			boolean canexit=false;
			
			//список Q полученных по слову 
			
			if(qindexList!=null)
			 for(String qindex:qindexList){
				 if(canexit)break;
				 List<String> resultThree=new ArrayList<String> ();
				
				 
						 if(qindex!=null){ 
						 Map<String, String> viewed=new HashMap<String, String> ();

						 boolean doit=true;
						  int count=0; 
						   do {
							   if(canexit)break;
					       		count++;
					            if(count > 100) break;    
					            String preview=qindex;
					            //думали об этом Q уже?
					            if(!viewed.containsKey(qindex)){
					            
					            	
					            	
					            List<String> vi=new ArrayList<String> ();
					            vi.add(qindex);
					
					            		int icount=0; 
							            do {  
							            	 if(canexit)break;

							            String qdata=vi.get(0);
							            savetypeid =qdata;
							            icount++;
							            //resultThree.add(qdata);

							            if(icount > 1000) break;  
							            if(!viewed.containsKey(qdata)){resultThree.add(qdata);
							            	List<String> listofinstance =null;
							            	List<String> listsubclassof =null;
							            	List<String> listotherprops =null;
							            	List<String> listhaspart =null;
							            	String label="";
							            	
						            		
							            	listofinstance = iilinstanceList.get(qdata);
						            		listsubclassof = iilsubclassofList.get(qdata);
						            		listotherprops = iilproperties.get(qdata);
						            		listhaspart = iilhaspart.get(qdata);
						            		
						            		
									 if (listhaspart != null) {

											for (String ins : listhaspart) {
												vi.add(ins);

												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													canexit=true;
													morethatone=1;
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													canexit=true;
													morethatone=1;
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													canexit=true;
													morethatone=1;
													break;
												}

											}
										}
						            		
						            	if(listotherprops!=null){
						            		
						            		  for(String ins:listotherprops){
						            			     if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";   canexit=true; morethatone=1; break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  canexit=true; morethatone=1; break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";   canexit=true; morethatone=1; break;}
						            			  
						            		  }
						            		}
						            	
							            if(listofinstance!=null){
								            for(String ins:listofinstance){
								            	
								            	  vi.add(ins);
								            	 
								            	     if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  canexit=true; morethatone=1; break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; morethatone=1; break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  canexit=true; morethatone=1; break;}
								            }
							            }
							            if(listsubclassof!=null){
								            for(String ins:listsubclassof){
								            
								            	  vi.add(ins);
								            	 
								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";     canexit=true; morethatone=1; break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; morethatone=1; break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  canexit=true; morethatone=1; break;}
								            }
							            }

							             vi.remove(qdata);
							            }
							            
							            
							            } while (vi.size()>0);
							            
					            
					
					            if(preview==qindex)doit=false;
					            
					            if(qindex==null)doit=false;
					            viewed.put(qindex, qindex);
					            }
					            
					        } while (doit);
						 }
						 
						 if(resultThree.size()>1){ morethatone=1;}
						 
						 //returndata.put(qindex, resultThree);
			 }
			 }
			
			 List<String> resultsavetypeid=new ArrayList<String> ();
			 resultsavetypeid.add(savetypeid);
			 if(morethatone>0){resultsavetypeid.add(savetypeid);}else{resultsavetypeid.add("none");}
			 returndata.put("1", resultsavetypeid);
			return returndata;
		}
		
		
		
		public Map<String, List> searchMainObjectForCompress(List<String> qindexList, Map<String, List> iilobjectsList,
				Map<String, List> iilinstanceList, Map<String, List> iilsubclassofList, Map<String, List> iilproperties, Map<String, List> iilmaincategory, Map<String, List> iiltext, Map<String, List> iilhaspart) {
			
			


			Map<String, List> returndata=new HashMap<String, List>();
			 
			 
			Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
			 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
			 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
			   String itypeid ="";
			   String icategoryid ="";
			   String savetypeid ="none";
			   String savecategoryid ="";
			   String itogo = "";
			   String[] res=new String[2];
			   res[0]="";
			   res[1]="";
			   
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			if(sgeo==false&&shuman==false&&sorg==false){
		//----------------------------Q---------------------------- 
		   
			boolean canexit=false;
			//список Q полученных по слову 
			if(qindexList!=null)
			 for(String qindex:qindexList){ 
				 int morethatone=0;
				 
				 if(canexit)break;
				 List<String> resultThree=new ArrayList<String> ();
				
				 
						 if(qindex!=null){ 
						 Map<String, String> viewed=new HashMap<String, String> ();
					
						 boolean doit=true;
						  int count=0; 
						   do {
							   if(canexit)break;
					       		count++;
					            if(count > 100) break;    
					            String preview=qindex;
					            //думали об этом Q уже?
					            if(!viewed.containsKey(qindex)){
					            
					            	
					            	
					            List<String> vi=new ArrayList<String> ();
					            vi.add(qindex);
					
					            		int icount=0; 
							            do {  
							            	 if(canexit)break;
							            	 	String qdata=vi.get(0);
							            	 		icount++;
//------>>>>>>>>>>//resultThree.add(qdata);

							            
							            
							            if(icount > 1000) break;  
							            if(!viewed.containsKey(qdata)){
							            	
							            	resultThree.add(qdata);
							            	
							            	List<String> listofinstance =null;
							            	List<String> listsubclassof =null;
							            	List<String> listotherprops =null;
							            	List<String> listhaspart =null;
							            	String label="";
							            	
//присваиваем тоже самое значение (единичное первое найденное и возвращаем положительный результат поиска в иерархии)	
							            	if(!qindex.equals(qdata)){
							            		savetypeid = qdata;
							            	}
							            	
						            		
							            	listofinstance = iilinstanceList.get(qdata);
						            		listsubclassof = iilsubclassofList.get(qdata);
						            		listotherprops = iilproperties.get(qdata);
						            		listhaspart =iilhaspart.get(qdata);
						            		
						            		
							            if(listhaspart!=null){
							            		
							            		  for(String ins:listhaspart){
							            			  vi.add(ins);
							            			  
							            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
								            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
								            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
							            			  
							            		  }
							            }
						            		
						            	if(listotherprops!=null){
						            		
						            		  for(String ins:listotherprops){
						            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
						            			  
						            		  }
						            		}
							            if(listofinstance!=null){
								            for(String ins:listofinstance){
								            
								            	  vi.add(ins);
								            	
								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
								            }
							            }
							            if(listsubclassof!=null){
								            for(String ins:listsubclassof){
								            	
								            	  vi.add(ins);
								            	 
								            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
								            }
							            }

							             vi.remove(qdata);
							            }
							            
							            
							            } while (vi.size()>0);
							            
					            
					
					            if(preview==qindex)doit=false;
					            
					            if(qindex==null)doit=false;
					            viewed.put(qindex, qindex);
					            }
					            
					        } while (doit);
						   
						  
						 }
						//контроль качества 
						//присваиваем тоже самое значение (единичное первое найденное и возвращаем положительный результат поиска в иерархии)	
						 if(resultThree.size()>2){
						 returndata.put(qindex, resultThree);
						 }else{
							 savetypeid ="none";	 
						 }
			 }
			 }
			
			 List<String> resultsavetypeid=new ArrayList<String> ();
			 resultsavetypeid.add(savetypeid);
			 returndata.put("1", resultsavetypeid);
			return returndata;
		}
		
		@Override
		public String emotionTest(String a, int exec){
			
			if(exec==53){
				 Map<String, String> iwordPositive=fastMemoryModel.getWordPositive();
				 Map<String, String> iwordNegative=fastMemoryModel.getWordNegative();
				 Map<String, String> morfema=fastMemoryModel.getMorfema();
				 Map<String, String> morfemawords=fastMemoryModel.getMorfemaWords();
				 LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
				 
				 
				 List<String> wordBaseForms = luceneMorph.getMorphInfo(a);
				 String wordform="";
				 String nword ="";
					int sizer=0;
					try{sizer=wordBaseForms.size();}catch(Exception e){}
					
					if(sizer>0){
					
					StringBuilder sb = new StringBuilder();
					for (String s : wordBaseForms)
					{
					    sb.append(s);
					    sb.append(" #");
					}
					
					
					String ist = sb.toString();
					if(!ist.equals("")&&ist!=null){
						wordform=ist;
					}
					
				
				
					if(wordform.contains("|")){
						   String[] myData = wordform.split("\\|");
						   nword =myData[0].trim();
						   String morf =myData[1];}
					}
					
				   String m= morfema.get(nword); 
				   StringBuffer array = new StringBuffer();
				   if(m.contains("|")){
					   String[] mdata = m.split("\\|");   
					    int sizeb = mdata.length;
						for(int i=0;i<sizeb;i++){
							String single=mdata[i];
							 
							
							try{ String same=morfemawords.get(single);
							     array.append("words:"+single+" ["+same+"] because:");
							     
							     String pos=iwordPositive.get(single);
							     if(pos!=null){
							    	 array.append(nword+"["+single+"]").append("-POS ");
							    	 //wordjson.put("pos", true);
							    	 //synindex[0]=single;
							     }
							     }catch(Exception e){e.printStackTrace();}
							     
							try{ 
								     String neg=iwordNegative.get(single);
								     if(neg!=null){
								    	 array.append(nword+"["+single+"]").append("-NEG ");
								    	 //synindex[1]=single;
								     }
							}catch(Exception e){e.printStackTrace();} 
						}
						//wordjson.put("emo", array.toString());
						//synindex[2]= array.toString();
				   }else{
					   
					   try{ 
					     String same=morfemawords.get(m);
					     array.append("words:"+m+" ["+same+"] because:");
					     
						     String pos=iwordPositive.get(m);
						     if(pos!=null){
						    	 array.append(nword+"["+m+"]").append("-POS ");
						    	 //wordjson.put("emopos", m);
						    	 //wordjson.put("pos", true);
						    	 //synindex[0]=m;
						     }
						     }catch(Exception e){e.printStackTrace();}
						     
						try{ 
							     String neg=iwordNegative.get(m);
							     if(neg!=null){
							    	 array.append(nword+"["+m+"]").append("-NEG ");
							    	 //wordjson.put("emoneg", m);
							    	 //wordjson.put("neg", true);
							    	// synindex[1]=m;
							     }
						}catch(Exception e){e.printStackTrace();} 
				   }
				   
				     String itogo=array.toString();
					
					 return itogo;
			}
			if(exec==55){
				 Map<String, String> iwordPositive=fastMemoryModel.getWordPositive();
				 Map<String, String> iwordNegative=fastMemoryModel.getWordNegative();
				 StringBuilder sb = new StringBuilder();
				 for (Map.Entry<String, String> entry : iwordPositive.entrySet())
				 {
					 String key=entry.getKey();
					 String value=entry.getValue();
					 if(iwordNegative.containsKey(key)){
						 sb.append("same["+key+"]"+value+"-"+iwordNegative.get(key)+"#");	 
					 }
				 
				 }
				 String itogo=sb.toString();
				 
				 return itogo;	
			}
			if(exec==52){
				Map<String, ArrayList> iSymantex=nameFinderModelMEDAO.getSymantexhashMap();
				 StringBuffer sb=new StringBuffer();
				String lowerStr=a.toLowerCase();	
				ArrayList tempdoList = null;
		        tempdoList = iSymantex.get(lowerStr); int c=0;
			    if(tempdoList !=null){
			    	for (Object value: tempdoList) {c++;
			    		
			    		  String[] index = new String[2];  
			        	  index =(String[]) value;
			        	  
			        	    //===================================ГИПЕРОНИМЫ===СИНОНИМЫ===========									        	  
			        	  	sb.append(lowerStr).append("-").append(index[0]+" Type : "+index[1]);
			        		  
			            
			    	}
			    }
    
		     String itogo=sb.toString();
			
			 return itogo;
				
			}
			if(exec==70){
			   StringBuffer sbBuffer=new StringBuffer(); 
			   sbBuffer.append("begin: ");
			   try{ 
			    	Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
					Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
					Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
					Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
					Map<String, String> morfema = fastMemoryModel.getMorfema();
					Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
					LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
			    	String lowerStr=a.toLowerCase();
			    	
			    	if(a!=null){
			    		if(!a.equals("")){
			    			
			    			
			    			
						
			    			 String nword=a;
			    			
		//System.out.println("nword="+nword+"\n");	    			 
			    			 //nword - найти Q контекст
			    			 //searchRootObject 
			    			 //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
			    			 //записать весь лог


		                        boolean foundresult=false;
		                     
		                        String r ="";
		                        
		                        if(nonamesingle.containsKey(nword)){
		//0. управляемые определения                         	
		                        	
		                        	
		                        }else{
		 //1. поиск в синонимах                       
		                        try{  
		                 
		                        if(!foundresult){
									    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
		                        		   
									    String syn = iwordSynonims.get(nword);
										if (syn != null) {
											String[] newwords;
											if (syn.contains("/")) {
			
												newwords = syn.split("\\/");
			
											}
											
											if (syn.contains(",")) {
			
												newwords = syn.split(",");
			
											} else {
			
												String[] synindex = new String[1];
												synindex[0] = syn;
												newwords = synindex;
											}
											int siz = newwords.length;
											
											for (int ik = 0; ik < siz; ik++) {
												 String newnword = newwords[ik].trim();
						                         sbBuffer.append(newnword).append("=[type 2]");
						                        
											}
			
										}
								}
					    		} catch (Exception e) {
					    			e.printStackTrace();
					    		}
		//2. поиск в гиперонимах
		                        try{	
								
								
								if(!foundresult){
			
									ArrayList tempdoList = null;
									tempdoList = iSymantex.get(nword);
			
									if (tempdoList != null) {
										int j = 0;
										for (Object value : tempdoList) {
											j++;
											String[] index = new String[2];
											index = (String[]) value;
			
											String iSymantexword = index[0];
											String itype = index[1];
											iSymantexword = iSymantexword.trim();
					                        sbBuffer.append(iSymantexword).append("=[type 3]");
					                        
					                        
											}
										}
								
								}
			    		} catch (Exception e) {
			    			e.printStackTrace();
			    		}
		//3. поиск в морфемах
		                        
		                        try{     
		                        
									String m = morfema.get(nword);
									if (m != null) {

										if (m.contains("|")) {
											String[] mdata = m.split("\\|");
											int sizeb = mdata.length;
											for (int i = 0; i < sizeb; i++) {
												String single = mdata[i];

												try {
													String same = morfemawords.get(single);

													if (same != null) {

														if (same.contains("|")) {
															String[] msamedata = same.split("\\|");
															int sizec = msamedata.length;
															String wordform = "";
															for (int j = 0; j < sizec; j++) {
																String singleword = msamedata[j].trim()
																		.toLowerCase();

																String wrdi = singleword
																		.replaceAll("[^А-я]", "").trim();

																List<String> wordBaseForms = null;
																if (!"".equals(wrdi)) {
																	wordBaseForms = luceneMorph.getMorphInfo(wrdi);
																}
																int sizer = 0;
																try {
																	sizer = wordBaseForms.size();
																} catch (Exception e) {
																}

																if (sizer > 0) {

																	StringBuilder sb = new StringBuilder();
																	for (String s : wordBaseForms) {
																		sb.append(s);
																		sb.append(" #");
																	}

																	String ist = sb.toString();
																	if (!ist.equals("") && ist != null) {
																		wordform = ist;
																	}

																	if (wordform.contains("|")) {
																		String[] myData = wordform.split("\\|");
																		String inword = myData[0].trim();
																		String imorf = myData[1];
																		
																		if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																		
																			
													                         sbBuffer.append(inword).append("=[type 4]");
																		}
																	}
																}
															}

														}
													}

												} catch (Exception e) {
													e.printStackTrace();
												}

											}

										} else {

											try {
												String same = morfemawords.get(m);

												if (same != null) {
													if (same.contains("|")) {
														String[] msamedata = same.split("\\|");
														int sizec = msamedata.length;
														String wordform = "";
		//если результат найден выйди из цикла морфем												
														for (int i = 0; i < sizec; i++) {
															String singleword = msamedata[i].trim()
																	.toLowerCase();

															String wrdi = singleword.replaceAll("[^А-я]", "")
																	.trim();

															List<String> wordBaseForms = null;
															if (!"".equals(wrdi)) {
																wordBaseForms = luceneMorph.getMorphInfo(wrdi);
															}

															int sizer = 0;
															try {
																sizer = wordBaseForms.size();
															} catch (Exception e) {
															}
		//мень, не обрабатываются все результаты морфологии только первый
															if (sizer > 0) {

																StringBuilder sb = new StringBuilder();
																for (String s : wordBaseForms) {
																	sb.append(s);
																	sb.append(" #");
																}

																String ist = sb.toString();
																if (!ist.equals("") && ist != null) {
																	wordform = ist;
																}

																if (wordform.contains("|")) {
																	String[] myData = wordform.split("\\|");
																	String inword = myData[0].trim();
																	String imorf = myData[1];
																	if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																		
												                        sbBuffer.append(inword).append("=[type 4]");
																	}
																}
															}
														}

													}
												}
											} catch (Exception e) {
												e.printStackTrace();
											}

										}
									}
		                        	
		                        } catch (Exception e) {
		        	    			e.printStackTrace();
		        	    		}  
		                        
			    		}   

			    			 
			    			
			    		}
			    	}
			    	
			    	
			    	
			    }catch(Exception e){e.printStackTrace();}
			    String result=sbBuffer.toString();
			    return result;
			}
			if(exec==100){
				StringBuffer sbBuffer=new StringBuffer(); 
				sbBuffer.append("begin: ");
				 Map<String, List> iilpropertiesList=fastMemoryModel.getIilproperties();
				 List<String> iilproperties =null;
				 String nword=a;
										 iilproperties =iilpropertiesList.get(nword);
										 					            	if(iilproperties!=null){
															            		  for(String ins:iilproperties){
															            			  
															            			  sbBuffer.append("P="+ins);
															            		  }
															            		}
										 					            	
										 					            	String result=sbBuffer.toString();
										 								    return result;
			}
			if(exec==101){
				StringBuffer sbBuffer=new StringBuffer(); 
				sbBuffer.append("begin: ");
				 Map<String, List> iilpropertiesList=fastMemoryModel.getIilproperties();
				 List<String> iilproperties =null;
				 int s=0;
				 for(Entry<String, List> entry:iilpropertiesList.entrySet()){
					 s++;
						String key = entry.getKey();
						List<String> values = entry.getValue();
						sbBuffer.append("key="+key);
						for(String ins:values){										            			  
							sbBuffer.append("value="+ins);
						}
				 if(s>100)break;											            		
				 }
				 String result=sbBuffer.toString();
				 return result;
				 
			}
			if(exec==200){
				StringBuffer sbBuffer=new StringBuffer(); 
				sbBuffer.append("begin: ");
				 Map<String, List<String>> iilpropertiesList=fastMemoryModel.getIilQPKeyValue();
				 List<String> iilproperties =null;
				 String nword=a;
										 iilproperties =iilpropertiesList.get(nword);
										 					            	if(iilproperties!=null){
															            		  for(String ins:iilproperties){
															            			  
															            			  sbBuffer.append("QP="+ins+"#");
															            		  }
															            		}
										 					            	
										 					            	String result=sbBuffer.toString();
										 								    return result;
			}
			return null;
		}
		
		
		boolean isPresent(String serchword, String text){
			
			 String regex = "\\b"+serchword+"\\b";
		        Pattern pattern = Pattern.compile(regex);
		        Matcher matcher = pattern.matcher(text);
		    
		        while(matcher.find() == true){
		        	
		        	return true;
		        }
				return false;
		}
		
		
	    @Override
		public String searchRootRuleAVBProp(String cmd) {
	    String result="1";
	   
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
		Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
		Map<String, List> adjectiveNoun =nameFinderModelMEDAO.getadjectiveNounMap();
		
		
		
		Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
		Map<String, String> morfema = fastMemoryModel.getMorfema();
		Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
	    try{ 
	    	
	    	StringBuffer sbBuffer=new StringBuffer();
	    	if(cmd!=null){
	    		if(!cmd.equals("")){
	    			
	    			
	    			 String[] objectidData = cmd.split("\\|");
				
	    			 String nwordd=objectidData[0];
	    			 
	    			 List<String> listwords = new ArrayList<String>();
	    			 
	    			 List lwords = adjectiveNoun.get(nwordd);
	    			 if(lwords.size()>0){
	    				 listwords=lwords;
	    			 }else{
	    				 listwords.add(nwordd);
	    				 
	    			 }
	    			 
	    			 for(String nword:listwords){
	    			 //String vword=objectidData[1];
//System.out.println("nword="+nword+"\n");	    			 
	    			 //nword - найти Q контекст
	    			 //searchRootObject 
	    			 //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
	    			 //записать весь лог


                        boolean foundresult=false;
                        QResult z =new QResult();
                        String r ="";
                        
                        if(nonamesingle.containsKey(nword)){
//0. управляемые определения                         	
                        	List<String> qindexList = new  ArrayList<String>();
							//qindexList.add("Q1346360"); загрузка из ДБ
							qindexList.add(nonamesingle.get(nword));
                        	z = getQobjects(nword, "1", qindexList, true);
                            r = z.getOut();
                            sbBuffer.append(r);
                        	
                        	
                        }else{
 //1. поиск в синонимах                       
                        try{  
                    	z = getQobjects(nword, "1", null, false);
                        r = z.getOut();
                        sbBuffer.append(r);
                        foundresult=z.isFoundresult();
                        //System.out.println("START1="+nword+"\n");							
                        if(!foundresult){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
                        		   
							    String syn = iwordSynonims.get(nword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
									
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 if(foundresult)break;
										 String newnword = newwords[ik].trim();

										 z = getQobjects(newnword, "2", null, false);
				                         r = z.getOut();
				                         foundresult=z.isFoundresult();
				                         sbBuffer.append(r);
				                        
									}
	
								}
						}
			    		} catch (Exception e) {
			    			e.printStackTrace();
			    		}
//2. поиск в гиперонимах
                        try{	
						
						
						if(!foundresult){
	
							ArrayList tempdoList = null;
							tempdoList = iSymantex.get(nword);
	
							if (tempdoList != null) {
								int j = 0;
								for (Object value : tempdoList) {
									j++;
									String[] index = new String[2];
									index = (String[]) value;
	
									String iSymantexword = index[0];
									String itype = index[1];
									iSymantexword = iSymantexword.trim();

									// надо - выйти из цикла
									 if(foundresult)break;

									 z = getQobjects(iSymantexword, "3", null, false);
			                         r = z.getOut();
			                         foundresult=z.isFoundresult();
			                         sbBuffer.append(r);
			                        
									}
								}
						
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
//3. поиск в морфемах
                        /*
                        try{     
                        
							String m = morfema.get(nword);
							if (m != null) {

								if (m.contains("|")) {
									String[] mdata = m.split("\\|");
									int sizeb = mdata.length;
									for (int i = 0; i < sizeb; i++) {
										String single = mdata[i];

										try {
											String same = morfemawords.get(single);

											if (same != null) {

												if (same.contains("|")) {
													String[] msamedata = same.split("\\|");
													int sizec = msamedata.length;
													String wordform = "";
													for (int j = 0; j < sizec; j++) {
														String singleword = msamedata[j].trim()
																.toLowerCase();

														String wrdi = singleword
																.replaceAll("[^А-я]", "").trim();

														List<String> wordBaseForms = null;
														if (!"".equals(wrdi)) {
															wordBaseForms = luceneMorph.getMorphInfo(wrdi);
														}
														int sizer = 0;
														try {
															sizer = wordBaseForms.size();
														} catch (Exception e) {
														}

														if (sizer > 0) {

															StringBuilder sb = new StringBuilder();
															for (String s : wordBaseForms) {
																sb.append(s);
																sb.append(" #");
															}

															String ist = sb.toString();
															if (!ist.equals("") && ist != null) {
																wordform = ist;
															}

															if (wordform.contains("|")) {
																String[] myData = wordform.split("\\|");
																String inword = myData[0].trim();
																String imorf = myData[1];
																
																if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																	 //listnword.add(inword);
																	 z = getQobjects(inword, "4", null, false);
											                         r = z.getOut();
											                         //foundresult=z.isFoundresult();
											                         sbBuffer.append(r);
																}
															}
														}
													}

												}
											}

										} catch (Exception e) {
											e.printStackTrace();
										}

									}

								} else {

									try {
										String same = morfemawords.get(m);

										if (same != null) {
											if (same.contains("|")) {
												String[] msamedata = same.split("\\|");
												int sizec = msamedata.length;
												String wordform = "";
//если результат найден выйди из цикла морфем												
												for (int i = 0; i < sizec; i++) {
													String singleword = msamedata[i].trim()
															.toLowerCase();

													String wrdi = singleword.replaceAll("[^А-я]", "")
															.trim();

													List<String> wordBaseForms = null;
													if (!"".equals(wrdi)) {
														wordBaseForms = luceneMorph.getMorphInfo(wrdi);
													}

													int sizer = 0;
													try {
														sizer = wordBaseForms.size();
													} catch (Exception e) {
													}
//мень, не обрабатываются все результаты морфологии только первый
													if (sizer > 0) {

														StringBuilder sb = new StringBuilder();
														for (String s : wordBaseForms) {
															sb.append(s);
															sb.append(" #");
														}

														String ist = sb.toString();
														if (!ist.equals("") && ist != null) {
															wordform = ist;
														}

														if (wordform.contains("|")) {
															String[] myData = wordform.split("\\|");
															String inword = myData[0].trim();
															String imorf = myData[1];
															if(isPresent("( С жр)|( С мр)|( С ср)",imorf)){
																//listnword.add(inword);
																z = getQobjects(inword, "4", null, false);
										                        r = z.getOut();
										                        //foundresult=z.isFoundresult();
										                        sbBuffer.append(r);
															}
														}
													}
												}

											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

								}
							}
                        	
                        } catch (Exception e) {
        	    			e.printStackTrace();
        	    		}  
                        */
                        
	    		}   
                        /*
					//vword - найти контекст глаголов 
					boolean foundverb=false;
					 sbBuffer.append(" [verb=").append(vword).append("]");
					try{
					 String varbcat = verbglobalcategory.get(vword);
					 if(varbcat!=null){
					 if(!varbcat.equals("")){	 
						 //foundverb=true;
						 sbBuffer.append(" [varbcat=").append(varbcat).append("]");
					 }
					 }
					 
					 
		    		} catch (Exception e) {
		    			e.printStackTrace();
		    		}
	    			 
	    			 
					try{
                        if(!foundverb){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
							   
							    String syn = iwordSynonims.get(vword);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
	
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 //if(foundresult)break;
										 String newnword = newwords[ik].trim();
											try{
												 String varbcat = verbglobalcategory.get(newnword);
												 if(varbcat!=null){
												 if(!varbcat.equals("")){	 
													 //foundverb=true;
													 sbBuffer.append("["+newnword+"] [synonims=").append(varbcat).append("]");
												 }
												 }
												 
												 
									    		} catch (Exception e) {
									    			e.printStackTrace();
									    		}
								    			 
				                        
									}
								}
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
					
					try{	if(!foundverb){
						
						ArrayList tempdoList = null;
						tempdoList = iSymantex.get(vword);
						if (tempdoList != null) {
							int j = 0;
							for (Object value : tempdoList) {
								j++;
								String[] index = new String[2];
								index = (String[]) value;

								String iSymantexword = index[0];
								String itype = index[1];
								iSymantexword = iSymantexword.trim();
								// надо - выйти из цикла
								
								try{
									 String varbcat = verbglobalcategory.get(iSymantexword);
									 if(varbcat!=null){
									 if(!varbcat.equals("")){	 
										 //foundverb=true;
										 sbBuffer.append("["+iSymantexword+"] [synonims=").append(varbcat).append("]");
									 }
									 }
									 
									 
						    		} catch (Exception e) {
						    			e.printStackTrace();
						    		}
		                       

								}
							}
					}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
	    			*/ 
	    			 
	    			
	    		}}
	    	}
	    	result=sbBuffer.toString();
	    }catch(Exception e){e.printStackTrace();}
		
	    return result;
	}

}
