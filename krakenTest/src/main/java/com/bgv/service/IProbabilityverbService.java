package com.bgv.service;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Horosho;
import com.bgv.entity.Ploho;
import com.bgv.entity.Probabilitybwa;
import com.bgv.entity.Probabilitycounter;
import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbexpl;
import com.bgv.entity.Probabilityverbtrip;
import com.bgv.entity.Probabilityverbtripqq;
import com.bgv.entity.Probabilityverbtrippp;
import com.bgv.entity.Probabilityverbtripqcq;


public interface IProbabilityverbService {
	boolean addProbabilitycounter(Probabilityverb counter);

	List<Probabilityverb> getAllByPage(int pageSize, int pageNumber, Timestamp dateupdate);

	Long countText();

	void getLarge();

	void getLargeUpdate();

	boolean addProbabilitycounterTriplet(Probabilityverbtrip counter);

	void getLargeTriplet();

	boolean addProbabilitycounterTripletqq(Probabilityverbtripqq counter);

	void addProbabilitycounterTripletPredlog(Probabilityverbtrippp counter);

	void addProbabilityPloho(Ploho counter);

	void addProbabilityHorosho(Horosho counter);
	void addProbabilitycounterExpl(Probabilityverbexpl counter);

	void addProbabilitycounterTripletqcq(Probabilityverbtripqcq countern);

	void addProbabilitycounterProperty(Probabilitybwa counter);
}
