package com.bgv.service;

import com.bgv.practice.QResult;

public interface ITESTService {

	String searchRootRule(String cmd);

	String emotionTest(String a, int exec);

	String searchRootRuleSingle(String cmd);

	QResult searchRootRuleForCompress(String cmd);

	String searchRootRuleAVBProp(String cmd);

}
