package com.bgv.service;


import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.dao.IProbabilitydirtyDAO;
import com.bgv.entity.Probabilitydirty;



@Service
public class ProbabilitydirtyService implements IProbabilitydirtyService{
	
	@Autowired
	private IProbabilitydirtyDAO probability;

	@Override
	public boolean addProbabilitydirty(Probabilitydirty dirty) {
		probability.addProbabilitydirty(dirty);
		return false;
	}

	@Override
	public List<Probabilitydirty> getAlldirtytext() {
		return probability.getAlldirtytext();
		
	}

	@Override
	public void deleteText() {
		probability.deleteText();
		
	}

	@Override
	public void deleteText(Timestamp ourJavaTimestampObject) {
		probability.deleteText(ourJavaTimestampObject);
		
	}

	@Override
	public List<Probabilitydirty> getAllDocuments(Timestamp ourJavaTimestampObject) {
		return probability.getAllDocuments(ourJavaTimestampObject);
	}

	@Override
	public Long countText(Timestamp dateupdate) {
		return probability.countText(dateupdate);
		
	}

	@Override
	public List<Probabilitydirty> getAllByPage(int ip, int pageSize, Timestamp ourJavaTimestampObject) {
		return probability.getAllByPage(pageSize, ip, ourJavaTimestampObject);
		
	}
	
	

}
