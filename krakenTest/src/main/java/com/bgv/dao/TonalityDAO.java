package com.bgv.dao;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Morfematon;
import com.bgv.entity.Negative;
import com.bgv.entity.Positive;




@Transactional
@Repository
public class TonalityDAO implements ITonality {
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	@SuppressWarnings("unchecked")
	
	 @Override
	 public void getLargeNegative() {
		 Map<String, Integer> ilemmanounverbdb= new HashMap<String, Integer>();
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 ScrollableResults scResults = session.createCriteria(Negative.class)
	             .scroll(ScrollMode.FORWARD_ONLY);
					while(scResults.next()) {
						Negative dirty = (Negative)scResults.get(0);
						/*
						 String noun=dirty.getNounname().toLowerCase();
						 String verb=dirty.getVerbname().toLowerCase();
						   int tot = dirty.getTotalvalue();
						 
						  if(ilemmanounverbdb.containsKey(noun+" "+verb)){
							int tots=ilemmanounverbdb.get(noun+" "+verb);
							
							ilemmanounverbdb.put(noun+" "+verb, tot+tots);
							
						  }else ilemmanounverbdb.put(noun+" "+verb, tot);
						*/
					}
					
					//fastMemoryModel.cleareLemaNounAndVerb();
					//fastMemoryModel.setLemmanounverbdb(ilemmanounverbdb);
	    }	
	

	@SuppressWarnings("unchecked")
	@Override
	public boolean addTonality(String word, String nword, String type) {
		 int al=0;
		 int vl=0;
		 
		 
		  try{al=word.length();}catch (Exception e) {}
		  try{vl=nword.length();}catch (Exception e) {}
		
		  
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql ="";
		 if(type.equals("positive")){
		 
		 Query q = null;
		 if(al>1&&vl>1){
		     hql = "FROM Positive where word = :word AND nword = :nword";
		     q = session.createQuery(hql).setParameter("word", word).setParameter("nword", nword);
		 }
		 else if(al>1&&vl<1){
			 hql = "FROM Positive where word = :word AND nword = :nword";
	         q = session.createQuery(hql).setParameter("word", word).setParameter("nword", word);
		 }
		
		 List<Positive> persons = q.list();
		
		 if(persons.size() > 0){
			for(Positive mr:persons){
				Positive mrin=new Positive();
				mrin.setId(mr.getId());
				mrin.setWord(word);
				if(vl>1)
				mrin.setNword(nword);
				mrin.setTotal(mr.getTotal()+1);
				
			}
			
		 }else{
			 Positive mrin=new Positive();
			    mrin.setWord(word);
				if(vl>1)
				mrin.setNword(nword);
				else
				mrin.setNword(word);
				mrin.setTotal(1);
				
			 hibernateTemplate.save(mrin);
		 }
		 }
		 
		 if(type.equals("negative")){
			 
			 Query q = null;
			 if(al>1&&vl>1){
			     hql = "FROM Negative where word = :word AND nword = :nword";
			     q = session.createQuery(hql).setParameter("word", word).setParameter("nword", nword);
			 }
			 else if(al>1&&vl<1){
				 hql = "FROM Negative where word = :word AND nword = :nword";
		         q = session.createQuery(hql).setParameter("word", word).setParameter("nword", word);
			 }
			
			 List<Negative> persons = q.list();
			
			 if(persons.size() > 0){
				for(Negative mr:persons){
					Negative mrin=new Negative();
					mrin.setId(mr.getId());
					mrin.setWord(word);
					if(vl>1)
					mrin.setNword(nword);
					mrin.setTotal(mr.getTotal()+1);
					
				}
				
			 }else{
				 Negative mrin=new Negative();
				    mrin.setWord(word);
					if(vl>1)
					mrin.setNword(nword);
					else
					mrin.setNword(word);
					mrin.setTotal(1);
					
				 hibernateTemplate.save(mrin);
			 }
			 }
		 
		return false;
	}
	

} 