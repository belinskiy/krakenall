package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Probabilitymain;

public interface IProbabilitymainDAO {

	String addProbabilitymain(Probabilitymain probabilitymain);

	List<Probabilitymain> probabilityExists(int guid);

	List<Probabilitymain> probabilityExists(String tokentext);

	void deleteText(Timestamp dateupdate);

	List<Probabilitymain> getAllDocuments(Timestamp ourJavaTimestampObject);

	List<Probabilitymain> getAllByPage(int pageSize, int pageNumber, Timestamp dateupdate);

	Long countText(Timestamp dateupdate);



}
