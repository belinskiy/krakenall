package com.bgv.dao;

import java.util.List;

import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilitymainold;

public interface IProbabilitymainoldDAO {

	String addProbabilitymain(Probabilitymainold probabilitymain);

	List<Probabilitymainold> probabilityExists(int guid);

	List<Probabilitymainold> probabilityExists(String tokentext);



}
