package com.bgv.dao;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Person;
import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilitymainold;
import com.bgv.entity.Probabilitypos;

@Transactional
@Repository
public class ProbabilitymainoldDAO implements IProbabilitymainoldDAO{
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	
	
	@Override
	public String addProbabilitymain(Probabilitymainold probabilitymain) {
		String guid=null;
		List<Probabilitymainold> prob =probabilityExists(probabilitymain.getTokentext());
		if(prob.size() > 0) {
			for(Probabilitymainold p:prob)
			{			
				if(p!=null){
					
			    guid=p.getGuid();	
				p.setTokentotal(p.getTokentotal()+probabilitymain.getTokentotal());	
				hibernateTemplate.update(p);
				}
			}
			
		}
		else{
			 UUID idOne = UUID.randomUUID();
			 guid=String.valueOf(idOne);
			probabilitymain.setGuid(guid);
			hibernateTemplate.save(probabilitymain);
		
		}
		return guid;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Probabilitymainold> probabilityExists(String tokentext) {
		String hql = "FROM Probabilitymainold as p WHERE p.tokentext = :tokentext";
		List<Probabilitymainold> prob = (List<Probabilitymainold>) hibernateTemplate.findByNamedParam(hql, "tokentext" ,tokentext);
		return prob;
	}


	@Override
	public List<Probabilitymainold> probabilityExists(int guid) {
		// TODO Auto-generated method stub
		return null;
	}


}
