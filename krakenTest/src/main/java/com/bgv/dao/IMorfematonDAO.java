package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Morfematon;


public interface IMorfematonDAO {
	void getLarge();
	Morfematon getMorfematonById(int pid);
    boolean addMorfematon(Morfematon person);
    void updateMorfematon(Morfematon person);
    void deleteMorfematon(int pid);

	boolean morfematonExists(String aword, String vword, String bword, int awordton, int vwordton, int bwordton);
	
	
}
