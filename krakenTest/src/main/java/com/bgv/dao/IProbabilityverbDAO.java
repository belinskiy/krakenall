package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Horosho;
import com.bgv.entity.Ploho;
import com.bgv.entity.Probabilitybwa;
import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbexpl;
import com.bgv.entity.Probabilityverbtrip;
import com.bgv.entity.Probabilityverbtrippp;
import com.bgv.entity.Probabilityverbtripqcq;
import com.bgv.entity.Probabilityverbtripqq;

public interface IProbabilityverbDAO {
	boolean addProbabilitycounter(Probabilityverb counter);
	
	List<Probabilityverb> probabilityExists(String nounname, String verbname);

	List<Probabilityverb> getAllByPage(int pageSize, int pageNumber, Timestamp dateupdate);

	Long countText();

	void getLarge();

	void getLargeUpdate();

	void addProbabilitycounterTriplet(Probabilityverbtrip counter);

	List<Probabilityverbtrip> probabilityExistsTriplet(String nounname, String verbname, String nounnamet);

	void getLargeTriplet();

	List<Probabilityverbtripqq> probabilityExistsTripletqq(String nounname, String verbname, String nounnamet);

	void addProbabilitycounterTripletqq(Probabilityverbtripqq counter);

	void addProbabilitycounterTripletPredlog(Probabilityverbtrippp counter);

	List<Probabilityverbtrippp> probabilityExistsTripletPredlog(String nounname, String verbname, String nounnamet);

	void addProbabilityPloho(Ploho counter);

	void addProbabilityHorosho(Horosho counter);

	List<Horosho> probabilityExistsHorosho(String word);

	List<Ploho> probabilityExistsPloho(String word);
	
	
	void addProbabilitycounterExpl(Probabilityverbexpl counter);

	List<Probabilityverbexpl> probabilityExistsExpl(String nounname, String verbname, String nounnamet);

	void addProbabilitycounterTripletqcq(Probabilityverbtripqcq countern);

	List<Probabilityverbtripqcq> probabilityExistsTripletqcq(String nounname, String verbname, String nounnamet);

	void addProbabilitycounterProperty(Probabilitybwa counter);

	List<Probabilityverbexpl> probabilityExistsExpl(String text);
	
	
}
