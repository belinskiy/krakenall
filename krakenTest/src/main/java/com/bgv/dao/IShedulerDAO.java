package com.bgv.dao;

import java.util.List;

import com.bgv.entity.Sheduler;
import com.bgv.entity.Shedulerslog;

public interface IShedulerDAO {
	 List<Sheduler> getAllSheduler();
	  
	    boolean addSheduler(Sheduler person);
	    boolean addShedulerLog(Shedulerslog person);
	    void updateSheduler(Sheduler person);

		Sheduler getShedulerById(int pid);
}
