package com.bgv.dao;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilityobject;

@Transactional
@Repository
public class ProbabilityobjectDAO implements IProbabilityobjectDAO{
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	@Override
	public boolean addProbabilityobject(Probabilityobject probabilityobject) {
		hibernateTemplate.save(probabilityobject);
		return false;
		
	}
}
