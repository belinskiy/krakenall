package com.bgv.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.lucene.morphology.LuceneMorphology;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Qnoname;
import com.bgv.memory.FastModelDAO;


@Transactional
@Repository
public class QnonameDAO implements IQnonameDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	@Autowired
	private FastModelDAO fastMemoryModel;

	 
	 

	 
		 @Override
		 public void getLarge() {
			 Map<String, String>  singledb=new HashMap<String, String>();
			 Map<String, String>  phrasedb=new HashMap<String, String>();
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 ScrollableResults scResults = session.createCriteria(Qnoname.class).scroll(ScrollMode.FORWARD_ONLY);
						while(scResults.next()) {
							Qnoname dirty = (Qnoname)scResults.get(0);
							
							//разделить на два носителя single word and phrase в нормальной форме
							//phrase парно привести к нормальной форме при загрузке iilRead и при локальном поиске
							
							
							 String word=dirty.getWord().toLowerCase().trim();
							 //String q=dirty.getNword().toLowerCase();
							 String q=dirty.getNword().trim();
							 int type =dirty.getTotal();
							 
							 if(type==1)
							 singledb.put(word, q);
							 
							 if(type==2)
							 phrasedb.put(word, q);
								
						}
						
						fastMemoryModel.cleareQnonamesingle();
						fastMemoryModel.setQnonamesingledb(singledb);
						
						fastMemoryModel.cleareQnonamephrasedb();
						fastMemoryModel.setQnonamephrasedbdb(phrasedb);
		
		    }	
		
		 

		 

}
