package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Person;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilitypos;

@Transactional
@Repository
public class ProbabilitymainDAO implements IProbabilitymainDAO{
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	
	
	@Override
	public String addProbabilitymain(Probabilitymain probabilitymain) {
		String guid=null;
		List<Probabilitymain> prob =probabilityExists(probabilitymain.getTokentext());
		if(prob.size() > 0) {
			for(Probabilitymain p:prob)
			{			
				if(p!=null){
					
					guid=p.getGuid();	
				p.setTokentotal(p.getTokentotal()+1);	
				hibernateTemplate.update(p);
				}
			}
			
		}
		else{
			 UUID idOne = UUID.randomUUID();
			 guid=String.valueOf(idOne);
			probabilitymain.setGuid(guid);
			hibernateTemplate.save(probabilitymain);
		
		}
		return guid;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Probabilitymain> probabilityExists(String tokentext) {
		String hql = "FROM Probabilitymain as p WHERE p.tokentext = :tokentext";
		List<Probabilitymain> prob = (List<Probabilitymain>) hibernateTemplate.findByNamedParam(hql, "tokentext", tokentext);
		return prob;
	}


	@Override
	public List<Probabilitymain> probabilityExists(int guid) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void deleteText(Timestamp dateupdate) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "delete Probabilitymain where dateupdate < :dateupdate AND tokentotal > 30";
		 Query q = session.createQuery(hql).setParameter("dateupdate", dateupdate);
		 q.executeUpdate();
		    
		    
		 //hibernateTemplate.bulkUpdate("delete from Probabilitydirty where dateupdate < "+dateupdate);
		
	}
	@Override
	public List<Probabilitymain> getAllDocuments(Timestamp dateupdate) {
		
		String query = "FROM Probabilitymain p where p.dateupdate < :dateupdate AND tokentotal > 30";
			    return (List<Probabilitymain>)  this.hibernateTemplate.findByNamedParam(query, "dateupdate", dateupdate);
			    
	}	
	
	@Override
	 public List<Probabilitymain> getAllByPage(final int pageSize, final int pageNumber, final Timestamp dateupdate) {
	        HibernateTemplate template =  this.hibernateTemplate;
	        return (List<Probabilitymain>) template.execute(new HibernateCallback() {
	            public Object doInHibernate(Session session) {
	                Query query = session.createQuery("from Probabilitymain where dateupdate < :dateupdate AND tokentotal > 30").setParameter("dateupdate", dateupdate);
	                query.setMaxResults(pageSize);
	                query.setFirstResult(pageSize * pageNumber);
	                return query.list();
	            }
	        });
	    }
	 @Override
		public Long countText(Timestamp dateupdate) {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "select count(*) from Probabilitymain where dateupdate < :dateupdate AND tokentotal > 30";
			 Query q = session.createQuery(hql).setParameter("dateupdate", dateupdate);
			 //q.executeUpdate();
			 
			 Long count = (Long) q.uniqueResult();
		
			 return count;
	    }
}
