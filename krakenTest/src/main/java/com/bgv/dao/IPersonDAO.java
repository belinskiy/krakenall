package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Person;

public interface IPersonDAO {
    List<Person> getAllPersons();
    Person getPersonById(int pid);
    boolean addPerson(Person person);
    void updatePerson(Person person);
    void deletePerson(int pid);
    boolean personExists(String username);
	List<Person> getAllDocuments(Timestamp dateupdate);
	int getMaxDocuments();
} 