package com.bgv.dao.probability;

import com.bgv.dao.probability.SubClaims.Datavalue;
import com.bgv.dao.probability.SubClaims.Value;

public class SubQualifiers {
   String hash;
   String snaktype;
   String property;
   String datatype;
   Datavalue datavalue;
   
   
	public String getHash() {
	return hash;
}


public void setHash(String hash) {
	this.hash = hash;
}


public String getSnaktype() {
	return snaktype;
}


public void setSnaktype(String snaktype) {
	this.snaktype = snaktype;
}


public String getProperty() {
	return property;
}


public void setProperty(String property) {
	this.property = property;
}


public String getDatatype() {
	return datatype;
}


public void setDatatype(String datatype) {
	this.datatype = datatype;
}


public Datavalue getDatavalue() {
	return datavalue;
}


public void setDatavalue(Datavalue datavalue) {
	this.datavalue = datavalue;
}


	public class Datavalue {

		String type;
		Object value;

		public Object getValue() {
			return value;
		}

		public void setValue(Value value) {
			this.value = value;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
		
		
		
	}
   
}
