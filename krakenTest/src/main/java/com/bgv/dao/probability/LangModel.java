package com.bgv.dao.probability;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bgv.dao.IProbabilitymainoldDAO;
import com.bgv.entity.Probabilitycounter;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilitymainold;
import com.bgv.nlp.obj.NameFinderModelMEDAO;
import com.bgv.service.IProbabilityService;
import com.bgv.service.IProbabilitycounterService;
import com.bgv.service.IProbabilitydirtyService;
import com.bgv.springmvc.configuration.ScrapingResultMessage;
import com.bgv.springmvc.configuration.TaskProducer;
import com.bgv.springmvc.domain.FindObject;
import com.bgv.springmvc.domain.TokenProbability;
import com.derby.model.Mprobabilitycounter;
import com.derby.model.MprobabilitycounterDao;
import com.derby.model.Mprobabilitydirty;
import com.derby.model.MprobabilitydirtyDao;
import com.derby.model.User;
import com.derby.model.UserDao;


@Service
public class LangModel implements ILangModel {

	    @Autowired
		private IProbabilityService probService;
	 
		@Autowired
		private IProbabilitydirtyService dirtyService; 
		
		@Autowired
		private IProbabilitycounterService counterService;
		
		@Autowired
		private IProbabilitymainoldDAO probabilitymainoldDAO;
		
		@Autowired
		private TaskProducer producer;
		
		@Autowired
		 private NameFinderModelMEDAO model;
	 
//		@Autowired
//		UserDao userDao;
//		
//		@Autowired
//		MprobabilitydirtyDao probabilitydirtyDao;
//		
//		@Autowired
//		MprobabilitycounterDao probabilitycounterDao;
		
		
		
	 //900000
	//@Scheduled(fixedRate=900000) 
	public void doTask() throws IOException, ParseException{
		System.out.println("START LangModel Mysql");
		UUID idOne = UUID.randomUUID();
	    String th=String.valueOf(idOne);
		Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
		

		try{
	
		
		   //List<Mprobabilitydirty> us = probabilitydirtyDao.getAllDocuments(ourJavaTimestampObject);
		   //System.out.println(us);		
		
			Long mcount=dirtyService.countText(ourJavaTimestampObject);
			System.out.println("LangModel=Counter="+mcount); 
			int totalpage=mcount.intValue(); 
			int pageSize=40;
			int pages=totalpage/pageSize;
			
			
			
			for(int ip=1; ip<pages; ip++){
				 System.out.println("LangModel=pages="+ip); 
				 List<Probabilitydirty>  idata = dirtyService.getAllByPage(ip, pageSize, ourJavaTimestampObject);
				 
				 int h=0;
				 for(Probabilitydirty dirty:idata){

						String thashs=dirty.getThash();
						String exguid = dirty.getExguid();
						List<Probabilitycounter> count = counterService.probabilityExists(thashs);
						
						
						if(count.size() > 0) {
							
						}else{
							ScrapingResultMessage taskMessage = new ScrapingResultMessage();
							taskMessage.setUrl(exguid);
							taskMessage.setSummary(thashs);
							taskMessage.setCodeSnippets(dirty.getTokenjson());
							
						
						}
					h++;	
					
					 
				 }
			
			}
					
			dirtyService.deleteText(ourJavaTimestampObject);
			
		

	}catch(Exception e){e.printStackTrace();}
			System.out.println("END LangModel Mysql="+th); 

	}
	//@Scheduled(fixedRate=1000000) 
	public void doComulitativeTask() throws IOException, ParseException{
		System.out.println("START Comulitative");
		
		 
		    Map<String, String> probabilitycounter = model.getPhraseCounterMap();
		    Map<String, Integer> probabilitymain=model.getPhraseTokenMapValue();
		    
		    System.out.println("Total docs="+probabilitycounter.size());
		    System.out.println("Total tokens="+probabilitymain.size());
		    
		
		Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());	
		
		
		
		Long mcount=probService.countText(ourJavaTimestampObject);
		//System.out.println("Comulitative=Counter="+mcount); 
		int totalpage=mcount.intValue(); 
		int pageSize=40;
		int pages=totalpage/pageSize;
		
		for(int ip=1; ip<pages; ip++){
			 System.out.println("Comulitative=page number="+ip); 
			 List<Probabilitymain>  idata = probService.getAllByPage(ip, pageSize, ourJavaTimestampObject);
			
			  for(Probabilitymain main:idata){
				  
				 System.out.println("30=h="+main.getTokentext()); 
				  
				  
				 Probabilitymainold probabilitymainold= new Probabilitymainold();
				  
				 probabilitymainold.setGuid(main.getGuid());
				 probabilitymainold.setTokentext(main.getTokentext());
				 probabilitymainold.setTokentotal(main.getTokentotal());
				 probabilitymainold.setTokentype(main.getTokentype());
				 probabilitymainoldDAO.addProbabilitymain(probabilitymainold);
				  
			  }
		}
		
		
		/*
		  List<Probabilitymain> data =probService.getAllDocuments(ourJavaTimestampObject);
		  System.out.println("30=h="+data.size()); 
		  for(Probabilitymain main:data){
			  
			  System.out.println("30=h="+main.getTokentext()); 
			  
			  
			 Probabilitymainold probabilitymainold= new Probabilitymainold();
			  
			 probabilitymainold.setGuid(main.getGuid());
			 probabilitymainold.setTokentext(main.getTokentext());
			 probabilitymainold.setTokentotal(main.getTokentotal());
			 probabilitymainold.setTokentype(main.getTokentype());
			 probabilitymainoldDAO.addProbabilitymain(probabilitymainold);
			  
		  }*/
		  
		  probService.deleteMainText(ourJavaTimestampObject);
		  System.out.println("END LangModel doComulitativeTask");
	}
	
	//@Scheduled(fixedRate=100000) 
/*	public void doDerbyTask() throws IOException, ParseException{
		
		UUID idOne = UUID.randomUUID();
	    String th=String.valueOf(idOne);
	    System.out.println("START LangModel doTask"+th);
	    Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
	
		
		//probabilitydirtyDao.save("1", th, "tokenjson-lalala");
		

				
		 //getAllDirty data - after process delete it
		
		 //verify if hash is not prezent and add it
		
		 //save all results
		try{
	
		
		   List<Mprobabilitydirty> data = probabilitydirtyDao.getAllDocuments(ourJavaTimestampObject);
		   probabilitydirtyDao.deleteAllDocuments(ourJavaTimestampObject);	
		  
		    System.out.println("LangModel=h="+data.size()); 
			int h=0;
			for(Mprobabilitydirty dirty:data){
				String thashs=dirty.getThash();
				String exguid = dirty.getExguid();
				try{
				
				List<Mprobabilitycounter> count = probabilitycounterDao.findByName(thashs);
				
				
				  System.out.println("count="+count.toString()); 
				
			//System.out.println(dirty.toString()); 
				
				
				if(count.isEmpty()) {
	
					System.out.println("hash="+thashs);
					
					 probabilitycounterDao.save(exguid, thashs);
					
					
					 JSONParser parser = new JSONParser();
					 String stringToParse=dirty.getTokenjson();
					 
				        JSONArray slideContent = (JSONArray) parser.parse(stringToParse);
				        Iterator i = slideContent.iterator();					 
				        
				        
				        
				     List<TokenProbability> problist = new ArrayList<TokenProbability>();
					 TokenProbability c=new TokenProbability();
			         List<String[]> alltokens=new ArrayList<String[]>();
			         List<FindObject> obj=new ArrayList<FindObject>();
int k=0;
				     while (i.hasNext()) {
				            
				        	
				        	
				     JSONObject json = (JSONObject) i.next();
				            
				        
				        
				       
					 
					 

			         
			         String thash="";
			         if(k==0)
			         thash = (String) ""+json.get("thash");
			         
			         
			         else if(k==1){
			         JSONArray findObject = (JSONArray) json.get("findObject");
					 Iterator<JSONObject> iteratorfindObject = findObject.iterator();
			         while (iteratorfindObject.hasNext()) {
			         	
			     	    JSONObject find = iteratorfindObject.next();
			     	        String id = (String) find.get("id");
				            String names = (String) find.get("name");
				            FindObject n=new FindObject();
				            n.setId(id);
				            n.setName(names);
				            obj.add(n);
				            
			         }
				        }
			         else  if(k==2){
					 JSONArray tokens = (JSONArray) json.get("tokens");
					 Iterator<JSONObject> iterator = tokens.iterator();
			         while (iterator.hasNext()) {
			         	
			     	    JSONObject tok = iterator.next();
			     	        String ids = (String) tok.get("name");
				            String names = (String) tok.get("value");
				            String[] index = new String[2];
				            index[0]=ids;
				            index[1]=names;	
				            alltokens.add(index);
				            
			         }
			         
			         }
			         k++;
					
				        }
				        c.setAlltokens(alltokens);
					    c.setFindobject(obj);
				        problist.add(c);
					
					    probService.addDerbyTokenProbability(problist);
				       
				}}catch(Exception e){e.printStackTrace();}
			h++;	
			}
	}catch(Exception e){e.printStackTrace();}
			System.out.println("END LangModel doTask="+th); 

	}
*/	
}
