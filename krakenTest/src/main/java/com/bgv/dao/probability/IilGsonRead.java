package com.bgv.dao.probability;

import com.bgv.dao.probability.SubClaims.Datavalue;
import com.bgv.dao.probability.SubClaims.Mainsnak;
import com.bgv.dao.probability.SubReferences.SubSnaks;
import com.bgv.memory.FastModelDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonReader;



import com.google.gson.reflect.TypeToken;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class IilGsonRead implements IilGsonReadDAO{
	
	
	 @Autowired
		private FastModelDAO fastMemoryModel;
	 
	 
	//private static final String FILENAME = "/opt/wildfly/standalone/data/latest-all.json";
	//private static final String FILENAME ="/mindscan/wildfly/data/latest-all.json";
	private static final String FILENAME ="/mindscan/wildfly/02032017/latest-all.json";
	
	   Map<String,List> objects = new HashMap<String,List>();
	   Map<String,List> objectsToText = new HashMap<String,List>();
	   Map<String,List> properties = new HashMap<String,List>();
	   Map<String,List> statements = new HashMap<String,List>();
    
	//P31 instanceof-конкретный экземпляр
	   Map<String,List> ofinstance = new HashMap<String,List>();
	
	//P279 subclassof
	   Map<String,List> subclassof = new HashMap<String,List>();
    
	//P527 haspart-состоит из 
	   Map<String,List> haspart = new HashMap<String,List>();
		
	//P361 partof-является частью
	   Map<String,List> partof = new HashMap<String,List>();
    
	   Map<String,List>  propertyOfType = new HashMap<String,List>();
	   
	   Map<String,List>  maincategory = new HashMap<String,List>();
	   
	//Прочие свойства
	   Map<String,List> otherprops = new HashMap<String,List>();
	
	
	   Map<String, List<String>> qpproperties = new HashMap<String, List<String>>();
	   
	
    
   
    static Pattern pattern = Pattern.compile(", id=((Q|P)\\w+)}");
    
    
    @Override
    public void readJsonStream() throws IOException {
       InputStream in = new FileInputStream(FILENAME);
       
       JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

       int k=0;
        reader.beginArray();
        Gson gson = new GsonBuilder().create();
        while (reader.hasNext()) {
        	
        	
        	LibItem message = gson.fromJson(reader, LibItem.class);
               	
        	String entityID=message.getId();
        	String entityType=message.getType();
        	boolean isproperty=false;
        	
        	if (entityType.equals("property")) isproperty=true;

        	k++;
        	//System.out.println("k="+k+" Id: " + message.getId());
        	
        	//----------------------------------Labels--------------------------------
        
        	
        	try{
        	
        		
        		for (Map.Entry<String, Labels> entry : message.getLabels().entrySet())
        		{
        			if("ru".equals(entry.getKey())){
        				
        				String entityname=readE(entry.getValue().getValue().toLowerCase());
        				
        				if(!isproperty){
            				if(objects.containsKey(entityname)){ 
            					
            					List<String> prors = objects.get(entityname);
            					List<String> dd = new ArrayList();
            					dd.add(entityID);
	        		    		for(String l:prors){
	        		    			dd.add(l);
	        		    		}
	        		    		objects.put(entityname, dd);
            					
            				}else{
            					List<String> dd = new ArrayList();
            					dd.add(entityID);
            					objects.put(entityname, dd);
            				}
            				
        				}
        				if(isproperty){
        					if(properties.containsKey(entityname)){
            					List<String> prors = properties.get(entityname);
            					List<String> dd = new ArrayList();
            					dd.add(entityID);
	        		    		for(String l:prors){
	        		    			dd.add(l);
	        		    		}
	        		    		properties.put(entityname, dd);
        						
        					}else{
            					List<String> dd = new ArrayList();
            					dd.add(entityID);
            					properties.put(entityname, dd);
            				}
        					
        				}
        				
        				
        				if(objectsToText.containsKey(entityID)){
        					List<String> prors = objectsToText.get(entityID);
        					List<String> dd = new ArrayList();
        					dd.add(entityname);
        		    		for(String l:prors){
        		    			dd.add(l);
        		    		}
        		    		objectsToText.put(entityID, dd);
        				}else{
        					List<String> dd = new ArrayList();
        					dd.add(entityname);
        					objectsToText.put(entityID, dd);
        				}
        				
        				//System.out.println("Labels lang: " + entry.getValue().getLanguage());
        				//System.out.println("Labels val: " + entry.getValue().getValue());
        				
        				
        			}	
        		}	
        		
        	
        	}catch(Exception e){}
        	
        	
        	
        	boolean thisisru=false;
        	//----------------------------------Aliases--------------------------------
        	
        	try{
        		
        		for (Map.Entry<String, ArrayList<Aliases>> entry : message.getAliases().entrySet())
        		{
        			String key=entry.getKey();
        			ArrayList<Aliases> value=entry.getValue();
        			if("ru".equals(key)){
        				thisisru=true;
        				
        				if(value!=null)
        				for(Aliases al:value){
        					

        					String entityname=readE(al.getValue().toLowerCase());
        					if(entityname!=null){
        						if(!isproperty){
		            				if(objects.containsKey(entityname)){ 
		            					
		            					List<String> prors = objects.get(entityname);
		            					List<String> dd = new ArrayList();
		            					dd.add(entityID);
			        		    		for(String l:prors){
			        		    			dd.add(l);
			        		    		}
			        		    		objects.put(entityname, dd);
		        					}else{
		            					List<String> dd = new ArrayList();
		            					dd.add(entityID);
		            					objects.put(entityname, dd);
		            				}
	            				}
	            				if(isproperty){
	            					
	            					if(properties.containsKey(entityname)){
	            						List<String> prors = properties.get(entityname);
	                					List<String> dd = new ArrayList();
	                					dd.add(entityID);
	    	        		    		for(String l:prors){
	    	        		    			dd.add(l);
	    	        		    		}
	    	        		    		properties.put(entityname, dd);
	            						}else{
	                    					List<String> dd = new ArrayList();
	                    					dd.add(entityID);
	                    					properties.put(entityname, dd);
	                    				}
            					}
	            				
	            				
	            				if(objectsToText.containsKey(entityID)){
	            					List<String> prors = objectsToText.get(entityID);
	            					List<String> dd = new ArrayList();
	            					dd.add(entityname);
	            		    		for(String l:prors){
	            		    			dd.add(l);
	            		    		}
	            		    		objectsToText.put(entityID, dd);
	            				}else{
	            					List<String> dd = new ArrayList();
	            					dd.add(entityname);
	            					objectsToText.put(entityID, dd);
	            				}
        					}
            				
            				
            				//System.out.println("aliases: " + al.getLanguage());
            				//System.out.println("aliases: " + al.getValue());
        				}
        				
        			}	
        		}	
        		
        		}catch(Exception e){e.printStackTrace();}
        	
        	//----------------------------------Claims------------------------------
        	
        	if(thisisru)
        	try{
        		
        		//P31 instance	 - есть? да - значит это конкретный экземпляр
        		//P279 подкласс от
        		//P527 состоит из 
        		//P361 является частью
        		
        		//и все другие свойства
        		
        		 
        		Map<String, ArrayList<SubClaims>> map = message.getClaims();
        		
        		for (Map.Entry<String, ArrayList<SubClaims>> entry : map.entrySet())
        		{

        			String statmentType=entry.getKey();

        			ArrayList<SubClaims> io = entry.getValue();
        		    if(io!=null){
        		    	
        		    
        		    		
        			    	
		        		    for(SubClaims sclaim:io){
		        		    	
		        		    	List<String> prors= new ArrayList<String>(); 			 		        		    			 
		        		    			 Mainsnak ms= sclaim.getMainsnak();
		        		    			 Datavalue datavalue = ms.getDatavalue();
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		 								try{
		 									if (datavalue.getType().equals("time")) {

		 										SimpleDateFormat formatter = new SimpleDateFormat("+yyyy-MM-dd'T'HH:mm:ss'Z'");
		 										formatter.setTimeZone(TimeZone.getTimeZone("GMT"));

		 										Object ss = null;
		 										try {
		 											ss = datavalue.getValue();
		 										} catch (Exception e) {
		 										}
		 										if (ss != null)
		 											if (datavalue.getValue() instanceof LinkedTreeMap) {
		 												LinkedTreeMap<String, Object> mapResult = (LinkedTreeMap<String, Object>) datavalue
		 														.getValue();
		 												for (Entry<String, Object> entrymap : mapResult.entrySet()) {
		 													String key = entry.getKey();
		 													Object value = entrymap.getValue();

		 													try {
		 														Date date = (Date) formatter.parse((String) value);

		 														long epoch = date.getTime();
		 														String keymap = entityID + "" + key;
		 														//String valuemap = "" + epoch;
		 														String valuemap = "" +(String) value;
		 														if (qpproperties.containsKey(keymap)) {
		 															List<String> list= qpproperties.get(keymap);
		 															list.add(valuemap);
		 														}else{
		 															List<String> list=new ArrayList<String>();
		 															list.add(valuemap);
		 															qpproperties.put(keymap, list);
		 														}
		 														
		 														
		 														
		 														//System.out.println(key + " => " + date + "=" + entityID);
		 													} catch (Exception e) {
		 														// It does not match your
		 														// input
		 													}

		 												}
		 											}

		 									} else if (datavalue.getType().equals("string")) {
		 										try {
		 											String dv = datavalue.getValue().toString();
		 											String keymap = entityID + "" + ms.getProperty();
		 											String valuemap = "" + dv;
		 											if (qpproperties.containsKey(keymap)) {
		 												List<String> list= qpproperties.get(keymap);
		 												list.add(valuemap);
		 											}else{
		 												List<String> list=new ArrayList<String>();
		 												list.add(valuemap);
		 												qpproperties.put(keymap, list);
		 											}
		 										} catch (Exception e) {

		 										}
		 									} else if (datavalue.getType().equals("globecoordinate")) {

		 									} else if (datavalue.getType().equals("quantity")) {
		 										try{
		 										Object ss = null;
		 										try {
		 											ss = datavalue.getValue();
		 										} catch (Exception e) {
		 										}
		 										if (ss != null)
		 											if (datavalue.getValue() instanceof LinkedTreeMap) {
		 												LinkedTreeMap<String, Object> mapResult = (LinkedTreeMap<String, Object>) datavalue
		 														.getValue();
		 												for (Entry<String, Object> entrymap : mapResult.entrySet()) {
		 													String key = entry.getKey();
		 													Object value = entrymap.getValue();

		 												}
		 											}
		 										
		 									} catch (Exception e) {

		 									}
		 									} else if (datavalue.getType().equals("wikibase-entityid")) {
		 											try{
		 												String dv = null;
		 											try {
		 												dv = datavalue.getValue().toString();
		 											} catch (Exception e) {
		 											}
		 											String p = null;
		 											if (dv != null) {
		 												Matcher m = pattern.matcher(dv);
		 														while (m.find()) {
		 															p = m.group(1);
		 															String keymap = entityID + "" + ms.getProperty();
		 															String valuemap = "" + p;
		 															if (qpproperties.containsKey(keymap)) {
		 																List<String> list= qpproperties.get(keymap);
		 																list.add(valuemap);
		 															}else{
		 																List<String> list=new ArrayList<String>();
		 																list.add(valuemap);
		 																qpproperties.put(keymap, list);
		 															}
		 														}
		 											}
		 											} catch (Exception e) {}
		 									}
		 								 } catch (Exception e) {}
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 
		        		    			 String dv=null;
		        		    			 try{  	 
		        		    				 try{dv=datavalue.getValue().toString(); }catch(Exception e){}
		        		    				 	String p=null; 
		        		    				 	if(dv!=null){
		        		    				 		Matcher m = pattern.matcher(dv);
			        		    					while (m.find()) {
			        		    						p=m.group(1);
			        		    					}
			        		    					if(p!=null)
			        		    					prors.add(p);
		        		    					}
		        		    				
		        		    				
		        		    				 
		        		    			 
		        		    			 
		        		    			 
		        		    			 }catch(Exception e){e.printStackTrace();}
		        		    			 
		        		    			 /*
		        		    			 ArrayList<SubReferences> rf = sclaim.getReferences();
		        		    			 if(rf!=null)
		        		    			 for(SubReferences sf:rf){
		        		    				// System.out.println("Claims References Property="+ sf.getHash());
		        		    				 Map<String, ArrayList<SubSnaks>> sn = sf.getSnaks();
		        		    				 for (Map.Entry<String, ArrayList<SubSnaks>> snSnaks : sn.entrySet())
		        		    	        		{
		        		    					 //System.out.println("Claims Snaks key="+ snSnaks.getKey());
		        		    	        		}
		        		    				 
		        		    			 }
		        		    			 */
		        		    			 
		        		    
		        		    if("P31".endsWith(statmentType)){
		        		    	if(ofinstance.containsKey(entityID)){
		        		    		List<String> dd = ofinstance.get(entityID);
		        		    		for(String l:prors){
		        		    			dd.add(l);
		        		    		}
		        		    		ofinstance.put(entityID, dd);
		        		    		
		        		    	}else
		        		    		
		        		    	//P31 instanceof-конкретный экземпляр
		        		    	ofinstance.put(entityID, prors);
		        		    }
		        		    	else if("P279".endsWith(statmentType)){
		        		    		if(subclassof.containsKey(entityID)){
		            		    		List<String> dd = subclassof.get(entityID);
		            		    		for(String l:prors){
		            		    			dd.add(l);
		            		    		}
		            		    		subclassof.put(entityID, dd);
		            		    		
		            		    	}else
		        		    		//P279 subclassof
		        		    	subclassof.put(entityID, prors);
		        		    } 	
		        		    	else if("P527".endsWith(statmentType)){
		        		    		if(haspart.containsKey(entityID)){
		            		    		List<String> dd = haspart.get(entityID);
		            		    		for(String l:prors){
		            		    			dd.add(l);
		            		    		}
		            		    		haspart.put(entityID, dd);
		            		    		
		            		    	}else
		        		    	//P527 haspart-состоит из 
		        		    	haspart.put(entityID, prors);
		        		    }	
		        		    	else if("P361".endsWith(statmentType))	{
		        		    	//P361 partof-является частью
		        		    		 
		           		    	 if(partof.containsKey(entityID))
		           		    	{
		         		    		List<String> dd = partof.get(entityID);
		         		    		for(String l:prors){
		         		    			dd.add(l);
		         		    		}
		         		    		partof.put(entityID, dd);
		         		    		
		         		    	}else
		        		    	 partof.put(entityID, prors);
		        		    }else if("P1963".endsWith(statmentType))	{
		        		    	//P361 propertyOfType-является свойствами типа
	        		    		 
		           		    	 if(propertyOfType.containsKey(entityID))
		           		    	{
		         		    		List<String> dd = propertyOfType.get(entityID);
		         		    		for(String l:prors){
		         		    			dd.add(l);
		         		    		}
		         		    		propertyOfType.put(entityID, dd);
		         		    		
		         		    	}else
		         		    		propertyOfType.put(entityID, prors);
		        		    }else if("P910".endsWith(statmentType)||"P373".endsWith(statmentType))	{
		        		    	//topic's main category-является категорией
	        		    		 
		           		    	 if(maincategory.containsKey(entityID))
		           		    	{
		         		    		List<String> dd = maincategory.get(entityID);
		         		    		for(String l:prors){
		         		    			dd.add(l);
		         		    		}
		         		    		maincategory.put(entityID, dd);
		         		    		
		         		    	}else
		         		    		maincategory.put(entityID, prors);
		        		    }else{
		        		    	//Прочие свойства - это все свойства
		        		    	 
		        		    	 if(otherprops.containsKey(entityID))
		        		    	 {
		          		    		List<String> dd = otherprops.get(entityID);
		          		    		for(String l:prors){
		          		    			dd.add(l);
		          		    		}
		          		    		otherprops.put(entityID, dd);
		          		    		
		          		    	}else
		        		    	otherprops.put(entityID, prors);}
		        		   
		        		    
		        		    }
        		    
        		    }
        		}
        		 
        		 
        		}catch(Exception e){e.printStackTrace();}
        	
        	 //if(stop)break;
        	
        }
        
       
        reader.endArray();
        reader.close();
         
        fastMemoryModel.setIilobjects(objects);

        fastMemoryModel.setIilproperties(properties);
        
        fastMemoryModel.setIilobjectsToText(objectsToText);

        fastMemoryModel.setIilstatements(statements);

        fastMemoryModel.setOfinstance(ofinstance);

        fastMemoryModel.setIilsubclassof(subclassof);

        fastMemoryModel.setIilhaspart(haspart);

        fastMemoryModel.setIilpartof(partof);
        
        fastMemoryModel.setIilpropertyOfType(propertyOfType);

        fastMemoryModel.setIilotherprops(otherprops);
        
        fastMemoryModel.setIilQPKeyValue(qpproperties);
        
        fastMemoryModel.setMaincategory(maincategory);
        
        
    }
public String readE(String dirty){
	 try{dirty = dirty.replaceAll("ё|Ё", "е");}catch(Exception e){}
	return dirty;
}

}
