package com.bgv.dao.probability;

import java.io.IOException;

public interface IilGsonReadDAO {

	void readJsonStream() throws IOException;

}
