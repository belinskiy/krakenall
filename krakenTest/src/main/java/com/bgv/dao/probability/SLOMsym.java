package com.bgv.dao.probability;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.bgv.springmvc.domain.Config;

import opennlp.tools.util.InvalidFormatException;

public class SLOMsym {
	static Map<String, String> mapword = new HashMap<String, String>();
	static Map<String, String> mapcategory = new HashMap<String, String>();
	static Map<String, ArrayList> hashMap = new HashMap<String, ArrayList>();
	static String root = "E:/Share/C/Downloads/";
	static String fname = "symantic.txt";
	
	 public static void main(String[] args) throws Exception {
		 String file=root+fname;
		 loadSymantex(file);
		    
		 Map<String, String>  totalkvantorMap= new HashMap<String, String>();
		 Map<String, String>  kvantorMap= new HashMap<String, String>();
		/* kvantorMap.put("есть", "1");
		 kvantorMap.put("не есть", "1");
		 kvantorMap.put("и", "2");
		 kvantorMap.put("или", "2");
		 kvantorMap.put("неверно что", "2");
		 kvantorMap.put("если и только если", "2");
		 
		 
		 kvantorMap.put("для всех", "3");
		 kvantorMap.put("для каждого", "3");
		 kvantorMap.put("для любого", "3");
		 kvantorMap.put("все", "3");
		 kvantorMap.put("каждый", "3");
		 kvantorMap.put("любой", "3");
		 kvantorMap.put("ни один", "3");
		 
		 
		 kvantorMap.put("существует", "4");
		 kvantorMap.put("некоторые", "4");
		 kvantorMap.put("большинство", "4");
		 kvantorMap.put("какой нибудь", "4");
		 
		 kvantorMap.put("больше", "5");
		 kvantorMap.put("меньше", "5");
		 
		 kvantorMap.put("равно", "5");
		 kvantorMap.put("криво", "5");
		 
		 kvantorMap.put("высоко", "5");
		 kvantorMap.put("низко", "5");
		 
		 kvantorMap.put("далеко", "5");
		 kvantorMap.put("близко", "5");
		 
		 kvantorMap.put("снижение", "5");
		 kvantorMap.put("увеличение", "5");
		 
		 
		 
		 kvantorMap.put("подняться", "5");
		 kvantorMap.put("упасть", "5");
		 
		 kvantorMap.put("снизить", "5");
		 kvantorMap.put("повысить", "5");
		 
		 kvantorMap.put("уменьшиться", "5");
		 kvantorMap.put("увеличиться", "5");
		 
		 kvantorMap.put("сократиться", "5");
		 kvantorMap.put("удлиняться", "5");
		 
		 kvantorMap.put("превышать", "5"); */
		 kvantorMap.put("нос", "5");
		  for (Map.Entry<String, String> entryd : kvantorMap.entrySet()){
			  
		   String word=entryd.getKey();
		   String kvantorType=entryd.getValue();
		 
		    ArrayList tempdoList = null;
		    tempdoList = hashMap.get(word);
		    if(tempdoList !=null){
		    	for (Object value: tempdoList) {
		    		
		    		  String[] index = new String[2];  
		        	  index =(String[]) value;
		             
		            	  System.out.println("word="+word+" Value : "+index[0]+" Type : "+index[1]);
		            	  totalkvantorMap.put(index[0], kvantorType);
		            
		    	}
		    }
	    
		    Map<String, String> countmap = new ConcurrentHashMap();
		    Map<String, String> unicmap = new ConcurrentHashMap();
		    countmap.put(word, word);
		    //Iterator<Map.Entry<String, String>> it = countmap.entrySet().iterator();
		    while (countmap!=null&&countmap.size()>0) {
		    	
		    for (Map.Entry<String, String> entry : countmap.entrySet()){
		    	//Map.Entry<String, String>entry = it.next();
		    	String d=entry.getKey();
		    	tempdoList = hashMap.get(d);
		    	if(tempdoList!=null)
		    	for (Object value: tempdoList) {
		        	  String[] index = new String[2];  
		        	  index =(String[]) value;
		        	 
		        	  System.out.println(d+" Value : "+index[0]+" Type : "+index[1]);
		        	  totalkvantorMap.put(index[0], kvantorType);
		              if(index[1].equals("hypernyms")){
		            	  if(!unicmap.containsKey(index[0])){
			            	  unicmap.put(index[0], index[0]);
				            	// System.out.println(d+" Value : "+index[0]+" Type : "+index[1]);
					            	  Map<String, String> m = getHypernyms(index[0]);
					            	  if(m!=null){
					            		  for (Map.Entry<String, String> s : m.entrySet())  
					            			  countmap.put(s.getKey(), s.getValue());
					            	  }
					            	  else
					            		  System.out.println(" Value : "+index[0]+" Type : "+index[1]);
			            	  } 
		              }
		              countmap.remove(d); 
		              
		          }
		    	else countmap.remove(d);
		    }
		    
		    
		    }}
		  
		  for (Map.Entry<String, String> entryd : totalkvantorMap.entrySet()){
			  
			   String word=entryd.getKey();
			   String kvantorType=entryd.getValue();
			   kvantorMap.put(word, kvantorType);
			   System.out.println("word="+word+" kvantor="+kvantorType);
		  }
		  for (Map.Entry<String, String> entryd : kvantorMap.entrySet()){
			  String word=entryd.getKey();
			   String kvantorType=entryd.getValue();
			   String text=word+"#"+kvantorType+"\n";
			   
				 
						
						try{
					        
				            OutputStreamWriter writer = new OutputStreamWriter(
				                  new FileOutputStream("E:/Share/C/Downloads/kvantor2.txt", true), "UTF-8");
				            BufferedWriter fbw = new BufferedWriter(writer);
				            fbw.write(text);
				            fbw.newLine();
				            fbw.close();
				        }catch (Exception e) {
				          
				        }
		
		  }
		  
	 }
	 public static Map getHypernyms(String word){
		 Map hashmap = new ConcurrentHashMap();
		 ArrayList tempdoList = null;
		    tempdoList = hashMap.get(word);
		    if(tempdoList !=null){
		    	for (Object value: tempdoList) {
		    	
		    		  String[] index = new String[2];  
		        	  index =(String[]) value;
		              if(index[1].equals("hypernyms")){
		            	  //System.out.println(word+" Value : "+index[0]+" Type : "+index[1]);
		            	  hashmap.put(index[0], index[0]);  
		            	  
		              }
		    	}
		    	return hashmap;
		    }
		    else return null;
		    
			//return hashmap;
	 }
	 
	 
	
	 public static void loadSymantex(String textFileName) throws InvalidFormatException, IOException, ParseException{
		   File file=new File(textFileName);
         
         FileInputStream inStream=new FileInputStream(file);
         InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

    

   JSONParser parser = new JSONParser();
			//Linux
			//   Object obj = parser.parse(new BufferedReader(reader));
			  		
			  //Windows		
		    Object obj = parser.parse(new FileReader(textFileName));
		
		    JSONObject jsonObject = (JSONObject) obj;
		
		   
		    JSONArray companyList = (JSONArray) jsonObject.get("Company List");
		
		    Iterator<JSONObject> iterator = companyList.iterator();
			    while (iterator.hasNext()) {
			    	
				    JSONObject json = iterator.next();
				    String ids = (String) ""+json.get("id");
			        String entry1 = (String) json.get("entry1");
			        String entry2 = (String) ""+json.get("entry2");
			        String relation_name = (String) ""+json.get("relation_name");
			          String[] index = new String[2];
						index[0]=entry2;
						index[1]=relation_name;
			          
			          mapword.put(entry1, entry2); 
			          mapcategory.put(entry2, entry2);
			          addValues(entry1, index);
			    }
	 }
	 public static void createFile(){
		 
		    JSONObject obj = new JSONObject();
				obj.put("Name", "NLP");
				obj.put("Author", "BGVv1");
				
				JSONArray company = new JSONArray();
				
				
		   
		  
		    Iterator it = hashMap.keySet().iterator();
		    ArrayList tempList = null;
		    int i=1;
		    while (it.hasNext()) {
		       String key = it.next().toString();             
		       tempList = hashMap.get(key);
		       if (tempList != null) {
		          for (Object value: tempList) {
		        	  String[] index = new String[2];  
		        	  index =(String[]) value;
		        
		        JSONObject objs = new JSONObject();
		        objs.put("id", i);
		        objs.put("entry1", key);
		        objs.put("entry2", index[0]);
		        objs.put("relation_name", index[1]);	        
				company.add(objs);
				i++;
				
				
				obj.put("Company List", company);

		    
   }
		       }
		    }
		    Config cf=new Config();
	//String root = "C:/Users/Electron/Downloads/";
	//	    String fname = "symantic.txt";
			
				String d=root+fname;
				try (FileWriter file = new FileWriter(d)) {
					file.write(obj.toJSONString());
					System.out.println("MS Successfully Copied JSON Object to File..."+d);
					//System.out.println("\nJSON Object: " + obj);
				} catch (IOException e) {
					
					e.printStackTrace();
					
				}
	 }
	 
	 public static void addValues(String key, String[] index) {
		   ArrayList tempList = null;
		   if (hashMap.containsKey(key)) {
		      tempList = hashMap.get(key);
		      if(tempList == null)
		         tempList = new ArrayList();
		      tempList.add(index);  
		   } else {
		      tempList = new ArrayList();
		      tempList.add(index);               
		   }
		   hashMap.put(key,tempList);
		}
	 

	 
	
}
