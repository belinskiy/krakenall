package com.bgv.dao.probability;

import java.util.ArrayList;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class SubClaims {
	String id;
	Mainsnak mainsnak;
	String type;
	String rank;
	
	@SerializedName("qualifiers")
	private Map<String, ArrayList<SubQualifiers>> qualifiers;
	
	
	@SerializedName("references")
	ArrayList<SubReferences> references;
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Mainsnak getMainsnak() {
		return mainsnak;
	}

	public void setMainsnak(Mainsnak mainsnak) {
		this.mainsnak = mainsnak;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public Map<String, ArrayList<SubQualifiers>> getQualifiers() {
		return qualifiers;
	}

	public void setQualifiers(Map<String, ArrayList<SubQualifiers>> qualifiers) {
		this.qualifiers = qualifiers;
	}

	public ArrayList<SubReferences> getReferences() {
		return references;
	}

	public void setReferences(ArrayList<SubReferences> references) {
		this.references = references;
	}

	public class Mainsnak {
		
		String	snaktype;
		
		String	property;
		String datatype;
		String type;
		String rank;
		Datavalue datavalue;
		public String getSnaktype() {
			return snaktype;
		}
		public void setSnaktype(String snaktype) {
			this.snaktype = snaktype;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getDatatype() {
			return datatype;
		}
		public void setDatatype(String datatype) {
			this.datatype = datatype;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getRank() {
			return rank;
		}
		public void setRank(String rank) {
			this.rank = rank;
		}
		public Datavalue getDatavalue() {
			return datavalue;
		}
		public void setDatavalue(Datavalue datavalue) {
			this.datavalue = datavalue;
		}
		
		
	}
	
	public class Datavalue {
//		String value;
//
//		public String getValue() {
//			return value;
//		}
//
//		public void setValue(String value) {
//			this.value = value;
//		}
		
		String type;
		Object value;

		public Object getValue() {
			return value;
		}

		public void setValue(Value value) {
			this.value = value;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
		
		
		
	}
	
	public class Value {
		@SerializedName("entity-type")
		String	entitytype;
		@SerializedName("numeric-id")
		String	numericid;
		public String getEntitytype() {
			return entitytype;
		}
		public void setEntitytype(String entitytype) {
			this.entitytype = entitytype;
		}
		public String getNumericid() {
			return numericid;
		}
		public void setNumericid(String numericid) {
			this.numericid = numericid;
		}
		
		
	}
	
}
