package com.bgv.dao.probability;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.bgv.springmvc.domain.Config;
import com.bgv.springmvc.domain.Objects;

import opennlp.tools.util.InvalidFormatException;

public class SLOM {
	
	static Map<String, String> mapword = new HashMap<String, String>();
	static Map<String, String> mapcategory = new HashMap<String, String>();
	static Map<String, ArrayList> hashMap = new HashMap<String, ArrayList>();
	
	 final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	    final static String DB_URL = "jdbc:mysql://172.17.53.12/ruwiki";

	   
	    final static String USER = "admin";
	    final static String PASS = "kjwQ5%6JYgmu";
	   
	 public static void main(String[] args) throws Exception {
		 
		 Connection conn = null;
		    Statement stmt = null;
		    try{
		      
		       Class.forName("com.mysql.jdbc.Driver");

		     
		       System.out.println("Connecting to a selected database...");
		       conn = DriverManager.getConnection(DB_URL, USER, PASS);
		       System.out.println("Connected database successfully...");
		       
		      
		       System.out.println("Creating statement...");
		       stmt = conn.createStatement();

		       String sql = "SELECT lang_pos.id AS id, page_title AS entry1, wiki_text.text AS entry2, relation_type.name AS relation_name FROM lang_pos JOIN page ON lang_pos.page_id = page.id JOIN meaning ON meaning.lang_pos_id = lang_pos.id JOIN relation ON relation.meaning_id = meaning.id JOIN wiki_text ON wiki_text.id = relation.wiki_text_id JOIN relation_type ON relation_type.id = relation.relation_type_id WHERE lang_id = 804";
		       ResultSet rs = stmt.executeQuery(sql);
		     
		       while(rs.next()){
		    	 
		    	  String id  = rs.getString("entry1");
		          String ttext = rs.getString("entry2");
		          String tname = rs.getString("relation_name");
		          
		          String[] index = new String[2];
					index[0]=ttext;
					index[1]=tname;
		          
		          mapword.put(id, ttext); 
		          mapcategory.put(ttext, ttext);
		          addValues(id, index);
		          
		         // System.out.println(""+id+" "+ttext+" "+tname);
		          
		        
		       }
		       rs.close();
		    }catch(SQLException se){
		       //Handle errors for JDBC
		       se.printStackTrace();
		    }catch(Exception e){
		       //Handle errors for Class.forName
		       e.printStackTrace();
		    }finally{
		       //finally block used to close resources
		       try{
		          if(stmt!=null)
		             conn.close();
		       }catch(SQLException se){
		       }// do nothing
		       try{
		          if(conn!=null)
		             conn.close();
		       }catch(SQLException se){
		          se.printStackTrace();
		       }//end finally try
		    }//end try
		    
		    createFile();
		    
		    
		    
		    String word="женщина";		 
		    ArrayList tempdoList = null;
		    tempdoList = hashMap.get(word);
		    if(tempdoList !=null){
		    	for (Object value: tempdoList) {
		    		
		    		  String[] index = new String[2];  
		        	  index =(String[]) value;
		             
		            	  System.out.println("Value : "+index[0]+" Type : "+index[1]);
		            
		    	}
		    }
		    
		    Map<String, String> countmap = new ConcurrentHashMap();
		    Map<String, String> unicmap = new ConcurrentHashMap();
		    countmap.put(word, word);
		    //Iterator<Map.Entry<String, String>> it = countmap.entrySet().iterator();
		    while (countmap!=null&&countmap.size()>0) {
		    	
		    for (Map.Entry<String, String> entry : countmap.entrySet()){
		    	//Map.Entry<String, String>entry = it.next();
		    	String d=entry.getKey();
		    	tempdoList = hashMap.get(d);
		    	if(tempdoList!=null)
		    	for (Object value: tempdoList) {
		        	  String[] index = new String[2];  
		        	  index =(String[]) value;
		        	  //гиперонимы
		              if(index[1].equals("hypernyms")){
		            	  if(!unicmap.containsKey(index[0])){
			            	  unicmap.put(index[0], index[0]);
				            	  System.out.println("Value : "+index[0]+" Type : "+index[1]);
					            	  Map<String, String> m = getHypernyms(index[0]);
					            	  if(m!=null){
					            		  for (Map.Entry<String, String> s : m.entrySet())  
					            			  countmap.put(s.getKey(), s.getValue());
					            	  }
			            	  } 
		              }
		              countmap.remove(d); 
		              
		          }
		    	else countmap.remove(d);
		    }
		    
		    
		    }
		    
		    
//		    mapword.get(word);
//		    
//		    int k=0;
//		    for (Map.Entry<String, String> entry : mapcategory.entrySet())
//		    {
//		    	if(entry.getValue().equals("им"))
//		        System.out.println(""+k+" "+entry.getValue());
//		        k++;
//		    }
		    
		    /*
		    Iterator it = hashMap.keySet().iterator();
		    ArrayList tempList = null;

		    while (it.hasNext()) {
		       String key = it.next().toString();             
		       tempList = hashMap.get(key);
		       if (tempList != null) {
		          for (Object value: tempList) {
		        	  String[] index = new String[2];  
		        	  index =(String[]) value;
		             System.out.println("Key : "+key+ " , Value : "+index[0]+" Type : "+index[1]);
		          }
		       }
		    }
		    */
	 }
	 
	 
	 public static void addValues(String key, String[] index) {
		   ArrayList tempList = null;
		   if (hashMap.containsKey(key)) {
		      tempList = hashMap.get(key);
		      if(tempList == null)
		         tempList = new ArrayList();
		      tempList.add(index);  
		   } else {
		      tempList = new ArrayList();
		      tempList.add(index);               
		   }
		   hashMap.put(key,tempList);
		}
	 
	 public static Map getHypernyms(String word){
		 Map hashmap = new ConcurrentHashMap();
		 ArrayList tempdoList = null;
		    tempdoList = hashMap.get(word);
		    if(tempdoList !=null){
		    	for (Object value: tempdoList) {
		    		//поиск
		    		  String[] index = new String[2];  
		        	  index =(String[]) value;
		              if(index[1].equals("hypernyms")){
		            	  System.out.println(word+" Value : "+index[0]+" Type : "+index[1]);
		            	  hashmap.put(index[0], index[0]);  
		            	  
		              }
		    	}
		    	return hashmap;
		    }
		    else return null;
		    
			//return hashmap;
	 }
	 
	 
	 public static void createFile(){
		 
		    JSONObject obj = new JSONObject();
				obj.put("Name", "NLP");
				obj.put("Author", "BGVv1");
				
				JSONArray company = new JSONArray();
				
				
		   
		  
		    Iterator it = hashMap.keySet().iterator();
		    ArrayList tempList = null;
		    int i=1;
		    while (it.hasNext()) {
		       String key = it.next().toString();             
		       tempList = hashMap.get(key);
		       if (tempList != null) {
		          for (Object value: tempList) {
		        	  String[] index = new String[2];  
		        	  index =(String[]) value;
		            // System.out.println("Key : "+key+ " , Value : "+index[0]+" Type : "+index[1]);
		    
		    
		    
		   // for (Objects dept : depts) {
		        
		        JSONObject objs = new JSONObject();
		        objs.put("id", i);
		        objs.put("entry1", key);
		        objs.put("entry2", index[0]);
		        objs.put("relation_name", index[1]);	        
				company.add(objs);
				i++;
				
				
				obj.put("Company List", company);
		    //}
		    
      }
		       }
		    }
		    Config cf=new Config();
		    String root = "E:/Share/C/Downloads/";
		    String fname = "symantic.txt";
			
				String d=root+fname;
				try (FileWriter file = new FileWriter(d)) {
					file.write(obj.toJSONString());
					System.out.println("MS Successfully Copied JSON Object to File..."+d);
					//System.out.println("\nJSON Object: " + obj);
				} catch (IOException e) {
					
					e.printStackTrace();
					
				}
	 }
	 
	 public void loadSymantex(String textFileName) throws InvalidFormatException, IOException, ParseException{
		   File file=new File(textFileName);
           
           FileInputStream inStream=new FileInputStream(file);
           InputStreamReader reader=new InputStreamReader(inStream,"UTF-8");

      
 
     JSONParser parser = new JSONParser();
//Linux
//     Object obj = parser.parse(new BufferedReader(reader));
    		
    //Windows		
		    Object obj = parser.parse(new FileReader(textFileName));
		
		    JSONObject jsonObject = (JSONObject) obj;
		
		   
		    JSONArray companyList = (JSONArray) jsonObject.get("Company List");
		
		    Iterator<JSONObject> iterator = companyList.iterator();
			    while (iterator.hasNext()) {
			    	
				    JSONObject json = iterator.next();
				    String ids = (String) ""+json.get("id");
			        String entry1 = (String) json.get("entry1");
			        String entry2 = (String) ""+json.get("entry2");
			        String relation_name = (String) ""+json.get("relation_name");
			          String[] index = new String[2];
						index[0]=entry2;
						index[1]=relation_name;
			          
			          mapword.put(entry1, entry2); 
			          mapcategory.put(entry2, entry2);
			          addValues(entry1, index);
			    }
	 }
}
