package com.bgv.dao.probability;
import java.util.ArrayList;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class LibItem {
/*
  "descriptions": {},

 
*/
	
	
	  String  id;
	  String   type;
	  String   lastrevid;
	  String   modified;
	  
	  
	@SerializedName("labels")
	private Map<String, Labels>  labels;
	
	
	@SerializedName("aliases")
	private Map<String,ArrayList<Aliases>> aliases;
	
	@SerializedName("claims")
	private Map<String, ArrayList<SubClaims>> claims;
	
	 
	public Map<String,ArrayList<Aliases>> getAliases() {
		return aliases;
	}
	public void setAliases(Map<String,ArrayList<Aliases>> aliases) {
		this.aliases = aliases;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

public Map<String, Labels>  getLabels() {
		return labels;
	}
	public void setLabels(Map<String, Labels> labels) {
		this.labels = labels;
	}

	public String getLastrevid() {
		return lastrevid;
	}
	public void setLastrevid(String lastrevid) {
		this.lastrevid = lastrevid;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	@Override
	public String toString() {
		return "LibItem [id=" + id + ", type=" + type + ", lastrevid=" + lastrevid + ", modified=" + modified + "]";
	}
	public Map<String, ArrayList<SubClaims>> getClaims() {
		return claims;
	}
	public void setClaims(Map<String, ArrayList<SubClaims>> claims) {
		this.claims = claims;
	}

	  
	 
	    
}
