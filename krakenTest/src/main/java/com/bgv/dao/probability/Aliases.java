package com.bgv.dao.probability;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Aliases {
	
	String language;
	String value;
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	/*
	 @SerializedName("ru")
	 List<Langs> ln;

	
	public List<Langs> getLn() {
		return ln;
	}


	public void setLn(List<Langs> ln) {
		this.ln = ln;
	}


	public class Langs {
		
		String language;
		String value;
		public String getLanguage() {
			return language;
		}
		public void setLanguage(String language) {
			this.language = language;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	
		
	}
	*/
}
