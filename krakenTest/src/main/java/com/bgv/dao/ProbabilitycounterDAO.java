package com.bgv.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Probabilitycounter;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilitymain;
import com.bgv.entity.Probabilityobject;

@Transactional
@Repository
public class ProbabilitycounterDAO implements IProbabilitycounterDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	@Override
	public boolean addProbabilitycounter(Probabilitycounter counter) {
		hibernateTemplate.save(counter);
		return false;
	}

	@Override
	public List<Probabilitycounter> probabilityExists(String tokentext) {
		String hql = "FROM Probabilitycounter as p WHERE p.thash = :thash";
		@SuppressWarnings("unchecked")
		List<Probabilitycounter> prob = (List<Probabilitycounter>) hibernateTemplate.findByNamedParam(hql, "thash" ,tokentext);
		return prob;
	}
}
