package com.bgv.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Probabilityadjective;
import com.bgv.entity.Probabilityverbn;
import com.bgv.memory.FastModelDAO;

@Transactional
@Repository
public class ProbabilityverbnDAO implements IProbabilityverbnDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	 @Autowired
		private FastModelDAO fastMemoryModel;
	 
	@Override
	public boolean addProbabilitycounter(Probabilityverbn counter) {
		List<Probabilityverbn> prob = probabilityExists(counter.getNounname(), counter.getVerbname());
		if(prob!=null){
			if(prob.size() > 0) {
				for(Probabilityverbn p:prob)
				{			
					if(p!=null){
						p.setTotalvalue(p.getTotalvalue()+1);	
						hibernateTemplate.update(p);
					}
				}
				
			}else hibernateTemplate.save(counter);
			
		}else
		hibernateTemplate.save(counter);
		return false;
	}
	@Override
	public List<Probabilityverbn> getNounnameFromVerb(String verbname) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbn where verbname = :verbname";
		 Query q = session.createQuery(hql).setParameter("verbname", verbname);
		 return q.list();
		
	}
	
	@Override
	public List<Probabilityverbn> probabilityExists(String nounname,String verbname) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbn where nounname = :nounname AND verbname = :verbname";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname);
		 return q.list();
		
	}
	
	@Override
	public List<Probabilityadjective> probabilityExistsadj(String nounname, String adjective) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityadjective where nounname = :nounname AND adjective = :adjective";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("adjective", adjective);
		 return q.list();
		
	}
	
	@Override
	 public List<Probabilityverbn> getAllByPage(final int pageSize, final int pageNumber, final Timestamp dateupdate) {
	        HibernateTemplate template =  this.hibernateTemplate;
	        return (List<Probabilityverbn>) template.execute(new HibernateCallback() {
	            public Object doInHibernate(Session session) {
	                Query query = session.createQuery("SELECT p FROM Probabilityverbn p");
	                query.setMaxResults(pageSize);
	                query.setFirstResult(pageSize * pageNumber);
	                return query.list();
	            }
	        });
	    }	
	
	 @Override
	 public void getLarge() {
		 Map<String, Integer> ilemmanounverbdb= new HashMap<String, Integer>();
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 ScrollableResults scResults = session.createCriteria(Probabilityverbn.class)
	             .scroll(ScrollMode.FORWARD_ONLY);
					while(scResults.next()) {
						Probabilityverbn dirty = (Probabilityverbn)scResults.get(0);
						
						 String noun=dirty.getNounname().toLowerCase();
						 String verb=dirty.getVerbname().toLowerCase();
						   int tot = dirty.getTotalvalue();
						 
						  if(ilemmanounverbdb.containsKey(noun+" "+verb)){
							int tots=ilemmanounverbdb.get(noun+" "+verb);
							
							ilemmanounverbdb.put(noun+" "+verb, tot+tots);
							
						  }else ilemmanounverbdb.put(noun+" "+verb, tot);
						
					}
					
					fastMemoryModel.cleareLemaNounAndVerb();
					fastMemoryModel.setLemmanounverbdb(ilemmanounverbdb);
	    }	
	
	
	    @Override
		public Long countText() {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "select count(*) from Probabilityverbn";
			 Query q = session.createQuery(hql);
			 //q.executeUpdate();
			 
			 Long count = (Long) q.uniqueResult();
		
			 return count;
	    }
	    
		@Override
		public void addProbabilitycounteradj(Probabilityadjective countern) {
			List<Probabilityadjective> prob = probabilityExistsadj(countern.getNounname(), countern.getAdjective());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilityadjective p:prob)
					{			
						if(p!=null){
							p.setTotalvalue(p.getTotalvalue()+1);	
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(countern);
				
			}else
			hibernateTemplate.save(countern);
		
		}
}
