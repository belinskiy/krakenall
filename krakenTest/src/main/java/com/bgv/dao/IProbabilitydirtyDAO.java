package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Probabilitydirty;

public interface IProbabilitydirtyDAO {

	boolean addProbabilitydirty(Probabilitydirty dirty);

	List<Probabilitydirty> getAlldirtytext();

	void deleteText();

	void deleteText(Timestamp dateupdate);

	List<Probabilitydirty> getAllDocuments(Timestamp dateupdate);

	Long countText(Timestamp dateupdate);

	List<Probabilitydirty> getAllByPage(int pageSize, int pageNumber, Timestamp dateupdate);

}
