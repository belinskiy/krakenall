package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Probabilityadjective;
import com.bgv.entity.Probabilityverbn;

public interface IProbabilityverbnDAO {
	boolean addProbabilitycounter(Probabilityverbn counter);
	
	List<Probabilityverbn> probabilityExists(String nounname, String verbname);

	List<Probabilityverbn> getAllByPage(int pageSize, int pageNumber, Timestamp dateupdate);

	Long countText();

	void getLarge();

	List<Probabilityverbn> getNounnameFromVerb(String verbname);

	void addProbabilitycounteradj(Probabilityadjective countern);

	List<Probabilityadjective> probabilityExistsadj(String nounname, String adjective);
}
