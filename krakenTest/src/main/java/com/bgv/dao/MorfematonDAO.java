package com.bgv.dao;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Morfematon;




@Transactional
@Repository
public class MorfematonDAO implements IMorfematonDAO {
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	@Override
	public Morfematon getMorfematonById(int pid) {
		return hibernateTemplate.get(Morfematon.class, pid);
	}
	@SuppressWarnings("unchecked")
	
	 @Override
	 public void getLarge() {
		 Map<String, Integer> ilemmanounverbdb= new HashMap<String, Integer>();
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 ScrollableResults scResults = session.createCriteria(Morfematon.class)
	             .scroll(ScrollMode.FORWARD_ONLY);
					while(scResults.next()) {
						Morfematon dirty = (Morfematon)scResults.get(0);
						/*
						 String noun=dirty.getNounname().toLowerCase();
						 String verb=dirty.getVerbname().toLowerCase();
						   int tot = dirty.getTotalvalue();
						 
						  if(ilemmanounverbdb.containsKey(noun+" "+verb)){
							int tots=ilemmanounverbdb.get(noun+" "+verb);
							
							ilemmanounverbdb.put(noun+" "+verb, tot+tots);
							
						  }else ilemmanounverbdb.put(noun+" "+verb, tot);
						*/
					}
					
					//fastMemoryModel.cleareLemaNounAndVerb();
					//fastMemoryModel.setLemmanounverbdb(ilemmanounverbdb);
	    }	
	
	
	@Override
	public boolean addMorfematon(Morfematon person) {
		hibernateTemplate.save(person);
		return false;
	}
	@Override
	public void updateMorfematon(Morfematon person) {
//		Morfematon p = getMorfematonById(person.getPid());
//		p.setUsername(person.getUsername());
//		p.setPassword(person.getPassword());
//		p.setAge(person.getAge());
//		p.setGender(person.getGender());
//		p.setCity(person.getCity());
//		hibernateTemplate.update(p);
	}
	@Override
	public void deleteMorfematon(int pid) {
		hibernateTemplate.delete(getMorfematonById(pid));
	}
	@SuppressWarnings("unchecked")
	@Override
	public boolean morfematonExists(String aword, String vword, String bword, int awordton, int vwordton, int bwordton) {
		 int al=0;
		 int vl=0;
		 int bl=0;
		 
		  try{al=aword.length();}catch (Exception e) {}
		  try{vl=vword.length();}catch (Exception e) {}
		  try{bl=bword.length();}catch (Exception e) {}
		  
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql ="";
		 Query q = null;
		 if(al>1&&vl>1&&bl>1){
		     hql = "FROM Morfematon where aword = :aword AND vword = :vword AND bword = :bword";
		         q = session.createQuery(hql).setParameter("aword", aword).setParameter("vword", vword).setParameter("bword", bword);
		 }
		 else if(al>1&&vl>1&&bl<1){
			  hql = "FROM Morfematon where aword = :aword AND vword = :vword AND bword IS NULL";
				 q = session.createQuery(hql).setParameter("aword", aword).setParameter("vword", vword);
		 }
		 else if(al>1&&vl<1&&bl>1){
			  hql = "FROM Morfematon where aword = :aword AND vword IS NULL AND bword = :bword";
				 q = session.createQuery(hql).setParameter("aword", aword).setParameter("bword", bword);
		 }
		 else if(al<1&&vl>1&&bl>1){
			  hql = "FROM Morfematon where aword IS NULL AND vword = :vword AND bword = :bword";
				 q = session.createQuery(hql).setParameter("vword", vword).setParameter("bword", bword);
		 }
		 List<Morfematon> persons = q.list();
		
		 if(persons.size() > 0){
			for(Morfematon mr:persons){
				Morfematon mrin=new Morfematon();
				mrin.setId(mr.getId());
				
				if(al<1)
				mrin.setAword(mr.getAword());
				else
				mrin.setAword(aword);
				if(awordton<-1)
				mrin.setAwordton(mr.getAwordton());
				else
				mrin.setAwordton(awordton);	
				
				if(bl<1)
				mrin.setBword(mr.getBword());
				else
				mrin.setBword(bword);
				
				if(bwordton<-1)
				mrin.setBwordton(mr.getBwordton());
				else
				mrin.setBwordton(bwordton);
				
				if(vl<-1)
				mrin.setVword(mr.getVword());
				else
				mrin.setVword(vword);
				
				
				if(vwordton<-1)
				mrin.setVwordton(mr.getVwordton());
				else 
				mrin.setVwordton(vwordton);
			}
			
		 }else{
			 Morfematon mrin=new Morfematon();
			
				mrin.setAword(aword);
				mrin.setAwordton(awordton);
				
				mrin.setBword(bword);
				mrin.setBwordton(bwordton);
				
				mrin.setVword(vword);
				mrin.setVwordton(vwordton);
			 hibernateTemplate.save(mrin);
		 }
		return false;
		
		
		
		
	}
	

} 