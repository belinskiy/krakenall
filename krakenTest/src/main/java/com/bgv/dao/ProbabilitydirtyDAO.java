package com.bgv.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Person;
import com.bgv.entity.Probabilitydirty;


@Transactional
@Repository
public class ProbabilitydirtyDAO implements IProbabilitydirtyDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean addProbabilitydirty(Probabilitydirty dirty) {
		hibernateTemplate.save(dirty);
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Probabilitydirty> getAlldirtytext() {
		String hql = "FROM Probabilitydirty as p ORDER BY p.pid";
		return (List<Probabilitydirty>) hibernateTemplate.find(hql);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void deleteText() {

		sessionFactory.getCurrentSession().createSQLQuery("truncate table probabilitydirty").executeUpdate();
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void deleteText(Timestamp dateupdate) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "delete Probabilitydirty where dateupdate < :dateupdate";
		 Query q = session.createQuery(hql).setParameter("dateupdate", dateupdate);
		 q.executeUpdate();
		    
		    
		 //hibernateTemplate.bulkUpdate("delete from Probabilitydirty where dateupdate < "+dateupdate);
		
	}
	@Override
	public List<Probabilitydirty> getAllDocuments(Timestamp dateupdate) {
		
		String query = "FROM Probabilitydirty p where p.dateupdate < :dateupdate ";
			    return (List<Probabilitydirty>)  this.hibernateTemplate.findByNamedParam(query, "dateupdate", dateupdate);
			    
	}	
	
	@Override
	 public List<Probabilitydirty> getAllByPage(final int pageSize, final int pageNumber, final Timestamp dateupdate) {
	        HibernateTemplate template =  this.hibernateTemplate;
	        return (List<Probabilitydirty>) template.execute(new HibernateCallback() {
	            public Object doInHibernate(Session session) {
	                Query query = session.createQuery("from Probabilitydirty where dateupdate < :dateupdate").setParameter("dateupdate", dateupdate);
	                query.setMaxResults(pageSize);
	                query.setFirstResult(pageSize * pageNumber);
	                return query.list();
	            }
	        });
	    }
	
	 @Override
		public Long countText(Timestamp dateupdate) {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "select count(*) from Probabilitydirty where dateupdate < :dateupdate";
			 Query q = session.createQuery(hql).setParameter("dateupdate", dateupdate);
			 //q.executeUpdate();
			 
			 Long count = (Long) q.uniqueResult();
		
			 return count;
	    }
	 
		//Integer count = (Integer) sessionFactory().getSession().createQuery("select count(*) from Employee").uniqueResult();

}
