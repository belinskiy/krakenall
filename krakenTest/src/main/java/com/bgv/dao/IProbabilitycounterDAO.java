package com.bgv.dao;

import java.util.List;

import com.bgv.entity.Probabilitycounter;

public interface IProbabilitycounterDAO {


	boolean addProbabilitycounter(Probabilitycounter counter);

	List<Probabilitycounter> probabilityExists(String tokentext);

}
