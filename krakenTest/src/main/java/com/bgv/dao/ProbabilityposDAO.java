package com.bgv.dao;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Person;
import com.bgv.entity.Probabilitypos;

@Transactional
@Repository
public class ProbabilityposDAO implements IProbabilityposDAO{
	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	@Override
	public boolean addProbabilitypos(Probabilitypos probabilitypos) {
		hibernateTemplate.save(probabilitypos);
		return false;
	}
}
