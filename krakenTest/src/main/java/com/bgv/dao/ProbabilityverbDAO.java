package com.bgv.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.lucene.morphology.LuceneMorphology;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.bgv.entity.Horosho;
import com.bgv.entity.Ploho;
import com.bgv.entity.Probabilitybwa;
import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbexpl;
import com.bgv.entity.Probabilityverbn;
import com.bgv.entity.Probabilityverbtrip;
import com.bgv.entity.Probabilityverbtrippp;
import com.bgv.entity.Probabilityverbtripqcq;
import com.bgv.entity.Probabilityverbtripqq;
import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.obj.ObjDAO;
import com.bgv.service.IProbabilityverbnService;

@Transactional
@Repository
public class ProbabilityverbDAO implements IProbabilityverbDAO{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	 @Autowired
		private FastModelDAO fastMemoryModel;

	 
	 
	@Override
	public boolean addProbabilitycounter(Probabilityverb counter) {
		List<Probabilityverb> prob = probabilityExists(counter.getNounname(), counter.getVerbname());
		if(prob!=null){
			if(prob.size() > 0) {
				for(Probabilityverb p:prob)
				{			
					if(p!=null){
						p.setTotalvalue(p.getTotalvalue()+1);	
						hibernateTemplate.update(p);
					}
				}
				
			}else hibernateTemplate.save(counter);
			
		}else
		hibernateTemplate.save(counter);
		return false;
	}

	@Override
	public List<Probabilityverb> probabilityExists(String nounname,String verbname) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverb where nounname = :nounname AND verbname = :verbname";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname);
		 return q.list();
		
	}
	@Override
	public List<Probabilityverbtrip> probabilityExistsTriplet(String nounname,String verbname, String nounnamet) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbtrip where nounname = :nounname AND verbname = :verbname AND nounnamet = :nounnamet";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname).setParameter("nounnamet", nounnamet);
		 return q.list();
		
	}
	@Override
	public List<Probabilityverbtripqq> probabilityExistsTripletqq(String nounname,String verbname, String nounnamet) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbtripqq where nounname = :nounname AND verbname = :verbname AND nounnamet = :nounnamet";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname).setParameter("nounnamet", nounnamet);
		 return q.list();
		
	}
	@Override
	public List<Probabilityverbtripqcq> probabilityExistsTripletqcq(String nounname,String verbname, String nounnamet) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbtripqcq where nounname = :nounname AND verbname = :verbname AND nounnamet = :nounnamet";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname).setParameter("nounnamet", nounnamet);
		 return q.list();
		
	}
	
	@Override
	public List<Probabilityverbtrippp> probabilityExistsTripletPredlog(String nounname,String verbname, String nounnamet) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbtrippp where nounname = :nounname AND verbname = :verbname AND nounnamet = :nounnamet";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname).setParameter("nounnamet", nounnamet);
		 return q.list();
		
	}
	
	@Override
	public List<Horosho> probabilityExistsHorosho(String word) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Horosho where word = :word";
		 Query q = session.createQuery(hql).setParameter("word", word);
		 return q.list();
		
	}
	
	@Override
	public List<Ploho> probabilityExistsPloho(String word) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Ploho where word = :word";
		 Query q = session.createQuery(hql).setParameter("word", word);
		 return q.list();
		
	}
	
	@Override
	 public List<Probabilityverb> getAllByPage(final int pageSize, final int pageNumber, final Timestamp dateupdate) {
	        HibernateTemplate template =  this.hibernateTemplate;
	        return (List<Probabilityverb>) template.execute(new HibernateCallback() {
	            public Object doInHibernate(Session session) {
	                Query query = session.createQuery("SELECT p FROM Probabilityverb p");
	                query.setMaxResults(pageSize);
	                query.setFirstResult(pageSize * pageNumber);
	                return query.list();
	            }
	        });
	    }	
	
	    @Override
		public Long countText() {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "select count(*) from Probabilityverb";
			 Query q = session.createQuery(hql);
			 //q.executeUpdate();
			 
			 Long count = (Long) q.uniqueResult();
		
			 return count;
	    }
	    

		 
		 
		 @Override
		 public void getLargeUpdate() {}

		@Override
		public void addProbabilitycounterTriplet(Probabilityverbtrip counter) {
			List<Probabilityverbtrip> prob = probabilityExistsTriplet(counter.getNounname(), counter.getVerbname(),counter.getNounnamet());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilityverbtrip p:prob)
					{			
						if(p!=null){
							p.setTotalvalue(p.getTotalvalue()+1);	
							//p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
			hibernateTemplate.save(counter);
		
			
		}
		@Override
		public void addProbabilitycounterTripletqq(Probabilityverbtripqq counter) {
			List<Probabilityverbtripqq> prob = probabilityExistsTripletqq(counter.getNounname(), counter.getVerbname(),counter.getNounnamet());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilityverbtripqq p:prob)
					{			
						if(p!=null){
							//p.setTotalvalue(p.getTotalvalue()+1);	
							p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
			hibernateTemplate.save(counter);
		
			
		}
		
		
		@Override
		public void addProbabilitycounterTripletPredlog(Probabilityverbtrippp counter) {
			List<Probabilityverbtrippp> prob = probabilityExistsTripletPredlog(counter.getNounname(), counter.getVerbname(),counter.getNounnamet());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilityverbtrippp p:prob)
					{			
						if(p!=null){
							p.setTotalvalue(p.getTotalvalue()+1);	
							//p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
			hibernateTemplate.save(counter);	
		}	
		
	    
		 @Override
		 public void getLarge() {
			 Map<String, Integer>  inounverbdb=new HashMap<String, Integer>();
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 ScrollableResults scResults = session.createCriteria(Probabilityverb.class).scroll(ScrollMode.FORWARD_ONLY);
						while(scResults.next()) {
							Probabilityverb dirty = (Probabilityverb)scResults.get(0);
							
							 String noun=dirty.getNounname().toLowerCase();
							 String verb=dirty.getVerbname().toLowerCase();
							   int tot = dirty.getTotalvalue();
							 
							 if(inounverbdb.containsKey(noun+" "+verb)){
								int tots=inounverbdb.get(noun+" "+verb);
								         inounverbdb.put(noun+" "+verb, tot+tots);
								
							 }else inounverbdb.put(noun+" "+verb, tot);
							
						}
						
						fastMemoryModel.cleareNounAndVerb();
						fastMemoryModel.setNounverbdb(inounverbdb);
		    }	
		
		 
		 
		 @Override
		public void getLargeTriplet() {
			 Map<String, Integer>  inounverbdb=new HashMap<String, Integer>();
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 ScrollableResults scResults = session.createCriteria(Probabilityverbtrip.class).scroll(ScrollMode.FORWARD_ONLY);
						while(scResults.next()) {
							Probabilityverbtrip dirty = (Probabilityverbtrip)scResults.get(0);
							
							 String noun=dirty.getNounname().toLowerCase();
							 String verb=dirty.getVerbname().toLowerCase();
							 String nount=dirty.getNounnamet().toLowerCase();
							   int tot = dirty.getTotalvalue();
							 
							 if(inounverbdb.containsKey(noun+" "+verb+" "+nount)){
								int tots=inounverbdb.get(noun+" "+verb+" "+nount);
								         inounverbdb.put(noun+" "+verb+" "+nount, tot+tots);
								
							 }else inounverbdb.put(noun+" "+verb+" "+nount, tot);
							
						}
						
						fastMemoryModel.cleareNounAndVerbTriplet();
						fastMemoryModel.setNounverbdbTriplet(inounverbdb);
			
		}

		@Override
		public void addProbabilityPloho(Ploho counter) {
			List<Ploho> prob = probabilityExistsPloho(counter.getWord());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Ploho p:prob)
					{			
						if(p!=null){
							p.setTotal(p.getTotal()+1);	
							//p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
			hibernateTemplate.save(counter);	
			
		}

		@Override
		public void addProbabilityHorosho(Horosho counter) {
			List<Horosho> prob = probabilityExistsHorosho(counter.getWord());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Horosho p:prob)
					{			
						if(p!=null){
							p.setTotal(p.getTotal()+1);	
							//p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
			hibernateTemplate.save(counter);	
			
		}

		
		
		
		@Override
		public void addProbabilitycounterExpl(Probabilityverbexpl counter) {
			try{
//			List<Probabilityverbexpl> prob = probabilityExistsExpl(counter.getNounname(), counter.getVerbname(),counter.getNounnamet());
//			if(prob!=null){
//				if(prob.size() > 0) {
//					for(Probabilityverbexpl p:prob)
//					{			
//						if(p!=null){
//							p.setTotalvalue(p.getTotalvalue()+1);	
//							//p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
//							hibernateTemplate.update(p);
//						}
//					}
//					
//				}else hibernateTemplate.save(counter);
//				
//			}else
				
			List<Probabilityverbexpl> prob = probabilityExistsExpl(counter.getExptext());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilityverbexpl p:prob)
					{			
						if(p!=null){
							p.setTotalvalue(p.getTotalvalue()+1);	
							//p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
				
				
			hibernateTemplate.save(counter);
		 }catch(Exception e){e.printStackTrace();}
			
		}

		@Override
		public List<Probabilityverbexpl> probabilityExistsExpl(String text) {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "FROM Probabilityverbexpl where exptext = :exptext";
			 Query q = session.createQuery(hql).setParameter("exptext", text);
			 return q.list();
		}
		
		@Override
		public List<Probabilityverbexpl> probabilityExistsExpl(String nounname, String verbname, String nounnamet) {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "FROM Probabilityverbexpl where nounname = :nounname AND verbname = :verbname AND nounnamet = :nounnamet";
			 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname).setParameter("nounnamet", nounnamet);
			 return q.list();
		}

		@Override
		public void addProbabilitycounterTripletqcq(Probabilityverbtripqcq countern) {
			List<Probabilityverbtripqcq> prob = probabilityExistsTripletqcq(countern.getNounname(), countern.getVerbname(),countern.getNounnamet());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilityverbtripqcq p:prob)
					{			
						if(p!=null){
							//p.setTotalvalue(p.getTotalvalue()+1);	
							p.setTotalvalue(p.getTotalvalue()+countern.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(countern);
				
			}else
			hibernateTemplate.save(countern);
			
		}

		@Override
		public void addProbabilitycounterProperty(Probabilitybwa counter) {
			
			
			List<Probabilitybwa> prob = probabilityExistsbwa(counter.getBeforeword(), counter.getPropword(),counter.getAfterword());
			if(prob!=null){
				if(prob.size() > 0) {
					for(Probabilitybwa p:prob)
					{			
						if(p!=null){
							//p.setTotalvalue(p.getTotalvalue()+1);	
							p.setTotalvalue(p.getTotalvalue()+counter.getTotalvalue());
							hibernateTemplate.update(p);
						}
					}
					
				}else hibernateTemplate.save(counter);
				
			}else
			hibernateTemplate.save(counter);
			
			
		}

		private List<Probabilitybwa> probabilityExistsbwa(String beforeword, String propword, String afterword) {
			 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			 String hql = "FROM Probabilitybwa where beforeword = :beforeword AND propword = :propword AND afterword = :afterword";
			 Query q = session.createQuery(hql).setParameter("beforeword", beforeword).setParameter("propword", propword).setParameter("afterword", afterword);
			 return q.list();
		}
		 

}
