package com.bgv.dao;

import java.sql.Timestamp;
import java.util.List;

import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbsqc;
import com.bgv.entity.Probabilityverbsqq;

public interface IProbabilityverbqq {
	boolean addProbabilitycounter(Probabilityverbsqq counter);

	void addProbabilitycounterqc(Probabilityverbsqc countern);

	List<Probabilityverbsqc> probabilityExists(String nounname, String verbname);
	
}
