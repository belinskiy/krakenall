package com.bgv.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.lucene.morphology.LuceneMorphology;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;


import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbn;
import com.bgv.entity.Probabilityverbsqc;
import com.bgv.entity.Probabilityverbsqq;
import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.obj.ObjDAO;
import com.bgv.service.IProbabilityverbnService;

@Transactional
@Repository
public class ProbabilityverbqqDAO implements IProbabilityverbqq{

	@Autowired
	private HibernateTemplate  hibernateTemplate;
	
	 
	@Override
	public boolean addProbabilitycounter(Probabilityverbsqq counter) {
		
		hibernateTemplate.save(counter);
		return false;
	}

	@Override
	public void addProbabilitycounterqc(Probabilityverbsqc countern) {
		
		hibernateTemplate.save(countern);
	}
/*
	@Override
	public void addProbabilitycounterqc(Probabilityverbsqc countern) {
		List<Probabilityverbsqc> prob = probabilityExists(countern.getNounname(), countern.getVerbname());
		if(prob!=null){
			if(prob.size() > 0) {
				for(Probabilityverbsqc p:prob)
				{			
					if(p!=null){
						p.setTotalvalue(p.getTotalvalue()+countern.getTotalvalue());	
						hibernateTemplate.update(p);
					}
				}
				
			}else hibernateTemplate.save(countern);
			
		}else
		hibernateTemplate.save(countern);
		
		
	}
*/
	@Override
	public List<Probabilityverbsqc> probabilityExists(String nounname,String verbname) {
		 Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		 String hql = "FROM Probabilityverbsqc where nounname = :nounname AND verbname = :verbname";
		 Query q = session.createQuery(hql).setParameter("nounname", nounname).setParameter("verbname", verbname);
		 return q.list();
		
	}
		 

}
