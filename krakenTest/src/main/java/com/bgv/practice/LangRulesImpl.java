package com.bgv.practice;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.languagetool.JLanguageTool;
import org.languagetool.language.Russian;
import org.languagetool.rules.RuleMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.nlp.obj.NameFinderModelMEDAO;


@Service
public class LangRulesImpl implements LangRulesI{
	 @Autowired
	 private NameFinderModelMEDAO langDAO;
	 
	@Override
	public boolean[] semanticRuleSet(String word1, String morf1, String word2, String morf2,String word3, String morf3,String word4, String morf4){
		boolean[] index = new boolean[4];
		//правила семантического дерева
		//B-1-RULE-1//   [С — существительное], часть, [С — существительное]
		boolean ruleb1=false;
		if(isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",morf1)){if(word2.equals("часть")) if(isPresent("С (жр|мр|ср),(ед|мн),(вн|рд)",morf3)){ruleb1=true;} }
		
		//B-1-RULE-2//   [С — существительное], это часть, [С — существительное]
		boolean ruleb2=false;
		if(isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",morf1)){if(word2.equals("это")){ if(word3.equals("часть"))if(isPresent("С (жр|мр|ср),(ед|мн),(вн|рд)",morf4)){ruleb2=true;}} }
		
		//B-1-RULE-3 [С — существительное], состоит из, [С — существительное]
		boolean ruleb3=false;
		if(isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",morf1)){if(word2.equals("состоит")){ if(word3.equals("из"))if(isPresent("С (жр|мр|ср),(ед|мн),(вн|рд)",morf4)){ruleb3=true;}} }
		
		//B-1-RULE-4 [С — существительное], является частью, [С — существительное]
		boolean ruleb4=false;
		if(isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",morf1)){if(word2.equals("является")){ if(word3.equals("частью"))if(isPresent("С (жр|мр|ср),(ед|мн),(вн|рд)",morf4)){ruleb4=true;}} }
		
		index[0]=ruleb1;
		index[1]=ruleb2;
		index[2]=ruleb3;
		index[3]=ruleb4;
		
		return index;
	}
	
	@Override
	public boolean verbRuleSet(String word1, String morf1, String word2, String morf2,String word3, String morf3,String word4, String morf4){
		
		//Правила действий объекта
		//A-0-RULE-1//   [С — существительное, Г — глагол]
		boolean rulea1=false;
		if(isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",morf1)){if(isPresent("Г",morf2)){rulea1=true;}}
		
		return rulea1;
	}



	
	boolean isPresent(String serchword, String text){
		
		 String regex = "\\b"+serchword+"\\b";
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(text);
	    
	        while(matcher.find() == true){
	        	
	        	return true;
	        }
			return false;
	}
//-------------------Правила подлежащего и скозуемого----------------------------------------------------------------------------------------------	
	public boolean[] predicateRuleSet(String word1, String morf1, String word2, String morf2,String word3, String morf3,String word4, String morf4){
		boolean[] index = new boolean[2];
		//правила семантического дерева
		//B-1-RULE-1//   [С — существительное] или личным местоимением в именительном падеже (я, ты, он, она, оно, мы, вы, они).
		boolean ruleb1=false;
		if(isPresent("С (жр|мр|ср),(ед|мн),им",morf1)){ruleb1=true;}
		boolean ruleb2=false;
		if(isPresent("МС [0-9]л,(((жр|мр|ср),(ед|мн))|(ед|мн)),им",morf1)){ruleb2=true;}
		
//Кто что - можно преверять по базе объектов (я же не знаю что это сущ. какого-то там рода)
		
		index[0]=ruleb1;
		index[1]=ruleb2;
		
	//A. Простое глагольное сказуемое
		//1. одним глаголом в форме изъявительного, повелительного или условного наклонения.
		//2. устойчивым сочетанием глагольного характера
		//3. фразеологизмом - пляшет под чужую дудку
	//Список всех фразеологизмов
	//https://ru.wiktionary.org/wiki/%D0%9F%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5:%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%84%D1%80%D0%B0%D0%B7%D0%B5%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B7%D0%BC%D0%BE%D0%B2_%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%B3%D0%BE_%D1%8F%D0%B7%D1%8B%D0%BA%D0%B0
		
	//Б. Составное глагольное сказуемое
		
		return index;
	}
	@Override
	public boolean[] podlejasheeRuleSet(String morf){
		boolean[] index = new boolean[2];
		//правила семантического дерева
		//B-1-RULE-1//   [С — существительное] или личным местоимением в именительном падеже (я, ты, он, она, оно, мы, вы, они).
		boolean ruleb1=false;
		if(isPresent("С (жр|мр|ср),(ед|мн),(им|аббр)",morf)){ruleb1=true;}
		boolean ruleb2=false;
		if(isPresent("МС [0-9]л,(((жр|мр|ср),(ед|мн))|(ед|мн)),им",morf)){ruleb2=true;}
		
		index[0]=ruleb1;
		index[1]=ruleb2;
		
		return index;
		
	}
//---------------------Правила разделения на предложения и поиска вопрсов--------------------------------------------------------------------------
	@Override
	public Map<Integer,String> getSenteces(String subjectString) {
		Map<Integer,String> mapString = new HashMap<Integer,String>();
        String[] sentences = null;
        Pattern re = Pattern.compile(
            "# Match a sentence ending in punctuation or EOS.\n" +
            "[^.!?\\s]    # First char is non-punct, non-ws\n" +
            "[^.!?]*      # Greedily consume up to punctuation.\n" +
            "(?:          # Group for unrolling the loop.\n" +
            "  [.!?]      # (special) inner punctuation ok if\n" +
            "  (?!['\"]?\\s|$)  # not followed by ws or EOS.\n" +
            "  [^.!?]*    # Greedily consume up to punctuation.\n" +
            ")*           # Zero or more (special normal*)\n" +
            "[.!?]?       # Optional ending punctuation.\n" +
            "['\"]?       # Optional closing quote.\n" +
            "(?=\\s|$)", 
            Pattern.MULTILINE | Pattern.COMMENTS);
        Matcher reMatcher = re.matcher(subjectString);
        int i=0;
        while (reMatcher.find()) {
        	i++;
        	mapString.put(i, reMatcher.group());
            
          
        } 
        
        return mapString;
    } 	
	
	@Override
	public boolean hasVoprosSymbol(String str) {
		boolean t=false;
	    if (str != null && str.length() > 0) {
	      str = str.substring(str.length()-1);
	      
	      if(str.equals("?"))t=true;
	    }
	    return t;
	}
	public boolean voprosIsMissing(String str) throws IOException {
		JLanguageTool langTool = new JLanguageTool(new Russian());
		//JLanguageTool langTool =langDAO.getLanguageTool();
		langTool.disableRule("UPPERCASE_SENTENCE_START");
    	langTool.disableRule("WHITESPACE_RULE");
    	
    	List<RuleMatch> matches = langTool.check(str);
		
		for (RuleMatch match : matches) {
		  System.out.println("Возможно ошибка " +
		      match.getFromPos() + "-" + match.getToPos() + ": " +
		      match.getMessage());
		  System.out.println("Можно исправить(вар-ты): " +
		      match.getSuggestedReplacements());
		}
		return false;
		
	}
	
	//кто, что, какой, который, чей, каков, сколько, где, куда, когда, откуда, почему, зачем
	@Override
	public boolean narRuleSet(String morf1){
		
		//Правила наречи¤
		//¤а j Ќ
		//¤н j Ќ вопр
		//¤о j Ќ указат
		//¤п j Ќ разг
		boolean rulea1=false;
		if(isPresent(" Н вопр",morf1)){rulea1=true;}
		
		return rulea1;
	}

	@Override
	public boolean[] skazuemoeRule(String worda, String morfa, String wordb, String morfb) {
		boolean[] index = new boolean[2];
		boolean rulea1=false;
		boolean rulea2=false;
		if(isPresent("Г",morfa)){rulea1=true;}
		if(isPresent("ИНФИНИТИВ",morfa)){rulea1=true;}
		if(worda.equals("быть")){rulea2=true;}
		
		
		index[0]=rulea1;
		index[1]=rulea2;
		return index;
	}
	
	@Override
	public boolean deeprichastieRule(String morf) {
		boolean ruleb1=false;
		if(isPresent("ДЕЕПРИЧАСТИЕ|ПРИЧАСТИЕ",morf))
		{ruleb1=true;} 
		return ruleb1;
	}
	
	@Override
	public boolean sushRuleSet(String morf) {
		boolean ruleb1=false;
		if(isPresent("( С жр)|( С мр)|( С ср)",morf))
		{ruleb1=true;} 
		return ruleb1;
	}

	@Override
	public boolean prolognarRuleSet(String morf) {
		boolean ruleb1=false;
		if(isPresent("( ПРЕДК )|( Н)|( П )",morf))
		{ruleb1=true;} 
		return ruleb1;
	}
	
}
