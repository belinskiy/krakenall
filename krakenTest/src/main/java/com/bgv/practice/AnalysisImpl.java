package com.bgv.practice;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.languagetool.JLanguageTool;
import org.languagetool.rules.RuleMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bgv.dao.IObjectsDAO;
import com.bgv.entity.Horosho;
import com.bgv.entity.Ploho;
import com.bgv.entity.Probabilityadjective;
import com.bgv.entity.Probabilitybwa;
import com.bgv.entity.Probabilitydirty;
import com.bgv.entity.Probabilityverb;
import com.bgv.entity.Probabilityverbexpl;
import com.bgv.entity.Probabilityverbn;
import com.bgv.entity.Probabilityverbsqc;
import com.bgv.entity.Probabilityverbsqq;
import com.bgv.entity.Probabilityverbtrip;
import com.bgv.entity.Probabilityverbtrippp;
import com.bgv.entity.Probabilityverbtripqcq;
import com.bgv.entity.Probabilityverbtripqq;
import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.obj.NameFinderModelMEDAO;

import com.bgv.service.IProbabilityverbService;
import com.bgv.service.IProbabilityverbnService;
import com.bgv.service.IProbabilityverbqqService;
import com.bgv.service.IQnonameService;
import com.bgv.service.ITESTService;
import com.bgv.springmvc.domain.Config;
import com.bgv.springmvc.domain.Message;

import opennlp.tools.util.InvalidFormatException;

@Service
public class AnalysisImpl implements AnalysisDAO{
	static boolean firstrun=false; 
	@Autowired
		private FastModelDAO fastMemoryModel;
	 
	 @Autowired
		private LangRulesI langRules;
	 
	  @Autowired
	  private  CleanTextDAO cleanText;
	 
	  
	  @Autowired
	  private NameFinderModelMEDAO nameFinderModelMEDAO;
	 
	  @Autowired
		private IProbabilityverbService verbService;
	     
	  @Autowired
		private IProbabilityverbnService verbServicen;
	  
	  @Autowired
		private IProbabilityverbqqService verbServicenqq;
	  
	  
	  @Autowired
		 private IObjectsDAO objcs;
	  @Autowired
		 private  IQnonameService nonameService;
	  
	  @Autowired
		private ITESTService testService;
	  
	  @Override
		 public  Map<Integer,String> getSentecesFromString(String strincoming){
			
			 Map<Integer,String> stringsMap=langRules.getSenteces(strincoming);
			 
			 return stringsMap;
			 
		}
	  
	  
	  
	 @Override
	 public void thisIsVopros(String strincoming){
		
		 Map<Integer,String> stringsMap=langRules.getSenteces(strincoming);
		 Map<String, String> voprNarechia = nameFinderModelMEDAO.getvoprNarechiaMap();
		 Map<String, String> voprosiMap= fastMemoryModel.getVonrosiPrimeri();
		
		 
		 for (Map.Entry<Integer,String> entrys : stringsMap.entrySet())
		 {
			 Integer key = entrys.getKey();
			 String val = entrys.getValue();
			 boolean stateone=false;
			 boolean statetwo=false;
			 boolean statethree=false;
			 stateone = langRules.hasVoprosSymbol(val);
			 if(!stateone){
			 int[] level=new int[5];
			 level[0]=1;
			 level[1]=0;
			 level[2]=1;
			 level[3]=0;
			 level[4]=0;
			 
			 Map<Integer, List<String[]>> map = cleanText.getText(level, val);
			 for (Map.Entry<Integer, List<String[]>> entryscleaner : map.entrySet())
			 {List<String[]> value = null;
			    
				 Integer k = entryscleaner.getKey();
				 value = entryscleaner.getValue();
				 //System.out.println("k="+k);
				 for(String[] index:value){
					 //System.out.println("index[0]="+index[0]+" "+voprNarechia.containsKey(index[0]));
					 if(voprNarechia.containsKey(index[0])){
						 statetwo=true; 
					 }
					 
					 if(k==3){
						 //System.out.println("index[1]="+index[1]+" narRule="+langRules.narRuleSet(index[1]));
						 if(langRules.narRuleSet(index[1])){
							 statethree=true; 
						 }
						 
						 
					 }
				 }
			     
			 }
			 
			 
			 }
			 
		
		//ADD SENTESE TO MAP -  это вопросительное предложение
		//if((stateone&&statetwo)||statethree){
		if(stateone){	
			voprosiMap.put(val, "1");
		}	
		else if(statethree&&statetwo){
			voprosiMap.put(val, "2");
		} 
		else if(statethree){
			voprosiMap.put(val, "3");
		} 
		
		
		
		//Поискать в предложении фразу - (потому что) | (потому, что) | (потому как) | (оттого что) | поскольку | (так как) | Ибо | (Вследствие того что) | (Благодаря тому что) |(В силу того что)	 
		//boolean explain=isPresent("(потому что)|(потому, что)|(потому как)|(оттого что)|(оттого, что)|(так как)|(вследствие того что)",val); 
		boolean explain=isPresent("(потому что)|(потому, что)",val); 
		if(explain)
		{
			 Map<String, String> explainMap= fastMemoryModel.getExplainPrimeri();
			 explainMap.put(val, "3");
		}
			 
		 }
	 }
	 
	 
	@Override
	public void getRelationsObjectAndVerb(Map<Integer, List<String[]>> inmap){
		try{Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTime().getTime());
		List<String[]> alltokensProp = inmap.get(3);
		Map<String, Integer>  wordPhraseCount=new HashMap<String, Integer>();
		Map<String, Integer> wordRelationCount=new HashMap<String, Integer>();
		int size = alltokensProp.size();
		
		for (int i = 0; i < size; i++){
			String[] first=null;
			String[] second=null;
			String[] three=null;
			String[] four=null;
			
			
		String word1=""; 
		String morf1=""; 
		
		String word2=""; 
		String morf2="";
		
		String word3=""; 
		String morf3="";
		
		String word4=""; 
		String morf4="";
		
		first=alltokensProp.get(i);
		String firsttag=first[1];
		if(i+1<size){
			if(firsttag.contains("|")){
				String[] morfdata = firsttag.split("\\|");
				word1=first[0];
				//word1=morfdata[0];
				morf1=morfdata[1];	
			}
			
			
			second=alltokensProp.get(i+1);
			String secondtag=second[1];
			
			if(secondtag.contains("|")){
				String[] morfdata = secondtag.split("\\|");
				word2=second[0];
				//word2=morfdata[0];
				morf2=morfdata[1];	
			}
			
			
			if(i+2<size){
				three=alltokensProp.get(i+2);
				String threetag=three[1];	
				
				if(threetag.contains("|")){
					String[] morfdata = threetag.split("\\|");
					word3=three[0];
					//word3=morfdata[0];
					morf3=morfdata[1];	
				}
				
			}
			
			if(i+3<size){
				four=alltokensProp.get(i+3);
				String fourtag=four[1];	
				
				if(fourtag.contains("|")){
					String[] morfdata = fourtag.split("\\|");
					word4=four[0];
					//word4=morfdata[0];
					morf4=morfdata[1];	
				}
			}
		    boolean add=false;
			boolean addverb=false;	
	
			String firstphraseverb="";
			String secondphraseverb="";
				addverb=langRules.verbRuleSet(word1, morf1, word2, morf2, word3, morf3, word4, morf4);	
				
				if(addverb){
					firstphraseverb=word1;
					secondphraseverb=word2;
					
					if(wordPhraseCount.containsKey(firstphraseverb+" "+secondphraseverb)){
						int cont=wordPhraseCount.get(firstphraseverb+" "+secondphraseverb);
						wordPhraseCount.put(firstphraseverb+" "+secondphraseverb, cont+1); 
					}else
					wordPhraseCount.put(firstphraseverb+" "+secondphraseverb, 1);
					}
				
				
			}
		

		
		
		

		try{
			
			saveVerb(wordPhraseCount, timestamp);
			
		}catch(Exception e){e.printStackTrace();
		}
		
		try{
			
			//saveRelation(wordRelationCount, timestamp);
			
		}catch(Exception e){e.printStackTrace();
		}
		
	}}catch(Exception e){e.printStackTrace();}
	}
	
	
	
	
	void saveVerb(Map<String, Integer> wordCount,java.sql.Timestamp timestamp){
	
		
		try {	
			
			  
			    Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCount();

			    	  
			    	  for (Map.Entry<String, Integer> entry : wordCount.entrySet())
			    	  {
				    	   try{
					    		  String tokens = entry.getKey().trim();
					    		  Integer indx = entry.getValue();
					    		  if(probabilitymain.containsKey(tokens)){
					    			  int cont=probabilitymain.get(tokens);
					    			  //probabilitymain.put(tokens, cont+indx); 
					    			  probabilitymain.put(tokens, cont+1); 
					    		  }
					    		  else
					    			  probabilitymain.put(tokens, 1); 
				    		  
				    	      } catch (Exception ex){}
			  	       }
			    	  

				
				
			
		} catch (Exception ex){ex.printStackTrace();}
	}
	
	void saveRelation(Map<String, Integer> wordCount,java.sql.Timestamp timestamp){
	
		
		try {	
			
			  
			    Map<String, Integer> probabilityr=fastMemoryModel.getObjectRelationCount();

			    	  
			    	  for (Map.Entry<String, Integer> entry : wordCount.entrySet())
			    	  {
				    	   try{
					    		  String tokens = entry.getKey().trim();
					    		  Integer indx = entry.getValue();
					    		  if(probabilityr.containsKey(tokens)){
					    			  int cont=probabilityr.get(tokens);
					    			  //probabilitymain.put(tokens, cont+indx); 
					    			  probabilityr.put(tokens, cont+1); 
					    		  }
					    		  else
					    			  probabilityr.put(tokens, 1); 
				    		  
				    	      } catch (Exception ex){}
			  	       }
			    	  

				
				
			
		} catch (Exception ex){ex.printStackTrace();}
	}
	@Override
	 public void restReLoadQnoname() throws IOException {
	    	nonameService.getLarge();
		}
	@Override
    public void restLoadVerb() throws IOException {
		verbService.getLarge();
		verbServicen.getLarge();
		
		 Map<String, Integer>   inounverbdb=fastMemoryModel.getNounverbdb();
		 Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getLemmanounverbdb();
		 
		 
		 System.out.println("inounverbdb="+inounverbdb.size()); 
		 System.out.println("ilemmanounverbdb="+ilemmanounverbdb.size());
		 
		 
		 
		
//		 Map<String, Integer>  inounverbdb=new HashMap<String, Integer>();
//		 Map<String, Integer> ilemmanounverbdb= new HashMap<String, Integer>();
//		
//		Long mcount=verbService.countText();
//		
//		int totalpage=mcount.intValue(); 
//		int pageSize=1050;
//		int pages=totalpage/pageSize;
//		for(int ip=1; ip<pages; ip++){
//			List<Probabilityverb>  idata =verbService.getAllByPage(ip, pageSize, null);	
//			 
//			 for(Probabilityverb dirty:idata){
//				 String noun=dirty.getNounname().toLowerCase();
//				 String verb=dirty.getVerbname().toLowerCase();
//				 int tot = dirty.getTotalvalue();
//				 
//				 if(inounverbdb.containsKey(noun+" "+verb)){
//					int tots=inounverbdb.get(noun+" "+verb);
//					
//					inounverbdb.put(noun+" "+verb, tot+tots);
//					
//				 }else inounverbdb.put(noun+" "+verb, tot);
//				
//				   
//			 }
//			 
//		}
//		
//		fastMemoryModel.cleareNounAndVerb();
//		fastMemoryModel.setNounverbdb(inounverbdb);
//		
//		Long mcountn=verbServicen.countText();
//		
//		int totalpagen=mcountn.intValue(); 
//		int pageSizen=1050;
//		int pagesn=totalpagen/pageSizen;
//		for(int ip=1; ip<pagesn; ip++){
//			List<Probabilityverbn>  idata =verbServicen.getAllByPage(ip, pageSizen, null);	
//			
//			 for(Probabilityverbn dirty:idata){
//				 String noun=dirty.getNounname().toLowerCase();
//				 String verb=dirty.getVerbname().toLowerCase();
//				 int tot = dirty.getTotalvalue();
//				 
//				 if(ilemmanounverbdb.containsKey(noun+" "+verb)){
//					int tots=ilemmanounverbdb.get(noun+" "+verb);
//					
//					ilemmanounverbdb.put(noun+" "+verb, tot+tots);
//					
//				 }else ilemmanounverbdb.put(noun+" "+verb, tot);
//				
//				   
//			 }
//			 
//		}
//		
//		fastMemoryModel.cleareLemaNounAndVerb();
//		fastMemoryModel.setLemmanounverbdb(ilemmanounverbdb);
	}
	@Override
    public void compressVerb() throws IOException {
		
		Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getLemmanounverbdb();
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops();
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 
		
		for (Map.Entry<String, Integer> entry : ilemmanounverbdb.entrySet())
		{
		    
		   String worsd= entry.getKey();
		   String[] myData = worsd.split(" ");
			
			String noun=myData[0].toLowerCase().trim();			
			String verb=myData[1].toLowerCase().trim();
			
				
				 int tot = entry.getValue();
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			 //Сжать по human или organization или location - other
			 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid ="";
				   String savecategoryid ="";
				   
					if(objectid!=null){
						String syn = idcategorymap.get(objectid);
						if(syn!=null){
							if(syn.contains("/")){
					  			
					  		String[] newwords = syn.split("\\/");
					  		
					  		itypeid=newwords[0];
					  		icategoryid=newwords[1];
							}
							
							if(itypeid.equals("1")){sgeo=true;   
							 savetypeid ="Q17334923";
							 savecategoryid =icategoryid;}
							else if(itypeid.equals("3")){shuman=true; 
							 savetypeid ="Q5";
							 savecategoryid =icategoryid;}
	           			 	else if(itypeid.equals("2")){sorg=true;
	           			 	savetypeid ="Q43229";
	           			 	savecategoryid =icategoryid;}
						}
						
					}
					if(sgeo==false&&shuman==false&&sorg==false){
				//----------------------------Q---------------------------- 
					List<String> qindexList = iiltextList.get(noun);
					boolean canexit=false;
					
					 if(qindexList!=null)
					 for(String qindex:qindexList){
						 if(canexit)break;
					 if(qindex!=null){ 
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
						   if(canexit)break;
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            	 if(canexit)break;
						            //System.out.println("--------search-----="+vi.get(0));
						            String qdata=vi.get(0);
						            icount++;
						            //String words=getTextofQ(first, iiltext);
						            String wordsprop=getPropertyOfType(qdata, iiltype);

				            
						            
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(qdata)){
						            	List<String> listofinstance =null;
						            	List<String> listsubclassof =null;
						            	List<String> listotherprops =null;
						            	String label="";
						            	
					            		listofinstance =iilinstanceList.get(qdata);
					            		listsubclassof =iilsubclassofList.get(qdata);
					            		listotherprops =iilotherpropsList.get(qdata);
					            		
					            		
						            	//if(iorclass==1){
						            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
						            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
						            		//}
					            			
					            	if(listotherprops!=null){
					            		
					            		  for(String ins:listotherprops){
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
					            			  
					            		  }
					            		}
						            if(listofinstance!=null){
							            for(String ins:listofinstance){
							            	//System.out.println("--------ofinstance-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						            if(listsubclassof!=null){
							            for(String ins:listsubclassof){
							            	//System.out.println("--------subclassof-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						             vi.remove(qdata);
						            }
						            
						            
						            } while (vi.size()>0);
						            
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					   //itogo=sb.toString();
					 }}
					 }
				//--------------------------Save---------------------------	
					
				 Probabilityverbsqq countern= new Probabilityverbsqq();
				 //itypeid ="";
				 //icategoryid ="";
				   
				 countern.setCategoryid(icategoryid);
				 countern.setTypeid(savetypeid);
				 countern.setNounname(noun);
				 countern.setVerbname(verb);
				 countern.setTotalvalue(tot);
				 if(sgeo!=false||shuman!=false||sorg!=false)
				 verbServicenqq.addProbabilitycounter(countern); 

			 }
		}
	
	@Override
    public void compressVerbCategory() throws IOException {
		
		Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getLemmanounverbdb();
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops();
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 
		 Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
		 
		 
		for (Map.Entry<String, Integer> entry : ilemmanounverbdb.entrySet())
		{
		    
		   String worsd= entry.getKey();
		   String[] myData = worsd.split(" ");
			
			String noun=myData[0].toLowerCase().trim();			
			String verb=myData[1].toLowerCase().trim();
			
				
				 int tot = entry.getValue();
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			 //Сжать по human или organization или location - other
			 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid ="";
				    savetypeid =noun;
				   String savecategoryid ="";
				   
					if(objectid!=null){
						String syn = idcategorymap.get(objectid);
						if(syn!=null){
							if(syn.contains("/")){
					  			
					  		String[] newwords = syn.split("\\/");
					  		
					  		itypeid=newwords[0];
					  		icategoryid=newwords[1];
							}
							
							if(itypeid.equals("1")){sgeo=true;   
							 savetypeid ="Q17334923";
							 savecategoryid =icategoryid;}
							else if(itypeid.equals("3")){shuman=true; 
							 savetypeid ="Q5";
							 savecategoryid =icategoryid;}
	           			 	else if(itypeid.equals("2")){sorg=true;
	           			 	savetypeid ="Q43229";
	           			 	savecategoryid =icategoryid;}
						}
						
					}
					if(sgeo==false&&shuman==false&&sorg==false){
				//----------------------------Q---------------------------- 
					List<String> qindexList = iiltextList.get(noun);
					boolean canexit=false;
					
					 if(qindexList!=null)
					 for(String qindex:qindexList){
						 if(canexit)break;
					 if(qindex!=null){ 
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
						   if(canexit)break;
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            	 if(canexit)break;
						            //System.out.println("--------search-----="+vi.get(0));
						            String qdata=vi.get(0);
						            savetypeid =qdata;
						            icount++;
						            //String words=getTextofQ(first, iiltext);
						            String wordsprop=getPropertyOfType(qdata, iiltype);

				            
						            
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(qdata)){
						            	List<String> listofinstance =null;
						            	List<String> listsubclassof =null;
						            	List<String> listotherprops =null;
						            	String label="";
						            	
					            		listofinstance =iilinstanceList.get(qdata);
					            		listsubclassof =iilsubclassofList.get(qdata);
					            		listotherprops =iilotherpropsList.get(qdata);
					            		
					            		
						            	//if(iorclass==1){
						            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
						            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
						            		//}
					            			
					            	if(listotherprops!=null){
					            		
					            		  for(String ins:listotherprops){
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
					            			  
					            		  }
					            		}
						            if(listofinstance!=null){
							            for(String ins:listofinstance){
							            	//System.out.println("--------ofinstance-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						            if(listsubclassof!=null){
							            for(String ins:listsubclassof){
							            	//System.out.println("--------subclassof-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						             vi.remove(qdata);
						            }
						            
						            
						            } while (vi.size()>0);
						            
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					   //itogo=sb.toString();
					 }}
					 }
				//--------------------------Save---------------------------	
					
				 Probabilityverbsqc countern= new Probabilityverbsqc();
				 //itypeid ="";
				 //icategoryid ="";
				   
				 countern.setCategoryid(verb);
				 countern.setTypeid(noun);
				 countern.setNounname(savetypeid);
				 
				 countern.setVerbname(verb);
				 try{
					 String varbcat = verbglobalcategory.get(verb);
					 if(varbcat!=null){
						 if(!varbcat.equals("")){  countern.setVerbname(varbcat); } else countern.setVerbname("none");
					 } else countern.setVerbname("none");
					 }catch(Exception e){}
				 
				 
				 countern.setTotalvalue(tot);
				// if(sgeo!=false||shuman!=false||sorg!=false)
				 if(!countern.getVerbname().equals("none"))
				 verbServicenqq.addProbabilitycounterqc(countern); 

			 }
		}
	
	@Override
    public void compressVerbCategoryVersionB() throws IOException {
		
		Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getLemmanounverbdb();
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops();
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 
		 Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
		 Map<String, Probabilityverbsqc> store=new  HashMap<String, Probabilityverbsqc>();
		 Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		 Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
		for (Map.Entry<String, Integer> entry : ilemmanounverbdb.entrySet())
		{
		    
		   String worsd= entry.getKey();
		   String[] myData = worsd.split(" ");
			
			String noun=myData[0].toLowerCase().trim();			
			String verb=myData[1].toLowerCase().trim();
			
				
				 int tot = entry.getValue();
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			 //Сжать по human или organization или location - other
			 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid ="";
				    savetypeid =noun;
				   String savecategoryid ="";
				   
					if(objectid!=null){
						String syn = idcategorymap.get(objectid);
						if(syn!=null){
							if(syn.contains("/")){
					  			
					  		String[] newwords = syn.split("\\/");
					  		
					  		itypeid=newwords[0];
					  		icategoryid=newwords[1];
							}
							
							if(itypeid.equals("1")){sgeo=true;   
							 savetypeid ="Q17334923";
							 savecategoryid =icategoryid;}
							else if(itypeid.equals("3")){shuman=true; 
							 savetypeid ="Q5";
							 savecategoryid =icategoryid;}
	           			 	else if(itypeid.equals("2")){sorg=true;
	           			 	savetypeid ="Q43229";
	           			 	savecategoryid =icategoryid;}
						}
						
					}
				
				boolean foundverb=false;
				
				String varbcategory="none";
				
				
				 try{
					 String varbcat = verbglobalcategory.get(verb);
					 
					 if(varbcat!=null){
						 
						 if(!varbcat.equals("")){  varbcategory=varbcat; foundverb=true; } else foundverb=false; //countern.setVerbname("none");
					 
					 } else foundverb=false; //countern.setVerbname("none");
					 
				 }catch(Exception e){}
				 
				 
					try{
                        if(!foundverb){
							    //если не найдено верхнее значение или пределеное, поискать в синонимах или iSyn
							   
							    String syn = iwordSynonims.get(verb);
								if (syn != null) {
									String[] newwords;
									if (syn.contains("/")) {
	
										newwords = syn.split("\\/");
	
									}
									if (syn.contains(",")) {
	
										newwords = syn.split(",");
	
									} else {
	
										String[] synindex = new String[1];
										synindex[0] = syn;
										newwords = synindex;
									}
									int siz = newwords.length;
	
									for (int ik = 0; ik < siz; ik++) {
										// Если найдено значение
										// надо - выйти из цикла
										 if(foundverb)break;
										 String newnword = newwords[ik].trim();
											try{
												 String varbcat = verbglobalcategory.get(newnword);
												 if(varbcat!=null){
												 if(!varbcat.equals("")){	 
													 varbcategory=varbcat; foundverb=true;
												 }
												 }
												 
												 
									    		} catch (Exception e) {
									    			e.printStackTrace();
									    		}
								    			 
				                        
									}
								}
						}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
				 
					try{	if(!foundverb){
						
						ArrayList tempdoList = null;
						tempdoList = iSymantex.get(verb);
						if (tempdoList != null) {
							int j = 0;
							for (Object value : tempdoList) {
								j++;
								String[] index = new String[2];
								index = (String[]) value;

								String iSymantexword = index[0];
								String itype = index[1];
								iSymantexword = iSymantexword.trim();
								// надо - выйти из цикла
								 if(foundverb)break;
								try{
									 String varbcat = verbglobalcategory.get(iSymantexword);
									 if(varbcat!=null){
									 if(!varbcat.equals("")){	 
										 varbcategory=varbcat; foundverb=true;
									 }
									 }
									 
									 
						    		} catch (Exception e) {
						    			e.printStackTrace();
						    		}
		                       

								}
							}
					}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
				 
				 
				 
				 
				 
						if(sgeo==false&&shuman==false&&sorg==false){

						QResult list = testService.searchRootRuleForCompress(noun);
						
                        try{
                        	
                        	
                        	
                        	Map<String, List> returt=list.getReturt();
                        	
                        	for(Entry<String, List> ent:returt.entrySet()){
                        		String qvalue=ent.getKey();
                        		List qlist = ent.getValue();
                        		int qsize=qlist.size();
                        		
                        		//----------------контроль качества---------------------
                        		if(qsize>2){
                        		 String lastQvalue = (String) qlist.get(qsize - 1);
                        		 savetypeid=lastQvalue;
		                        		 Probabilityverbsqc countern= new Probabilityverbsqc();
		   								 countern.setCategoryid(verb); //глагол
		   								 countern.setTypeid(noun); //существительное
		   								 countern.setNounname(savetypeid);
		   							     countern.setVerbname(varbcategory);
		   								 countern.setTotalvalue(tot);
		   								 
		   								 
		   								 if(store.containsKey(savetypeid+" "+verb)){
		   									Probabilityverbsqc l = store.get(savetypeid+" "+verb);
		   									int ltot=l.getTotalvalue();
		   									int all=countern.getTotalvalue()+ltot;
		   									l.setTotalvalue(all);
		   									store.put(savetypeid+" "+verb, l);
		   									
		   								 }
		   								 else
		   								 store.put(savetypeid+" "+verb, countern);
                        		}
                        	}
                        }catch(Exception e){}
					
						}else{
								 Probabilityverbsqc countern= new Probabilityverbsqc();
								 countern.setCategoryid(verb); //глагол
								 countern.setTypeid(noun); //существительное
								 countern.setNounname(savetypeid);
								 
							     countern.setVerbname(varbcategory);
								
								 countern.setTotalvalue(tot);
								 
								 
								 if(store.containsKey(savetypeid+" "+verb)){
									Probabilityverbsqc l = store.get(savetypeid+" "+verb);
									int ltot=l.getTotalvalue();
									int all=countern.getTotalvalue()+ltot;
									l.setTotalvalue(all);
									store.put(savetypeid+" "+verb, l);
									
								 }
								 else
								 store.put(savetypeid+" "+verb, countern);
						}
				
				

			 }
		
		for (Map.Entry<String, Probabilityverbsqc> entry : store.entrySet())
		{
			verbServicenqq.addProbabilitycounterqc(entry.getValue()); 
		}
		
		}
	@Override
    public void compressVerbAVB() throws IOException {
		verbService.getLargeTriplet();
		Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getNounverbdbTriplet();
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops();
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 
		
		for (Map.Entry<String, Integer> entry : ilemmanounverbdb.entrySet())
		{
		   int tot = entry.getValue();  
		   String worsd= entry.getKey();
		   String[] myData = worsd.split(" ");
			
			String nouna=myData[0].toLowerCase().trim();			
			String verb=myData[1].toLowerCase().trim();
			String nounb=myData[2].toLowerCase().trim();	
			 ArrayList<String> nounwords = new ArrayList<String>();  
			 nounwords.add(nouna);
			 nounwords.add(nounb);	
			 int counts=0;
			 for(String noun:nounwords){
				 counts++;
		   
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			 //Сжать по human или organization или location - other
			 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid =noun;
				   String savecategoryid ="";
				   
					if(objectid!=null){
						String syn = idcategorymap.get(objectid);
						if(syn!=null){
							if(syn.contains("/")){
					  			
					  		String[] newwords = syn.split("\\/");
					  		
					  		itypeid=newwords[0];
					  		icategoryid=newwords[1];
							}
							
							if(itypeid.equals("1")){sgeo=true;   
							 savetypeid ="Q17334923";
							 savecategoryid =icategoryid;}
							else if(itypeid.equals("3")){shuman=true; 
							 savetypeid ="Q5";
							 savecategoryid =icategoryid;}
	           			 	else if(itypeid.equals("2")){sorg=true;
	           			 	savetypeid ="Q43229";
	           			 	savecategoryid =icategoryid;}
						}
						
					}
					if(sgeo==false&&shuman==false&&sorg==false){
				//----------------------------Q---------------------------- 
					List<String> qindexList = iiltextList.get(noun);
					boolean canexit=false;
					
					 if(qindexList!=null)
					 for(String qindex:qindexList){
						 if(canexit)break;
					 if(qindex!=null){ 
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
						   if(canexit)break;
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            	 if(canexit)break;
						            //System.out.println("--------search-----="+vi.get(0));
						            String qdata=vi.get(0);
						            savetypeid =qdata;
						            icount++;
						            //String words=getTextofQ(first, iiltext);
						            //String wordsprop=getPropertyOfType(qdata, iiltype);

				            
						            
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(qdata)){
						            	List<String> listofinstance =null;
						            	List<String> listsubclassof =null;
						            	List<String> listotherprops =null;
						            	String label="";
						            	
					            		listofinstance =iilinstanceList.get(qdata);
					            		listsubclassof =iilsubclassofList.get(qdata);
					            		listotherprops =iilotherpropsList.get(qdata);
					            		
					            		
						            	//if(iorclass==1){
						            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
						            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
						            		//}
					            			
					            	if(listotherprops!=null){
					            		
					            		  for(String ins:listotherprops){
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
					            			  
					            		  }
					            		}
						            if(listofinstance!=null){
							            for(String ins:listofinstance){
							            	//System.out.println("--------ofinstance-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						            if(listsubclassof!=null){
							            for(String ins:listsubclassof){
							            	//System.out.println("--------subclassof-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						             vi.remove(qdata);
						            }
						            
						            
						            } while (vi.size()>0);
						            
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					   //itogo=sb.toString();
					 }}
					 
					 
					 }
					
					 if(counts==1)
					 nouna=savetypeid;
					 if(counts==2)
					 nounb=savetypeid;
				//--------------------------Save---------------------------	
		}	
			 Probabilityverbtripqq countern= new Probabilityverbtripqq();
			     countern.setNounname(nouna);
				 countern.setVerbname(verb);
				 countern.setNounnamet(nounb);
				 countern.setTotalvalue(tot);
				 //if(sgeo!=false||shuman!=false||sorg!=false)
				 verbService.addProbabilitycounterTripletqq(countern); 

			 }
		}
	
	
	
	@Override
    public String compressVerbAVBSingle(String nouna,String verb, String nounb) throws IOException {
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops();
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 
		 Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
		 
			 ArrayList<String> nounwords = new ArrayList<String>();  
			 nounwords.add(nouna);
			 nounwords.add(nounb);	
			 String varbcat =verb;
					 
			 int counts=0;
			 for(String noun:nounwords){
				 counts++;
		   
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			 //Сжать по human или organization или location - other
			 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid =noun;
				   String savecategoryid ="";
				   
					if(objectid!=null){
						String syn = idcategorymap.get(objectid);
						if(syn!=null){
							if(syn.contains("/")){
					  			
					  		String[] newwords = syn.split("\\/");
					  		
					  		itypeid=newwords[0];
					  		icategoryid=newwords[1];
							}
							
							if(itypeid.equals("1")){sgeo=true;   
							 savetypeid ="Q17334923";
							 savecategoryid =icategoryid;}
							else if(itypeid.equals("3")){shuman=true; 
							 savetypeid ="Q5";
							 savecategoryid =icategoryid;}
	           			 	else if(itypeid.equals("2")){sorg=true;
	           			 	savetypeid ="Q43229";
	           			 	savecategoryid =icategoryid;}
						}
						
					}
					if(sgeo==false&&shuman==false&&sorg==false){
				//----------------------------Q---------------------------- 
					List<String> qindexList = iiltextList.get(noun);
					boolean canexit=false;
					
					 if(qindexList!=null)
					 for(String qindex:qindexList){
						 if(canexit)break;
					 if(qindex!=null){ 
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
						   if(canexit)break;
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            	 if(canexit)break;
						            //System.out.println("--------search-----="+vi.get(0));
						            String qdata=vi.get(0);
						            savetypeid =qdata;
						            icount++;
						            //String words=getTextofQ(first, iiltext);
						            //String wordsprop=getPropertyOfType(qdata, iiltype);

				            
						            
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(qdata)){
						            	List<String> listofinstance =null;
						            	List<String> listsubclassof =null;
						            	List<String> listotherprops =null;
						            	List<String> listhaspart = null;
						            	String label="";
						            	
					            		listofinstance =iilinstanceList.get(qdata);
					            		listsubclassof =iilsubclassofList.get(qdata);
					            		listotherprops =iilotherpropsList.get(qdata);
					            		listhaspart = iilhaspart.get(qdata);
					            		
						            	//if(iorclass==1){
						            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
						            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
						            		//}
					            		if (listhaspart != null) {

											for (String ins : listhaspart) {
												vi.add(ins);

												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}

											}
										}

					            	if(listotherprops!=null){
					            		
					            		  for(String ins:listotherprops){
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
					            			  
					            		  }
					            		}
						            if(listofinstance!=null){
							            for(String ins:listofinstance){
							            	//System.out.println("--------ofinstance-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						            if(listsubclassof!=null){
							            for(String ins:listsubclassof){
							            	//System.out.println("--------subclassof-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						             vi.remove(qdata);
						            }
						            
						            
						            } while (vi.size()>0);
						            
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					   //itogo=sb.toString();
					 }}
					 
					 
					 }
					
					 if(counts==1)
					 nouna=savetypeid;
					 if(counts==2)
					 nounb=savetypeid;
				//--------------------------Save---------------------------
					 try{
						  varbcat = verbglobalcategory.get(verb);
						 if(varbcat!=null){
							 if(varbcat.equals("")){  varbcat="none";} 
						 } else varbcat="none";
						 }catch(Exception e){}
		}	
			 

			 return nouna+"#"+varbcat+"#"+nounb;
		}
	
	
	
	
	
	
	@Override
    public void compressVerbAVBAndCategory() throws IOException {
		verbService.getLargeTriplet();
		Map<String, Integer>	ilemmanounverbdb=fastMemoryModel.getNounverbdbTriplet();
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops();
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 Map<String, List> iiltype=fastMemoryModel.getIilpropertyOfType();
		 
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 
		 
		 Map<String, String> verbglobalcategory=nameFinderModelMEDAO.getVerbGlobalCategoryMap();
		 
		 
		
		for (Map.Entry<String, Integer> entry : ilemmanounverbdb.entrySet())
		{
		   int tot = entry.getValue();  
		   String worsd= entry.getKey();
		   String[] myData = worsd.split(" ");
			
			String nouna=myData[0].toLowerCase().trim();			
			String verb=myData[1].toLowerCase().trim();
			String nounb=myData[2].toLowerCase().trim();	
			 ArrayList<String> nounwords = new ArrayList<String>();  
			 nounwords.add(nouna);
			 nounwords.add(nounb);	
			 int counts=0;
			 for(String noun:nounwords){
				 counts++;
		   
			boolean sgeo=false;
			boolean shuman=false;
			boolean sorg=false;
			 //Сжать по human или organization или location - other
			 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid =noun;
				   String savecategoryid ="";
				   
					if(objectid!=null){
						String syn = idcategorymap.get(objectid);
						if(syn!=null){
							if(syn.contains("/")){
					  			
					  		String[] newwords = syn.split("\\/");
					  		
					  		itypeid=newwords[0];
					  		icategoryid=newwords[1];
							}
							
							if(itypeid.equals("1")){sgeo=true;   
							 savetypeid ="Q17334923";
							 savecategoryid =icategoryid;}
							else if(itypeid.equals("3")){shuman=true; 
							 savetypeid ="Q5";
							 savecategoryid =icategoryid;}
	           			 	else if(itypeid.equals("2")){sorg=true;
	           			 	savetypeid ="Q43229";
	           			 	savecategoryid =icategoryid;}
						}
						
					}
					if(sgeo==false&&shuman==false&&sorg==false){
				//----------------------------Q---------------------------- 
					List<String> qindexList = iiltextList.get(noun);
					boolean canexit=false;
					
					 if(qindexList!=null)
					 for(String qindex:qindexList){
						 if(canexit)break;
					 if(qindex!=null){ 
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
						   if(canexit)break;
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            	 if(canexit)break;
						            //System.out.println("--------search-----="+vi.get(0));
						            String qdata=vi.get(0);
						            savetypeid =qdata;
						            icount++;
						            //String words=getTextofQ(first, iiltext);
						            //String wordsprop=getPropertyOfType(qdata, iiltype);

				            
						            
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(qdata)){
						            	List<String> listofinstance =null;
						            	List<String> listsubclassof =null;
						            	List<String> listotherprops =null;
						            	String label="";
						            	
					            		listofinstance =iilinstanceList.get(qdata);
					            		listsubclassof =iilsubclassofList.get(qdata);
					            		listotherprops =iilotherpropsList.get(qdata);
					            		
					            		
						            	//if(iorclass==1){
						            		//listofinstance = getListInstance(qdata, iilinstanceList);label="ofinstance";
						            		//listsubclassof = getListInstance(qdata, iilsubclassofList);label="subclassof";
						            		//}
					            			
					            	if(listotherprops!=null){
					            		
					            		  for(String ins:listotherprops){
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
					            			  
					            		  }
					            		}
						            if(listofinstance!=null){
							            for(String ins:listofinstance){
							            	//System.out.println("--------ofinstance-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						            if(listsubclassof!=null){
							            for(String ins:listsubclassof){
							            	//System.out.println("--------subclassof-----="+ins);
							            	  vi.add(ins);
							            	  //String lwordsprop=getPropertyOfType(ins, iiltype);
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
							            }
						            }
						             vi.remove(qdata);
						            }
						            
						            
						            } while (vi.size()>0);
						            
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					   //itogo=sb.toString();
					 }}
					 
					 
					 }
					
					 if(counts==1)
					 nouna=savetypeid;
					 if(counts==2)
					 nounb=savetypeid;
				//--------------------------Save---------------------------	
		}	
			 Probabilityverbtripqcq countern= new Probabilityverbtripqcq();
			     countern.setNounname(nouna);
				 countern.setVerbname(verb);
				 countern.setNounnamet(nounb);
				 
				 try{
				 String varbcat = verbglobalcategory.get(verb);
				 if(varbcat!=null){
					 if(!varbcat.equals("")){  countern.setVerbname(varbcat); } else countern.setVerbname("none");
				 } else countern.setVerbname("none");
				 }catch(Exception e){}
				 
				 countern.setTotalvalue(tot);
				 //if(sgeo!=false||shuman!=false||sorg!=false)
				 verbService.addProbabilitycounterTripletqcq(countern); 

			 }
		}

	
	@Override
    public  Map<String, Integer> getAtributesVerb(String verbname) throws IOException {
		
		List<Probabilityverbn> verblist = verbServicen.getNounnameFromVerb(verbname);
		
		 Map<String, Integer> ilemmanounverbdb=fastMemoryModel.getNounverbdbTriplet();
		
		 Map<String, String> liniarmap=nameFinderModelMEDAO.getLiniarmap();
		 Map<String, String> idcategorymap=nameFinderModelMEDAO.getIdcategorymapobj();
		 
		 Map<String, List> iiltextList=fastMemoryModel.getIilobjects();
		 Map<String, List> iilinstanceList=fastMemoryModel.getOfinstance();
		 
		 
		 
		 
		 Map<String, List> iilsubclassofList=fastMemoryModel.getIilsubclassof();
		 Map<String, List> iiltext=fastMemoryModel.getIilobjectsToText();
		 
		 
		 Map<String, List> iilpropertiesList=fastMemoryModel.getIilproperties();
		 Map<String, List> iilpropertyOfTypeList=fastMemoryModel.getIilpropertyOfType();
		 Map<String, List> iilotherpropsList=fastMemoryModel.getIilotherprops(); 
		
		
		
		
		 Map<String, String> qgeo=fastMemoryModel.getCheckIfGeo();
		 Map<String, String> qhuman=fastMemoryModel.getCheckIfHuman();
		 Map<String, String> qorg=fastMemoryModel.getCheckIfOrg();
		 Map<String, List> iilhaspart= fastMemoryModel.getIilhaspart();
		
		 
		 Map<String, Integer> totalMap=new HashMap<String, Integer> ();
		 
		 
		 
		for (Probabilityverbn entry:verblist)
		{try{
		   int tot = entry.getTotalvalue();  
		   String worsd= entry.getNounname();

			String nouna=worsd.toLowerCase().trim();			
			String verb=verbname.toLowerCase().trim();
			 
			 System.out.println("--------verb-----="+verb);
			
			
			 ArrayList<String> nounwords = new ArrayList<String>();  
			 nounwords.add(nouna);
			
			 int counts=0;
			 for(String noun:nounwords){
				 counts++;
		   
				 List<String> resultThree=new ArrayList<String> ();
				 
				    boolean sgeo=false;
					boolean shuman=false;
					boolean sorg=false;
					 //Сжать по human или organization или location - other
					 //поиск в объектах O и поиск в Q
				   String objectid=liniarmap.get(noun);
				   String itypeid ="";
				   String icategoryid ="";
				   String savetypeid =noun;
				   String savecategoryid ="";
				   
				//----------------------------Q---------------------------- 
					List<String> qindexList = iiltextList.get(noun);
					boolean canexit=false;
					
					 if(qindexList!=null)
					 for(String qindex:qindexList){
						 if(canexit)break;
					 if(qindex!=null){ 
					 Map<String, String> viewed=new HashMap<String, String> ();
					 
					 StringBuffer sb=new StringBuffer();
					 
					 boolean doit=true;
					  int count=0; 
					   do {
						   if(canexit)break;
				       		count++;
				            if(count > 100) break;    
				            String preview=qindex;
				            if(!viewed.containsKey(qindex)){
				            
				            	
				            	
				            List<String> vi=new ArrayList<String> ();
				            vi.add(qindex);

				            		int icount=0; 
						            do {  
						            	 if(canexit)break;
						            
						            	 //System.out.println("--------resultThree.add="+vi.get(0));
						            	 String qdata=vi.get(0);
						            	 savetypeid =qdata;
						            	 resultThree.add(qdata);
						            	 icount++;
						            	 
						                  
						            
						            if(icount > 1000) break;  
						            if(!viewed.containsKey(qdata)){
						            	List<String> listofinstance =null;
						            	List<String> listsubclassof =null;
						            	List<String> listotherprops =null;
						            	String label="";
						            	List<String> listhaspart =null;
						            	
						            	
					            		listofinstance =iilinstanceList.get(qdata);
					            		listsubclassof =iilsubclassofList.get(qdata);
					            		listotherprops =iilotherpropsList.get(qdata);
					            		listhaspart    =iilhaspart.get(qdata);
					            
					            		if(listhaspart!=null){
						            		
						            		  for(String ins:listhaspart){
						            			  
						            	     vi.add(ins);
						            			  
						            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
						            			  
						            		  }
						            		}	
					            			
					            	if(listotherprops!=null){
					            		
					            		  for(String ins:listotherprops){
					            			 
					            			  vi.add(ins);
					            			  
					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
					            			  
					            		  }
					            		}
						            if(listofinstance!=null){
							            for(String ins:listofinstance){
							            	
							            	  vi.add(ins);
							            	 
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
							            }
						            }
						            if(listsubclassof!=null){
							            for(String ins:listsubclassof){
							            
							            	  vi.add(ins);
							            	  
							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923";  break;}
						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5";  break;}
						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229";  break;}
							            }
						            }
					            		
//					            		if(listhaspart!=null){
//						            		
//						            		  for(String ins:listhaspart){
//						            			  
//						            	     vi.add(ins);
//						            			  
//						            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//							            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//							            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//						            			  
//						            		  }
//						            		}	
//					            			
//					            	if(listotherprops!=null){
//					            		
//					            		  for(String ins:listotherprops){
//					            			 
//					            			  vi.add(ins);
//					            			  
//					            			  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//					            			  
//					            		  }
//					            		}
//						            if(listofinstance!=null){
//							            for(String ins:listofinstance){
//							            	
//							            	  vi.add(ins);
//							            	 
//							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//							            }
//						            }
//						            if(listsubclassof!=null){
//							            for(String ins:listsubclassof){
//							            
//							            	  vi.add(ins);
//							            	  
//							            	  if(qgeo.containsKey(ins)){sgeo=true;  savetypeid ="Q17334923"; canexit=true; break;}
//						            			 else if(qhuman.containsKey(ins)){shuman=true; savetypeid ="Q5"; canexit=true; break;}
//						            			 else if(qorg.containsKey(ins)){sorg=true;savetypeid ="Q43229"; canexit=true; break;}
//							            }
//						            }
						             vi.remove(qdata);
						            }
						            
						            
						            } while (vi.size()>0);
						            
				            

				            if(preview==qindex)doit=false;
				            
				            if(qindex==null)doit=false;
				            
				            viewed.put(qindex, qindex);
				            }
				            
				        } while (doit);
					   
					  
					 }}
					 
					 
					
					
					 if(counts==1)
					 nouna=savetypeid;
				
				//--------------------------Save---------------------------	

					 					            	
					 					            	
					for(String qdata:resultThree){
					try{	
						 //System.out.println("--------iilpropertiesList="+qdata);
						 List<String> iilproperties =null;
						 iilproperties =iilpropertiesList.get(qdata);
						 					            	if(iilproperties!=null){
											            		  for(String ins:iilproperties){
											            			  
											            			  if(ins!=null)
											            				  if(totalMap.containsKey(ins)){
											            					  int d=totalMap.get(ins); totalMap.put(ins, d+1);
											            				  }else
											            			      totalMap.put(ins, 1);
											            		  }
											            		}
						 					            	
						
						 List<String> iilproperty =null;
						 iilproperty =iilpropertyOfTypeList.get(qdata);
						 					            	if(iilproperty!=null){
											            		  for(String ins:iilproperty){
											            			  
											            			  if(ins!=null)
											            				  if(totalMap.containsKey(ins)){
											            					  int d=totalMap.get(ins); totalMap.put(ins, d+1);
											            				  }else
											            			      totalMap.put(ins, 1);
											            		  }
											            		}
						 					            	
					}catch(Exception e){}				            	
						 					            	
					}
		}	
			   
				

		}catch(Exception e){}	 }
	return totalMap;
		}
	
	
	
	public boolean checkIfHuman(String str){
		boolean human=false;
		//instance of P31=Q5/Q202444
		
		return human;
	}
	
	public boolean checkIfOrg(String str){
		boolean org=false;
		//instance of P31=Q43229
		//subclass of P279=Q43229
		//otherprops topic's main category=Q5613113/Q43229
		return org;
	}
	
	public boolean checkIfGeo(String str){
		boolean geo=false;
		//subclass of =Q515 Q2264924 Q208511 Q6256 Q1520223 Q7270 Q13226383 Q2221906 Q17334923
		return geo;
	}
	
	
	public String getPropertyOfType(String idx, Map<String, List> iiltext){
	  	  String ofinstance =null;
	  	  try{ 
		    		 List<String> list= iiltext.get(idx);
		    		 StringBuffer sb=new StringBuffer();
		    		 String begin="P[("+idx+")";
		    		 for(String c:list){
		    			 sb.append(c).append("-"); 
		    		 }
		    		 String end="]";
		    		 ofinstance = begin+sb.toString()+end; 
			  }catch(Exception e){}
	  	if(ofinstance ==null)ofinstance="";
	  	return ofinstance;
			
		}
	
	@Override
    public void reLoadVerb() throws IOException {
		
		Map<String, Integer>   inounverbdb=fastMemoryModel.getNounverbdb();
		
		//Map<String, Integer>  inounverbdb=new HashMap<String, Integer>();
		// Map<String, Integer> ilemmanounverbdb= new HashMap<String, Integer>();
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
		for (Map.Entry<String, Integer> entry : inounverbdb.entrySet())
		{
		    
		   String worsd= entry.getKey();
		   String[] myData = worsd.split(" ");
			
			String noun=myData[0].toLowerCase().trim();
			
			
			String verb=myData[1].toLowerCase().trim();
			
				
				 int tot = entry.getValue();
			
				 
				 String  lemmanoun=getLemaWord(noun,luceneMorph);
				 String  lemmaverb=getLemaWord(verb,luceneMorph);
				 
				 Probabilityverbn countern= new Probabilityverbn();
				
				 countern.setNounname(lemmanoun);
				 countern.setVerbname(lemmaverb);
				 countern.setTotalvalue(tot);
				 verbServicen.addProbabilitycounter(countern); 

			 }
			 
		
		
		//verbService.getLargeUpdate();
//		Map<String, Integer>  inounverbdb=new HashMap<String, Integer>();
//		 Map<String, Integer> ilemmanounverbdb= new HashMap<String, Integer>();
//		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
//		Long mcount=verbService.countText();
//		System.out.println("loadverbTask=Counter="+mcount); 
//		int totalpage=mcount.intValue(); 
//		int pageSize=50;
//		int pages=totalpage/pageSize;
//		for(int ip=1; ip<pages; ip++){
//			List<Probabilityverb>  idata =verbService.getAllByPage(ip, pageSize, null);	
//			 int h=0;
//			 for(Probabilityverb dirty:idata){
//				 String noun=dirty.getNounname().toLowerCase();
//				 String verb=dirty.getVerbname().toLowerCase();
//				 int tot = dirty.getTotalvalue();
//			
//				 
//				 String  lemmanoun=getLemaWord(noun,luceneMorph);
//				 String  lemmaverb=getLemaWord(verb,luceneMorph);
//				 
//				 Probabilityverbn countern= new Probabilityverbn();
//				
//				 countern.setNounname(lemmanoun);
//				 countern.setVerbname(lemmaverb);
//				 countern.setTotalvalue(tot);
//				 verbServicen.addProbabilitycounter(countern); 
//
//			 }
//			 
//		}
		
	}
	@Override
	 public void restUpdateObject() throws IOException {
			
			try {
				boolean sts=objcs.update();
				 
				
			} catch (ClassNotFoundException | SQLException e) {
				
				e.printStackTrace();
			}
			
		}
	@Override
    public void restLoadEmotion() {
	       try {
            	try {
					nameFinderModelMEDAO.loadEmotion("positive");
					nameFinderModelMEDAO.loadEmotion("negative");
				
            	} catch (InvalidFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	@Scheduled(fixedRate=36000000)
    public void loadverbTask() throws IOException {
		if(firstrun){ 
			System.out.println("BEGIN----=loadverbTask---");
			verbService.getLarge();
			verbServicen.getLarge();
			verbService.getLargeTriplet();
		}
		firstrun=true;
		
	}
	
	public String getLemaWord(String word, LuceneMorphology luceneMorph){
		try{
		 List<String> wordBaseForms = luceneMorph.getMorphInfo(word);
		 String wordform="";
			int sizer=0;
			try{sizer=wordBaseForms.size();}catch(Exception e){}
			
			if(sizer>0){
			
			StringBuilder sb = new StringBuilder();
			for (String s : wordBaseForms)
			{
			    sb.append(s);
			    sb.append(" #");
			}
			
			
			String ist = sb.toString();
			if(!ist.equals("")&&ist!=null){
				wordform=ist;
			}
			
		
		
			if(wordform.contains("|")){
				   String[] myData = wordform.split("\\|");
				   String nword =myData[0].trim(); word=nword;
			}
			}}catch(Exception e){}
		return word;
		}
	
	//Сохранение триплета 
	@Scheduled(fixedRate=680000)
    public void doSleepVerbTaskTriplet() throws IOException {
		//LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
		 Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCountTriplet();
		 System.out.println("BEGIN----=doSleepVerbTaskTriplet---"+probabilitymain.size());
		 Map<String, Integer> temmain= new HashMap<String, Integer>(probabilitymain);
		 
	
		 
		 
		 fastMemoryModel.cleareObjectAndVerbCountTriplet();
		 for (Map.Entry<String, Integer> entry : temmain.entrySet())
		 {
		 String key=entry.getKey();
		 int value=entry.getValue();
		 if(key.contains(" ")){
			 
			 
				String[] myData = key.split(" ");
				Probabilityverbtrip counter= new Probabilityverbtrip();
				
				
				String nounname=cleareVal(myData[0]).trim();
				counter.setNounname(nounname);
				
				String verbname=cleareVal(myData[1]).trim();
				counter.setVerbname(verbname);
				
				String nounnameB=cleareVal(myData[2]).trim();
				counter.setNounnamet(nounnameB);
				
				
				//counter.setTotalvalue(1);
				counter.setTotalvalue(value);
				
				verbService.addProbabilitycounterTriplet(counter);
				

		 }
	}
	}
	
	
	//Обработка пояснений по тексту Explain
	@Scheduled(fixedRate=240000)
    public void doSleepExplainTask() {
		
		 Map<String, String> explainMap= fastMemoryModel.getExplainPrimeri();
		 
		 System.out.println("BEGIN----=doSleepExplainTask---"+explainMap.size());
		 
		 Map<String, String> explaintemmain= new HashMap<String, String>(explainMap);
		 fastMemoryModel.getExplainPrimeriCleare();
		 int f=0;
		 for (Map.Entry<String, String> entry : explaintemmain.entrySet())
		 {
			 f++;
			 if(f>20000)break;
			 try{
			 String key=entry.getKey();
			 String value=entry.getValue();
			 String exptype = "";
			 String exptext = "";
				 if (value != null) {
					 String[] myData = value.split("#");
					  exptype =  myData[0].trim(); 
					  exptext =  myData[1].trim(); 
				 }
		 
		
		
		       int maxLength=15000;
			   if (exptext.length() > maxLength) {
				 exptext = exptext.substring(0, maxLength);
				} 
			   
		
		if(exptype.equals("1")){
			if (key != null) {
				if (key.contains("#")) {
					String[] myData = key.split("#");
			
			 Probabilityverbexpl counter=new Probabilityverbexpl();
			
			   
			 counter.setExptext(exptext);
			
			 counter.setExptype(exptype);
			 String nounname =  myData[0];
			   
			   int maxLengthn=35;
			   if (nounname.length() > maxLengthn) {
				   nounname = nounname.substring(0, maxLengthn);
				}
			 counter.setNounname(nounname);
			 String verbname = myData[1];
			 
			 if (verbname.length() > maxLengthn) {
				 verbname = verbname.substring(0, maxLengthn);
				}
			 
			 
			 counter.setVerbname(verbname);
			 String nounnamet = myData[2];
			 if (nounnamet.length() > maxLengthn) {
				 nounnamet = nounnamet.substring(0, maxLengthn);
				}
			 
			 counter.setNounnamet(nounnamet);
			 
			 
			 verbService.addProbabilitycounterExpl(counter);}}
		 }
		 if(exptype.equals("4")){
				if (key != null) {
					if (key.contains("#")) {
						String[] myData = key.split("#");
				
				 Probabilityverbexpl counter=new Probabilityverbexpl();
				
				 counter.setExptext(exptext);
				
				 counter.setExptype(exptype);
				 String nounname =  myData[0];
				  int maxLengthn=35;
				   if (nounname.length() > maxLengthn) {
					   nounname = nounname.substring(0, maxLengthn);
					}
				 counter.setNounname(nounname);
				 String verbname = myData[1];
				 if (verbname.length() > maxLengthn) {
					 verbname = verbname.substring(0, maxLengthn);
					}
				 
				 counter.setVerbname(verbname);
				 String nounnamet = myData[2];
				 if (nounnamet.length() > maxLengthn) {
					 nounnamet = nounnamet.substring(0, maxLengthn);
					}
				 counter.setNounnamet(nounnamet);
				 
				 
				 verbService.addProbabilitycounterExpl(counter);}
				}
		 }
		 if(exptype.equals("5")){
				if (key != null) {
					if (key.contains("#")) {
						String[] myData = key.split("#");
				
				 Probabilityverbexpl counter=new Probabilityverbexpl();
				
				 counter.setExptext(exptext);
				
				 counter.setExptype(exptype);
				 String nounname =  myData[0];
				 int maxLengthn=35;
				   if (nounname.length() > maxLengthn) {
					   nounname = nounname.substring(0, maxLengthn);
					}
				 counter.setNounname(nounname);
				 String verbname = myData[1];
				 if (verbname.length() > maxLengthn) {
					 verbname = verbname.substring(0, maxLengthn);
					}
				 counter.setVerbname(verbname);
				 String nounnamet = myData[2];
				 if (nounnamet.length() > maxLengthn) {
					 nounnamet = nounnamet.substring(0, maxLengthn);
					}
				 counter.setNounnamet(nounnamet);
				 
				 
				 verbService.addProbabilitycounterExpl(counter);}
				}
		 }
		 if(exptype.equals("2")){
			
		 Horosho counter= new Horosho();
		 int maxLengthn=30;
		   if (key.length() > maxLengthn) {
			   key = key.substring(0, maxLengthn);
			}
		 counter.setWord(key);
		 counter.setTotal(1);	
		 verbService.addProbabilityHorosho(counter);
		 }
		 if(exptype.equals("3")){
			
			 Ploho counter= new Ploho();
			 int maxLengthn=30;
			   if (key.length() > maxLengthn) {
				   key = key.substring(0, maxLengthn);
				}
			 counter.setWord(key);
			 counter.setTotal(1);
			 verbService.addProbabilityPloho(counter);
		 }
		 
		 }catch(Exception e){e.printStackTrace();}
		 }

	}
	
	//Обработка триплетов содержащих предлог направления
	@Scheduled(fixedRate=570000)
    public void doSleepVerbTaskTripletPredlog() throws IOException {
		//LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
		 Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCountTripletPredlog();
		 System.out.println("BEGIN----=doSleepVerbTaskTripletPredlog---"+probabilitymain.size());
		 Map<String, Integer> temmain= new HashMap<String, Integer>(probabilitymain);
		 
	
		 
		 
		 fastMemoryModel.cleareObjectAndVerbCountTripletPredlog();
		 for (Map.Entry<String, Integer> entry : temmain.entrySet())
		 {
		 String key=entry.getKey();
		 int value=entry.getValue();
		 if(key.contains(" ")){
			 
			 
				String[] myData = key.split(" ");
				Probabilityverbtrippp counter= new Probabilityverbtrippp();
				
				
				String nounname=cleareVal(myData[0]).trim();
				counter.setNounname(nounname);
				
				String verbname=cleareVal(myData[1]).trim();
				counter.setVerbname(verbname);
				
				String nounnameB=cleareVal(myData[2]).trim();
				counter.setNounnamet(nounnameB);
				
				
				//counter.setTotalvalue(1);
				counter.setTotalvalue(value);
				
				verbService.addProbabilitycounterTripletPredlog(counter);
				

		 }
	}
	}
	//Обработка глаголов в т.ч. в нормальной форме
	@Scheduled(fixedRate=620000)
    public void doSleepVerbTask() throws IOException {
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
		 Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCount();
		 System.out.println("BEGIN----=doSleepVerbTask---"+probabilitymain.size());
		 Map<String, Integer> temmain= new HashMap<String, Integer>(probabilitymain);
		 
		 Map<String, Map<String, Integer>> verbList=fastMemoryModel.getObjectAndVerbList();
		 
		 
		 fastMemoryModel.cleareObjectAndVerbCount();
		 for (Map.Entry<String, Integer> entry : temmain.entrySet())
		 {
		 String key=entry.getKey();
		 if(key.contains(" ")){
				String[] myData = key.split(" ");
				Probabilityverb counter= new Probabilityverb();
				
				
				String nounname=cleareVal(myData[0]).trim();
				counter.setNounname(nounname);
				
				String verbname=cleareVal(myData[1]).trim();
				counter.setVerbname(verbname);
				
				counter.setTotalvalue(1);
				
				verbService.addProbabilitycounter(counter);
				
				Probabilityverbn countern= new Probabilityverbn();
				 String  lemmanoun=getLemaWord(nounname,luceneMorph);
				 String  lemmaverb=getLemaWord(verbname,luceneMorph);
				 countern.setNounname(lemmanoun);
				 countern.setVerbname(lemmaverb);
				 countern.setTotalvalue(1);
				 verbServicen.addProbabilitycounter(countern); 
				
				
				
				
				
				
				if(verbList.containsKey(myData[0])){
					Map<String, Integer> singverb=verbList.get(myData[0]);
					
					if(singverb.containsKey(myData[1])){
						int val=singverb.get(myData[1]);
						singverb.put(myData[1], val+1);
					}
					else{
						singverb.put(myData[1], 1);	
					}
					verbList.put(myData[0], singverb);
					
				}
				else{
					 Map<String, Integer> singverb= new HashMap<String, Integer>();
					 singverb.put(myData[1], 1);
					
					 verbList.put(myData[0], singverb);
				}
		 }
	}
	}
	//Обновление Probabilitybwa отношений P между объектами
	@Scheduled(fixedRate=440000)
    public void doSleepQPropertyTask() throws IOException {
		
		 Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndVerbCountProperty();
		 System.out.println("BEGIN----=doSleepQPropertyTask---"+probabilitymain.size());
		 Map<String, Integer> temmain= new HashMap<String, Integer>(probabilitymain);
		 									  fastMemoryModel.cleareObjectAndVerbCountProperty();
		 
		 for (Map.Entry<String, Integer> entry : temmain.entrySet())
		 {
		 try{
			    String key=entry.getKey();
		
				String[] myData = key.split("\\|");
				Probabilitybwa counter= new Probabilitybwa();
				
				
				String beforeword=cleareVal(myData[0]).trim();
				counter.setBeforeword(beforeword);
				
				String propword=cleareVal(myData[1]).trim();
				counter.setPropword(propword);
				
				String afterword=cleareVal(myData[2]).trim();
				counter.setAfterword(afterword);
				
				counter.setTotalvalue(entry.getValue());
				if(!beforeword.equals(afterword))
				verbService.addProbabilitycounterProperty(counter);
		 }catch(Exception e){}
	
		 
	}
	}
	
	
	@Scheduled(fixedRate=810000)
    public void doSleepAdjectiveTask() throws IOException {
		
		LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
		 Map<String, Integer> probabilitymain=fastMemoryModel.getObjectAndAdjectiveCount();
		 System.out.println("BEGIN----=doSleepAdjectiveTask---"+probabilitymain.size());
		 Map<String, Integer> temmain= new HashMap<String, Integer>(probabilitymain);
	     
		 fastMemoryModel.cleareObjectAndAdjectiveCount();
		 for (Map.Entry<String, Integer> entry : temmain.entrySet())
		 {
		 String key=entry.getKey();
		 if(key.contains(" ")){
				String[] myData = key.split(" ");
			
				
				String nounname=cleareVal(myData[1]).trim();
				String adjective=cleareVal(myData[0]).trim();
				
				Probabilityadjective countern= new Probabilityadjective();
				 String  lemmanoun=getLemaWord(nounname,luceneMorph);
				 String  lemmaadjective=getLemaWord(adjective,luceneMorph);
				 countern.setNounname(lemmanoun);
				 countern.setAdjective(lemmaadjective);
				 countern.setTotalvalue(1);
				 verbServicen.addProbabilitycounteradj(countern); 

		 }
	}
	}
	
	/*
	@Scheduled(fixedRate=100000)
    public void doSleepRealationTask() throws IOException {
		 Map<String, Integer> probabilityr=fastMemoryModel.getObjectRelationCount();
		 System.out.println("BEGIN----=doSleepRealationTask---"+probabilityr.size());
		 Map<String, Integer> tem= new HashMap<String, Integer>(probabilityr);
		 
		 Map<String, Map<String, Integer>> rList=fastMemoryModel.getObjectRelationList();
		 
		 
		 fastMemoryModel.cleareObjectRelation();
		 for (Map.Entry<String, Integer> entry : tem.entrySet())
		 {
		 String key=entry.getKey();
		 if(key.contains(" ")){
				String[] myData = key.split(" ");
				
				if(rList.containsKey(myData[0])){
					Map<String, Integer> singverb=rList.get(myData[0]);
					
					if(singverb.containsKey(myData[1])){
						int val=singverb.get(myData[1]);
						singverb.put(myData[1], val+1);
					}
					else{
						singverb.put(myData[1], 1);	
					}
					rList.put(myData[0], singverb);
					
				}
				else{
					 Map<String, Integer> singverb= new HashMap<String, Integer>();
					 singverb.put(myData[1], 1);
					
					 rList.put(myData[0], singverb);
				}
		 }
	}
	}
	*/
	
	boolean isPresent(String serchword, String text){
		
		 String regex = "\\b"+serchword+"\\b";
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(text);
	    
	        while(matcher.find() == true){
	        	
	        	return true;
	        }
			return false;
	}
	
	private String cleareVal(String val){
		try{
			val=val.toLowerCase();
			val=val.trim();
			
		}catch(Exception w){}
		
		return val;
	}



	@Override
	public boolean[] skazuemoeRule(String worda,String morfa, String wordb,String morfb) {
		boolean[] z = langRules.skazuemoeRule( worda, morfa,  wordb, morfb);
		return z;
	} 
	@Override
	public boolean[] podlejasheeRule(String morf) {
		boolean[] z = langRules.podlejasheeRuleSet(morf);
		return z;
	}
	
	@Override
	public boolean deeprichastieRule(String morf) {
		boolean z = langRules.deeprichastieRule(morf);
		return z;
	}
	
	@Override
	public boolean sushRule(String morf) {
		boolean z = langRules.sushRuleSet(morf);
		return z;
	}



	@Override
	public boolean prolognarRule(String morf) {
		boolean z = langRules.prolognarRuleSet(morf);
		return z;
	}



	@Override
	public boolean hasVoprosSymbol(String singltext) {
	
		return langRules.hasVoprosSymbol(singltext);
	}
	
	@Override
	public JSONObject correctorData(String text) {
		JSONObject outjson = new JSONObject();
		try{
			
	List<String[]> alltokenstrim = new  ArrayList<String[]>();
	//MultiThreadedJLanguageTool langTool = new MultiThreadedJLanguageTool(new Russian());
	//JLanguageTool langTool = new JLanguageTool(new Russian());
	JLanguageTool langTool = nameFinderModelMEDAO.getLanguageTool();
	langTool.disableRule("UPPERCASE_SENTENCE_START");
	//langTool.disableRule("WHITESPACE_RULE");
	
	String delims = " ";
	StringTokenizer st = new StringTokenizer(text, delims);
	StringBuffer firstfb=new StringBuffer();
	JSONArray jArray = new JSONArray();
	
	while (st.hasMoreElements()) {
		 JSONObject jsonlemma = new JSONObject();
		
		 String word=(String) st.nextElement();
		 try{word=word.replaceAll("\\pP","");}catch(Exception e){e.printStackTrace();}
		 jsonlemma.put("nword", word);
		 List<RuleMatch> matches = langTool.check(word);
		 StringBuffer errresult=new StringBuffer();
		 
		 JSONArray ljArray = new JSONArray();
		 for (RuleMatch match : matches) {
			 JSONObject ojsonlemma = new JSONObject();
			 
			  //System.out.println(" " +match.getFromPos() + "-" + match.getToPos() + ": " + match.getMessage());
			  //System.out.println(": " +match.getSuggestedReplacements());
			 ojsonlemma.put("start", match.getFromPos());
			 ojsonlemma.put("end", match.getToPos());
			 ojsonlemma.put("type", match.getMessage());
			 
			List lists=match.getSuggestedReplacements();
			
			for(Object l:lists){
				String s=(String)l;
				errresult.append(s).append("|");
			}
			ojsonlemma.put("suggested", errresult.toString());
			ljArray.add(ojsonlemma);
		 }
		 
		 jsonlemma.put("rules", ljArray);
		
		//String[] index = new String[2];
		//index[0]=word;
		//index[1]=errresult.toString();	
		//alltokenstrim.add(index);
		
		jArray.add(jsonlemma);
	}
	outjson.put("wordlist", jArray);
	
	String dirtyBB=text;
	
	List<RuleMatch> matches = langTool.check(dirtyBB);
	 JSONArray ruleArray = new JSONArray();
	for (RuleMatch match : matches) {
		 JSONObject ruleobj = new JSONObject();
		 
		 ruleobj.put("start", match.getFromPos());
		 ruleobj.put("end", match.getToPos());
		 ruleobj.put("type", match.getMessage());
		 
		    List lists=match.getSuggestedReplacements();
		    StringBuffer errresult=new StringBuffer();
			for(Object l:lists){
				String s=(String)l;
				errresult.append(s).append("|");
			}
			ruleobj.put("suggested", errresult.toString());
			ruleArray.add(ruleobj);
		 
		 //System.out.println(" " + match.getFromPos() + "-" + match.getToPos() + ": " + match.getMessage());
	     //System.out.println("(===): " + match.getSuggestedReplacements());
	}
	
	outjson.put("sentence", ruleArray);
	
	
	}catch(Exception e){}
		return outjson;
	}
	
	
	@Override
	public Message getMorfologyWord(String word) {
		
	String wordform="none"; 
	String normalform="none"; 
	LuceneMorphology luceneMorph = nameFinderModelMEDAO.getLuceneMorph();
	
	try{
	
	
	List<String> wordBaseForms = luceneMorph.getMorphInfo(word);
	StringBuilder sb = new StringBuilder();
	for (String s : wordBaseForms)
	{
	    sb.append(s);
	    sb.append(",");
	}
	
	
	String ist = sb.toString();
	if(!ist.equals("")&&ist!=null){
		wordform=ist;
	}
	}catch(Exception e){}
	
	
	
	try{
		
		
		List<String> wordBaseForms = luceneMorph.getNormalForms(word);
		StringBuilder sb = new StringBuilder();
		for (String s : wordBaseForms)
		{
		    sb.append(s);
		    sb.append(" ");
		}
		
		
		String ist = sb.toString();
		if(!ist.equals("")&&ist!=null){
			normalform=ist;
		}
		}catch(Exception e){}
	
	
	Message msg = new Message(word, wordform, normalform);
	return msg;
	
	// RussianAnalyzer russian = new RussianAnalyzer();
	// russian.
	}
	
	
}


