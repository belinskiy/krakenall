package com.bgv.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;

public class ProcessorResult {
	Map<Integer, String[]>  data;
	JSONArray wordlist;
	Map<String, Integer> categoryMap;
	String singltext;
	Map<String, Map<String, List>> listobjects;
	Map<String, Integer> adjectiveCount;
	Map<String, Integer[]> wordindex;
	ArrayList<Integer> podlejashie;
	ArrayList<Integer> skazuemie;
	ArrayList<Integer> deeprichastie;
	public Map<Integer, String[]> getData() {
		return data;
	}
	public void setData(Map<Integer, String[]> data) {
		this.data = data;
	}
	public JSONArray getWordlist() {
		return wordlist;
	}
	public void setWordlist(JSONArray wordlist) {
		this.wordlist = wordlist;
	}
	public String getSingltext() {
		return singltext;
	}
	public void setSingltext(String singltext) {
		this.singltext = singltext;
	}
	public Map<String, Integer> getCategoryMap() {
		return categoryMap;
	}
	public void setCategoryMap(Map<String, Integer> categoryMap) {
		this.categoryMap = categoryMap;
	}
	public Map<String, Map<String, List>> getListobjects() {
		return listobjects;
	}
	public void setListobjects(Map<String, Map<String, List>> listobjects) {
		this.listobjects = listobjects;
	}
	public void setAdjective(Map<String, Integer> adjectiveCount) {
		this.adjectiveCount=adjectiveCount;
		
	}
	public Map<String, Integer> getAdjective() {
		return adjectiveCount;
	}
	public Map<String, Integer[]> getWordindex() {
		return wordindex;
	}
	public void setWordindex(Map<String, Integer[]> wordindex) {
		this.wordindex = wordindex;
	}
	public ArrayList<Integer> getPodlejashie() {
		return podlejashie;
	}
	public void setPodlejashie(ArrayList<Integer> podlejashie) {
		this.podlejashie = podlejashie;
	}
	public ArrayList<Integer> getDeeprichastie() {
		return deeprichastie;
	}
	public void setDeeprichastie(ArrayList<Integer> deeprichastie) {
		this.deeprichastie = deeprichastie;
	}
	public ArrayList<Integer> getSkazuemie() {
		return skazuemie;
	}
	public void setSkazuemie(ArrayList<Integer> skazuemie) {
		this.skazuemie = skazuemie;
	}

	
	
}
