package com.bgv.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bgv.memory.FastModelDAO;
import com.bgv.nlp.obj.IndexWrapper;
import com.bgv.nlp.obj.NameFinderModelMEDAO;
import com.bgv.nlp.obj.ObjDAO;
import com.bgv.service.IParserSevice;
import com.bgv.springmvc.domain.FindObject;

@Service
public class ProcessorImpl implements ProcessorDAO {
	// @Autowired
	// IParserSevice maltrest;

	@Autowired
	private ObjDAO objs;

	@Autowired
	private FastModelDAO fastMemoryModel;

	@Autowired
	private NameFinderModelMEDAO nameFinderModelMEDAO;

	@Autowired
	private AnalysisDAO analysis;

	@Override
	// public Map<Integer, String[]> getTextRezult(String text, boolean
	// ineededu) {

	public ProcessorResult getTextRezult(String text, boolean ineededu, boolean getmalt) {
		Map<Integer, String[]> out = new HashMap<Integer, String[]>();
		ProcessorResult returnresult = new ProcessorResult();
		returnresult.setSingltext(text);
		Map<String, Integer> allcategoryMap = new HashMap<String, Integer>();
		Map<String, Integer> allemmotionMap = new HashMap<String, Integer>();
		allemmotionMap.put("positive", 0);
		allemmotionMap.put("negative", 0);

		Map<String, Integer> wordPhraseCount = new HashMap<String, Integer>();
		Map<String, Integer> wordPhraseCountProperty = new HashMap<String, Integer>();
		Map<String, Integer> wordPhraseCountPredlod = new HashMap<String, Integer>();
		Map<String, Integer> wordHoroshoCount = new HashMap<String, Integer>();
		Map<String, Integer> wordPlohoCount = new HashMap<String, Integer>();
		Map<String, String> podlejasheeExclude = new HashMap<String, String>();
		podlejasheeExclude.put("из", "из");

		Map<String, String> razProtsent = new HashMap<String, String>();
		razProtsent.put("раз", "раз");
		razProtsent.put("процентный", "процентный");
		razProtsent.put("процентных", "процентных");
		razProtsent.put("процент", "процент");

		// Возможно перенести блок, т.к. инициализация выполняется для каждого
		// предложения
		// Тормоза
		LuceneMorphology luceneMorph = objs.getLuceneMorph();
		Map<String, List> iilobjectsList = fastMemoryModel.getIilobjects();
		Map<String, List> iiltext = fastMemoryModel.getIilobjectsToText();
		Map<String, List> iiltype = fastMemoryModel.getIilpropertyOfType();
		Map<String, List> iilproperties = fastMemoryModel.getIilotherprops();
		Map<String, List> iilinstanceList = fastMemoryModel.getOfinstance();
		Map<String, List> iilsubclassofList = fastMemoryModel.getIilsubclassof();
		Map<String, List> iilmaincategory = fastMemoryModel.getMaincategory();
		Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
		Map<String, Integer> triplet = fastMemoryModel.getNounverbdbTriplet();
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, String> iwordFraseologism = fastMemoryModel.getWordFraseologism();
		Map<String, String> igetWordExclude = fastMemoryModel.getWordExclude();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
		Map<String, String> iwordPositive = fastMemoryModel.getWordPositive();
		Map<String, String> iwordNegative = fastMemoryModel.getWordNegative();
		Map<String, String> liniarmap = nameFinderModelMEDAO.getLiniarmap();
		Map<String, String> idcategorymap = nameFinderModelMEDAO.getIdcategorymapobj();
		Map<String, String> morfema = fastMemoryModel.getMorfema();
		Map<String, String> familii = nameFinderModelMEDAO.getFamilii();
		Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
		Map<String, String> negpredlog = fastMemoryModel.getWordNegPreglog();
		Map<String, String> predlogMesta = fastMemoryModel.getWordPredlogMesta();
		Map<String, String> narechiyaVremeni = fastMemoryModel.getNarechiyaVremeni();
		Map<String, String> dniMeseats = fastMemoryModel.getDniMeseats();
		Map<String, String> politehwords = fastMemoryModel.getPolitehwords();
		Map<String, String> economicswords = fastMemoryModel.getEconomicswords();
		Map<String, String> metricsTypeOnel = fastMemoryModel.getMetricsTypeOne();
		Map<String, String> metricsTypeTwol = fastMemoryModel.getMetricsTypeTwo();
		Map<String, String> metricsTypeThreel = fastMemoryModel.getMetricsTypeThree();
		Map<String, String> wordKvantor = fastMemoryModel.getWordKvantor();
		Map<String, String> wordExplainOne = fastMemoryModel.getWordExplainOne();
		Map<String, String> wordExplainTwo = fastMemoryModel.getWordExplainTwo();
		Map<String, String> wordExplainThree = fastMemoryModel.getWordExplainThree();

		Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
		Map<String, String> nonamephrase = fastMemoryModel.getQnonamephrasedbdb();

		Map<String, List<String>> qppropertiesvalue = fastMemoryModel.getIilQPKeyValue();
		Map<String, List> iilpropertiesList = fastMemoryModel.getIilproperties();

		// Тормоза

		Map<String, String> strongmed = new HashMap<String, String>();
		strongmed = nameFinderModelMEDAO.getLiniarmapmed();

		Map<String, String> weaksynonimMapmed = new HashMap<String, String>();
		weaksynonimMapmed = nameFinderModelMEDAO.getSynonimMapmed();

		Map<String, String> weakcontextMapmed = new HashMap<String, String>();
		weakcontextMapmed = nameFinderModelMEDAO.getContextMapmed();

		Map<String, Map<String, List>> listobjects = new HashMap<String, Map<String, List>>();

		// Map<String, String> explainMap= fastMemoryModel.getExplainPrimeri();

		// stnum++;
		JSONObject srtjson = new JSONObject();
		JSONArray wordlist = new JSONArray();
		// номер строки в документе
		// srtjson.put("number", ""+stnum);

		String singltext = text;
		// if(hasVoprosSymbol(singltext)){srtjson.put("isvopros", "true");}else
		// srtjson.put("isvopros", "false");

		// начальный текст
		srtjson.put("origtext", singltext);
		// srtjson.put("datatest", "datatest");
		/*
		 * String resmalt="c"; if(getmalt){ try{
		 * resmalt=maltrest.getCollPOSdata(singltext); srtjson.put("maltdata",
		 * resmalt); } catch (Exception e) { e.printStackTrace(); } }
		 */

		ArrayList<Integer> podlejashie = new ArrayList<Integer>();
		ArrayList<Integer> skazuemie = new ArrayList<Integer>();
		ArrayList<Integer> deeprichastie = new ArrayList<Integer>();
		ArrayList<String> explainList = new ArrayList<String>();
		ArrayList<String> explainPropertyList = new ArrayList<String>();

		String dirty = singltext;
		String dirtyNoProbel = text;
		try {
			dirty = singltext.replaceAll("\\pP", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dirty = dirty.replaceAll("ё|Ё", "е");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dirtyNoProbel = dirty.replaceAll("[\\s]{2,}", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}

		String tokens[] = dirty.toString().split("\\p{P}?[\\| \\t\\n\\r]+");
		List<String[]> alltokens = new ArrayList<String[]>();
		List<String[]> alltokenstrim = new ArrayList<String[]>();
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		int size = tokens.length;
		// длина предложения
		srtjson.put("words", "" + size);

		String[] iilarrayL1 = new String[size];
		String[] nwordarrayL2 = new String[size];
		String[] morfoarrayL3 = new String[size];
		String[] emotionarrayL4 = new String[size];

		String[] emotionarrayL4OUT = new String[size];

		String[] fonemaarrayL5 = new String[size];
		String[] objectarrayL6 = new String[size];
		String[] familiiarrayL7 = new String[size];
		String[] politeharrayL8 = new String[size];
		String[] chislaarrayL9 = new String[size];
		String[] timeplaceL10 = new String[size];
		String[] kvantorL11 = new String[size];
		String[] metricsL12 = new String[size];
		String[] economicsarrayL15 = new String[size];

		ArrayList<String> cores = new ArrayList<String>();
		ArrayList<String> maincore = new ArrayList<String>();
		ArrayList<String> maincorelemma = new ArrayList<String>();

		List<IndexWrapper> probelTextCount = new ArrayList<>();
		Matcher matcher = Pattern.compile("[\\s]{2,}").matcher(dirty);
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();
			IndexWrapper p = new IndexWrapper(start, end);

			probelTextCount.add(p);

		}

		for (int i = 0; i < size; i++) {
			String[] indexsingle = new String[2];
			String[] indexbigram = new String[2];
			String[] indextrigram = new String[2];
			String[] indexfourgram = new String[2];
			String[] indexfivegram = new String[2];
			// String[] indexsixgram = new String[2];
			// String[] indexsevengram = new String[2];
			// String[] indexeightgram = new String[2];
			// String[] indexninegram = new String[2];

			String single = null;
			String bigram = null;
			String trigram = null;
			String fourgram = null;
			String fivegram = null;
			// String sixgram = null;
			// String sevengram = null;
			// String eightgram = null;
			// String ninegram = null;

			single = tokens[i];
			indexsingle[0] = single;
			indexsingle[1] = i + "|1";

			if (wordCount.containsKey(single)) {

				int t = wordCount.get(single);

				indexsingle[1] = i + "|1|" + (t + 1);
				wordCount.put(single, t + 1);

			} else {
				wordCount.put(single, 1);
				indexsingle[1] = i + "|1|1";
			}

			if (i + 1 < size) {
				bigram = tokens[i] + " " + tokens[i + 1];
				indexbigram[0] = bigram;
				indexbigram[1] = i + "|2";
				if (wordCount.containsKey(bigram)) {

					int t = wordCount.get(bigram);
					indexbigram[1] = i + "|2|" + (t + 1);
					wordCount.put(bigram, t + 1);

				} else {
					wordCount.put(bigram, 1);
					indexbigram[1] = i + "|2|1";
				}

			}
			if (i + 2 < size) {
				trigram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
				indextrigram[0] = trigram;
				indextrigram[1] = i + "|3";

				if (wordCount.containsKey(trigram)) {

					int t = wordCount.get(trigram);
					indextrigram[1] = i + "|3|" + (t + 1);
					wordCount.put(trigram, t + 1);

				} else {
					wordCount.put(trigram, 1);
					indextrigram[1] = i + "|3|1";
				}

			}
			if (i + 3 < size) {
				fourgram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2] + " " + tokens[i + 3];
				indexfourgram[0] = fourgram;
				indexfourgram[1] = i + "|4";

				if (wordCount.containsKey(fourgram)) {

					int t = wordCount.get(fourgram);
					indexfourgram[1] = i + "|4|" + (t + 1);
					wordCount.put(fourgram, t + 1);

				} else {
					wordCount.put(fourgram, 1);
					indexfourgram[1] = i + "|4|1";
				}

			}
			if (i + 4 < size) {
				fivegram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2] + " " + tokens[i + 3] + " "
						+ tokens[i + 4];
				indexfivegram[0] = fivegram;
				indexfivegram[1] = i + "|5";

				if (wordCount.containsKey(fivegram)) {

					int t = wordCount.get(fivegram);
					indexfivegram[1] = i + "|5|" + (t + 1);
					wordCount.put(fivegram, t + 1);

				} else {
					wordCount.put(fivegram, 1);
					indexfivegram[1] = i + "|5|1";
				}

			}
			/*
			 * if (i + 5 < size) { sixgram = tokens[i] + " " + tokens[i + 1] +
			 * " " + tokens[i + 2] + " " + tokens[i + 3] + " " + tokens[i + 4] +
			 * " " + tokens[i + 5]; indexsixgram[0] = sixgram; indexsixgram[1] =
			 * i + "|6";
			 * 
			 * if (wordCount.containsKey(sixgram)) {
			 * 
			 * int t = wordCount.get(sixgram); indexsixgram[1] = i + "|6|" + (t
			 * + 1); wordCount.put(sixgram, t + 1);
			 * 
			 * } else { wordCount.put(sixgram, 1); indexsixgram[1] = i + "|6|1";
			 * }
			 * 
			 * }
			 * 
			 * if (i + 6 < size) { sevengram = tokens[i] + " " + tokens[i + 1] +
			 * " " + tokens[i + 2] + " " + tokens[i + 3] + " " + tokens[i + 4] +
			 * " " + tokens[i + 5] + " " + tokens[i + 6]; indexsevengram[0] =
			 * sevengram; indexsevengram[1] = i + "|7";
			 * 
			 * if (wordCount.containsKey(sevengram)) {
			 * 
			 * int t = wordCount.get(sevengram); indexsevengram[1] = i + "|7|" +
			 * (t + 1); wordCount.put(sevengram, t + 1);
			 * 
			 * } else { wordCount.put(sevengram, 1); indexsevengram[1] = i +
			 * "|7|1"; }
			 * 
			 * }
			 * 
			 * if (i + 7 < size) { eightgram = tokens[i] + " " + tokens[i + 1] +
			 * " " + tokens[i + 2] + " " + tokens[i + 3] + " " + tokens[i + 4] +
			 * " " + tokens[i + 5] + " " + tokens[i + 6] + " " + tokens[i + 7];
			 * indexeightgram[0] = eightgram; indexeightgram[1] = i + "|8";
			 * 
			 * if (wordCount.containsKey(eightgram)) {
			 * 
			 * int t = wordCount.get(eightgram); indexeightgram[1] = i + "|8|" +
			 * (t + 1); wordCount.put(eightgram, t + 1);
			 * 
			 * } else { wordCount.put(eightgram, 1); indexeightgram[1] = i +
			 * "|8|1"; }
			 * 
			 * }
			 * 
			 * if (i + 8 < size) { ninegram = tokens[i] + " " + tokens[i + 1] +
			 * " " + tokens[i + 2] + " " + tokens[i + 3] + " " + tokens[i + 4] +
			 * " " + tokens[i + 5] + " " + tokens[i + 6] + " " + tokens[i + 7] +
			 * " " + tokens[i + 8]; indexninegram[0] = ninegram;
			 * indexninegram[1] = i + "|9";
			 * 
			 * if (wordCount.containsKey(ninegram)) {
			 * 
			 * int t = wordCount.get(ninegram); indexninegram[1] = i + "|9|" +
			 * (t + 1); wordCount.put(eightgram, t + 1);
			 * 
			 * } else { wordCount.put(ninegram, 1); indexninegram[1] = i +
			 * "|9|1"; }
			 * 
			 * }
			 */
			if (single != null)
				alltokens.add(indexsingle);
			if (bigram != null)
				alltokens.add(indexbigram);
			if (trigram != null)
				alltokens.add(indextrigram);
			if (fourgram != null)
				alltokens.add(indexfourgram);
			if (fivegram != null)
				alltokens.add(indexfivegram);
			/*
			 * if (sixgram != null) alltokens.add(indexsixgram);
			 * 
			 * if (sevengram != null) alltokens.add(indexsevengram);
			 * 
			 * if (eightgram != null) alltokens.add(indexeightgram);
			 * 
			 * if (ninegram != null) alltokens.add(indexninegram);
			 */
			if (i > 15000) {
				break;
			}
		}

		// Возможны торможения
		for (String[] wordtokend : alltokens) {
			String[] index = new String[2];
			index[0] = wordtokend[0].trim();
			index[1] = wordtokend[1];

			alltokenstrim.add(index);
		}
		alltokens = alltokenstrim;

		Map<String, String> findobject = new HashMap<String, String>();
		Map<String, Integer> categoryMap = new HashMap<String, Integer>();
		Map<String, Integer> verbCount = new HashMap<String, Integer>();
		Map<String, Integer> adjectiveCount = new HashMap<String, Integer>();

		// List<String, Map> listobjects = new ArrayList<String, Map>();

		List<FindObject> listArray = new ArrayList<FindObject>();
		Map<String, Integer[]> predicateswordlist = new HashMap<String, Integer[]>();
		try {

			for (String[] wordtoken : alltokens) {

				try {

					String lowerStr = wordtoken[0].toLowerCase();

					// ==================================medical
					// objs=========================================
					try {

						// анализ основных объектов по токенам
						String objectid = strongmed.get(lowerStr);

						if (objectid != null) {
							FindObject obj = new FindObject();
							obj.setId(objectid);
							obj.setIsmat("false");
							// это токен N
							obj.setName(wordtoken[0]);
							// это расчет первого вхождения 3672 РФ - на 1
							// Токене
							// нужно вхождение на N токене и нграмма T:N

							// int b=getPisitionB(dirty, wordtoken);
							int[] beginend = new int[2];
							beginend = getPisitionC(dirtyNoProbel, wordtoken, probelTextCount);
							int b = beginend[0];
							int d = beginend[1];

							obj.setStrpos("" + b);
							obj.setStrend("" + d);
							if (b > -1)
								listArray.add(obj);
							else if (b == -1) {
								b = 0;
								d = wordtoken[0].length();
								obj.setStrpos("" + b);
								obj.setStrend("" + d);

								listArray.add(obj);
							}
						}

						// анализ слабых синонимов объектов по токенам

						String weaksynonim = weaksynonimMapmed.get(lowerStr);

						if (weaksynonim != null) {
							FindObject weakobj = new FindObject();

							String weakobjectid = getMyweekID(wordtoken, weaksynonim, lowerStr, weakcontextMapmed,
									alltokens, dirty);

							if (weakobjectid != null) {

								weakobj.setId(weakobjectid);
								weakobj.setName(wordtoken[0]);
								weakobj.setIsmat("false");
								int[] beginend = new int[2];
								beginend = getPisitionC(dirtyNoProbel, wordtoken, probelTextCount);
								int bweak = beginend[0];
								int dweak = beginend[1];

								weakobj.setStrpos("" + bweak);
								weakobj.setStrend("" + dweak);

								if (bweak > -1)
									listArray.add(weakobj);
								else if (bweak == -1) {
									bweak = 0;
									dweak = wordtoken[0].length();
									weakobj.setStrpos("" + bweak);
									weakobj.setStrend("" + dweak);
									listArray.add(weakobj);
								}
							}

						}

					} catch (Exception e) {
					}

					// ==================================medical
					// objs=========================================

					String tokenleght = "";
					String wordPosition = "";
					int iwordPosition = 0;
					int itokenleght = 0;
					// wordtoken[1] метрика слова или фразы "1|9|1" позиция
					// начала / количество слов \ повторяемость
					if (wordtoken[1].contains("|")) {
						String[] myData = wordtoken[1].split("\\|");
						tokenleght = myData[1];
						wordPosition = myData[0];
						iwordPosition = Integer.parseInt(wordPosition);
						itokenleght = Integer.parseInt(tokenleght);
					}
					// ----2 поискать сущ

					String poly = politehwords.get(lowerStr);
					if (poly != null) {

						try {
							politeharrayL8[iwordPosition] = poly;
							if (itokenleght > 1) {
								int co = itokenleght - 1;
								for (int o = 0; o < co; o++)
									politeharrayL8[iwordPosition + co] = poly;

							}
						} catch (Exception e) {
						}
					}

					String economics = economicswords.get(lowerStr);
					if (economics != null) {

						try {
							economicsarrayL15[iwordPosition] = economics;
							if (itokenleght > 1) {
								int co = itokenleght - 1;
								for (int o = 0; o < co; o++)
									economicsarrayL15[iwordPosition + co] = economics;

							}
						} catch (Exception e) {
						}
					}
					// анализ основных объектов по токенам
					String objectid = liniarmap.get(lowerStr);

					if (objectid != null) {

						findobject.put("object" + wordPosition, objectid + "|" + wordtoken[1]);
						try {
							objectarrayL6[iwordPosition] = objectid;
							if (itokenleght > 1) {
								int co = itokenleght - 1; // 3
								for (int o = 0; o < co; o++)
									objectarrayL6[iwordPosition + co] = objectid;

							}
						} catch (Exception e) {
						}

						wordlist.add(findobject);
						// resultarray.append(wordtoken[0]+"["+objectid+"] ");
					}

					try {
						if (tokenleght.equals("1")) {
							JSONObject wordjson = new JSONObject();
							// позиция слова и слово
							// wordjson.put("рosition",
							// ""+wordPosition+"-"+lowerStr);

							boolean familiinFind = true;
							try {
								String familiiid = familii.get(lowerStr);

								if (familiiid != null) {
									if (Character.isUpperCase(wordtoken[0].codePointAt(0))) {
										familiiarrayL7[iwordPosition] = lowerStr;
										wordjson.put("familii", lowerStr);
										familiinFind = false;
									}
								}
							} catch (Exception e) {
							}

							String wordform = "";

							// !!!!!!!!!!!!!!!!!!!!!!!!!!!не нашел слово === ищи
							// синоним - правоохранители
							String wrd = lowerStr.replaceAll("[^А-я]", "").trim();
							List<String> wordBaseForms = null;
							if (!"".equals(wrd)) {
								wordBaseForms = luceneMorph.getMorphInfo(wrd);
							}

							int sizer = 0;
							try {
								sizer = wordBaseForms.size();
							} catch (Exception e) {
							}
							boolean ifindemo = true;
							if (sizer > 0) {

								StringBuilder sb = new StringBuilder();
								for (String s : wordBaseForms) {
									sb.append(s);
									sb.append(" #");
								}

								String ist = sb.toString();
								if (!ist.equals("") && ist != null) {
									wordform = ist;
								}

								if (wordform.contains("|")) {
									String[] myData = wordform.split("\\|");
									String nword = myData[0].trim();
									String morf = myData[1];
									// Начальная форма слова и морфология
									wordjson.put("nword", nword);
									wordjson.put("morf", morf);

									nwordarrayL2[iwordPosition] = nword;
									morfoarrayL3[iwordPosition] = morf;

									boolean[] rule = new boolean[2];
									rule[0] = false;
									rule[1] = false;

									if (!podlejasheeExclude.containsKey(lowerStr))
										rule = analysis.podlejasheeRule(morf);

									if (rule[0] == true || rule[1] == true) {
										// Подлежащее
										wordjson.put("rule1", true);
										podlejashie.add(iwordPosition);
									} else {
										boolean deepr =false;
										try {   deepr = analysis.deeprichastieRule(morf);} catch (Exception e) {}
										     if(deepr){
										    	 //if(thisIsVeb(nword,luceneMorph))
												deeprichastie.add(iwordPosition);
												 //System.out.println("D1==="+iwordPosition);
											 }else{
												boolean[] srule = new boolean[2];
												srule[0] = false;
												srule[1] = false;
												srule = analysis.skazuemoeRule(nword, morf, "", "");
												
												if (srule[0] == true && srule[1] == true) {
													// сказуемое
													wordjson.put("rule2", true);
													skazuemie.add(iwordPosition);
													// resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb2word#
													// ");
		
												} else if (srule[0] == true) {
													wordjson.put("rule3", true);
													// resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb1#
													// ");
													skazuemie.add(iwordPosition);
												}
										}
									}

									if (familiinFind) {
										try {
											String familiiid = familii.get(nword);

											if (familiiid != null) {
												if (Character.isUpperCase(wordtoken[0].codePointAt(0))) {
													familiiarrayL7[iwordPosition] = nword;
													familiinFind = false;
												}
											}
										} catch (Exception e) {
										}
									}
									try {
										if (!predicateswordlist.containsKey(lowerStr)) {
											Integer[] wordcont = new Integer[1];
											wordcont[0] = iwordPosition;
											predicateswordlist.put(lowerStr, wordcont);
										} else {
											Integer[] wordcontold = predicateswordlist.get(lowerStr);
											int oldsize = wordcontold.length;
											Integer[] wordcont = new Integer[oldsize + 1];
											for (int kl = 0; kl < oldsize; kl++) {
												wordcont[kl] = wordcontold[kl];
											}
											wordcont[oldsize] = iwordPosition;
											predicateswordlist.put(lowerStr, wordcont);
										}
								} catch (Exception e) {
								}
										
									// --------------------------MORFEMA----------------------------

									// Пожарная команда для морфем в виде????
									// синонимов, семантик, Q объектов
									if (!igetWordExclude.containsKey(lowerStr)) {

										

										boolean foundtonality = false;
										if (morfema.containsKey(nword)) {
											String[] morfjson = new String[3];
											morfjson = getMorfema(nword, morfema, iwordPositive, iwordNegative);

											String neg = morfjson[0];
											String pos = morfjson[1];
											String allposneg = morfjson[2];
											if (neg != null || pos != null) {
												// morfjson тональность слова
												wordjson.put("morfjson",
														"neg=" + neg + " pos=" + pos + " all=" + allposneg);// foundtonality=true;

												// emotionarrayL4[iwordPosition]=neg+"|"+pos;
												// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/a"+neg+"|"+pos;
												String prdf = "";

												if (iwordPosition != 0) {
													try {
														prdf = nwordarrayL2[iwordPosition - 1];
													} catch (Exception e) {
													}
													if (negpredlog.containsKey(prdf)) {
														if (neg != null) {
															int negt = allemmotionMap.get("positive");
															allemmotionMap.put("positive", negt + 1);
															String past = emotionarrayL4OUT[iwordPosition];
															if (past != null)
																emotionarrayL4OUT[iwordPosition] = past + "#false|true";
															else
																emotionarrayL4OUT[iwordPosition] = "#false|true";
														}
														if (pos != null) {
															int post = allemmotionMap.get("negative");
															allemmotionMap.put("negative", post + 1);
															String past = emotionarrayL4OUT[iwordPosition];
															if (past != null)
																emotionarrayL4OUT[iwordPosition] = past + "#true|false";
															else
																emotionarrayL4OUT[iwordPosition] = "#true|false";
														}
													} else {
														if (neg != null) {
															int negt = allemmotionMap.get("negative");
															allemmotionMap.put("negative", negt + 1);
															String past = emotionarrayL4OUT[iwordPosition];
															if (past != null)
																emotionarrayL4OUT[iwordPosition] = past + "#true|false";
															else
																emotionarrayL4OUT[iwordPosition] = "#true|false";
														}
														if (pos != null) {
															int post = allemmotionMap.get("positive");
															allemmotionMap.put("positive", post + 1);
															String past = emotionarrayL4OUT[iwordPosition];
															if (past != null)
																emotionarrayL4OUT[iwordPosition] = past + "#false|true";
															else
																emotionarrayL4OUT[iwordPosition] = "#false|true";
														}
													}
													foundtonality = true;
												} else {
													if (neg != null) {
														int negt = allemmotionMap.get("negative");
														allemmotionMap.put("negative", negt + 1);
													}
													if (pos != null) {
														int post = allemmotionMap.get("positive");
														allemmotionMap.put("positive", post + 1);
													}
													foundtonality = true;
												}

											}
										}
										// поиск тональности в синонимах
										if (!foundtonality) {
											// get sin
											// это не оригональное слово
											// lowerStr
											try {

												String syn = iwordSynonims.get(nword);
												if (syn != null) {
													String[] newwords;
													if (syn.contains("/")) {

														newwords = syn.split("\\/");

													}
													if (syn.contains(",")) {

														newwords = syn.split(",");

													} else {

														String[] synindex = new String[1];
														synindex[0] = syn;
														newwords = synindex;
													}
													int siz = newwords.length;

													for (int ik = 0; ik < siz; ik++) {
														// Если найдено значение
														// - выйти из цикла
														String[] morfjson = new String[3];
														wordjson.put("morfjsonsyn" + ik, "word=" + newwords[ik].trim());
														morfjson = getMorfema(newwords[ik].trim(), morfema,
																iwordPositive, iwordNegative);
														String neg = morfjson[0];
														String pos = morfjson[1];
														String allposneg = morfjson[2];
														if (neg != null || pos != null) {
															wordjson.put("morfjsonsyn" + ik,
																	"neg=" + neg + " pos=" + pos + " all=" + allposneg);// foundtonality=true;
															// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/s"+"["+newwords[ik]+"]"+neg+"|"+pos;

															String prdf = "";

															if (iwordPosition != 0) {
																try {
																	prdf = nwordarrayL2[iwordPosition - 1];
																} catch (Exception e) {
																}
																if (negpredlog.containsKey(prdf)) {
																	if (neg != null) {
																		int negt = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", negt + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#false|true";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#false|true";
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", post + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#true|false";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#true|false";
																	}
																} else {
																	if (neg != null) {
																		int negt = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", negt + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#true|false";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#true|false";
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", post + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#false|true";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#false|true";
																	}
																}
																foundtonality = true;
															} else {
																if (neg != null) {
																	int negt = allemmotionMap.get("negative");
																	allemmotionMap.put("negative", negt + 1);
																}
																if (pos != null) {
																	int post = allemmotionMap.get("positive");
																	allemmotionMap.put("positive", post + 1);
																}
																foundtonality = true;
															}
														}
													}
												}
											} catch (Exception e) {
												e.printStackTrace();
											}
										}

										// поиск тональности в семантик
										if (!foundtonality) {
											// это не оригональное слово
											// lowerStr

											try {
												ArrayList tempdoList = null;
												tempdoList = iSymantex.get(nword);
												if (tempdoList != null) {
													int j = 0;
													for (Object value : tempdoList) {
														j++;
														String[] index = new String[2];
														index = (String[]) value;

														String iSymantexword = index[0];
														String itype = index[1];
														// wordjson.put("isymantex"+j,
														// "word="+iSymantexword);
														String[] morfjson = new String[3];
														iSymantexword = iSymantexword.trim();
														morfjson = getMorfema(iSymantexword, morfema, iwordPositive,
																iwordNegative);
														String neg = morfjson[0];
														String pos = morfjson[1];
														String allposneg = morfjson[2];
														if (neg != null || pos != null) {
															wordjson.put("isymantex" + j,
																	"word=" + iSymantexword + " neg=" + neg + " pos="
																			+ pos + " all=" + allposneg);// foundtonality=true;

															// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/i"+"["+iSymantexword+"^"+itype+"]"+neg+"|"+pos;

															String prdf = "";

															if (iwordPosition != 0) {
																try {
																	prdf = nwordarrayL2[iwordPosition - 1];
																} catch (Exception e) {
																}
																if (negpredlog.containsKey(prdf)
																		|| itype.equals("antonyms")) {
																	if (neg != null) {
																		int negt = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", negt + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#false|true";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#false|true";
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", post + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#true|false";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#true|false";
																	}
																} else {
																	if (neg != null) {
																		int negt = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", negt + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#true|false";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#true|false";
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", post + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#false|true";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#false|true";
																	}
																}
																foundtonality = true;
																break;
															} else {
																if (itype.equals("antonyms")) {
																	if (neg != null) {
																		int negt = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", negt + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#false|true";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#false|true";
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", post + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#true|false";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#true|false";
																	}
																} else {
																	if (neg != null) {
																		int negt = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", negt + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#true|false";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#true|false";
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", post + 1);
																		String past = emotionarrayL4OUT[iwordPosition];
																		if (past != null)
																			emotionarrayL4OUT[iwordPosition] = past
																					+ "#false|true";
																		else
																			emotionarrayL4OUT[iwordPosition] = "#false|true";
																	}
																}
																foundtonality = true;
																break;
															}

														}

													}
												}
											} catch (Exception e) {
												e.printStackTrace();
											}
										}

										// поиск тональности Q объектов
										if (!foundtonality) {

											try {
												List<String> qindexList = iilobjectsList.get(nword);
												int j = 0;
												if (qindexList != null)
													for (String qindex : qindexList) {

														if (qindex != null) {
															j++;

															String words = getTextofQ(qindex, iiltext);
															// String
															// wordsprop=getPropertyOfType(qindex,
															// iiltype);

															String[] morfjson = new String[3];
															//wordjson.put("qdata" + j, "qdata=" + words);
															morfjson = getMorfema(words, morfema, iwordPositive,
																	iwordNegative);
															String neg = morfjson[0];
															String pos = morfjson[1];
															String allposneg = morfjson[2];
															if (neg != null || pos != null) {
																//wordjson.put("qdata" + j, "neg=" + neg + " pos=" + pos+ " all=" + allposneg);// foundtonality=true;
																// emotionarrayL4[iwordPosition]=neg+"|0|"+words+"#"+pos+"|0|"+words;
																// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/q"+"["+words+"]"+neg+"|"+pos;

																String prdf = "";

																if (iwordPosition != 0) {
																	try {
																		prdf = nwordarrayL2[iwordPosition - 1];
																	} catch (Exception e) {
																	}
																	if (negpredlog.containsKey(prdf)) {
																		if (neg != null) {
																			int negt = allemmotionMap.get("positive");
																			allemmotionMap.put("positive", negt + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#false|true";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#false|true";
																		}
																		if (pos != null) {
																			int post = allemmotionMap.get("negative");
																			allemmotionMap.put("negative", post + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#true|false";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#true|false";
																		}
																	} else {
																		if (neg != null) {
																			int negt = allemmotionMap.get("negative");
																			allemmotionMap.put("negative", negt + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#true|false";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#true|false";
																		}
																		if (pos != null) {
																			int post = allemmotionMap.get("positive");
																			allemmotionMap.put("positive", post + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#false|true";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#false|true";
																		}
																	}
																	foundtonality = true;
																} else {
																	if (neg != null) {
																		int negt = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", negt + 1);
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", post + 1);
																	}
																	foundtonality = true;
																}

															}
														}

													}
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}

								}

							} else {
								// Пожарная команда для морфологии ???
								// синонимов, семантик, Q объектов
								try {
									String syn = iwordSynonims.get(lowerStr);
									// get sin
									String[] newwords;
									if (syn != null) {
										if (syn.contains("/")) {
											newwords = syn.split("\\/");

										} else {

											String[] synindex = new String[1];
											synindex[0] = syn;
											newwords = synindex;
										}
										int siz = newwords.length;

										for (int ik = 0; ik < siz; ik++) {

											String mysynonim = clean(newwords[ik]).toLowerCase().trim();
											String wrdi = mysynonim.replaceAll("[^А-я]", "").trim();
											List<String> swordBaseForms = null;
											if (!"".equals(wrdi)) {
												swordBaseForms = luceneMorph.getMorphInfo(wrdi);
											}

											int sizers = 0;
											try {
												sizers = swordBaseForms.size();
											} catch (Exception e) {
											}

											if (sizers > 0) {

												StringBuilder sb = new StringBuilder();
												for (String s : swordBaseForms) {
													sb.append(s);
													sb.append(" #");
												}

												String ist = sb.toString();
												if (!ist.equals("") && ist != null) {
													wordform = ist;
												}

												if (wordform.contains("|")) {
													String[] myData = wordform.split("\\|");
													String morf = myData[1];
													String nword = myData[0].trim();
													boolean[] rule = new boolean[2];
													rule[0] = false;
													rule[1] = false;
													if (!podlejasheeExclude.containsKey(lowerStr))
														rule = analysis.podlejasheeRule(morf);

													if (rule[0] == true || rule[1] == true) {

														wordjson.put("rule4", true);

														podlejashie.add(iwordPosition);
														// не введено слово
														// синоним в выдачу
													} else {
														
														boolean deepr =false;
														try {   deepr = analysis.deeprichastieRule(morf);} catch (Exception e) {}
														     if(deepr){
														    	 //System.out.println("D2==="+iwordPosition);
														    	 //if(thisIsVeb(nword,luceneMorph))
																 deeprichastie.add(iwordPosition);
															 }else{
														boolean[] srule = new boolean[2];
														srule[0] = false;
														srule[1] = false;
														srule = analysis.skazuemoeRule(nword, morf, "", "");
														if (srule[0] == true && srule[1] == true) {
															wordjson.put("rule5", true);
															skazuemie.add(iwordPosition);
															// resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb2word#
															// ");
														} else if (srule[0] == true) {
															wordjson.put("rule6", true);
															// resultarray.append(wordtoken[0]+"["+nword+"]").append("-").append(wordtoken[1]).append("-").append("verb1#
															// ");
															// не введено слово
															// синоним в выдачу
															skazuemie.add(iwordPosition);
														}
															 }
													}

													// --------------------------MORFEMA----------------------------

													// Пожарная команда для
													// морфем в виде????
													// синонимов, семантик, Q
													// объектов
													if (!igetWordExclude.containsKey(lowerStr)) {

														boolean foundtonality = false;
														if (morfema.containsKey(nword)) {
															String[] morfjson = new String[3];
															morfjson = getMorfema(nword, morfema, iwordPositive,
																	iwordNegative);

															String neg = morfjson[0];
															String pos = morfjson[1];
															String allposneg = morfjson[2];
															if (neg != null || pos != null) {
																wordjson.put("morfjson", "neg=" + neg + " pos=" + pos
																		+ " all=" + allposneg);// foundtonality=true;
																								// emotionarrayL4[iwordPosition]=neg+"|"+pos;
																								// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/aa"+neg+"|"+pos;
																String prdf = "";

																if (iwordPosition != 0) {
																	try {
																		prdf = nwordarrayL2[iwordPosition - 1];
																	} catch (Exception e) {
																	}
																	if (negpredlog.containsKey(prdf)) {
																		if (neg != null) {
																			int negt = allemmotionMap.get("positive");
																			allemmotionMap.put("positive", negt + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#false|true";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#false|true";
																		}
																		if (pos != null) {
																			int post = allemmotionMap.get("negative");
																			allemmotionMap.put("negative", post + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#true|false";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#true|false";
																		}
																	} else {
																		if (neg != null) {
																			int negt = allemmotionMap.get("negative");
																			allemmotionMap.put("negative", negt + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#true|false";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#true|false";
																		}
																		if (pos != null) {
																			int post = allemmotionMap.get("positive");
																			allemmotionMap.put("positive", post + 1);
																			String past = emotionarrayL4OUT[iwordPosition];
																			if (past != null)
																				emotionarrayL4OUT[iwordPosition] = past
																						+ "#false|true";
																			else
																				emotionarrayL4OUT[iwordPosition] = "#false|true";
																		}
																	}
																	foundtonality = true;
																} else {
																	if (neg != null) {
																		int negt = allemmotionMap.get("negative");
																		allemmotionMap.put("negative", negt + 1);
																	}
																	if (pos != null) {
																		int post = allemmotionMap.get("positive");
																		allemmotionMap.put("positive", post + 1);
																	}
																	foundtonality = true;
																}
															}
														}
														// поиск тональности в
														// синонимах
														if (!foundtonality) {
															// get sin
															// это не
															// оригональное
															// слово lowerStr
															try {
																String synl = iwordSynonims.get(nword);
																if (synl != null) {
																	String[] newwordsl;
																	if (syn.contains("/")) {

																		newwordsl = synl.split("\\/");

																	}
																	if (synl.contains(",")) {

																		newwordsl = synl.split(",");

																	} else {

																		String[] synindex = new String[1];
																		synindex[0] = synl;
																		newwordsl = synindex;
																	}
																	int sizl = newwordsl.length;

																	for (int ikl = 0; ikl < sizl; ikl++) {
																		// Если
																		// найдено
																		// значение
																		// -
																		// выйти
																		// из
																		// цикла
																		String[] morfjson = new String[3];
																		wordjson.put("morfjsonsyn" + ikl,
																				"word=" + newwordsl[ikl].trim());
																		morfjson = getMorfema(newwordsl[ikl].trim(),
																				morfema, iwordPositive, iwordNegative);
																		String neg = morfjson[0];
																		String pos = morfjson[1];
																		String allposneg = morfjson[2];
																		if (neg != null || pos != null) {
																			wordjson.put("morfjsonsyn" + ikl,
																					"neg=" + neg + " pos=" + pos
																							+ " all=" + allposneg);// foundtonality=true;
																													// emotionarrayL4[iwordPosition]=neg+"|"+pos;
																													// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/sd"+"["+newwords[ik]+"]"+neg+"|"+pos;
																			String prdf = "";

																			if (iwordPosition != 0) {
																				try {
																					prdf = nwordarrayL2[iwordPosition
																							- 1];
																				} catch (Exception e) {
																				}
																				if (negpredlog.containsKey(prdf)) {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								negt + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#false|true";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#false|true";
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								post + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#true|false";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#true|false";
																					}
																				} else {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								negt + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#true|false";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#true|false";
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								post + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#false|true";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#false|true";
																					}
																				}
																				foundtonality = true;
																			} else {
																				if (neg != null) {
																					int negt = allemmotionMap
																							.get("negative");
																					allemmotionMap.put("negative",
																							negt + 1);
																				}
																				if (pos != null) {
																					int post = allemmotionMap
																							.get("positive");
																					allemmotionMap.put("positive",
																							post + 1);
																				}
																				foundtonality = true;
																			}
																		}
																	}
																}
															} catch (Exception e) {
																e.printStackTrace();
															}
														}

														// поиск тональности в
														// семантик
														if (!foundtonality) {
															// это не
															// оригональное
															// слово lowerStr

															try {
																ArrayList tempdoList = null;
																tempdoList = iSymantex.get(nword);
																if (tempdoList != null) {
																	int j = 0;
																	for (Object value : tempdoList) {
																		j++;
																		String[] index = new String[2];
																		index = (String[]) value;
																		String itype = index[1];

																		String iSymantexword = index[0];
																		// wordjson.put("isymantex"+j,
																		// "word="+iSymantexword);
																		String[] morfjson = new String[3];
																		iSymantexword = iSymantexword.trim();
																		morfjson = getMorfema(iSymantexword, morfema,
																				iwordPositive, iwordNegative);
																		String neg = morfjson[0];
																		String pos = morfjson[1];
																		String allposneg = morfjson[2];
																		if (neg != null || pos != null) {
																			wordjson.put("isymantex" + j,
																					"word=" + iSymantexword + " neg="
																							+ neg + " pos=" + pos
																							+ " all=" + allposneg);// foundtonality=true;
																													// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/ii"+"["+iSymantexword+"^"+itype+"]"+neg+"|"+pos;

																			String prdf = "";

																			if (iwordPosition != 0) {
																				try {
																					prdf = nwordarrayL2[iwordPosition
																							- 1];
																				} catch (Exception e) {
																				}
																				if (negpredlog.containsKey(prdf)
																						|| itype.equals("antonyms")) {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								negt + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#false|true";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#false|true";
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								post + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#true|false";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#true|false";
																					}
																				} else {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								negt + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#true|false";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#true|false";
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								post + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#false|true";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#false|true";
																					}
																				}
																				foundtonality = true;
																				break;
																			} else {
																				if (itype.equals("antonyms")) {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								negt + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#false|true";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#false|true";
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								post + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#true|false";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#true|false";
																					}
																				} else {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								negt + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#true|false";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#true|false";
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								post + 1);
																						String past = emotionarrayL4OUT[iwordPosition];
																						if (past != null)
																							emotionarrayL4OUT[iwordPosition] = past
																									+ "#false|true";
																						else
																							emotionarrayL4OUT[iwordPosition] = "#false|true";
																					}
																				}
																				foundtonality = true;
																				break;
																			}
																		}

																		// ===================================ГИПЕРОНИМЫ===СИНОНИМЫ==================================================
																		// resultarray.append(lowerStr).append("-").append(index[0]+"
																		// Type
																		// :
																		// "+index[1]).append("-").append("rule4");

																	}
																}
															} catch (Exception e) {
																e.printStackTrace();
															}
														}

														// поиск тональности Q
														// объектов
														if (!foundtonality) {

															try {
																List<String> qindexList = iilobjectsList.get(nword);
																int j = 0;
																if (qindexList != null)
																	for (String qindex : qindexList) {

																		if (qindex != null) {
																			j++;

																			String words = getTextofQ(qindex, iiltext);
																			// String
																			// wordsprop=getPropertyOfType(qindex,
																			// iiltype);

																			String[] morfjson = new String[3];
																			//wordjson.put("qdata" + j, "qdata=" + words);
																			morfjson = getMorfema(words, morfema,
																					iwordPositive, iwordNegative);
																			String neg = morfjson[0];
																			String pos = morfjson[1];
																			String allposneg = morfjson[2];
																			if (neg != null || pos != null) {
																				//wordjson.put("qdata" + j,
																				//		"neg=" + neg + " pos=" + pos
																				//				+ " all=" + allposneg);// foundtonality=true;
																														// emotionarrayL4[iwordPosition]=neg+"|0|"+words+"#"+pos+"|0|"+words;
																														// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/qq"+"["+words+"]"+neg+"|"+pos;
																				String prdf = "";

																				if (iwordPosition != 0) {
																					try {
																						prdf = nwordarrayL2[iwordPosition
																								- 1];
																					} catch (Exception e) {
																					}
																					if (negpredlog.containsKey(prdf)) {
																						if (neg != null) {
																							int negt = allemmotionMap
																									.get("positive");
																							allemmotionMap.put(
																									"positive",
																									negt + 1);
																							String past = emotionarrayL4OUT[iwordPosition];
																							if (past != null)
																								emotionarrayL4OUT[iwordPosition] = past
																										+ "#false|true";
																							else
																								emotionarrayL4OUT[iwordPosition] = "#false|true";
																						}
																						if (pos != null) {
																							int post = allemmotionMap
																									.get("negative");
																							allemmotionMap.put(
																									"negative",
																									post + 1);
																							String past = emotionarrayL4OUT[iwordPosition];
																							if (past != null)
																								emotionarrayL4OUT[iwordPosition] = past
																										+ "#true|false";
																							else
																								emotionarrayL4OUT[iwordPosition] = "#true|false";
																						}
																					} else {
																						if (neg != null) {
																							int negt = allemmotionMap
																									.get("negative");
																							allemmotionMap.put(
																									"negative",
																									negt + 1);
																							String past = emotionarrayL4OUT[iwordPosition];
																							if (past != null)
																								emotionarrayL4OUT[iwordPosition] = past
																										+ "#true|false";
																							else
																								emotionarrayL4OUT[iwordPosition] = "#true|false";
																						}
																						if (pos != null) {
																							int post = allemmotionMap
																									.get("positive");
																							allemmotionMap.put(
																									"positive",
																									post + 1);
																							String past = emotionarrayL4OUT[iwordPosition];
																							if (past != null)
																								emotionarrayL4OUT[iwordPosition] = past
																										+ "#false|true";
																							else
																								emotionarrayL4OUT[iwordPosition] = "#false|true";
																						}
																					}
																					foundtonality = true;
																				} else {
																					if (neg != null) {
																						int negt = allemmotionMap
																								.get("negative");
																						allemmotionMap.put("negative",
																								negt + 1);
																					}
																					if (pos != null) {
																						int post = allemmotionMap
																								.get("positive");
																						allemmotionMap.put("positive",
																								post + 1);
																					}
																					foundtonality = true;
																				}
																			}
																		}

																	}
															} catch (Exception e) {
																e.printStackTrace();
															}
														}
													}

												}
												break;
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							}
							// У этого множества созданного руками нет
							// приоритета
							if (!igetWordExclude.containsKey(lowerStr)) {
								String pos = null;
								String neg = null;
								try {
									pos = iwordPositive.get(lowerStr);
								} catch (Exception e) {
									e.printStackTrace();
								}

								try {
									neg = iwordNegative.get(lowerStr);
								} catch (Exception e) {
									e.printStackTrace();
								}

								if (neg != null || pos != null) {
									wordjson.put("posneglin", "neg=" + neg + " pos=" + pos);
									// emotionarrayL4[iwordPosition]=neg+"|0|"+lowerStr+"#"+pos+"|0|"+lowerStr;
									// emotionarrayL4[iwordPosition]=emotionarrayL4[iwordPosition]+"/f"+neg+"|"+pos;
									String prdf = "";

									if (iwordPosition != 0) {
										try {
											prdf = nwordarrayL2[iwordPosition - 1];
										} catch (Exception e) {
										}
										if (negpredlog.containsKey(prdf)) {
											if (neg != null) {
												int negt = allemmotionMap.get("positive");
												allemmotionMap.put("positive", negt + 1);
												String past = emotionarrayL4OUT[iwordPosition];
												if (past != null)
													emotionarrayL4OUT[iwordPosition] = past + "#false|true";
												else
													emotionarrayL4OUT[iwordPosition] = "#false|true";
											}
											if (pos != null) {
												int post = allemmotionMap.get("negative");
												allemmotionMap.put("negative", post + 1);
												String past = emotionarrayL4OUT[iwordPosition];
												if (past != null)
													emotionarrayL4OUT[iwordPosition] = past + "#true|false";
												else
													emotionarrayL4OUT[iwordPosition] = "#true|false";
											}
										} else {
											if (neg != null) {
												int negt = allemmotionMap.get("negative");
												allemmotionMap.put("negative", negt + 1);
												String past = emotionarrayL4OUT[iwordPosition];
												if (past != null)
													emotionarrayL4OUT[iwordPosition] = past + "#true|false";
												else
													emotionarrayL4OUT[iwordPosition] = "#true|false";
											}
											if (pos != null) {
												int post = allemmotionMap.get("positive");
												allemmotionMap.put("positive", post + 1);
												String past = emotionarrayL4OUT[iwordPosition];
												if (past != null)
													emotionarrayL4OUT[iwordPosition] = past + "#false|true";
												else
													emotionarrayL4OUT[iwordPosition] = "#false|true";
											}
										}

									} else {
										if (neg != null) {
											int negt = allemmotionMap.get("negative");
											allemmotionMap.put("negative", negt + 1);
										}
										if (pos != null) {
											int post = allemmotionMap.get("positive");
											allemmotionMap.put("positive", post + 1);
										}

									}
								}
							}

							wordlist.add(wordjson);

							try {
								// _в на_
								if (predlogMesta.containsKey(lowerStr)) {
									chislaarrayL9[iwordPosition] = "1";

								}
								// числа
								else if (lowerStr.matches(".*\\d.*")) {
									chislaarrayL9[iwordPosition] = "2";

								}

								// _сколько_
								else if (isPresent("(ЧИСЛ сравн)", morfoarrayL3[iwordPosition])) {
									chislaarrayL9[iwordPosition] = "3";

								}

								else if (isPresent("(ЧИСЛ)", morfoarrayL3[iwordPosition])) {
									chislaarrayL9[iwordPosition] = "4";

								}
								// _КОГДА_ катя завтра вечером
								else if (narechiyaVremeni.containsKey(nwordarrayL2[iwordPosition])) {
									chislaarrayL9[iwordPosition] = "5";

								}

								else if (dniMeseats.containsKey(nwordarrayL2[iwordPosition])) {
									chislaarrayL9[iwordPosition] = "6";

								} else if (narechiyaVremeni.containsKey(tokens[iwordPosition].toLowerCase())) {
									chislaarrayL9[iwordPosition] = "5";

								}
							} catch (Exception e) {
							}
							// ---------------------------------------Объясняет--------------------------------------

							// ----------------------------------------------------------------Хорошо----------------------------------------
							if (ineededu) {
								try {
									String morfposc = tokens[iwordPosition].toLowerCase().trim();
									// if (morfposb.equals("это")){
									if (wordExplainOne.containsKey(morfposc)) {
										String type = wordExplainOne.get(morfposc);

										explainList.add(type + "#" + "1" + "#" + iwordPosition);
									}
									// }
									if (iwordPosition > 0 && iwordPosition <= size) {

										try {
											if (tokens[iwordPosition - 1] != null) {

												String morfposb = tokens[iwordPosition - 1].toLowerCase().trim();
												morfposc = tokens[iwordPosition].toLowerCase().trim();

												try {
													if (isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",
															morfoarrayL3[iwordPosition - 1])) {

														if (isPresent("Г", morfoarrayL3[iwordPosition])) {

															String s = morfposb + " " + morfposc;

															if (verbCount.containsKey(s)) {
																int cont = verbCount.get(s);
																verbCount.put(s, cont + 1);
															} else
																verbCount.put(s, 1);

														}
													}
												} catch (Exception e) {
													e.printStackTrace();
												}
/*
												try {
													if (isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",
															morfoarrayL3[iwordPosition])) {

														if (isPresent("( ПРЕДК )|( Н)|( П )",
																morfoarrayL3[iwordPosition - 1])) {

															morfposb = morfposb.replaceAll("[^А-я\\s-]", "");
															morfposc = morfposc.replaceAll("[^А-я\\s-]", "");

															String s = morfposb + " " + morfposc;

															if (adjectiveCount.containsKey(s)) {
																int cont = adjectiveCount.get(s);
																adjectiveCount.put(s, cont + 1);
															} else
																adjectiveCount.put(s, 1);

														}
													}
												} catch (Exception e) {
													e.printStackTrace();
												}
*/
												// if (morfposa.equals("это")){
												if (wordExplainTwo.containsKey(morfposb + " " + morfposc)) {
													String s = morfposb + " " + morfposc;
													String type = wordExplainTwo.get(s);
													explainList.add(type + "#" + "2" + "#" + iwordPosition);
												}
												// }
												if (iwordPosition - 2 >= 0)
													if (tokens[iwordPosition - 2] != null) {
														String morfposa = tokens[iwordPosition - 2].toLowerCase()
																.trim();

														String s = morfposa + " " + morfposb + " " + morfposc;

														if (wordExplainThree.containsKey(s)) {
															String type = wordExplainThree.get(s);
															explainList.add(type + "#" + "3" + "#" + iwordPosition);
														}
													}

											}

										} catch (Exception e) {
											e.printStackTrace();
										}
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							
							try {
								if(iwordPosition - 1<size&&iwordPosition - 1>=0)
									if (tokens[iwordPosition - 1] != null) {

										String morfposb = tokens[iwordPosition - 1].toLowerCase().trim();
										String morfposc = tokens[iwordPosition].toLowerCase().trim();
								if (isPresent("( С жр,ед,им)|( С мр,ед,им)|( С ср,ед,им)",
										morfoarrayL3[iwordPosition])) {

									if (isPresent("( ПРЕДК )|( Н)|( П )",
											morfoarrayL3[iwordPosition - 1])) {

										morfposb = morfposb.replaceAll("[^А-я\\s-]", "");
										morfposc = morfposc.replaceAll("[^А-я\\s-]", "");

										String s = morfposb + " " + morfposc;

										if (adjectiveCount.containsKey(s)) {
											int cont = adjectiveCount.get(s);
											adjectiveCount.put(s, cont + 1);
										} else
											adjectiveCount.put(s, 1);

									}
								}}
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							// -- --квантор-- --тип
							// Предицирующие связки - 1 «есть» и «не есть» S
							// есть Р
							// (где S - логич. подлежащее, или субъект, Р -
							// логич.
							// сказуемое, или предикат)
							// Пропозициональные связки - 2 «и», «или»,
							// «неверно,
							// что», «если…, то», «если и только если» —
							// отрицание
							// («неверно, что...»), «&» («∧») — конъюнкция
							// («и»),
							// «v» — нестрогая дизъюнкция («или»), «v» — строгая
							// дизъюнкция («либо... либо...»), «⇒» — импликация
							// («если..., то...»), «⇔» — эквиваленция («если и
							// только если»).
							// Квантор общности - 3 «для всех…», «для каждого…»,
							// «для любого…» или «все…», «каждый…», «любой…» «ни
							// один»
							// Квантор существования - 4 «существует»,
							// «некоторые»,
							// «большинство», «какой-нибудь»
							// Квантор количественный - 5 больше меньше равно
							// высоко
							// и низко далеко и близко, снизил,
							// уменьшилась-увеличилась, снижение, поднялась,
							// сократились, продлят,превышать

							// Квантор качественый - 6 <word>умнее</word>
							// <wordform>умный|Y П сравн,од,но,</wordform>
							// <word>выше</word> <wordform> выша|G С
							// жр,ед,дт,выше|j
							// Н,высокий|Y П сравн,од,но,</wordform>

							if (nwordarrayL2[iwordPosition] != null) {
								String eKvantor = wordKvantor.get(nwordarrayL2[iwordPosition]);
								if (eKvantor != null) {
									try {
										kvantorL11[iwordPosition] = eKvantor;
									} catch (Exception e) {
									}
								}
							}

							if (isPresent("(сравн)", morfoarrayL3[iwordPosition])) {
								kvantorL11[iwordPosition] = "6";
							}

							try {
								String a = "";
								String b = "";
								String c = "";

								int ia = iwordPosition - 2;
								int ib = iwordPosition - 1;
								int ic = iwordPosition;

								if (ic >= 0 && ic <= size) {
									if (chislaarrayL9[iwordPosition] != null) {
										c = chislaarrayL9[iwordPosition];
										if (c.equals("5")) {
											timeplaceL10[iwordPosition] = "time";
										}
										if (c.equals("6")) {
											timeplaceL10[iwordPosition] = "time";
										}
									}

									try {
										if (nwordarrayL2[iwordPosition] != null)
											if (metricsTypeOnel.containsKey(nwordarrayL2[iwordPosition])) {
												metricsL12[iwordPosition] = metricsTypeOnel
														.get(nwordarrayL2[iwordPosition]);
												// if(getrezult)
												// System.out.println("datavalue1="+iwordPosition+"
												// "+nwordarrayL2[iwordPosition]+"
												// "+metricsTypeOnel.get(nwordarrayL2[iwordPosition]));

											}
									} catch (Exception e) {
									}

								}

								if (ib >= 0 && ib <= size) {
									try {
										if (nwordarrayL2[iwordPosition - 1] != null
												&& nwordarrayL2[iwordPosition] != null)
											if (metricsTypeTwol.containsKey(nwordarrayL2[iwordPosition - 1] + " "
													+ nwordarrayL2[iwordPosition])) {
												String typeTwo = metricsTypeTwol.get(nwordarrayL2[iwordPosition - 1]
														+ " " + nwordarrayL2[iwordPosition]);
												metricsL12[iwordPosition] = typeTwo;
												metricsL12[iwordPosition - 1] = typeTwo;
											}

									} catch (Exception e) {
									}

									if (chislaarrayL9[iwordPosition - 1] != null) {
										b = chislaarrayL9[iwordPosition - 1];

										if ((b.equals("2") || b.equals("4")) && metricsL12[iwordPosition] != null) {

											metricsL12[iwordPosition - 1] = metricsL12[iwordPosition];

										}

										if (b.equals("2") && c.equals("6")) {
											timeplaceL10[iwordPosition] = "time";
											timeplaceL10[iwordPosition - 1] = "time";
										} else if (b.equals("4") && c.equals("6")) {
											timeplaceL10[iwordPosition] = "time";
											timeplaceL10[iwordPosition - 1] = "time";
										} else if (b.equals("2") && c.equals("5")) {
											timeplaceL10[iwordPosition] = "time";
											timeplaceL10[iwordPosition - 1] = "time";
										} else if (b.equals("4") && c.equals("5")) {
											timeplaceL10[iwordPosition] = "time";
											timeplaceL10[iwordPosition - 1] = "time";
										}
									}
								}

								try {
									if (ia >= 0 && ia <= size) {

										try {
											if (nwordarrayL2[iwordPosition - 2] != null
													&& nwordarrayL2[iwordPosition - 1] != null
													&& nwordarrayL2[iwordPosition] != null)
												if (metricsTypeThreel.containsKey(nwordarrayL2[iwordPosition - 2] + " "
														+ nwordarrayL2[iwordPosition - 1] + " "
														+ nwordarrayL2[iwordPosition])) {
													String typeThree = metricsTypeThreel
															.get(nwordarrayL2[iwordPosition - 2] + " "
																	+ nwordarrayL2[iwordPosition - 1] + " "
																	+ nwordarrayL2[iwordPosition]);
													metricsL12[iwordPosition] = typeThree;
													metricsL12[iwordPosition - 1] = typeThree;
													metricsL12[iwordPosition - 2] = typeThree;
												}

										} catch (Exception e) {
										}

										if (chislaarrayL9[iwordPosition - 2] != null
												&& chislaarrayL9[iwordPosition - 1] != null
												&& chislaarrayL9[iwordPosition] != null) {
											a = chislaarrayL9[iwordPosition - 2];
											try {
												if ((a.equals("2") || a.equals("4"))
														&& metricsL12[iwordPosition - 1] != null) {
													metricsL12[iwordPosition - 2] = metricsL12[iwordPosition - 1];
												}
											} catch (Exception e) {
											}
											if (a.equals("1") && b.equals("2") && c.equals("5")) {
												timeplaceL10[iwordPosition] = "time";
												timeplaceL10[iwordPosition - 1] = "time";
												timeplaceL10[iwordPosition - 2] = "time";
											} else if (a.equals("1") && b.equals("4") && c.equals("5")) {
												timeplaceL10[iwordPosition] = "time";
												timeplaceL10[iwordPosition - 1] = "time";
												timeplaceL10[iwordPosition - 2] = "time";
											} else if (a.equals("5") && b.equals("1") && c.equals("2")) {
												timeplaceL10[iwordPosition] = "time";
												timeplaceL10[iwordPosition - 1] = "time";
												timeplaceL10[iwordPosition - 2] = "time";
											} else if (a.equals("5") && b.equals("1") && c.equals("4")) {
												timeplaceL10[iwordPosition] = "time";
												timeplaceL10[iwordPosition - 1] = "time";
												timeplaceL10[iwordPosition - 2] = "time";
											}
											// в десять раз
											else if (a.equals("1") && (b.equals("2") || b.equals("4"))
													&& razProtsent.containsKey(nwordarrayL2[iwordPosition])) {
												timeplaceL10[iwordPosition] = "kvant";
												timeplaceL10[iwordPosition - 1] = "kvant";
												timeplaceL10[iwordPosition - 2] = "kvant";
											}

										}

									}
								} catch (Exception e) {
								}
								try {
									if (ib >= 0 && ib <= size && b.equals("1")) {
										String objsd = "";
										objsd = objectarrayL6[ic];

										if (objsd != null) {

											String d = idcategorymap.get(objsd);
											String[] newwordsl;
											if (d != null)
												if (d.contains("/")) {

													newwordsl = d.split("\\/");
													String val = newwordsl[1];

													if (val.equals("9")) {
														timeplaceL10[iwordPosition] = "place";
													}
												}

										}
									}
								} catch (Exception e) {
								}

							} catch (Exception e) {
								e.printStackTrace();
							}

						}

					} catch (Exception e) {
						e.printStackTrace();
					}
					// Map<String, List> returtQ = new HashMap<String, List>();

					// проверить если идущее слово или цифры относится к вопросу
					// _сколько_ ЧИСЛ десять, ЧИСЛ сравн
					// проверить если идущее слово относится к вопросу
					// ___КОГДА___? А-формат дат, наречия и месяцы, ЧИСЛ
					// десятое, в десять, на десятое

					// проверить если идущее слово относится к вопросу
					// _ГДЕ_место(предлоги в на)

					// try {
					// if (tokenleght.equals("1")) {}
					// } catch (Exception e) {
					// e.printStackTrace();
					// }

					// -------------------------------обработка
					// фраз----------------------------------------без
					// приведения к начальным формам morfo
					// -------------------------------обработка
					// фраз----------------------------------------без
					// приведения к начальным формам morfo
					// -------------------------------обработка
					// фраз----------------------------------------без
					// приведения к начальным формам morfo
					// -------------------------------обработка
					// фраз----------------------------------------без
					// приведения к начальным формам morfo
					// -------------------------------обработка
					// фраз----------------------------------------без
					// приведения к начальным формам morfo
					// -------------------------------обработка
					// фраз----------------------------------------без
					// приведения к начальным формам morfo

					JSONObject wordphrasejson = new JSONObject();
					// wordphrasejson.put("рosition",
					// ""+wordPosition+"-"+lowerStr);

					// 3 поискать объекты - NB приводятся к нижнему регистру

					// нужно взять фонемы и приветси прил. глаг. и проч. к
					// нормальной форме
					// деревянный = дерево
					// неправомерный = право

					// if (!igetWordExclude.containsKey(lowerStr)) {
					try {
						//
						// начало поиска объектов
						// одиночные слова
						if (tokenleght.equals("1")) {

							// nwordarrayL2 - нормальная форма/приветси
							// прил. глаг. и проч. к существительному
							// поискать синонимы если нет

							ArrayList<String> listnword = new ArrayList<String>();
							String nword = null;
							try {
								nword = nwordarrayL2[iwordPosition];
							} catch (Exception e) {
							}

							if (nword != null) {
								if (nonamesingle.containsKey(nword)) {
									// if(nword.equals("ты")){
									List<String> qindexList = new ArrayList<String>();
									// qindexList.add("Q1346360"); загрузка из
									// ДБ
									qindexList.add(nonamesingle.get(nword));

									Map<String, List> returtQ = new HashMap<String, List>();

									returtQ = searchMainObjectArray(qindexList, iilobjectsList, iilinstanceList,
											iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);
									/*
									 * if(nword.equals("ты")) for (Entry<String,
									 * List> rere : returtQ.entrySet()) {
									 * //System.out.print(""); String
									 * keys=rere.getKey();
									 * //System.out.print("keys="+keys);
									 * 
									 * 
									 * 
									 * List<String> values=rere.getValue();
									 * 
									 * for (String val : values) {
									 * //System.out.print("val="+val); }
									 * 
									 * 
									 * }
									 */

									listobjects.put("" + iwordPosition, returtQ);

								} else {

									if (!igetWordExclude.containsKey(lowerStr)) {
										String morf = morfoarrayL3[iwordPosition];
										boolean issush = analysis.sushRule(morf);
										if (issush) {
											listnword.add(nword);
										} else {
											// если это наречие прилагательное
											// if(isPresent("( ПРЕДК )|( Н)|( П
											// )",morf)) !!!НЕ глагол
											boolean prolognar = analysis.prolognarRule(morf);
											if (prolognar) {

												String m = morfema.get(nword);
												if (m != null) {

													if (m.contains("|")) {
														String[] mdata = m.split("\\|");
														int sizeb = mdata.length;
														for (int i = 0; i < sizeb; i++) {
															String single = mdata[i];

															try {
																String same = morfemawords.get(single);

																if (same != null) {

																	if (same.contains("|")) {
																		String[] msamedata = same.split("\\|");
																		int sizec = msamedata.length;
																		String wordform = "";
																		for (int j = 0; j < sizec; j++) {
																			String singleword = msamedata[j].trim()
																					.toLowerCase();

																			String wrdi = singleword
																					.replaceAll("[^А-я]", "").trim();

																			List<String> wordBaseForms = null;
																			if (!"".equals(wrdi)) {
																				wordBaseForms = luceneMorph
																						.getMorphInfo(wrdi);
																			}
																			int sizer = 0;
																			try {
																				sizer = wordBaseForms.size();
																			} catch (Exception e) {
																			}

																			if (sizer > 0) {

																				StringBuilder sb = new StringBuilder();
																				for (String s : wordBaseForms) {
																					sb.append(s);
																					sb.append(" #");
																				}

																				String ist = sb.toString();
																				if (!ist.equals("") && ist != null) {
																					wordform = ist;
																				}

																				if (wordform.contains("|")) {
																					String[] myData = wordform
																							.split("\\|");
																					String inword = myData[0].trim();
																					String imorf = myData[1];
																					boolean iissush = analysis
																							.sushRule(imorf);
																					if (iissush) {
																						listnword.add(inword);
																					}
																				}
																			}
																		}

																	}
																}

															} catch (Exception e) {
																e.printStackTrace();
															}

														}

													} else {

														try {
															String same = morfemawords.get(m);

															if (same != null) {
																if (same.contains("|")) {
																	String[] msamedata = same.split("\\|");
																	int sizec = msamedata.length;
																	String wordform = "";
																	for (int i = 0; i < sizec; i++) {
																		String singleword = msamedata[i].trim()
																				.toLowerCase();

																		String wrdi = singleword
																				.replaceAll("[^А-я]", "").trim();

																		List<String> wordBaseForms = null;
																		if (!"".equals(wrdi)) {
																			wordBaseForms = luceneMorph
																					.getMorphInfo(wrdi);
																		}

																		int sizer = 0;
																		try {
																			sizer = wordBaseForms.size();
																		} catch (Exception e) {
																		}

																		if (sizer > 0) {

																			StringBuilder sb = new StringBuilder();
																			for (String s : wordBaseForms) {
																				sb.append(s);
																				sb.append(" #");
																			}

																			String ist = sb.toString();
																			if (!ist.equals("") && ist != null) {
																				wordform = ist;
																			}

																			if (wordform.contains("|")) {
																				String[] myData = wordform.split("\\|");
																				String inword = myData[0].trim();
																				String imorf = myData[1];
																				boolean iissush = analysis
																						.sushRule(imorf);
																				if (iissush) {
																					listnword.add(inword);
																				}
																			}
																		}
																	}

																}
															}
														} catch (Exception e) {
															e.printStackTrace();
														}

													}
												}
											}
										}

										String itemtype = "";
										for (String texst : listnword) {

											try {

												List<String> qindexList = iilobjectsList.get(texst);
												// поиск категории
												if (qindexList != null)
													for (String qindex : qindexList) {

														try {
															List<String> list = iilmaincategory.get(qindex);
															for (String c : list) {
																String textcateg = getTextofQ(c, iiltext);
																if (categoryMap.containsKey(textcateg)) {
																	int r = categoryMap.get(textcateg);
																	categoryMap.put(textcateg, r + 1);
																} else {
																	categoryMap.put(textcateg, 1);
																}
															}
														} catch (Exception e) {
														}

													}
												// поиск категории
												String[] res = new String[2];
												Map<String, List> returtQ = new HashMap<String, List>();

												returtQ = searchMainObjectArray(qindexList, iilobjectsList,
														iilinstanceList, iilsubclassofList, iilproperties,
														iilmaincategory, iiltext, iilhaspart);
												if (listobjects.containsKey("" + iwordPosition)) {
													listobjects.put("" + iwordPosition + "|" + tokenleght, returtQ);
												} else {
													listobjects.put("" + iwordPosition, returtQ);
												}

											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
								}
							} else {
								// искать синонимы слова т.к. для него нет
								// морфологии

							}

						} else {
							if (nonamephrase.containsKey(lowerStr)) {
								// if(nword.equals("ты")){
								List<String> qindexList = new ArrayList<String>();
								// qindexList.add("Q1346360"); загрузка из ДБ
								qindexList.add(nonamephrase.get(lowerStr));

								Map<String, List> returtQ = new HashMap<String, List>();

								returtQ = searchMainObjectArray(qindexList, iilobjectsList, iilinstanceList,
										iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);

								listobjects.put("" + iwordPosition, returtQ);

							} else {
								if (ineededu)
									if (!igetWordExclude.containsKey(lowerStr) && !predlogMesta.containsKey(lowerStr)) {
										boolean foundpropType = false;
										List<String> iilQPproperties = null;
										String currentpropType = "";
										iilQPproperties = iilpropertiesList.get(lowerStr);
										if (iilQPproperties != null) {
											for (String ins : iilQPproperties) {

												// P найдена фраза описывающая
												// свойство
												foundpropType = true;
												currentpropType = ins;
												// System.out.println("=>"+ins);
												// sbBuffer.append("P="+ins);
											}
										}
										if (foundpropType) {
											// 1. проверь данные об этом
											// свойстве слева и справа Q
											// 2. вновь найденные свойства не
											// известны добавь свойства объекту
											// 3. если свойства известны
											// пропусти

											explainPropertyList
													.add(currentpropType + "|" + iwordPosition + "|" + tokenleght);
										}

									}

								List<String> qindexList = iilobjectsList.get(lowerStr);

								if (qindexList != null)
									for (String qindex : qindexList) {
										// поиск категории по фразе
										// ДОБАВИТЬ поиск в иерархии объектов по
										// фразе

										try {
											List<String> list = iilmaincategory.get(qindex);
											for (String c : list) {
												String textcateg = getTextofQ(c, iiltext);
												if (categoryMap.containsKey(textcateg)) {
													int r = categoryMap.get(textcateg);
													categoryMap.put(textcateg, r + 1);
												} else {
													categoryMap.put(textcateg, 1);
												}
											}
										} catch (Exception e) {
										}
									}
								// поиск категории по фразе

								// мега тормоза
								Map<String, List> returtQ = new HashMap<String, List>();
								returtQ = searchMainObjectArray(qindexList, iilobjectsList, iilinstanceList,
										iilsubclassofList, iilproperties, iilmaincategory, iiltext, iilhaspart);

								// если вариантов несколько то они
								// перезатираются
								if (listobjects.containsKey("" + iwordPosition)) {
									listobjects.put("" + iwordPosition + "|" + tokenleght, returtQ);
								} else {
									listobjects.put("" + iwordPosition, returtQ);
								}

							}
						}

					} catch (Exception e) {
						e.printStackTrace();

					}
					// }
					// returtQ
					// wordlist.add(wordphrasejson);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			JSONObject responseDetailsJson = new JSONObject();
			JSONArray jsonArray = new JSONArray();

			for (FindObject p : listArray) {

				JSONObject formDetailsJson = new JSONObject();
				formDetailsJson.put("id", p.getId());
				formDetailsJson.put("name", p.getName());
				formDetailsJson.put("pos", p.getStrpos());
				formDetailsJson.put("end", p.getStrend());

				jsonArray.add(formDetailsJson);
			}

			responseDetailsJson.put("medobjects", jsonArray);

			wordlist.add(responseDetailsJson);

			JSONObject responseJson = new JSONObject();
			JSONArray jsonArraym = new JSONArray();

			for (Entry<String, Map<String, List>> entry : listobjects.entrySet()) {
				try {
					String keys = entry.getKey();
					Map<String, List> values = entry.getValue();

					for (Entry<String, List> rt : values.entrySet()) {
						try {

							// выбор первого и последнего значения из списка
							List l = rt.getValue();
							String start = (String) l.get(0);
							String end = (String) l.get((l.size() - 1));

							JSONObject formDetailsJson = new JSONObject();
							formDetailsJson.put("ids", keys);
							formDetailsJson.put("start", start.substring(1));
							formDetailsJson.put("end", end.substring(1));
							jsonArraym.add(formDetailsJson);

						} catch (Exception e) {
						}

					}
				} catch (Exception e) {
				}

			}
			responseJson.put("objects", jsonArraym);
			wordlist.add(responseJson);

			JSONObject responseJsoncategory = new JSONObject();
			JSONArray jsonArraymcategory = new JSONArray();
			try {
				for (Entry<String, Integer> rt : categoryMap.entrySet()) {

					String start = rt.getKey();
					int l = rt.getValue();
					String end = "" + l;

					JSONObject formDetailsJson = new JSONObject();

					formDetailsJson.put("start", start);
					formDetailsJson.put("end", end);
					jsonArraymcategory.add(formDetailsJson);

				}
			} catch (Exception e) {
			}

			responseJsoncategory.put("sc", jsonArraymcategory);
			wordlist.add(responseJsoncategory);

			returnresult.setWordlist(wordlist);
			int ispod = 0;
			int iskaz = 0;
			int iexplain = 0;
			int iexplainprop = 0;

			try {
				ispod = podlejashie.size();
			} catch (Exception e) {
			}
			try {
				iskaz = skazuemie.size();
			} catch (Exception e) {
			}
			try {
				iexplain = explainList.size();
			} catch (Exception e) {
			}
			try {
				iexplainprop = explainPropertyList.size();
			} catch (Exception e) {
			}
			if (iexplainprop > 0)
				for (String sca : explainPropertyList) {
					// explainPropertyList.add(currentpropType+"|"+iwordPosition+"|"+tokenleght);
					try {
						String[] negpos = sca.split("\\|");

						String phrase = negpos[0].trim();
						String piwordPosition = negpos[1].trim();
						String ptokenleght = negpos[2].trim();
						int ipiwordPosition = Integer.parseInt(piwordPosition);
						int iptokenleght = Integer.parseInt(ptokenleght);

						int wordbefore = ipiwordPosition - 1;
						int wordafter = iptokenleght + ipiwordPosition + 1;

						if (wordbefore >= 0 && wordafter < size) {
							String firstphraseverb = "";
							String secondphraseverb = "";
							String threedphraseverb = "";

							firstphraseverb = nwordarrayL2[wordbefore];
							secondphraseverb = phrase;
							threedphraseverb = nwordarrayL2[wordafter];
							if (firstphraseverb != null && threedphraseverb != null)
								wordPhraseCountProperty
										.put(firstphraseverb + "|" + secondphraseverb + "|" + threedphraseverb, 1);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			String[] podarrayL16 = new String[ispod];
			int i = 0;
			for (int pod : podlejashie) {

				podarrayL16[i] = "" + pod;
				i++;
			}

			String[] scaarrayL17 = new String[iskaz];
			i = 0;
			for (int sca : skazuemie) {

				scaarrayL17[i] = "" + sca;
				i++;
			}

			String[] exparrayL18 = new String[iexplain];
			i = 0;
			for (String sca : explainList) {
				exparrayL18[i] = "" + sca;
				i++;
			}

			out.put(0, tokens);
			out.put(1, iilarrayL1);
			out.put(2, nwordarrayL2);
			out.put(3, morfoarrayL3);

			out.put(4, emotionarrayL4);
			out.put(41, emotionarrayL4OUT);

			out.put(5, fonemaarrayL5);
			out.put(6, objectarrayL6);
			out.put(7, familiiarrayL7);
			out.put(8, politeharrayL8);
			out.put(9, chislaarrayL9);
			out.put(10, timeplaceL10);
			out.put(11, kvantorL11);
			out.put(12, metricsL12);
			out.put(15, economicsarrayL15);
			out.put(16, podarrayL16);
			out.put(17, scaarrayL17);
			out.put(18, exparrayL18);

			try {
				
				if (verbCount != null) {
					if (verbCount.size() > 0)
						saveVerb(verbCount);
				}

			} catch (Exception e) {
			}
			try {
				if (ineededu)
				if (adjectiveCount != null) {
					if (adjectiveCount.size() > 0)
						saveAdjective(adjectiveCount);
				}
			} catch (Exception e) {
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		try {
			if (wordPhraseCountProperty != null) {
				if (wordPhraseCountProperty.size() > 0)
					saveVerbProperty(wordPhraseCountProperty);
			}

		} catch (Exception e) {
		}

		// for (Map.Entry<String, Integer> entry : categoryMap.entrySet()) {
		// if (allcategoryMap.containsKey(entry.getKey())) {
		// int r = allcategoryMap.get(entry.getKey());
		// allcategoryMap.put(entry.getKey(), r + entry.getValue());
		// } else {
		// allcategoryMap.put(entry.getKey(), 1);
		// }
		// }
		

		returnresult.setCategoryMap(categoryMap);
		returnresult.setData(out);
		returnresult.setListobjects(listobjects);
		returnresult.setAdjective(adjectiveCount);
		returnresult.setWordindex(predicateswordlist);
		returnresult.setPodlejashie(podlejashie);
		returnresult.setSkazuemie(skazuemie);
		returnresult.setDeeprichastie(deeprichastie);
		return returnresult;
	}

	void saveVerbProperty(Map<String, Integer> wordCount) {
		try {
			Map<String, Integer> probabilitymain = fastMemoryModel.getObjectAndVerbCountProperty();
			for (Map.Entry<String, Integer> entry : wordCount.entrySet()) {
				try {
					String tokens = entry.getKey().trim();
					// System.out.println("===++==="+tokens);
					Integer indx = entry.getValue();
					if (probabilitymain.containsKey(tokens)) {
						int cont = probabilitymain.get(tokens);

						probabilitymain.put(tokens, cont + 1);
					} else
						probabilitymain.put(tokens, 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	void saveVerb(Map<String, Integer> wordCount) {

		try {

			Map<String, Integer> probabilitymain = fastMemoryModel.getObjectAndVerbCount();

			for (Map.Entry<String, Integer> entry : wordCount.entrySet()) {
				try {
					String tokens = entry.getKey().trim();
					Integer indx = entry.getValue();
					if (probabilitymain.containsKey(tokens)) {
						int cont = probabilitymain.get(tokens);
						// probabilitymain.put(tokens, cont+indx);
						probabilitymain.put(tokens, cont + 1);
					} else
						probabilitymain.put(tokens, 1);

				} catch (Exception ex) {
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	void saveAdjective(Map<String, Integer> wordCount) {

		try {
			Map<String, Integer> probabilitymain = fastMemoryModel.getObjectAndAdjectiveCount();

			for (Map.Entry<String, Integer> entry : wordCount.entrySet()) {
				try {
					String tokens = entry.getKey().trim();
					Integer indx = entry.getValue();
					if (probabilitymain.containsKey(tokens)) {
						int cont = probabilitymain.get(tokens);

						probabilitymain.put(tokens, cont + 1);
					} else
						probabilitymain.put(tokens, 1);

				} catch (Exception ex) {
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	// public JSONObject createRestRezult(Map<Integer, String[]> in, String
	// singltext) {
	public JSONObject createRestRezult(ProcessorResult rin, boolean ineededu, boolean getrezult) {
		Map<Integer, String[]> in = rin.getData();
		String singltext = rin.getSingltext();
		JSONArray wordlist = rin.getWordlist();

		Map<String, Integer> adjectiveList = rin.getAdjective();

		Map<String, String> idcategorymap = nameFinderModelMEDAO.getIdcategorymapobj();
		Map<String, Map<String, String>> objectMap = new HashMap<String, Map<String, String>>();
		Map<String, String> iwordPositiveHuman = fastMemoryModel.getWordPositiveHuman();
		Map<String, String> iwordNegativeHuman = fastMemoryModel.getWordNegativeHuman();

		Map<String, String> negpredlog = fastMemoryModel.getWordNegPreglog();
		Map<String, Integer> inounverbdb = fastMemoryModel.getNounverbdb();
		Map<String, Integer> ilemmanounverbdb = fastMemoryModel.getLemmanounverbdb();
		Map<String, Integer> triplet = fastMemoryModel.getNounverbdbTriplet();

		Map<String, String> explainMap = fastMemoryModel.getExplainPrimeri();
		Map<String, String> predlogMesta = fastMemoryModel.getWordPredlogMesta();
		Map<String, Integer> wordPhraseCount = fastMemoryModel.getObjectAndVerbCountTriplet();
		Map<String, Integer> wordPhraseCountPredlod = fastMemoryModel.getObjectAndVerbCountTripletPredlog();
		JSONObject srtjson = new JSONObject();
		String[] tokens = null;
		try {
			tokens = in.get(0);
		} catch (Exception e) {
		}
		if (tokens != null) {
			int size = tokens.length;
			srtjson.put("words", "" + size);
			srtjson.put("wordlist", wordlist);
			String[] iilarrayL1 = new String[size];
			String[] nwordarrayL2 = new String[size];
			String[] morfoarrayL3 = new String[size];
			String[] emotionarrayL4 = new String[size];
			String[] emotionarrayL4OUT = new String[size];
			String[] fonemaarrayL5 = new String[size];
			String[] objectarrayL6 = new String[size];
			String[] familiiarrayL7 = new String[size];
			String[] politeharrayL8 = new String[size];
			String[] chislaarrayL9 = new String[size];
			String[] timeplaceL10 = new String[size];
			String[] kvantorL11 = new String[size];
			String[] metricsL12 = new String[size];
			String[] economicsarrayL15 = new String[size];
			String[] podarrayL16;
			String[] scaarrayL17;
			String[] exparrayL18;

			iilarrayL1 = in.get(1);
			nwordarrayL2 = in.get(2);
			morfoarrayL3 = in.get(3);
			emotionarrayL4 = in.get(4);
			emotionarrayL4OUT = in.get(41);
			fonemaarrayL5 = in.get(5);
			objectarrayL6 = in.get(6);
			familiiarrayL7 = in.get(7);
			politeharrayL8 = in.get(8);
			chislaarrayL9 = in.get(9);
			timeplaceL10 = in.get(10);
			kvantorL11 = in.get(11);
			metricsL12 = in.get(12);
			economicsarrayL15 = in.get(15);
			podarrayL16 = in.get(16);
			scaarrayL17 = in.get(17);
			exparrayL18 = in.get(18);

			int podsize = podarrayL16.length;
			int scasize = scaarrayL17.length;
			int iexplain = exparrayL18.length;

			Integer[] scaarray = new Integer[scasize];

			// расчет появления подлежащего и сказуемого по глагольному ряду в
			// этом предолжении

			// создаем массив возможных пар

			JSONArray core = new JSONArray();

			ArrayList<String> objs = new ArrayList<String>();
			Map<String, Integer[]> resmap = new HashMap<String, Integer[]>();

			for (int i = 0; i < podsize; i++) {
				// for(Integer pod:podlejashie){//pod=0 pod=7
				int pod = Integer.parseInt(podarrayL16[i]);

				for (int j = 0; j < scasize; j++) {
					// for(Integer skaz:skazuemie){//skaz=2 skaz=6
					int skaz = Integer.parseInt(scaarrayL17[j]);

					// 0-2 0-6
					// 7-6 7-6
					Integer[] inf = new Integer[3];
					inf[0] = pod;
					inf[1] = skaz;

					String podlej = tokens[pod].toLowerCase().trim();
					String skazuem = tokens[skaz].toLowerCase().trim();
					// может мочь быть

					// будет, был не добавлен
					// отрицание
					int ihasNo = 0;

					try {
						if (negpredlog.containsKey(nwordarrayL2[skaz - 1]))
							ihasNo = 1;
					} catch (Exception e) {
					}

					if (nwordarrayL2[skaz].equals("может") || nwordarrayL2[skaz].equals("мочь")
							|| nwordarrayL2[skaz].equals("быть") || nwordarrayL2[skaz].equals("хотеть")
							|| nwordarrayL2[skaz].equals("хочу")) {
						try {
							if (negpredlog.containsKey(nwordarrayL2[skaz - 1]))
								ihasNo = 1;
						} catch (Exception e) {
						}

						if (skaz >= 0 && skaz <= size) {
							boolean ex = false;
							// --->
							int lim = 3;
							for (int s = skaz + 1; s < skaz + lim; s++) {
								if (s < 0)
									break;
								if (s >= size)
									break;
								// System.out.println("===================="+morfoarrayL3[s]);
								// System.out.println("===================="+nwordarrayL2[s]);

								if (isPresent("Г", morfoarrayL3[s]) || isPresent("ИНФИНИТИВ", morfoarrayL3[s])) {
									if (nwordarrayL2[s].equals("может") || nwordarrayL2[s].equals("мочь")
											|| nwordarrayL2[s].equals("быть") || nwordarrayL2[skaz].equals("хотеть")
											|| nwordarrayL2[skaz].equals("хочу")) {
										try {
											if (negpredlog.containsKey(nwordarrayL2[s - 1]))
												ihasNo = 1;
										} catch (Exception e) {
										}
									} else {

										try {
											if (negpredlog.containsKey(nwordarrayL2[s - 1]))
												ihasNo = 1;
										} catch (Exception e) {
										}
										skaz = s;
										ex = true;
										break;
									}
								}
							}

							// <---
							if (!ex)
								for (int s = skaz - 1; s > skaz - lim; s--) {
									if (s < 0)
										break;
									if (s >= size)
										break;
									if (isPresent("Г", morfoarrayL3[s]) || isPresent("ИНФИНИТИВ", morfoarrayL3[s])) {
										if (nwordarrayL2[s].equals("может") || nwordarrayL2[s].equals("мочь")
												|| nwordarrayL2[s].equals("быть") || nwordarrayL2[skaz].equals("хотеть")
												|| nwordarrayL2[skaz].equals("хочу")) {
											try {
												if (negpredlog.containsKey(nwordarrayL2[s - 1]))
													ihasNo = 1;
											} catch (Exception e) {
											}
										} else {

											try {
												if (negpredlog.containsKey(nwordarrayL2[s - 1]))
													ihasNo = 1;
											} catch (Exception e) {
											}
											skaz = s;
											ex = true;
											break;
										}
									}

								}

						}
					}
					String key = "" + pod + "" + skaz;
					inf[0] = pod;
					inf[1] = skaz;
					inf[2] = ihasNo;
					resmap.put(key, inf);
				}

			}

			// найти ближайшее Б для анализа отношений и свойств
			// maincorelemma maincore

			Map<Integer[], Integer> unsortMapI = new HashMap<Integer[], Integer>();
			Map<Integer[], Integer> unsortMaplemmaI = new HashMap<Integer[], Integer>();
			Map<String, Integer> unsortMapIS = new HashMap<String, Integer>();
			Map<String, Integer> unsortMaplemmaIS = new HashMap<String, Integer>();

			for (Map.Entry<String, Integer[]> entry : resmap.entrySet()) {
				Integer[] inf = new Integer[3];

				inf = entry.getValue();

				int pod = inf[0];
				int skaz = inf[1];
				int ihasNo = inf[2];
				String wordkey = null;
				String lemmakey = null;
				int totilemma = -2;
				int toti = -1;

				String podlej = tokens[pod].toLowerCase().trim();
				String skazuem = tokens[skaz].toLowerCase().trim();

				try {
					wordkey = podlej + " " + skazuem;
				} catch (Exception e) {
				}

				try {
					lemmakey = nwordarrayL2[pod] + " " + nwordarrayL2[skaz];
				} catch (Exception e) {
				}

				try {
					if (wordkey != null)
						toti = inounverbdb.get(wordkey);
				} catch (Exception e) {
				}
				try {
					if (lemmakey != null)
						totilemma = ilemmanounverbdb.get(lemmakey);
				} catch (Exception e) {
				}
				unsortMapI.put(inf, toti);
				unsortMaplemmaI.put(inf, totilemma);

				unsortMapIS.put(pod + "" + skaz, toti);
				unsortMaplemmaIS.put(pod + "" + skaz, totilemma);
			}

			Map<Integer[], Integer> sortedd = sortByValueI(unsortMapI);
			Map<Integer[], Integer> sorteddlemmaI = sortByValueI(unsortMaplemmaI);

			// --------------------------Получены пары значений, они
			// отсортированы Integer их величина-------------------
			// --------------------------Обработка правил выбора
			// ------------------------------------
			Map<Integer[], Integer> sorted = getPodSkazRules(sortedd, unsortMapIS, sorteddlemmaI, unsortMaplemmaIS,
					getrezult, scaarrayL17, nwordarrayL2);
			// Map<Integer[], Integer> sorted =
			// getPodSkazRules(sorteddlemmaI,sortedd, getrezult, scaarrayL17);
			int podexplain = 0;
			int skazexplain = 0;

			int lk = 1;
			for (Map.Entry<Integer[], Integer> entry : sorted.entrySet()) {
				Integer[] inf = new Integer[3];

				inf = entry.getKey();

				int pod = inf[0];
				int skaz = inf[1];
				int ihasNo = inf[2];

				// A
				podexplain = pod;
				// V
				skazexplain = skaz;
				// B
				// if (isPresent("((С (жр|мр|ср))|(П (жр|мр|ср))",
				// morfoarrayL3[p]))
				lk++;
				if (lk > 1)
					break;

			}

			// ====================================================EDU=-============================================

			if (ineededu) {
				int g = skazexplain + 1;
				if (g < size) {
					if (morfoarrayL3[skazexplain + 1] != null)
						if (isPresent("С (жр|мр|ср),(ед|мн)", morfoarrayL3[skazexplain + 1])) {

							String firstphraseverb = "";
							String secondphraseverb = "";
							String threedphraseverb = "";

							firstphraseverb = nwordarrayL2[podexplain];
							secondphraseverb = nwordarrayL2[skazexplain];
							threedphraseverb = nwordarrayL2[skazexplain + 1];

							if (firstphraseverb != null && threedphraseverb != null) {
								if (!firstphraseverb.equals(threedphraseverb)) {
									if (wordPhraseCount.containsKey(
											firstphraseverb + " " + secondphraseverb + " " + threedphraseverb)) {
										int cont = wordPhraseCount
												.get(firstphraseverb + " " + secondphraseverb + " " + threedphraseverb);
										wordPhraseCount.put(
												firstphraseverb + " " + secondphraseverb + " " + threedphraseverb,
												cont + 1);
									} else
										wordPhraseCount.put(
												firstphraseverb + " " + secondphraseverb + " " + threedphraseverb, 1);
								}
							}
						}

					if (tokens[skazexplain + 1] != null)
						if (predlogMesta.containsKey(tokens[skazexplain + 1])) {
							if (skazexplain + 2 <= size) {
								String morf = "";
								try {
									morf = morfoarrayL3[skazexplain + 2];
								} catch (Exception e) {
									morf = null;
								}
								if (morf != null) {

									if (isPresent("С (жр|мр|ср),(ед|мн)", morfoarrayL3[skazexplain + 2])) {

										String firstphraseverb = "";
										String secondphraseverb = "";
										String threedphraseverb = "";

										firstphraseverb = nwordarrayL2[podexplain];
										secondphraseverb = nwordarrayL2[skazexplain];
										threedphraseverb = nwordarrayL2[skazexplain + 2];

										if (firstphraseverb != null && threedphraseverb != null)
											if (!firstphraseverb.equals(threedphraseverb)) {
												if (wordPhraseCountPredlod.containsKey(firstphraseverb + " "
														+ secondphraseverb + " " + threedphraseverb)) {
													int cont = wordPhraseCountPredlod.get(firstphraseverb + " "
															+ secondphraseverb + " " + threedphraseverb);
													wordPhraseCountPredlod.put(firstphraseverb + " " + secondphraseverb
															+ " " + threedphraseverb, cont + 1);
												} else
													wordPhraseCountPredlod.put(firstphraseverb + " " + secondphraseverb
															+ " " + threedphraseverb, 1);
											}
									}

								}
							}
						}

				}
			}

			for (int x = 0; x < iexplain; x++) {

				String explainData = exparrayL18[x];

				try {

					if (explainData != null) {
						if (explainData.contains("#")) {
							String[] myData = explainData.split("#");

							String type = myData[0].trim();
							String oneormorewords = myData[1];
							String posend = myData[2];
							// это - было выше
							// существительные
							// глаголы
							// и прилагательные и сказуемые

							try {
								if (type.equals("1") || type.equals("4")) {
									// запрещается
									String nounname = "";
									String verbname = "";
									String nounnamet = "";
									String exptext = singltext;

									int iwordPosition = Integer.parseInt(posend);

									String exptype = type;
									if (podexplain > 0) {
										nounname = nwordarrayL2[podexplain];
									}
									if (skazexplain > 0) {
										try {
											verbname = nwordarrayL2[skazexplain];
											if (skazexplain > podexplain)
												if (size >= skazexplain + 2)
													if (isPresent("ИНФИНИТИВ дст", morfoarrayL3[skazexplain + 1])) {
														verbname = nwordarrayL2[skazexplain + 1];
														if (size >= skazexplain + 2)
															if (isPresent("(С (жр|мр|ср))|( П (жр|мр|ср))",
																	morfoarrayL3[skazexplain + 2])) {
																nounnamet = nwordarrayL2[skazexplain + 2];
															}
													}
										} catch (Exception e) {
										}

									}
									nounname = nounname.replaceAll("[^А-я]", "").trim();
									verbname = verbname.replaceAll("[^А-я]", "").trim();
									nounnamet = nounnamet.replaceAll("[^А-я]", "").trim();
									String expdatat = nounname + "#" + verbname + "#" + nounnamet + "#" + exptype + "#"
											+ UUID.randomUUID();

									if (!nounname.equals("") && !verbname.equals(""))
										explainMap.put(expdatat, exptype + "#" + exptext);

									/*
									 * else { //Нужны будут новые формы глагола
									 * = новый массив L20 - нельзя бегать голым
									 * - <wordform>бегать|a ИНФИНИТИВ
									 * дст,</wordform> бегать как?
									 * 
									 * 
									 * if(size>=iwordPosition+2) if
									 * (isPresent("ИНФИНИТИВ дст",
									 * morfoarrayL3[iwordPosition+1])){
									 * verbname=nwordarrayL2[iwordPosition+1];
									 * if(size>=iwordPosition+2){ if
									 * (isPresent("(С (жр|мр|ср))|( П (жр|мр|ср))"
									 * , morfoarrayL3[iwordPosition+2])){
									 * 
									 * nounnamet=nwordarrayL2[iwordPosition+2];
									 * }} }
									 * 
									 * }
									 */
									if (size >= iwordPosition + 2)
										if (isPresent("ИНФИНИТИВ дст", morfoarrayL3[iwordPosition + 1])) {
											verbname = nwordarrayL2[iwordPosition + 1];
											if (size >= iwordPosition + 2) {
												String morf = "";
												try {
													morf = morfoarrayL3[iwordPosition + 2];
												} catch (Exception e) {
												}
												if (isPresent("(С (жр|мр|ср))|( П (жр|мр|ср))", morf)) {
													nounnamet = nwordarrayL2[iwordPosition + 2];
												}
											}
										}
									nounname = nounname.replaceAll("[^А-я]", "").trim();
									verbname = verbname.replaceAll("[^А-я]", "").trim();
									nounnamet = nounnamet.replaceAll("[^А-я]", "").trim();
									expdatat = nounname + "#" + verbname + "#" + nounnamet + "#" + exptype + "#"
											+ UUID.randomUUID();

									if (!nounnamet.equals(""))
										explainMap.put(expdatat, exptype + "#" + exptext);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								if (type.equals("2")) {
									// хорошо

									int iwordPosition = Integer.parseInt(posend);
									int beforeEto = iwordPosition - 2;
									// int beforeEto=iwordPosition-2;
									// if
									// (nwordarrayL2[iwordPosition-1].equals("это"))
									// {
									//
									if (beforeEto >= 0) {
										String word = tokens[beforeEto];
										explainMap.put(word, "2#2");
									}
									// }

								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								if (type.equals("3")) {
									// плохо
									int iwordPosition = Integer.parseInt(posend);
									int beforeEto = iwordPosition - 2;
									// int beforeEto=iwordPosition-2;
									// if
									// (nwordarrayL2[iwordPosition-1].equals("это"))
									// {
									if (beforeEto >= 0) {
										String word = tokens[beforeEto];
										explainMap.put(word, "3#3");
									}
									// }

								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							// else if(type.equals("4")){
							// //разрешается
							// int iwordPosition =Integer.parseInt(posend);
							// int beforeEto=iwordPosition-2;
							// String word=tokens[beforeEto];
							// explainMap.put(word, "4");
							// }
							try {
								if (type.equals("5")) {
									// потому что
									String nounname = "";
									String verbname = "";
									String nounnamet = "";
									String exptext = singltext;
									String exptype = type;

									int iwordPosition = Integer.parseInt(posend);
									int beforeEto = iwordPosition - 1;
									if (beforeEto >= 0)// {
										nounname = tokens[beforeEto];
									nounname = nounname.replaceAll("[^А-я]", "").trim();
									verbname = verbname.replaceAll("[^А-я]", "").trim();
									nounnamet = nounnamet.replaceAll("[^А-я]", "").trim();
									String expdatat = nounname + "#" + verbname + "#" + nounnamet + "#" + exptype + "#"
											+ UUID.randomUUID();
									explainMap.put(expdatat, "5" + "#" + exptext);
									// }
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// ====================================================EDU===END========================================================

			int j = 1;

			JSONArray maincorearray = new JSONArray();
			for (Map.Entry<Integer[], Integer> entry : sorted.entrySet()) {
				JSONObject inobj = new JSONObject();
				JSONObject itempod = new JSONObject();
				JSONObject itemscaz = new JSONObject();

				int tonpos = 0;
				int tonneg = 0;

				int tonposscaz = 0;
				int tonnegscaz = 0;

				int negbefore = 0;
				int posbefore = 0;

				int negmiddle = 0;
				int posmiddle = 0;

				int negafter = 0;
				int posafter = 0;

				// {"type":"1","word":"Москву","strpos":"15","strend":"21",
				// pos:"true", neg:"true"}

				Integer[] inf = new Integer[3];

				inf = entry.getKey();

				int pod = inf[0];
				int skaz = inf[1];
				int ihasNo = inf[2];
				String spod = tokens[pod].trim();
				int pbegin = 0;
				int pend = 0;
				List<IndexWrapper> list = new ArrayList<>();
				list = findIndexesForKeyword(singltext, spod);
				try {
					if (list.size() > 0) {

						pbegin = list.get(0).getStart();
						pend = spod.length() + pbegin;

					}
				} catch (Exception e) {
				}

				String sskaz = tokens[skaz].trim();

				int sbegin = 0;
				int send = 0;
				List<IndexWrapper> lists = new ArrayList<>();
				lists = findIndexesForKeyword(singltext, sskaz);
				try {
					if (list.size() > 0) {

						sbegin = lists.get(0).getStart();
						send = spod.length() + sbegin;

					}
				} catch (Exception e) {
				}

				int[] ipod = getNegPos(emotionarrayL4OUT[inf[0]]);
				itempod.put("type", 1);
				itempod.put("podid", inf[0]);
				itempod.put("word", spod);
				itempod.put("start", pbegin);
				itempod.put("end", pend);
				try {
					String objsd = "";
					objsd = objectarrayL6[pod];
					if (objsd != null) {
						itempod.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
					}
				} catch (Exception e) {
				}

				String prdf = "";

				try {
					prdf = nwordarrayL2[pod - 1];
				} catch (Exception e) {
				}

				try {
					if (iwordNegativeHuman.containsKey(nwordarrayL2[pod])) {
						if (negpredlog.containsKey(prdf)) {
							itempod.put("tonpos", "" + 1);
							tonpos++;
						} else {
							itempod.put("tonneg", "" + 1);
							tonneg++;
						}
					} else {
						if (ipod[0] > 0) {
							itempod.put("tonneg", "" + ipod[0]);
							tonneg++;
						}
					}
				} catch (Exception e) {
				}
				try {
					if (iwordPositiveHuman.containsKey(nwordarrayL2[pod])) {
						if (negpredlog.containsKey(prdf)) {
							itempod.put("tonneg", "" + 1);
							tonneg++;
						} else {
							itempod.put("tonpos", "" + 1);
							tonpos++;
						}
					} else {
						if (ipod[1] > 0) {
							itempod.put("tonpos", "" + ipod[1]);
							tonpos++;
						}
					}
				} catch (Exception e) {
				}

				try {
					String timeplace = "";

					timeplace = timeplaceL10[pod];

					if (timeplace != null)
						itempod.put("timeplace", "" + timeplace);

				} catch (Exception e) {
				}

				try {
					String v = "" + pod;
					Map<String, String> returtQ = objectMap.get(v);
					if (returtQ != null) {
						StringBuffer sb = new StringBuffer();

						for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
							sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

						}

						itempod.put("qdata", sb.toString());
					}

				} catch (Exception e) {
				}

				prdf = "";

				try {
					prdf = nwordarrayL2[skaz - 1];
				} catch (Exception e) {
				}
				int[] iskaz = getNegPos(emotionarrayL4OUT[inf[1]]);

				itemscaz.put("type", 2);
				itemscaz.put("skazid", inf[1]);
				itemscaz.put("word", sskaz);
				itemscaz.put("start", sbegin);
				itemscaz.put("end", send);
				itemscaz.put("ihasno", ihasNo);

				try {
					String objsd = "";
					objsd = objectarrayL6[skaz];
					if (objsd != null) {
						itemscaz.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
					}
				} catch (Exception e) {
				}

				try {
					if (iwordNegativeHuman.containsKey(nwordarrayL2[skaz])) {
						if (ihasNo == 1) {
							itemscaz.put("tonpos", "" + 1);
							tonposscaz++;
						} else {
							itemscaz.put("tonneg", "" + 1);
							tonnegscaz++;
						}
					} else {
						if (iskaz[0] > 0) {
							itemscaz.put("tonneg", "" + iskaz[0]);
							tonnegscaz++;
						}
					}
				} catch (Exception e) {
				}
				try {
					if (iwordPositiveHuman.containsKey(nwordarrayL2[skaz])) {
						if (ihasNo == 1) {
							itemscaz.put("tonneg", "" + 1);
							tonnegscaz++;
						} else {
							itemscaz.put("tonpos", "" + 1);
							tonposscaz++;
						}
					} else {
						if (iskaz[1] > 0) {
							itemscaz.put("tonpos", "" + iskaz[1]);
							tonposscaz++;
						}
					}
				} catch (Exception e) {
				}
				try {

					String timeplace = "";
					timeplace = timeplaceL10[skaz];
					if (timeplace != null)
						itemscaz.put("timeplace", "" + timeplace);
				} catch (Exception e) {
				}

				try {
					String v = "" + skaz;
					Map<String, String> returtQ = objectMap.get(v);
					if (returtQ != null) {
						StringBuffer sb = new StringBuffer();

						for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
							sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

						}

						itemscaz.put("qdata", sb.toString());
					}

				} catch (Exception e) {
				}

				String before = "";
				String middle = "";
				String after = "";
				JSONArray corebefore = new JSONArray();
				JSONArray coremiddle = new JSONArray();
				JSONArray coreafter = new JSONArray();

				try {
					int lim = 10;

					// 1 < 9 сказуемое дальше
					if (pod < skaz) {

						/*
						 * if(ineededu){ int g=skaz+1; if(g<size){
						 * if(morfoarrayL3[skaz+1]!=null)
						 * if(isPresent("С (жр|мр|ср),(ед|мн)",
						 * morfoarrayL3[skaz+1])){
						 * 
						 * String firstphraseverb=""; String
						 * secondphraseverb=""; String threedphraseverb="";
						 * 
						 * firstphraseverb=nwordarrayL2[pod];
						 * secondphraseverb=nwordarrayL2[skaz];
						 * threedphraseverb=nwordarrayL2[skaz+1];
						 * if(!firstphraseverb.equals(threedphraseverb)){
						 * if(wordPhraseCount.containsKey(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb)){ int
						 * cont=wordPhraseCount.get(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb);
						 * wordPhraseCount.put(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb, cont+1);
						 * }else wordPhraseCount.put(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb, 1); } }
						 * 
						 * 
						 * if(tokens[skaz+1]!=null)
						 * if(predlogMesta.containsKey(tokens[skaz+1])){
						 * 
						 * if(morfoarrayL3[skaz+2]!=null){
						 * 
						 * if(isPresent("С (жр|мр|ср),(ед|мн)",
						 * morfoarrayL3[skaz+2])){
						 * 
						 * String firstphraseverb=""; String
						 * secondphraseverb=""; String threedphraseverb="";
						 * 
						 * firstphraseverb=nwordarrayL2[pod];
						 * secondphraseverb=nwordarrayL2[skaz];
						 * threedphraseverb=nwordarrayL2[skaz+2];
						 * if(!firstphraseverb.equals(threedphraseverb)){
						 * if(wordPhraseCountPredlod.containsKey(
						 * firstphraseverb+" "+secondphraseverb+" "
						 * +threedphraseverb)){ int
						 * cont=wordPhraseCountPredlod.get(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb);
						 * wordPhraseCountPredlod.put(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb, cont+1);
						 * }else wordPhraseCountPredlod.put(firstphraseverb+" "
						 * +secondphraseverb+" "+threedphraseverb, 1); } }
						 * 
						 * } }
						 * 
						 * } }
						 * 
						 */
						// 1-1 1-1>1-10(-9) 0
						inobj.put("vec", "direct");
						for (int p = pod - 1; p > pod - lim; p--) {
							if (p < 0)
								break;
							if (p >= size)
								break;
							String l = tokens[p];
							// <----

							// проверить если идущее слово или цифры относится к
							// вопросу _сколько_ ЧИСЛ десять, ЧИСЛ сравн
							// проверить если идущее слово относится к вопросу
							// ___КОГДА___? А-формат дат, наречия и месяцы, ЧИСЛ
							// десятое, в десять, на десятое

							// проверить если идущее слово относится к вопросу
							// _ГДЕ_место(предлоги в на)
							JSONObject beforeforarray = new JSONObject();

							try {

								String typevalue = "";
								typevalue = metricsL12[p];
								if (typevalue != null) {
									beforeforarray.put("typevalue", "" + typevalue);
								}

								String timeplace = "";
								timeplace = timeplaceL10[p];
								if (timeplace != null) {

									beforeforarray.put("timeplace", "" + timeplace);

								}
								String politeh = "";
								politeh = politeharrayL8[p];
								if (politeh != null) {
									beforeforarray.put("politeh", "" + politeh);
								}

								String economy = "";
								economy = economicsarrayL15[p];
								if (economy != null) {
									beforeforarray.put("economy", "" + economy);
								}

								String kvant = "";
								kvant = kvantorL11[p];
								if (kvant != null) {
									beforeforarray.put("kvant", "" + kvant);
								}

								String objsd = "";
								objsd = objectarrayL6[p];

								if (objsd != null) {
									beforeforarray.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
								}

							} catch (Exception e) {
							}

							int wordtype = 0;

							if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[p])) {
								before = before + " Б[" + l + "]";

								int[] is = getNegPos(emotionarrayL4OUT[p]);

								beforeforarray.put("nmb", p);
								beforeforarray.put("type", 1);
								beforeforarray.put("word", l);

								prdf = "";
								try {
									prdf = nwordarrayL2[p - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										} else {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										}
									} else {
										if (is[0] > 0) {
											beforeforarray.put("tonneg", "" + is[0]);
											negbefore++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										} else {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										}
									} else {
										if (is[1] > 0) {
											beforeforarray.put("tonpos", "" + is[1]);
											posbefore++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[p].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									beforeforarray.put("start", beginpos);
									beforeforarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + p;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										beforeforarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}

							} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[p])) {
								before = before + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[p]);
								beforeforarray.put("nmb", p);
								beforeforarray.put("type", 0);
								beforeforarray.put("word", l);
								beforeforarray.put("tonneg", "" + is[0]);
								beforeforarray.put("tonpos", "" + is[1]);

								prdf = "";
								try {
									prdf = nwordarrayL2[p - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										} else {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										}
									} else {
										if (is[0] > 0) {
											beforeforarray.put("tonneg", "" + is[0]);
											negbefore++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										} else {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										}
									} else {
										if (is[1] > 0) {
											beforeforarray.put("tonpos", "" + is[1]);
											posbefore++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[p].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									beforeforarray.put("start", beginpos);
									beforeforarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {

									String v = "" + p;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										beforeforarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							}
							corebefore.add(beforeforarray);
						}

						// посередине, между подлижащим и сказуемым нет данных
						// --->
						for (int s = pod + 1; s < skaz; s++) {
							if (s < 0)
								break;
							if (s >= size)
								break;
							String l = tokens[s];
							JSONObject middlearray = new JSONObject();
							try {
								String typevalue = "";
								typevalue = metricsL12[s];
								if (typevalue != null) {
									middlearray.put("typevalue", "" + typevalue);
								}

								String timeplace = "";
								timeplace = timeplaceL10[s];
								if (timeplace != null) {

									middlearray.put("timeplace", "" + timeplace);

								}
								String politeh = "";
								politeh = politeharrayL8[s];
								if (politeh != null) {
									middlearray.put("politeh", "" + politeh);
								}

								String economy = "";
								economy = economicsarrayL15[s];
								if (economy != null) {
									middlearray.put("economy", "" + economy);
								}

								String kvant = "";
								kvant = kvantorL11[s];
								if (kvant != null) {
									middlearray.put("kvant", "" + kvant);
								}

								String objsd = "";
								objsd = objectarrayL6[s];
								if (objsd != null) {
									middlearray.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
								}

							} catch (Exception e) {
							}

							if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
								middle = middle + " Б[" + l + "]";
								int[] is = getNegPos(emotionarrayL4OUT[s]);

								middlearray.put("nmb", s);
								middlearray.put("type", 1);
								middlearray.put("word", l);
								middlearray.put("tonneg", "" + is[0]);
								middlearray.put("tonpos", "" + is[1]);

								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}
								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										} else {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										}
									} else {
										if (is[0] > 0) {
											middlearray.put("tonneg", "" + is[0]);
											negmiddle++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										} else {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										}
									} else {
										if (is[1] > 0) {
											middlearray.put("tonpos", "" + is[1]);
											posmiddle++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									middlearray.put("start", beginpos);
									middlearray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										middlearray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
								middle = middle + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[s]);
								middlearray.put("nmb", s);
								middlearray.put("type", 0);
								middlearray.put("word", l);
								middlearray.put("tonneg", "" + is[0]);
								middlearray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										} else {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										}
									} else {
										if (is[0] > 0) {
											middlearray.put("tonneg", "" + is[0]);
											negmiddle++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										} else {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										}
									} else {
										if (is[1] > 0) {
											middlearray.put("tonpos", "" + is[1]);
											posmiddle++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									middlearray.put("start", beginpos);
									middlearray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										middlearray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							}

							coremiddle.add(middlearray);
						}

						int calcvalue = 0;

						// 9 = 9+1 9+1<9+10

						for (int s = skaz + 1; s < skaz + lim; s++) {
							if (s < 0)
								break;
							if (s >= size)
								break;
							String l = tokens[s];
							JSONObject afterarray = new JSONObject();
							if (nwordarrayL2[s] != null)
								if (triplet.containsKey(
										nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " " + nwordarrayL2[s])) {
									int curval = triplet.get(
											nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " " + nwordarrayL2[s]);
									if (calcvalue < curval) {
										calcvalue = curval;
										afterarray.put("triplet", nwordarrayL2[s]);
									}
								}

							try {
								String typevalue = "";
								typevalue = metricsL12[s];
								if (typevalue != null) {
									afterarray.put("typevalue", "" + typevalue);
								}

								String timeplace = "";
								timeplace = timeplaceL10[s];
								if (timeplace != null) {

									afterarray.put("timeplace", "" + timeplace);

								}
								String politeh = "";
								politeh = politeharrayL8[s];
								if (politeh != null) {
									afterarray.put("politeh", "" + politeh);
								}
								String economy = "";
								economy = economicsarrayL15[s];
								if (economy != null) {
									afterarray.put("economy", "" + economy);
								}

								String kvant = "";
								kvant = kvantorL11[s];
								if (kvant != null) {
									afterarray.put("kvant", "" + kvant);
								}

								String objsd = "";
								objsd = objectarrayL6[s];
								if (objsd != null) {
									afterarray.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
								}

							} catch (Exception e) {
							}
							if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
								after = after + " Б[" + l + "]";

								int[] is = getNegPos(emotionarrayL4OUT[s]);

								afterarray.put("nmb", s);
								afterarray.put("type", 1);
								afterarray.put("word", l);
								afterarray.put("tonneg", "" + is[0]);
								afterarray.put("tonpos", "" + is[1]);

								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										} else {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										}
									} else {
										if (is[0] > 0) {
											afterarray.put("tonneg", "" + is[0]);
											negafter++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										} else {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										}
									} else {
										if (is[1] > 0) {
											afterarray.put("tonpos", "" + is[1]);
											posafter++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									afterarray.put("start", beginpos);
									afterarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										afterarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
								after = after + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[s]);
								afterarray.put("nmb", s);
								afterarray.put("type", 0);
								afterarray.put("word", l);
								afterarray.put("tonneg", "" + is[0]);
								afterarray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										} else {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										}
									} else {
										if (is[0] > 0) {
											afterarray.put("tonneg", "" + is[0]);
											negafter++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										} else {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										}
									} else {
										if (is[1] > 0) {
											afterarray.put("tonpos", "" + is[1]);
											posafter++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									afterarray.put("start", beginpos);
									afterarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										afterarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}

							}
							coreafter.add(afterarray);
						}
						String wordkey = "";
					}

					// 7 6
					int calcvalue = 0;

					if (pod > skaz) {
						inobj.put("vec", "indirect");
						// <-----
						for (int s = skaz - 1; s > skaz - lim; s--) {
							if (s < 0)
								break;
							if (s >= size)
								break;
							String l = tokens[s];
							JSONObject afterarray = new JSONObject();
							try {
								if (nwordarrayL2[s] != null)
									if (triplet.containsKey(nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " "
											+ nwordarrayL2[s])) {
										int curval = triplet.get(nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " "
												+ nwordarrayL2[s]);
										if (calcvalue < curval) {
											calcvalue = curval;
											afterarray.put("triplet", nwordarrayL2[s]);
										}
									}
								String typevalue = "";
								typevalue = metricsL12[s];
								if (typevalue != null) {
									afterarray.put("typevalue", "" + typevalue);
								}

								String timeplace = "";
								timeplace = timeplaceL10[s];
								if (timeplace != null) {

									afterarray.put("timeplace", "" + timeplace);

								}
								String politeh = "";
								politeh = politeharrayL8[s];
								if (politeh != null) {
									afterarray.put("politeh", "" + politeh);
								}

								String economy = "";
								economy = economicsarrayL15[s];
								if (economy != null) {
									afterarray.put("economy", "" + economy);
								}

								String kvant = "";
								kvant = kvantorL11[s];
								if (kvant != null) {
									afterarray.put("kvant", "" + kvant);
								}

								String objsd = "";
								objsd = objectarrayL6[s];
								if (objsd != null) {
									afterarray.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
								}

							} catch (Exception e) {
							}

							if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
								after = after + " Б[" + l + "]";

								int[] is = getNegPos(emotionarrayL4OUT[s]);
								afterarray.put("nmb", s);
								afterarray.put("type", 1);
								afterarray.put("word", l);
								afterarray.put("tonneg", "" + is[0]);
								afterarray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}
								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										} else {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										}
									} else {
										if (is[0] > 0) {
											afterarray.put("tonneg", "" + is[0]);
											negafter++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										} else {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										}
									} else {
										if (is[1] > 0) {
											afterarray.put("tonpos", "" + is[1]);
											posafter++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									afterarray.put("start", beginpos);
									afterarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										afterarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
								before = before + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[s]);
								afterarray.put("nmb", s);
								afterarray.put("type", 0);
								afterarray.put("word", l);
								afterarray.put("tonneg", "" + is[0]);
								afterarray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}
								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										} else {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										}
									} else {
										if (is[0] > 0) {
											afterarray.put("tonneg", "" + is[0]);
											negafter++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											afterarray.put("tonneg", "" + 1);
											negafter++;
										} else {
											afterarray.put("tonpos", "" + 1);
											posafter++;
										}
									} else {
										if (is[1] > 0) {
											afterarray.put("tonpos", "" + is[1]);
											posafter++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									afterarray.put("start", beginpos);
									afterarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										afterarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							}
							coreafter.add(afterarray);
						}
						// 8

						// посередине, между подлижащим и сказуемым нет данных
						for (int s = skaz + 1; s < pod; s++) {
							if (s < 0)
								break;
							if (s >= size)
								break;
							String l = tokens[s];
							JSONObject middlearray = new JSONObject();
							try {
								String typevalue = "";
								typevalue = metricsL12[s];
								if (typevalue != null) {
									middlearray.put("typevalue", "" + typevalue);
								}
								String timeplace = "";
								timeplace = timeplaceL10[s];
								if (timeplace != null) {

									middlearray.put("timeplace", "" + timeplace);

								}
								String politeh = "";
								politeh = politeharrayL8[s];
								if (politeh != null) {
									middlearray.put("politeh", "" + politeh);
								}

								String economy = "";
								economy = economicsarrayL15[s];
								if (economy != null) {
									middlearray.put("economy", "" + economy);
								}

								String kvant = "";
								kvant = kvantorL11[s];
								if (kvant != null) {
									middlearray.put("kvant", "" + kvant);
								}

								String objsd = "";
								objsd = objectarrayL6[s];
								if (objsd != null) {
									middlearray.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
								}

							} catch (Exception e) {
							}

							if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
								middle = middle + " Б[" + l + "]";

								int[] is = getNegPos(emotionarrayL4OUT[s]);

								middlearray.put("nmb", s);
								middlearray.put("type", 1);
								middlearray.put("word", l);
								middlearray.put("tonneg", "" + is[0]);
								middlearray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										} else {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										}
									} else {
										if (is[0] > 0) {
											middlearray.put("tonneg", "" + is[0]);
											negmiddle++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										} else {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										}
									} else {
										if (is[1] > 0) {
											middlearray.put("tonpos", "" + is[1]);
											posmiddle++;
										}
									}
								} catch (Exception e) {
								}
								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									middlearray.put("start", beginpos);
									middlearray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										middlearray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
								middle = middle + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[s]);
								middlearray.put("nmb", s);
								middlearray.put("type", 0);
								middlearray.put("word", l);
								middlearray.put("tonneg", "" + is[0]);
								middlearray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[s - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										} else {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										}
									} else {
										if (is[0] > 0) {
											middlearray.put("tonneg", "" + is[0]);
											negmiddle++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[s])) {
										if (negpredlog.containsKey(prdf)) {
											middlearray.put("tonneg", "" + 1);
											negmiddle++;
										} else {
											middlearray.put("tonpos", "" + 1);
											posmiddle++;
										}
									} else {
										if (is[1] > 0) {
											middlearray.put("tonpos", "" + is[1]);
											posmiddle++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[s].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									middlearray.put("start", beginpos);
									middlearray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + s;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										middlearray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}

							}
							coremiddle.add(middlearray);
						}

						for (int p = pod + 1; p < pod + lim; p++) {
							if (p < 0)
								break;
							if (p >= size)
								break;
							String l = tokens[p];
							JSONObject beforeforarray = new JSONObject();

							try {

								String typevalue = "";
								typevalue = metricsL12[p];
								if (typevalue != null) {
									beforeforarray.put("typevalue", "" + typevalue);
								}
								String timeplace = "";
								timeplace = timeplaceL10[p];
								if (timeplace != null) {

									beforeforarray.put("timeplace", "" + timeplace);

								}
								String politeh = "";
								politeh = politeharrayL8[p];
								if (politeh != null) {
									beforeforarray.put("politeh", "" + politeh);
								}

								String economy = "";
								economy = economicsarrayL15[p];
								if (economy != null) {
									beforeforarray.put("economy", "" + economy);
								}

								String kvant = "";
								kvant = kvantorL11[p];
								if (kvant != null) {
									beforeforarray.put("kvant", "" + kvant);
								}

								String objsd = "";
								objsd = objectarrayL6[p];
								if (objsd != null) {
									beforeforarray.put("object", "" + objsd + "|" + idcategorymap.get(objsd));
								}

							} catch (Exception e) {
							}

							if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[p])) {
								before = before + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[p]);

								beforeforarray.put("nmb", p);
								beforeforarray.put("type", 1);
								beforeforarray.put("word", l);
								beforeforarray.put("tonneg", "" + is[0]);
								beforeforarray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[p - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										} else {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										}
									} else {
										if (is[0] > 0) {
											beforeforarray.put("tonneg", "" + is[0]);
											negbefore++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										} else {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										}
									} else {
										if (is[1] > 0) {
											beforeforarray.put("tonpos", "" + is[1]);
											posbefore++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[p].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									beforeforarray.put("start", beginpos);
									beforeforarray.put("end", endpos);

								} catch (Exception e) {
								}
								try {
									String v = "" + p;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										beforeforarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}
							}

							else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[p])) {
								after = after + " " + l;

								int[] is = getNegPos(emotionarrayL4OUT[p]);
								beforeforarray.put("nmb", p);
								beforeforarray.put("type", 0);
								beforeforarray.put("word", l);
								beforeforarray.put("tonneg", "" + is[0]);
								beforeforarray.put("tonpos", "" + is[1]);
								prdf = "";
								try {
									prdf = nwordarrayL2[p - 1];
								} catch (Exception e) {
								}

								try {
									if (iwordNegativeHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										} else {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										}
									} else {
										if (is[0] > 0) {
											beforeforarray.put("tonneg", "" + is[0]);
											negbefore++;
										}
									}
								} catch (Exception e) {
								}
								try {
									if (iwordPositiveHuman.containsKey(nwordarrayL2[p])) {
										if (negpredlog.containsKey(prdf)) {
											beforeforarray.put("tonneg", "" + 1);
											negbefore++;
										} else {
											beforeforarray.put("tonpos", "" + 1);
											posbefore++;
										}
									} else {
										if (is[1] > 0) {
											beforeforarray.put("tonpos", "" + is[1]);
											posbefore++;
										}
									}
								} catch (Exception e) {
								}

								try {
									String wordpos = tokens[p].trim();
									int beginpos = 0;
									int endpos = 0;
									List<IndexWrapper> listspos = new ArrayList<>();
									listspos = findIndexesForKeyword(singltext, wordpos);

									if (listspos.size() > 0) {

										beginpos = listspos.get(0).getStart();
										endpos = wordpos.length() + beginpos;

									}

									beforeforarray.put("start", beginpos);
									beforeforarray.put("end", endpos);

								} catch (Exception e) {
								}

								try {
									String v = "" + p;
									Map<String, String> returtQ = objectMap.get(v);
									if (returtQ != null) {
										StringBuffer sb = new StringBuffer();

										for (Map.Entry<String, String> entryQ : returtQ.entrySet()) {
											sb.append("" + entryQ.getKey() + "|" + entryQ.getValue() + "n ");

										}

										beforeforarray.put("qdata", sb.toString());
									}

								} catch (Exception e) {
								}

							}
							corebefore.add(beforeforarray);
						}

						String wordkey = "";

					}

					inobj.put("corebefore", corebefore);
					inobj.put("coremiddle", coremiddle);
					inobj.put("coreafter", coreafter);
					inobj.put("itempod", itempod);
					inobj.put("itemscaz", itemscaz);
					maincorearray.add(inobj);

				} catch (Exception e) {
					e.printStackTrace();
				}

				tonpos = tonpos + posbefore;
				tonneg = tonneg + negbefore;

				tonposscaz = tonposscaz + posafter;
				tonnegscaz = tonnegscaz + negafter;

				// int negt = coreemmotionMap.get("negative");
				// coreemmotionMap.put("negative", negt+tonneg+tonnegscaz);
				// int post = coreemmotionMap.get("positive");
				// coreemmotionMap.put("positive", post+tonpos+tonposscaz);

				j++;
				if (j > 1)
					break;
			}
			JSONArray adjarray = new JSONArray();

			for (Entry<String, Integer> d : adjectiveList.entrySet()) {
				adjarray.add(d.getKey());
			}

			srtjson.put("adjective", "" + adjarray);
			srtjson.put("core", "" + maincorearray);

			// listjson.add(srtjson);

		}
		return srtjson;
	}

	private Map<Integer[], Integer> getNewPodSkazRules(Map<Integer[], Integer> sortedd,
			Map<String, Integer> unsortMapIS, Map<Integer[], Integer> sorteddlemmaI,
			Map<String, Integer> unsortMaplemmaIS, boolean getrezult, String[] scaarrayL17, String[] nwordarrayL2,
			Map<Integer, String> predicates) {
		
		
		for (Entry<Integer, String> entryradio : predicates.entrySet()) {
			
			System.out.println("X==="+nwordarrayL2[entryradio.getKey()]);
			
		}
		
		
		// if (!getrezult)
		// return sortedd;
		// существуют состояния
		// нет значений sortedd==0 в леммах есть значения sorteddlemmaI>0
		// значений sortedd>0 в леммах есть значения sorteddlemmaI>0 (добавить
		// то что не пересекается)
		// значение sortedd>0 с величиной -1 значение sorteddlemmaI> с величиной
		// 1
		// значение sortedd>0 с величиной -1 значение sorteddlemmaI> с величиной
		// -1

		int size = sortedd.size();
		int sizeLemma = sorteddlemmaI.size();
		Map<Integer[], Integer> result = new HashMap<Integer[], Integer>();

		if (size > 0 && sizeLemma > 0) {

			// 0. мне его обработка, я мы существует - собавление слов к
			// контексту, Возможный перебор по синонимам
			// 0. обработка имен николай

			try {

				// ГИПОТЕЗЫ
				// 1. Расстояние между А V не больше 2х слов между(предлог,
				// прилагательное, наречие) //мальчик быстро в иди
				// 2. Два А V разделенные или нет знаком припинания //Граждани
				// волнуются, пока Собянин строит дома
				// 3. Обработка множественного числа по лексемам //Граждани

				// if(size<=0) return sortedd;

				List<Integer[]> data = new ArrayList<Integer[]>();
				SenceScaz[] distance = new SenceScaz[size];
				List<SenceScaz> scazList = new ArrayList<SenceScaz>();

				Map.Entry<Integer[], Integer> firstEntry = sortedd.entrySet().iterator().next();
				Integer[] firstinf = new Integer[3];
				firstinf = firstEntry.getKey();
				int podfirst = firstinf[0];
				int skazfirst = firstinf[1];

				int lk = 0;
				for (Map.Entry<Integer[], Integer> entry : sortedd.entrySet()) {
					Integer[] inf = new Integer[3];

					inf = entry.getKey();
					int inftotal = entry.getValue();
					// A
					int pod = inf[0];
					// V
					int skaz = inf[1];
					int ihasNo = inf[2];

					// значение sortedd>0 с величиной -1
					if (inftotal == -1) {
						inftotal = unsortMaplemmaIS.get(pod + "" + skaz);

						if (inftotal == -1) {
							// поиск синонимов глаголов или сущ для нахождения
							// не отрицательной величины
						}
					}

					// Расстояние между ними
					int r = pod - skaz;
					SenceScaz sa = new SenceScaz(pod, skaz, Math.abs(r), inftotal, ihasNo);
					distance[lk] = sa;
					lk++;
					// if (lk > 1) break;
				}

				// отсортирован по расстоянию между A V
				Arrays.sort(distance);

				// создать массив лучших не больше 2х слов между
				// if(pod1-skaz1)<2
				// проверить Гипотезу последовательность следования А V V
				// if(skaz1-skaz2)<2(средней длины предложения) && if(skaz1[p]
				// нет pod[any] между ними skaz2[p])
				// какая частота у pod1+skaz1 и pod1+skaz2

				// проверить Гипотезу последовательность следования А V, A V с
				// учетом и без ранее найденных гипотез
				// if(pod1-skaz1)<2 частота +
				// if(pod2-skaz2)<2 частота +
				// pod1-skaz1 расстояние не пересекается с pod2-skaz2
				// (врач сказал и(совершенно ничего) не сделал нужную запись,
				// из-за его не компетентности я чуть не заболел)

				// проверить если, идут глаголы подрят (-->>Врач -->>сказал и не
				// -->>сделал нужную запись) A V V
				// Развязка<<-- на пересечении ЦКАД и Калужского ш. -->>увеличит
				// пропускную способность шоссе и -->>перераспределит
				// транспортные потоки по этим магистралям
				// -->>Открыли движение<<-- по новому автодорожному путепроводу
				// на 33 км Киевского направления. 4-полосную -->>эстакаду
				// -->>построили меньше чем за год
				// у обоих глаголов положительный вес Integer? для А

				// между ними есть знаки препинания, это два разных А V, A V# А
				// V V, A V?

				Integer firstA = 0;
				Integer secondA = 0;
				Map<Integer, Integer> first = new HashMap<Integer, Integer>();
				// List
				// Integer firstV=0;
				// Integer secondV=0;
				int canexit = 0;
				int count = 0;
				int i = 0;
				for (SenceScaz temp : distance) {

					// Первым значением является максимальное значиение согласно
					// обизости 2-х АV
					int structype = 0;
					Integer[] inffirst = new Integer[11];
					inffirst[0] = structype;
					inffirst[1] = temp.getPod();
					inffirst[2] = temp.getScaz();
					inffirst[3] = temp.getIhasno();
					inffirst[4] = temp.getTotal();
					inffirst[5] = temp.getQuantity();
					inffirst[6] = 0;
					inffirst[7] = 0;
					inffirst[8] = 0;
					inffirst[9] = 0;
					inffirst[10] = 0;
					result.put(inffirst, i);
					i++;

					System.out.println(" temp=" + temp.getPod() + " " + temp.getScaz() + " total:" + temp.getTotal()
							+ " quantity: " + temp.getQuantity() + " " + nwordarrayL2[temp.getPod()] + " "
							+ nwordarrayL2[temp.getScaz()]);
					// System.out.println("fruits " + temp.getPod() + " : " +
					// temp.getScaz() + ", Quantity : " + temp.getQuantity());
					// расстояние наименьшее у 2-х различных А

					//
					// если такого А никогда не было начинаем его обработку
					if (!first.containsKey(temp.getPod())) {
						// проверить Гипотезу последовательность следования А V
						// V

						// 5-7 x-следующий глагол
						for (String f : scaarrayL17) {
							int x = Integer.parseInt(f);
							if (temp.getScaz() != x) {
								int inverbDistance = Math.abs(temp.getScaz() - x);
								if (inverbDistance <= 4) {
									// нужна проверка на то что между ними нет
									// глаголов(кроме мочь быть) и сущ (будет
									// хорошо сказал, но не красиво сделал)
									// а между ними есть A? можно мапе
									// посмотреть А идущие по порядку
									// а какая частота у А+х?
									structype = 1;
									Integer[] inf = new Integer[11];
									inf[0] = structype;
									inf[1] = temp.getPod();
									inf[2] = temp.getScaz();
									inf[3] = temp.getIhasno();
									inf[4] = temp.getTotal();
									inf[5] = temp.getQuantity();

									int secondTotal = -1;

									try {
										Integer secondT = unsortMapIS.get(temp.getPod() + "" + x);
										if (secondT != null) {
											secondTotal = secondT;
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
									inf[6] = temp.getPod();
									inf[7] = x;
									inf[8] = temp.getIhasno();
									inf[9] = secondTotal;
									inf[10] = temp.getQuantity() + inverbDistance;

									/*
									 * inf[0] = temp.getPod(); inf[1] =
									 * temp.getScaz(); inf[2] = x; inf[3] =
									 * temp.getIhasno(); inf[4] =
									 * temp.getTotal(); inf[5] = -1; inf[6] =
									 * -1; inf[7] = -1; inf[8] = -1; inf[9] =
									 * -1;
									 */
									// if(temp.getTotal()>-1){
									System.out.println(" AVV=" + temp.getPod() + " " + temp.getScaz() + " " + x + " "
											+ nwordarrayL2[temp.getPod()] + " " + nwordarrayL2[temp.getScaz()] + " "
											+ nwordarrayL2[x]);
									result.put(inf, i);
									// }
								}
							}

							first.put(temp.getPod(), temp.getScaz());
							scazList.add(temp);
							count++;
							// таких значений уже 2а с самым коротким
							// расстоянием?
							if (canexit > 2) {
								break;
							}
							// проверить Гипотезу последовательность следования
							// А V, A V с учетом и без ранее найденных гипотез
							// if(pod1-skaz1)<2 частота +
							// if(pod2-skaz2)<2 частота +
							// pod1-skaz1 расстояние не пересекается с
							// pod2-skaz2
							// сравни с ранее наденными значениями

							if (count - 2 >= 0 && scazList.size() > 0) {
								SenceScaz previus = scazList.get(count - 2);
								int p = previus.getPod();
								int s = previus.getScaz();
								int t = previus.getTotal();
								int n = previus.getIhasno();
								boolean df = false;
								if (p != temp.getPod() && s != temp.getScaz())
									df = canProsecc(p, s, temp.getPod(), temp.getScaz());
								// System.out.println(" cprevius="+p+" "+s+"
								// "+t);
								// установлено следование А V, A V
								if (df) {
									// проверить их значения частота +
									// если значений несколько выбрать лучшую
									// пару

									/*
									 * Integer[] inf = new Integer[10]; inf[0] =
									 * p; inf[1] = s; inf[2] = -1; inf[3] = n;
									 * inf[4] = t;
									 * 
									 * inf[5] = temp.getPod(); inf[6] =
									 * temp.getScaz(); inf[7] = -1; inf[8] =
									 * temp.getIhasno(); inf[9] =
									 * temp.getTotal();
									 */

									if (t > -1 || temp.getTotal() > -1) {
										structype = 2;
										Integer[] inf = new Integer[11];
										inf[0] = structype;
										inf[1] = previus.getPod();
										inf[2] = previus.getScaz();
										inf[3] = previus.getIhasno();
										inf[4] = previus.getTotal();
										inf[5] = previus.getQuantity();

										inf[6] = temp.getPod();
										inf[7] = temp.getScaz();
										inf[8] = temp.getIhasno();
										inf[9] = temp.getTotal();
										inf[10] = temp.getQuantity();

										result.put(inf, i);

										System.out.println(" firstAV =" + p + " " + s + " " + t + " " + nwordarrayL2[p]
												+ " " + nwordarrayL2[s]);
										System.out.println(" secondAV=" + temp.getPod() + " " + temp.getScaz() + " "
												+ temp.getTotal() + " " + nwordarrayL2[temp.getPod()] + " "
												+ nwordarrayL2[temp.getScaz()]);

										canexit++;
									}
								}
							}

						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		// ---есть значения sorteddlemmaI>0
		else if (size <= 0 && sizeLemma > 0) {

		}
		// ---нет подлежащих и
		else if (size <= 0 && sizeLemma <= 0) {

		}

		// результатом может быть A V#A V V#А V, A V
		// if (result.size() > 0)
		return result;
		// else
		// return sortedd;
	}

	private Map<Integer[], Integer> getPodSkazRules(Map<Integer[], Integer> sortedd, Map<String, Integer> unsortMapIS,
			Map<Integer[], Integer> sorteddlemmaI, Map<String, Integer> unsortMaplemmaIS, boolean getrezult,
			String[] scaarrayL17, String[] nwordarrayL2) {
		if (!getrezult)
			return sortedd;
		// существуют состояния
		// нет значений sortedd==0 в леммах есть значения sorteddlemmaI>0
		// значений sortedd>0 в леммах есть значения sorteddlemmaI>0 (добавить
		// то что не пересекается)
		// значение sortedd>0 с величиной -1 значение sorteddlemmaI> с величиной
		// 1
		// значение sortedd>0 с величиной -1 значение sorteddlemmaI> с величиной
		// -1

		int size = sortedd.size();
		int sizeLemma = sorteddlemmaI.size();
		Map<Integer[], Integer> result = new HashMap<Integer[], Integer>();

		if (size > 0 && sizeLemma > 0) {

			// 0. мне его обработка, я мы существует - собавление слов к
			// контексту, Возможный перебор по синонимам
			// 0. обработка имен николай

			try {

				// ГИПОТЕЗЫ
				// 1. Расстояние между А V не больше 2х слов между(предлог,
				// прилагательное, наречие) //мальчик быстро в иди
				// 2. Два А V разделенные или нет знаком припинания //Граждани
				// волнуются, пока Собянин строит дома
				// 3. Обработка множественного числа по лексемам //Граждани

				// if(size<=0) return sortedd;

				List<Integer[]> data = new ArrayList<Integer[]>();
				SenceScaz[] distance = new SenceScaz[size];
				List<SenceScaz> scazList = new ArrayList<SenceScaz>();

				Map.Entry<Integer[], Integer> firstEntry = sortedd.entrySet().iterator().next();
				Integer[] firstinf = new Integer[3];
				firstinf = firstEntry.getKey();
				int podfirst = firstinf[0];
				int skazfirst = firstinf[1];

				int lk = 0;
				for (Map.Entry<Integer[], Integer> entry : sortedd.entrySet()) {
					Integer[] inf = new Integer[3];

					inf = entry.getKey();
					int inftotal = entry.getValue();
					// A
					int pod = inf[0];
					// V
					int skaz = inf[1];
					int ihasNo = inf[2];

					// значение sortedd>0 с величиной -1
					if (inftotal == -1) {
						inftotal = unsortMaplemmaIS.get(pod + "" + skaz);

						if (inftotal == -1) {
							// поиск синонимов глаголов или сущ для нахождения
							// не отрицательной величины
						}
					}

					// Расстояние между ними
					int r = pod - skaz;
					SenceScaz sa = new SenceScaz(pod, skaz, Math.abs(r), inftotal, ihasNo);
					distance[lk] = sa;
					lk++;
					// if (lk > 1) break;
				}

				// отсортирован по расстоянию между A V
				Arrays.sort(distance);

				// создать массив лучших не больше 2х слов между
				// if(pod1-skaz1)<2
				// проверить Гипотезу последовательность следования А V V
				// if(skaz1-skaz2)<2(средней длины предложения) && if(skaz1[p]
				// нет pod[any] между ними skaz2[p])
				// какая частота у pod1+skaz1 и pod1+skaz2

				// проверить Гипотезу последовательность следования А V, A V с
				// учетом и без ранее найденных гипотез
				// if(pod1-skaz1)<2 частота +
				// if(pod2-skaz2)<2 частота +
				// pod1-skaz1 расстояние не пересекается с pod2-skaz2
				// (врач сказал и(совершенно ничего) не сделал нужную запись,
				// из-за его не компетентности я чуть не заболел)

				// проверить если, идут глаголы подрят (-->>Врач -->>сказал и не
				// -->>сделал нужную запись) A V V
				// Развязка<<-- на пересечении ЦКАД и Калужского ш. -->>увеличит
				// пропускную способность шоссе и -->>перераспределит
				// транспортные потоки по этим магистралям
				// -->>Открыли движение<<-- по новому автодорожному путепроводу
				// на 33 км Киевского направления. 4-полосную -->>эстакаду
				// -->>построили меньше чем за год
				// у обоих глаголов положительный вес Integer? для А

				// между ними есть знаки препинания, это два разных А V, A V# А
				// V V, A V?

				Integer firstA = 0;
				Integer secondA = 0;
				Map<Integer, Integer> first = new HashMap<Integer, Integer>();
				// List
				// Integer firstV=0;
				// Integer secondV=0;
				int canexit = 0;
				int count = 0;
				for (SenceScaz temp : distance) {

					System.out.println(" temp=" + temp.getPod() + " " + temp.getScaz() + " total:" + temp.getTotal()
							+ " quantity: " + temp.getQuantity() + " " + nwordarrayL2[temp.getPod()] + " "
							+ nwordarrayL2[temp.getScaz()]);
					// System.out.println("fruits " + temp.getPod() + " : " +
					// temp.getScaz() + ", Quantity : " + temp.getQuantity());
					// расстояние наименьшее у 2-х различных А

					//
					// если такого А никогда не было начинаем его обработку
					if (!first.containsKey(temp.getPod())) {
						// проверить Гипотезу последовательность следования А V
						// V

						// 5-7 x-следующий глагол
						for (String f : scaarrayL17) {
							int x = Integer.parseInt(f);
							if (temp.getScaz() != x)
								if (Math.abs(temp.getScaz() - x) <= 4) {
									// нужна проверка на то что между ними нет
									// глаголов(кроме мочь быть) и сущ (будет
									// хорошо сказал, но не красиво сделал)
									// а между ними есть A? можно мапе
									// посмотреть А идущие по порядку
									// а какая частота у А+х?

									Integer[] inf = new Integer[10];

									inf[0] = temp.getPod();
									inf[1] = temp.getScaz();
									inf[2] = x;
									inf[3] = temp.getIhasno();
									inf[4] = temp.getTotal();
									inf[5] = -1;
									inf[6] = -1;
									inf[7] = -1;
									inf[8] = -1;
									inf[9] = -1;

									// if(temp.getTotal()>-1){
									System.out.println(" AVV=" + temp.getPod() + " " + temp.getScaz() + " " + x + " "
											+ nwordarrayL2[temp.getPod()] + " " + nwordarrayL2[temp.getScaz()] + " "
											+ nwordarrayL2[x]);
									result.put(inf, temp.getTotal());
									// }
								}

							first.put(temp.getPod(), temp.getScaz());
							scazList.add(temp);
							count++;
							// таких значений уже 2а с самым коротким
							// расстоянием?
							if (canexit > 2) {
								break;
							}
							// проверить Гипотезу последовательность следования
							// А V, A V с учетом и без ранее найденных гипотез
							// if(pod1-skaz1)<2 частота +
							// if(pod2-skaz2)<2 частота +
							// pod1-skaz1 расстояние не пересекается с
							// pod2-skaz2
							// сравни с ранее наденными значениями

							if (count - 2 >= 0 && scazList.size() > 0) {
								SenceScaz previus = scazList.get(count - 2);
								int p = previus.getPod();
								int s = previus.getScaz();
								int t = previus.getTotal();
								int n = previus.getIhasno();
								boolean df = false;
								if (p != temp.getPod() && s != temp.getScaz())
									df = canProsecc(p, s, temp.getPod(), temp.getScaz());
								// System.out.println(" cprevius="+p+" "+s+"
								// "+t);
								// установлено следование А V, A V
								if (df) {
									// проверить их значения частота +
									// если значений несколько выбрать лучшую
									// пару
									Integer[] inf = new Integer[10];
									inf[0] = p;
									inf[1] = s;
									inf[2] = -1;
									inf[3] = n;
									inf[4] = t;

									inf[5] = temp.getPod();
									inf[6] = temp.getScaz();
									inf[7] = -1;
									inf[8] = temp.getIhasno();
									inf[9] = temp.getTotal();
									if (t > -1 || temp.getTotal() > -1) {
										result.put(inf, t + temp.getTotal());

										System.out.println("AVAV first =" + p + " " + s + " " + t + " "
												+ nwordarrayL2[p] + " " + nwordarrayL2[s]);
										System.out.println("AVAV second=" + temp.getPod() + " " + temp.getScaz() + " "
												+ temp.getTotal() + " " + nwordarrayL2[temp.getPod()] + " "
												+ nwordarrayL2[temp.getScaz()]);

										canexit++;
									}
								}
							}

						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		// ---есть значения sorteddlemmaI>0
		else if (size <= 0 && sizeLemma > 0) {

		}
		// ---нет подлежащих и
		else if (size <= 0 && sizeLemma <= 0) {

		}

		// результатом может быть A V#A V V#А V, A V
		if (result.size() > 0)
			return result;
		else
			return sortedd;
	}

	public boolean canProsecc(int p, int s, int cp, int cs) {
		boolean sts = false;
		// System.out.println("p=" + p + " s=" + s + " cp=" + cp + " cs=" + cs);
		if (cp < cs) {
			// 0 12 1 15 подлежащее внутри интервала?
			// 1 12 0 15

			if (p >= cp && p <= cs) {
				// System.out.println(" -previus=1");
			} else {
				if (s >= cp && s <= cs) {
					// System.out.println(" -previus=2");
				} else {
					// за пределами интервала
					// System.out.println(" -previus=3");
					sts = true;
				}
			}

		} else {
			// 0 6 1 8 подлежащее внутри интервала?
			if (p >= cs && p <= cp) {
			} else {
				if (s >= cs && s <= cp) {
				} else {
					// за пределами интервала
					sts = true;
				}
			}
		}

		return sts;
	}

	public String[] getMorfema(String nword, Map<String, String> morfema, Map<String, String> iwordPositive,
			Map<String, String> iwordNegative) {
		String[] synindex = new String[3];
		synindex[0] = null;
		synindex[1] = null;
		synindex[2] = null;
		try {
			String m = morfema.get(nword);
			StringBuffer array = new StringBuffer();
			if (m != null) {
				if (m.contains("|")) {
					String[] mdata = m.split("\\|");
					int sizeb = mdata.length;
					for (int i = 0; i < sizeb; i++) {
						String single = mdata[i];
						try {
							String neg = iwordNegative.get(single);
							if (neg != null) {
								array.append(nword + "[" + single + "]").append("-NEG ");
								synindex[0] = single;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						try {
							String pos = iwordPositive.get(single);
							if (pos != null) {
								array.append(nword + "[" + single + "]").append("-POS ");
								// wordjson.put("pos", true);
								synindex[1] = single;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					// wordjson.put("emo", array.toString());
					synindex[2] = array.toString();
				} else {

					try {
						String neg = iwordNegative.get(m);
						if (neg != null) {
							// resultarray.append(lowerStr+"["+m+"]").append("-NEG
							// ");
							// wordjson.put("emoneg", m);
							// wordjson.put("neg", true);
							synindex[0] = m;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						String pos = iwordPositive.get(m);
						if (pos != null) {
							// resultarray.append(lowerStr+"["+m+"]").append("-POS
							// ");
							// wordjson.put("emopos", m);
							// wordjson.put("pos", true);
							synindex[1] = m;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return synindex;
	}

	public String clean(String text) {

		String dirty = text;
		try {
			dirty = text.replaceAll("\\pP", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dirty = dirty.replaceAll("ё|Ё", "е");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dirty = dirty.replaceAll("[\\s]{2,}", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dirty;
	}

	public String getTextofQ(String idx, Map<String, List> iiltext) {
		String ofinstance = null;
		try {
			List<String> list = iiltext.get(idx);
			StringBuffer sb = new StringBuffer();
			for (String c : list) {
				sb.append(c).append("-");
			}
			ofinstance = sb.toString();
		} catch (Exception e) {
		}

		return ofinstance;
	}

	boolean isPresent(String serchword, String text) {
		if (text != null) {
			String regex = "\\b" + serchword + "\\b";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(text);

			while (matcher.find() == true) {

				return true;
			}
			return false;
		} else
			return false;
	}

	public Map<String, List> searchMainObjectArray(List<String> qindexList, Map<String, List> iilobjectsList,
			Map<String, List> iilinstanceList, Map<String, List> iilsubclassofList, Map<String, List> iilproperties,
			Map<String, List> iilmaincategory, Map<String, List> iiltext, Map<String, List> iilhaspart) {

		Map<String, List> returndata = new HashMap<String, List>();

		Map<String, String> qgeo = fastMemoryModel.getCheckIfGeo();
		Map<String, String> qhuman = fastMemoryModel.getCheckIfHuman();
		Map<String, String> qorg = fastMemoryModel.getCheckIfOrg();
		String itypeid = "";
		String icategoryid = "";
		String savetypeid = "none";
		String savecategoryid = "";
		String itogo = "";
		String[] res = new String[2];
		res[0] = "";
		res[1] = "";

		boolean sgeo = false;
		boolean shuman = false;
		boolean sorg = false;
		if (sgeo == false && shuman == false && sorg == false) {
			// ----------------------------Q----------------------------
			// https://dumps.wikimedia.org/wikidatawiki/entities/
			boolean canexit = false;
			// список Q полученных по слову
			if (qindexList != null)
				for (String qindex : qindexList) {
					if (canexit)
						break;
					List<String> resultThree = new ArrayList<String>();

					if (qindex != null) {
						Map<String, String> viewed = new HashMap<String, String>();

						// StringBuffer sb=new StringBuffer();

						boolean doit = true;
						int count = 0;
						do {
							if (canexit)
								break;
							count++;
							if (count > 100)
								break;
							String preview = qindex;
							// думали об этом Q уже?
							if (!viewed.containsKey(qindex)) {

								List<String> vi = new ArrayList<String>();
								vi.add(qindex);

								int icount = 0;
								do {
									if (canexit)
										break;

									// System.out.println("--------vi.get-----="+vi.toString());
									// System.out.println(icount+"--------vi.get-----="+vi.get(0));
									String qdata = vi.get(0);
									icount++;
									resultThree.add(qdata);
									// String words=getTextofQ(first, iiltext);
									// String wordsprop=getPropertyOfType(qdata,
									// iilproperties);
									// List<String> otherPropList=
									// iilproperties.get(qdata);
									// if(otherPropList!=null)
									// for(String c:otherPropList){
									//
									// }

									if (icount > 1000)
										break;
									if (!viewed.containsKey(qdata)) {
										List<String> listofinstance = null;
										List<String> listsubclassof = null;
										List<String> listotherprops = null;
										List<String> listhaspart = null;
										String label = "";
										savetypeid = qdata;

										listofinstance = iilinstanceList.get(qdata);
										listsubclassof = iilsubclassofList.get(qdata);
										listotherprops = iilproperties.get(qdata);
										listhaspart = iilhaspart.get(qdata);
										// System.out.println(icount+"--------listofinstance-----="+listofinstance);
										// System.out.println(icount+"--------listsubclassof-----="+listsubclassof);

										/*
										 * String values=getCategoryText(qdata,
										 * iilmaincategory, iiltext);
										 * sb.append(" "+qdata+"category["+
										 * values+"]");
										 */

										// if(iorclass==1){
										// listofinstance =
										// getListInstance(qdata,
										// iilinstanceList);label="ofinstance";
										// listsubclassof =
										// getListInstance(qdata,
										// iilsubclassofList);label="subclassof";
										// }

										if (listhaspart != null) {

											for (String ins : listhaspart) {
												vi.add(ins);

												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}

											}
										}

										if (listotherprops != null) {

											for (String ins : listotherprops) {
												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}

											}
										}
										if (listofinstance != null) {
											for (String ins : listofinstance) {
												// System.out.println("--------ofinstance-----="+ins);
												vi.add(ins);
												// String
												// lwordsprop=getPropertyOfType(ins,
												// iiltype);
												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}
											}
										}
										if (listsubclassof != null) {
											for (String ins : listsubclassof) {
												// System.out.println("--------subclassof-----="+ins);
												vi.add(ins);
												// String
												// lwordsprop=getPropertyOfType(ins,
												// iiltype);
												if (qgeo.containsKey(ins)) {
													sgeo = true;
													savetypeid = "Q17334923";
													break;
												} else if (qhuman.containsKey(ins)) {
													shuman = true;
													savetypeid = "Q5";
													break;
												} else if (qorg.containsKey(ins)) {
													sorg = true;
													savetypeid = "Q43229";
													break;
												}
											}
										}

										// if(listhaspart!=null){
										//
										// for(String ins:listhaspart){
										// vi.add(ins);
										//
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										//
										// }
										// }
										//
										// if(listotherprops!=null){
										//
										// for(String ins:listotherprops){
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										//
										// }
										// }
										// if(listofinstance!=null){
										// for(String ins:listofinstance){
										// //System.out.println("--------ofinstance-----="+ins);
										// vi.add(ins);
										// //String
										// lwordsprop=getPropertyOfType(ins,
										// iiltype);
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										// }
										// }
										// if(listsubclassof!=null){
										// for(String ins:listsubclassof){
										// //System.out.println("--------subclassof-----="+ins);
										// vi.add(ins);
										// //String
										// lwordsprop=getPropertyOfType(ins,
										// iiltype);
										// if(qgeo.containsKey(ins)){sgeo=true;
										// savetypeid ="Q17334923";
										// canexit=true; break;}
										// else
										// if(qhuman.containsKey(ins)){shuman=true;
										// savetypeid ="Q5"; canexit=true;
										// break;}
										// else
										// if(qorg.containsKey(ins)){sorg=true;savetypeid
										// ="Q43229"; canexit=true; break;}
										// }
										// }
										// String
										// valuesCategory=getCategoryText(qdata,
										// iilmaincategory, iiltext);
										// returndata.put(qdata,
										// ""+icount+"#"+valuesCategory);
										vi.remove(qdata);
									}

								} while (vi.size() > 0);

								if (preview == qindex)
									doit = false;

								if (qindex == null)
									doit = false;
								// returndata.put(qindex, ""+icount);
								// String valuesCategory=getCategoryText(qindex,
								// iilmaincategory, iiltext);
								// returndata.put(qindex,
								// ""+icount+"u"+valuesCategory);
								viewed.put(qindex, qindex);
							}

						} while (doit);

						// itogo = sb.toString();
					}
					returndata.put(qindex, resultThree);
				}
		}

		List<String> resultsavetypeid = new ArrayList<String>();
		resultsavetypeid.add(savetypeid);
		returndata.put("1", resultsavetypeid);
		// returndata.put("2", itogo);
		// res[0]=savetypeid;
		// res[1]=itogo;
		return returndata;
	}

	public List<IndexWrapper> findIndexesForKeyword(String searchString, String keyword) {
		String regex = "\\b" + keyword + "\\b";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(searchString);

		List<IndexWrapper> wrappers = new ArrayList<IndexWrapper>();
		int start = 0;
		int end = 0;
		boolean inotfind = true;
		while (matcher.find() == true) {
			end = matcher.end();
			start = matcher.start();
			IndexWrapper wrapper = new IndexWrapper(start, end);
			wrappers.add(wrapper);
			inotfind = false;
		}
		if (inotfind) {
			IndexWrapper wrapper = new IndexWrapper(start, end);
			wrappers.add(wrapper);
		}
		return wrappers;
	}

	private Map<Integer[], Integer> sortByValueI(Map<Integer[], Integer> unsortMap) {

		// 1. Convert Map to List of Map
		List<Map.Entry<Integer[], Integer>> list = new LinkedList<Map.Entry<Integer[], Integer>>(unsortMap.entrySet());

		// 2. Sort list with Collections.sort(), provide a custom Comparator
		// Try switch the o1 o2 position for a different order
		Collections.sort(list, new Comparator<Map.Entry<Integer[], Integer>>() {
			public int compare(Map.Entry<Integer[], Integer> o1, Map.Entry<Integer[], Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		// 3. Loop the sorted list and put it into a new insertion order Map
		// LinkedHashMap
		Map<Integer[], Integer> sortedMap = new LinkedHashMap<Integer[], Integer>();
		for (Map.Entry<Integer[], Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	private Map<String, Integer> sortByValueSI(Map<String, Integer> unsortMap) {

		// 1. Convert Map to List of Map
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// 2. Sort list with Collections.sort(), provide a custom Comparator
		// Try switch the o1 o2 position for a different order
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		// 3. Loop the sorted list and put it into a new insertion order Map
		// LinkedHashMap
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Map.Entry<String, Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public int[] getNegPos(String word) {
		int[] synindex = new int[2];
		synindex[0] = 0;
		synindex[1] = 0;
		if (word != null) {
			if (word.contains("#")) {
				String[] myData = word.split("#");
				int n = 0;
				int p = 0;

				for (String s : myData) {
					if (s.contains("|")) {
						String[] negpos = s.split("\\|");

						String neg = negpos[0].trim();
						String pos = negpos[1].trim();

						if (neg.equals("true")) {
							n = n + 1;
						}
						if (pos.equals("true")) {
							p = p + 1;
						}
					}
				}
				synindex[0] = n;
				synindex[1] = p;
			}
		}
		return synindex;
	}

	int[] getPisitionC(String Str, String[] wordtoken, List<IndexWrapper> probelTextCount) {

		int[] beginend = new int[2];
		beginend[0] = -1000;
		beginend[1] = -1000;
		// text:T|N - Т номер слова N грамма I количество слов вхождений
		// номер слова N + его длина оптередяют смещение
		// если это 0 то отдать сразу
		// int myposition=-1000;
		try {
			String text = wordtoken[0];
			String TN = wordtoken[1];

			String[] objectidData = TN.split("\\|");

			String T = objectidData[0];
			String I = objectidData[2];

			if (I.equals("1")) {
				List<IndexWrapper> list = new ArrayList<>();
				list = findIndexesForKeyword(Str, text);
				try {
					if (list.size() > 0) {
						int lbeforeposstart = list.get(0).getStart();
						int lbeforeposend = list.get(0).getEnd();

						beginend = getTruePos(lbeforeposstart, lbeforeposend, probelTextCount);
						// myposition=Str.indexOf( text );
						return beginend;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				// int t=Integer.parseInt(T);
				int i = Integer.parseInt(I);
				if (i > 0)
					i = i - 1;
				List<IndexWrapper> list = new ArrayList<>();
				list = findIndexesForKeyword(Str, text);
				try {
					if (list.size() > 0) {

						// myposition=findIndexesForKeyword(Str,text).get(i).getStart();
						int lbeforeposstart = list.get(i).getStart();
						int lbeforeposend = list.get(i).getEnd();

						beginend = getTruePos(lbeforeposstart, lbeforeposend, probelTextCount);

						return beginend;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return beginend;
		}
		return beginend;
	}

	public int[] getTruePos(int beforeposstart, int beforeposend, List<IndexWrapper> probelTextCount) {
		int[] beginend = new int[2];
		beginend[0] = 0;
		beginend[1] = 0;
		try {
			for (IndexWrapper entry : probelTextCount) {
				int start = entry.getStart();
				int end = entry.getEnd();
				if (beforeposstart > start) {
					int deleted = end - start - 1;
					if (deleted > 0) {
						beforeposstart = beforeposstart + deleted;
					}
				}
				if (start < beforeposend) {
					int deleted = end - start - 1;
					if (deleted > 0) {
						beforeposend = beforeposend + deleted;
					}
				}

			}

			int begin = beforeposstart;
			int bend = beforeposend;

			beginend[0] = begin;
			beginend[1] = bend;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return beginend;

	}

	private String getMyweekID(String[] weaksynonimText, String weaksynonim, String weaksynonimlowerStr,
			Map<String, String> weakcontextMap, List<String[]> alltokens, String allText) {

		// может быть один ID а может быть несколько -- "|"
		Map<String, String> weaksynmap = new HashMap<String, String>();
		if (weaksynonim.contains("|")) {
			String[] myData = weaksynonim.split("\\|");
			for (String syns : myData) {
				weaksynmap.put(syns, "1");
			}

		} else {
			weaksynmap.put(weaksynonim, "1");

		}

		Map<String, String> contextMap = new HashMap<String, String>();

		contextMap = weakcontextMap;
		String objectid = null;
		int k = 0;
		for (String[] wordtoken : alltokens) {

			try {
				String lowerStr = wordtoken[0].toLowerCase().trim();
				// System.out.println("token="+lowerStr);

				// анализ контекста объектов по токенам
				// слово из текста совпадает со словом синонимма - тогда его в
				// контексте не ищем
				// if(!weaksynonimlowerStr.equals(lowerStr))
				// System.out.println("weaksynonimlowerStr="+weaksynonimlowerStr+"
				// lowerStr="+lowerStr);

				if (!weaksynonimlowerStr.equals(lowerStr))
					objectid = contextMap.get(lowerStr);

				if (objectid == null) {

					// return null;

				} else {
					// System.out.println(" Context_objectid="+objectid);
					// может быть один ID а может быть несколько -- "|"
					if (objectid.contains("|")) {
						String[] objectidData = objectid.split("\\|");
						for (String key : objectidData) {
							if (weaksynmap.containsKey(key)) {
								boolean dd = calcWordsDistanceB(weaksynonimText, wordtoken[0], allText);
								if (dd)
									return key;
							}

						}
					} else {
						if (weaksynmap.containsKey(objectid)) {

							boolean dd = calcWordsDistanceB(weaksynonimText, wordtoken[0], allText);
							if (dd)
								return objectid;
						}

						// расстояние пока не учитываем

					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			k++;
			if (k > 15000) {
				break;
			}
		}

		return null;
	}

	private boolean calcWordsDistanceB(String[] weaksynonimText, String synonimContext, String text) {
		boolean dist = false;
		try {

			int firstW = getPisitionB(text, weaksynonimText);
			List<Integer> list = new ArrayList<>();
			list = getWordPisitionB(text, synonimContext);
			int k = 0;
			for (Integer secondW : list) {
				if (firstW > -1 && secondW > -1) {

					// 100 символов
					// firstW 10+длина слова - secondW 100= -90 < 30
					// secondW 100+длина слова - firstW 10= 50 < 30

					if (secondW > firstW) {
						int total = secondW - (firstW + weaksynonimText[0].length());
						if (total < 100)
							dist = true;

					} else {

						int total = firstW - (secondW + synonimContext.length());
						if (total < 100)
							dist = true;
					}

				}
				k++;
				if (k > 30) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dist;
	}

	int getPisitionB(String Str, String[] wordtoken) {

		// text:T|N - Т номер слова N грамма I количество слов вхождений
		// номер слова N + его длина оптередяют смещение
		// если это 0 то отдать сразу
		int myposition = -1000;
		try {
			String text = wordtoken[0];
			String TN = wordtoken[1];

			String[] objectidData = TN.split("\\|");

			String T = objectidData[0];
			String I = objectidData[2];

			if (I.equals("1")) {
				List<IndexWrapper> list = new ArrayList<>();
				list = findIndexesForKeyword(Str, text);
				try {
					if (list.size() > 0) {
						myposition = list.get(0).getStart();

						// myposition=Str.indexOf( text );
						return myposition;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				// int t=Integer.parseInt(T);
				int i = Integer.parseInt(I);
				if (i > 0)
					i = i - 1;
				List<IndexWrapper> list = new ArrayList<>();
				list = findIndexesForKeyword(Str, text);
				try {
					if (list.size() > 0) {

						myposition = findIndexesForKeyword(Str, text).get(i).getStart();

						// int last=0;
						// int lastgo=0;
						// //T это номер слова в строке == 22 а i это вхождение
						// 2-е например
						//
						// //int k=0; //считаем вхождения в строку
						//
						// //k=wordCountin.get(text);
						//
						// for(int k=1;k<=i;k++){
						//
						// //получаем индексы всех вхождений слова
						// last=Str.indexOf( text, lastgo );
						// lastgo=last+text.length();
						//
						//
						//
						// if(k>20)break;
						// }

						return myposition;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return myposition;
		}
		return myposition;
	}

	private List<Integer> getWordPisitionB(String text, String synonimContext) {
		List<Integer> list = new ArrayList<>();
		try {

			String regex = "\\b" + synonimContext + "\\b";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(text);
			int k = 0;
			while (matcher.find() == true) {

				int start = matcher.start();

				int myposition = start + synonimContext.length();
				list.add(myposition);

				k++;
				if (k > 10) {
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Override
	public JSONObject getVecRezult(String lowerStr) {
		JSONObject srtjson = new JSONObject();
		try {
			int iwordPosition = 1;
			WordVectors vecl = nameFinderModelMEDAO.getWordVectors();
			Collection<String> lst = vecl.wordsNearest(lowerStr, 10);

			for (String vec : lst) {

				srtjson.put("words" + iwordPosition, "" + clean(vec));
			}
		} catch (Exception e) {
		}
		return srtjson;
	}

	@Override
	public JSONObject getVariationRezult(String text, boolean ineededu, boolean getmalt, boolean min) {

		Map<Integer, String[]> out = new HashMap<Integer, String[]>();
		ProcessorResult returnresult = new ProcessorResult();
		returnresult.setSingltext(text);
		Map<String, Integer> allcategoryMap = new HashMap<String, Integer>();
		Map<String, Integer> allemmotionMap = new HashMap<String, Integer>();
		allemmotionMap.put("positive", 0);
		allemmotionMap.put("negative", 0);

		Map<String, Integer> wordPhraseCount = new HashMap<String, Integer>();
		Map<String, Integer> wordPhraseCountProperty = new HashMap<String, Integer>();
		Map<String, Integer> wordPhraseCountPredlod = new HashMap<String, Integer>();
		Map<String, Integer> wordHoroshoCount = new HashMap<String, Integer>();
		Map<String, Integer> wordPlohoCount = new HashMap<String, Integer>();
		Map<String, String> podlejasheeExclude = new HashMap<String, String>();
		podlejasheeExclude.put("из", "из");

		Map<String, String> razProtsent = new HashMap<String, String>();
		razProtsent.put("раз", "раз");
		razProtsent.put("процентный", "процентный");
		razProtsent.put("процентных", "процентных");
		razProtsent.put("процент", "процент");

		// Возможно перенести блок, т.к. инициализация выполняется для каждого
		// предложения
		// Тормоза
		LuceneMorphology luceneMorph = objs.getLuceneMorph();
		WordVectors vecl = nameFinderModelMEDAO.getWordVectors();

		Map<String, List> iilobjectsList = fastMemoryModel.getIilobjects();
		Map<String, List> iiltext = fastMemoryModel.getIilobjectsToText();
		Map<String, List> iiltype = fastMemoryModel.getIilpropertyOfType();
		Map<String, List> iilproperties = fastMemoryModel.getIilotherprops();
		Map<String, List> iilinstanceList = fastMemoryModel.getOfinstance();
		Map<String, List> iilsubclassofList = fastMemoryModel.getIilsubclassof();
		Map<String, List> iilmaincategory = fastMemoryModel.getMaincategory();
		Map<String, List> iilhaspart = fastMemoryModel.getIilhaspart();
		Map<String, Integer> triplet = fastMemoryModel.getNounverbdbTriplet();
		Map<String, String> iwordSynonims = fastMemoryModel.getWordSynonims();
		Map<String, String> iwordFraseologism = fastMemoryModel.getWordFraseologism();
		Map<String, String> igetWordExclude = fastMemoryModel.getWordExclude();
		Map<String, ArrayList> iSymantex = nameFinderModelMEDAO.getSymantexhashMap();
		Map<String, String> iwordPositive = fastMemoryModel.getWordPositive();
		Map<String, String> iwordNegative = fastMemoryModel.getWordNegative();
		Map<String, String> liniarmap = nameFinderModelMEDAO.getLiniarmap();
		Map<String, String> idcategorymap = nameFinderModelMEDAO.getIdcategorymapobj();
		Map<String, String> morfema = fastMemoryModel.getMorfema();
		Map<String, String> familii = nameFinderModelMEDAO.getFamilii();
		Map<String, String> morfemawords = fastMemoryModel.getMorfemaWords();
		Map<String, String> negpredlog = fastMemoryModel.getWordNegPreglog();
		Map<String, String> predlogMesta = fastMemoryModel.getWordPredlogMesta();
		Map<String, String> narechiyaVremeni = fastMemoryModel.getNarechiyaVremeni();
		Map<String, String> dniMeseats = fastMemoryModel.getDniMeseats();
		Map<String, String> politehwords = fastMemoryModel.getPolitehwords();
		Map<String, String> economicswords = fastMemoryModel.getEconomicswords();
		Map<String, String> metricsTypeOnel = fastMemoryModel.getMetricsTypeOne();
		Map<String, String> metricsTypeTwol = fastMemoryModel.getMetricsTypeTwo();
		Map<String, String> metricsTypeThreel = fastMemoryModel.getMetricsTypeThree();
		Map<String, String> wordKvantor = fastMemoryModel.getWordKvantor();
		Map<String, String> wordExplainOne = fastMemoryModel.getWordExplainOne();
		Map<String, String> wordExplainTwo = fastMemoryModel.getWordExplainTwo();
		Map<String, String> wordExplainThree = fastMemoryModel.getWordExplainThree();

		Map<String, String> nonamesingle = fastMemoryModel.getQnonamesingledb();
		Map<String, String> nonamephrase = fastMemoryModel.getQnonamephrasedbdb();

		Map<String, List<String>> qppropertiesvalue = fastMemoryModel.getIilQPKeyValue();
		Map<String, List> iilpropertiesList = fastMemoryModel.getIilproperties();

		// Тормоза

		Map<String, String> strongmed = new HashMap<String, String>();
		strongmed = nameFinderModelMEDAO.getLiniarmapmed();

		Map<String, String> weaksynonimMapmed = new HashMap<String, String>();
		weaksynonimMapmed = nameFinderModelMEDAO.getSynonimMapmed();

		Map<String, String> weakcontextMapmed = new HashMap<String, String>();
		weakcontextMapmed = nameFinderModelMEDAO.getContextMapmed();

		Map<String, Map<String, List>> listobjects = new HashMap<String, Map<String, List>>();

		// Map<String, String> explainMap= fastMemoryModel.getExplainPrimeri();

		// stnum++;
		JSONObject srtjson = new JSONObject();
		JSONArray wordlist = new JSONArray();
		// номер строки в документе
		// srtjson.put("number", ""+stnum);

		String singltext = text;
		// if(hasVoprosSymbol(singltext)){srtjson.put("isvopros", "true");}else
		// srtjson.put("isvopros", "false");

		// начальный текст
		srtjson.put("origtext", singltext);

		ArrayList<Integer> podlejashie = new ArrayList<Integer>();
		ArrayList<Integer> skazuemie = new ArrayList<Integer>();
		ArrayList<String> explainList = new ArrayList<String>();
		ArrayList<String> explainPropertyList = new ArrayList<String>();

		String dirty = singltext;
		String dirtyNoProbel = text;
		try {
			dirty = singltext.replaceAll("\\pP", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dirty = dirty.replaceAll("ё|Ё", "е");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			dirtyNoProbel = dirty.replaceAll("[\\s]{2,}", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}

		String tokens[] = dirty.toString().split("\\p{P}?[\\| \\t\\n\\r]+");
		List<String[]> alltokens = new ArrayList<String[]>();
		List<String[]> alltokenstrim = new ArrayList<String[]>();
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		int size = tokens.length;
		// длина предложения
		// srtjson.put("words", "" + size);

		String[] iilarrayL1 = new String[size];
		String[] nwordarrayL2 = new String[size];
		String[] morfoarrayL3 = new String[size];
		String[] emotionarrayL4 = new String[size];

		String[] emotionarrayL4OUT = new String[size];

		String[] fonemaarrayL5 = new String[size];
		String[] objectarrayL6 = new String[size];
		String[] familiiarrayL7 = new String[size];
		String[] politeharrayL8 = new String[size];
		String[] chislaarrayL9 = new String[size];
		String[] timeplaceL10 = new String[size];
		String[] kvantorL11 = new String[size];
		String[] metricsL12 = new String[size];
		String[] economicsarrayL15 = new String[size];

		ArrayList<String> cores = new ArrayList<String>();
		ArrayList<String> maincore = new ArrayList<String>();
		ArrayList<String> maincorelemma = new ArrayList<String>();

		List<IndexWrapper> probelTextCount = new ArrayList<>();
		Matcher matcher = Pattern.compile("[\\s]{2,}").matcher(dirty);
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();
			IndexWrapper p = new IndexWrapper(start, end);

			probelTextCount.add(p);

		}

		for (int i = 0; i < size; i++) {
			String[] indexsingle = new String[2];
			String[] indexbigram = new String[2];
			String[] indextrigram = new String[2];
			String[] indexfourgram = new String[2];
			String[] indexfivegram = new String[2];

			String single = null;
			String bigram = null;
			String trigram = null;
			String fourgram = null;
			String fivegram = null;

			single = tokens[i];
			indexsingle[0] = single;
			indexsingle[1] = i + "|1";

			if (wordCount.containsKey(single)) {

				int t = wordCount.get(single);

				indexsingle[1] = i + "|1|" + (t + 1);
				wordCount.put(single, t + 1);

			} else {
				wordCount.put(single, 1);
				indexsingle[1] = i + "|1|1";
			}

			if (i + 1 < size) {
				bigram = tokens[i] + " " + tokens[i + 1];
				indexbigram[0] = bigram;
				indexbigram[1] = i + "|2";
				if (wordCount.containsKey(bigram)) {

					int t = wordCount.get(bigram);
					indexbigram[1] = i + "|2|" + (t + 1);
					wordCount.put(bigram, t + 1);

				} else {
					wordCount.put(bigram, 1);
					indexbigram[1] = i + "|2|1";
				}

			}
			if (i + 2 < size) {
				trigram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
				indextrigram[0] = trigram;
				indextrigram[1] = i + "|3";

				if (wordCount.containsKey(trigram)) {

					int t = wordCount.get(trigram);
					indextrigram[1] = i + "|3|" + (t + 1);
					wordCount.put(trigram, t + 1);

				} else {
					wordCount.put(trigram, 1);
					indextrigram[1] = i + "|3|1";
				}

			}
			if (i + 3 < size) {
				fourgram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2] + " " + tokens[i + 3];
				indexfourgram[0] = fourgram;
				indexfourgram[1] = i + "|4";

				if (wordCount.containsKey(fourgram)) {

					int t = wordCount.get(fourgram);
					indexfourgram[1] = i + "|4|" + (t + 1);
					wordCount.put(fourgram, t + 1);

				} else {
					wordCount.put(fourgram, 1);
					indexfourgram[1] = i + "|4|1";
				}

			}
			if (i + 4 < size) {
				fivegram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2] + " " + tokens[i + 3] + " "
						+ tokens[i + 4];
				indexfivegram[0] = fivegram;
				indexfivegram[1] = i + "|5";

				if (wordCount.containsKey(fivegram)) {

					int t = wordCount.get(fivegram);
					indexfivegram[1] = i + "|5|" + (t + 1);
					wordCount.put(fivegram, t + 1);

				} else {
					wordCount.put(fivegram, 1);
					indexfivegram[1] = i + "|5|1";
				}

			}

			if (single != null)
				alltokens.add(indexsingle);
			if (bigram != null)
				alltokens.add(indexbigram);
			if (trigram != null)
				alltokens.add(indextrigram);
			if (fourgram != null)
				alltokens.add(indexfourgram);
			if (fivegram != null)
				alltokens.add(indexfivegram);

			if (i > 15000) {
				break;
			}
		}

		// Возможны торможения
		for (String[] wordtokend : alltokens) {
			String[] index = new String[2];
			index[0] = wordtokend[0].trim();
			index[1] = wordtokend[1];
			alltokenstrim.add(index);
		}
		alltokens = alltokenstrim;

		Map<Integer, List<String>> mas = new HashMap<Integer, List<String>>();

		for (String[] wordtoken : alltokens) {

			Map<String, Integer> mapWords = new HashMap<>();
			Map<String, Integer> mapWordsResult = new HashMap<>();

			String lowerStr = wordtoken[0].toLowerCase();
			String tokenleght = "";
			String wordPosition = "";
			int iwordPosition = 0;
			int itokenleght = 0;
			// wordtoken[1] метрика слова или фразы "1|9|1" позиция
			// начала / количество слов \ повторяемость
			if (wordtoken[1].contains("|")) {
				String[] myData = wordtoken[1].split("\\|");
				tokenleght = myData[1];
				wordPosition = myData[0];
				iwordPosition = Integer.parseInt(wordPosition);

				if (tokenleght.equals("1")) {

					// if (min) {
					// try {
					// List<String> wordBaseForms =
					// luceneMorph.getNormalForms(lowerStr);
					//
					// for (String nword : wordBaseForms) {
					// // искомое слово начальной формы
					// try {
					// Map<String, Integer> nwordmap = getWordVariation(nword,
					// iwordPosition, morfema,
					// morfemawords, luceneMorph, iwordSynonims, iSymantex,
					// iilobjectsList,
					// iiltext);
					//
					// for (Map.Entry<String, Integer> entry :
					// nwordmap.entrySet()) {
					// String wword = entry.getKey();
					//
					// //mapWords.put(wword, iwordPosition);
					// mapWordsResult.put(wword, iwordPosition);
					// }
					//
					// /*
					// * try{Collection<String> lst =
					// * vecl.wordsNearest(nword, 5); for(String
					// * vec:lst){
					// *
					// * mapWords.put(clean(vec), iwordPosition);
					// * }} catch (Exception e) {}
					// */
					//
					// } catch (Exception e) {
					// }
					// }
					//
					// } catch (Exception e) {
					// }
					//
					//
					// } else {
					// искомое слово без начальной формы
					try {
						Map<String, Integer> wordmap = getWordVariation(lowerStr, iwordPosition, morfema, morfemawords,
								luceneMorph, iwordSynonims, iSymantex, iilobjectsList, iiltext);

						for (Map.Entry<String, Integer> entry : wordmap.entrySet()) {
							String wword = entry.getKey();

							mapWords.put(wword, iwordPosition);

						}

						try {
							Collection<String> lst = vecl.wordsNearest(lowerStr, 10);
							for (String vec : lst) {

								mapWords.put(clean(vec), iwordPosition);
							}
						} catch (Exception e) {
						}

					} catch (Exception e) {
					}

					try {
						List<String> wordBaseForms = luceneMorph.getNormalForms(lowerStr);

						for (String nword : wordBaseForms) {
							// искомое слово начальной формы
							try {
								Map<String, Integer> nwordmap = getWordVariation(nword, iwordPosition, morfema,
										morfemawords, luceneMorph, iwordSynonims, iSymantex, iilobjectsList, iiltext);

								for (Map.Entry<String, Integer> entry : nwordmap.entrySet()) {
									String wword = entry.getKey();

									mapWords.put(wword, iwordPosition);

								}

								/*
								 * try{Collection<String> lst =
								 * vecl.wordsNearest(nword, 5); for(String
								 * vec:lst){
								 * 
								 * mapWords.put(clean(vec), iwordPosition); }}
								 * catch (Exception e) {}
								 */

							} catch (Exception e) {
							}
						}

					} catch (Exception e) {
					}

					try {
						// все слова без начальной формы
						for (Map.Entry<String, Integer> entry : mapWords.entrySet()) {
							mapWordsResult.put(entry.getKey(), iwordPosition);
							try {
								Map<String, Integer> wordmaps = getWordVariation(entry.getKey(), iwordPosition, morfema,
										morfemawords, luceneMorph, iwordSynonims, iSymantex, iilobjectsList, iiltext);

								for (Map.Entry<String, Integer> entrys : wordmaps.entrySet()) {
									String wword = entrys.getKey();

									mapWordsResult.put(wword, iwordPosition);

								}

							} catch (Exception e) {
							}
						}
					} catch (Exception e) {
					}

					try {
						// все слова начальной формы
						for (Map.Entry<String, Integer> entry : mapWords.entrySet()) {
							mapWordsResult.put(entry.getKey(), iwordPosition);
							try {
								List<String> wordBaseFormsMap = luceneMorph.getNormalForms(entry.getKey());

								for (String nword : wordBaseFormsMap) {

									Map<String, Integer> nwordmap = getWordVariation(nword, iwordPosition, morfema,
											morfemawords, luceneMorph, iwordSynonims, iSymantex, iilobjectsList,
											iiltext);

									for (Map.Entry<String, Integer> entrys : nwordmap.entrySet()) {
										String wword = entrys.getKey();

										mapWordsResult.put(wword, iwordPosition);

									}

								}
							} catch (Exception e) {
							}

						}

					} catch (Exception e) {
					}

				}

				ArrayList<String> coresc = new ArrayList<String>();
				for (Map.Entry<String, Integer> entry : mapWordsResult.entrySet()) {
					coresc.add(entry.getKey());
				}
				mas.put(iwordPosition, coresc);
			}

		}

		// }

		for (int i = 0; i < size; i++) {
			try {
				List<String> element = mas.get(i);
				int j = 0;
				for (String word : element) {
					srtjson.put("words" + i + "L" + j, "" + word);
					j++;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/*
		 * String[][] graph = new String[size][10000];
		 * 
		 * for (int i = 0; i < size; i++) { List<String> element = mas.get(i);
		 * for(String word:element){ for (int j = i+1; j < size; j++) {
		 * List<String> nextelement = mas.get(j); for(String dword:nextelement){
		 * graph[i][j] = dword; } } } }
		 */

		return srtjson;
	}

	public Map<String, Integer> getWordVariation(String nword, int iwordPosition, Map<String, String> morfema,
			Map<String, String> morfemawords, LuceneMorphology luceneMorph, Map<String, String> iwordSynonims,
			Map<String, ArrayList> iSymantex, Map<String, List> iilobjectsList, Map<String, List> iiltext) {
		Map<String, Integer> wordMap = new HashMap<String, Integer>();

		// Начальная форма слова и морфология ?

		boolean[] rule = new boolean[2];
		rule[0] = false;
		rule[1] = false;

		// --------------------------MORFEMA----------------------------

		// Пожарная команда для морфем в виде????
		// синонимов, семантик, Q объектов

		boolean foundtonality = false;
		if (morfema.containsKey(nword)) {
			List<String> morfemaRes = getWordMorphema(morfema, morfemawords, luceneMorph, nword);
			for (String lword : morfemaRes) {
				wordMap.put(lword, 1);
			}

		}
		// поиск в синонимах
		if (!foundtonality) {
			// get sin
			// это не оригональное слово
			// lowerStr
			try {

				String syn = iwordSynonims.get(nword);
				if (syn != null) {
					String[] newwords;
					if (syn.contains("/")) {

						newwords = syn.split("\\/");

					}
					if (syn.contains(",")) {

						newwords = syn.split(",");

					} else {

						String[] synindex = new String[1];
						synindex[0] = syn;
						newwords = synindex;
					}
					int siz = newwords.length;

					for (int ik = 0; ik < siz; ik++) {
						// Если найдено значение
						// - выйти из цикла
						try {
							String iword = newwords[ik].trim();
							if (iword != null)
								if (!iword.equals(""))
									wordMap.put(iword, 2);
							List<String> morfemaRes = getWordMorphema(morfema, morfemawords, luceneMorph, iword);
							for (String lword : morfemaRes) {
								wordMap.put(lword, 2);
							}

						} catch (Exception e) {
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// поиск тональности в семантик
		if (!foundtonality) {
			// это не оригональное слово
			// lowerStr

			try {
				ArrayList tempdoList = null;
				tempdoList = iSymantex.get(nword);
				if (tempdoList != null) {
					int j = 0;
					for (Object value : tempdoList) {
						j++;
						String[] index = new String[2];
						index = (String[]) value;

						String iSymantexword = index[0];
						String itype = index[1];
						// wordjson.put("isymantex"+j,
						// "word="+iSymantexword);
						String[] morfjson = new String[3];
						iSymantexword = iSymantexword.trim();
						if (!itype.equals("antonyms"))
							try {
								String iword = iSymantexword;
								if (iword != null)
									if (!iword.equals(""))
										wordMap.put(iword, 3);
								List<String> morfemaRes = getWordMorphema(morfema, morfemawords, luceneMorph, iword);
								for (String lword : morfemaRes) {
									wordMap.put(lword, 3);
								}

							} catch (Exception e) {
							}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// поиск тональности Q объектов
		if (!foundtonality) {

			try {
				List<String> qindexList = iilobjectsList.get(nword);
				int j = 0;
				if (qindexList != null)
					for (String qindex : qindexList) {

						if (qindex != null) {
							j++;

							String words = getTextofQ(qindex, iiltext);
							try {
								List<String> list = iiltext.get(qindex);
								StringBuffer sb = new StringBuffer();
								for (String qword : list) {
									try {
										String iword = qword;
										if (iword != null)
											if (!iword.equals(""))
												wordMap.put(iword, 3);
										List<String> morfemaRes = getWordMorphema(morfema, morfemawords, luceneMorph,
												iword);
										for (String lword : morfemaRes) {
											wordMap.put(lword, 4);
										}

									} catch (Exception e) {
									}
								}

							} catch (Exception e) {
							}

						}

					}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return wordMap;

	}

	public List<String> getWordMorphema(Map<String, String> morfema, Map<String, String> morfemawords,
			LuceneMorphology luceneMorph, String nword) {
		List<String> listnword = new ArrayList<String>();

		if (true)
			return listnword;

		String m = morfema.get(nword);
		if (m != null) {

			if (m.contains("|")) {
				String[] mdata = m.split("\\|");
				int sizeb = mdata.length;
				for (int i = 0; i < sizeb; i++) {
					String single = mdata[i];

					try {
						String same = morfemawords.get(single);

						if (same != null) {

							if (same.contains("|")) {
								String[] msamedata = same.split("\\|");
								int sizec = msamedata.length;
								String wordform = "";
								for (int j = 0; j < sizec; j++) {
									String singleword = msamedata[j].trim().toLowerCase();

									String wrdi = singleword.replaceAll("[^А-я]", "").trim();

									List<String> wordBaseForms = null;
									if (!"".equals(wrdi)) {
										wordBaseForms = luceneMorph.getMorphInfo(wrdi);
									}
									int sizer = 0;
									try {
										sizer = wordBaseForms.size();
									} catch (Exception e) {
									}

									if (sizer > 0) {

										StringBuilder sb = new StringBuilder();
										for (String s : wordBaseForms) {
											sb.append(s);
											sb.append(" #");
										}

										String ist = sb.toString();
										if (!ist.equals("") && ist != null) {
											wordform = ist;
										}

										if (wordform.contains("|")) {
											String[] myData = wordform.split("\\|");
											String inword = myData[0].trim();
											String imorf = myData[1];
											// boolean iissush =
											// analysis.sushRule(imorf);
											// if (iissush) {

											listnword.add(inword);
											// }
										}
									}
								}

							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			} else {

				try {
					String same = morfemawords.get(m);

					if (same != null) {
						if (same.contains("|")) {
							String[] msamedata = same.split("\\|");
							int sizec = msamedata.length;
							String wordform = "";
							for (int i = 0; i < sizec; i++) {
								String singleword = msamedata[i].trim().toLowerCase();

								String wrdi = singleword.replaceAll("[^А-я]", "").trim();

								List<String> wordBaseForms = null;
								if (!"".equals(wrdi)) {
									wordBaseForms = luceneMorph.getMorphInfo(wrdi);
								}

								int sizer = 0;
								try {
									sizer = wordBaseForms.size();
								} catch (Exception e) {
								}

								if (sizer > 0) {

									StringBuilder sb = new StringBuilder();
									for (String s : wordBaseForms) {
										sb.append(s);
										sb.append(" #");
									}

									String ist = sb.toString();
									if (!ist.equals("") && ist != null) {
										wordform = ist;
									}

									if (wordform.contains("|")) {
										String[] myData = wordform.split("\\|");
										String inword = myData[0].trim();
										String imorf = myData[1];
										// boolean iissush =
										// analysis.sushRule(imorf);
										// if (iissush) {
										listnword.add(inword);
										// }
									}
								}
							}

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		return listnword;
	}

	@Override
	public JSONObject createNewResult(ProcessorResult rin, boolean ineededu, boolean getrezult,
			Map<Integer, String> predicates) {

		ineededu = false;

		Map<Integer, String[]> in = rin.getData();
		String singltext = rin.getSingltext();
		JSONArray wordlist = rin.getWordlist();

		Map<String, Integer> adjectiveList = rin.getAdjective();

		Map<String, String> idcategorymap = nameFinderModelMEDAO.getIdcategorymapobj();
		Map<String, Map<String, String>> objectMap = new HashMap<String, Map<String, String>>();
		Map<String, String> iwordPositiveHuman = fastMemoryModel.getWordPositiveHuman();
		Map<String, String> iwordNegativeHuman = fastMemoryModel.getWordNegativeHuman();

		Map<String, String> negpredlog = fastMemoryModel.getWordNegPreglog();
		Map<String, Integer> inounverbdb = fastMemoryModel.getNounverbdb();
		Map<String, Integer> ilemmanounverbdb = fastMemoryModel.getLemmanounverbdb();
		Map<String, Integer> triplet = fastMemoryModel.getNounverbdbTriplet();

		Map<String, String> explainMap = fastMemoryModel.getExplainPrimeri();
		Map<String, String> predlogMesta = fastMemoryModel.getWordPredlogMesta();
		Map<String, Integer> wordPhraseCount = fastMemoryModel.getObjectAndVerbCountTriplet();
		Map<String, Integer> wordPhraseCountPredlod = fastMemoryModel.getObjectAndVerbCountTripletPredlog();
		
		Map<String, List> adjectiveNoun =nameFinderModelMEDAO.getadjectiveNounMap();
		
		JSONObject srtjson = new JSONObject();
		String[] tokens = null;
		try {
			tokens = in.get(0);
		} catch (Exception e) {
		}
		if (tokens != null) {
			int size = tokens.length;
			srtjson.put("words", "" + size);
			srtjson.put("wordlist", wordlist);
			String[] iilarrayL1 = new String[size];
			String[] nwordarrayL2 = new String[size];
			String[] morfoarrayL3 = new String[size];
			String[] emotionarrayL4 = new String[size];
			String[] emotionarrayL4OUT = new String[size];
			String[] fonemaarrayL5 = new String[size];
			String[] objectarrayL6 = new String[size];
			String[] familiiarrayL7 = new String[size];
			String[] politeharrayL8 = new String[size];
			String[] chislaarrayL9 = new String[size];
			String[] timeplaceL10 = new String[size];
			String[] kvantorL11 = new String[size];
			String[] metricsL12 = new String[size];
			String[] economicsarrayL15 = new String[size];
			String[] podarrayL16;
			String[] scaarrayL17;
			String[] exparrayL18;

			iilarrayL1 = in.get(1);
			nwordarrayL2 = in.get(2);
			morfoarrayL3 = in.get(3);
			emotionarrayL4 = in.get(4);
			emotionarrayL4OUT = in.get(41);
			fonemaarrayL5 = in.get(5);
			objectarrayL6 = in.get(6);
			familiiarrayL7 = in.get(7);
			politeharrayL8 = in.get(8);
			chislaarrayL9 = in.get(9);
			timeplaceL10 = in.get(10);
			kvantorL11 = in.get(11);
			metricsL12 = in.get(12);
			economicsarrayL15 = in.get(15);
			// podarrayL16 = in.get(16);
			scaarrayL17 = in.get(17);
			exparrayL18 = in.get(18);

			// int podsize = podarrayL16.length;
			int scasize = scaarrayL17.length;
			int iexplain = exparrayL18.length;

			Integer[] scaarray = new Integer[scasize];

			// расчет появления подлежащего и сказуемого по глагольному ряду в
			// этом предолжении

			// создаем массив возможных пар

			JSONArray core = new JSONArray();

			ArrayList<String> objs = new ArrayList<String>();
			Map<String, Integer[]> resmap = new HashMap<String, Integer[]>();

			// ---------------------------------BEGIN----------------------------------
			Map<Integer, String> predicateseExcluded = new HashMap<Integer, String>(predicates);
//			for (Entry<Integer, String> entryradio : predicates.entrySet()) {
//				
//				System.out.println("A==="+nwordarrayL2[entryradio.getKey()]);
//				
//			}
			ArrayList<Integer> podlejashieList = rin.getPodlejashie();
			ArrayList<Integer> skazuemieList = rin.getSkazuemie();
			ArrayList<Integer> deeprichastieList = rin.getDeeprichastie();
			
			for (Integer skaz : deeprichastieList) {
				System.out.println("B2===");
				System.out.println("B2===");
				System.out.println("B2===");
				System.out.println("B2===");
				String skazuem = tokens[skaz].toLowerCase().trim();
				System.out.println("B2==="+nwordarrayL2[skaz]+" "+skazuem); 
			}
			ArrayList<Integer> skazuemieAll= new ArrayList<Integer>();
			skazuemieAll.addAll(skazuemieList);
			skazuemieAll.addAll(deeprichastieList);
			
			
					
			if (predicateseExcluded.size() > 0) {

				for (int i = 0; i < podlejashieList.size(); i++) {

					int pod = podlejashieList.get(i);
					if (predicateseExcluded.containsKey(pod))
						predicateseExcluded.remove(pod);

				}
			}

			if (predicateseExcluded.size() > 0) {
				for (Entry<Integer, String> entryP : predicateseExcluded.entrySet()) {
					podlejashieList.add(entryP.getKey());
				}
			}

		
			
			// ------------------------------------END----------------------------------
			scasize = skazuemieAll.size();
			for (int i = 0; i < podlejashieList.size(); i++) {
				// for(Integer pod:podlejashie){//pod=0 pod=7
				int pod = podlejashieList.get(i);

				for (int j = 0; j < scasize; j++) {
					// for(Integer skaz:skazuemie){//skaz=2 skaz=6
					//int skaz = Integer.parseInt(scaarrayL17[j]);
					int skaz = skazuemieAll.get(j);
					// 0-2 0-6
					// 7-6 7-6
					Integer[] inf = new Integer[3];
					inf[0] = pod;
					inf[1] = skaz;

					String podlej = tokens[pod].toLowerCase().trim();
					String skazuem = tokens[skaz].toLowerCase().trim();
					// может мочь быть

					// будет, был не добавлен
					// отрицание
					int ihasNo = 0;

					try {
						if (negpredlog.containsKey(nwordarrayL2[skaz - 1]))
							ihasNo = 1;
					} catch (Exception e) {
					}

					if (nwordarrayL2[skaz].equals("может") || nwordarrayL2[skaz].equals("мочь")
							|| nwordarrayL2[skaz].equals("быть") || nwordarrayL2[skaz].equals("хотеть")
							|| nwordarrayL2[skaz].equals("хочу")) {
						try {
							if (negpredlog.containsKey(nwordarrayL2[skaz - 1]))
								ihasNo = 1;
						} catch (Exception e) {
						}

						if (skaz >= 0 && skaz <= size) {
							boolean ex = false;
							// --->
							int lim = 3;
							for (int s = skaz + 1; s < skaz + lim; s++) {
								if (s < 0)
									break;
								if (s >= size)
									break;
								// System.out.println("===================="+morfoarrayL3[s]);
								// System.out.println("===================="+nwordarrayL2[s]);

								if (isPresent("Г", morfoarrayL3[s]) || isPresent("ИНФИНИТИВ", morfoarrayL3[s])) {
									if (nwordarrayL2[s].equals("может") || nwordarrayL2[s].equals("мочь")
											|| nwordarrayL2[s].equals("быть") || nwordarrayL2[skaz].equals("хотеть")
											|| nwordarrayL2[skaz].equals("хочу")) {
										try {
											if (negpredlog.containsKey(nwordarrayL2[s - 1]))
												ihasNo = 1;
										} catch (Exception e) {
										}
									} else {

										try {
											if (negpredlog.containsKey(nwordarrayL2[s - 1]))
												ihasNo = 1;
										} catch (Exception e) {
										}
										skaz = s;
										ex = true;
										break;
									}
								}
							}

							// <---
							if (!ex)
								for (int s = skaz - 1; s > skaz - lim; s--) {
									if (s < 0)
										break;
									if (s >= size)
										break;
									if (isPresent("Г", morfoarrayL3[s]) || isPresent("ИНФИНИТИВ", morfoarrayL3[s])) {
										if (nwordarrayL2[s].equals("может") || nwordarrayL2[s].equals("мочь")
												|| nwordarrayL2[s].equals("быть") || nwordarrayL2[skaz].equals("хотеть")
												|| nwordarrayL2[skaz].equals("хочу")) {
											try {
												if (negpredlog.containsKey(nwordarrayL2[s - 1]))
													ihasNo = 1;
											} catch (Exception e) {
											}
										} else {

											try {
												if (negpredlog.containsKey(nwordarrayL2[s - 1]))
													ihasNo = 1;
											} catch (Exception e) {
											}
											skaz = s;
											ex = true;
											break;
										}
									}

								}

						}
					}
					String key = "" + pod + "" + skaz;
					inf[0] = pod;
					inf[1] = skaz;
					inf[2] = ihasNo;
					resmap.put(key, inf);
				}

			}

			// найти ближайшее Б для анализа отношений и свойств
			// maincorelemma maincore

			Map<Integer[], Integer> unsortMapI = new HashMap<Integer[], Integer>();
			Map<Integer[], Integer> unsortMaplemmaI = new HashMap<Integer[], Integer>();
			Map<String, Integer> unsortMapIS = new HashMap<String, Integer>();
			Map<String, Integer> unsortMaplemmaIS = new HashMap<String, Integer>();

			for (Map.Entry<String, Integer[]> entry : resmap.entrySet()) {
				Integer[] inf = new Integer[3];

				inf = entry.getValue();

				int pod = inf[0];
				int skaz = inf[1];
				int ihasNo = inf[2];
				String wordkey = null;
				String lemmakey = null;
				int totilemma = -2;
				int toti = -1;

				String podlej = tokens[pod].toLowerCase().trim();
				String skazuem = tokens[skaz].toLowerCase().trim();

				try {
					wordkey = podlej + " " + skazuem;
				} catch (Exception e) {
				}

				try {
					lemmakey = nwordarrayL2[pod] + " " + nwordarrayL2[skaz];
				} catch (Exception e) {
				}

				try {
					if (wordkey != null)
						toti = inounverbdb.get(wordkey);
				} catch (Exception e) {
				}
				try {
					if (lemmakey != null)
						totilemma = ilemmanounverbdb.get(lemmakey);
				} catch (Exception e) {
				}
				unsortMapI.put(inf, toti);
				unsortMaplemmaI.put(inf, totilemma);

				unsortMapIS.put(pod + "" + skaz, toti);
				unsortMaplemmaIS.put(pod + "" + skaz, totilemma);
			}

			Map<Integer[], Integer> sortedd = sortByValueI(unsortMapI);
			Map<Integer[], Integer> sorteddlemmaI = sortByValueI(unsortMaplemmaI);

			// --------------------------Получены пары значений, они
			// отсортированы Integer их величина-------------------
			// --------------------------Обработка правил выбора
			// ------------------------------------
			Map<Integer[], Integer> sorted = getNewPodSkazRules(sortedd, unsortMapIS, sorteddlemmaI, unsortMaplemmaIS,
					getrezult, scaarrayL17, nwordarrayL2, predicates);
			// Map<Integer[], Integer> sorted =
			// getPodSkazRules(sorteddlemmaI,sortedd, getrezult, scaarrayL17);

			// ====================================================EDU=-============================================
			// выбираем ТОЛЬКО первое значение массива!
			/*
			 * int podexplain = 0; int skazexplain = 0; int lk = 1;
			 * 
			 * for (Map.Entry<Integer[], Integer> entry : sorted.entrySet()) {
			 * Integer[] inf = new Integer[3];
			 * 
			 * inf = entry.getKey(); int structype=0; structype=inf[0];
			 * 
			 * 
			 * int pod = inf[0]; int skaz = inf[1]; int ihasNo = inf[2];
			 * 
			 * // A podexplain = pod; // V skazexplain = skaz; // B // if
			 * (isPresent("((С (жр|мр|ср))|(П (жр|мр|ср))", // morfoarrayL3[p]))
			 * lk++; if (lk > 1) break;
			 * 
			 * }
			 * 
			 * 
			 * 
			 * if (ineededu) { int g = skazexplain + 1; if (g < size) { if
			 * (morfoarrayL3[skazexplain + 1] != null) if
			 * (isPresent("С (жр|мр|ср),(ед|мн)", morfoarrayL3[skazexplain +
			 * 1])) {
			 * 
			 * String firstphraseverb = ""; String secondphraseverb = ""; String
			 * threedphraseverb = "";
			 * 
			 * firstphraseverb = nwordarrayL2[podexplain]; secondphraseverb =
			 * nwordarrayL2[skazexplain]; threedphraseverb =
			 * nwordarrayL2[skazexplain + 1];
			 * 
			 * if (firstphraseverb != null && threedphraseverb != null) { if
			 * (!firstphraseverb.equals(threedphraseverb)) { if
			 * (wordPhraseCount.containsKey( firstphraseverb + " " +
			 * secondphraseverb + " " + threedphraseverb)) { int cont =
			 * wordPhraseCount .get(firstphraseverb + " " + secondphraseverb +
			 * " " + threedphraseverb); wordPhraseCount.put( firstphraseverb +
			 * " " + secondphraseverb + " " + threedphraseverb, cont + 1); }
			 * else wordPhraseCount.put( firstphraseverb + " " +
			 * secondphraseverb + " " + threedphraseverb, 1); } } }
			 * 
			 * if (tokens[skazexplain + 1] != null) if
			 * (predlogMesta.containsKey(tokens[skazexplain + 1])) { if
			 * (skazexplain + 2 <= size) { String morf = ""; try { morf =
			 * morfoarrayL3[skazexplain + 2]; } catch (Exception e) { morf =
			 * null; } if (morf != null) {
			 * 
			 * if (isPresent("С (жр|мр|ср),(ед|мн)", morfoarrayL3[skazexplain +
			 * 2])) {
			 * 
			 * String firstphraseverb = ""; String secondphraseverb = ""; String
			 * threedphraseverb = "";
			 * 
			 * firstphraseverb = nwordarrayL2[podexplain]; secondphraseverb =
			 * nwordarrayL2[skazexplain]; threedphraseverb =
			 * nwordarrayL2[skazexplain + 2];
			 * 
			 * if (firstphraseverb != null && threedphraseverb != null) if
			 * (!firstphraseverb.equals(threedphraseverb)) { if
			 * (wordPhraseCountPredlod.containsKey(firstphraseverb + " " +
			 * secondphraseverb + " " + threedphraseverb)) { int cont =
			 * wordPhraseCountPredlod.get(firstphraseverb + " " +
			 * secondphraseverb + " " + threedphraseverb);
			 * wordPhraseCountPredlod.put(firstphraseverb + " " +
			 * secondphraseverb + " " + threedphraseverb, cont + 1); } else
			 * wordPhraseCountPredlod.put(firstphraseverb + " " +
			 * secondphraseverb + " " + threedphraseverb, 1); } }
			 * 
			 * } } }
			 * 
			 * } }
			 * 
			 * for (int x = 0; x < iexplain; x++) {
			 * 
			 * String explainData = exparrayL18[x];
			 * 
			 * try {
			 * 
			 * if (explainData != null) { if (explainData.contains("#")) {
			 * String[] myData = explainData.split("#");
			 * 
			 * String type = myData[0].trim(); String oneormorewords =
			 * myData[1]; String posend = myData[2]; // это - было выше //
			 * существительные // глаголы // и прилагательные и сказуемые
			 * 
			 * try { if (type.equals("1") || type.equals("4")) { // запрещается
			 * String nounname = ""; String verbname = ""; String nounnamet =
			 * ""; String exptext = singltext;
			 * 
			 * int iwordPosition = Integer.parseInt(posend);
			 * 
			 * String exptype = type; if (podexplain > 0) { nounname =
			 * nwordarrayL2[podexplain]; } if (skazexplain > 0) { try { verbname
			 * = nwordarrayL2[skazexplain]; if (skazexplain > podexplain) if
			 * (size >= skazexplain + 2) if (isPresent("ИНФИНИТИВ дст",
			 * morfoarrayL3[skazexplain + 1])) { verbname =
			 * nwordarrayL2[skazexplain + 1]; if (size >= skazexplain + 2) if
			 * (isPresent("(С (жр|мр|ср))|( П (жр|мр|ср))",
			 * morfoarrayL3[skazexplain + 2])) { nounnamet =
			 * nwordarrayL2[skazexplain + 2]; } } } catch (Exception e) { }
			 * 
			 * } nounname = nounname.replaceAll("[^А-я]", "").trim(); verbname =
			 * verbname.replaceAll("[^А-я]", "").trim(); nounnamet =
			 * nounnamet.replaceAll("[^А-я]", "").trim(); String expdatat =
			 * nounname + "#" + verbname + "#" + nounnamet + "#" + exptype + "#"
			 * + UUID.randomUUID();
			 * 
			 * if (!nounname.equals("") && !verbname.equals(""))
			 * explainMap.put(expdatat, exptype + "#" + exptext);
			 * 
			 * 
			 * if (size >= iwordPosition + 2) if (isPresent("ИНФИНИТИВ дст",
			 * morfoarrayL3[iwordPosition + 1])) { verbname =
			 * nwordarrayL2[iwordPosition + 1]; if (size >= iwordPosition + 2) {
			 * String morf = ""; try { morf = morfoarrayL3[iwordPosition + 2]; }
			 * catch (Exception e) { } if
			 * (isPresent("(С (жр|мр|ср))|( П (жр|мр|ср))", morf)) { nounnamet =
			 * nwordarrayL2[iwordPosition + 2]; } } } nounname =
			 * nounname.replaceAll("[^А-я]", "").trim(); verbname =
			 * verbname.replaceAll("[^А-я]", "").trim(); nounnamet =
			 * nounnamet.replaceAll("[^А-я]", "").trim(); expdatat = nounname +
			 * "#" + verbname + "#" + nounnamet + "#" + exptype + "#" +
			 * UUID.randomUUID();
			 * 
			 * if (!nounnamet.equals("")) explainMap.put(expdatat, exptype + "#"
			 * + exptext); } } catch (Exception e) { e.printStackTrace(); } try
			 * { if (type.equals("2")) { // хорошо
			 * 
			 * int iwordPosition = Integer.parseInt(posend); int beforeEto =
			 * iwordPosition - 2; // int beforeEto=iwordPosition-2; // if //
			 * (nwordarrayL2[iwordPosition-1].equals("это")) // { // if
			 * (beforeEto >= 0) { String word = tokens[beforeEto];
			 * explainMap.put(word, "2#2"); } // }
			 * 
			 * } } catch (Exception e) { e.printStackTrace(); } try { if
			 * (type.equals("3")) { // плохо int iwordPosition =
			 * Integer.parseInt(posend); int beforeEto = iwordPosition - 2; //
			 * int beforeEto=iwordPosition-2; // if //
			 * (nwordarrayL2[iwordPosition-1].equals("это")) // { if (beforeEto
			 * >= 0) { String word = tokens[beforeEto]; explainMap.put(word,
			 * "3#3"); } // }
			 * 
			 * } } catch (Exception e) { e.printStackTrace(); } // else
			 * if(type.equals("4")){ // //разрешается // int iwordPosition
			 * =Integer.parseInt(posend); // int beforeEto=iwordPosition-2; //
			 * String word=tokens[beforeEto]; // explainMap.put(word, "4"); // }
			 * try { if (type.equals("5")) { // потому что String nounname = "";
			 * String verbname = ""; String nounnamet = ""; String exptext =
			 * singltext; String exptype = type;
			 * 
			 * int iwordPosition = Integer.parseInt(posend); int beforeEto =
			 * iwordPosition - 1; if (beforeEto >= 0)// { nounname =
			 * tokens[beforeEto]; nounname = nounname.replaceAll("[^А-я]",
			 * "").trim(); verbname = verbname.replaceAll("[^А-я]", "").trim();
			 * nounnamet = nounnamet.replaceAll("[^А-я]", "").trim(); String
			 * expdatat = nounname + "#" + verbname + "#" + nounnamet + "#" +
			 * exptype + "#" + UUID.randomUUID(); explainMap.put(expdatat, "5" +
			 * "#" + exptext); // } } } catch (Exception e) {
			 * e.printStackTrace(); }
			 * 
			 * } } } catch (Exception e) { e.printStackTrace(); } }
			 */
			// ====================================================EDU===END========================================================

			int j = 1;

			// ====================================================CREATE JSON
			// OUT========================================================

			JSONArray maincorearray = new JSONArray();
			// aMax
			Map<String, Integer[]> sortresult = new HashMap<String, Integer[]>();
			Map<Integer, Integer> verbPredicatNoun = new HashMap<Integer, Integer>();
			Map<String, Integer[]> radioOne = new HashMap<String, Integer[]>();

			Map<String, Integer[]> noAlternatives = new HashMap<String, Integer[]>();

			for (Map.Entry<Integer[], Integer> entry : sorted.entrySet()) {

				Integer[] inf = new Integer[11];
				inf = entry.getKey();
				int podvalue = inf[1];
				int skazvalue = inf[2];
				int total = inf[4];
				int quantity = inf[5];
				int structype = 0;
				structype = inf[0];

				// Какой из них лучший?
				// Прошлое будущее настоящее
				// Заменить отрицание антонимами(если сущесвуют)

				// у меня будет болеть голова если не пить витамины?
				// total>100
				// total>1000
				// total>10000
				// Выбрать 5 лучших и после запросить для них жсон
				// предикат total quantity параметры

				// bad-good pod/skaz
				/*
				 * if(predicates.containsKey(podvalue)){
				 * 
				 * 
				 * if(quantity<4){ if(total>0){ //good
				 * maincorearray.add(getAV(tokens, singltext,inf,1));
				 * 
				 * if(structype==1){ System.out.println(structype+"PRED pod=" +
				 * podvalue + " " + inf[2] + " total:" + total + " quantity: " +
				 * quantity + " " + nwordarrayL2[podvalue] + " " +
				 * nwordarrayL2[inf[2]]+" "+nwordarrayL2[inf[7]] + " total:"
				 * +inf[9]); }else System.out.println(structype+"PRED pod=" +
				 * podvalue + " " + inf[2] + " total:" + total + " quantity: " +
				 * quantity + " " + nwordarrayL2[podvalue] + " " +
				 * nwordarrayL2[inf[2]]);
				 * 
				 * 
				 * 
				 * if(structype==2){ maincorearray.add(getAV(tokens,
				 * singltext,inf,2));
				 * 
				 * System.out.println("PRED podecond=" + inf[6] + " " + inf[7] +
				 * " total:" + inf[9] + " quantity: " + inf[10] + " " +
				 * nwordarrayL2[inf[6]] + " " + nwordarrayL2[inf[7]]);} }else{
				 * //not bad
				 * 
				 * 
				 * //данный глагол уже задействован ? с лучшйми показателями
				 * quantity и total //да - игнорировать
				 * 
				 * //нет - проверить существительное уже задействовано с лучшйми
				 * показателями quantity и total //добавить в коллекцию } }
				 * }else{ //min quantity //a max total if(quantity<4){
				 * if(total>0){ //good maincorearray.add(getAV(tokens,
				 * singltext,inf,1));
				 * 
				 * if(structype==1){ System.out.println(structype+"NOPerd pod="
				 * + podvalue + " " + inf[2] + " total:" + total + " quantity: "
				 * + quantity + " " + nwordarrayL2[podvalue] + " " +
				 * nwordarrayL2[inf[2]]+" "+nwordarrayL2[inf[7]] + " total:"
				 * +inf[9]);
				 * 
				 * }else System.out.println(structype+"NOPerd pod=" + podvalue +
				 * " " + inf[2] + " total:" + total + " quantity: " + quantity +
				 * " " + nwordarrayL2[podvalue] + " " + nwordarrayL2[inf[2]]);
				 * 
				 * 
				 * 
				 * if(structype==2){ maincorearray.add(getAV(tokens,
				 * singltext,inf,2));
				 * 
				 * System.out.println("NOPerd podecond=" + inf[6] + " " + inf[7]
				 * + " total:" + inf[9] + " quantity: " + inf[10] + " " +
				 * nwordarrayL2[inf[6]] + " " + nwordarrayL2[inf[7]]); } }else{
				 * //not bad
				 * 
				 * 
				 * 
				 * } }
				 * 
				 * }
				 * 
				 */

				// Какой из них лучший?

				if (predicates.containsKey(podvalue)) {
					verbPredicatNoun.put(skazvalue, podvalue);

					if (quantity < 4) {
						if (total > 0) {
							// good
						
							// добавить если только не существует более длинный
							// класс structype==1
							if (structype == 1) {
								// radioOne.put(key, value);
								// sortresult.put(podvalue+""+skazvalue, inf);

								if (sortresult.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = sortresult.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										sortresult.put(podvalue + "" + skazvalue, inf);
									} else if (stored[0] == 1) {
									} else if (stored[0] == 2) {
										// обновить значение 2 увеличив до 3х
										sortresult.put(podvalue + "" + skazvalue, inf);
									}

								} else
									sortresult.put(podvalue + "" + skazvalue, inf);

							} else if (structype == 2) {
								// maincorearray.add(getAV(tokens,
								// singltext,inf,2));

								if (sortresult.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = sortresult.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										sortresult.put(podvalue + "" + skazvalue, inf);
									}
									// не трогать сеплет
									else if (stored[0] == 1) {
									}

									// else if(stored[0]==2){}

									// обработать схвост

									int podsecond = inf[6];
									int skazsecond = inf[7];
									verbPredicatNoun.put(skazsecond, podsecond);

									// если такого значения хвоста нет

									// sortresult.put(podsecond+""+skazsecond,
									// inf);
									if (sortresult.containsKey(podsecond + "" + skazsecond)) {
										Integer[] lstored = sortresult.get(podsecond + "" + skazsecond);
										// перезаписать
										if (lstored[0] == 0) {
											sortresult.put(podsecond + "" + skazsecond, inf);
										} else if (lstored[0] == 1) {
										} else if (lstored[0] == 2) {
										}

									} else
										sortresult.put(podsecond + "" + skazsecond, inf);

								} else
									sortresult.put(podvalue + "" + skazvalue, inf);

							} else if (structype == 0) {

								if (sortresult.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = sortresult.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										sortresult.put(podvalue + "" + skazvalue, inf);
									} else if (stored[0] == 1) {
									} else if (stored[0] == 2) {
									}

								} else
									sortresult.put(podvalue + "" + skazvalue, inf);

							}

						} else {
							
							
							System.out.println(structype + " not bad structype pod=" + podvalue + " " + inf[2] + " total:" + total
									+ " quantity: " + quantity + " " + nwordarrayL2[podvalue] + " " + nwordarrayL2[inf[2]]);
							// not bad
							// noAlternatives

							// данный глагол уже задействован ? с лучшйми
							// показателями quantity и total
							// да - игнорировать

							// нет - проверить существительное уже задействовано
							// с лучшйми показателями quantity и total
							// добавить в коллекцию

							if (structype == 1) {

								if (noAlternatives.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = noAlternatives.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										noAlternatives.put(podvalue + "" + skazvalue, inf);
									} else if (stored[0] == 1) {
									} else if (stored[0] == 2) {
										// обновить значение 2 увеличив до 3х
										noAlternatives.put(podvalue + "" + skazvalue, inf);
									}

								} else
									noAlternatives.put(podvalue + "" + skazvalue, inf);

							} else if (structype == 2) {
								// maincorearray.add(getAV(tokens,
								// singltext,inf,2));

								if (noAlternatives.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = noAlternatives.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										noAlternatives.put(podvalue + "" + skazvalue, inf);
									}
									// не трогать сеплет
									else if (stored[0] == 1) {
									}

									// else if(stored[0]==2){}

									// обработать схвост

									int podsecond = inf[6];
									int skazsecond = inf[7];
									verbPredicatNoun.put(skazsecond, podsecond);

									// если такого значения хвоста нет

									// noAlternatives.put(podsecond+""+skazsecond,
									// inf);
									if (noAlternatives.containsKey(podsecond + "" + skazsecond)) {
										Integer[] lstored = noAlternatives.get(podsecond + "" + skazsecond);
										// перезаписать
										if (lstored[0] == 0) {
											noAlternatives.put(podsecond + "" + skazsecond, inf);
										} else if (lstored[0] == 1) {
										} else if (lstored[0] == 2) {
										}

									} else
										noAlternatives.put(podsecond + "" + skazsecond, inf);

								} else
									noAlternatives.put(podvalue + "" + skazvalue, inf);

							} else if (structype == 0) {

								if (noAlternatives.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = noAlternatives.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										noAlternatives.put(podvalue + "" + skazvalue, inf);
									} else if (stored[0] == 1) {
									} else if (stored[0] == 2) {
									}

								} else
									noAlternatives.put(podvalue + "" + skazvalue, inf);

							}

						}
					}
				} else {
					// min quantity
					// a max total
					if (quantity < 4) {
						if (total > 0) {
							// если глагол учавствует в предикатной структуре
							// его использовать нельзя - список всех глаголов и
							// предикатов
							//
							if (structype == 1) {
								// radioOne.put(key, value);
								// radioOne.put(podvalue+""+skazvalue, inf);

								if (radioOne.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = radioOne.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										radioOne.put(podvalue + "" + skazvalue, inf);
									} else if (stored[0] == 1) {
									} else if (stored[0] == 2) {
									}

								} else
									radioOne.put(podvalue + "" + skazvalue, inf);

							} else if (structype == 2) {
								// maincorearray.add(getAV(tokens,
								// singltext,inf,2));

								if (radioOne.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = radioOne.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										radioOne.put(podvalue + "" + skazvalue, inf);
									}
									// не трогать сеплет
									else if (stored[0] == 1) {
									}

									// else if(stored[0]==2){}

									// обработать схвост

									int podsecond = inf[6];
									int skazsecond = inf[7];

									// если такого значения хвоста нет

									// radioOne.put(podsecond+""+skazsecond,
									// inf);
									if (radioOne.containsKey(podsecond + "" + skazsecond)) {
										Integer[] lstored = radioOne.get(podsecond + "" + skazsecond);
										// перезаписать
										if (lstored[0] == 0) {
											radioOne.put(podsecond + "" + skazsecond, inf);
										} else if (lstored[0] == 1) {
										} else if (lstored[0] == 2) {
										}

									} else
										radioOne.put(podsecond + "" + skazsecond, inf);

								} else
									radioOne.put(podvalue + "" + skazvalue, inf);

							} else if (structype == 0) {

								if (radioOne.containsKey(podvalue + "" + skazvalue)) {
									Integer[] stored = radioOne.get(podvalue + "" + skazvalue);
									// перезаписать
									if (stored[0] == 0) {
										radioOne.put(podvalue + "" + skazvalue, inf);
									} else if (stored[0] == 1) {
									} else if (stored[0] == 2) {
									}

								} else
									radioOne.put(podvalue + "" + skazvalue, inf);

							}

						} else {
							// not bad
							// total<0 малоероятное предикатное событие
							// им можно пользоваться когда нет альтернатив
							
							System.out.println(structype + " 2not bad structype pod=" + podvalue + " " + inf[2] + " total:" + total + " quantity: " + quantity + " " + nwordarrayL2[podvalue] + " " + nwordarrayL2[inf[2]]);
						}
					}

				}

				System.out.println(" 1-----------------CREATE JSON OUT----------------------" + structype);

				// j++;
				// if (j > 4)
				// break;
				// }
			}
			
//			for (Entry<Integer, String> entryradio : predicates.entrySet()) {
//				
//				System.out.println("C==="+nwordarrayL2[entryradio.getKey()]);
//				
//			}
//			

			for (Entry<String, Integer[]> entryradio : radioOne.entrySet()) {
				String podAndskaz = entryradio.getKey();

				if (!sortresult.containsKey(podAndskaz)) {
					Integer[] infl = entryradio.getValue();

					int podvalue = infl[1];
					int skazvalue = infl[2];

					if (!verbPredicatNoun.containsKey(skazvalue)) {

						sortresult.put(podvalue + "" + skazvalue, infl);
					}

				}
			}

			if (sortresult.size() > 0) {
				
			} else if (noAlternatives.size() > 0){
				sortresult=noAlternatives;
			}

				for (Entry<String, Integer[]> entrysorted : sortresult.entrySet()) {
					// good

					Integer[] inf = new Integer[11];
					inf = entrysorted.getValue();
					int podvalue = inf[1];
					int skazvalue = inf[2];
					int total = inf[4];
					int quantity = inf[5];
					int structype = 0;
					structype = inf[0];

					if (structype == 0) {
						maincorearray.add(getAV(tokens, singltext, inf, 1, morfoarrayL3, nwordarrayL2, triplet, adjectiveNoun));

						System.out.println(structype + "structype pod=" + podvalue + " " + inf[2] + " total:" + total
								+ " quantity: " + quantity + " " + nwordarrayL2[podvalue] + " " + nwordarrayL2[inf[2]]);

					}
					if (structype == 1) {
						maincorearray.add(getAV(tokens, singltext, inf, 1, morfoarrayL3, nwordarrayL2, triplet, adjectiveNoun));

						System.out.println(structype + "structype pod=" + podvalue + " " + inf[2] + " total:" + total
								+ " quantity: " + quantity + " " + nwordarrayL2[podvalue] + " " + nwordarrayL2[inf[2]]
								+ " " + nwordarrayL2[inf[7]] + " total:" + inf[9]);

					}
					if (structype == 2) {

						maincorearray.add(getAV(tokens, singltext, inf, 1, morfoarrayL3, nwordarrayL2, triplet,adjectiveNoun));
						maincorearray.add(getAV(tokens, singltext, inf, 2, morfoarrayL3, nwordarrayL2, triplet,adjectiveNoun));

						System.out.println(structype + "structype pod=" + podvalue + " " + inf[2] + " total:" + total
								+ " quantity: " + quantity + " " + nwordarrayL2[podvalue] + " " + nwordarrayL2[inf[2]]);

						System.out.println(structype + "structype podecond=" + inf[6] + " " + inf[7] + " total:"
								+ inf[9] + " quantity: " + inf[10] + " " + nwordarrayL2[inf[6]] + " "
								+ nwordarrayL2[inf[7]]);
					}
				}
			

			JSONArray adjarray = new JSONArray();

			for (Entry<String, Integer> d : adjectiveList.entrySet()) {
				System.out.println("adjective "+d.getKey());
				adjarray.add(d.getKey());
			}

			srtjson.put("adjective", "" + adjarray);
			srtjson.put("core", "" + maincorearray);

			// listjson.add(srtjson);

		}
		return srtjson;

	}

	public JSONArray getAV(String[] tokens, String singltext, Integer[] inf, int type,String[] morfoarrayL3,String[] nwordarrayL2, Map<String, Integer> triplet, Map<String, List> adjectiveNoun) {

		int pod = inf[1];
		int skaz = inf[2];
		int ihasNo = inf[3];
		int total = inf[4];
		int quantity = inf[5];

		int skazsecond = -1;
		int ihasNosecond = -1;
		int totalsecond = -1;
		int quantitysecond = -1;
		String sskazsecond = "";
		if (inf[0] == 1) {
			skazsecond = inf[7];
			ihasNosecond = inf[8];
			totalsecond = inf[9];
			quantitysecond = inf[10];
			sskazsecond = tokens[skazsecond].trim();
		}

		if (type == 2) {

			pod = inf[6];
			skaz = inf[7];
			ihasNo = inf[8];
			total = inf[9];
			quantity = inf[10];
		}

		JSONObject inobj = new JSONObject();
		JSONObject poditem = new JSONObject();
		JSONObject scazfirst = new JSONObject();
		JSONObject scazsecond = new JSONObject();
		JSONArray corebefore = new JSONArray();
		JSONArray coremiddle = new JSONArray();
		JSONArray coreafter = new JSONArray();
		JSONArray maincorearray = new JSONArray();
		int tonpos = 0;
		int tonneg = 0;

		int tonposscaz = 0;
		int tonnegscaz = 0;

		int negbefore = 0;
		int posbefore = 0;

		int negmiddle = 0;
		int posmiddle = 0;

		int negafter = 0;
		int posafter = 0;

		String spod = tokens[pod].trim();
		int pbegin = 0;
		int pend = 0;
		List<IndexWrapper> list = new ArrayList<>();
		list = findIndexesForKeyword(singltext, spod);
		try {
			if (list.size() > 0) {

				pbegin = list.get(0).getStart();
				pend = spod.length() + pbegin;

			}
		} catch (Exception e) {
		}

		String sskaz = tokens[skaz].trim();

		int sbegin = 0;
		int send = 0;
		List<IndexWrapper> lists = new ArrayList<>();
		lists = findIndexesForKeyword(singltext, sskaz);
		try {
			if (list.size() > 0) {

				sbegin = lists.get(0).getStart();
				send = spod.length() + sbegin;

			}
		} catch (Exception e) {
		}

		poditem.put("type", 1);
		poditem.put("podid", pod);
		poditem.put("word", spod);
		poditem.put("start", pbegin);
		poditem.put("end", pend);

		scazfirst.put("type", 2);
		scazfirst.put("skazid", skaz);
		scazfirst.put("word", sskaz);
		scazfirst.put("start", sbegin);
		scazfirst.put("end", send);
		scazfirst.put("ihasno", ihasNo);

		scazsecond.put("type", 3);
		scazsecond.put("skazid", skazsecond);
		scazsecond.put("word", sskazsecond);
		scazsecond.put("start", sbegin);
		scazsecond.put("end", send);
		scazsecond.put("ihasno", ihasNosecond);
		
		int lim = 10;
		int size=tokens.length;
		// 1 < 9 сказуемое дальше
		if (pod < skaz) {

			// 1-1 1-1>1-10(-9) 0
			inobj.put("vec", "direct");
			for (int p = pod - 1; p > pod - lim; p--) {
				if (p < 0)
					break;
				if (p >= size)
					break;
				String l = tokens[p];
				// <----

				// проверить если идущее слово или цифры относится к
				// вопросу _сколько_ ЧИСЛ десять, ЧИСЛ сравн
				// проверить если идущее слово относится к вопросу
				// ___КОГДА___? А-формат дат, наречия и месяцы, ЧИСЛ
				// десятое, в десять, на десятое

				// проверить если идущее слово относится к вопросу
				// _ГДЕ_место(предлоги в на)
				JSONObject beforeforarray = new JSONObject();


				int wordtype = 0;

				if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[p])) {
				
					beforeforarray.put("nmb", p);
					beforeforarray.put("type", 1);
					beforeforarray.put("word", l);



					try {
						String wordpos = tokens[p].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						beforeforarray.put("start", beginpos);
						beforeforarray.put("end", endpos);

					} catch (Exception e) {
					}


				} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[p])) {
				

				
					beforeforarray.put("nmb", p);
					beforeforarray.put("type", 0);
					beforeforarray.put("word", l);
					beforeforarray.put("tonneg", "");
					beforeforarray.put("tonpos", "");

						try {	
						List adj = adjectiveNoun.get(nwordarrayL2[p]);
						if(adj.size()>0){
							beforeforarray.put("type", 5);
						}
						} catch (Exception e) {}
					
					try {
						String wordpos = tokens[p].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						beforeforarray.put("start", beginpos);
						beforeforarray.put("end", endpos);

					} catch (Exception e) {
					}

				
				}
				corebefore.add(beforeforarray);
			}

			// посередине, между подлижащим и сказуемым нет данных
			// --->
			for (int s = pod + 1; s < skaz; s++) {
				if (s < 0)
					break;
				if (s >= size)
					break;
				String l = tokens[s];
				JSONObject middlearray = new JSONObject();
				try {} catch (Exception e) {
				}

				if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
				
					middlearray.put("nmb", s);
					middlearray.put("type", 1);
					middlearray.put("word", l);
					middlearray.put("tonneg", "" );
					middlearray.put("tonpos", "");


					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						middlearray.put("start", beginpos);
						middlearray.put("end", endpos);

					} catch (Exception e) {
					}

					
				} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
					
					middlearray.put("nmb", s);
					middlearray.put("type", 0);
					middlearray.put("word", l);
					middlearray.put("tonneg", "");
					middlearray.put("tonpos", "");
					try {	
					List adj = adjectiveNoun.get(nwordarrayL2[s]);
					if(adj.size()>0){
						middlearray.put("type", 5);
					}
					} catch (Exception e) {}
					
					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						middlearray.put("start", beginpos);
						middlearray.put("end", endpos);

					} catch (Exception e) {
					}

				}

				coremiddle.add(middlearray);
			}

			int calcvalue = 0;

			// 9 = 9+1 9+1<9+10

			for (int s = skaz + 1; s < skaz + lim; s++) {
				if (s < 0)
					break;
				if (s >= size)
					break;
				String l = tokens[s];
				JSONObject afterarray = new JSONObject();
				if (nwordarrayL2[s] != null)
					if (triplet.containsKey(
							nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " " + nwordarrayL2[s])) {
						int curval = triplet.get(
								nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " " + nwordarrayL2[s]);
						if (calcvalue < curval) {
							calcvalue = curval;
							afterarray.put("triplet", nwordarrayL2[s]);
						}
					}

				
				if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
					
					afterarray.put("nmb", s);
					afterarray.put("type", 1);
					afterarray.put("word", l);
					afterarray.put("tonneg", "");
					afterarray.put("tonpos", "");

					
					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						afterarray.put("start", beginpos);
						afterarray.put("end", endpos);

					} catch (Exception e) {
					}


				} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
				
					afterarray.put("nmb", s);
					afterarray.put("type", 0);
					afterarray.put("word", l);
					afterarray.put("tonneg", "");
					afterarray.put("tonpos", "");
					try {	
						List adj = adjectiveNoun.get(nwordarrayL2[s]);
						if(adj.size()>0){
							afterarray.put("type", 5);
						}
						} catch (Exception e) {}

					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						afterarray.put("start", beginpos);
						afterarray.put("end", endpos);

					} catch (Exception e) {
					}

					

				}
				coreafter.add(afterarray);
			}
			
		}

		// 7 6
		int calcvalue = 0;

		if (pod > skaz) {
			inobj.put("vec", "indirect");
			// <-----
			for (int s = skaz - 1; s > skaz - lim; s--) {
				if (s < 0)
					break;
				if (s >= size)
					break;
				String l = tokens[s];
				JSONObject afterarray = new JSONObject();
				try {
					if (nwordarrayL2[s] != null)
						if (triplet.containsKey(nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " "
								+ nwordarrayL2[s])) {
							int curval = triplet.get(nwordarrayL2[inf[0]] + " " + nwordarrayL2[inf[1]] + " "
									+ nwordarrayL2[s]);
							if (calcvalue < curval) {
								calcvalue = curval;
								afterarray.put("triplet", nwordarrayL2[s]);
							}
						}
				
				} catch (Exception e) {
				}

				if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
				
					afterarray.put("nmb", s);
					afterarray.put("type", 1);
					afterarray.put("word", l);
					afterarray.put("tonneg", "");
					afterarray.put("tonpos", "");
					
					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						afterarray.put("start", beginpos);
						afterarray.put("end", endpos);

					} catch (Exception e) {
					}

					
				} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
				
					afterarray.put("nmb", s);
					afterarray.put("type", 0);
					afterarray.put("word", l);
					afterarray.put("tonneg", "");
					afterarray.put("tonpos", "");
					try {	
						List adj = adjectiveNoun.get(nwordarrayL2[s]);
						if(adj.size()>0){
							afterarray.put("type", 5);
						}
						} catch (Exception e) {}
					
					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						afterarray.put("start", beginpos);
						afterarray.put("end", endpos);

					} catch (Exception e) {
					}

					
				}
				coreafter.add(afterarray);
			}
			// 8

			// посередине, между подлижащим и сказуемым нет данных
			for (int s = skaz + 1; s < pod; s++) {
				if (s < 0)
					break;
				if (s >= size)
					break;
				String l = tokens[s];
				JSONObject middlearray = new JSONObject();
				

				if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[s])) {
				
					middlearray.put("nmb", s);
					middlearray.put("type", 1);
					middlearray.put("word", l);
					middlearray.put("tonneg", "");
					middlearray.put("tonpos", "");
					
					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						middlearray.put("start", beginpos);
						middlearray.put("end", endpos);

					} catch (Exception e) {
					}

				
				} else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[s])) {
				
					middlearray.put("nmb", s);
					middlearray.put("type", 0);
					middlearray.put("word", l);
					middlearray.put("tonneg", "");
					middlearray.put("tonpos", "");
					try {	
						List adj = adjectiveNoun.get(nwordarrayL2[s]);
						if(adj.size()>0){
							middlearray.put("type", 5);
						}
						} catch (Exception e) {}

					try {
						String wordpos = tokens[s].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						middlearray.put("start", beginpos);
						middlearray.put("end", endpos);

					} catch (Exception e) {
					}


				}
				coremiddle.add(middlearray);
			}

			for (int p = pod + 1; p < pod + lim; p++) {
				if (p < 0)
					break;
				if (p >= size)
					break;
				String l = tokens[p];
				JSONObject beforeforarray = new JSONObject();

				

				if (isPresent("((С (жр|мр|ср))|(МС [0-9]л))", morfoarrayL3[p])) {
				
					beforeforarray.put("nmb", p);
					beforeforarray.put("type", 1);
					beforeforarray.put("word", l);
					beforeforarray.put("tonneg", "");
					beforeforarray.put("tonpos", "");
	

					try {
						String wordpos = tokens[p].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						beforeforarray.put("start", beginpos);
						beforeforarray.put("end", endpos);

					} catch (Exception e) {
					}

				}

				else if (!isPresent("(СОЮЗ|ПРЕДЛ)", morfoarrayL3[p])) {
					
					beforeforarray.put("nmb", p);
					beforeforarray.put("type", 0);
					beforeforarray.put("word", l);
					beforeforarray.put("tonneg", "");
					beforeforarray.put("tonpos", "");
					try {	
						List adj = adjectiveNoun.get(nwordarrayL2[p]);
						if(adj.size()>0){
							beforeforarray.put("type", 5);
						}
						} catch (Exception e) {}
					try {
						String wordpos = tokens[p].trim();
						int beginpos = 0;
						int endpos = 0;
						List<IndexWrapper> listspos = new ArrayList<>();
						listspos = findIndexesForKeyword(singltext, wordpos);

						if (listspos.size() > 0) {

							beginpos = listspos.get(0).getStart();
							endpos = wordpos.length() + beginpos;

						}

						beforeforarray.put("start", beginpos);
						beforeforarray.put("end", endpos);

					} catch (Exception e) {
					}

					
				}
				corebefore.add(beforeforarray);
			}

			String wordkey = "";

		}

		inobj.put("corebefore", corebefore);
		inobj.put("coremiddle", coremiddle);
		inobj.put("coreafter", coreafter);
		inobj.put("predicat", poditem);
		inobj.put("verb", scazfirst);
		inobj.put("verbsec", scazsecond);
		maincorearray.add(inobj);

		return maincorearray;
	}
	
	public boolean thisIsVeb(String word, LuceneMorphology luceneMorph){
		boolean rt=false;
		List<String> wordBaseForms = null;
		String wordform = "";
		if (!"".equals(word)) {
			wordBaseForms = luceneMorph.getMorphInfo(word);
		}
		int sizer = 0;
		try {
			sizer = wordBaseForms.size();
		} catch (Exception e) {
		}

		if (sizer > 0) {

			StringBuilder sb = new StringBuilder();
			for (String s : wordBaseForms) {
				sb.append(s);
				sb.append(" #");
			}

			String ist = sb.toString();
			if (!ist.equals("") && ist != null) {
				wordform = ist;
			}

			if (wordform.contains("|")) {
				String[] myData = wordform.split("\\|");
				String inword = myData[0].trim();
				String imorf = myData[1];
				
				if(isPresent("Г|ИНФИНИТИВ",imorf)){
					rt=true;
				}
			}
		}
		return rt;
	}
}
