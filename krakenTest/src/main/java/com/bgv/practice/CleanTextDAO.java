package com.bgv.practice;

import java.util.List;
import java.util.Map;

public interface CleanTextDAO {

	Map<Integer, List<String[]>> getText(int[] level, String text);

}
