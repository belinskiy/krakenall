package com.bgv.practice;

import java.util.Map;

import org.json.simple.JSONObject;

public interface ProcessorDAO {
	//ProcessorResult getTextRezult(String text, boolean ineededu);

	//JSONObject createRestRezult(Map<Integer, String[]> in, String singltext);

	//JSONObject createRestRezult(ProcessorResult rin);

	JSONObject createRestRezult(ProcessorResult rin, boolean ineededu, boolean getrezult);

	ProcessorResult getTextRezult(String text, boolean ineededu, boolean getmalt);

	JSONObject getVariationRezult(String text, boolean ineededu, boolean getmalt, boolean minimumres);

	JSONObject getVecRezult(String lowerStr);

	JSONObject createNewResult(ProcessorResult rin, boolean ineededu, boolean getrezult, Map<Integer, String> predicates);
}
