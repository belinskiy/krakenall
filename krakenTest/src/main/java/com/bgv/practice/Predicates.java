package com.bgv.practice;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

public class Predicates {
	JSONObject outjson = new JSONObject();
	Map<Integer, String> predicates = new HashMap<Integer, String>();
	public JSONObject getOutjson() {
		return outjson;
	}
	public void setOutjson(JSONObject outjson) {
		this.outjson = outjson;
	}
	public Map<Integer, String> getPredicates() {
		return predicates;
	}
	public void setPredicates(Map<Integer, String> predicates) {
		this.predicates = predicates;
	}
	
	
}
