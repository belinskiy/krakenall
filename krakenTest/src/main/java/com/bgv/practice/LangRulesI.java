package com.bgv.practice;

import java.util.Map;

public interface LangRulesI {

	boolean[] semanticRuleSet(String word1, String morf1, String word2, String morf2, String word3, String morf3,
			String word4, String morf4);

	boolean verbRuleSet(String word1, String morf1, String word2, String morf2, String word3, String morf3,
			String word4, String morf4);

	Map<Integer, String> getSenteces(String subjectString);

	boolean hasVoprosSymbol(String str);

	boolean narRuleSet(String morf1);

	boolean[] podlejasheeRuleSet(String morf);

	boolean[] skazuemoeRule(String worda, String morfa, String wordb, String morfb);

	boolean sushRuleSet(String morf);

	boolean prolognarRuleSet(String morf);

	boolean deeprichastieRule(String morf);

}
