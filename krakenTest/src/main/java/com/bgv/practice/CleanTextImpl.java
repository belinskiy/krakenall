package com.bgv.practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.languagetool.JLanguageTool;
import org.languagetool.MultiThreadedJLanguageTool;
import org.languagetool.language.Russian;
import org.languagetool.rules.RuleMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bgv.nlp.obj.NameFinderModelMEDAO;
import com.bgv.nlp.obj.ObjDAO;

@Service
public class CleanTextImpl implements CleanTextDAO{
	 @Autowired
	 private ObjDAO objs;
	 @Autowired
	 private NameFinderModelMEDAO langDAO;
	
	 
	 @Override
	public Map<Integer, List<String[]>> getText(int[] level, String text){
		//String[] textresult = new String[5];
		//textresult[0] = "-1";textresult[1] = "-1";textresult[2] = "-1";textresult[3] = "-1";textresult[4] = "-1";
		//List<String> alltokenstrim = new  ArrayList<String>();
		
		Map<Integer, List<String[]>> mapresult = new  HashMap<Integer, List<String[]>>();
		try{if(text.length()>0){
		//1
			try{if(level[0]==1){
			List<String[]> alltokenstrim = new  ArrayList<String[]>();
			
			//StringBuffer firstresult=new StringBuffer();
			String dirty=text;
	    	try{dirty=text.replaceAll("\\pP"," ");}catch(Exception e){	e.printStackTrace();}
	    	String tokens[] = dirty.toString().split("\\p{P}?[\\| \\t\\n\\r]+");
	    	
	    	for (String word: tokens) { 
	    		if(word!=null){
	    			try{word=word.trim();}catch(Exception e){e.printStackTrace();}
	    		if(!word.equals("")){
	    			String[] index = new String[2];
	    			index[0]=word;
	    			index[1]="";	
	    		alltokenstrim.add(index);
	    		//firstresult.append(word).append(" ");
	    		}
	    		}	
	    		
	    	}
	    	mapresult.put(1, alltokenstrim);
	    	
	    	//textresult[0] = firstresult.toString();
	    	
	    	
		}}catch(Exception e){}
		//2
			try{if(level[1]==1){
			List<String[]> alltokenstrim = new  ArrayList<String[]>();
			//MultiThreadedJLanguageTool langTool = new MultiThreadedJLanguageTool(new Russian());
			//JLanguageTool langTool = new JLanguageTool(new Russian());
			JLanguageTool langTool = langDAO.getLanguageTool();
			langTool.disableRule("UPPERCASE_SENTENCE_START");
	    	langTool.disableRule("WHITESPACE_RULE");
	    	
	    	String delims = " ";
	    	StringTokenizer st = new StringTokenizer(text, delims);
	    	StringBuffer firstfb=new StringBuffer();
	    	while (st.hasMoreElements()) {
				String word=(String) st.nextElement();
				try{word=word.replaceAll("\\pP","");}catch(Exception e){e.printStackTrace();}
				List<RuleMatch> matches = langTool.check(word);
				StringBuffer errresult=new StringBuffer();
				for (RuleMatch match : matches) {
					  //System.out.println(" " +match.getFromPos() + "-" + match.getToPos() + ": " + match.getMessage());
					  //System.out.println(": " +match.getSuggestedReplacements());
					List lists=match.getSuggestedReplacements();
					
					for(Object l:lists){
						String s=(String)l;
						errresult.append(s).append("|");
					}
				}
				
				String[] index = new String[2];
    			index[0]=word;
    			index[1]=errresult.toString();	
    			alltokenstrim.add(index);
				
				//firstfb.append(word).append(" ");
			}
	    	/*
	    	String dirtyBB=firstfb.toString();
	    	
	    	List<RuleMatch> matches = langTool.check(dirtyBB);
			
			for (RuleMatch match : matches) {
			  System.out.println(" " +
			      match.getFromPos() + "-" + match.getToPos() + ": " +
			      match.getMessage());
			  System.out.println("(===): " +
			      match.getSuggestedReplacements());
			}
			*/
			
	    	mapresult.put(2, alltokenstrim);
			
		}}catch(Exception e){}
		//3
			try{if(level[2]==1){
		//			 LuceneMorphology luceneMorph = new RussianLuceneMorphology();	
		 LuceneMorphology luceneMorph = objs.getLuceneMorph();
			List<String[]> alltokensProp = mapresult.get(1);
			List<String[]> alltokenstrim = new  ArrayList<String[]>();
			
			for (String[] wordtokend: alltokensProp) { 
				String word = wordtokend[0];
			try{
				String wordform="-1";	
				List<String> wordBaseForms = luceneMorph.getMorphInfo(word.toLowerCase());
				StringBuilder sb = new StringBuilder();
				for (String s : wordBaseForms)
				{
				    sb.append(s);
				    sb.append("#");
				}
				
				
				String ist = sb.toString();
				if(!ist.equals("")&&ist!=null){
					wordform=ist;
					
					String[] index = new String[2];
	    			index[0]=word;
	    			index[1]=wordform;	
	    			alltokenstrim.add(index);
					
				}
				}catch(Exception e){//e.printStackTrace();
				String[] index = new String[2];
    			index[0]=word;
    			index[1]=word+"|unk";	
    			alltokenstrim.add(index);
				}
				
				
			}
			mapresult.put(3, alltokenstrim);
			
		}}catch(Exception e){//e.printStackTrace();
		}
		//4	
		try{if(level[3]==1){
			List<String[]> alltokenstrim = new  ArrayList<String[]>();
				String searchString=text; 
		        String regex = "\\p{Punct}";
		        Pattern pattern = Pattern.compile(regex);
		        Matcher matcher = pattern.matcher(searchString);
		 
		      
		        StringBuilder sb = new StringBuilder();
		        while(matcher.find() == true){
		            int end = matcher.end();
		            int start = matcher.start();
		            sb.append("s").append(start).append("e").append(end).append("m").append(""+matcher.group());
		           
		        }
		     
		        String[] index = new String[2];
    			index[0]="";
    			index[1]=sb.toString();	
    			alltokenstrim.add(index);
    			mapresult.put(4, alltokenstrim);
			
		}}catch(Exception e){}
		//5
		try{if(level[4]==1){}}catch(Exception e){}
		
		}
	}catch(Exception e){}
		
		return mapresult;
	}

}
