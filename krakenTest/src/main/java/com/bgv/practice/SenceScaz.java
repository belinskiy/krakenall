package com.bgv.practice;

public class SenceScaz implements Comparable<SenceScaz>{
	private int pod;
	private int scaz;
	private int quantity;
	private int total;
	private int ihasno;
	public SenceScaz(int pod, int scaz, int quantity,int total, int ihasno) {
		super();
		this.pod = pod;
		this.scaz = scaz;
		this.quantity = quantity;
		this.total = total;
		this.ihasno = ihasno;
	}
	
	
	
	
	public int getIhasno() {
		return ihasno;
	}




	public void setIhasno(int ihasno) {
		this.ihasno = ihasno;
	}




	public int getTotal() {
		return total;
	}




	public void setTotal(int total) {
		this.total = total;
	}




	public int getPod() {
		return pod;
	}




	public void setPod(int pod) {
		this.pod = pod;
	}




	public int getScaz() {
		return scaz;
	}




	public void setScaz(int scaz) {
		this.scaz = scaz;
	}




	public int getQuantity() {
		return quantity;
	}




	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}




	@Override
	public int compareTo(SenceScaz o) {
		int compareQuantity = ((SenceScaz) o).getQuantity();

		//ascending order
		return this.quantity - compareQuantity;

		//descending order
		//return Math.abs(compareQuantity - this.quantity);
	}

}
