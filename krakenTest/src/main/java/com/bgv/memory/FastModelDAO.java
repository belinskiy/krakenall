package com.bgv.memory;

import java.util.List;
import java.util.Map;

public interface FastModelDAO {

	//Map<String, String> getObjectAndVerb();

	Map<String, String> getVonrosiPrimeri();

	Map<String, Integer> getObjectAndVerbCount();
	Map<String, Integer> getObjectAndAdjectiveCount();
	

	Map<String, Map<String, Integer>> getObjectAndVerbList();

//	void cleareObjectAndVerb();

	Map<String, Integer> getObjectRelationCount();

	Map<String, Map<String, Integer>> getObjectRelationList();

	void cleareObjectRelation();

	void cleareObjectAndVerbCount();
	
	void cleareObjectAndVerbCountTriplet();
	
	void cleareNounAndVerb();
	
	void cleareNounAndVerbTriplet();

	void cleareLemaNounAndVerb();
	

	Map<String, List> getIilobjects();

	Map<String, List> getIilproperties();

	Map<String, List> getIilstatements();

	Map<String, List> getOfinstance();

	Map<String, List> getIilsubclassof();

	Map<String, List> getIilhaspart();

	Map<String, List> getIilpartof();

	Map<String, List> getIilotherprops();

	void setIilobjects(Map<String, List> objects);

	void setIilproperties(Map<String, List> properties);

	void setIilstatements(Map<String, List> iilstatements);

	void setOfinstance(Map<String, List> ofinstance);

	void setIilsubclassof(Map<String, List> iilsubclassof);

	void setIilhaspart(Map<String, List> iilhaspart);

	void setIilpartof(Map<String, List> iilpartof);

	void setIilotherprops(Map<String, List> iilotherprops);

	void setVonrosiPrimeri(Map<String, String> vonrosiPrimeri);

	void setObjectAndVerbCount(Map<String, Integer> objectAndVerbCount);

	void setObjectAndVerbList(Map<String, Map<String, Integer>> objectAndVerbList);

	void setObjectRelationCount(Map<String, Integer> objectRelationCount);

	void setObjectRelationList(Map<String, Map<String, Integer>> objectRelationList);

	Map<String, List> getIilobjectsToText();

	void setIilobjectsToText(Map<String, List> iilobjectsToText);

	void setIilpropertyOfType(Map<String, List> iilpropertyOfType);

	Map<String, List> getIilpropertyOfType();

	Map<String, String> getExplainPrimeri();

	void getVonrosiPrimeriCleare();

	void getExplainPrimeriCleare();

	Map<String, String> getWordSynonims();

	void setWordSynonims(Map<String, String> wordSynonims);

	Map<String, String> getWordFraseologism();

	void setWordFraseologism(Map<String, String> wordFraseologism);

	void setWordExclude(Map<String, String> wordmap);
	Map<String, String> getWordExclude();

	

	Map<String, Integer> getNounverbdb();

	void setNounverbdb(Map<String, Integer> nounverbdb);

	Map<String, Integer> getLemmanounverbdb();

	void setLemmanounverbdb(Map<String, Integer> lemmanounverbdb);

	void setWordPositive(Map<String, String> wordmap);

	Map<String, String> getWordPositive();

	Map<String, String> getWordNegative();

	void setWordNegative(Map<String, String> wordmap);

	Map<String, String> getMorfema();

	void setMorfema(Map<String, String> morfema);

	void setMorfemaWords(Map<String, String> morfword);
	Map<String, String> getMorfemaWords();

	Map<String, String> getCheckIfOrg();

	void setCheckIfOrg(Map<String, String> checkIfOrg);

	Map<String, String> getCheckIfHuman();

	void setCheckIfHuman(Map<String, String> checkIfHuman);

	Map<String, String> getCheckIfGeo();

	void setCheckIfGeo(Map<String, String> checkIfGeo);

	void setMaincategory(Map<String, List> maincategory);

	Map<String, List> getMaincategory();

	Map<String, String> getWordNegPreglog();

	void setWordNegPreglog(Map<String, String> wordNegPreglog);

	Map<String, String> getPolitehwords();

	void setPolitehwords(Map<String, String> politehwords);

	Map<String, String> getWordPositiveHuman();

	void setWordPositiveHuman(Map<String, String> wordmap);

	Map<String, String> getWordNegativeHuman();

	void setWordNegativeHuman(Map<String, String> wordmap);

	void setWordPredlogMesta(Map<String, String> wordmap);
	Map<String, String> getWordPredlogMesta();

	void setNarechiyaVremeni(Map<String, String> wordmap);
	Map<String, String> getNarechiyaVremeni();

	void setDniMeseats(Map<String, String> wordmap);
	Map<String, String> getDniMeseats();

	Map<String, Integer> getObjectAndVerbCountTriplet();

	Map<String, Integer> getNounverbdbTriplet();

	void setNounverbdbTriplet(Map<String, Integer> nounverbdb);

	void setMetricsTypeOne(Map<String, String> wordmapOne);

	void setMetricsTypeTwo(Map<String, String> wordmapTwo);

	void setMetricsTypeThree(Map<String, String> wordmapThree);

	Map<String, String> getMetricsTypeOne();

	Map<String, String> getMetricsTypeTwo();

	Map<String, String> getMetricsTypeThree();

	Map<String, String> getEconomicswords();

	void setEconomicswords(Map<String, String> economicsw);

	Map<String, Integer> getObjectAndVerbCountTripletPredlog();

	void cleareObjectAndVerbCountTripletPredlog();

	void setWordKvantor(Map<String, String> matwordsc);
	Map<String, String> getWordKvantor();

	Map<String, Integer> getObjectHorosho();

	Map<String, Integer> getObjectPloho();

	void setWordHorosho(Map<String, Integer> matwordsc);

	void setWordPloho(Map<String, Integer> matwordsc);

	void cleareHorosho();

	void clearePloho();

	Map<String, String> getWordExplainOne();
	void setWordExplainOne(Map<String, String> matwordsc);
	void cleareExplainOne();
	
	
	Map<String, String> getWordExplainTwo();
	void setWordExplainTwo(Map<String, String> matwordsc);
	void cleareExplainTwo();
	
	Map<String, String> getWordExplainThree();
	void setWordExplainThree(Map<String, String> matwordsc);
	void cleareExplainThree();

	void cleareQnonamesingle();

	void setQnonamesingledb(Map<String, String> singledb);

	Map<String, String> getQnonamesingledb();

	void cleareQnonamephrasedb();

	void setQnonamephrasedbdb(Map<String, String> phrasedb);

	Map<String, String> getQnonamephrasedbdb();

	void cleareObjectAndAdjectiveCount();

	void setIilQPKeyValue(Map<String, List<String>> qpproperties);
	Map<String, List<String>> getIilQPKeyValue();

	Map<String, Integer> getObjectAndVerbCountProperty();

	void cleareObjectAndVerbCountProperty();
	
	
}
