package com.bgv.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

@Service
public class FastModelImpl implements FastModelDAO{
	static Map<String, String> checkIfOrg=new ConcurrentHashMap<String, String>();
	static Map<String, String> checkIfHuman=new ConcurrentHashMap<String, String>();
	static Map<String, String> checkIfGeo=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> nonamesingledb=new ConcurrentHashMap<String, String>();
	static Map<String, String> nonamephrasedb=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> morfema=new ConcurrentHashMap<String, String>();
	static Map<String, String> morfemawords=new ConcurrentHashMap<String, String>();
	
    static Map<String, Integer> nounverbdb=new ConcurrentHashMap<String, Integer>();
    static Map<String, Integer> nounverbdbTriplet=new ConcurrentHashMap<String, Integer>();
    
	
	static Map<String, Integer> lemmanounverbdb=new ConcurrentHashMap<String, Integer>();
	
	static Map<String, String> wordPositive=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> wordNegative=new ConcurrentHashMap<String, String>();
	
	static Map<String, Integer> wordHorosho=new ConcurrentHashMap<String, Integer>();
	static Map<String, Integer> wordPloho=new ConcurrentHashMap<String, Integer>();
	
	static Map<String, String> wordPositiveHuman=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> wordNegativeHuman=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> politehwords=new ConcurrentHashMap<String, String>();
	static Map<String, String> economicswords=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> wordSynonims=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> wordFraseologism=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> wordExclude=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> wordNegPreglog=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> predlogMesta=new ConcurrentHashMap<String, String>();
	static Map<String, String> narechiyaVremeni=new ConcurrentHashMap<String, String>();
	static Map<String, String> dniMeseats=new ConcurrentHashMap<String, String>();
	
	
	static Map<String, String> metricsTypeOne=new ConcurrentHashMap<String, String>();
	static Map<String, String> metricsTypeTwo=new ConcurrentHashMap<String, String>();
	static Map<String, String> metricsTypeThree=new ConcurrentHashMap<String, String>();
	
	static Map<String, String> explainTypeOne=new ConcurrentHashMap<String, String>();
	static Map<String, String> explainTypeTwo=new ConcurrentHashMap<String, String>();
	static Map<String, String> explainTypeThree=new ConcurrentHashMap<String, String>();
	
	//static Map<String, String> ObjectAndVerb=new ConcurrentHashMap<String, String>();
	static Map<String, Integer> ObjectAndVerbCount=new ConcurrentHashMap<String, Integer>();
	static Map<String, Integer> ObjectAndAdjectiveCount=new ConcurrentHashMap<String, Integer>();
	
	static Map<String, Integer> ObjectAndVerbCountTriplet=new ConcurrentHashMap<String, Integer>();
	static Map<String, Integer> ObjectRelationCount=new ConcurrentHashMap<String, Integer>();
	static Map<String, Integer> ObjectAndVerbCountProperty=new ConcurrentHashMap<String, Integer>();
	static Map<String, Integer> ObjectAndVerbCountTripletPredlog=new ConcurrentHashMap<String, Integer>();
	static Map<String, String> kvantor=new ConcurrentHashMap<String, String>();
	
	
	static Map<String, Map<String, Integer>> ObjectAndVerbList=new ConcurrentHashMap<String, Map<String, Integer>>();
	static Map<String, Map<String, Integer>> ObjectRelationList=new ConcurrentHashMap<String, Map<String, Integer>>();
	
	static Map<String, String> vonrosiPrimeri=new ConcurrentHashMap<String, String>();
	static Map<String, String> explainPrimeri=new ConcurrentHashMap<String, String>();
	
	
	//-----------------------------------------------------------------------------------
	static   Map<String,List> iilobjects = new ConcurrentHashMap<String,List>();
	static   Map<String,List> iilobjectsToText = new ConcurrentHashMap<String,List>();
	static   Map<String,List> iilproperties = new ConcurrentHashMap<String,List>();
	static   Map<String,List> iilstatements = new ConcurrentHashMap<String,List>();
    
	//P31 instanceof
	static   Map<String,List> ofinstance = new ConcurrentHashMap<String,List>();
	
	//P279 subclassof
	static   Map<String,List> iilsubclassof = new ConcurrentHashMap<String,List>();
    
	//P527 haspart
	static   Map<String,List> iilhaspart = new ConcurrentHashMap<String,List>();
		
	//P361 partof
	static   Map<String,List> iilpartof = new ConcurrentHashMap<String,List>();
    
	//
	static   Map<String,List> iilotherprops = new ConcurrentHashMap<String,List>();
	
	static   Map<String,List> iilpropertyOfType = new ConcurrentHashMap<String,List>();
	
	static   Map<String,List>  maincategory = new ConcurrentHashMap<String,List>();
	static   Map<String, List<String>> qpproperties = new ConcurrentHashMap<String,List<String>>();

	//-----------------------------------------------------------------------------


	//-----------------------------------------------------------------------------
	
	
	
	@Override
	public Map<String, String> getWordSynonims() {
		return wordSynonims;
	}
	@Override
	public  Map<String, String> getPolitehwords() {
		return politehwords;
	}
	@Override
	public  void setPolitehwords(Map<String, String> politehwords) {
		FastModelImpl.politehwords = politehwords;
	}
	@Override
	public  Map<String, String> getWordNegPreglog() {
		return wordNegPreglog;
	}
	@Override
	public  void setWordNegPreglog(Map<String, String> wordNegPreglog) {
		FastModelImpl.wordNegPreglog = wordNegPreglog;
	}
	@Override
	public Map<String, List> getMaincategory() {
		return maincategory;
	}
	@Override
	public void setMaincategory(Map<String, List> maincategory) {
		FastModelImpl.maincategory = maincategory;
	}

	@Override
	public  Map<String, String> getCheckIfOrg() {
		return checkIfOrg;
	}
	@Override
	public  void setCheckIfOrg(Map<String, String> checkIfOrg) {
		FastModelImpl.checkIfOrg = checkIfOrg;
	}
	@Override
	public  Map<String, String> getCheckIfHuman() {
		return checkIfHuman;
	}
	@Override
	public  void setCheckIfHuman(Map<String, String> checkIfHuman) {
		FastModelImpl.checkIfHuman = checkIfHuman;
	}
	@Override
	public  Map<String, String> getCheckIfGeo() {
		return checkIfGeo;
	}
	@Override
	public  void setCheckIfGeo(Map<String, String> checkIfGeo) {
		FastModelImpl.checkIfGeo = checkIfGeo;
	}
	@Override
	public void setWordSynonims(Map<String, String> wordSynonims) {
		FastModelImpl.wordSynonims = wordSynonims;
	}
	@Override
	public  Map<String, String> getWordFraseologism() {
		return wordFraseologism;
	}
	@Override
	public  void setWordFraseologism(Map<String, String> wordFraseologism) {
		FastModelImpl.wordFraseologism = wordFraseologism;
	}
	
	
	
	@Override
public Map<String, List> getIilpropertyOfType() {
		return iilpropertyOfType;
	}
	@Override
	public  void setIilpropertyOfType(Map<String, List> iilpropertyOfType) {
		FastModelImpl.iilpropertyOfType = iilpropertyOfType;
	}

	//	@Override
//	public Map<String, String> getObjectAndVerb() {
//		return ObjectAndVerb;
//	}
	@Override
	public  Map<String, List> getIilobjectsToText() {
		return iilobjectsToText;
	}
	
	@Override
	public  void setIilobjectsToText(Map<String, List> iilobjectsToText) {
		FastModelImpl.iilobjectsToText = iilobjectsToText;
	}

	//	public static void setObjectAndVerb(Map<String, String> objectAndVerb) {
//		ObjectAndVerb = objectAndVerb;
//	}
	@Override
	public Map<String, List> getIilobjects() {
		return iilobjects;
	}

	@Override
	public void setIilobjects(Map<String, List> iilobjects) {
		FastModelImpl.iilobjects = iilobjects;
	}
	
	
	
	@Override
	public Map<String, List> getIilproperties() {
		return iilproperties;
	}
	@Override
	public void setIilproperties(Map<String, List> iilproperties) {
		FastModelImpl.iilproperties = iilproperties;
	}
	
	
	@Override
	public Map<String, List> getIilstatements() {
		return iilstatements;
	}
	
	@Override
	public void setIilstatements(Map<String, List> iilstatements) {
		FastModelImpl.iilstatements = iilstatements;
	}
	@Override
	public Map<String, List> getOfinstance() {
		return ofinstance;
	}

	@Override
	public  void setOfinstance(Map<String, List> ofinstance) {
		FastModelImpl.ofinstance = ofinstance;
	}
	@Override
	public  Map<String, List> getIilsubclassof() {
		return iilsubclassof;
	}

	@Override
	public void setIilsubclassof(Map<String, List> iilsubclassof) {
		FastModelImpl.iilsubclassof = iilsubclassof;
	}
	
	
	@Override
	public  Map<String, List> getIilhaspart() {
		return iilhaspart;
	}

	@Override
	public void setIilhaspart(Map<String, List> iilhaspart) {
		FastModelImpl.iilhaspart = iilhaspart;
	}
	@Override
	public  Map<String, List> getIilpartof() {
		return iilpartof;
	}

	@Override
	public void setIilpartof(Map<String, List> iilpartof) {
		FastModelImpl.iilpartof = iilpartof;
	}
	@Override
	public  Map<String, List> getIilotherprops() {
		return iilotherprops;
	}

	@Override
	public void setIilotherprops(Map<String, List> iilotherprops) {
		FastModelImpl.iilotherprops = iilotherprops;
	}

	@Override
	public Map<String, String> getVonrosiPrimeri() {
		return vonrosiPrimeri;
	}
	@Override
	public void getVonrosiPrimeriCleare(){try{vonrosiPrimeri.clear();}catch(Exception  e){}}
	
	
	@Override
	public Map<String, String> getExplainPrimeri() {
		return explainPrimeri;
	}
	@Override
	public void getExplainPrimeriCleare(){try{explainPrimeri.clear();}catch(Exception  e){}}
	
	@Override
	public void setVonrosiPrimeri(Map<String, String> vonrosiPrimeri) {
		FastModelImpl.vonrosiPrimeri = vonrosiPrimeri;
	}
	@Override
	public  Map<String, Integer> getObjectAndVerbCount() {
		return ObjectAndVerbCount;
	}
	@Override
	public  Map<String, Integer> getObjectAndVerbCountTriplet() {
		return ObjectAndVerbCountTriplet;
	}

	@Override
	public void setObjectAndVerbCount(Map<String, Integer> objectAndVerbCount) {
		ObjectAndVerbCount = objectAndVerbCount;
	}
	@Override
	public void cleareObjectAndVerbCount() {
		ObjectAndVerbCount.clear();
	}
	@Override
	public void cleareObjectAndVerbCountTriplet() {
		ObjectAndVerbCountTriplet.clear();
	}
	
	@Override
	public void cleareObjectAndVerbCountTripletPredlog() {
		ObjectAndVerbCountTripletPredlog.clear();
	}
	
	@Override
	public Map<String, Map<String, Integer>> getObjectAndVerbList() {
		return ObjectAndVerbList;
	}
	
	@Override
	public void setObjectAndVerbList(Map<String, Map<String, Integer>> objectAndVerbList) {
		ObjectAndVerbList = objectAndVerbList;
	}
	@Override
	public Map<String, Integer> getObjectRelationCount() {
		return ObjectRelationCount;
	}
	
	@Override
	public void setObjectRelationCount(Map<String, Integer> objectRelationCount) {
		ObjectRelationCount = objectRelationCount;
	}
	@Override
	public void cleareObjectRelation() {
		ObjectRelationCount.clear();
	}
	
	@Override
	public Map<String, Map<String, Integer>> getObjectRelationList() {
		return ObjectRelationList;
	}
	
	@Override
	public void setObjectRelationList(Map<String, Map<String, Integer>> objectRelationList) {
		ObjectRelationList = objectRelationList;
	}
	@Override
	public void setWordExclude(Map<String, String> wordmap) {
		FastModelImpl.wordExclude=wordmap;
		
	}
	@Override
	public Map<String, String> getWordExclude() {
		
		return wordExclude;
	}
	@Override
	public void cleareNounAndVerb() {
		nounverbdb.clear();

	}
	@Override
	public void cleareNounAndVerbTriplet() {
		nounverbdbTriplet.clear();

	}
	@Override
	public void cleareLemaNounAndVerb() {
		lemmanounverbdb.clear();
		
	}
	
	@Override
	public void cleareHorosho() {
		wordHorosho.clear();
		
	}
	@Override
	public void clearePloho() {
		wordPloho.clear();
		
	}
	
	@Override
	public  Map<String, Integer> getNounverbdb() {
		return nounverbdb;
	}
	@Override
	public  void setNounverbdb(Map<String, Integer> nounverbdb) {
		FastModelImpl.nounverbdb = nounverbdb;
	}
	
	
	@Override
	public  Map<String, Integer> getNounverbdbTriplet() {
		return nounverbdbTriplet;
	}
	@Override
	public  void setNounverbdbTriplet(Map<String, Integer> nounverbdb) {
		FastModelImpl.nounverbdbTriplet = nounverbdb;
	}
	
	
	
	@Override
	public  Map<String, Integer> getLemmanounverbdb() {
		return lemmanounverbdb;
	}
	@Override
	public  void setLemmanounverbdb(Map<String, Integer> lemmanounverbdb) {
		FastModelImpl.lemmanounverbdb = lemmanounverbdb;
	}
	@Override
	public Map<String, String> getWordPositiveHuman() {
		return wordPositiveHuman;
		
	}
	
	@Override
	public void setWordPositiveHuman(Map<String, String> wordmap) {
		FastModelImpl.wordPositiveHuman = wordmap;
		
	}	
	
	@Override
	public Map<String, String> getWordNegativeHuman() {
		return wordNegativeHuman;
		
	}
	
	@Override
	public void setWordNegativeHuman(Map<String, String> wordmap) {
		FastModelImpl.wordNegativeHuman = wordmap;
		
	}
	
	@Override
	public Map<String, String> getWordPositive() {
		return wordPositive;
		
	}
	
	@Override
	public void setWordPositive(Map<String, String> wordmap) {
		FastModelImpl.wordPositive = wordmap;
		
	}	
	
	@Override
	public Map<String, String> getWordNegative() {
		return wordNegative;
		
	}
	
	@Override
	public void setWordNegative(Map<String, String> wordmap) {
		FastModelImpl.wordNegative = wordmap;
		
	}
	@Override
	public  Map<String, String> getMorfema() {
		return morfema;
	}
	@Override
	public  void setMorfema(Map<String, String> morfema) {
		FastModelImpl.morfema = morfema;
	}
	@Override
	public void setMorfemaWords(Map<String, String> morfword) {
		FastModelImpl.morfemawords=morfword;
		
	}
	@Override
	public Map<String, String> getMorfemaWords() {
	
		return morfemawords;
	}
	@Override
	public void setWordPredlogMesta(Map<String, String> wordmap) {
		FastModelImpl.predlogMesta=wordmap;
		
	}
	@Override
	public Map<String, String> getWordPredlogMesta() {
		return predlogMesta;
	}
	@Override
	public void setNarechiyaVremeni(Map<String, String> wordmap) {
		FastModelImpl.narechiyaVremeni=wordmap;
	}
	@Override
	public Map<String, String> getNarechiyaVremeni() {
		return narechiyaVremeni;
	}
	@Override
	public void setDniMeseats(Map<String, String> wordmap) {
		FastModelImpl.dniMeseats=wordmap;
		
	}
	@Override
	public Map<String, String> getDniMeseats() {
		
		return dniMeseats;
	}
	@Override
	public  void setMetricsTypeOne(Map<String, String> metricsTypeOne) {
		FastModelImpl.metricsTypeOne = metricsTypeOne;
	}
	@Override
	public void setMetricsTypeTwo(Map<String, String> wordmapTwo) {
		FastModelImpl.metricsTypeTwo=wordmapTwo;
		
	}
	@Override
	public void setMetricsTypeThree(Map<String, String> wordmapThree) {
		FastModelImpl.metricsTypeThree=wordmapThree;
		
	}
	@Override
	public  Map<String, String> getMetricsTypeOne() {
		return metricsTypeOne;
	}

	@Override
	public  Map<String, String> getMetricsTypeTwo() {
		return metricsTypeTwo;
	}
	@Override
	public  Map<String, String> getMetricsTypeThree() {
		return metricsTypeThree;
	}
	@Override
	public Map<String, String> getEconomicswords() {
		
		return economicswords;
	}
	@Override
	public void setEconomicswords(Map<String, String> economicsw) {
		FastModelImpl.economicswords=economicsw;
		
	}
	@Override
	public Map<String, Integer> getObjectAndVerbCountTripletPredlog() {
		return ObjectAndVerbCountTripletPredlog;
	}
	@Override
	public void setWordKvantor(Map<String, String> matwordsc) {
		
		FastModelImpl.kvantor=matwordsc;
	}
	@Override
	public Map<String, String> getWordKvantor() {
		
		return kvantor;
	}
	@Override
	public Map<String, Integer> getObjectHorosho() {
		return wordHorosho;
	}
	@Override
	public Map<String, Integer> getObjectPloho() {
		
		return wordPloho;
	}	
	@Override
	public void setWordHorosho(Map<String, Integer> matwordsc) {
		
		FastModelImpl.wordHorosho=matwordsc;
	}
	
	@Override
	public void setWordPloho(Map<String, Integer> matwordsc) {
		
		FastModelImpl.wordPloho=matwordsc;
	}
	
	
	

	
	
	@Override
	public Map<String, String> getWordExplainOne() {
		return explainTypeOne;
	}
	@Override
	public void setWordExplainOne(Map<String, String> matwordsc) {
		FastModelImpl.explainTypeOne=matwordsc;
	}
	@Override
	public void cleareExplainOne() {
		explainTypeOne.clear();
	}
	@Override
	public Map<String, String> getWordExplainTwo() {
		return explainTypeTwo;
	}
	@Override
	public void setWordExplainTwo(Map<String, String> matwordsc) {
		FastModelImpl.explainTypeTwo=matwordsc;
	}
	@Override
	public void cleareExplainTwo() {
		explainTypeTwo.clear();
	}
	@Override
	public Map<String, String> getWordExplainThree() {	
		return explainTypeThree;
	}
	@Override
	public void setWordExplainThree(Map<String, String> matwordsc) {
		FastModelImpl.explainTypeThree=matwordsc;
	}
	@Override
	public void cleareExplainThree() {
		explainTypeThree.clear();
	}
	@Override
	public void cleareQnonamesingle() {
		nonamesingledb.clear();
		
	}
	@Override
	public void setQnonamesingledb(Map<String, String> singledb) {
		FastModelImpl.nonamesingledb=singledb;
		
	}
	
	@Override
	public Map<String, String> getQnonamesingledb() {
		return nonamesingledb;
	}
	@Override
	public void cleareQnonamephrasedb() {
		nonamephrasedb.clear();
	}
	@Override
	public void setQnonamephrasedbdb(Map<String, String> phrasedb) {
		FastModelImpl.nonamephrasedb=phrasedb;
	}
	
	@Override
	public Map<String, String> getQnonamephrasedbdb() {
		return nonamephrasedb;
	}
	
	@Override
	public Map<String, Integer> getObjectAndAdjectiveCount() {
		return ObjectAndAdjectiveCount;
	}
	@Override
	public void cleareObjectAndAdjectiveCount() {
		ObjectAndAdjectiveCount.clear();
		
	}
	@Override
	public void setIilQPKeyValue(Map<String, List<String>> qpproperties) {
		FastModelImpl.qpproperties = qpproperties;
	}
	@Override
	public Map<String, List<String>> getIilQPKeyValue() {
		return qpproperties;
	}
	@Override
	public Map<String, Integer> getObjectAndVerbCountProperty() {
		
		 return ObjectAndVerbCountProperty;
	}
	@Override
	public void cleareObjectAndVerbCountProperty() {
		ObjectAndVerbCountProperty.clear();
		
	}
}
