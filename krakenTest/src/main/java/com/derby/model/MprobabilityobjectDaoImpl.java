package com.derby.model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
public class MprobabilityobjectDaoImpl implements MprobabilityobjectDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	
	
	@Override
	public void save(String guid, int objecta, int objectb, int objectc, int objectd,  int objecte, int objectf, 
			int objectg, int objecti,int objectj, int objectk) {
		
		
		
		String sql = "INSERT INTO mprobabilityobject(guid, objecta, objectb, objectc, objectd, objecte, objectf, objectg, objecti, objectj, objectk) VALUES (:guid, :objecta, :objectb, :objectc, :objectd, :objecte, :objectf, :objectg, :objecti, :objectj, :objectk)";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("guid", guid);
		paramMap.put("objecta", objecta);
		paramMap.put("objectb", objectb);
		paramMap.put("objectc", objectc);
		paramMap.put("objectd", objectd);
		paramMap.put("objecte", objecte);
		paramMap.put("objectf", objectf);
		paramMap.put("objectg", objectg);
		paramMap.put("objecti", objecti);
		paramMap.put("objectj", objectj);
		paramMap.put("objectk", objectk);
		namedParameterJdbcTemplate.update(sql, paramMap);
        
	}




}