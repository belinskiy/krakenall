package com.derby.model;

public class Mprobabilitymain {


	private String guid;

	private String tokentext;

	private int tokentotal;

	private int tokentype;

	public Mprobabilitymain() {
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getTokentext() {
		return this.tokentext;
	}

	public void setTokentext(String tokentext) {
		this.tokentext = tokentext;
	}

	public int getTokentotal() {
		return this.tokentotal;
	}

	public void setTokentotal(int tokentotal) {
		this.tokentotal = tokentotal;
	}

	public int getTokentype() {
		return this.tokentype;
	}

	public void setTokentype(int tokentype) {
		this.tokentype = tokentype;
	}

}