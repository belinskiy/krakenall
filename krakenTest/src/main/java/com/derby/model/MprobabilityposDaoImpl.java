package com.derby.model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
public class MprobabilityposDaoImpl implements MprobabilityposDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	
	
	@Override
	public void save(String guid, int tokenpos, int tokentype) {
		
		
		
		String sql = "INSERT INTO mprobabilitypos(guid, tokenpos, tokentype) VALUES (:guid, :tokenpos, :tokentype)";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("guid", guid);
		paramMap.put("tokenpos", tokenpos);
		paramMap.put("tokentype", tokentype);

		namedParameterJdbcTemplate.update(sql, paramMap);
        
	}




}