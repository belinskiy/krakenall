package com.derby.model;

import java.sql.Timestamp;
import java.util.List;

public interface MprobabilitydirtyDao {
	List<Mprobabilitydirty> findAll();

	Mprobabilitydirty findByName(String name);

	void save(String exguid, String thash, String tokenjson);

	List<Mprobabilitydirty> getAllDocuments(Timestamp dateupdate);

	void deleteAllDocuments(Timestamp dateupdate);
}
