package com.derby.model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
public class MprobabilitydirtyDaoImpl implements MprobabilitydirtyDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public Mprobabilitydirty findByName(String thash) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("thash", thash);
        
		String sql = "SELECT * FROM mprobabilitydirty WHERE thash=:thash";
		
		Mprobabilitydirty result = namedParameterJdbcTemplate.queryForObject(
                    sql,
                    params,
                    new MprobabilitydirtyMapper());
                    
        //new BeanPropertyRowMapper(Customer.class));
        
        return result;
        
	}
	
	@Override
	public void save(String exguid, String thash, String tokenjson) {
		
		
		
		String sql = "INSERT INTO mprobabilitydirty(exguid, thash, tokenjson, dateupdate) VALUES (:exguid, :thash, :tokenjson, :dateupdate)";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("exguid", exguid);
		paramMap.put("thash", thash);
		paramMap.put("tokenjson", tokenjson);
		Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
		paramMap.put("dateupdate", ourJavaTimestampObject);
		
		namedParameterJdbcTemplate.update(sql, paramMap);
        
	}
	
	
	@Override
	public void deleteAllDocuments(Timestamp dateupdate) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dateupdate", dateupdate);
		
		String sql = "DELETE FROM mprobabilitydirty WHERE dateupdate < :dateupdate";
		
        namedParameterJdbcTemplate.update(sql, params);

	}
	
	
	@Override
	public List<Mprobabilitydirty> getAllDocuments(Timestamp dateupdate) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dateupdate", dateupdate);
		
		String sql = "SELECT * FROM mprobabilitydirty WHERE dateupdate < :dateupdate";
		
        List<Mprobabilitydirty> result = namedParameterJdbcTemplate.query(sql, params, new MprobabilitydirtyMapper());
         
        return result;
        
	}

	@Override
	public List<Mprobabilitydirty> findAll() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		String sql = "SELECT * FROM mprobabilitydirty";
		
        List<Mprobabilitydirty> result = namedParameterJdbcTemplate.query(sql, params, new MprobabilitydirtyMapper());
         
        return result;
        
	}

	private static final class MprobabilitydirtyMapper implements RowMapper<Mprobabilitydirty> {

		public Mprobabilitydirty mapRow(ResultSet rs, int rowNum) throws SQLException {
			Mprobabilitydirty dirty = new Mprobabilitydirty();
			dirty.setExguid(rs.getString("exguid"));
			dirty.setThash(rs.getString("thash"));
			dirty.setTokenjson(rs.getString("tokenjson"));
			dirty.setDateupdate(rs.getTimestamp("dateupdate"));
			return dirty;
		}
	}

}