package com.derby.model;

import java.io.Serializable;
import javax.persistence.*;


public class Mprobabilitycounter{

	private String exguid;

	private String thash;

	public Mprobabilitycounter() {
	}


	public String getExguid() {
		return this.exguid;
	}

	public void setExguid(String exguid) {
		this.exguid = exguid;
	}

	public String getThash() {
		return this.thash;
	}

	public void setThash(String thash) {
		this.thash = thash;
	}

	@Override
	public String toString() {
		return "Mprobabilitycounter [exguid=" + exguid + ", thash=" + thash + "]";
	}

}