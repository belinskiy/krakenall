package com.derby.model;

import java.util.List;

public interface UserDao {
User findByName(String name);
	
	List<User> findAll();
}
