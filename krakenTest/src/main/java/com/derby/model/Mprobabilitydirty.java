package com.derby.model;

import java.sql.Timestamp;

public class Mprobabilitydirty {


//	private int pid;

	private String exguid;

	private String thash;

	
	private String tokenjson;
	
	private Timestamp dateupdate;

	public Mprobabilitydirty() {
	}

//	public int getPid() {
//		return this.pid;
//	}
//
//	public void setPid(int pid) {
//		this.pid = pid;
//	}

	public String getExguid() {
		return this.exguid;
	}

	public void setExguid(String exguid) {
		this.exguid = exguid;
	}

	public String getThash() {
		return this.thash;
	}

	public void setThash(String thashb) {
		this.thash = thashb;
	}

	public String getTokenjson() {
		return this.tokenjson;
	}

	public void setTokenjson(String tokenjson) {
		this.tokenjson = tokenjson;
	}
	public Timestamp getDateupdate() {
		return this.dateupdate;
	}

	public void setDateupdate(Timestamp dateupdate) {
		this.dateupdate = dateupdate;
	}

	@Override
	public String toString() {
		return "Mprobabilitydirty [exguid=" + exguid + ", thash=" + thash + ", tokenjson=" + tokenjson
				+ ", dateupdate=" + dateupdate + "]";
	}
}