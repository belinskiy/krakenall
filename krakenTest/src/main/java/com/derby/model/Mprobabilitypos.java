package com.derby.model;

public class Mprobabilitypos {

	private String guid;

	private int tokenpos;

	private int tokentype;

	public Mprobabilitypos() {
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getTokenpos() {
		return this.tokenpos;
	}

	public void setTokenpos(int tokenpos) {
		this.tokenpos = tokenpos;
	}

	public int getTokentype() {
		return this.tokentype;
	}

	public void setTokentype(int tokentype) {
		this.tokentype = tokentype;
	}

}