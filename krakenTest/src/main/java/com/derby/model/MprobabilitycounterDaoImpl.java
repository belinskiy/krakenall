package com.derby.model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
public class MprobabilitycounterDaoImpl implements MprobabilitycounterDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public List<Mprobabilitycounter> findByName(String thash) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("thash", thash);
        
		String sql = "SELECT * FROM mprobabilitycounter WHERE thash=:thash";
		
		List<Mprobabilitycounter> result = namedParameterJdbcTemplate.query(
                    sql,
                    params,
                    new MprobabilitycounterMapper());
                    
        //new BeanPropertyRowMapper(Customer.class));
        
        return result;
        
	}
	
	@Override
	public void save(String exguid, String thash) {
		
		
		
		String sql = "INSERT INTO mprobabilitycounter(exguid, thash) VALUES (:exguid, :thash)";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("exguid", exguid);
		paramMap.put("thash", thash);
		
		namedParameterJdbcTemplate.update(sql, paramMap);
        
	}
	
	
	
	private static final class MprobabilitycounterMapper implements RowMapper<Mprobabilitycounter> {

		public Mprobabilitycounter mapRow(ResultSet rs, int rowNum) throws SQLException {
			Mprobabilitycounter dirty = new Mprobabilitycounter();
			dirty.setExguid(rs.getString("exguid"));
			dirty.setThash(rs.getString("thash"));
			
			return dirty;
		}
	}

}