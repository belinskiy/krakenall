package com.derby.model;

import java.sql.Timestamp;
import java.util.List;

public interface MprobabilitycounterDao {
	
	List<Mprobabilitycounter> findByName(String thash);

	void save(String exguid, String thash);

}
