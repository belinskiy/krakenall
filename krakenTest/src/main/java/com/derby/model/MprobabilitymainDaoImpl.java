package com.derby.model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
public class MprobabilitymainDaoImpl implements MprobabilitymainDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public List<Mprobabilitymain> findByName(String tokentext) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("tokentext", tokentext);
        
		String sql = "SELECT * FROM mprobabilitymain WHERE tokentext=:tokentext";
		
		List<Mprobabilitymain> result = namedParameterJdbcTemplate.query(
                    sql,
                    params,
                    new MprobabilitymainMapper());
                    
      
        return result;
        
	}
	
	@Override
	public String save(String tokentext, int tokentotal, int tokentype) {
		
		 UUID idOne = UUID.randomUUID();
		 String guid=String.valueOf(idOne);
		 
		 
		String sql = "INSERT INTO mprobabilitymain(guid, tokentext, tokentotal, tokentype) VALUES (:guid, :tokentext, :tokentotal, :tokentype)";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("guid", guid);
		paramMap.put("tokentext", tokentext);
		paramMap.put("tokentotal", tokentotal);
		paramMap.put("tokentype", tokentype);
		
		namedParameterJdbcTemplate.update(sql, paramMap);
		return guid;
        
	}
	@Override
	public void update(String tokentext) {
		
		 
		String sql = "update mprobabilitymain set tokentotal=tokentotal+1 where tokentext=:tokentext";
		Map<String, Object> paramMap = new HashMap<String, Object>();
	  	paramMap.put("tokentext", tokentext);
		
		namedParameterJdbcTemplate.update(sql, paramMap);
	  
	}
	
	
	
	@Override
	public List<Mprobabilitymain> getAllDocuments(Timestamp dateupdate) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dateupdate", dateupdate);
		
		String sql = "SELECT * FROM mprobabilitymain WHERE dateupdate < :dateupdate";
		
        List<Mprobabilitymain> result = namedParameterJdbcTemplate.query(sql, params, new MprobabilitymainMapper());
         
        return result;
        
	}

	@Override
	public List<Mprobabilitymain> findAll() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		String sql = "SELECT * FROM mprobabilitymain";
		
        List<Mprobabilitymain> result = namedParameterJdbcTemplate.query(sql, params, new MprobabilitymainMapper());
         
        return result;
        
	}

	private static final class MprobabilitymainMapper implements RowMapper<Mprobabilitymain> {

		public Mprobabilitymain mapRow(ResultSet rs, int rowNum) throws SQLException {
			Mprobabilitymain dirty = new Mprobabilitymain();
			dirty.setGuid(rs.getString("guid"));
			dirty.setTokentext(rs.getString("tokentext"));
			dirty.setTokentotal(rs.getInt("tokentotal"));
			dirty.setTokentype(rs.getInt("tokentype"));
			return dirty;
		}
	}

}