package com.derby.model;

import java.sql.Timestamp;
import java.util.List;

public interface MprobabilitymainDao {
	List<Mprobabilitymain> findAll();

	List<Mprobabilitymain> findByName(String name);

	

	List<Mprobabilitymain> getAllDocuments(Timestamp dateupdate);

	

	String save(String thash, int tokentotal, int tokentype);

	void update(String tokentext);

}
