CREATE TABLE users (
  id         INTEGER PRIMARY KEY,
  name VARCHAR(30),
  email  VARCHAR(50)
);

CREATE TABLE mprobabilitydirty (
  thash  VARCHAR(150) PRIMARY KEY,
  exguid VARCHAR(150),
  
  tokenjson LONG VARCHAR,
  dateupdate TIMESTAMP
  
);

CREATE TABLE mprobabilitycounter (
  thash  VARCHAR(150) PRIMARY KEY,
  exguid VARCHAR(150)
  
);

CREATE TABLE mprobabilitymain (
  guid VARCHAR(150),
  tokentext  VARCHAR(150) PRIMARY KEY,
  tokentotal         INTEGER,
  tokentype         INTEGER
);

CREATE TABLE mprobabilityobject (
  guid  VARCHAR(150) ,
  objecta        INTEGER,
  objectb        INTEGER,
  objectc        INTEGER,
  objectd        INTEGER,
  objecte        INTEGER,
  objectf        INTEGER,
  objectg        INTEGER,
  objecti        INTEGER,
  objectj        INTEGER,
  objectk        INTEGER
);

CREATE TABLE mprobabilitypos (
  guid  VARCHAR(150) ,
  tokenpos         INTEGER,
  tokentype        INTEGER
);


